## Install dependencies
FROM node:22 as build

WORKDIR /www
COPY package*.json /www/
COPY *.config.js /www/
COPY dtsgen.json /www/

## Build packages

COPY scripts/ /www/scripts
COPY specifications/ /www/specifications

COPY services/runtime/package* /www/services/runtime/
COPY services/workspaces/package* /www/services/workspaces/
COPY services/api-gateway/package* /www/services/api-gateway/
COPY services/events/package* /www/services/events/

# Packages
COPY packages/ /www/packages
RUN rm -rf packages/design-system packages/sdk packages/blocks


RUN BUILD_PACKAGES=0 npm ci && \
    # Build packages && replace their node_modules symlinks with themselves
    npm run build:types && npm run build:packages && rm -rf node_modules/@prisme.ai && mv packages/ node_modules/@prisme.ai && \
    # Fix AWS Inspector false positive on monorepo-symlink-test : https://github.com/browserify/resolve/issues/319#issuecomment-1726675634
    rm -rf /www/node_modules/resolve/test

FROM node:22-alpine3.20 as node_modules

WORKDIR /www
COPY --from=build /www/package*.json /www/
COPY --from=build /www/dtsgen.json /www/
COPY --from=build /www/node_modules/ /www/node_modules/
COPY --from=build /www/specifications/ /www/specifications

COPY --from=build /www/services/runtime/node_modules/ /www/services/runtime/node_modules/


RUN apk upgrade --no-cache && \
    # gcompat required by RE2
    apk add gcompat curl && \
    # Security upgrade
    npm upgrade -g npm
