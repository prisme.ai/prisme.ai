module.exports = () => {
  process.env.TZ = 'UTC';
  const IN_RUNTIME = process.argv.some((cur) =>
    cur.includes('services/runtime')
  );
  return {
    moduleNameMapper: {
      '\\.tsx?$': 'babel-jest',
      '\\.s?css$': 'identity-obj-proxy',
      '\\.svg$': '<rootDir>/__mocks__/svgMock.js',
      '\\.svgr$': '<rootDir>/__mocks__/svgrMock.js',
      '^@/(.*)$': '<rootDir>/services/studio/src/$1',
    },
    modulePathIgnorePatterns: ['<rootDir>/tests-e2e/'],
    testEnvironment: IN_RUNTIME ? 'node' : 'jsdom',
    coveragePathIgnorePatterns: [
      // Console
      '<rootDir>/services/console/.*/context.ts',
      '<rootDir>/services/console/utils/yaml.ts',
      '<rootDir>/packages/validation',

      // dists
      '<rootDir>/.*/dist/',

      // Runtime
      '<rootDir>/services/runtime/src/eda',
      '<rootDir>/services/runtime/src/cache',
      '<rootDir>/services/runtime/src/storage',
      '<rootDir>/packages/broker',
    ],
    setupFiles: ['<rootDir>/.jest/setEnvVars.js'],
    testTimeout: 500,
    modulePathIgnorePatterns: ['<rootDir>/services/studio'],
  };
};
