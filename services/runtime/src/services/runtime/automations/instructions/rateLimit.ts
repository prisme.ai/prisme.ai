import { rateLimiter } from '../../../rateLimits/rateLimiter';
import { ContextsManager } from '../../contexts';

export async function rateLimit(
  rateLimit: Prismeai.RateLimit['rateLimit'],
  ctx: ContextsManager
) {
  return await rateLimiter.rateLimit(ctx.workspaceId, rateLimit);
}
