export class Break {
  constructor(
    public scope: Prismeai.Break['break']['scope'] = 'automation',
    public payload: any = {}
  ) {
    this.scope = scope;
    this.payload = payload;
  }
}

export enum InstructionType {
  Emit = 'emit',
  Fetch = 'fetch',
  Conditions = 'conditions',
  Set = 'set',
  Delete = 'delete',
  Break = 'break',
  Wait = 'wait',
  Repeat = 'repeat',
  All = 'all',
  Comment = 'comment',
  createUserTopic = 'createUserTopic',
  joinUserTopic = 'joinUserTopic',
  RateLimit = 'rateLimit',
  Try = 'try',
}
