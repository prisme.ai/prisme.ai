import aws4 from 'aws4';

export function signAwsV4Request(
  url: string,
  req: any,
  credentials: {
    accessKeyId: string;
    secretAccessKey: string;
  },
  opts: {
    region?: string;
    service?: string;
  } = {}
): Record<string, string> {
  const parsedURL = new URL(url);

  const signedOptions = {
    method: req.method,
    host: parsedURL.host,
    path: parsedURL.pathname + parsedURL.search,
    body: req.body,
    headers: req.headers,
    ...opts,
  };

  aws4.sign(signedOptions, credentials);

  return signedOptions.headers;
}
