import express from 'express';
import path from 'path';
import fs from 'fs';
import bodyParser from 'body-parser';
import multer from 'multer';

const upload = multer({
  limits: {
    fileSize: 1024 * 1024, // max size: 1M
  },
}).any();

export default function mock() {
  var app = express();

  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(express.static(path.join(__dirname, 'public')));

  const { imgBase64 } = JSON.parse(
    fs
      .readFileSync(path.resolve(__dirname, './binaryExamples.test.json'))
      .toString()
  );
  app.use('/image/jpeg', (req, res) => {
    res.setHeader('content-type', 'image/jpeg');
    res.send(Buffer.from(imgBase64, 'base64'));
  });
  app.use('/html', (req, res) => {
    res.setHeader('content-type', 'text/html; charset=utf-8');
    res.send(
      Buffer.from(
        'PCFET0NUWVBFIGh0bWw+CjxodG1sPgogIDxoZWFkPgogIDwvaGVhZD4KICA8Ym9keT4KICAgICAgPGgxPkhlcm1hbiBNZWx2aWxsZSAtIE1vYnktRGljazwvaDE+CgogICAgICA8ZGl2PgogICAgICAgIDxwPgogICAgICAgICAgQXZhaWxpbmcgaGltc2VsZiBvZiB0aGUgbWlsZCwgc3VtbWVyLWNvb2wgd2VhdGhlciB0aGF0IG5vdyByZWlnbmVkIGluIHRoZXNlIGxhdGl0dWRlcywgYW5kIGluIHByZXBhcmF0aW9uIGZvciB0aGUgcGVjdWxpYXJseSBhY3RpdmUgcHVyc3VpdHMgc2hvcnRseSB0byBiZSBhbnRpY2lwYXRlZCwgUGVydGgsIHRoZSBiZWdyaW1lZCwgYmxpc3RlcmVkIG9sZCBibGFja3NtaXRoLCBoYWQgbm90IHJlbW92ZWQgaGlzIHBvcnRhYmxlIGZvcmdlIHRvIHRoZSBob2xkIGFnYWluLCBhZnRlciBjb25jbHVkaW5nIGhpcyBjb250cmlidXRvcnkgd29yayBmb3IgQWhhYidzIGxlZywgYnV0IHN0aWxsIHJldGFpbmVkIGl0IG9uIGRlY2ssIGZhc3QgbGFzaGVkIHRvIHJpbmdib2x0cyBieSB0aGUgZm9yZW1hc3Q7IGJlaW5nIG5vdyBhbG1vc3QgaW5jZXNzYW50bHkgaW52b2tlZCBieSB0aGUgaGVhZHNtZW4sIGFuZCBoYXJwb29uZWVycywgYW5kIGJvd3NtZW4gdG8gZG8gc29tZSBsaXR0bGUgam9iIGZvciB0aGVtOyBhbHRlcmluZywgb3IgcmVwYWlyaW5nLCBvciBuZXcgc2hhcGluZyB0aGVpciB2YXJpb3VzIHdlYXBvbnMgYW5kIGJvYXQgZnVybml0dXJlLiBPZnRlbiBoZSB3b3VsZCBiZSBzdXJyb3VuZGVkIGJ5IGFuIGVhZ2VyIGNpcmNsZSwgYWxsIHdhaXRpbmcgdG8gYmUgc2VydmVkOyBob2xkaW5nIGJvYXQtc3BhZGVzLCBwaWtlLWhlYWRzLCBoYXJwb29ucywgYW5kIGxhbmNlcywgYW5kIGplYWxvdXNseSB3YXRjaGluZyBoaXMgZXZlcnkgc29vdHkgbW92ZW1lbnQsIGFzIGhlIHRvaWxlZC4gTmV2ZXJ0aGVsZXNzLCB0aGlzIG9sZCBtYW4ncyB3YXMgYSBwYXRpZW50IGhhbW1lciB3aWVsZGVkIGJ5IGEgcGF0aWVudCBhcm0uIE5vIG11cm11ciwgbm8gaW1wYXRpZW5jZSwgbm8gcGV0dWxhbmNlIGRpZCBjb21lIGZyb20gaGltLiBTaWxlbnQsIHNsb3csIGFuZCBzb2xlbW47IGJvd2luZyBvdmVyIHN0aWxsIGZ1cnRoZXIgaGlzIGNocm9uaWNhbGx5IGJyb2tlbiBiYWNrLCBoZSB0b2lsZWQgYXdheSwgYXMgaWYgdG9pbCB3ZXJlIGxpZmUgaXRzZWxmLCBhbmQgdGhlIGhlYXZ5IGJlYXRpbmcgb2YgaGlzIGhhbW1lciB0aGUgaGVhdnkgYmVhdGluZyBvZiBoaXMgaGVhcnQuIEFuZCBzbyBpdCB3YXMu4oCUTW9zdCBtaXNlcmFibGUhIEEgcGVjdWxpYXIgd2FsayBpbiB0aGlzIG9sZCBtYW4sIGEgY2VydGFpbiBzbGlnaHQgYnV0IHBhaW5mdWwgYXBwZWFyaW5nIHlhd2luZyBpbiBoaXMgZ2FpdCwgaGFkIGF0IGFuIGVhcmx5IHBlcmlvZCBvZiB0aGUgdm95YWdlIGV4Y2l0ZWQgdGhlIGN1cmlvc2l0eSBvZiB0aGUgbWFyaW5lcnMuIEFuZCB0byB0aGUgaW1wb3J0dW5pdHkgb2YgdGhlaXIgcGVyc2lzdGVkIHF1ZXN0aW9uaW5ncyBoZSBoYWQgZmluYWxseSBnaXZlbiBpbjsgYW5kIHNvIGl0IGNhbWUgdG8gcGFzcyB0aGF0IGV2ZXJ5IG9uZSBub3cga25ldyB0aGUgc2hhbWVmdWwgc3Rvcnkgb2YgaGlzIHdyZXRjaGVkIGZhdGUuIEJlbGF0ZWQsIGFuZCBub3QgaW5ub2NlbnRseSwgb25lIGJpdHRlciB3aW50ZXIncyBtaWRuaWdodCwgb24gdGhlIHJvYWQgcnVubmluZyBiZXR3ZWVuIHR3byBjb3VudHJ5IHRvd25zLCB0aGUgYmxhY2tzbWl0aCBoYWxmLXN0dXBpZGx5IGZlbHQgdGhlIGRlYWRseSBudW1ibmVzcyBzdGVhbGluZyBvdmVyIGhpbSwgYW5kIHNvdWdodCByZWZ1Z2UgaW4gYSBsZWFuaW5nLCBkaWxhcGlkYXRlZCBiYXJuLiBUaGUgaXNzdWUgd2FzLCB0aGUgbG9zcyBvZiB0aGUgZXh0cmVtaXRpZXMgb2YgYm90aCBmZWV0LiBPdXQgb2YgdGhpcyByZXZlbGF0aW9uLCBwYXJ0IGJ5IHBhcnQsIGF0IGxhc3QgY2FtZSBvdXQgdGhlIGZvdXIgYWN0cyBvZiB0aGUgZ2xhZG5lc3MsIGFuZCB0aGUgb25lIGxvbmcsIGFuZCBhcyB5ZXQgdW5jYXRhc3Ryb3BoaWVkIGZpZnRoIGFjdCBvZiB0aGUgZ3JpZWYgb2YgaGlzIGxpZmUncyBkcmFtYS4gSGUgd2FzIGFuIG9sZCBtYW4sIHdobywgYXQgdGhlIGFnZSBvZiBuZWFybHkgc2l4dHksIGhhZCBwb3N0cG9uZWRseSBlbmNvdW50ZXJlZCB0aGF0IHRoaW5nIGluIHNvcnJvdydzIHRlY2huaWNhbHMgY2FsbGVkIHJ1aW4uIEhlIGhhZCBiZWVuIGFuIGFydGlzYW4gb2YgZmFtZWQgZXhjZWxsZW5jZSwgYW5kIHdpdGggcGxlbnR5IHRvIGRvOyBvd25lZCBhIGhvdXNlIGFuZCBnYXJkZW47IGVtYnJhY2VkIGEgeW91dGhmdWwsIGRhdWdodGVyLWxpa2UsIGxvdmluZyB3aWZlLCBhbmQgdGhyZWUgYmxpdGhlLCBydWRkeSBjaGlsZHJlbjsgZXZlcnkgU3VuZGF5IHdlbnQgdG8gYSBjaGVlcmZ1bC1sb29raW5nIGNodXJjaCwgcGxhbnRlZCBpbiBhIGdyb3ZlLiBCdXQgb25lIG5pZ2h0LCB1bmRlciBjb3ZlciBvZiBkYXJrbmVzcywgYW5kIGZ1cnRoZXIgY29uY2VhbGVkIGluIGEgbW9zdCBjdW5uaW5nIGRpc2d1aXNlbWVudCwgYSBkZXNwZXJhdGUgYnVyZ2xhciBzbGlkIGludG8gaGlzIGhhcHB5IGhvbWUsIGFuZCByb2JiZWQgdGhlbSBhbGwgb2YgZXZlcnl0aGluZy4gQW5kIGRhcmtlciB5ZXQgdG8gdGVsbCwgdGhlIGJsYWNrc21pdGggaGltc2VsZiBkaWQgaWdub3JhbnRseSBjb25kdWN0IHRoaXMgYnVyZ2xhciBpbnRvIGhpcyBmYW1pbHkncyBoZWFydC4gSXQgd2FzIHRoZSBCb3R0bGUgQ29uanVyb3IhIFVwb24gdGhlIG9wZW5pbmcgb2YgdGhhdCBmYXRhbCBjb3JrLCBmb3J0aCBmbGV3IHRoZSBmaWVuZCwgYW5kIHNocml2ZWxsZWQgdXAgaGlzIGhvbWUuIE5vdywgZm9yIHBydWRlbnQsIG1vc3Qgd2lzZSwgYW5kIGVjb25vbWljIHJlYXNvbnMsIHRoZSBibGFja3NtaXRoJ3Mgc2hvcCB3YXMgaW4gdGhlIGJhc2VtZW50IG9mIGhpcyBkd2VsbGluZywgYnV0IHdpdGggYSBzZXBhcmF0ZSBlbnRyYW5jZSB0byBpdDsgc28gdGhhdCBhbHdheXMgaGFkIHRoZSB5b3VuZyBhbmQgbG92aW5nIGhlYWx0aHkgd2lmZSBsaXN0ZW5lZCB3aXRoIG5vIHVuaGFwcHkgbmVydm91c25lc3MsIGJ1dCB3aXRoIHZpZ29yb3VzIHBsZWFzdXJlLCB0byB0aGUgc3RvdXQgcmluZ2luZyBvZiBoZXIgeW91bmctYXJtZWQgb2xkIGh1c2JhbmQncyBoYW1tZXI7IHdob3NlIHJldmVyYmVyYXRpb25zLCBtdWZmbGVkIGJ5IHBhc3NpbmcgdGhyb3VnaCB0aGUgZmxvb3JzIGFuZCB3YWxscywgY2FtZSB1cCB0byBoZXIsIG5vdCB1bnN3ZWV0bHksIGluIGhlciBudXJzZXJ5OyBhbmQgc28sIHRvIHN0b3V0IExhYm9yJ3MgaXJvbiBsdWxsYWJ5LCB0aGUgYmxhY2tzbWl0aCdzIGluZmFudHMgd2VyZSByb2NrZWQgdG8gc2x1bWJlci4gT2gsIHdvZSBvbiB3b2UhIE9oLCBEZWF0aCwgd2h5IGNhbnN0IHRob3Ugbm90IHNvbWV0aW1lcyBiZSB0aW1lbHk/IEhhZHN0IHRob3UgdGFrZW4gdGhpcyBvbGQgYmxhY2tzbWl0aCB0byB0aHlzZWxmIGVyZSBoaXMgZnVsbCBydWluIGNhbWUgdXBvbiBoaW0sIHRoZW4gaGFkIHRoZSB5b3VuZyB3aWRvdyBoYWQgYSBkZWxpY2lvdXMgZ3JpZWYsIGFuZCBoZXIgb3JwaGFucyBhIHRydWx5IHZlbmVyYWJsZSwgbGVnZW5kYXJ5IHNpcmUgdG8gZHJlYW0gb2YgaW4gdGhlaXIgYWZ0ZXIgeWVhcnM7IGFuZCBhbGwgb2YgdGhlbSBhIGNhcmUta2lsbGluZyBjb21wZXRlbmN5LgogICAgICAgIDwvcD4KICAgICAgPC9kaXY+CiAgPC9ib2R5Pgo8L2h0bWw+',
        'base64'
      )
    );
  });
  app.use('/anything', upload, (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    let body = req.body;
    const files = !req.files
      ? {}
      : (req.files as any).reduce((files: any, file: any) => {
          if ((file?.mimetype || '').includes('text/plain')) {
            files[file.fieldname] = file.buffer.toString();
            return files;
          }
          files[file.fieldname] = `data:${
            file.mimetype
          };base64,${file.buffer.toString('base64')}`;
          return files;
        }, {});
    res.send({
      args: req.query,
      files,
      form: ['multipart/form-data', 'application/x-www-form-urlencoded'].some(
        (cur) => (req.headers['content-type'] || '').includes(cur)
      )
        ? body
        : {},
      headers: req.headers,
      json: body,
      method: req.method,
      origin: req.ip,
      url: req.url,
    });
  });
  app.use('/response-headers', (req, res) => {
    Object.entries(req.query).forEach(([k, v]) => {
      res.setHeader(k, v as string);
    });
    res.send();
  });

  // catch 404 and forward to error handler
  app.use(function (req, res, next) {
    let err: any = new Error('Not Found');
    err.status = 404;
    next(err);
  });

  app.use(function (err: any, req: any, res: any, next: any) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: {},
    });
  });

  return app;
}
