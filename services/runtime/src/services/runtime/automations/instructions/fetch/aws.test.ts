import { signAwsV4Request } from './aws';

describe('AWS Signature V4', () => {
  test('Works with accessKey:secretAccessKey pair', () => {
    const url =
      'https://bedrock-runtime.eu-west-3.amazonaws.com/model/mistral.mistral-large-2402-v1:0/converse-stream';
    const amzDate = '20250121T085434Z';
    const req = {
      method: 'POST',
      host: 'bedrock-runtime.eu-west-3.amazonaws.com',
      path: '/model/mistral.mistral-large-2402-v1:0/converse-stream',
      body: '{"system":[{"text":"Tu es un assistant très sympathique répondant aux questions qu\'on te pose"}],"messages":[{"role":"user","content":[{"text":"Bonjour, comment vas-tu ?"}]}]}',
      headers: {
        'x-correlation-id': 'f610ab67-ecd2-4092-846d-8938d56235ee',
        'content-type': 'application/json',
        'User-Agent': 'Prisme.ai Workspaces',
        'X-Amz-Date': amzDate,
      },
    };

    const credentials = {
      accessKeyId: 'myAccessKeyId',
      secretAccessKey: 'mySecretAccessKey',
    };

    const headers = signAwsV4Request(url, req, credentials, {
      service: 'bedrock',
    });
    expect(headers).toMatchObject({
      Authorization:
        'AWS4-HMAC-SHA256 Credential=myAccessKeyId/20250121/eu-west-3/bedrock/aws4_request, SignedHeaders=content-length;content-type;host;x-amz-date;x-correlation-id, Signature=8c441269f378df744f424ba8fc1e7c75e08dba396be4edbd597e306c0734444a',
    });
  });
});
