import { Readable } from 'node:stream';
import * as streamWeb from 'node:stream/web';
import mime from 'mime-types';

import { URL, URLSearchParams } from 'url';
import { ContextsManager } from '../../../contexts';
import {
  CORRELATION_ID_HEADER,
  FETCH_USER_AGENT_HEADER,
  API_URL,
  API_KEY_HEADER,
  FETCH_KEEPALIVE_DISABLED,
  FETCH_KEEPALIVE_TIMEOUT,
  FETCH_KEEPALIVE_TIMEOUT_THRESHOLD,
  FETCH_RETRY_CODES,
  FETCH_MAX_RETRIES,
  FETCH_RETRY_STATUS,
  FETCH_RETRY_MAX_TIMEOUT,
  FETCH_RETRY_MIN_TIMEOUT,
  FETCH_RETRY_TIMEOUT_FACTOR,
} from '../../../../../../config';
import { Broker } from '@prisme.ai/broker';
import { EventType } from '../../../../../eda';
import { Logger } from '../../../../../logger';
import { getAccessToken } from '../../../../../utils/jwks';
import { InvalidInstructionError } from '../../../../../errors';
import { ReadableStream } from '../../../../../utils';

import { throttler } from '../../../../rateLimits/throttler';
import { signAwsV4Request } from './aws';
import { processSseStream, SseType } from './processSseStream';
import { RequestInit, Agent } from 'undici';
import { isAllowedURL } from '../../../../../utils/isAllowedURL';

const AUTHENTICATE_PRISMEAI_URLS = ['/workspaces', '/pages', '/files']
  .map((cur) => `${API_URL}${cur}`)
  .concat((process.env.AUTHENTICATE_PRISMEAI_URLS || '').split(','));

const base64Regex =
  /^([0-9a-zA-Z+/]{4})*(([0-9a-zA-Z+/]{2}==)|([0-9a-zA-Z+/]{3}=))?$/;

function detectSSE(contentType: string): SseType | false {
  if (contentType.includes('text/event-stream')) {
    return SseType.Standard;
  }
  if (contentType.includes('application/vnd.amazon.eventstream')) {
    return SseType.Amazon;
  }
  return false;
}

export async function prismeaiFetch(
  fetchParams: Prismeai.Fetch['fetch'],
  logger: Logger,
  ctx: ContextsManager,
  broker: Broker,
  opts?: {
    maxRetries?: number;
  }
): Promise<any> {
  if (!fetchParams.url) {
    throw new InvalidInstructionError(`Invalid fetch instruction : empty url`);
  }
  await throttler.workspace(ctx.workspaceId, ctx.system).fetch(ctx);

  const maxRetries =
    typeof opts?.maxRetries !== 'undefined'
      ? opts.maxRetries
      : FETCH_MAX_RETRIES;
  let {
    url,
    body,
    headers = {},
    method,
    query,
    multipart,
    emitErrors = true,
    stream,
    prismeaiApiKey,
    outputMode,
    auth,
  } = fetchParams;
  const lowercasedHeaders: Record<string, string> = Object.entries(
    headers || {}
  )
    .map(([key, value]) => [key.toLowerCase(), value])
    .reduce(
      (acc, [key, value]) => ({
        ...acc,
        [key]: value,
      }),
      {}
    );

  if (!lowercasedHeaders['content-type'] && body) {
    headers = {
      ...headers,
      'content-type': 'application/json',
    };
  }

  const isPrismeaiRequest = AUTHENTICATE_PRISMEAI_URLS.some((cur) =>
    url.startsWith(cur)
  );
  const parsedURL = new URL(url);
  if (!isAllowedURL(parsedURL)) {
    throw new InvalidInstructionError(`Forbidden host '${parsedURL.host}'`);
  }

  if (
    isPrismeaiRequest &&
    !lowercasedHeaders['x-prismeai-token'] &&
    !lowercasedHeaders['authorization']
  ) {
    const { jwt } = await getAccessToken({
      userId: ctx?.session?.origin?.userId || ctx?.session?.userId,
      prismeaiSessionId:
        ctx?.session?.origin?.sessionId || ctx?.session?.sessionId,
      expiresIn: 10,
      correlationId: ctx.correlationId,
      workspaceId: ctx.workspaceId,
    });
    headers['Authorization'] = `Bearer ${jwt}`;
  }

  if (isPrismeaiRequest && prismeaiApiKey?.name) {
    headers[API_KEY_HEADER] = await ctx.getWorkspaceApiKey(
      prismeaiApiKey?.name
    );
  }

  const params: RequestInit = {
    headers: {
      [CORRELATION_ID_HEADER]: ctx.run.correlationId,
      ...headers,
      'User-Agent': FETCH_USER_AGENT_HEADER,
      'x-prismeai-workspace-id': ctx.workspaceId,
    },
    method: (method || 'GET').toUpperCase(),
    keepalive: !FETCH_KEEPALIVE_DISABLED,
    dispatcher: new Agent({
      keepAliveTimeout: FETCH_KEEPALIVE_TIMEOUT,
      keepAliveTimeoutThreshold: FETCH_KEEPALIVE_TIMEOUT_THRESHOLD,
    }),
  };

  // Process body
  if ((body || multipart) && (method || 'get')?.toLowerCase() !== 'get') {
    if (multipart) {
      delete (params.headers as any)['content-type'];
      params.body = new FormData() as any;
      for (const { fieldname, value, ...opts } of multipart) {
        let convertedValue: any = value;
        // Only test the beginning as Regexp.test is under the hood a recursive function that crashes (exceeded call stack) on big file base64
        const isBase64 =
          typeof value === 'string' &&
          !parseInt(value) &&
          value.toLowerCase() != 'false' &&
          value.toLowerCase() != 'true' &&
          base64Regex.test(value.slice(0, 1024)); // Must slice a multiplicator of 4 to fit with b64 regex !
        if (isBase64 && opts.filename) {
          convertedValue = Buffer.from(value as any, 'base64');
        } else if (Array.isArray(value)) {
          convertedValue = Buffer.from(value);
        } else if (
          !((value as any) instanceof Buffer) &&
          typeof value != 'string'
        ) {
          convertedValue = JSON.stringify(value);
        }

        if (!opts?.contentType && opts.filename) {
          const detectedMimetype = mime.lookup(opts.filename);
          opts.contentType = detectedMimetype || '';
        }

        if (typeof convertedValue !== 'string' || opts.filename) {
          (params.body as FormData).append(
            fieldname,
            new Blob([convertedValue], {
              type: opts?.contentType,
            }),
            opts.filename
          );
        } else {
          (params.body as FormData).append(fieldname, convertedValue);
        }
      }
    } else if (
      lowercasedHeaders['content-type'] &&
      (lowercasedHeaders['content-type'] || '').includes(
        'application/x-www-form-urlencoded'
      )
    ) {
      params.body = typeof body === 'object' ? new URLSearchParams(body) : body;
    } else {
      params.body = typeof body === 'object' ? JSON.stringify(body) : body;
    }
  }

  for (let [key, val] of Object.entries(query || {})) {
    if (Array.isArray(val)) {
      val.forEach((cur) => {
        parsedURL.searchParams.append(key, cur);
      });
    } else {
      parsedURL.searchParams.append(key, val);
    }
  }

  // AWS Authentication
  if (auth?.awsv4?.accessKeyId && auth?.awsv4?.secretAccessKey) {
    const { accessKeyId, secretAccessKey, ...opts } = auth.awsv4;
    params.headers = {
      ...params.headers,
      ...signAwsV4Request(
        parsedURL.toString(),
        params,
        {
          accessKeyId,
          secretAccessKey,
        },
        opts
      ),
    };
  }

  let result: Response;
  try {
    result = await fetch(parsedURL, params as any);
    if (FETCH_RETRY_STATUS.includes(result.status)) {
      const json = await getResponseBody(result);
      const retryAfterMs = json.details?.retryAfter
        ? json.details?.retryAfter * 1000
        : undefined;
      throw {
        retryOnStatus: result.status,
        retryAfter:
          retryAfterMs && retryAfterMs < FETCH_RETRY_MAX_TIMEOUT
            ? retryAfterMs
            : undefined,
      };
    }
  } catch (e) {
    const networkError = e as any;
    const retryableError =
      networkError?.retryOnStatus ||
      (networkError?.code && FETCH_RETRY_CODES.includes(networkError?.code)) ||
      FETCH_RETRY_CODES.includes(networkError?.cause?.code);

    if (retryableError && maxRetries > 0) {
      const tryCounter = FETCH_MAX_RETRIES - (maxRetries - 1);
      const retryAfter =
        networkError?.retryAfter ||
        Math.min(
          FETCH_RETRY_MIN_TIMEOUT * FETCH_RETRY_TIMEOUT_FACTOR ** tryCounter,
          FETCH_RETRY_MAX_TIMEOUT
        );

      logger.info({
        msg: `Retrying request towards ${url} in ${retryAfter}ms as we received ${
          networkError?.retryOnStatus
            ? `${networkError?.retryOnStatus} status`
            : `${networkError?.code || networkError?.cause?.code} error`
        } (try n°${tryCounter}) ...`,
      });
      await new Promise((resolve) => setTimeout(resolve, retryAfter));
      return await prismeaiFetch(fetchParams, logger, ctx, broker, {
        ...opts,
        maxRetries: maxRetries - 1,
      });
    }

    const error = {
      error: networkError?.cause?.code || 'FetchNetworkError',
      message:
        networkError?.message || networkError?.reason || `${networkError}`,
      details: networkError?.cause,
      status: networkError?.retryOnStatus,
    };
    if (emitErrors) {
      broker.send<Prismeai.FailedFetch['payload']>(EventType.FailedFetch, {
        request: fetchParams,
        error,
      });
    }
    return { error };
  }

  let responseBody: any, error: any;
  const httpError = result.status >= 400 && result.status < 600;
  const isSSE = detectSSE(result.headers.get('content-type') || '');
  if (!stream?.event && !isSSE) {
    responseBody = await getResponseBody(result);
    if (httpError && emitErrors) {
      error = responseBody;
    }
  } else if (result.body) {
    outputMode = 'detailed_response';
    responseBody = await processSseStream(
      isSSE as SseType,
      // result.body incorrectly typed, missing undici typings
      Readable.fromWeb(result.body as any as streamWeb.ReadableStream),
      broker,
      logger,
      stream
    );
  }

  if (error && emitErrors) {
    broker.send<Prismeai.FailedFetch['payload']>(EventType.FailedFetch, {
      request: fetchParams,
      response: {
        status: result.status,
        body: error,
        headers: result.headers,
      },
    });
  }

  if ((outputMode === 'data_url' || outputMode === 'base64') && !httpError) {
    const base64 =
      typeof responseBody === 'string'
        ? Buffer.from(responseBody).toString('base64')
        : responseBody.toString('base64');
    if (outputMode === 'base64') {
      return base64;
    }
    return `data:${
      result.headers.get('content-type') || 'application/octet-stream'
    };base64,${base64}`;
  } else if (outputMode === 'detailed_response') {
    const headers: Record<string, any> = {};
    result.headers.forEach((value, key) => {
      headers[key] =
        Array.isArray(value) && value.length === 1 ? value[0] : value;
    });
    return {
      headers,
      status: result.status,
      body: responseBody,
      stream: responseBody instanceof ReadableStream,
    };
  } else {
    return responseBody;
  }
}

async function getResponseBody(response: Response) {
  const contentType = response.headers.get('Content-Type') || '';
  if (contentType.includes('application/json')) {
    const json = await response.json();
    return json;
  }
  if (contentType.includes('text/')) {
    return await response.text();
  }
  if (
    contentType.includes('application/') ||
    contentType.includes('image/') ||
    contentType.includes('audio/')
  ) {
    return Buffer.from(await response.arrayBuffer());
  }
  return await response.text();
}
