// Allow passing our fake test api keys/jwt to remote test endpoints
process.env.AUTHENTICATE_PRISMEAI_URLS = 'http://,https://';

import { createServer } from 'node:http';
import fs from 'fs';
import path from 'path';
import { Broker } from '@prisme.ai/broker';
import { ContextsManager } from '../../../contexts';
import { prismeaiFetch } from './index';
import { Broker as MockBroker } from '@prisme.ai/broker/lib/__mocks__';
import { ReadableStream } from '../../../../../utils';
import mockServer from './mockServer';

jest.setTimeout(8000);

const app = mockServer();
const port = 7777;
const mockHttpServer = app.listen(port, function () {
  console.log(
    'Express server listening on port ' + (mockHttpServer.address() as any).port
  );
});

const servers: any = [];
const { imgBase64 } = JSON.parse(
  fs
    .readFileSync(path.resolve(__dirname, './binaryExamples.test.json'))
    .toString()
);
const testImg = Buffer.from(imgBase64, 'base64');

type ExpectedResponse = {
  body?: any;
  bodyStrContains?: string;
  headers?: any;
  status?: number;
};
const tests: Record<
  string,
  Omit<Prismeai.Fetch['fetch'], 'method'> & {
    method?: string;
    response: ExpectedResponse;
  }
> = {
  basic: {
    url: 'http://localhost:7777/anything',
    body: {
      foo: 'bar',
    },
    query: {
      someQuery: 'string',
    },
    headers: {
      foo: 'barbar',
    },
    method: 'POST',
    response: {
      headers: {
        'content-type': 'application/json',
      },
      status: 200,
      body: {
        args: { someQuery: 'string' },
        json: {
          foo: 'bar',
        },
        method: 'POST',
      },
    },
  },

  headersResponse: {
    url: 'http://localhost:7777/response-headers?foo=bar',
    headers: {},
    response: {
      headers: {
        foo: 'bar',
      },
    },
  },

  textResponse: {
    url: 'http://localhost:7777/html',
    headers: {},
    body: 'Some text content',
    response: {
      bodyStrContains:
        'Availing himself of the mild, summer-cool weather that now reigned in these latitudes',
    },
  },

  imgResponse: {
    url: 'http://localhost:7777/image/jpeg',
    headers: {},
    body: 'Some text content',
    response: {
      headers: {
        'content-type': 'image/jpeg',
        'content-length': '35588',
      },
      body: testImg,
    },
  },

  formUrlEncoded: {
    url: 'http://localhost:7777/anything',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    method: 'POST',
    body: {
      foo: 'bar',
    },
    response: {
      body: {
        form: {
          foo: 'bar',
        },
        method: 'POST',
      },
    },
  },

  bufferUpload: {
    url: 'http://localhost:7777/anything',
    headers: {},
    method: 'POST',
    multipart: [
      {
        fieldname: 'file',
        value: testImg as any,
        filename: 'test.jpg',
        contentType: 'image/jpeg',
      },
    ],
    response: {
      body: {
        files: {
          file: `data:image/jpeg;base64,${testImg.toString('base64')}`,
        },
      },
    },
  },

  base64Upload: {
    url: 'http://localhost:7777/anything',
    headers: {},
    method: 'POST',
    multipart: [
      {
        fieldname: 'file',
        value: testImg.toString('base64'),
        filename: 'test.jpg',
        contentType: 'image/jpeg',
      },
    ],
    response: {
      body: {
        files: {
          file: `data:image/jpeg;base64,${testImg.toString('base64')}`,
        },
      },
    },
  },

  textUpload: {
    url: 'http://localhost:7777/anything',
    headers: {},
    method: 'POST',
    multipart: [
      {
        fieldname: 'file',
        value: 'Contenu test',
        filename: 'test.txt',
      },
    ],
    response: {
      body: {
        files: {
          file: `Contenu test`,
        },
      },
    },
  },

  uploadWithMimetypeDetection: {
    url: 'http://localhost:7777/anything',
    headers: {},
    method: 'POST',
    multipart: [
      {
        fieldname: 'file',
        value: testImg as any,
        filename: 'test.jpg', // Skip contentType as we want it to be auto detected
      },
    ],
    response: {
      body: {
        files: {
          file: `data:image/jpeg;base64,${testImg.toString('base64')}`,
        },
      },
    },
  },

  uploadWithMetadata: {
    url: 'http://localhost:7777/anything',
    headers: {},
    method: 'POST',
    multipart: [
      {
        fieldname: 'file',
        value: testImg as any,
        filename: 'test.jpg', // Skip contentType as we want it to be auto detected
      },
      {
        fieldname: 'category',
        value: 'someUserCategory',
      },
    ],
    response: {
      body: {
        files: {
          file: `data:image/jpeg;base64,${testImg.toString('base64')}`,
        },
        form: {
          category: 'someUserCategory',
        },
      },
    },
  },

  sseStreaming: {
    url: 'https://api.staging.prisme.ai/v2/workspaces/RmHcGzB/webhooks/writeHTTPsse',
    response: {
      status: 200,
      headers: {
        'content-type': 'text/event-stream',
      },
    },
  },

  autoRetries: {
    url: 'https://api.staging.prisme.ai/v2/workspaces/RmHcGzB/webhooks/testRetries',
    response: {
      status: 200,
      body: {
        success: true,
      },
    },
  },
};

function testResponse(
  res: {
    body?: any | 'ignore';
    headers?: object | 'ignore';
    status?: number | 'ignore';
  },
  expected: ExpectedResponse
) {
  if (res?.body !== 'ignore') {
    if (expected?.body) {
      if (expected.body instanceof Buffer) {
        expect(res.body).toBeInstanceOf(Buffer);
        expect(res.body).toEqual(expected.body);
      } else {
        expect(res.body).toMatchObject(expected.body);
      }
    }
    if (expected?.bodyStrContains) {
      expect(res.body).toContain(expected.bodyStrContains);
    }
  }
  if (res?.headers !== 'ignore') {
    if (expected?.headers) {
      expect(res.headers).toMatchObject(expected.headers);
    }
  }
  if (res?.status !== 'ignore') {
    if (expected?.status) {
      expect(res.status).toEqual(expected.status);
    }
  }
}

const getApiKey = (wkId) => 'apiKey:' + wkId;
const getMocks = () => {
  const broker = new MockBroker() as any as Broker<any>;

  const workspaceId = `wk-${Math.round(Math.random() * 100)}`;
  const correlationId = `correl-${Math.round(Math.random() * 10000)}`;
  const ctx = {
    workspaceId,
    correlationId,
    system: {}, // System secrets,
    run: {
      correlationId,
    },
    session: {
      userId: `usr-${Math.round(Math.random() * 10000)}`,
      sessionId: `sess-${Math.round(Math.random() * 10000)}`,
    },
    getWorkspaceApiKey: () => getApiKey(ctx.workspaceId),
  } as any as ContextsManager;

  return {
    broker: broker.child({
      workspaceId,
      userId: ctx.session?.userId,
      sessionId: ctx.session?.sessionId,
    }),
    ctx,
  };
};

describe('Request formats', () => {
  const { broker, ctx } = getMocks();

  it('application/json', async () => {
    const body = await prismeaiFetch(
      {
        ...(tests.basic as any),
      },
      console as any,
      ctx,
      broker
    );
    testResponse(
      { body, headers: 'ignore', status: 'ignore' },
      tests.basic.response
    );
  });

  it('application/x-www-form-urlencoded', async () => {
    const body = await prismeaiFetch(
      {
        ...(tests.formUrlEncoded as any),
      },
      console as any,
      ctx,
      broker
    );
    expect(body.headers['content-type']).toEqual(
      'application/x-www-form-urlencoded'
    );
    testResponse({ body }, tests.formUrlEncoded.response);
  });

  it('data_url as input url', async () => {
    const res = await prismeaiFetch(
      {
        url: 'data:text/csv;filename:OUNGn7dEpm7f045aP6WjF.testsSpec.csv; base64,77u/cXVlc3Rpb247cmVmZXJlbmNlQW5zd2VyDQpCb25qb3VyICEgIDtCb25qb3VyLCBjb21tZW50IHB1aXMtamUgdm91cyBhaWRlciA/',
        outputMode: 'detailed_response',
      },
      console as any,
      ctx,
      broker
    );
    expect(res.headers['content-type']).toEqual('text/csv');
    expect(res.body).toEqual(
      'question;referenceAnswer\r\nBonjour !  ;Bonjour, comment puis-je vous aider ?'
    );
  });
});

describe('Output modes', () => {
  const { broker, ctx } = getMocks();

  it('detailed_response from application/json', async () => {
    const res = await prismeaiFetch(
      {
        ...(tests.basic as any),
        outputMode: 'detailed_response',
      },
      console as any,
      ctx,
      broker
    );

    testResponse({ ...res, headers: 'ignore' }, tests.basic.response);
  });

  it('detailed_response including headers', async () => {
    const res = await prismeaiFetch(
      {
        ...(tests.headersResponse as any),
        outputMode: 'detailed_response',
      },
      console as any,
      ctx,
      broker
    );

    testResponse(res, tests.headersResponse.response);
  });

  it('body (default) from text/html', async () => {
    const res = await prismeaiFetch(
      {
        ...(tests.textResponse as any),
      },
      console as any,
      ctx,
      broker
    );
    testResponse({ body: res }, tests.textResponse.response);
  });

  it('data_url with text content-type', async () => {
    const res = await prismeaiFetch(
      {
        ...(tests.textResponse as any),
        outputMode: 'data_url',
      },
      console as any,
      ctx,
      broker
    );
    expect(res).toEqual(
      'data:text/html; charset=utf-8;base64,PCFET0NUWVBFIGh0bWw+CjxodG1sPgogIDxoZWFkPgogIDwvaGVhZD4KICA8Ym9keT4KICAgICAgPGgxPkhlcm1hbiBNZWx2aWxsZSAtIE1vYnktRGljazwvaDE+CgogICAgICA8ZGl2PgogICAgICAgIDxwPgogICAgICAgICAgQXZhaWxpbmcgaGltc2VsZiBvZiB0aGUgbWlsZCwgc3VtbWVyLWNvb2wgd2VhdGhlciB0aGF0IG5vdyByZWlnbmVkIGluIHRoZXNlIGxhdGl0dWRlcywgYW5kIGluIHByZXBhcmF0aW9uIGZvciB0aGUgcGVjdWxpYXJseSBhY3RpdmUgcHVyc3VpdHMgc2hvcnRseSB0byBiZSBhbnRpY2lwYXRlZCwgUGVydGgsIHRoZSBiZWdyaW1lZCwgYmxpc3RlcmVkIG9sZCBibGFja3NtaXRoLCBoYWQgbm90IHJlbW92ZWQgaGlzIHBvcnRhYmxlIGZvcmdlIHRvIHRoZSBob2xkIGFnYWluLCBhZnRlciBjb25jbHVkaW5nIGhpcyBjb250cmlidXRvcnkgd29yayBmb3IgQWhhYidzIGxlZywgYnV0IHN0aWxsIHJldGFpbmVkIGl0IG9uIGRlY2ssIGZhc3QgbGFzaGVkIHRvIHJpbmdib2x0cyBieSB0aGUgZm9yZW1hc3Q7IGJlaW5nIG5vdyBhbG1vc3QgaW5jZXNzYW50bHkgaW52b2tlZCBieSB0aGUgaGVhZHNtZW4sIGFuZCBoYXJwb29uZWVycywgYW5kIGJvd3NtZW4gdG8gZG8gc29tZSBsaXR0bGUgam9iIGZvciB0aGVtOyBhbHRlcmluZywgb3IgcmVwYWlyaW5nLCBvciBuZXcgc2hhcGluZyB0aGVpciB2YXJpb3VzIHdlYXBvbnMgYW5kIGJvYXQgZnVybml0dXJlLiBPZnRlbiBoZSB3b3VsZCBiZSBzdXJyb3VuZGVkIGJ5IGFuIGVhZ2VyIGNpcmNsZSwgYWxsIHdhaXRpbmcgdG8gYmUgc2VydmVkOyBob2xkaW5nIGJvYXQtc3BhZGVzLCBwaWtlLWhlYWRzLCBoYXJwb29ucywgYW5kIGxhbmNlcywgYW5kIGplYWxvdXNseSB3YXRjaGluZyBoaXMgZXZlcnkgc29vdHkgbW92ZW1lbnQsIGFzIGhlIHRvaWxlZC4gTmV2ZXJ0aGVsZXNzLCB0aGlzIG9sZCBtYW4ncyB3YXMgYSBwYXRpZW50IGhhbW1lciB3aWVsZGVkIGJ5IGEgcGF0aWVudCBhcm0uIE5vIG11cm11ciwgbm8gaW1wYXRpZW5jZSwgbm8gcGV0dWxhbmNlIGRpZCBjb21lIGZyb20gaGltLiBTaWxlbnQsIHNsb3csIGFuZCBzb2xlbW47IGJvd2luZyBvdmVyIHN0aWxsIGZ1cnRoZXIgaGlzIGNocm9uaWNhbGx5IGJyb2tlbiBiYWNrLCBoZSB0b2lsZWQgYXdheSwgYXMgaWYgdG9pbCB3ZXJlIGxpZmUgaXRzZWxmLCBhbmQgdGhlIGhlYXZ5IGJlYXRpbmcgb2YgaGlzIGhhbW1lciB0aGUgaGVhdnkgYmVhdGluZyBvZiBoaXMgaGVhcnQuIEFuZCBzbyBpdCB3YXMu4oCUTW9zdCBtaXNlcmFibGUhIEEgcGVjdWxpYXIgd2FsayBpbiB0aGlzIG9sZCBtYW4sIGEgY2VydGFpbiBzbGlnaHQgYnV0IHBhaW5mdWwgYXBwZWFyaW5nIHlhd2luZyBpbiBoaXMgZ2FpdCwgaGFkIGF0IGFuIGVhcmx5IHBlcmlvZCBvZiB0aGUgdm95YWdlIGV4Y2l0ZWQgdGhlIGN1cmlvc2l0eSBvZiB0aGUgbWFyaW5lcnMuIEFuZCB0byB0aGUgaW1wb3J0dW5pdHkgb2YgdGhlaXIgcGVyc2lzdGVkIHF1ZXN0aW9uaW5ncyBoZSBoYWQgZmluYWxseSBnaXZlbiBpbjsgYW5kIHNvIGl0IGNhbWUgdG8gcGFzcyB0aGF0IGV2ZXJ5IG9uZSBub3cga25ldyB0aGUgc2hhbWVmdWwgc3Rvcnkgb2YgaGlzIHdyZXRjaGVkIGZhdGUuIEJlbGF0ZWQsIGFuZCBub3QgaW5ub2NlbnRseSwgb25lIGJpdHRlciB3aW50ZXIncyBtaWRuaWdodCwgb24gdGhlIHJvYWQgcnVubmluZyBiZXR3ZWVuIHR3byBjb3VudHJ5IHRvd25zLCB0aGUgYmxhY2tzbWl0aCBoYWxmLXN0dXBpZGx5IGZlbHQgdGhlIGRlYWRseSBudW1ibmVzcyBzdGVhbGluZyBvdmVyIGhpbSwgYW5kIHNvdWdodCByZWZ1Z2UgaW4gYSBsZWFuaW5nLCBkaWxhcGlkYXRlZCBiYXJuLiBUaGUgaXNzdWUgd2FzLCB0aGUgbG9zcyBvZiB0aGUgZXh0cmVtaXRpZXMgb2YgYm90aCBmZWV0LiBPdXQgb2YgdGhpcyByZXZlbGF0aW9uLCBwYXJ0IGJ5IHBhcnQsIGF0IGxhc3QgY2FtZSBvdXQgdGhlIGZvdXIgYWN0cyBvZiB0aGUgZ2xhZG5lc3MsIGFuZCB0aGUgb25lIGxvbmcsIGFuZCBhcyB5ZXQgdW5jYXRhc3Ryb3BoaWVkIGZpZnRoIGFjdCBvZiB0aGUgZ3JpZWYgb2YgaGlzIGxpZmUncyBkcmFtYS4gSGUgd2FzIGFuIG9sZCBtYW4sIHdobywgYXQgdGhlIGFnZSBvZiBuZWFybHkgc2l4dHksIGhhZCBwb3N0cG9uZWRseSBlbmNvdW50ZXJlZCB0aGF0IHRoaW5nIGluIHNvcnJvdydzIHRlY2huaWNhbHMgY2FsbGVkIHJ1aW4uIEhlIGhhZCBiZWVuIGFuIGFydGlzYW4gb2YgZmFtZWQgZXhjZWxsZW5jZSwgYW5kIHdpdGggcGxlbnR5IHRvIGRvOyBvd25lZCBhIGhvdXNlIGFuZCBnYXJkZW47IGVtYnJhY2VkIGEgeW91dGhmdWwsIGRhdWdodGVyLWxpa2UsIGxvdmluZyB3aWZlLCBhbmQgdGhyZWUgYmxpdGhlLCBydWRkeSBjaGlsZHJlbjsgZXZlcnkgU3VuZGF5IHdlbnQgdG8gYSBjaGVlcmZ1bC1sb29raW5nIGNodXJjaCwgcGxhbnRlZCBpbiBhIGdyb3ZlLiBCdXQgb25lIG5pZ2h0LCB1bmRlciBjb3ZlciBvZiBkYXJrbmVzcywgYW5kIGZ1cnRoZXIgY29uY2VhbGVkIGluIGEgbW9zdCBjdW5uaW5nIGRpc2d1aXNlbWVudCwgYSBkZXNwZXJhdGUgYnVyZ2xhciBzbGlkIGludG8gaGlzIGhhcHB5IGhvbWUsIGFuZCByb2JiZWQgdGhlbSBhbGwgb2YgZXZlcnl0aGluZy4gQW5kIGRhcmtlciB5ZXQgdG8gdGVsbCwgdGhlIGJsYWNrc21pdGggaGltc2VsZiBkaWQgaWdub3JhbnRseSBjb25kdWN0IHRoaXMgYnVyZ2xhciBpbnRvIGhpcyBmYW1pbHkncyBoZWFydC4gSXQgd2FzIHRoZSBCb3R0bGUgQ29uanVyb3IhIFVwb24gdGhlIG9wZW5pbmcgb2YgdGhhdCBmYXRhbCBjb3JrLCBmb3J0aCBmbGV3IHRoZSBmaWVuZCwgYW5kIHNocml2ZWxsZWQgdXAgaGlzIGhvbWUuIE5vdywgZm9yIHBydWRlbnQsIG1vc3Qgd2lzZSwgYW5kIGVjb25vbWljIHJlYXNvbnMsIHRoZSBibGFja3NtaXRoJ3Mgc2hvcCB3YXMgaW4gdGhlIGJhc2VtZW50IG9mIGhpcyBkd2VsbGluZywgYnV0IHdpdGggYSBzZXBhcmF0ZSBlbnRyYW5jZSB0byBpdDsgc28gdGhhdCBhbHdheXMgaGFkIHRoZSB5b3VuZyBhbmQgbG92aW5nIGhlYWx0aHkgd2lmZSBsaXN0ZW5lZCB3aXRoIG5vIHVuaGFwcHkgbmVydm91c25lc3MsIGJ1dCB3aXRoIHZpZ29yb3VzIHBsZWFzdXJlLCB0byB0aGUgc3RvdXQgcmluZ2luZyBvZiBoZXIgeW91bmctYXJtZWQgb2xkIGh1c2JhbmQncyBoYW1tZXI7IHdob3NlIHJldmVyYmVyYXRpb25zLCBtdWZmbGVkIGJ5IHBhc3NpbmcgdGhyb3VnaCB0aGUgZmxvb3JzIGFuZCB3YWxscywgY2FtZSB1cCB0byBoZXIsIG5vdCB1bnN3ZWV0bHksIGluIGhlciBudXJzZXJ5OyBhbmQgc28sIHRvIHN0b3V0IExhYm9yJ3MgaXJvbiBsdWxsYWJ5LCB0aGUgYmxhY2tzbWl0aCdzIGluZmFudHMgd2VyZSByb2NrZWQgdG8gc2x1bWJlci4gT2gsIHdvZSBvbiB3b2UhIE9oLCBEZWF0aCwgd2h5IGNhbnN0IHRob3Ugbm90IHNvbWV0aW1lcyBiZSB0aW1lbHk/IEhhZHN0IHRob3UgdGFrZW4gdGhpcyBvbGQgYmxhY2tzbWl0aCB0byB0aHlzZWxmIGVyZSBoaXMgZnVsbCBydWluIGNhbWUgdXBvbiBoaW0sIHRoZW4gaGFkIHRoZSB5b3VuZyB3aWRvdyBoYWQgYSBkZWxpY2lvdXMgZ3JpZWYsIGFuZCBoZXIgb3JwaGFucyBhIHRydWx5IHZlbmVyYWJsZSwgbGVnZW5kYXJ5IHNpcmUgdG8gZHJlYW0gb2YgaW4gdGhlaXIgYWZ0ZXIgeWVhcnM7IGFuZCBhbGwgb2YgdGhlbSBhIGNhcmUta2lsbGluZyBjb21wZXRlbmN5LgogICAgICAgIDwvcD4KICAgICAgPC9kaXY+CiAgPC9ib2R5Pgo8L2h0bWw+'
    );
  });

  it('body with image/jpeg content-type', async () => {
    const res = await prismeaiFetch(
      {
        ...(tests.imgResponse as any),
      },
      console as any,
      ctx,
      broker
    );
    testResponse(
      {
        body: res,
        headers: 'ignore',
      },
      tests.imgResponse.response
    );
  });

  it('detailed_response with image/jpeg content-type', async () => {
    const res = await prismeaiFetch(
      {
        ...(tests.imgResponse as any),
        outputMode: 'detailed_response',
      },
      console as any,
      ctx,
      broker
    );
    testResponse({ ...res, body: 'ignore' }, tests.imgResponse.response);
  });
});

describe('Multipart', () => {
  const { broker, ctx } = getMocks();

  it('Upload from buffer', async () => {
    const res = await prismeaiFetch(
      {
        ...(tests.bufferUpload as any),
      },
      console as any,
      ctx,
      broker
    );
    testResponse({ body: res }, tests.bufferUpload.response);
  });

  it('Upload from base64', async () => {
    const res = await prismeaiFetch(
      {
        ...(tests.base64Upload as any),
      },
      console as any,
      ctx,
      broker
    );

    testResponse({ body: res }, tests.base64Upload.response);
  });

  it('File upload from text', async () => {
    const res = await prismeaiFetch(
      {
        ...(tests.textUpload as any),
      },
      console as any,
      ctx,
      broker
    );
    testResponse({ body: res }, tests.textUpload.response);
  });

  it('Upload with mimetype auto detected', async () => {
    const res = await prismeaiFetch(
      {
        ...(tests.uploadWithMimetypeDetection as any),
        multipart: tests.uploadWithMimetypeDetection.multipart?.map(
          ({ contentType, ...file }) => file
        ),
      },
      console as any,
      ctx,
      broker
    );
    testResponse({ body: res }, tests.uploadWithMimetypeDetection.response);
  });

  it('Upload with metadata fields', async () => {
    const res = await prismeaiFetch(
      {
        ...(tests.uploadWithMetadata as any),
      },
      console as any,
      ctx,
      broker
    );

    testResponse({ body: res }, tests.uploadWithMetadata.response);
  });
});

describe('SSE', () => {
  const { broker, ctx } = getMocks();

  it('Stream response', async () => {
    const res = await prismeaiFetch(
      {
        ...(tests.sseStreaming as any),
      },
      console as any,
      ctx,
      broker
    );
    testResponse(res, tests.sseStreaming.response);
    expect(res.body).toBeInstanceOf(ReadableStream);
    let chunks: any[] = [];
    for await (let chunk of res.body) {
      // Merge together chunks data as they could be differently split
      chunks = [...chunks, ...chunk?.chunk?.data];
    }
    expect(chunks).toMatchObject([
      {
        foo: 'yes',
      },
      {
        foo: 'one',
      },
      {
        foo: 'children call',
      },
      {
        foo: 'output',
      },
    ]);
  });
});

describe('Authentication', () => {
  const { broker, ctx } = getMocks();

  it('Prismeai ApiKey', async () => {
    const body = await prismeaiFetch(
      {
        ...(tests.basic as any),
        prismeaiApiKey: {
          name: 'myWorkspace',
        },
      },
      console as any,
      ctx,
      broker
    );
    expect(body.headers['x-prismeai-api-key']).toEqual(
      getApiKey(ctx.workspaceId)
    );
  });
});

describe('Error handling', () => {
  const { broker, ctx } = getMocks();

  it('Retries', async () => {
    const res = await prismeaiFetch(
      {
        ...(tests.autoRetries as any),
        query: {
          sessionId: ctx.session?.sessionId,
        },
        outputMode: 'detailed_response',
      },
      console as any,
      ctx,
      broker
    );
    testResponse(res, tests.autoRetries.response);
  });

  it('A socket error while streaming is properly handled', async () => {
    // Start our broken server
    const port = Math.floor(Math.random() * (4096 - 1024) + 1024);
    const url = new URL('http://localhost:' + port);

    const server = createServer(async (req, res) => {
      try {
        res.setHeader('content-type', 'text/event-stream');
        res.write('data: test1\n\n');
        await new Promise((resolve) => setTimeout(resolve, 200));
        res.write('data: test2\n\n');
        await new Promise((resolve) => setTimeout(resolve, 200));
        res.write('data: test3\n\n');
        await new Promise((resolve) => setTimeout(resolve, 200));
        res.write('data: test4\n\n');
        await new Promise((resolve) => setTimeout(resolve, 200));
        res.write('data: test5\n\n');
        await new Promise((resolve) => setTimeout(resolve, 200));
        res.end();
      } catch {}
    });
    servers.push(server);
    await server.listen(port);

    let chunks: any[] = [];
    let res2;
    try {
      const res = await prismeaiFetch(
        {
          url: url.toString(),
        },
        console as any,
        ctx,
        broker
      );
      for await (let chunk of res.body) {
        // Merge together chunks data as they could be differently split
        chunks = [...chunks, ...chunk?.chunk?.data];
      }
      expect(chunks).toMatchObject([
        'test1',
        'test2',
        'test3',
        'test4',
        'test5',
      ]);

      setTimeout(() => server.closeAllConnections(), 200);
      res2 = await prismeaiFetch(
        {
          url: url.toString(),
        },
        console as any,
        ctx,
        broker
      );
    } catch {}

    let error;
    for await (let chunk of res2.body) {
      if (chunk?.error) {
        error = chunk?.error;
      }
    }
    expect(error).toMatchObject({
      message: 'An error occured while reading stream',
    });
  });
});

afterAll(async () => {
  await servers.map((cur) => cur.close());
  mockHttpServer.close();
});
