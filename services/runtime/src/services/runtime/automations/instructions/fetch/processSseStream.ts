import { ReadableStream } from '../../../../../utils';
import { DebouncedFunc } from 'lodash';
import get from 'lodash/get';
import throttle from 'lodash/throttle';
import { RUNTIME_EMITS_BROKER_TOPIC } from '../../../../../../config';
import { StreamChunk, parseSseStream } from './parseSseStream';
import { parseAmazonEventStream } from './parseAmazonEventStream';
import { Broker } from '@prisme.ai/broker';
import { Logger } from '../../../../../logger';

export enum SseType {
  Standard = 'sse',
  Amazon = 'amazon-eventstream',
}

function parseStream(
  sseType: SseType,
  sse: NodeJS.ReadableStream,
  callback: (_: StreamChunk) => Promise<void>,
  broker: Broker
) {
  if (sseType == SseType.Amazon) {
    return parseAmazonEventStream(
      sse,
      async (chunk) => {
        callback({
          data: [chunk],
        });
      },
      broker
    );
  }
  return parseSseStream(sse, callback, broker);
}

export async function processSseStream(
  sseType: SseType,
  sse: NodeJS.ReadableStream,
  broker: Broker,
  logger: Logger,
  streamOpts: Prismeai.Fetch['fetch']['stream']
): Promise<ReadableStream<any> | undefined> {
  let outStream: ReadableStream<any> | undefined;
  if (!streamOpts?.event) {
    outStream = new ReadableStream(undefined, true);
  }

  let chunkIndex = 0;
  let concatenated: string | any[] | undefined;
  const send = (payload: any) => {
    if (outStream instanceof ReadableStream) {
      outStream.push(payload);
      return;
    }
    return broker.send(
      streamOpts?.event!,
      payload,
      {
        serviceTopic: RUNTIME_EMITS_BROKER_TOPIC,
      },
      { target: streamOpts?.target, options: streamOpts?.options }
    );
  };
  const sendChunk = streamOpts?.concatenate?.throttle
    ? throttle(send, streamOpts?.concatenate?.throttle)
    : send;

  const processingPromise = parseStream(
    sseType,
    sse,
    async (chunk: StreamChunk) => {
      try {
        if (streamOpts?.concatenate?.path) {
          chunk.data = chunk.data.filter((data) => {
            if (data?.keepAlive === true && Object.keys(data).length === 1) {
              return false; // Do not send internal keepAlive chunks inside data
            }

            const toConcatenate = get(data, streamOpts?.concatenate?.path!);
            if (toConcatenate) {
              if (!concatenated) {
                concatenated = toConcatenate;
              } else {
                concatenated = concatenated.concat(toConcatenate);
              }
            }

            return true;
          });
        }
        const fullChunk = {
          index: chunkIndex,
          chunk,
          concatenated: concatenated ? { value: concatenated } : undefined,
          additionalPayload: streamOpts?.payload,
        };
        await sendChunk(fullChunk);
        chunkIndex++;
      } catch (err) {
        logger.warn({ msg: `Could not stream fetch response chunk`, err });
      }
    },
    broker
  );

  const endStream = async (error?: any) => {
    if (streamOpts?.concatenate?.throttle) {
      // If the events were throttled we flush at the end in order to emit the last one right away.
      (sendChunk as DebouncedFunc<any>).flush();
    }

    const endChunk = !streamOpts?.endChunk
      ? undefined
      : {
          index: chunkIndex,
          chunk: { data: [streamOpts?.endChunk] },
          concatenated: concatenated ? { value: concatenated } : undefined,
          additionalPayload: streamOpts?.payload,
        };

    // Send our last chunks
    if (outStream) {
      if (error) {
        outStream.push({ error });
      }
      if (endChunk) {
        outStream.push(endChunk);
      }
    } else {
      if (error) {
        await sendChunk({ error });
      }
      if (endChunk) {
        await sendChunk(endChunk);
      }
    }

    if (outStream) {
      outStream.push(null);
    }
  };

  if (streamOpts?.event) {
    try {
      await processingPromise;
      await endStream();
    } catch (err) {
      logger.error({
        err: `An error occured while reading or closing fetch response stream`,

        details: err,
      });
    }
  } else if (outStream && outStream instanceof ReadableStream) {
    // Do not wait for promise so we can consume it while streaming
    processingPromise
      // This can crash because of a keepAlive race condition
      .catch((err) => {
        logger.error({
          err: `An error occured while reading fetch response stream`,
          details: `${err} - ${(err as any).cause}`,
          stack: err.stack,
        });
        return { err };
      })
      .then(async (res) => {
        try {
          await endStream(
            res?.err
              ? {
                  message: 'An error occured while reading stream',
                  reason: `${res?.err} - ${(res?.err as any).cause}`,
                }
              : undefined
          );
        } catch (err) {
          logger.error({
            err: `An error occured while closing fetch response stream`,
            details: err,
          });
        }
      });
  }

  return outStream;
}
