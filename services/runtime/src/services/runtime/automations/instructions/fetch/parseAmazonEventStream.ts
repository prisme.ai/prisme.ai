import { logger } from '@azure/storage-blob';
import { EventType } from '../../../../../eda';
import { Broker } from '@prisme.ai/broker';
import { EventStreamCodec } from '@smithy/eventstream-codec';

type StreamChunk = any;

export async function parseAmazonEventStream(
  eventStream: NodeJS.ReadableStream,
  callback: (_: StreamChunk) => Promise<void>,
  broker: Broker
) {
  const encoder = new TextEncoder();
  const decoder = new TextDecoder();
  const codec = new EventStreamCodec(
    (input) => input.toString(),
    encoder.encode.bind(encoder)
  );
  let buffer = Buffer.alloc(0);

  async function sendNextMessage() {
    // Avoid "Attempt to access memory outside buffer bounds" when we finished reading buffer
    if (buffer.length < 4) {
      return false;
    }
    const length = buffer.readUInt32BE(0); // Current chunk size
    if (buffer.length < length) {
      return false; // Incomplete chunk
    }

    codec.feed(buffer.slice(0, length));
    buffer = buffer.slice(length);

    const message = codec.getMessage().getMessage();
    if (!message) {
      return false;
    }
    const decoded = decoder.decode(message.body);
    try {
      await callback(JSON.parse(decoded) as StreamChunk);
    } catch {
      await callback(decoded);
    }
    return true;
  }

  function errHandler(err: any) {
    broker
      .send(EventType.Error, {
        error: 'FailedChunkParsing',
        message: 'Could not parse AWS EventStream chunk',
        details: `${err}`,
      })
      .catch((err: any) => logger.error({ err }));
  }

  for await (const chunk of eventStream) {
    buffer = Buffer.concat([buffer, chunk as Buffer]);
    try {
      await sendNextMessage();
    } catch (err) {
      errHandler(err);
    }
  }
  // Flush remaining messages
  try {
    while (await sendNextMessage());
  } catch (err) {
    errHandler(err);
  }
}
