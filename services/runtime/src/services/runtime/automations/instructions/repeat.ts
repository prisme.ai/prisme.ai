import { Broker } from '@prisme.ai/broker';
import { Logger } from 'pino';
import { runInstructions } from '..';
import { Cache } from '../../../../cache';
import { Workspace } from '../../../workspaces';
import { ContextsManager } from '../../contexts';
import { Readable } from 'stream';
import { throttler } from '../../../rateLimits/throttler';
import { Break } from './types';
import { EventType } from '../../../../eda';
import { REPEAT_MAX_BATCH_SIZE } from '../../../../../config/rateLimits';
import { InvalidArgumentsError, PrismeError } from '../../../../errors';

type RepeatOn = Extract<Prismeai.Repeat['repeat'], { on: string }>;
type RepeatUntil = Extract<Prismeai.Repeat['repeat'], { until: number }>;

const isRepeatOn = (value: Prismeai.Repeat['repeat']): value is RepeatOn => {
  return !!(value as RepeatOn).on;
};
const isRepeatUntil = (
  value: Prismeai.Repeat['repeat']
): value is RepeatUntil => {
  return !!(value as RepeatUntil).until;
};

export const REPEAT_ITEM_VAR_NAME = 'item';
export const REPEAT_INDEX_VAR_NAME = '$index';
export async function repeat(
  value: Prismeai.Repeat['repeat'],
  {
    workspace,
    logger,
    broker,
    ctx,
    cache,
  }: {
    workspace: Workspace;
    logger: Logger;
    broker: Broker;
    ctx: ContextsManager;
    cache: Cache;
  }
) {
  const until = isRepeatUntil(value) ? value.until : undefined;
  const on = isRepeatOn(value) ? value.on : undefined;
  const { do: doInstructions } = value;

  // Streams reading
  if (<any>on instanceof Readable) {
    const stream = on as any as Readable;
    try {
      for await (let buffer of stream) {
        try {
          ctx.set(REPEAT_ITEM_VAR_NAME, buffer);
          await runInstructions(doInstructions, {
            workspace,
            ctx,
            logger,
            broker,
            cache,
          });
        } catch (err) {
          broker
            .send(EventType.Error, err as any)
            .catch((err) => logger.error({ err }));
        }
      }
    } catch (err) {
      throw new PrismeError(
        `Stream abruptly closed while reading from a repeat instruction`,
        {
          err,
        }
      );
    }
    return;
  }

  const values =
    typeof on === 'object' && !Array.isArray(on)
      ? Object.entries(on).map(([key, value]) => ({ key, value }))
      : on || [];

  const maxIterations =
    typeof until !== 'undefined' &&
    (!on || until < ((<any>values)?.length || 0))
      ? until
      : (<any>values)?.length || 0;

  const executeIteration = async (
    index: number,
    cloneContext: boolean = false
  ): Promise<'break' | true> => {
    // Check throttle every 500 iterations
    const throttleBy = 500;
    if (index > 0 && index % throttleBy === 0) {
      const throttleIterations = Math.min(throttleBy, maxIterations - index);
      await throttler
        .workspace(ctx.workspaceId)
        .repeat(ctx, throttleIterations);
    }

    const item = values?.length ? values[index] : index;
    const subCtx = cloneContext
      ? ctx.child(
          // Share existing local variables to allow setting outside variable, but prevent new variables from conflicting with each others
          {},
          {
            keepLocalContext: true,
          }
        )
      : ctx;
    subCtx.set(REPEAT_ITEM_VAR_NAME, item);
    subCtx.set(REPEAT_INDEX_VAR_NAME, index);
    try {
      await runInstructions(doInstructions, {
        workspace,
        ctx: subCtx,
        logger,
        broker,
        cache,
      });
    } catch (err) {
      if (err instanceof Break && err.scope === 'repeat') {
        return 'break';
      } else {
        throw err;
      }
    }
    return true;
  };

  // Batch execution
  if (value?.batch?.size) {
    if (value?.batch?.size > REPEAT_MAX_BATCH_SIZE) {
      throw new InvalidArgumentsError(
        `Repeat batch size must be lower than ${REPEAT_MAX_BATCH_SIZE}`
      );
    }
    let i = 0;
    while (i < maxIterations) {
      const batchSize =
        i + value?.batch?.size < maxIterations + 1
          ? value?.batch?.size
          : maxIterations - i;
      let shouldBreak = false;
      const promises = new Array(batchSize).fill(null).map((_, idx) => {
        return executeIteration(i + idx, true).then((res) => {
          if (res === 'break') {
            shouldBreak = true;
          }
        });
      });
      i += batchSize;

      await Promise.all(promises);

      if (shouldBreak) {
        return;
      }

      if (value?.batch?.interval) {
        await new Promise((resolve) =>
          setTimeout(resolve, value?.batch?.interval)
        );
      }
    }

    return;
  }

  // Sequential execution
  for (let i = 0; i < maxIterations; i++) {
    const res = await executeIteration(i);
    if (res === 'break') {
      break;
    }
  }

  ctx.delete(REPEAT_ITEM_VAR_NAME);
}
