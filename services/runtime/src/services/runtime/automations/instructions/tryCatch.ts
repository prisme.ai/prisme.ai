import { Broker } from '@prisme.ai/broker';
import { runInstructions } from '..';
import { Logger } from '../../../../logger';
import { Workspace } from '../../../workspaces';
import { ContextsManager } from '../../contexts';
import { Cache } from '../../../../cache';

export const CATCH_ERROR_VAR_NAME = '$error';

export async function tryCatch(
  all: Prismeai.Try['try'],
  {
    workspace,
    logger,
    broker,
    ctx,
    cache,
  }: {
    workspace: Workspace;
    logger: Logger;
    broker: Broker;
    ctx: ContextsManager;
    cache: Cache;
  }
) {
  try {
    await runInstructions(all.do, {
      workspace,
      ctx,
      logger,
      broker,
      cache,
    });
  } catch (err) {
    const error = err as any;
    const errorObj = error?.toJSON
      ? error.toJSON()
      : {
          error: error.code || error.error,
          message: error.message,
          details: error.details,
          source: error.source,
          break: (error as Prismeai.Break['break'])?.payload,
        };
    await ctx.set(CATCH_ERROR_VAR_NAME, errorObj);
    if (all.catch) {
      await runInstructions(all.catch, {
        workspace,
        ctx,
        logger,
        broker,
        cache,
      });
    }
  }
}
