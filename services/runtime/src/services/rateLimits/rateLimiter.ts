import {
  RateLimiterRedis,
  RateLimiterRes,
  RateLimiterAbstract,
} from 'rate-limiter-flexible';
import { RATE_LIMITS_STORAGE } from '../../../config/rateLimits';
import { buildRedis } from '../../cache/redis';
import { logger } from '../../logger';
import { InvalidArgumentsError, TooManyRequests } from '../../errors';
import { RateLimitOpts, RateLimitResponse } from './types';

let redisClient: ReturnType<typeof buildRedis> | undefined;
try {
  if (process.env.RATE_LIMITS_STORAGE_HOST !== 'deactivated') {
    redisClient = buildRedis('rateLimiter', RATE_LIMITS_STORAGE);
    redisClient.connect().catch((err) =>
      logger.error({
        msg: `Failed RateLimiter initialization while connecting to redis instance`,
        err,
      })
    );
  }
} catch (err) {
  logger.error({
    msg: `Could not initialize RateLimiter redis instance`,
    err,
  });
  if (redisClient) {
    redisClient.disconnect();
  }
}
class RateLimiter {
  private rateLimiter: RateLimiterAbstract;
  public name: string;
  public limit: number;
  public window: number;

  constructor(
    workspaceId: string,
    name: string,
    limit: number,
    window: number
  ) {
    if (!workspaceId || !name || !limit || !window) {
      throw new InvalidArgumentsError(
        `Invalid rate limit instruction, missing one of name, limit or window`,
        {
          workspaceId,
          name,
          limit,
          window,
        }
      );
    }
    const key = `runtime:ratelimiter:${workspaceId}:${name || 'default'}`;
    this.rateLimiter = new RateLimiterRedis({
      storeClient: redisClient,
      keyPrefix: key,
      points: limit,
      duration: window,
      useRedisPackage: true,
    });

    this.limit = limit;
    this.window = window;
    this.name = name;
  }

  async consume(
    consumer: string,
    points: number,
    throwOnErrors: boolean
  ): Promise<RateLimitResponse> {
    if (!consumer) {
      throw new InvalidArgumentsError(
        'Missing consumer argument for rateLimit'
      );
    }
    try {
      const limits = await this.rateLimiter.consume(consumer, points);

      return {
        name: this.name,
        ok: true,
        retryAfter: limits.msBeforeNext / 1000,
        limit: this.limit,
        window: this.window,
        remaining: limits.remainingPoints,
        consumer,
        requested: points,
      };
    } catch (err) {
      const limits = err as RateLimiterRes;
      const res = {
        name: this.name,
        ok: false,
        retryAfter: limits.msBeforeNext / 1000,
        limit: this.limit,
        window: this.window,
        remaining: limits.remainingPoints,
        consumer,
        requested: points,
      };
      if (!throwOnErrors) {
        return res;
      }
      throw new TooManyRequests(this.name, res);
    }
  }
}
class WorkspacesRateLimiter {
  // Map workspace ids to operation names to their corresponding RateLimiter
  private workspaces: Record<string, Record<string, RateLimiter>>;

  constructor() {
    this.workspaces = {};
  }

  public async rateLimit(workspaceId: string, opts: RateLimitOpts) {
    if (!this.workspaces?.[workspaceId]) {
      this.workspaces[workspaceId] = {};
    }
    if (
      !this.workspaces?.[workspaceId]?.[opts.name] ||
      this.workspaces[workspaceId][opts.name].limit !== opts.limit ||
      this.workspaces[workspaceId][opts.name].window !== opts.window
    ) {
      this.workspaces[workspaceId][opts.name] = new RateLimiter(
        workspaceId,
        opts.name,
        opts.limit,
        opts.window
      );
    }

    return await this.workspaces[workspaceId][opts.name].consume(
      opts.consumer,
      opts.consume || 1,
      opts.break ?? true
    );
  }
}

const rateLimiter = new WorkspacesRateLimiter();
export { rateLimiter };
