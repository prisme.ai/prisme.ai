export interface ThrottleOpts {
  burstRate: number; // Only work with native instruction throttling and not custom rate limits
  rate: number;
}

export enum ThrottleType {
  Automations = 'automations',
  Emits = 'emits',
  Fetchs = 'fetchs',
  Repeats = 'repeats',
}

export type RateLimitOpts = Omit<Prismeai.RateLimit['rateLimit'], 'output'>;

export type RateLimitResponse = {
  name: string;
  retryAfter: number;
  consumer: string;
  limit: number;
  remaining: number;
  window: number;
  ok: boolean;
  requested: number;
};
