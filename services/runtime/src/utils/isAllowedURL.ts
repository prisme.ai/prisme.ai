import { FETCH_FORBIDDEN_HOSTS } from '../../config';

export function isAllowedURL(url: URL) {
  // Allow data_url fetching as it is a convenient way of extracting mimetype, filename & decoded base64 from a data_url
  if (url.protocol === 'data:' && !url.hostname) {
    return true;
  }

  const hostname = url.hostname;
  return !FETCH_FORBIDDEN_HOSTS.some((cur) => {
    if (cur == hostname) {
      return true;
    }
    if (cur.endsWith('*') && hostname.startsWith(cur.slice(0, -1))) {
      return true;
    }
    if (cur.startsWith('*') && hostname.endsWith(cur.slice(1))) {
      return true;
    }
    return false;
  });
}
