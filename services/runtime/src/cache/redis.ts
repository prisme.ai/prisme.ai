import { createClient } from '@redis/client';
import { RedisClientType } from '@redis/client/dist/lib/client';
import { SetOptions } from '.';
import { APP_NAME } from '../../config';
import { CacheDriver, CacheOptions } from './types';

export function buildRedis(name: string, opts: CacheOptions) {
  const { host, password, ...otherOpts } = opts;
  const client = createClient({
    url: opts.host,
    password: opts.password,
    name: `${APP_NAME}-${name}`,
    ...otherOpts,
  });

  client.on('error', (err: Error) => {
    console.error(`Error occured with runtime ${name} redis driver : ${err}`);
  });
  client.on('connect', () => {
    console.info(`runtime ${name} redis connected.`);
  });
  client.on('reconnecting', () => {
    console.info(`runtime ${name} redis reconnecting ...`);
  });
  client.on('ready', () => {
    console.info(`runtime ${name} redis is ready.`);
  });
  return client as RedisClientType;
}

export default class RedisCache implements CacheDriver {
  private client: RedisClientType;

  constructor(opts: CacheOptions) {
    this.client = buildRedis('cache', opts);
    this.client.on('error', (err: Error) => {
      console.error(`Error occured with cache redis driver : ${err}`);
    });
  }

  async connect() {
    await this.client.connect();
    return true;
  }

  async get(key: string) {
    return await this.client.get(key);
  }

  async set(key: string, value: any, { ttl }: SetOptions = {}) {
    return await this.client.set(key, value, {
      EX: ttl,
    });
  }

  async getObject<T = object>(key: string): Promise<T | undefined> {
    const raw = await this.get(key);
    if (!raw) {
      return undefined;
    }
    return JSON.parse(raw as string);
  }

  async setObject(key: string, object: object, opts?: SetOptions) {
    const raw = JSON.stringify(object || {});
    return await this.set(key, raw, opts);
  }

  async addToSet(key: string, value: any | any[]) {
    return await this.client.sAdd(key, value);
  }

  async isInSet(key: string, value: any): Promise<boolean> {
    return await this.client.sIsMember(key, value);
  }

  async listSet(key: string) {
    return await this.client.sMembers(key);
  }
}
