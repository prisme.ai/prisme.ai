module.exports = () => {
  process.env.TZ = 'UTC';
  //Disable rate limiter redis (fixes jest hangs because of open handles)
  process.env.RATE_LIMITS_STORAGE_HOST = 'deactivated';
  // Shorten workspace deletion event procesing delay (fixes jest hangs because of open handles)
  process.env.WORKSPACE_DELETION_DELAY = '100';
  // ALlow localhost fetches
  process.env.FETCH_FORBIDDEN_HOSTS = 'none';

  return {
    testEnvironment: 'node',
    coveragePathIgnorePatterns: [
      // Runtime
      '<rootDir>/src/eda',
      '<rootDir>/src/cache',
      '<rootDir>/src/storage',
    ],
    transformIgnorePatterns: ['node_modules/(?!(mime)/)'],
    testTimeout: 500,
  };
};
