import { CacheOptions, CacheType } from '../src/cache';
import { ThrottleOpts } from '../src/services/rateLimits/types';
import { CONTEXTS_CACHE } from './cache';

/**
 * Native instructions throttling
 */

export const RATE_LIMIT_AUTOMATIONS: ThrottleOpts = {
  rate: parseInt(process.env.RATE_LIMIT_AUTOMATIONS || '100'),
  burstRate: parseInt(process.env.RATE_LIMIT_AUTOMATIONS_BURST || '400'),
};

export const RATE_LIMIT_EMITS: ThrottleOpts = {
  rate: parseInt(process.env.RATE_LIMIT_EMITS || '30'),
  burstRate: parseInt(process.env.RATE_LIMIT_EMITS_BURST || '100'),
};

export const RATE_LIMIT_FETCHS: ThrottleOpts = {
  rate: parseInt(process.env.RATE_LIMIT_FETCHS || '50'),
  burstRate: parseInt(process.env.RATE_LIMIT_FETCHS_BURST || '200'),
};

export const RATE_LIMIT_REPEATS: ThrottleOpts = {
  rate: parseInt(process.env.RATE_LIMIT_REPEATS || '1000'),
  burstRate: parseInt(process.env.RATE_LIMIT_REPEATS_BURST || '4000'),
};

export const RATE_LIMIT_DISABLED = ['true', 'yes', 'y', '1'].includes(
  (process.env.RATE_LIMIT_DISABLED || 'false').toLowerCase()
);

/**
 * Custom instructions rate limits
 */

export const RATE_LIMITS_STORAGE: CacheOptions = process.env
  .RATE_LIMITS_STORAGE_HOST
  ? {
      type: CacheType.Redis,
      host: process.env.RATE_LIMITS_STORAGE_HOST,
      password: process.env.RATE_LIMITS_STORAGE_PASSWORD,
    }
  : CONTEXTS_CACHE;

/**
 * Repeats
 */

export const REPEAT_MAX_BATCH_SIZE = parseInt(
  process.env.REPEAT_MAX_BATCH_SIZE || '50'
);
