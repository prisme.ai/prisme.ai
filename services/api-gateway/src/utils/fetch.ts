import nodeFetch, { RequestInit } from 'node-fetch';
process.env.GLOBAL_AGENT_ENVIRONMENT_VARIABLE_NAMESPACE = '';
import 'global-agent/bootstrap';

export default async function fetch(url: string, options?: RequestInit) {
  return await nodeFetch(url, options);
}
