import fetch from '../../utils/fetch';
import { GatewayConfig, syscfg } from '../../config';
import { JWK } from './types';
import { JWKStore } from './store';
import { URL } from 'url';
import { logger } from '../../logger';

export async function publishJWKToRuntime(
  gtwcfg: GatewayConfig,
  jwks: JWKStore,
  retries = 3
): Promise<boolean> {
  const key = jwks.store?.keys?.[0];
  if (!key) {
    return false;
  }
  const url = gtwcfg.config.services.runtime.url;
  try {
    const res = await publishInternalPrivateJWK(
      new URL('/sys/certs', url).toString(),
      key
    );
    logger.info({
      msg: `Sucessfully updated ${url} with new signing JWK`,
      res,
    });
  } catch (err) {
    if (retries > 0) {
      const retryIn = 5000 * (3 - retries);
      logger.error({
        msg: `Failed updating ${url} with new JWK. Retrying in ${retryIn} seconds`,
      });
      await new Promise((resolve) => setTimeout(resolve, retryIn));
      return await publishJWKToRuntime(gtwcfg, jwks, retries - 1);
    }
    logger.error({
      msg: `Failed updating ${url} with new signing JWK`,
      err,
    });
    return false;
  }

  return true;
}

async function publishInternalPrivateJWK(url: string, jwk: JWK) {
  const res = await fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      [syscfg.API_KEY_HEADER]: syscfg.INTERNAL_API_KEY,
    },
    body: JSON.stringify({
      jwk,
    }),
  });
  const json = await res.json();
  if (res.status >= 400 && res.status < 600) {
    throw json;
  }
  return json;
}
