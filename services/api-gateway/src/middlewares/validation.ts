import { NextFunction, Request, Response } from 'express';
import * as OpenApiValidator from 'express-openapi-validator';
import { HttpError } from 'express-openapi-validator/dist/framework/types';
import { syscfg } from '../config';
import {
  ConfigurationError,
  NotFoundError,
  RequestValidationError,
} from '../types/errors';
import yaml from 'js-yaml';
import fs from 'fs';

interface ValidationOpts {
  ignorePaths?: string[];
}
export const validationMiddleware = ({ ignorePaths = [] }: ValidationOpts) => {
  const ignoreRegexps = ignorePaths.map((regexp) => new RegExp(regexp));

  // Load the yml file ourselves as express-openapi-validator loader does not support yaml overrides (<<:)
  let apiSpec: any;
  try {
    apiSpec = yaml.load(fs.readFileSync(syscfg.OPENAPI_FILEPATH).toString());
  } catch (err) {
    throw new ConfigurationError(
      `Could not load OpenAPI definition file at '${syscfg.OPENAPI_FILEPATH}'`,
      { err }
    );
  }
  return OpenApiValidator.middleware({
    apiSpec,
    validateRequests: true,
    validateResponses: false,
    validateSecurity: false,
    validateApiSpec: false,
    ignorePaths: !ignorePaths.length
      ? undefined
      : (path: string) => {
          return ignoreRegexps.some((cur) => cur.test(path));
        },
  });
};

export const validationErrorMiddleware = (
  err: Error,
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (err instanceof HttpError) {
    if (err.message === 'not found') {
      next(new NotFoundError());
    } else {
      next(new RequestValidationError(err.message, err.errors));
    }
  } else {
    next(err);
  }
};

export async function cleanIncomingRequest(req: Request) {
  for (let header in req.headers) {
    if (
      header.startsWith('x-prismeai-') &&
      !syscfg.ALLOWED_PRISMEAI_HEADERS_FROM_OUTSIDE.includes(header)
    ) {
      delete req.headers[header];
    }
  }
}
