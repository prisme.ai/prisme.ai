import crypto from 'crypto';
import yaml from 'js-yaml';
import { logger } from '../logger';

export function generateChecksum(
  buffer: Buffer | string | object,
  algorithm?: string
) {
  try {
    let str = serialize(buffer);
    const checksum = crypto
      .createHash(algorithm || 'md5')
      .update(str, 'utf8')
      .digest('hex');
    return checksum;
  } catch (err) {
    logger.error({
      msg: 'Error raised while generating checksum',
      err,
    });
    return 'error';
  }
}

function serialize(obj: any): string {
  if (obj instanceof Buffer) {
    return obj.toString('utf8');
  } else if (typeof obj === 'object') {
    obj = yaml.dump(obj);
  }

  return obj;
}
