import { parse } from 'path';
import { DSULRootFiles, DSULType, FolderIndex } from '../services/DSULStorage';

export type DsulLocator = {
  type: DSULType | false; // false for unkown
  basepath?: string; // parent directory path
  dsulpath?: string; // dsul filepath from basepath
  slug?: string;
  folderIndex?: boolean;
};
export function parseDsulLocator(filepath: string): DsulLocator {
  // Detect parent directory from the very beginning so we don't mess up with slugs named like parent dirs
  let directoryStartIdx: number;
  let type: DSULType | undefined;
  if (
    (directoryStartIdx = filepath.indexOf(DSULType.Automations + '/')) !== -1
  ) {
    type = DSULType.Automations;
  } else if (
    (directoryStartIdx = filepath.indexOf(DSULType.Pages + '/')) !== -1
  ) {
    type = DSULType.Pages;
  } else if (
    (directoryStartIdx = filepath.indexOf(DSULType.Imports + '/')) !== -1
  ) {
    type = DSULType.Imports;
  }

  if (type && directoryStartIdx !== -1) {
    const locator = {
      type,
      basepath: filepath.slice(0, directoryStartIdx), // parent path before our automations/pages/imports directory
      dsulpath: filepath.slice(directoryStartIdx), // begins with automations/...
    };
    // Dont check filepath.endsWith as it would match sub automations named __index__
    const filename = locator.dsulpath.slice(type.length + 1); // + 1 for separator /
    if (filename.startsWith(FolderIndex)) {
      return { ...locator, folderIndex: true };
    }
    return {
      ...locator,
      slug: filename.lastIndexOf('.')
        ? filename.slice(0, filename.lastIndexOf('.'))
        : filename,
    };
  }

  const parsed = parse(filepath);
  // Root files
  if (DSULRootFiles.includes(parsed.name as DSULType)) {
    return {
      type: parsed.name as DSULType,
      basepath: parsed.dir,
      dsulpath: parsed.name + parsed.ext,
    };
  }

  return {
    type: false,
  };
}
