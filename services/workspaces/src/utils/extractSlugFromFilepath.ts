export function extractSlugFromFilepath(filepath: string) {
  return filepath.lastIndexOf('.')
    ? filepath.slice(0, filepath.lastIndexOf('.'))
    : filepath;
}
