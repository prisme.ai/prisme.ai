import { DSULType } from '../services/DSULStorage';
import { parseDsulLocator } from './parseDsulLocator';

describe('Detect root files', () => {
  it('Valid paths', () => {
    expect(parseDsulLocator('./test/current/index.yml')).toEqual({
      type: DSULType.DSULIndex,
      basepath: './test/current',
      dsulpath: 'index.yml',
    });
    expect(parseDsulLocator('./test/current/index.yaml')).toEqual({
      type: DSULType.DSULIndex,
      basepath: './test/current',
      dsulpath: 'index.yaml',
    });

    expect(parseDsulLocator('./.import.yml')).toEqual({
      type: DSULType.ImportConfig,
      basepath: '.',
      dsulpath: '.import.yml',
    });

    expect(parseDsulLocator('runtime.yaml')).toEqual({
      type: DSULType.RuntimeModel,
      basepath: '',
      dsulpath: 'runtime.yaml',
    });

    expect(parseDsulLocator('sub/folder/security.yml')).toEqual({
      type: DSULType.Security,
      basepath: 'sub/folder',
      dsulpath: 'security.yml',
    });
  });

  it('Unknown paths', () => {
    expect(parseDsulLocator('./test/current/someUnknownFile.yml')).toEqual({
      type: false,
    });
  });
});

describe('Detect automations/pages/imports files', () => {
  it('Basic paths', () => {
    expect(parseDsulLocator('./test/automations/index.yml')).toEqual({
      type: DSULType.Automations,
      slug: 'index',
      basepath: './test/',
      dsulpath: 'automations/index.yml',
    });

    // Can be named like root files
    expect(parseDsulLocator('pages/security.yml')).toEqual({
      type: DSULType.Pages,
      slug: 'security',
      basepath: '',
      dsulpath: 'pages/security.yml',
    });

    expect(parseDsulLocator('imports/security.yml')).toEqual({
      type: DSULType.Imports,
      slug: 'security',
      basepath: '',
      dsulpath: 'imports/security.yml',
    });
  });

  it('Nested paths', () => {
    expect(parseDsulLocator('./myarchive/imports/sub/index.yml')).toEqual({
      type: DSULType.Imports,
      slug: 'sub/index',
      basepath: './myarchive/',
      dsulpath: 'imports/sub/index.yml',
    });

    // Can be named like root files
    expect(parseDsulLocator('pages/nested/security.yml')).toEqual({
      type: DSULType.Pages,
      slug: 'nested/security',
      basepath: '',
      dsulpath: 'pages/nested/security.yml',
    });

    // Nested __index__ are not mistaken for folder indexes
    expect(parseDsulLocator('automations/nested/__index__.yml')).toEqual({
      type: DSULType.Automations,
      slug: 'nested/__index__',
      basepath: '',
      dsulpath: 'automations/nested/__index__.yml',
    });
  });
});

describe('Detect automations/pages/imports folder indexes', () => {
  it('Basic paths', () => {
    expect(parseDsulLocator('./test/automations/__index__.yml')).toEqual({
      type: DSULType.Automations,
      basepath: './test/',
      dsulpath: 'automations/__index__.yml',
      folderIndex: true,
    });

    expect(parseDsulLocator('pages/__index__.yml')).toEqual({
      type: DSULType.Pages,
      basepath: '',
      dsulpath: 'pages/__index__.yml',
      folderIndex: true,
    });

    expect(parseDsulLocator('imports/__index__.yml')).toEqual({
      type: DSULType.Imports,
      basepath: '',
      dsulpath: 'imports/__index__.yml',
      folderIndex: true,
    });
  });
});
