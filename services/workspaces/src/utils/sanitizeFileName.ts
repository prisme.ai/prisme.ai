import { TextDecoder, TextEncoder } from 'util';

export function sanitizeFileName(fileName: string = '') {
  // Limiter la longueur du nom de fichier à 1024 octets en UTF-8
  let utf8Bytes = new TextEncoder().encode(fileName);
  if (utf8Bytes.length > 1024) {
    fileName = new TextDecoder().decode(utf8Bytes.slice(0, 1024));
  }
  fileName = decodeURIComponent(fileName)
    .normalize('NFD')
    .replace(/[\u0300-\u036f]/g, '');
  // On définit les caractères que l'on souhaite garder
  const unsafeChars = /[^0-9a-zA-Z!\-_.\*'()]/g;
  // Remplacer les caractères non autorisés par un underscore '_'
  let sanitized = fileName.trim().replace(unsafeChars, '_');

  return sanitized;
}
