import { extractPageEvents } from './extractEvents';

it('should extract page emit events', () => {
  expect(extractPageEvents({})).toEqual({
    listen: [],
    emit: [],
  });

  expect(
    extractPageEvents({
      updateOn: 'listen A',
      onInit: 'emit A',
      blocks: [
        {
          slug: 'RichText',
          updateOn: 'listen B',
          onInit: 'emit B',
        },
        {
          slug: 'BlocksList',
          updateOn: 'listen C',
          onInit: 'emit C',
          blocks: [
            {
              slug: 'RichText',
              updateOn: 'listen D',
              onInit: 'emit D',
            },
          ],
        },
      ],
    })
  ).toEqual({
    listen: ['listen A', 'listen B', 'listen C', 'listen D'],
    emit: ['emit A', 'emit B', 'emit C', 'emit D'],
  });

  expect(
    extractPageEvents({
      blocks: [
        {
          slug: 'Form',
          onSubmit: 'emit A',
        },
        {
          slug: 'Custom',
          onDoingSomething: 'emit B',
          doSomethingOn: 'listen B',
        },
      ],
    })
  ).toEqual({
    listen: ['listen B'],
    emit: ['emit A', 'emit B'],
  });
});
