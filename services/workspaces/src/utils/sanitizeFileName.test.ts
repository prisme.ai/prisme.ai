import { sanitizeFileName } from './sanitizeFileName';

describe('Should handle basic names', () => {
  it('should handle parenthesis', () => {
    expect(sanitizeFileName('my_document.pdf')).toEqual('my_document.pdf');
    expect(sanitizeFileName('photos & videos @2021!.zip')).toEqual(
      'photos___videos__2021!.zip'
    );
    expect(sanitizeFileName('résumé(John Doe).docx')).toEqual(
      'resume(John_Doe).docx'
    );
    expect(sanitizeFileName('projet#1 final^version.tar.gz')).toEqual(
      'projet_1_final_version.tar.gz'
    );
    expect(sanitizeFileName('données<>analyse|rapport.csv')).toEqual(
      'donnees__analyse_rapport.csv'
    );
    expect(sanitizeFileName('   espaces    dans le nom.txt   ')).toEqual(
      'espaces____dans_le_nom.txt'
    );
    expect(sanitizeFileName('confuence éé.png')).toEqual('confuence_ee.png');
    expect(sanitizeFileName(encodeURIComponent('confuence éé.png'))).toEqual(
      'confuence_ee.png'
    );
    expect(sanitizeFileName(encodeURIComponent('confuenceéé.png'))).toEqual(
      'confuenceee.png'
    );
    expect(sanitizeFileName('confuence%C3%A9%C3%A9.png')).toEqual(
      'confuenceee.png'
    );
    expect(sanitizeFileName('confluencee%CC%81e%CC%81.png')).toEqual(
      'confluenceee.png'
    );
  });
});
