import { generateChecksum } from '../../utils/generateChecksum';
import { DSULStorage, DSULType, ImportConfig } from '../DSULStorage';

export type WorkspaceStatus = PrismeaiAPI.GetWorkspaceStatus.Responses.$200;

type Automations = WorkspaceStatus['automations'];
type Imports = WorkspaceStatus['imports'];
type Pages = WorkspaceStatus['pages'];
type Changes = WorkspaceStatus['changes'];
type VersionReference = WorkspaceStatus['version'];
type WorkspaceEntries = {
  automations: Automations;
  imports: Imports;
  pages: Pages;
};
type WorkspaceIndex = {
  automations: Record<string, Prismeai.AutomationMeta>;
  imports: Record<string, Prismeai.AppInstanceMeta>;
  pages?: Record<string, Prismeai.PageMeta>;
};
type Checksums = Partial<{
  [k in keyof WorkspaceEntries]: Record<string, string>;
}>;

export class WorkspaceIntrospector {
  private storage: DSULStorage;

  constructor(storage: DSULStorage) {
    this.storage = storage;
  }

  async status(workspaceId: string): Promise<WorkspaceStatus> {
    const entries = await this.listWorkspaceAutomationsImports(workspaceId);
    const result: WorkspaceStatus = {
      version: {},
      errors: [],
      changes: [],
      ...entries,
    };

    let importConfig: ImportConfig | undefined;
    try {
      importConfig = await this.storage.get({
        workspaceId,
        dsulType: DSULType.ImportConfig,
      });
    } catch (err) {
      result.errors.push({
        msg: `No import config found. Ignore this warning if no import/export have ever been made on this workspace`,
        err,
      });
    }

    if (importConfig?.version) {
      result.version = importConfig.version;
    }

    const [automationsIndex, importsIndex, pagesIndex] = await Promise.all([
      this.storage.folderIndex({
        workspaceId,
        dsulType: DSULType.AutomationsIndex,
      }),
      this.storage.folderIndex({
        workspaceId,
        dsulType: DSULType.ImportsIndex,
      }),
      this.storage.folderIndex({
        workspaceId,
        dsulType: DSULType.PagesIndex,
      }),
    ]);

    const { errors, changes } = await this.checkRuntimeModelIntegrity(
      workspaceId,
      entries,
      importConfig?.checksums || {},
      {
        automations: Object.entries(automationsIndex || {}).reduce(
          (index, [slug, autom]) => ({
            ...index,
            [slug]: autom.checksum,
          }),
          {}
        ),
        imports: Object.entries(importsIndex || {}).reduce(
          (index, [slug, cur]) => ({
            ...index,
            [slug]: cur.checksum,
          }),
          {}
        ),
      }
    );
    result.errors.push(...errors);
    result.changes!.push(...changes);

    // Detect suspect diffs since last import (diffs although nobody changed them)
    if (result.changes?.length) {
      const diffErors = await this.detectAbnormalChanges(
        result.changes,
        {
          automations: automationsIndex || {},
          imports: importsIndex || {},
          pages: pagesIndex || {},
        },
        result.version
      );
      result.errors.push(...diffErors);
    }

    // Detect page changes
    if (pagesIndex) {
      Object.entries(importConfig?.checksums?.pages || {}).forEach(
        ([slug, expectedChecksum]) => {
          const currentChecksum = pagesIndex?.[slug]?.checksum;
          if (!currentChecksum || expectedChecksum !== currentChecksum) {
            result.changes!.push({
              type: DSULType.Pages,
              slug,
              currentChecksum,
              expectedChecksum,
              updatedAt: pagesIndex?.[slug]?.updatedAt,
              updatedBy: pagesIndex?.[slug]?.updatedBy,
            });
          }
        }
      );
    }

    return result;
  }

  detectAbnormalChanges(
    changes: Changes = [],
    index: WorkspaceIndex,
    version: VersionReference
  ) {
    const errors: object[] = [];
    changes.forEach((cur, idx) => {
      if (!cur.slug) {
        return;
      }
      const correspondingIndex =
        cur.type === 'automations' ? index.automations : index.imports;
      if (correspondingIndex && correspondingIndex?.[cur.slug]) {
        changes[idx].updatedAt = correspondingIndex[cur.slug].updatedAt;
        changes[idx].updatedBy = correspondingIndex[cur.slug].updatedBy;
      } else {
        errors.push({
          msg: `Detected changes on ${cur.type} ${cur.slug} although ${cur.slug} is not registered in ${cur.type} __index__ file. One of the runtime model or __index__ file is corrupted (better be __index__).  `,
        });
      }

      // use importedAt in priority, as updates made before import do not matter
      const checksumsUpdatedAt = version?.importedAt || version?.exportedAt;
      if (
        checksumsUpdatedAt &&
        changes[idx].updatedAt &&
        new Date(changes[idx].updatedAt!).getTime() <
          new Date(checksumsUpdatedAt!).getTime()
      ) {
        errors.push({
          msg: `Detected changes on ${cur.type} ${cur.slug} although last ${
            version?.importedAt ? 'import' : 'export'
          } date is ${checksumsUpdatedAt} and last ${
            cur.slug
          } update would be from ${changes[idx].updatedAt} (as told per ${
            cur.type
          }/__index__.yml file)`,
        });
      }
    });
    return errors;
  }

  async listWorkspaceAutomationsImports(
    workspaceId: string
  ): Promise<WorkspaceEntries> {
    const [automationFiles, importFiles, pageFiles] = await Promise.all([
      this.storage
        .find({
          workspaceId: workspaceId,
          dsulType: DSULType.Automations,
          parentFolder: true,
        })
        .catch(() => []),
      this.storage
        .find({
          workspaceId: workspaceId,
          dsulType: DSULType.Imports,
          parentFolder: true,
        })
        .catch(() => []),
      this.storage
        .find({
          workspaceId: workspaceId,
          dsulType: DSULType.Pages,
          parentFolder: true,
        })
        .catch(() => []),
    ]);

    const automations = automationFiles
      .map(({ key }: { key: string }) => {
        const slug = key.lastIndexOf('.')
          ? key.slice(0, key.lastIndexOf('.'))
          : key;
        return { slug };
      })
      .filter(({ slug }: any) => slug !== '__index__');

    const imports = importFiles
      .map(({ key }: { key: string }) => {
        const slug = key.lastIndexOf('.')
          ? key.slice(0, key.lastIndexOf('.'))
          : key;
        return { slug };
      })
      .filter(({ slug }: any) => slug !== '__index__');

    const pages = (pageFiles || [])
      .map(({ key }: { key: string }) => {
        const slug = key.lastIndexOf('.')
          ? key.slice(0, key.lastIndexOf('.'))
          : key;
        return { slug };
      })
      .filter(({ slug }: any) => slug !== '__index__');

    return { automations, imports, pages: pages! };
  }

  async checkRuntimeModelIntegrity(
    workspaceId: string,
    entries: WorkspaceEntries,
    importChecksums: Checksums,
    storageChecksums: Checksums
  ) {
    const errors: object[] = [];
    const storageModel = await this.storage.get({
      workspaceId: workspaceId,
      dsulType: DSULType.RuntimeModel,
    });

    // Check any missing automations/appInstances from runtime model
    const checkTypes = Object.keys(entries || {}).filter(
      (cur) => cur !== DSULType.Pages
    ) as (DSULType.Automations | DSULType.Imports)[];
    for (let type of checkTypes) {
      const expectedSlugs = entries[type] || [];
      expectedSlugs.forEach(({ slug }) => {
        const storageEntry = storageModel?.[type]?.[slug!];
        if (!storageEntry) {
          errors.push({
            error: `Missing${type.charAt(0).toUpperCase()}${type.slice(
              1
            )}InModel`,
            type,
            slug,
            message: `${type} ${slug} present on storage but missing from runtime model`,
          });
          return true;
        }
        return false;
      });
    }

    // Detect (normal) changes from last import/export checksums
    const changes: Changes = [];
    const checkTypesChecksums = Object.keys(importChecksums || {}).filter(
      (cur) => cur !== DSULType.Pages
    ) as (DSULType.Automations | DSULType.Imports)[];
    for (let type of checkTypesChecksums) {
      const currentChecksums = Object.entries(importChecksums[type] || []) as [
        string,
        string
      ][];
      currentChecksums.forEach(([slug, expectedChecksum]) => {
        const storageEntry = storageModel?.[type]?.[slug!];
        if (!storageEntry) {
          errors.push({
            error: `Missing${type.charAt(0).toUpperCase()}${type.slice(
              1
            )}InModel`,
            type,
            slug,
            message: `${type} ${slug} present on last import/export but missing from runtime model`,
          });
          return;
        }
        const currentChecksum = generateChecksum(storageEntry);
        if (currentChecksum !== expectedChecksum) {
          changes.push({
            type,
            slug,
            currentChecksum,
            expectedChecksum,
          });
        }
      });
    }

    // Detect ABNORMAL changes between runtime model & persisted automations/appInstances files
    for (let type of checkTypesChecksums) {
      const currentChecksums = Object.entries(storageChecksums[type] || []) as [
        string,
        string
      ][];
      currentChecksums.forEach(([slug, expectedChecksum]) => {
        const storageEntry = storageModel?.[type]?.[slug!];
        if (!storageEntry) {
          return;
        }
        const currentChecksum = generateChecksum(storageEntry);
        if (currentChecksum !== expectedChecksum) {
          errors.push({
            error: `Outdated${type.charAt(0).toUpperCase()}${type.slice(
              1
            )}InModel`,
            message: `${type} ${slug} checksum is different between current runtime model & ${type} file. Save again the ${type} to force synchronization.`,
            type,
            slug,
            currentChecksum,
            expectedChecksum,
          });
        }
      });
    }

    return { errors, changes };
  }
}
