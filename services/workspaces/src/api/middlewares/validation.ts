import { NextFunction, Request, Response } from 'express';
import * as OpenApiValidator from 'express-openapi-validator';
import { HttpError } from 'express-openapi-validator/dist/framework/types';
import yaml from 'js-yaml';
import fs from 'fs';
import { DEBUG, OPENAPI_FILEPATH } from '../../../config';
import { ErrorSeverity, PrismeError } from '../../errors';

interface ValidationOpts {
  ignorePaths?: string[];
}
export const validationMiddleware = ({ ignorePaths = [] }: ValidationOpts) => {
  const ignoreRegexps = ignorePaths.map((regexp) => new RegExp(regexp));

  // Load the yml file ourselves as express-openapi-validator loader does not support yaml overrides (<<:)
  let apiSpec: any;
  try {
    apiSpec = yaml.load(fs.readFileSync(OPENAPI_FILEPATH).toString());
  } catch (err) {
    throw new PrismeError(
      `Could not load OpenAPI definition file at '${OPENAPI_FILEPATH}'`,
      { err },
      ErrorSeverity.Fatal
    );
  }

  return OpenApiValidator.middleware({
    apiSpec,
    validateRequests: true,
    validateResponses: DEBUG,
    validateSecurity: false,
    validateApiSpec: false,
    ignorePaths: !ignorePaths.length
      ? undefined
      : (path: string) => {
          return ignoreRegexps.some((cur) => cur.test(path));
        },
  });
};

export class RequestValidationError extends PrismeError {
  constructor(msg: string, details: any[] = []) {
    super(msg, details);
  }
}

export const validationErrorMiddleware = (
  err: Error,
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (err instanceof HttpError) {
    next(new RequestValidationError(err.message, err.errors));
  } else {
    next(err);
  }
};
