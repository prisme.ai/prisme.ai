ARG DEPENDENCIES_IMAGE=registry.gitlab.com/prisme.ai/prisme.ai/prisme.ai-dependencies
ARG DEPENDENCIES_TAG=latest

FROM $DEPENDENCIES_IMAGE:$DEPENDENCIES_TAG as build

RUN apk add git openssh && \
  addgroup -S api && adduser -S api -G api && \
  chown -R api:api /www

# We need to migrate our helm charts to use non privileged port (currently using port 80) before switching to api user
USER root:api
WORKDIR /www
COPY --chown=api:api services/workspaces/ /www/services/workspaces
# If we do not move node_modules @prisme.ai/ as ./packages, npm prune --production broke them trying to recreate usual symlinks
RUN npm run build:workspaces && mv node_modules/@prisme.ai packages/ && npm prune --production && \
  # Prepare directories needed for dynamic files creation
  # Git versioning
  mkdir /tmp/versioning && chmod 771 /tmp/versioning  && \
  # Heapdumps
  mkdir /www/heapdumps && chmod 771 /www/heapdumps && \
  # Models & Uploads (this should be mounted as a volume for persistence) && \
  mkdir -p /www/data/models /www/data/uploads && chmod -R 771 /www/data/

EXPOSE 3003
ENV OPENAPI_FILEPATH /www/specifications/swagger.yml
ENV NODE_ENV production
ENV WORKSPACES_STORAGE_GIT_DIRPATH /tmp/versioning
ENV WORKSPACES_STORAGE_FILESYSTEM_DIRPATH /www/data/models
ENV UPLOADS_STORAGE_FILESYSTEM_DIRPATH /www/data/uploads
ENV HEAPDUMPS_DIRECTORY /www/heapdumps

CMD [ "node", "--abort-on-uncaught-exception", "services/workspaces/dist/index.js"]

