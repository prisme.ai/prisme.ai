import type { NextApiRequest, NextApiResponse } from 'next';
import { getMessages } from '@/i18n/getMessages';

type ResponseData = {
  messages: Record<string, unknown>;
};

export default async function handler(req: NextApiRequest, res: NextApiResponse<ResponseData>) {
  const lang = `${req.query.lang}` || 'en';
  const { messages } = await getMessages(lang, 'en');
  res.status(200).json({ messages });
}
