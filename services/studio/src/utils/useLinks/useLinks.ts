import useBlocks from '@/components/PageBuilder/useBlocks';
import useTranslations from '@/i18n/useTranslations';
import { useWorkspace } from '@/providers/Workspace';
import { useMemo } from 'react';

export function useInstructionsLinks() {
  const t = useTranslations('common');
  const { workspace } = useWorkspace();

  return useMemo(
    () =>
      Object.entries(workspace.automations || {}).map(([slug]) => {
        return {
          keyword: slug,
          onClick() {
            window.open(`/workspaces/${workspace.id}/automations/${encodeURIComponent(slug)}`);
          },
          hoverMessage: t('codeEditor.links.hoverMessage_automation', {
            key: navigator.platform.indexOf('Mac') > -1 ? '⌘ cmd' : '^ ctrl',
          }),
        };
      }),
    [workspace.automations, workspace.id, t],
  );
}

export function useBlocksLinks() {
  const t = useTranslations('common');
  const { workspace } = useWorkspace();
  const { variants: blocks } = useBlocks();

  return useMemo(() => {
    return blocks
      .filter(({ owned }) => owned)
      .map(({ slug }) => ({
        keyword: slug,
        onClick() {
          window.open(`/workspaces/${workspace.id}/blocks/${encodeURIComponent(slug)}`);
        },
        hoverMessage: t('codeEditor.links.hoverMessage_block'),
      }));
  }, [blocks, workspace.id, t]);
}
