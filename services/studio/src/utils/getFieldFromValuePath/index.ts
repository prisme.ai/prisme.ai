import { Schema } from '@/components/SchemaForm';

export default function getFieldFromValuePath(schema: Schema, path: string) {
  const pathParts = path.split(/\./);
  const found = pathParts.reduce<Schema | null>((prev, p) => {
    if (!prev || !prev.properties) return null;
    return prev.properties[p];
  }, schema);
  return found;
}
