import { expect, it } from 'vitest';
import { Schema } from '@/components/SchemaForm';
import getFieldFromValuePath from './index';

it('should get field from value path', () => {
  const schema: Schema = {
    type: 'object',
    properties: {
      foo: {
        type: 'object',
        properties: {
          bar: {
            type: 'string',
          },
        },
      },
    },
  };
  expect(getFieldFromValuePath(schema, 'foo.bar')).toEqual({
    type: 'string',
  });
});

// TODO
it.skip('should get field from value path with arrays', () => {
  const schema: Schema = {
    type: 'array',
    items: {
      type: 'object',
      properties: {
        bar: {
          type: 'string',
        },
      },
    },
  };
  expect(getFieldFromValuePath(schema, '[0].bar')).toEqual({
    type: 'string',
  });
});
