import { expect, it } from 'vitest';
import { toKebab } from './index';

it('shoud convert to kebab', () => {
  expect(toKebab('abc def ghi')).toBe('abc-def-ghi');
  expect(toKebab('abc DEF ghi')).toBe('abc-DEF-ghi');
});
