import { useRouter as oUseRouter } from 'next/navigation';
import { useMemo } from 'react';

export interface RouteChangeEventDetails {
  route: string;
  push?: boolean;
  replace?: boolean;
}

export const RouteChangeStartEvent = 'routeChangeStart';

interface UseRouterOptions {
  force?: boolean;
}

export default function useRouter({ force }: UseRouterOptions = {}) {
  const router = oUseRouter();
  return useMemo(() => {
    const oPush = router.push;
    const oReplace = router.replace;
    return {
      ...router,
      push(route: string) {
        if (!force) {
          const event = new CustomEvent<RouteChangeEventDetails>(RouteChangeStartEvent, {
            cancelable: true,
            detail: {
              route,
              push: true,
            },
          });
          globalThis.dispatchEvent(event);
          if (event.defaultPrevented) return;
        }
        return oPush(route);
      },
      replace(route: string) {
        if (!force) {
          const event = new CustomEvent<RouteChangeEventDetails>(RouteChangeStartEvent, {
            cancelable: true,
            detail: {
              route,
              replace: true,
            },
          });
          globalThis.dispatchEvent(event);
          if (event.defaultPrevented) return;
        }
        return oReplace(route);
      },
    };
  }, [router, force]);
}
