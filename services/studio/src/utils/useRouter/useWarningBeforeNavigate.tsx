import { ReactNode, useEffect, useRef } from 'react';
import useRouter, { RouteChangeEventDetails, RouteChangeStartEvent } from './useRouter';
import WarningIcon from '@/svgs/warning.svg';
import useTranslations from '@/i18n/useTranslations';
import { useModal } from '@/providers/Modal';

interface WarningBeforeNavigateOptions {
  icon?: ReactNode;
  content?: ReactNode;
  okLabel?: ReactNode;
  cancelLabel?: ReactNode;
}

export default function useWarningBeforeNavigate(
  warn: boolean,
  { icon = WarningIcon, content, okLabel, cancelLabel }: WarningBeforeNavigateOptions = {},
) {
  const t = useTranslations('workspaces');
  const showWarn = useRef(warn);
  const modal = useModal();

  useEffect(() => {
    showWarn.current = warn;
  }, [warn]);

  const { push, replace } = useRouter({ force: true });
  useEffect(() => {
    const listener = (e: Event) => {
      const {
        route,
        push: isPush,
        replace: isReplace,
      } = (e as CustomEvent<RouteChangeEventDetails>).detail;
      if (showWarn.current) {
        showWarn.current = false;
        e.preventDefault();
        modal.confirm({
          icon,
          content: content || t('workspace.dirtyWarning'),
          onOk: () => {
            if (isPush) {
              push(route);
            }
            if (isReplace) {
              replace(route);
            }
            showWarn.current = warn;
          },
          onCancel() {
            showWarn.current = warn;
          },
          okText: okLabel || t('workspace.dirtyOk'),
          cancelText: cancelLabel || t('cancel', { ns: 'common' }),
        });
      }
    };
    globalThis.addEventListener(RouteChangeStartEvent, listener);
    return () => {
      globalThis.removeEventListener(RouteChangeStartEvent, listener);
    };
  }, [warn, t, cancelLabel, okLabel, content, icon, push, replace, modal]);
}
