import { routing } from '@/i18n/routing';
import { useEnv } from '@/providers/Env';
import { useWorkspace } from '@/providers/Workspace';
import { useLocale } from 'next-intl';
import { useCallback } from 'react';

export class ReplaceStateEvent extends Event {
  public prevLocation = '';
  public nextLocation = '';
}

export function replaceSilently(newPath: string) {
  const [, lang, ...url] = window.location.pathname.split('/') || [];
  const e = new ReplaceStateEvent('replaceState');
  e.prevLocation = `/${url.join('/')}`;
  e.nextLocation = newPath;
  window.history.replaceState({}, '', `/${lang}${newPath}`.replace(/\/\//, '/'));

  window?.dispatchEvent(e);
}

export const usePageEndpoint = () => {
  const { PAGES_HOST = `${global?.location?.origin}/pages` } = useEnv<{
    PAGES_HOST: string;
  }>();
  const language = useLocale();
  const {
    workspace: { slug },
  } = useWorkspace();

  if (!slug) return '';

  return `${window.location.protocol}//${slug}${PAGES_HOST}/${language}`;
};

export function useUrls() {
  const { PAGES_HOST = `${global?.location?.origin}/pages`, API_URL = '' } = useEnv<{
    PAGES_HOST: string;
    API_URL: string;
  }>();

  const generateEndpoint = useCallback(
    (workspaceId: string, slug: string) => `${API_URL}/workspaces/${workspaceId}/webhooks/${slug}`,
    [API_URL],
  );

  const getSubdomain = useCallback(
    (host: string) => {
      return host.split(PAGES_HOST)[0];
    },
    [PAGES_HOST],
  );

  const generatePageUrl = useCallback(
    (workspaceSlug: string, pageSlug: string) => {
      return `${window.location.protocol}//${workspaceSlug}${PAGES_HOST}/${
        pageSlug === 'index' ? '' : pageSlug
      }`;
    },
    [PAGES_HOST],
  );
  return { generateEndpoint, getSubdomain, generatePageUrl };
}

export function removeLocaleFromPath(pathname: string) {
  const [first, ...parts] = pathname.split(/\//).filter(Boolean);
  if (routing.locales.includes(first as (typeof routing.locales)[number])) {
    return `/${parts.join('/')}`;
  }
  return pathname;
}
