import { expect, it, vi } from 'vitest';
import { removeLocaleFromPath, replaceSilently, usePageEndpoint } from './index';
// @ts-ignore
import { mock } from '@/providers/Workspace';

vi.mock('@/providers/Workspace', () => {
  const mock = {
    slug: 'my.website',
  };
  return {
    mock,
    useWorkspace: () => ({
      workspace: mock,
    }),
  };
});

vi.mock('next-intl', () => ({
  useLocale: () => 'en',
}));

vi.mock('@/providers/Env', () => {
  return {
    useEnv() {
      return {
        API_URL: 'https://api',
        PAGES_HOST: '.pages.prisme.ai',
      };
    },
  };
});

it('should get pages host', () => {
  expect(usePageEndpoint()).toBe('http://my.website.pages.prisme.ai/en');

  mock.slug = '';
  expect(usePageEndpoint()).toBe('');
});

it('should replace silently current url', () => {
  const replaceState = vi.fn();
  window.history.replaceState = replaceState;
  // @ts-ignore
  delete window.location;
  window.location = { pathname: '/fr/foo/bar' } as Location;
  replaceSilently('/somewhere/else');
  expect(replaceState).toBeCalledWith({}, '', '/fr/somewhere/else');
});

it('should remove locale from path', () => {
  expect(removeLocaleFromPath('/foo')).toBe('/foo');
  expect(removeLocaleFromPath('/en/foo')).toBe('/foo');
  expect(removeLocaleFromPath('/en')).toBe('/');
  expect(removeLocaleFromPath('/')).toBe('/');
  expect(removeLocaleFromPath('/foo/bar')).toBe('/foo/bar');
});
