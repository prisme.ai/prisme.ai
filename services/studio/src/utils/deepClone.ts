export default function deepClone(o: unknown) {
  return JSON.parse(JSON.stringify(o));
}
