import { useEffect, useRef } from 'react';

export const useYaml = () => {
  const workerRef = useRef<Worker>();

  useEffect(() => {
    workerRef.current = new Worker(new URL('./yaml.worker.ts', import.meta.url), {
      type: 'module',
    });
    return () => {
      workerRef.current?.terminate();
    };
  }, []);

  const call = <T>(action: string, data: any): Promise<T> =>
    new Promise((resolve, reject) => {
      const requestId = Math.random();
      const callback = ({ data: { id, data, error } }: MessageEvent) => {
        if (id !== requestId) return;
        if (error) reject(error);
        else resolve(data);
        workerRef.current?.removeEventListener('message', callback);
      };
      workerRef.current?.addEventListener('message', callback);
      workerRef.current?.postMessage({ action, data, id: requestId });
    });
  const toJSON = async <T>(value: string) => {
    return await call<T>('load', value);
  };
  const toYaml = async <T>(value: T) => {
    return await call<string>('dump', value);
  };
  return { toYaml, toJSON };
};

export default useYaml;
