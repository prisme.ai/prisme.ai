import yaml from 'js-yaml';

addEventListener('message', ({ data: { action, data, id } }) => {
  try {
    let message;
    switch (action) {
      case 'load':
        message = yaml.load(data);
        break;
      case 'dump':
        message = yaml.dump(data, {
          noRefs: true,
          styles: {
            '!!null': 'empty',
          },
          lineWidth: 999,
        });
        break;
    }

    postMessage({ id, data: message });
  } catch (error) {
    postMessage({ id, error });
  }
});
