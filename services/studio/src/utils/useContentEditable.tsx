import {
  FocusEvent,
  FormEvent,
  KeyboardEvent,
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';

interface ContentEditable {
  value?: string;
  onInput?: (value: string) => void;
  onChange?: (value: string) => void;
  onRemove?: () => void;
  onEnter?: (value: string) => void;
}

export const useContentEditable = ({
  value: initialValue,
  onChange,
  onInput,
  onEnter,
}: ContentEditable) => {
  const [value, setValue] = useState(initialValue);
  const prevValue = useRef(initialValue);
  const onFieldInput = useCallback(
    (e: FormEvent<HTMLSpanElement>) => {
      const target = e.target as HTMLSpanElement;
      onChange && onChange(target.innerText);
      prevValue.current = target.innerText;
    },
    [onChange],
  );
  const onKeyDown = useCallback(
    (e: KeyboardEvent) => {
      if (e.key === 'Enter') {
        e.preventDefault();
        const target = e.target as HTMLElement;
        onInput && onInput(target.innerText);
        onEnter && onEnter(target.innerText);
      }
    },
    [onInput, onEnter],
  );
  const onBlur = useCallback(
    (e: FocusEvent) => {
      const target = e.target as HTMLElement;
      onInput && onInput(target.innerText);
    },
    [onInput],
  );
  useEffect(() => {
    if (initialValue !== prevValue.current) {
      setValue(initialValue);
      prevValue.current = initialValue;
    }
  }, [initialValue]);

  return useMemo(
    () => ({
      onKeyDown,
      onBlur,
      onInput: onFieldInput,
      contentEditable: true,
      suppressContentEditableWarning: true,
      value,
    }),
    [onKeyDown, onBlur, onFieldInput, value],
  );
};

export default useContentEditable;
