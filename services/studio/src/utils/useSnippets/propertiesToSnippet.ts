import { Schema } from '../../components/SchemaForm';

export default function propertiesToSnippet(properties: Schema['properties'], prefix = '  ') {
  return Object.entries(properties || {}).reduce((prev, [name, { type, enum: enumList }], k) => {
    if (type === 'boolean')
      return `${prev}
${prefix}${name}: $\{${k + 1}|true,false|}`;

    if (enumList)
      return `${prev}
${prefix}${name}: $\{${k + 1}|${enumList.join(',')}|}`;

    return `${prev}
${prefix}${name}: $${k + 1}`;
  }, '');
}
