import getSchema from '@/components/BlocksListEditor/getSchema';
import propertiesToSnippet from '@/utils/useSnippets/propertiesToSnippet';
import useBlocks from '@/components/PageBuilder/useBlocks';
import { Schema } from '@/components/SchemaForm';
import { useWorkspace } from '@/providers/Workspace';
import useLocalizedText from '@/utils/useLocalizedText';
import { useEffect, useMemo, useState } from 'react';
import BUILTIN_INSTRUCTIONS from '@prisme.ai/validation/instructions.json';
import useTranslations from '@/i18n/useTranslations';

export function useBlocksSnippets() {
  const { localize } = useLocalizedText();
  const {
    workspace: { id: workspaceId },
  } = useWorkspace();
  const { variants: blocks } = useBlocks();
  const [schemas, setSchemas] = useState<
    {
      slug: string;
      schema: Schema;
      description: Prismeai.LocalizedText;
    }[]
  >([]);

  useEffect(() => {
    async function fetchSchemas() {
      const schemas = await Promise.all(
        blocks.map(async ({ slug, description = '' }) => {
          const schema = await getSchema({ slug: slug, workspaceId, blocks });
          return {
            slug,
            description,
            schema,
          };
        }),
      );
      setSchemas(schemas);
    }
    fetchSchemas();
  }, [blocks, workspaceId]);

  return useMemo(() => {
    return {
      trigger: /- \w*/,
      suggestions: (schemas || []).map(({ slug, schema, description }) => {
        return {
          label: slug,
          documentation: localize(description),
          insertText: `slug: ${slug}${propertiesToSnippet(schema.properties || {})}`,
        };
      }),
    };
  }, [localize, schemas]);
}

function getBuiltinAttributes(slug: string, properties: Record<string, unknown> = {}) {
  switch (slug) {
    case 'repeat':
      return `
    do:
      - $1`;
    case 'conditions':
      return `
    '$1':
      - $2
    $\{3: default:
      - $4}`;
    case 'comment':
      return ` $1`;
    default:
      if (properties.type === 'object') {
        if (properties.properties)
          return `${propertiesToSnippet(properties.properties as Schema['properties'], '    ')}`;
      }
      return `
    $1`;
  }
}

export function useInstrutionsSnippets() {
  const t = useTranslations('workspaces');
  const { localize } = useLocalizedText();
  const { workspace } = useWorkspace();

  return useMemo(() => {
    return {
      trigger: /- \w*/,
      suggestions: [
        ...Object.entries(BUILTIN_INSTRUCTIONS).map(([slug, instruction]) => {
          const attrs = getBuiltinAttributes(
            slug,
            instruction.properties[slug as keyof typeof instruction.properties],
          );
          return {
            label: slug,
            documentation: t(`automations.instruction.description_${slug}`),
            insertText: `${slug}:${attrs ? `${attrs}` : ' $1'}`,
          };
        }),
        ...Object.entries(workspace.automations || {}).map(
          ([slug, { arguments: properties = {}, description }]) => {
            const attrs = propertiesToSnippet(properties as Schema, '    ');
            return {
              label: slug,
              documentation: localize(description),
              insertText: `${slug}:${attrs}`,
            };
          },
        ),
        ...Object.values(workspace.imports || {}).flatMap(({ slug: appSlug, automations }) => {
          return Object.values(automations || {}).map(
            ({ slug, arguments: properties, description }) => {
              const attrs = propertiesToSnippet(properties as Schema, '    ');
              return {
                label: `${appSlug}.${slug}`,
                documentation: localize(description),
                insertText: `${appSlug}.${slug}:${attrs}`,
              };
            },
          );
        }),
      ],
    };
  }, [workspace.automations, workspace.imports, t, localize]);
}
