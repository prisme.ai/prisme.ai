import { expect, it } from 'vitest';
import propertiesToSnippet from './propertiesToSnippet';

it('should write properties for snippet', () => {
  expect(
    propertiesToSnippet(
      {
        string: {
          type: 'string',
        },
        boolean: {
          type: 'boolean',
        },
        enum: {
          type: 'string',
          enum: ['foo', 'bar'],
        },
      },
      '  ',
    ),
  ).toBe(`
  string: $1
  boolean: $\{2|true,false|}
  enum: $\{3|foo,bar|}`);
});
