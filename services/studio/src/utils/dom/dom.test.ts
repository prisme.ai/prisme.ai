import { expect, it, vi } from 'vitest';
import { selectText } from './index';
it('should select text', () => {
  vi.spyOn(document, 'createRange');
  const element = document.createElement('div');
  element.innerText = 'foo';
  selectText(element);
  expect(document.createRange).toHaveBeenCalled();
});
