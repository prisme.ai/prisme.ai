import { useEnv } from '@/providers/Env/';
import { Api } from '@prisme.ai/sdk';
import Storage from '@/utils/Storage';
import isServerSide from '../isServerSide';
import { useLocale } from 'next-intl';

const cache = new Map<string, Api>();

export default function useApi() {
  const {
    API_URL = '',
    INTERNAL_API_URL,
    OIDC_PROVIDER_URL = '',
    OIDC_STUDIO_CLIENT_ID = '',
    OIDC_CLIENT_ID_HEADER = '',
    CONSOLE_URL = '',
    WEBSOCKETS_DEFAULT_TRANSPORTS = [],
  } = useEnv<{
    API_URL: string;
    OIDC_PROVIDER_URL: string;
    OIDC_STUDIO_CLIENT_ID: string;
    OIDC_CLIENT_ID_HEADER: string;
    CONSOLE_URL: string;
    WEBSOCKETS_DEFAULT_TRANSPORTS: string[];
    INTERNAL_API_URL: string;
  }>();
  const host = isServerSide() && INTERNAL_API_URL ? INTERNAL_API_URL : API_URL;
  const language = useLocale();
  if (!cache.has(host)) {
    const getRedirectURI = () => {
      try {
        return new URL('/signin', CONSOLE_URL || 'http://localhost:3000').toString();
      } catch {
        return '';
      }
    };
    const api = new Api({
      host,
      oidc: {
        url: OIDC_PROVIDER_URL,
        clientId: OIDC_STUDIO_CLIENT_ID,
        clientIdHeader: OIDC_CLIENT_ID_HEADER,
        redirectUri: getRedirectURI(),
      },
      websockets: {
        transports: WEBSOCKETS_DEFAULT_TRANSPORTS,
      },
    });
    api.language = language;
    api.token = Storage.get('access-token');
    const legacyToken = Storage.get('auth-token');
    if (!api.token && legacyToken) {
      api.legacyToken = legacyToken;
    }
    cache.set(API_URL, api);
  }
  const api = cache.get(host);
  if (!api) {
    throw new Error();
  }
  return api;
}
