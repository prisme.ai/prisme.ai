import { JSONPath } from 'jsonpath-plus';

export function interpolateString(str: string, values: Record<string, any> = {}) {
  return str.replace(
    /\$\{([^}]+)\}/g,
    (_, m) => JSONPath({ json: values, path: m, wrap: false }) || '',
  );
}

export function interpolate(into: any, values: Record<string, any>) {
  if (typeof into === 'string') {
    return interpolateString(into, values);
  }
  const isArray = Array.isArray(into);
  const newObject = isArray ? [...into] : { ...into };
  for (const key of Object.keys(newObject)) {
    const value = newObject[key];
    newObject[key] = interpolate(value, values);
  }
  return newObject;
}
