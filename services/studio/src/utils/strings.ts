import { remove as removeDiacritics } from 'diacritics';
import hash from 'hash-sum';

export const truncate = (str = '', len: number, ellipsis = '…') => {
  const original = `${str || ''}`;
  const truncated = original.substring(0, len);
  return `${truncated}${original.length > truncated.length ? ellipsis : ''}`;
};

export const slugify = (name: string) => {
  const slug = removeDiacritics(name)
    .replace(/[^a-zA-Z0-9 _-]+/g, '')
    .trim()
    .slice(0, 20);

  return slug;
};

export const slugifyUnique = (name: string, existents: string[]) => {
  const base = slugify(name);
  let slug = base;
  let idx = 0;
  while (existents.includes(slug)) {
    idx++;
    slug = `${base}-${idx}`;
  }

  return slug;
};

export function stringToHexaColor(text: string) {
  return hash(text).substring(0, 6);
}
