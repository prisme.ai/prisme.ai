import createMiddleware from 'next-intl/middleware';
import { routing } from './i18n/routing';

const handleI18nRouting = createMiddleware(routing);

type NextRequest = Parameters<typeof handleI18nRouting>[0];

export default function middleware(req: NextRequest) {
  const res = handleI18nRouting(req);

  if (process.env.CSP_HEADER) {
    res.headers.set('Content-Security-Policy', process.env.CSP_HEADER);
  }

  return res;
}

export const config = {
  // Match only internationalized pathnames
  matcher: ['/((?!api|_next|_vercel|.*\\..*).*)'],
};
