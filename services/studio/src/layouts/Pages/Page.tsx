'use client';

import DefaultErrorPage from 'next/error';
import React, { useEffect, useState } from 'react';
import { usePageContent } from '@/providers/PageContent';
import Loading from '@/components/Loading';
import { notFound } from 'next/navigation';
import PageContent from '@/components/PageContent';
import useApi from '@/utils/api/useApi';

export const Page = () => {
  const api = useApi();
  const { page, error, loading } = usePageContent();

  const [displayError, setDisplayError] = useState(false);

  useEffect(() => {
    window.parent.postMessage('page-ready', '*');
    if (!loading && error && error && ![401, 403].includes(error)) {
      setTimeout(() => setDisplayError(true), 10);
    }
  }, [error, loading]);

  if (!page && loading) return <Loading />;

  if (page) {
    if (page.apiKey) {
      api.apiKey = page.apiKey;
    }
    return <PageContent page={page} error={error} />;
  }

  if (error && ![401, 403].includes(error)) {
    if (displayError) {
      return notFound();
    }
    return <Loading />;
  }

  if (typeof window === 'undefined') {
    return null;
  }

  return <DefaultErrorPage statusCode={error || 0} />;
};

export default Page;
