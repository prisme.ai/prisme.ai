'use client';

import { redirect, useSelectedLayoutSegments } from 'next/navigation';

export default function NotFound() {
  const [lang, product, id] = useSelectedLayoutSegments() || [];
  if (product === 'workspaces') {
    redirect(`/${lang}/${product}/${id}`);
  }

  return null;
}
