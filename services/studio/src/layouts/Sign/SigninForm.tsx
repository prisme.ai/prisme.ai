'use client';

import { Form } from 'react-final-form';
import { Button, Input } from 'antd';
import Field from '@/components/Field';
import { useCallback, useEffect, useMemo, useState } from 'react';
import { useUser } from '@/providers/User';
import Storage from '@/utils/Storage';
import Link from 'next/link';
import MicrosoftIcon from '@/svgs/microsoft.svg';
import useLocalizedText from '@/utils/useLocalizedText';
import { useTranslations } from 'next-intl';
import { useEnv } from '@/providers/Env';
import styles from './sign.module.scss';

interface AuthProvider {
  name: string;
  extends?: string;
  label?: Prismeai.LocalizedText;
  icon?: string;
  url?: string;
}

interface Values {
  email: string;
  password: string;
}

interface SigninFormProps {
  onSignin?: (user: Prismeai.User | null) => void;
  show403?: false | string;
}

export const SigninForm = ({ show403 }: SigninFormProps) => {
  const config = useEnv<{
    API_URL: string;
    ENABLED_AUTH_PROVIDERS: AuthProvider[];
    CONSOLE_URL: string;
  }>();
  const { API_URL, CONSOLE_URL } = config;
  const ENABLED_AUTH_PROVIDERS = useMemo(
    () => config.ENABLED_AUTH_PROVIDERS || [{ name: 'local' }],
    [config],
  );

  const t = useTranslations('sign');
  const { localize } = useLocalizedText();
  const { loading, signin, initAuthentication } = useUser();
  const [error, setError] = useState(false);
  const submit = useCallback(
    async ({ email, password }: Values) => {
      const success = await signin(email, password);
      setError(!success);
    },
    [signin],
  );
  const validate = (values: Values) => {
    const errors: Partial<Values> = {};
    if (!values.email) {
      errors.email = 'required';
    }
    if (!values.password) {
      errors.password = 'required';
    }
    return errors;
  };

  useEffect(() => {
    async function init(redirect?: string | null) {
      const url = await initAuthentication({
        redirect: redirect || '',
      });
      window.location.assign(url);
    }
    const urlParams = new URLSearchParams(window.location.search);
    const interactionUid = urlParams.get('interaction');
    const code = urlParams.get('code');
    const redirect = urlParams.get('redirect');
    if (!code && !interactionUid && !urlParams.get('error') && !show403) {
      init(redirect);
    }
  }, [initAuthentication, show403]);

  const oAuthButtons: {
    name: string;
    url?: string;
    label?: Prismeai.LocalizedText;
    icon?: string;
  }[] = useMemo(() => {
    function getProviderDetails(provider: string) {
      switch (provider) {
        case 'azure':
          return {
            url: `${API_URL}/login/azure`,
            icon: MicrosoftIcon,
            label: t('in.withAzure'),
          };
      }
      return null;
    }
    return ENABLED_AUTH_PROVIDERS.filter(({ name }) => name !== 'local').map(
      ({ name, extends: provider = name, ...rest }) => {
        const providerDetails = getProviderDetails(provider);
        const url =
          rest?.url && rest?.url.startsWith('/')
            ? `${API_URL}${rest?.url}`
            : rest?.url || providerDetails?.url || `${API_URL}/login?provider=${name}`;
        return {
          name,
          ...providerDetails,
          ...rest,
          url,
        };
      },
    );
  }, [t, API_URL, ENABLED_AUTH_PROVIDERS]);

  // 1. Init authentication flow
  const urlParams = new URLSearchParams(window.location.search);
  const interactionUid = urlParams.get('interaction');
  const code = urlParams.get('code');
  const redirect = urlParams.get('redirect');
  if (!code && !interactionUid && !urlParams.get('error')) {
    if (!show403) {
      return null;
    }
    return (
      <div className={styles['signin-init']}>
        <div className={styles['signin-init-text']}>{show403}</div>
        <div>
          <Button
            onClick={async () => {
              const url = await initAuthentication({
                redirect: redirect || '',
              });
              window.location.assign(url);
            }}
            type="primary"
            className={styles['signin-init-button']}
          >
            {t('in.signin')}
          </Button>
        </div>
      </div>
    );
  } else if (code) {
    // Authorization code processing, wait for redirection
    return null;
  }

  const hasLocal = !!ENABLED_AUTH_PROVIDERS.find(({ name }) => name === 'local');
  const hasOAuthButtons = hasLocal && oAuthButtons.length > 0;

  return (
    <Form onSubmit={submit} validate={validate}>
      {({ handleSubmit }) => (
        <form onSubmit={handleSubmit} className={styles['signin-form']} data-testid="signin-form">
          <div className={styles['signin-form-providers']}>
            {oAuthButtons.map(
              ({ icon, label, url }) =>
                url && (
                  <Link
                    href={url}
                    className={styles['signin-form-provider-link']}
                    key={`${label}-${url}`}
                  >
                    <Button
                      type="primary"
                      htmlType="button"
                      className={styles['signin-form-provider-link-button']}
                    >
                      {icon && (
                        // eslint-disable-next-line @next/next/no-img-element
                        <img src={icon} width={16} height={16} alt="Microsoft" />
                      )}

                      {label && (
                        <span className={styles['signin-form-provider-link-text']}>
                          {localize(label)}
                        </span>
                      )}
                    </Button>
                  </Link>
                ),
            )}
            {hasOAuthButtons ? (
              <div className={styles['signin-form-separator']}>
                <span className={styles['signin-form-separator-text']}>{t('in.or')}</span>
              </div>
            ) : null}
            {hasLocal && (
              <>
                <div className={styles['signin-form-local']}>
                  <Field
                    name="email"
                    containerClassName={styles['signin-form-local-field']}
                    initialValue={Storage.get('__email')}
                  >
                    {({ input: { type, ...inputProps }, className }) => {
                      void type;
                      return (
                        <Input
                          placeholder={t('in.email')}
                          className={`${className} ${styles['signin-form-local-input']}`}
                          {...inputProps}
                        />
                      );
                    }}
                  </Field>
                  <Field name="password" containerClassName={styles['signin-form-local-field']}>
                    {({ input: { type, ...inputProps }, className }) => {
                      void type;
                      return (
                        <Input
                          placeholder={t('in.password')}
                          className={`${className} ${styles['signin-form-local-input']}`}
                          type={'password' as any}
                          {...inputProps}
                        />
                      );
                    }}
                  </Field>
                  {error && (
                    <div className={styles['signin-form-local-error']}>
                      {t('in.error_AuthenticationError')}
                    </div>
                  )}
                </div>
                <Button
                  type="primary"
                  disabled={loading}
                  className={styles['signin-form-local-button']}
                  htmlType="submit"
                >
                  {t('in.submit')}
                </Button>
                <div className={styles['signin-link']}>
                  {t.rich('in.forgot', {
                    a: (children) => (
                      <Link href={`${CONSOLE_URL}/forgot`} className="pr-link">
                        {children}
                      </Link>
                    ),
                  })}
                </div>
              </>
            )}
          </div>
        </form>
      )}
    </Form>
  );
};
export default SigninForm;
