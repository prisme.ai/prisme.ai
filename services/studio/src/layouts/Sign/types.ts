export enum SignType {
  In = 'in',
  Up = 'up',
  Forgot = 'forgot',
  Reset = 'reset',
  Validate = 'validate',
  Manual = 'validate.manual',
  DeleteAccount = 'account.delete',
  DeleteAccountError = 'account.delete.error',
}
