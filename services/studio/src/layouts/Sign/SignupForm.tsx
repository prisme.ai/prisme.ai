'use client';

import { ReactNode, useCallback, useEffect } from 'react';
import { Form } from 'react-final-form';
import { useUser } from '../../providers/User';
import { Button, Checkbox, Input, notification } from 'antd';
import Field from '../../components/Field';
import { isFormFieldValid } from '../../utils/forms';
import Storage from '../../utils/Storage';
import { useLocale, useTranslations } from 'next-intl';
import Link from 'next/link';
import { useEnv } from '@/providers/Env';
import styles from './sign.module.scss';
import useRouter from '@/utils/useRouter';

interface AuthProvider {
  name: string;
  extends?: string;
  label?: Prismeai.LocalizedText;
  icon?: string;
  url?: string;
}

interface SignupFormProps {
  onSignup?: (user: Prismeai.User, next: () => void) => void;
  redirect?: string;
}

interface Values {
  email: string;
  password: string;
  firstName: string;
  lastName: string;
  cgu: boolean;
}

export const SignupForm = ({
  onSignup,
  redirect = `https://${window.location.host}`,
}: SignupFormProps) => {
  const t = useTranslations('sign');
  const language = useLocale();
  const { user, loading, error, signup } = useUser();

  const config = useEnv<{
    ENABLED_AUTH_PROVIDERS: AuthProvider[];
  }>();
  const ENABLED_AUTH_PROVIDERS = config.ENABLED_AUTH_PROVIDERS || [{ name: 'local' }];

  const hasLocalSignup = ENABLED_AUTH_PROVIDERS.some(({ name }) => name === 'local');

  const { push } = useRouter();

  useEffect(() => {
    if (!error) return;
    notification.error({
      message: t.has(`up.error_${error.error}`) ? t(`up.error_${error.error}`) : t('up.error'),
      placement: 'bottomRight',
    });
  }, [error, t]);

  useEffect(() => {
    if (!user || loading || user.authData?.anonymous) return;
    push('/');
  }, [loading, push, user]);

  const submit = useCallback(
    async ({ email, password, firstName, lastName }: Values) => {
      Storage.set('redirect-once-authenticated', redirect);
      const res = await signup(email, password, firstName, lastName, language);
      if (!res) return;
      const { validation, ...user } = res;
      function next() {
        if (validation === 'auto') {
          // User is auto validated, he can go to console home right now
          return;
        }
        if (validation === 'manual') {
          // User needs to wait for a super admin to validate its account.
          push(
            `/validate?${new URLSearchParams({
              email: email,
              manual: 'true',
            }).toString()}`,
          );
          return;
        }
        // User must validate his account from its email.
        push(
          `/validate?${new URLSearchParams({
            email: email,
            sent: 'true',
          }).toString()}`,
        );
      }
      if (onSignup) return onSignup(user, next);
      return next();
    },
    [redirect, signup, language, onSignup, push],
  );

  const validate = (values: Values) => {
    const errors: Partial<Record<keyof Values, string>> = {};
    if (!values.cgu) {
      errors.cgu = 'required';
    }
    if (!values.email) {
      errors.email = 'required';
    }
    if (!values.password) {
      errors.password = 'required';
    }
    if (!values.firstName) {
      errors.firstName = 'required';
    }
    if (!values.lastName) {
      errors.lastName = 'required';
    }
    return errors;
  };

  if (!hasLocalSignup) {
    return <div>{t('up.unavailable')}</div>;
  }

  return (
    <Form onSubmit={submit} validate={validate}>
      {({ handleSubmit }) => (
        <form onSubmit={handleSubmit} className={styles['signin-form']}>
          <div className={styles['signin-form-local']}>
            <Field name="firstName">
              {({ input: { type, ...inputProps }, className }) => {
                void type;
                return (
                  <Input
                    placeholder={t('up.firstName')}
                    className={`${className} ${styles['signin-form-local-input']}`}
                    {...inputProps}
                  />
                );
              }}
            </Field>
            <Field name="lastName">
              {({ input: { type, ...inputProps }, className }) => {
                void type;
                return (
                  <Input
                    placeholder={t('up.lastName')}
                    className={`${className} ${styles['signin-form-local-input']}`}
                    {...inputProps}
                  />
                );
              }}
            </Field>
            <Field name="email" initialValue={Storage.get('__email')}>
              {({ input: { type, ...inputProps }, className }) => {
                void type;
                return (
                  <Input
                    placeholder={t('up.email')}
                    className={`${className} ${styles['signin-form-local-input']}`}
                    {...inputProps}
                  />
                );
              }}
            </Field>
            <Field name="password">
              {({ input: { type, ...inputProps }, className }) => {
                void type;
                return (
                  <Input
                    placeholder={t('up.password')}
                    className={`${className} ${styles['signin-form-local-input']}`}
                    type="password"
                    {...inputProps}
                  />
                );
              }}
            </Field>
            <Field name="cgu" className="text-xs">
              {({ input, meta }) => (
                <div>
                  <Checkbox
                    checked={input.value}
                    onChange={({ target: { checked } }) => input.onChange(checked)}
                  >
                    <span className={isFormFieldValid(meta) ? 'text-error' : ''}>
                      {t.rich('up.cgu', {
                        a: (children: ReactNode) => (
                          <Link
                            href="https://www.prisme.ai/mentions-legales"
                            className="pr-link"
                            target="_blank"
                            rel="noreferrer"
                          >
                            {children}
                          </Link>
                        ),
                      })}
                    </span>
                  </Checkbox>
                </div>
              )}
            </Field>
          </div>
          <Button
            type="primary"
            disabled={loading}
            className={styles['signin-form-local-button']}
            htmlType="submit"
          >
            {t('up.submit')}
          </Button>
        </form>
      )}
    </Form>
  );
};
export default SignupForm;
