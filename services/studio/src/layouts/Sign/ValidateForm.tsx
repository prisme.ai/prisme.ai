import { useUser } from '@/providers/User';
import { Button } from 'antd';
import { useLocale, useTranslations } from 'next-intl';
import { useCallback, useEffect, useRef, useState } from 'react';

interface ValidateFormProps {
  email: string;
  sent: boolean;
}

export const ValidateForm = ({
  email: defaultEmail = '',
  sent: alreadySent = false,
}: ValidateFormProps) => {
  const timerRef: { current: NodeJS.Timeout | void } = useRef();
  const [counter, setCounter] = useState(0);

  const t = useTranslations('sign');
  const language = useLocale();
  const { sendValidationMail, loading } = useUser();

  const enableButtonDelayed = useCallback(() => {
    setCounter(30); // We make a user wait 30s before being able to resend a validation email.
  }, []);

  useEffect(() => {
    if (alreadySent) {
      enableButtonDelayed();
    }
    return () => {
      if (timerRef.current) clearTimeout(timerRef.current);
    };
  }, [enableButtonDelayed, alreadySent]);

  useEffect(() => {
    if (counter > 0) {
      timerRef.current = setTimeout(() => setCounter(counter - 1), 1000);
    }
  }, [counter]);

  const submit = useCallback(async () => {
    await sendValidationMail(defaultEmail, language);
    enableButtonDelayed();
  }, [language, defaultEmail, sendValidationMail, enableButtonDelayed]);

  return (
    <>
      <div className="flex flex-col items-center text-center md:w-96">
        {t(`validate.emailSentAt`)}
      </div>
      <div className="flex flex-col items-center text-center font-bold md:w-96">{defaultEmail}</div>
      <div className="mb-4 flex flex-col items-center text-center md:w-96">
        {t('validate.followInstructions')}
      </div>
      <Button
        type="primary"
        disabled={loading || !!counter}
        className="!mb-4 flex !h-12 !font-bold md:w-96"
        htmlType="submit"
        onClick={submit}
      >
        {t('validate.submit')}
        {counter ? ` (${counter}s...)` : ''}
      </Button>
    </>
  );
};
export default ValidateForm;
