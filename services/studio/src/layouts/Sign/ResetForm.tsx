import useTranslations from '@/i18n/useTranslations';
import { OperationSuccess, useUser } from '@/providers/User';
import { Button, Input } from 'antd';
import { useCallback, useEffect } from 'react';
import { Field, Form } from 'react-final-form';
import styles from './sign.module.scss';
import { useNotifications } from '@/providers/Notifications';
import { useRouter } from 'next/navigation';

interface Values {
  password: string;
  confirm: string;
}

interface ResetFormProps {
  token: string;
}

export default function ResetForm({ token }: ResetFormProps) {
  const t = useTranslations('sign');
  const notification = useNotifications();
  const { push } = useRouter();
  const { passwordReset, loading, success: { type: successType = '' } = {}, error } = useUser();

  const submit = useCallback(
    async ({ password }: Values) => {
      await passwordReset(token, password);
    },
    [passwordReset, token],
  );

  const validate = (values: Values) => {
    const errors: Partial<Values> = {};
    if (!values.password) {
      errors.password = 'required';
    }
    if (!values.confirm) {
      errors.confirm = 'required';
    } else if (values.password !== values.confirm) {
      errors.confirm = 'must match';
    }
    return errors;
  };

  useEffect(() => {
    if (!error) return;
    notification.error({
      message: t('reset.error', {
        context: error.error,
      }),
      placement: 'bottomRight',
      onClose() {
        push('/forgot');
      },
    });
  }, [error, notification, push, t]);

  return (
    <>
      <Form onSubmit={submit} validate={validate}>
        {({ handleSubmit }) => (
          <form onSubmit={handleSubmit} className={styles['form']}>
            <Field name="password">
              {({ input: { type, ...inputProps } }) => {
                void type;
                return (
                  <div className={styles['form-field']}>
                    <Input
                      placeholder={t('reset.password')}
                      className={styles['form-input']}
                      type={'password' as any}
                      {...inputProps}
                    />
                  </div>
                );
              }}
            </Field>
            <Field name="confirm">
              {({ input: { type, ...inputProps } }) => {
                void type;
                return (
                  <div className={styles['form-field']}>
                    <Input
                      placeholder={t('reset.confirm')}
                      className={styles['form-input']}
                      type={'password' as any}
                      {...inputProps}
                    />
                  </div>
                );
              }}
            </Field>
            <div className={styles['form-field']}>
              <Button
                type="primary"
                disabled={loading || successType === OperationSuccess.passwordReset}
                className={styles['form-button']}
                htmlType="submit"
              >
                {t('reset.submit')}
              </Button>
            </div>
          </form>
        )}
      </Form>
      {successType === OperationSuccess.passwordReset
        ? t('forgot.success', { context: successType })
        : null}
    </>
  );
}
