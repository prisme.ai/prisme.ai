'use client';

import MainLogo from '@/svgs/main-logo.svg';
import { useTranslations } from 'next-intl';
import { UserProvider } from '@/providers/User';
import { useCustomization } from '@/providers/Customization';
import styles from './sign.module.scss';

export const SignLayout = ({ children }: { children?: React.ReactNode }) => {
  const t = useTranslations('sign');
  const { mainLogo } = useCustomization();

  return (
    <UserProvider>
      <div className={styles['sign-layout']}>
        <div className={styles['sign-layout-left']}>
          <div className={styles['sign-layout-logo-ctn']}>
            {mainLogo ? (
              <img src={mainLogo} alt="Prisme.ai" className={styles['sign-layout-logo']} />
            ) : (
              <MainLogo
                alt="Prisme.ai"
                width={120}
                height={120}
                className={styles['sign-layout-logo']}
                color="white"
              />
            )}
          </div>
          <div className={styles['sign-layout-title']}>
            {t.rich('in.header', {
              br: () => <br />,
            })}
          </div>
        </div>
        <div className={styles['sign-layout-right']}>{children}</div>
      </div>
    </UserProvider>
  );
};

export default SignLayout;
