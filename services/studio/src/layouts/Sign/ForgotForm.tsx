import Field from '@/components/Field';
import { OperationSuccess, useUser } from '@/providers/User';
import { Button, Input } from 'antd';
import { useLocale, useTranslations } from 'next-intl';
import Link from 'next/link';
import { useCallback, useEffect } from 'react';
import { Form } from 'react-final-form';
import { useNotifications } from '@/providers/Notifications';
import styles from './sign.module.scss';

interface Values {
  email: string;
  password: string;
}

export const ForgotForm = () => {
  const t = useTranslations('sign');
  const language = useLocale();
  const { sendPasswordResetMail, loading, success: { type: successType = '' } = {} } = useUser();
  const notification = useNotifications();

  const submit = useCallback(
    async ({ email }: Values) => {
      await sendPasswordResetMail(email, language);
    },
    [language, sendPasswordResetMail],
  );

  const validate = (values: Values) => {
    const errors: Partial<Values> = {};
    if (!values.email) {
      errors.email = 'required';
    }
    return errors;
  };

  useEffect(() => {
    if (successType === OperationSuccess.emailSent) {
      notification.success({
        message: t(`forgot.success_${successType}`),
        placement: 'bottomRight',
      });
    }
  }, [successType, notification, t]);

  return (
    <>
      <Form onSubmit={submit} validate={validate}>
        {({ handleSubmit }) => (
          <form onSubmit={handleSubmit} className={styles['forgot-form']}>
            <Field name="email" containerClassName={styles['forgot-form-field']}>
              {({ input: { type, ...inputProps }, className }) => {
                void type;
                return (
                  <Input
                    placeholder={t('forgot.email')}
                    className={`${className} ${styles['forgot-form-input']}`}
                    {...inputProps}
                  />
                );
              }}
            </Field>
            <Button
              type="primary"
              disabled={loading || successType === OperationSuccess.emailSent}
              className={styles['forgot-form-button']}
              htmlType="submit"
            >
              {t('forgot.submit')}
            </Button>
            <div className={styles['signin-link']}>
              {t.rich('forgot.signin', {
                url: '/signin',
                a: (children) => (
                  <Link href={`signin`} className="pr-link">
                    {children}
                  </Link>
                ),
              })}
            </div>
          </form>
        )}
      </Form>
    </>
  );
};
export default ForgotForm;
