'use client';

import { ReactNode } from 'react';
import { useTranslations } from 'next-intl';
import Link from 'next/link';
import { useEnv } from '@/providers/Env';
import { SignType } from './types';
import styles from './sign.module.scss';
import { useUser } from '@/providers/User';
import { redirect } from 'next/navigation';

interface AuthProvider {
  name: string;
  extends?: string;
  label?: Prismeai.LocalizedText;
  icon?: string;
  url?: string;
}

function getLink(type: SignType) {
  switch (type) {
    case SignType.In:
    case SignType.Validate:
      return 'signup';
    default:
      return 'signin';
  }
}

export const SignContent = ({ children, type }: { children: ReactNode; type: SignType }) => {
  const t = useTranslations('sign');
  const { user } = useUser();
  if (user && user.authData && !user.authData.anonymous) {
    redirect('/');
  }
  const config = useEnv<{
    ENABLED_AUTH_PROVIDERS: AuthProvider[];
  }>();
  const ENABLED_AUTH_PROVIDERS = config.ENABLED_AUTH_PROVIDERS || [{ name: 'local' }];

  const hasLocalSignup = ENABLED_AUTH_PROVIDERS.some(({ name }) => name === 'local');

  return (
    <>
      <div className={styles['sign-layout-headline']}>{t(`${type}.topForm1`)}</div>
      <h2 className={styles['sign-layout-sign-title']}>{t(`${type}.topForm2`)}</h2>
      {hasLocalSignup && (
        <div className={styles['sign-layout-local-sign-form-ctn']}>
          {t.rich(`${type}.topForm3`, {
            a: (children: ReactNode) => (
              <Link href={getLink(type)} className="pr-link">
                {children}
              </Link>
            ),
          })}
        </div>
      )}
      <div className={styles['sign-layout-content']}>{children}</div>
    </>
  );
};
