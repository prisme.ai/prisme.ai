'use client';

import { ReactNode } from 'react';
import { WorkspacesProvider } from '../../providers/Workspaces';
import PermissionsProvider from '../../providers/Permissions';
import AppsProvider from '../../providers/Apps';
import Tracking from '../../providers/Tracking';
import InstallWorkspace from './InstallWorkspace';
import Notifications from '@/providers/Notifications';
import ModalProvider from '@/providers/Modal';

interface WorkspaceBuilderProps {
  children: ReactNode;
}

export const WorkspaceBuilder = ({ children }: WorkspaceBuilderProps) => {
  return (
    <Notifications>
      <ModalProvider>
        <WorkspacesProvider>
          <PermissionsProvider>
            <AppsProvider>
              <Tracking>
                <InstallWorkspace>{children}</InstallWorkspace>
              </Tracking>
            </AppsProvider>
          </PermissionsProvider>
        </WorkspacesProvider>
      </ModalProvider>
    </Notifications>
  );
};
export default WorkspaceBuilder;
