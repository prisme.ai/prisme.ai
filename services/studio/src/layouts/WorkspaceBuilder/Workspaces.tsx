import { DragEvent, useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { useWorkspaces } from '@/providers/Workspaces';
import { cleanSearch, removeEmpty, search } from '@/utils/filters';
import { Events, Workspace } from '@prisme.ai/sdk';
import { useTracking } from '@/providers/Tracking';
import Link from 'next/link';
import useLocalizedText from '@/utils/useLocalizedText';
import PlusIcon from '@/svgs/plus.svg';
import WorkspaceIcon from '@/svgs/workspace-simple.svg';
import ThreeDotsIcon from '@/svgs/three-dots.svg';
import ImportIcon from '@/svgs/import.svg';
import CopyIcon from '@/svgs/copy.svg';
import TrashIcon from '@/svgs/trash.svg';
import { Dropdown, notification, Tooltip } from 'antd';
import ConfirmButton from '@/components/ConfirmButton';
import { ItemType } from 'antd/es/menu/interface';
import Loading from '@/components/Loading';
import ProductHome from '@/blocks/blocks/ProductHome';
import { BlockProvider } from '@/blocks/Provider';
import BuilderIcon from '@/svgs/products/icon-ai-builder.svg';
import { Icon } from '@/blocks/blocks/Icon';
import styles from './workspaces.module.scss';
import { useEnv } from '@/providers/Env';
import useTranslations from '@/i18n/useTranslations';
import Head from '@/components/Head';
import useRouter from '@/utils/useRouter';

const MenuInCard = ({
  items,
  color = 'text-main-element-text',
  isOpen,
  setIsOpen,
}: {
  items: ItemType[];
  color?: string;
  isOpen: boolean;
  setIsOpen: (isOpen: boolean) => void;
}) => {
  return (
    <div onClick={(e) => e.stopPropagation()}>
      <Dropdown
        menu={{
          items,
        }}
        trigger={['click']}
        className={styles['workspace-menu-ctn']}
        open={isOpen}
        onOpenChange={setIsOpen}
      >
        <button onClick={(e) => e.preventDefault()} className={styles['workspace-menu']}>
          <ThreeDotsIcon className={color} />
        </button>
      </Dropdown>
    </div>
  );
};

export const WorkspacesView = () => {
  const { SUGGESTIONS_ENDPOINT = '' } = useEnv<{ SUGGESTIONS_ENDPOINT: string }>();
  const t = useTranslations('workspaces');
  const { localize } = useLocalizedText();
  const [searchValue, setSearchValue] = useState('');
  const { trackEvent } = useTracking();
  const [menuIsOpen, setMenuIsOpen] = useState<Map<string, boolean>>(new Map());

  const setIsOpen = useCallback(
    (key: string) => (isOpen: boolean) => {
      setMenuIsOpen((prev) => {
        const newMenus = new Map(prev);
        newMenus.set(key, isOpen);
        return newMenus;
      });
    },
    [],
  );

  const { push } = useRouter();
  const {
    workspaces,
    fetchWorkspaces,
    duplicateWorkspace,
    duplicating,
    importArchive,
    importing,
    deleteWorkspace,
  } = useWorkspaces();

  useEffect(() => {
    fetchWorkspaces();
  }, [fetchWorkspaces]);

  const filteredWorkspaces = useMemo(
    () =>
      workspaces
        .filter(removeEmpty)
        .sort(({ updatedAt: A = 0 }, { updatedAt: B = 0 }) => +new Date(B) - +new Date(A))
        .filter(({ name, description }) =>
          search(cleanSearch(searchValue))(`${name} ${description}`),
        ),
    [searchValue, workspaces],
  );

  const [suggestions, setSuggestions] = useState<Workspace[]>([]);
  useEffect(() => {
    if (!SUGGESTIONS_ENDPOINT) return;
    async function fetchSuggestions() {
      try {
        const res = await fetch(SUGGESTIONS_ENDPOINT, {
          credentials: 'include',
        });
        if (!res.ok) {
          throw new Error();
        }
        const suggestions: Workspace[] = await res.json();
        if (!Array.isArray(suggestions)) return;
        setSuggestions(suggestions.filter(({ id, name }) => id && name));
      } catch (e) {
        return;
      }
    }
    fetchSuggestions();
  }, [SUGGESTIONS_ENDPOINT]);

  const events = useMemo(
    () =>
      ({
        emit: (event: string, payload: Record<string, any>) => {
          switch (event) {
            case 'search workspaces':
              setSearchValue(payload.search);
              break;
            case 'create workspace':
              push('/workspaces/new');
              break;
            default:
              console.log(event, payload);
          }
        },
      }) as unknown as Events,
    [push],
  );

  const filteredSuggestions = useMemo(
    () =>
      suggestions.filter(({ name, description }) =>
        search(cleanSearch(searchValue))(`${name} ${description}`),
      ),
    [searchValue, suggestions],
  );

  const handleDuplicateWorkspace = useCallback(
    async (id: Workspace['id'], type?: 'suggestion') => {
      const workspace = await duplicateWorkspace(id);
      if (!workspace) return;
      trackEvent({
        name: type === 'suggestion' ? 'Install Workspace suggestion' : 'Duplicate Workspace',
        category: 'Workspaces',
        action: 'click',
      });
      push(`/workspaces/${workspace.id}`);
    },
    [duplicateWorkspace, push, trackEvent],
  );

  const handleImportArchive = useCallback(
    async (file: File, workspaceId?: string) => {
      const workspace = await importArchive(file, workspaceId);
      if (!workspace) return;
      push(`/workspaces/${workspace.id}`);
    },
    [importArchive, push],
  );

  const handlePickArchive = useCallback(
    async (workspaceId?: string) => {
      trackEvent({
        name: 'Import Archive',
        category: 'Workspaces',
        action: 'click',
      });
      const filePickr = document.createElement('input');
      filePickr.setAttribute('type', 'file');
      filePickr.setAttribute('accept', '.zip');
      document.body.appendChild(filePickr);
      filePickr.addEventListener('change', async (e: any) => {
        filePickr.parentNode?.removeChild(filePickr);
        const { files } = e.target as HTMLInputElement;
        if (!files) return;
        await handleImportArchive(files[0], workspaceId);
        if (workspaceId) {
          setIsOpen(workspaceId)(false);
        }
      });
      filePickr.addEventListener('cancel', () => {
        filePickr.parentNode?.removeChild(filePickr);
      });
      filePickr.click();
    },
    [handleImportArchive, setIsOpen, trackEvent],
  );

  const [dropped, setDropped] = useState(false);
  const handleDroppingArchive = useCallback(
    async (e: DragEvent<HTMLDivElement>) => {
      e.preventDefault();
      e.stopPropagation();
      setDropped(true);
      const file = Array.from(e.dataTransfer.items)[0].getAsFile();
      if (file) {
        await handleImportArchive(file);
      }
    },
    [handleImportArchive],
  );
  useEffect(() => {
    if (dropped && !importing) {
      setDropped(false);
    }
  }, [dropped, importing]);

  const handleDeleteWorkspace = useCallback(
    (workspaceId: string) => {
      deleteWorkspace(workspaceId);
      notification.success({
        message: t('workspace.delete.toast'),
        placement: 'bottomRight',
      });
      setIsOpen(workspaceId)(false);
    },
    [deleteWorkspace, setIsOpen, t],
  );

  const getWorkspaceMenu = useCallback(
    (workspaceId: string): ItemType[] => [
      {
        key: 'duplicate',
        label: (
          <button
            onClick={(e) => {
              e.stopPropagation();
              if (duplicating.has(workspaceId)) return;
              handleDuplicateWorkspace(workspaceId);
            }}
            className={styles['workspace-menu-btn']}
          >
            {duplicating.has(workspaceId) ? (
              <span>
                <Loading />
              </span>
            ) : (
              <CopyIcon />
            )}
            <span className={styles['workspace-menu-btn-label']}>
              {t('workspace.duplicate.label')}
            </span>
          </button>
        ),
      },
      {
        key: 'import',
        label: (
          <ConfirmButton
            confirmLabel={t('workspace.import.confirm')}
            onConfirm={() => handlePickArchive(workspaceId)}
            onClick={(e) => {
              e.stopPropagation();
            }}
            className={styles['workspace-menu-btn']}
            ButtonComponent="button"
          >
            {importing ? <Loading /> : <ImportIcon />}
            <span className={styles['workspace-menu-btn-label']}>
              {t('workspace.import.replace')}
            </span>
          </ConfirmButton>
        ),
      },
      {
        key: 'delete',
        label: (
          <ConfirmButton
            confirmLabel={t('workspace.delete.confirm')}
            onConfirm={() => handleDeleteWorkspace(workspaceId)}
            onClick={(e) => {
              e.stopPropagation();
            }}
            className={styles['workspace-menu-btn']}
            ButtonComponent="button"
          >
            <TrashIcon />
            <span className={styles['workspace-menu-btn-label']}>
              {t('workspace.delete.label')}
            </span>
          </ConfirmButton>
        ),
      },
    ],
    [duplicating, handleDeleteWorkspace, handleDuplicateWorkspace, handlePickArchive, importing, t],
  );
  const createMenu: ItemType[] = useMemo(
    () => [
      {
        key: 'create',
        label: (
          <Link href="/workspaces/new" className={styles['workspace-menu-btn']}>
            <PlusIcon />
            <span className={styles['workspace-menu-btn-label']}>
              {t('workspace.create.label')}
            </span>
          </Link>
        ),
      },
      {
        key: 'import',
        label: (
          <button
            onClick={(e) => {
              e.stopPropagation();
              if (importing) return;
              handlePickArchive();
            }}
            className={styles['workspace-menu-btn']}
          >
            {importing ? <Loading /> : <ImportIcon />}
            <span className={styles['workspace-menu-btn-label']}>
              {t('workspace.import.label')}
            </span>
          </button>
        ),
      },
    ],
    [handlePickArchive, importing, t],
  );
  const ctnEl = useRef<HTMLDivElement>(null);

  return (
    <div
      ref={ctnEl}
      className={`${styles['workspaces']} ${dropped ? styles['workspaces--importing'] : ''}`}
      onDragLeave={() => {
        ctnEl.current?.classList.remove(styles['workspaces--dragging']);
      }}
      onDragOver={(e) => {
        ctnEl.current?.classList.add(styles['workspaces--dragging']);
        e.preventDefault();
        e.stopPropagation();
      }}
      onDrop={handleDroppingArchive}
    >
      <Head title={t('workspaces.title')} description={t('workspaces.description')} />
      <BlockProvider
        events={events}
        config={{
          heading: {
            title: t('workspaces.welcome.title'),
            icon: <BuilderIcon width="40px" height="40px" />,
            description: t('workspaces.welcome.description'),
          },
          search: {
            onChange: 'search workspaces',
            onSubmit: 'search workspaces',
            placeholder: t('workspaces.search'),
          },
          title: t('workspaces.sectionTitle'),
          list: {
            items: filteredWorkspaces.map(
              ({ id, name, description, photo = <WorkspaceIcon /> }) => ({
                type: 'internal',
                value: `/workspaces/${id}`,
                id,
                text: localize(name),
                description: localize(description),
                icon: photo,
                after: (
                  <MenuInCard
                    items={getWorkspaceMenu(id)}
                    isOpen={!!menuIsOpen.get(id)}
                    setIsOpen={setIsOpen(id)}
                  />
                ),
              }),
            ),
            create: {
              type: 'event',
              value: 'create workspace',
              text: t('create.label'),
              before: (
                <MenuInCard
                  items={createMenu}
                  color="text-main-text"
                  isOpen={!!menuIsOpen.get('new')}
                  setIsOpen={setIsOpen('new')}
                />
              ),
            },
          },
          before: (
            <div className={styles['product-home-before']}>
              <div className="product-home-title">{t('workspaces.suggestions.title')}</div>
              <div className="product-home-list">
                {filteredSuggestions.map(({ id, name, description, photo = <WorkspaceIcon /> }) => (
                  <div key={id} className="product-home-list-item">
                    <div className="product-home-list-item-icon">
                      <Icon icon={photo} />
                    </div>
                    <div className="product-home-list-texts">
                      <div className="product-home-list-name">{localize(name)}</div>
                      <div className="product-home-list-description">{localize(description)}</div>
                    </div>
                    <Tooltip
                      title={
                        duplicating.has(id)
                          ? t('workspace.duplicate.duplicating')
                          : t('workspace.duplicate.label')
                      }
                      placement="left"
                    >
                      <button
                        className={styles['product-home-before-menu']}
                        type="button"
                        onClick={(e) => {
                          e.stopPropagation();
                          if (duplicating.has(id)) return;
                          handleDuplicateWorkspace(id);
                        }}
                      >
                        {duplicating.has(id) ? (
                          <Loading className="!text-main-element-text [&>svg]:h-[24px] [&>svg]:w-[24px]" />
                        ) : (
                          <CopyIcon className="text-main-element-text" />
                        )}
                      </button>
                    </Tooltip>
                  </div>
                ))}
              </div>
            </div>
          ),
        }}
      >
        <ProductHome />
      </BlockProvider>
    </div>
  );
};

export default WorkspacesView;
