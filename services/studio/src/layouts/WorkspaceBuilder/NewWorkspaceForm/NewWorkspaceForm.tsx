import SchemaForm, { Schema } from '@/components/SchemaForm';
import { useNotifications } from '@/providers/Notifications';
import { useTracking } from '@/providers/Tracking';
import { useUser } from '@/providers/User';
import { useWorkspaces } from '@/providers/Workspaces';
import { Button, ButtonProps } from 'antd';
import { useTranslations } from 'next-intl';
import { useCallback, useMemo } from 'react';
import { useForm } from 'react-final-form';
import Link from 'next/link';
import ArrowIcon from '@/svgs/arrow-up.svg';
import styles from './new-workspace-form.module.scss';
import useRouter from '@/utils/useRouter';

const SubmitButton = ({ ...props }: ButtonProps) => {
  const { getState } = useForm();
  const { hasValidationErrors } = getState();
  const { creating } = useWorkspaces();

  return (
    <Button
      type="primary"
      htmlType="submit"
      disabled={hasValidationErrors || creating}
      className="mt-[50px] !h-auto self-center px-[30px] py-[10px] !text-[20px] !font-bold font-bold"
      data-testid="create-workspace-submit-button"
      {...props}
    />
  );
};

export default function NewWorkspaceForm() {
  const t = useTranslations('workspaces');
  const { createWorkspace, creating } = useWorkspaces();
  const { replace } = useRouter();
  const { user } = useUser();
  const { trackEvent } = useTracking();
  const notification = useNotifications();

  const schema = useMemo(
    () =>
      ({
        type: 'object',
        properties: {
          name: {
            type: 'localized:string',
            title: t('workspace.create.name'),
            placeholder: t('workspace.create.name'),
            validators: {
              required: true,
            },
          } as Schema,
          description: {
            type: 'localized:string',
            title: t('workspace.create.description'),
            placeholder: t('workspace.create.description'),
            'ui:widget': 'textarea',
            validators: {
              required: true,
            },
          } as Schema,
          photo: {
            type: 'string',
            title: t('workspace.create.photo'),
            'ui:widget': 'upload',
            validators: {
              required: true,
            },
          } as Schema,
        },
      }) as Schema,
    [t],
  );

  const submit = useCallback(
    async (values: any) => {
      if (creating) return;
      try {
        const created = await createWorkspace(values);
        trackEvent({
          name: 'Create new Workspace',
          category: 'Workspaces',
          action: 'click',
          value: {
            workspaceId: created.id,
            workspace: created,
            userId: user?.id,
          },
        });
        replace(`/workspaces/${created.id}`);
      } catch (err) {
        notification.error({
          message:
            (err as any)?.error === 'InvalidFile' && (err as any)?.details?.maxSize
              ? t('InvalidFileError', {
                  ns: 'errors',
                  type: 'image',
                  maxSize: (err as any)?.details?.maxSize,
                })
              : t('unknown', {
                  ns: 'errors',
                  errorName: (err as any)?.error,
                }),
          placement: 'bottomRight',
        });
        return;
      }
    },
    [createWorkspace, creating, replace, t, trackEvent, user?.id, notification],
  );

  return (
    <div className={styles['products-single-view']}>
      <h2 className={styles['products-single-view-title-ctn']}>
        <Link href="/workspaces" className={styles['products-single-view-title-back-btn']}>
          <button>
            <ArrowIcon className={styles['products-single-view-title-back-btn-icon']} />
          </button>
        </Link>
        <div className={styles['products-single-view-title']}>{t('create.label')}</div>
      </h2>
      <div className={styles['products-single-view-content-ctn']}>
        <div className={styles['products-single-view-content']}>
          <SchemaForm
            schema={schema}
            buttons={[<SubmitButton key="submit">{t('workspace.create.submit')}</SubmitButton>]}
            onSubmit={submit}
            autoFocus
          />
        </div>
      </div>
    </div>
  );
}
