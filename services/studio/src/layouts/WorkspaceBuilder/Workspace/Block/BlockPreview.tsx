import { Block } from '@/providers/Block';
import { useWorkspace } from '@/providers/Workspace';
import {
  createContext,
  ReactNode,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import { getDefaults } from './getDefaults';
import Storage from '@/utils/Storage';
import useLocalizedText from '@/utils/useLocalizedText';
import SchemaForm, { Schema } from '@/components/SchemaForm';
import BlocksListEditorProvider from '@/components/BlocksListEditor/BlocksListEditorProvider';
import { useTranslations } from 'next-intl';
import { Tooltip } from 'antd';
import { useEnv } from '@/providers/Env';
import MobileIcon from '@/svgs/mobile.svg';
import TabletIcon from '@/svgs/tablet.svg';
import DesktopIcon from '@/svgs/desktop.svg';
import IFrame from '@/components/IFrame';
import styles from './block.module.scss';

interface BlockPreviewContext {
  reload: () => void;
  mounted: boolean;
}

const blockPreviewContext = createContext<BlockPreviewContext | undefined>(undefined);
export const useBlockPreview = () => {
  const context = useContext(blockPreviewContext);
  if (!context) {
    throw new Error();
  }
  return context;
};

interface BlockPreviewProviderProps {
  children: ReactNode;
}
export const BlockPreviewProvider = ({ children }: BlockPreviewProviderProps) => {
  const [mounted, setMounted] = useState(true);
  const reload = useCallback(() => {
    setMounted(false);
    setTimeout(() => setMounted(true), 1);
  }, []);
  return (
    <blockPreviewContext.Provider value={{ mounted, reload }}>
      {children}
    </blockPreviewContext.Provider>
  );
};

type BlockPreviewProps = Block;

export const BlockPreview = ({ blocks, schema, css }: BlockPreviewProps) => {
  const { PAGES_HOST = `${global?.location?.origin}/pages` } = useEnv<{ PAGES_HOST: string }>();
  const {
    workspace: { id: wId, slug },
  } = useWorkspace();
  const iframeRef = useRef<HTMLIFrameElement>(null);
  const storageKey = `__block_${wId}_${slug}`;
  const [values, setValues] = useState(Storage.get(storageKey));
  const t = useTranslations('workspaces');
  const { workspace } = useWorkspace();
  const [width, setWidth] = useState<'full' | 'tablet' | 'mobile'>('full');
  const { mounted } = useBlockPreview();
  const { localizeSchemaForm } = useLocalizedText();

  useEffect(() => {
    Storage.set(storageKey, values);
  }, [storageKey, values]);

  const update = useCallback(() => {
    if (!iframeRef.current) return;

    const appInstances = Object.entries(workspace.imports || {}).reduce<
      { slug: string; blocks: {} }[]
    >(
      (prev, [slug, { blocks = [] }]) => [
        ...prev,
        {
          slug,
          blocks: blocks.reduce((prev, { slug, ...block }) => ({ ...prev, [slug]: block }), {}),
        },
      ],
      [],
    );

    iframeRef.current?.contentWindow?.postMessage(
      {
        type: 'previewblock.update',
        page: {
          appInstances: [{ slug: '', blocks: workspace.blocks }, ...appInstances],
        },
        config: {
          blocks: blocks,
          css,
          ...getDefaults(schema as Schema),
          ...values,
        },
      },
      '*',
    );
  }, [blocks, css, schema, values, workspace]);

  useEffect(() => {
    update();
  }, [update]);

  useEffect(() => {
    const listener = (e: MessageEvent) => {
      const { type } = e.data || {};
      if (type === 'previewblock:init') {
        update();
      }
    };
    window.addEventListener('message', listener);
    return () => {
      window.removeEventListener('message', listener);
    };
  }, [update]);

  const cleanedSchema = useMemo(() => {
    if (schema && schema.type === 'object' && !schema.title) {
      return { ...schema, title: t('blocks.preview.config.label') } as Schema;
    }
    if (!schema) return schema;
    return localizeSchemaForm(schema);
  }, [localizeSchemaForm, schema, t]);

  const [formMounted, setFormMounted] = useState(true);

  return (
    <div className={styles['preview-ctn']}>
      <div className={styles['preview-iframe-ctn']}>
        {mounted && (
          <IFrame
            ref={iframeRef}
            className={`${styles['preview-iframe']} ${styles[`preview-iframe--${width}`]}`}
            src={`${window.location.protocol}//${slug}${PAGES_HOST}/__preview/block`}
          />
        )}
      </div>
      <div className={styles['preview-tools']}>
        <div className={styles['preview-toolbar']}>
          <Tooltip title={t('blocks.preview.toggleWidth.desktop')}>
            <button
              onClick={() => {
                setWidth('full');
              }}
              className={`${styles['preview-toolbar-btn']} ${width === 'full' ? styles['preview-toolbar-btn--active'] : ''}`}
            >
              <DesktopIcon />
            </button>
          </Tooltip>
          <Tooltip title={t('blocks.preview.toggleWidth.tablet')}>
            <button
              onClick={() => {
                setWidth('tablet');
              }}
              className={`${styles['preview-toolbar-btn']} ${width === 'tablet' ? styles['preview-toolbar-btn--active'] : ''}`}
            >
              <TabletIcon />
            </button>
          </Tooltip>
          <Tooltip title={t('blocks.preview.toggleWidth.mobile')}>
            <button
              onClick={() => {
                setWidth('mobile');
              }}
              className={`${styles['preview-toolbar-btn']} ${width === 'mobile' ? styles['preview-toolbar-btn--active'] : ''}`}
            >
              <MobileIcon />
            </button>
          </Tooltip>
        </div>
        <div className={styles['preview-form']}>
          {cleanedSchema && cleanedSchema.type && formMounted && (
            <BlocksListEditorProvider>
              <SchemaForm
                initialFieldObjectVisibility={false}
                schema={cleanedSchema}
                onChange={setValues}
                buttons={[
                  <button
                    key="reset"
                    className="absolute bottom-2 right-4 text-sm"
                    onClick={async () => {
                      setFormMounted(false);
                      await setValues(getDefaults(schema as Schema));
                      setFormMounted(true);
                    }}
                  >
                    {t('blocks.preview.config.reset')}
                  </button>,
                ]}
                initialValues={values}
              />
            </BlocksListEditorProvider>
          )}
        </div>
      </div>
    </div>
  );
};

export default BlockPreview;
