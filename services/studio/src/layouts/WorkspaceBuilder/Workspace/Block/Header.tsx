import { Button, Segmented, Tooltip } from 'antd';
import { useTranslations } from 'next-intl';
import CodeIcon from '@/svgs/code.svg';
import CopyIcon from '@/svgs/copy.svg';
import ReloadIcon from '@/svgs/reload.svg';
import EyeIcon from '@/svgs/eye-open.svg';
import EditIcon from '@/svgs/edit.svg';
import EditDetails from './EditDetails';
import styles from './block.module.scss';
import Loading from '@/components/Loading';
import { Block } from '@/providers/Block';
import IconButton from '@/components/IconButton';
import SettingsIcon from '@/svgs/gear.svg';

interface HeaderLeftProps {
  value: Prismeai.Block;
  duplicate: () => void;
  duplicating: boolean;
  displaySource: boolean;
  onDisplaySource: (state: boolean) => void;
  onDelete: () => void;
  onChange: (v: Block) => void;
}

export const HeaderLeft = ({
  value,
  duplicate,
  duplicating,
  displaySource,
  onDisplaySource,
  onDelete,
  onChange,
}: HeaderLeftProps) => {
  const t = useTranslations();
  return (
    <>
      <Tooltip title={t('workspaces.pages.details.title')} placement="bottom">
        <EditDetails
          value={{
            ...value,
          }}
          onSave={async (v) => {
            onChange({
              ...value,
              ...v,
            });
          }}
          onDelete={onDelete}
          context="pages"
        >
          <IconButton icon={<SettingsIcon />} />
        </EditDetails>
      </Tooltip>

      <Tooltip title={t('workspaces.pages.duplicate.help')} placement="bottom">
        <IconButton
          icon={<CopyIcon width="1.2rem" height="1.2rem" />}
          className={styles['header-button']}
          onClick={duplicate}
          disabled={duplicating}
        >
          {t('common.duplicate')}
        </IconButton>
      </Tooltip>
      <Tooltip title={t('workspaces.pages.source.help')} placement="bottom">
        <IconButton
          icon={<CodeIcon width="1.2rem" height="1.2rem" />}
          className={styles['header-button']}
          onClick={() => onDisplaySource(!displaySource)}
        >
          {displaySource ? t('workspaces.pages.source.close') : t('workspaces.pages.source.label')}
        </IconButton>
      </Tooltip>
    </>
  );
};

interface HeaderRightProps {
  value: Prismeai.Block;
  onSave: () => void;
  reload: () => void;
  dirty: boolean;
  saving: boolean;
  viewMode: number;
  setViewMode: (mode: number) => void;
  error?: boolean;
}
export const HeaderRight = ({
  value,
  onSave,
  reload,
  dirty,
  saving,
  viewMode,
  setViewMode,
  error,
}: HeaderRightProps) => {
  const t = useTranslations('workspaces');
  return (
    <>
      <Button
        onClick={onSave}
        disabled={!dirty || saving || error}
        type="primary"
        className={styles['header-button']}
      >
        {saving && <Loading />}
        {t('blocks.save.label')}
      </Button>
      <Tooltip title={t('pages.reload')} placement="bottom">
        <Button onClick={reload} type="primary" className={styles['header-button']}>
          <ReloadIcon />
        </Button>
      </Tooltip>
      <Segmented
        className={styles['header-segmented']}
        key="nav"
        options={[
          {
            value: 0,
            icon: (
              <Tooltip title={t('blocks.preview.label')} placement="bottom">
                <EyeIcon />
              </Tooltip>
            ),
            disabled: (value?.blocks || []).length === 0,
          },
          {
            value: 1,
            icon: (
              <Tooltip title={t('blocks.edit')} placement="bottom">
                <EditIcon />
              </Tooltip>
            ),
          },
        ]}
        value={(value.blocks || []).length === 0 ? 1 : viewMode}
        onChange={(v) => setViewMode(+v)}
      />
    </>
  );
};
