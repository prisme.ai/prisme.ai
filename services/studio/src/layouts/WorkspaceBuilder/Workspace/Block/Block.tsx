import { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import useKeyboardShortcut from '@/utils/useKeyboardShortcut';
import { useWorkspace } from '@/providers/Workspace';
import { useTracking } from '@/providers/Tracking';
import { useBlock } from '@/providers/Block';
import useLocalizedText from '@/utils/useLocalizedText';
import SourceEdit from '@/components/SourceEdit';
import { replaceSilently } from '@/utils/urls';
import { ApiError } from '@prisme.ai/sdk';
import { incrementName } from '@/utils/incrementName';
import BlockPreview, { useBlockPreview } from './BlockPreview';
import { getBackTemplateDots, removeTemplateDots } from '@/utils/templatesInBlocks';
import { useWorkspaceLayout } from '../Layout/context';
import LowCodeEditor from '@/components/LowCodeEditor';
import { HeaderLeft, HeaderRight } from './Header';
import equal from 'fast-deep-equal';
import BlocksListEditor from '@/components/BlocksListEditor';
import styles from './block.module.scss';
import Head from '@/components/Head';
import useTranslations from '@/i18n/useTranslations';
import useRouter from '@/utils/useRouter';
import useWarningBeforeNavigate from '@/utils/useRouter/useWarningBeforeNavigate';
import { useBlocksSnippets } from '@/utils/useSnippets/useSnippets';
import { useBlocksLinks } from '@/utils/useLinks/useLinks';
import { useNotifications } from '@/providers/Notifications';
import { validateBlock } from '@prisme.ai/validation';
import deepClone from '@/utils/deepClone';
import { ValidationError } from '@/utils/useYaml/yaml';
import { YAMLException } from 'js-yaml';

const Block = () => {
  const t = useTranslations('workspaces');
  const { trackEvent } = useTracking();
  const { replace, push } = useRouter();
  const notification = useNotifications();
  const { localize } = useLocalizedText();
  const { block, saveBlock, deleteBlock } = useBlock();
  const {
    workspace: { id: workspaceId, blocks = {}, name: workspaceName },
    createBlock,
  } = useWorkspace();
  const { reload } = useBlockPreview();

  const [value, setValue] = useState<typeof block>(removeTemplateDots(block));

  const [viewMode, setViewMode] = useState((value?.blocks || []).length === 0 ? 1 : 0);

  const { setTitle } = useWorkspaceLayout();
  useEffect(() => {
    setTitle(
      t('blocks.title', {
        name: localize(block.name),
        slug: block.slug,
      }),
    );
  }, [localize, block.name, block.slug, setTitle, t]);

  const [dirty, setDirty] = useState(false);
  useWarningBeforeNavigate(dirty);

  const prevValue = useRef(value);
  useEffect(() => {
    if (equal(value, prevValue.current)) return;
    setDirty(true);
  }, [value]);

  const [saving, setSaving] = useState(false);
  const [duplicating, setDuplicating] = useState(false);
  const duplicate = useCallback(async () => {
    if (!block.slug) return;
    trackEvent({
      name: 'Duplicate Block',
      action: 'click',
    });
    setDuplicating(true);
    const newSlug = incrementName(
      block.slug,
      Object.keys(blocks).map((k) => k),
      '{{name}}-{{n}}',
      { keepOriginal: true },
    );
    try {
      await createBlock({
        ...block,
        slug: newSlug,
      });
      push(`/workspaces/${workspaceId}/blocks/${newSlug}`);
      setDuplicating(false);
      notification.success({
        message: t('blocks.duplicate.success'),
        placement: 'bottomRight',
      });
    } catch {
      notification.error({
        message: t('blocks.duplicate.error'),
        placement: 'bottomRight',
      });
    }
  }, [block, blocks, createBlock, push, t, trackEvent, workspaceId, notification]);

  useEffect(() => {
    setViewMode((block?.blocks || []).length === 0 ? 1 : 0);
    setValue(removeTemplateDots(block));
  }, [block]);

  const onDelete = useCallback(() => {
    trackEvent({
      name: 'Delete Page',
      action: 'click',
    });
    replace(`/workspaces/${workspaceId}`);
    deleteBlock();
    notification.success({
      message: t('blocks.builder.delete.toast'),
      placement: 'bottomRight',
    });
  }, [deleteBlock, replace, trackEvent, workspaceId, notification, t]);

  const save = useCallback(
    async (toSave: typeof block = value) => {
      setSaving(true);
      const prevSlug = block.slug;
      trackEvent({
        name: 'Save Block',
        action: 'click',
      });
      try {
        const saved = await saveBlock(getBackTemplateDots(toSave));
        if (!saved) return;
        prevValue.current = saved;
        setDirty(false);
        notification.success({
          message: t('blocks.save.toast'),
          placement: 'bottomRight',
        });
        const { slug: newSlug } = saved;
        if (newSlug !== prevSlug) {
          replaceSilently(`/workspaces/${workspaceId}/blocks/${newSlug}`);
        }
      } catch (e) {
        const { details, error } = e as ApiError;
        const description = (
          <ul>
            {details ? (
              details.map(({ path, message }: any, key: number) => (
                <li key={key}>
                  {t('openapi', {
                    context: message,
                    path: path.replace(/^\.body\./, ''),
                  })}
                </li>
              ))
            ) : (
              <li>
                {t.has(`blocks.save.reason_${error}`)
                  ? t(`blocks.save.reason_${error}`)
                  : t('blocks.save.reason')}
              </li>
            )}
          </ul>
        );
        notification.error({
          message: t('blocks.save.error'),
          description,
          placement: 'bottomRight',
        });
      }
      setSaving(false);
    },
    [block.slug, saveBlock, t, trackEvent, value, workspaceId, notification],
  );

  useKeyboardShortcut([
    {
      key: 's',
      meta: true,
      command: (e) => {
        e.preventDefault();
        trackEvent({
          name: 'Save Page with shortcut',
          action: 'keydown',
        });
        save();
      },
    },
  ]);

  // Need to get the latest version with the latest value associated
  const saveAfterChange = useRef(save);
  useEffect(() => {
    saveAfterChange.current = save;
  }, [save]);

  const [displaySource, setDisplaySource] = useState(false);

  const [validationError, setValidationError] = useState<ValidationError>();
  const validateSource = useCallback((json: any) => {
    const { slug, ...toValidate } = json;
    void slug;
    const isValid = validateBlock(toValidate);
    const [error] = validateBlock.errors || [];
    console.log(error);
    setValidationError(error as ValidationError);
    return isValid;
  }, []);
  useEffect(() => {
    if (validateSource(value)) {
      setValidationError(undefined);
    }
  }, [value, validateSource]);
  const [syntaxError, setSyntaxError] = useState<YAMLException | null>(null);

  const source = useMemo(() => {
    return getBackTemplateDots(value);
  }, [value]);

  const setSource = useCallback((source: any) => {
    setValue(getBackTemplateDots(deepClone(source)));
  }, []);

  const snippets = useBlocksSnippets();

  return (
    <>
      <Head
        title={`[${localize(workspaceName)}]
          ${t('page_title', {
            elementName: localize(value.name),
          })}`}
        description=""
      />
      <LowCodeEditor
        header={{
          title: {
            value: value.name || '',
            onChange: (name) =>
              setValue(
                getBackTemplateDots({
                  ...value,
                  name,
                }),
              ),
          },
          left: (
            <HeaderLeft
              value={value}
              duplicate={duplicate}
              duplicating={duplicating}
              displaySource={displaySource}
              onDisplaySource={setDisplaySource}
              onDelete={onDelete}
              onChange={(p) => {
                setValue(p);
                save(p);
              }}
            />
          ),
          right: (
            <HeaderRight
              value={value}
              onSave={() => save()}
              dirty={dirty}
              reload={reload}
              saving={saving}
              viewMode={viewMode}
              setViewMode={setViewMode}
              error={!!validationError || !!syntaxError}
            />
          ),
        }}
      >
        <BlockPreview
          slug={block.slug}
          schema={value.schema}
          blocks={getBackTemplateDots(value.blocks)}
          block={value.block}
        />
        <SourceEdit
          value={source}
          onChange={setSource}
          onSave={save}
          visible={displaySource}
          snippets={snippets}
          links={useBlocksLinks()}
          error={validationError}
          onError={setSyntaxError}
        />
        <div
          className={`${styles['block-builder-ctn']} ${viewMode === 1 ? styles['block-builder-ctn--visible'] : ''}`}
        >
          <BlocksListEditor
            value={value}
            onChange={({ blocks }) => {
              setValue((prev) => {
                return { ...prev, blocks };
              });
            }}
          />
        </div>
      </LowCodeEditor>
    </>
  );
};
export default Block;
