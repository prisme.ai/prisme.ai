import { ReactNode, useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { useTracking } from '@/providers/Tracking';
import useLocalizedText from '@/utils/useLocalizedText';
import CSSEditor from '@/components/CSSEditor/CSSEditor';
import ConfirmButton from '@/components/ConfirmButton';
import { Button, Popover, PopoverProps, Tabs } from 'antd';
import { useTranslations } from 'next-intl';
import SchemaForm, { FieldProps, Schema } from '@/components/SchemaForm';
import CloseIcon from '@/svgs/close.svg';
import TrashIcon from '@/svgs/trash.svg';
import CodeIcon from '@/svgs/code.svg';

interface EditDetailsprops extends Omit<PopoverProps, 'content'> {
  value: any;
  onSave: (values: any) => Promise<void | Record<string, string>>;
  onDelete: () => void;
  context?: string;
  disabled?: boolean;
  children: ReactNode;
}

export const EditDetails = ({
  value,
  onSave,
  onDelete,
  disabled,
  onOpenChange,
  children,
  ...props
}: EditDetailsprops) => {
  const t = useTranslations('workspaces');
  const { localize } = useLocalizedText();
  const { trackEvent } = useTracking();
  const [open, setOpen] = useState(false);
  const [values, setValues] = useState(value);

  const configSchema: Schema = useMemo(
    () => ({
      type: 'object',
      properties: {
        name: {
          type: 'localized:string',
          title: t('blocks.details.name.label'),
        },
        slug: {
          type: 'string',
          title: t('blocks.details.slug.label'),
        },
        description: {
          type: 'localized:string',
          title: t('blocks.details.description.label'),
          'ui:widget': 'textarea',
          'ui:options': { textarea: { rows: 10 } },
        },
      },
    }),
    [t],
  );
  const advancedSchema: Schema = useMemo(
    () => ({
      type: 'object',
      properties: {
        photo: {
          type: 'string',
          title: t('blocks.details.photo.label'),
          description: t('blocks.details.photo.description'),
          'ui:widget': 'upload',
          'ui:options': {
            upload: { accept: 'image/jpg,image/gif,image/png,image/svg' },
          },
        },
        url: {
          type: 'string',
          title: t('blocks.details.url.label'),
          description: t.rich('blocks.details.url.description', {
            a: (children) => <a target="_blank">{children}</a>,
          }),
          /*(
            <Trans
              t={t}
              i18nKey="blocks.details.url.description"
              components={{
                a: <a target="_blank" />,
              }}
            />
          )*/ 'ui:widget': 'upload',
          'ui:options': {
            upload: {
              accept: '.js',
              defaultPreview: <CodeIcon className="flex items-center text-4xl !text-gray-200" />,
            },
          },
        },
      },
    }),
    [t],
  );
  const stylesSchema: Schema = useMemo(
    () => ({
      type: 'object',
      properties: {
        css: {
          type: 'string',
          'ui:widget': (props: FieldProps) => <CSSEditor {...props} />,
        },
      },
    }),
    [],
  );

  const initialOpenState = useRef(false);
  useEffect(() => {
    if (!initialOpenState.current) {
      initialOpenState.current = true;
      return;
    }
    trackEvent({
      name: `${open ? 'Open' : 'Close'} Details Panel`,
      action: 'click',
    });
  }, [open, trackEvent]);

  const onChange = useCallback(
    (schema: Schema) => (changedValues: any) => {
      const newValues = { ...values };
      Object.keys(schema?.properties || {}).forEach((k) => {
        newValues[k] = changedValues[k];
      });
      setValues(newValues);
    },
    [values],
  );

  const submit = useCallback(() => {
    onSave(values);
  }, [onSave, values]);

  const buttons = useMemo(
    () => [
      <div key="1" className="mx-4 !mt-2 flex flex-1 justify-end">
        <Button type="primary" htmlType="submit" disabled={disabled}>
          {t('save', { ns: 'common' })}
        </Button>
      </div>,
    ],
    [disabled, t],
  );

  return (
    <Popover
      title={() => (
        <div className="flex flex-1 justify-between">
          {t('blocks.details.title')}
          <button
            onClick={() => {
              trackEvent({
                name: 'Close Details Panel by clicking button',
                action: 'click',
              });
              setOpen(false);
            }}
          >
            <CloseIcon />
          </button>
        </div>
      )}
      destroyTooltipOnHide
      content={() => (
        <Tabs
          className="flex flex-1"
          items={[
            {
              key: 'config',
              label: t('blocks.builder.setup.label'),
              children: (
                <SchemaForm
                  schema={configSchema}
                  initialValues={values}
                  onChange={onChange(configSchema)}
                  onSubmit={submit}
                  buttons={buttons}
                />
              ),
              active: true,
            },
            {
              key: 'advanced',
              label: t('blocks.builder.advanced.label'),
              children: (
                <SchemaForm
                  schema={advancedSchema}
                  initialValues={values}
                  onChange={onChange(advancedSchema)}
                  onSubmit={submit}
                  buttons={buttons}
                />
              ),
            },
            {
              key: 'styles',
              label: t('blocks.builder.style.label'),
              children: (
                <SchemaForm
                  schema={stylesSchema}
                  initialValues={values}
                  onChange={onChange(stylesSchema)}
                  onSubmit={submit}
                  buttons={buttons}
                />
              ),
            },
          ]}
          tabBarExtraContent={
            <ConfirmButton
              onConfirm={onDelete}
              confirmLabel={t('blocks.builder.delete.confirm', {
                name: localize(value.name),
              })}
            >
              <TrashIcon className="translate-y-[-2px]" />
              <span className="flex">{t('blocks.builder.delete.label')}</span>
            </ConfirmButton>
          }
        />
      )}
      rootClassName="min-w-[50%]"
      open={open}
      onOpenChange={(v) => {
        setOpen(v);
        onOpenChange?.(v);
      }}
      trigger={'click'}
      {...props}
    >
      {children}
    </Popover>
  );
};

export default EditDetails;
