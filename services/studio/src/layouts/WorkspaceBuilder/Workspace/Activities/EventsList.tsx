import { useMemo } from 'react';
import { EventsContext } from '@/providers/Events';
import { Event } from '@prisme.ai/sdk';
import { useDateFormat } from '@/utils/dates';
import useLocalizedText from '@/utils/useLocalizedText';
import SourceDetails from '@/components/SourceDetails';
import EventDetails from './EventDetails';
import SectionContent from './SectionContent';
import { Collapse } from 'antd';
import styles from './activities.module.scss';

interface EventsListProps {
  events: Set<Event<Date>>;
  read: EventsContext['read'];
  isRead: EventsContext['isRead'];
  workspaceName: string;
}

export const EventLabel = ({
  read,
  workspaceName,
  ...event
}: Event<Date> & { read: boolean; workspaceName: string }) => {
  const { localize } = useLocalizedText();
  const dateFormat = useDateFormat();

  return (
    <SourceDetails workspaceId={event.source.workspaceId} appSlug={event.source.appSlug}>
      <SectionContent
        title={event.source.appSlug || localize(workspaceName)}
        date={dateFormat(event.createdAt, {
          relative: true,
        })}
        type={event.type}
        read={read}
        event={event}
      />
    </SourceDetails>
  );
};

export const EventsList = ({ events, workspaceName, read, isRead }: EventsListProps) => {
  const dateFormat = useDateFormat();

  const sections = useMemo(() => {
    const byDate = Array.from(events).reduce<[string, Set<Event<Date>>][]>((prev, event) => {
      const eventDay =
        dateFormat(new Date(event.createdAt), {
          relative: true,
          withoutHour: true,
        }) || '';
      let prevDate = prev.find(([date]) => date === eventDay);
      if (!prevDate) {
        prevDate = [eventDay, new Set()];
        prev.push(prevDate);
      }
      prevDate[1].add(event);
      return prev;
    }, []);

    return byDate.map(([title, events]) => {
      return {
        key: title,
        title,
        content: (
          <Collapse
            className={styles['activities-feed-section']}
            items={Array.from(events).map((event) => ({
              key: event.id,
              label: (
                <EventLabel {...event} read={isRead(event.id)} workspaceName={workspaceName} />
              ),
              children: <EventDetails {...event} />,
            }))}
            onChange={(ids) => {
              const id = ids[ids.length - 1];
              if (!id) return;
              read(id);
            }}
          />
        ),
      };
    });
  }, [dateFormat, events, isRead, read, workspaceName]);

  return sections.map(({ title, key = title, content }) => (
    <div key={key}>
      <h5 className={styles['activities-feed-section-title']}>{title}</h5>
      {content}
    </div>
  ));
};

export default EventsList;
