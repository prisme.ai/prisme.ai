import { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { Field, Form, FormSpy } from 'react-final-form';
import { filters } from './presetFilters';
import ObjectInput from '@/components/ObjectInput';
import dayjs from 'dayjs';
import { useQueryString } from '@/providers/QueryString';
import { Button, DatePicker, Popover } from 'antd';
import { useUser } from '@/providers/User';
import { useTranslations } from 'next-intl';
import FilterIcon from '@/svgs/filter.svg';
import styles from './filters.module.scss';
import utc from 'dayjs/plugin/utc';

dayjs.extend(utc);

type FiltersValue = {
  beforeDate?: dayjs.Dayjs;
  afterDate?: dayjs.Dayjs;
  fields?: Record<string, string>;
};

function getInitialValue(prevQuery: URLSearchParams) {
  const beforeDate = prevQuery.get('beforeDate');
  const afterDate = prevQuery.get('afterDate');
  const value: FiltersValue = {
    beforeDate: beforeDate ? dayjs(beforeDate) : undefined,
    afterDate: afterDate ? dayjs(afterDate) : undefined,
    fields: Array.from(prevQuery.keys()).reduce(
      (prev, k) =>
        ['beforeDate', 'afterDate', 'text'].includes(k)
          ? prev
          : {
              ...prev,
              [k]: prevQuery.get(k),
            },
      {},
    ),
  };
  return value;
}

const Filters = () => {
  const t = useTranslations('workspaces');
  const { queryString, setQueryString } = useQueryString();
  const [suggestionsPopupState, setSuggestionsPopupState] = useState(false);
  const fieldsInput = useRef<HTMLDivElement>(null);
  const { user } = useUser();
  const [formIsSet, setFormIsSet] = useState(false);

  useEffect(() => {
    setFormIsSet(true);
  }, []);

  const updateValue = useCallback(
    ({ values }: { values: FiltersValue }) => {
      if (!formIsSet) return;
      setQueryString((prevQuery) => {
        const newQuery = new URLSearchParams();
        const text = prevQuery.get('text');
        if (text) {
          newQuery.set('text', text);
        }
        if (values.beforeDate) {
          newQuery.set('beforeDate', values.beforeDate.format());
        }
        if (values.afterDate) {
          newQuery.set('afterDate', values.afterDate.format());
        }
        Object.entries(values.fields || {}).forEach(([k, v]) => {
          newQuery.set(k, v);
        });
        if (newQuery.toString() === prevQuery.toString()) return prevQuery;
        return newQuery;
      });
    },
    [setQueryString, formIsSet],
  );

  const reset = useCallback(() => {
    updateValue({ values: {} });
  }, [updateValue]);

  const builtinFilters = useMemo(
    () =>
      Object.entries(filters({ sessionId: user?.sessionId })).map(([key, filter]) => ({
        value: key,
        label: t(`events.filters.suggestions.label_${key}`),
        filter,
      })),
    [t, user?.sessionId],
  );

  const focusFieldsInput = useCallback(() => {
    setTimeout(() => {
      if (!fieldsInput.current) return;
      const [, input] = Array.from(fieldsInput.current.querySelectorAll('input')).reverse();
      if (!input) return;
      input.focus();
    }, 200);
  }, []);

  return (
    <div className={styles['container']}>
      <Form onSubmit={updateValue} initialValues={getInitialValue(queryString)}>
        {() => (
          <form className={styles['form']}>
            <FormSpy onChange={updateValue}></FormSpy>
            <div className={styles['form-main-section']}>
              <div className={styles['form-section-dates']}>
                <div className={styles['form-field--first']}>
                  <Field name="afterDate">
                    {({ input }) => (
                      <label className={styles['form-field-label']}>
                        <span className={styles['form-field-label-text']}>
                          {t('events.filters.afterDate')}
                        </span>
                        <DatePicker
                          showNow={true}
                          showTime={{
                            defaultValue: dayjs.utc().hour(0).minute(0).second(0),
                          }}
                          format="DD/MM/YYYY HH:mm:ss"
                          {...input}
                        />
                      </label>
                    )}
                  </Field>
                </div>
                <Field name="beforeDate">
                  {({ input }) => (
                    <label className={styles['form-field-label']}>
                      <span className={styles['form-field-label-text']}>
                        {t('events.filters.beforeDate')}
                      </span>
                      <DatePicker
                        showNow={true}
                        showTime={{
                          defaultValue: dayjs.utc().hour(23).minute(59).second(59),
                        }}
                        format={'DD/MM/YYYY HH:mm:ss'}
                        {...input}
                      />
                    </label>
                  )}
                </Field>
              </div>
              <div className={styles['form-section-add-filter-btn']}>
                <Field name="fields">
                  {({ input }) => (
                    <Button
                      type="primary"
                      onClick={() => {
                        input.onChange({
                          ...input.value,
                          '': input.value[''] || '',
                        });
                        focusFieldsInput();
                      }}
                    >
                      {t('events.filters.specific.add')}
                    </Button>
                  )}
                </Field>
              </div>
            </div>
            <div className={styles['form-section-shortcuts']}>
              <Button className={styles['form-section-shortcuts-btn']} type="text" onClick={reset}>
                {t('events.filters.reset')}
              </Button>
              <Popover
                onOpenChange={setSuggestionsPopupState}
                open={suggestionsPopupState}
                showArrow
                placement="bottom"
                content={
                  <div className="flex flex-col">
                    {builtinFilters.map(({ label, filter }) => (
                      <Button
                        key={label}
                        onClick={() => {
                          updateValue({
                            values: {
                              fields: filter,
                            },
                          });
                          setSuggestionsPopupState(false);
                        }}
                        icon={<FilterIcon />}
                        className={styles['form-section-shortcuts-btn-in-popover']}
                        type="text"
                      >
                        {label}
                      </Button>
                    ))}
                  </div>
                }
                trigger={['click']}
              >
                <Button className={styles['form-section-shortcuts-btn']} type="text">
                  {t('events.filters.suggestions.title')}
                </Button>
              </Popover>
            </div>
            <Field name="fields">
              {({ input }) =>
                input.value &&
                Object.keys(input.value).length > 0 && (
                  <div className={styles['form-section-filters-field']} ref={fieldsInput}>
                    <ObjectInput
                      {...input}
                      label={
                        <label className={styles['form-section-filters-label']}>
                          {t('events.filters.specific.label')}
                        </label>
                      }
                      removeLabel={t('events.filters.specific.remove')}
                      deleteIconClassName="!text-gray"
                    />
                  </div>
                )
              }
            </Field>
          </form>
        )}
      </Form>
    </div>
  );
};

export default Filters;
