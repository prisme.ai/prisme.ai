'use client';

import { useCallback, useEffect, useState } from 'react';
import { useScrollListener } from '@/utils/useScrollListener';
import useLocalizedText from '@/utils/useLocalizedText';
import { Button, Input, Tooltip } from 'antd';
import Filters from './Filters';
import { useQueryString } from '@/providers/QueryString';
import { useWorkspace } from '@/providers/Workspace';
import { useEvents } from '@/providers/Events';
import { useTranslations } from 'next-intl';
import EventsList from './EventsList';
import styles from './activities.module.scss';
import StretchContent from '@/components/StretchContent';
import ReloadIcon from '@/svgs/reload.svg';
import PlayIcon from '@/svgs/play.svg';
import PauseIcon from '@/svgs/pause.svg';
import WarningIcon from '@/svgs/warning.svg';
import { useWorkspaceLayout } from '../Layout/context';

export const Activities = () => {
  const t = useTranslations('workspaces');
  const { localize } = useLocalizedText();
  const {
    workspace: { name: workspaceName },
  } = useWorkspace();
  const { setTitle } = useWorkspaceLayout();
  const {
    events,
    loading,
    setFilters,
    filters,
    fetchEvents,
    fetchNextEvents,
    hasMore,
    read,
    isRead,
    isVirgin,
    running,
    start,
    stop,
  } = useEvents();
  const { queryString, setQueryString } = useQueryString();
  const { ref, bottom } = useScrollListener<HTMLDivElement>({ margin: -1 });

  useEffect(() => {
    setTitle('workspace.sections.activity');
  }, [setTitle]);

  const filtersCount = Array.from(queryString.entries()).filter(([key]) => key !== 'text').length;
  const [showFilters, setShowFilters] = useState(filtersCount > 0);

  const updateQuery = useCallback(
    (text: string) => {
      setQueryString((prevQuery) => {
        const newQuery = new URLSearchParams(prevQuery);
        if (text) {
          newQuery.set('text', text);
        } else {
          newQuery.delete('text');
        }

        if (newQuery.toString() === prevQuery.toString()) return prevQuery;
        return newQuery;
      });
    },
    [setQueryString],
  );

  useEffect(() => {
    const t = setTimeout(() => {
      const query = Array.from(queryString.entries()).reduce(
        (prev, [k, v]) => ({
          ...prev,
          [k]: v,
        }),
        {},
      );
      setFilters(query);
    }, 200);

    return () => {
      clearTimeout(t);
    };
  }, [fetchEvents, queryString, setFilters]);

  useEffect(() => {
    if (bottom) {
      fetchNextEvents();
    }
  }, [bottom, fetchNextEvents]);

  const noResult = !isVirgin && events.size === 0;

  return (
    <div className={styles['activities-ctn']}>
      <div className={styles['activities-header']}>
        <div className={styles['activities-header-filters']}>
          <Input
            className={styles['activities-header-filters-input']}
            type="search"
            placeholder={t('events.search.placeholder')}
            value={queryString.get('text') || ''}
            onChange={({ target: { value } }) => updateQuery(value)}
          />
          <Button
            className={styles['activities-header-filters-toggle']}
            onClick={() => setShowFilters(!showFilters)}
            type={showFilters ? 'primary' : 'text'}
          >
            {filtersCount > 0 && (
              <span
                className={`${styles['activities-header-filters-toggle-indicator']} ${showFilters ? styles['activities-header-filters-toggle-indicator--active'] : ''}`}
              />
            )}
            {t('events.filters.title')}
            {filtersCount > 0 && (
              <span className={styles['activities-header-filters-toggle-count']}>
                ({filtersCount})
              </span>
            )}
          </Button>
        </div>
        <StretchContent visible={showFilters}>
          <Filters />
        </StretchContent>
      </div>

      <div className={styles['activities-content']}>
        <div className={styles['activities-feed-ctn']} ref={ref}>
          <div className={styles['activities-feed-tools']}>
            <span className={styles['activities-feed-tools-label']}>
              {t(`events.running.label${running ? '' : '_off'}`)}
            </span>
            <Tooltip
              title={t(`events.running.description${running ? '_off' : ''}`)}
              placement="left"
            >
              <button
                onClick={running ? stop : start}
                className={styles['activities-feed-tools-button']}
              >
                {running ? <PauseIcon /> : <PlayIcon />}
              </button>
            </Tooltip>
            <Tooltip
              title={t(`events.reload.description${running ? '_off' : ''}`)}
              placement="left"
            >
              <button
                onClick={() => {
                  setFilters({ ...filters });
                }}
                disabled={running}
                className={`${styles['activities-feed-tools-button']} ${styles['activities-feed-tools-button--reload']} ${running ? styles['activities-feed-tools-button--reload-running'] : ''}`}
              >
                <ReloadIcon />
              </button>
            </Tooltip>
          </div>
          {!loading && noResult && (
            <div className={styles['activities-feed-empty']}>
              <WarningIcon className={styles['activities-feed-empty-icon']} />
              {t('events.filters.empty')}
            </div>
          )}
          {!noResult && !isVirgin && (
            <>
              <EventsList
                events={events}
                workspaceName={localize(workspaceName)}
                read={read}
                isRead={isRead}
              />
              {hasMore && <Button onClick={fetchNextEvents}>{t('events.more')}</Button>}
            </>
          )}
        </div>
      </div>
    </div>
  );
};
export default Activities;
