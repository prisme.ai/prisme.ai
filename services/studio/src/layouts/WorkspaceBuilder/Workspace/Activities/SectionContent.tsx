import { useMemo } from 'react';
import { useWorkspace } from '@/providers/Workspace';
import { Event } from '@prisme.ai/sdk';
import useLocalizedText from '@/utils/useLocalizedText';
import { useSourceDetails } from '@/components/SourceDetails';
import {
  AppLabel,
  AutomationLabel,
  ErrorLabel,
  EventLabel,
  PageLabel,
  RollbackVersion,
} from './Labels';
import { useTranslations } from 'next-intl';
import { Tooltip } from 'antd';
import styles from './activities.module.scss';

interface SectionContentProps {
  title: string;
  date: string;
  type: string;
  read: boolean;
  event: Event<Date>;
}

export const SectionContent = ({ title, date, type, read, event }: SectionContentProps) => {
  const t = useTranslations('workspaces');
  const { localize } = useLocalizedText();
  const { photo, name, description } = useSourceDetails();
  const {
    workspace: { imports = {} },
  } = useWorkspace();

  const labelValues = useMemo(() => {
    let automationSlug =
      event.payload?.automation?.slug ||
      event.payload?.automationSlug ||
      event.source?.automationSlug ||
      '';

    if (event.source.appSlug) {
      const app = Object.values(imports).find(({ appSlug }) => appSlug === event.source.appSlug);
      const automation = app?.automations?.find(({ slug }) => slug === automationSlug);

      automationSlug = automation?.slug || automationSlug;
    }

    const automationName = event?.payload?.automation?.name || '';

    function getWaits() {
      if (!event?.payload?.wait) return '';
      if (event?.payload?.wait.oneOf && event?.payload?.wait.oneOf.length > 0) {
        const events = event?.payload?.wait?.oneOf?.map(({ event }: any) => event);

        return t('feed.type_runtime.waits.pending_oneOf', {
          count: events.length,
          events: events.join(', '),
        });
      }
      return t('feed.type_runtime.waits.pending_timeout', {
        timeout: event?.payload?.wait?.timeout || 0,
      });
    }

    return {
      automationName: localize(automationName ? automationName : automationSlug),
      pageName: event?.payload?.page?.name && localize(event.payload.page.name),
      eventType: event?.payload?.event?.type,
      waits: getWaits(),
      appSlug:
        event?.payload?.appInstance?.appSlug ||
        event?.payload?.appSlug ||
        event?.payload?.app?.slug,
      appName:
        event?.payload?.appInstance?.appName ||
        event?.payload?.appInstance?.slug ||
        event?.payload?.appInstance?.appSlug ||
        event?.payload?.appSlug ||
        event?.payload?.app?.name,
      share: event?.payload?.target?.displayName || event?.payload?.target?.id,
    };
  }, [event, imports, localize, t]);

  const cleanedType = `${type}`.match(/^runtime\.waits\.fulfilled/)
    ? 'runtime.waits.fulfilled'
    : type;

  return (
    <div className={styles['activities-feed-section-item-ctn']}>
      <div className={styles['activities-feed-section-item-header']}>
        {photo && (
          <img
            src={photo}
            height={25}
            width={25}
            className={`${styles['activities-feed-section-item-img']} ${read ? styles['activities-feed-section-item-img--read'] : ''}`}
            alt={name}
          />
        )}
        <Tooltip
          title={
            <>
              <div className="font-bold">{title}</div>
              <div>{localize(description)}</div>
            </>
          }
        >
          <div
            className={`${styles['activities-feed-section-item-title']} ${read ? styles['activities-feed-section-item-title--read'] : ''}`}
          >
            {title}
          </div>
        </Tooltip>
        <div className={styles['activities-feed-section-item-date']}>{date}</div>
      </div>
      <div className={styles['activities-feed-section-item-details']}>
        {t.rich(
          t.has(`feed.type.${cleanedType}`) ? `feed.type.${cleanedType}` : 'feed.type.default',
          {
            ...labelValues,
            context: cleanedType,
            eventData: event.payload,
            automation: (chunks) => <AutomationLabel {...event}>{chunks}</AutomationLabel>,
            page: (chunks) => <PageLabel {...event}>{chunks}</PageLabel>,
            event: (chunks) => <EventLabel {...event}>{chunks}</EventLabel>,
            app: (chunks) => <AppLabel {...event}>{chunks}</AppLabel>,
            error: (chunks) => <ErrorLabel {...event}>{chunks}</ErrorLabel>,
            rollback: (chunks) => <RollbackVersion {...event}>{chunks}</RollbackVersion>,
            tooltip: (chunks) => <Tooltip>{chunks}</Tooltip>,
          },
        )}
      </div>
    </div>
  );
};

export default SectionContent;
