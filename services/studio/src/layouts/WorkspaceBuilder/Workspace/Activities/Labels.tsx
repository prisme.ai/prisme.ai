import { Button, notification, Popconfirm } from 'antd';
import Link from 'next/link';
import { ReactNode } from 'react';
import { useWorkspace } from '@/providers/Workspace';
import { Event } from '@prisme.ai/sdk';
import { useTranslations } from 'next-intl';
import useApi from '@/utils/api/useApi';
import WarningIcon from '@/svgs/warning.svg';

interface AutomationLabelProps extends Event<Date> {
  children?: ReactNode;
}
export const AutomationLabel = ({ children, source, payload }: AutomationLabelProps) => {
  const { workspace } = useWorkspace();

  if (source.appSlug) {
    return <span className="font-bold">{children}</span>;
  }
  const automationSlug = payload.slug || payload.automationSlug || source.automationSlug;
  const automationExists = !!workspace?.automations?.[automationSlug];
  if (automationExists) {
    return (
      <Link href={`/workspaces/${source.workspaceId}/automations/${automationSlug}`}>
        {children}
      </Link>
    );
  }
  return <span className="font-bold">{children}</span>;
};

export const PageLabel = ({ children, payload, source }: AutomationLabelProps) => {
  return (
    <Link href={`/workspaces/${source.workspaceId}/pages/${payload.page.id}`}>{children}</Link>
  );
};

export const AppLabel = ({ children, payload, source }: AutomationLabelProps) => {
  return (
    <Link href={`/workspaces/${source.workspaceId}/apps/${payload?.appInstance?.appName}`}>
      {children}
    </Link>
  );
};

export const ErrorLabel = ({ children }: AutomationLabelProps) => {
  return (
    <div className="flex items-center">
      <span className="text-pr-orange mr-[0.5rem] flex">
        <WarningIcon />
      </span>
      <span className="flex">{children}</span>
    </div>
  );
};

export const EventLabel = ({ children }: AutomationLabelProps) => {
  return <span className="font-bold">{children}</span>;
};

export const RollbackVersion = ({
  children,
  source: { workspaceId },
  payload: { version },
}: AutomationLabelProps) => {
  const api = useApi();
  const t = useTranslations('workspaces');
  const { fetchWorkspace } = useWorkspace();
  return (
    <Popconfirm
      title={t('workspace.versions.rollback.confirm')}
      okText={t('workspace.versions.rollback.confirm_ok')}
      cancelText={t('cancel', { ns: 'common' })}
      onCancel={(e) => e?.stopPropagation()}
      onConfirm={async (e) => {
        e?.stopPropagation();
        if (!workspaceId || !version) return;
        await api.workspaces(workspaceId).versions.rollback(version.name, {
          repository: version.repository,
        });
        fetchWorkspace();
        notification.success({
          message: t('workspace.versions.rollback.success'),
          placement: 'bottomRight',
        });
      }}
    >
      <Button className="font-bold" onClick={(e) => e.stopPropagation()}>
        {children}
      </Button>
    </Popconfirm>
  );
};
