import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import useKeyboardShortcut from '@/utils/useKeyboardShortcut';
import { usePage } from '@/providers/Page';
import { useWorkspace } from '@/providers/Workspace';
import { incrementName } from '@/utils/incrementName';
import { replaceSilently } from '@/utils/urls';
import { useTracking } from '@/providers/Tracking';
import { getBackTemplateDots, removeTemplateDots } from '@/utils/templatesInBlocks';
import PagePreview, { usePagePreview } from './PagePreview';
import { useWorkspaceLayout } from '../Layout/context';
import useLocalizedText from '@/utils/useLocalizedText';
import LowCodeEditor from '@/components/LowCodeEditor';
import { HeaderLeft, HeaderRight } from './Header';
import SourceEdit from '@/components/SourceEdit';
import styles from './page.module.scss';
import PageBuilder from '@/components/PageBuilder';
import { ApiError } from '@prisme.ai/sdk';
import equal from 'fast-deep-equal';
import Head from '@/components/Head';
import useTranslations from '@/i18n/useTranslations';
import useRouter from '@/utils/useRouter';
import useWarningBeforeNavigate from '@/utils/useRouter/useWarningBeforeNavigate';
import { useBlocksSnippets } from '@/utils/useSnippets/useSnippets';
import { useBlocksLinks } from '@/utils/useLinks/useLinks';
import { validatePage } from '@prisme.ai/validation';
import { useNotifications } from '@/providers/Notifications';
import deepClone from '@/utils/deepClone';
import { ValidationError } from '@/utils/useYaml/yaml';
import { YAMLException } from 'js-yaml';

const Page = () => {
  const t = useTranslations('workspaces');
  const { trackEvent } = useTracking();
  const { replace, push } = useRouter();
  const notification = useNotifications();
  const { page, savePage, saving, deletePage } = usePage();
  const { reload } = usePagePreview();
  // Used to reset the builder when value is changed from anywhere else
  const [builderMounted, setBuilderMounted] = useState(true);
  const {
    workspace: { id: workspaceId, pages = {}, name: workspaceName },
    createPage,
  } = useWorkspace();
  const [value, setValue] = useState(removeTemplateDots(page));
  const [viewMode, setViewMode] = useState(0);
  useEffect(() => {
    setViewMode(page.blocks?.length === 0 ? 1 : 0);
    setValue(removeTemplateDots(page));
  }, [page]);

  const [dirty, setDirty] = useState(false);
  useWarningBeforeNavigate(dirty);

  const prevValue = useRef(value);
  useEffect(() => {
    if (equal(value, prevValue.current)) return;
    setDirty(true);
  }, [value]);

  const [duplicating, setDuplicating] = useState(false);
  const duplicate = useCallback(async () => {
    if (!page.id || !page.slug) return;
    trackEvent({
      name: 'Duplicate Page',
      action: 'click',
    });
    setDuplicating(true);
    const newSlug = incrementName(
      page.slug,
      Object.keys(pages).map((k) => k),
      '{{name}}-{{n}}',
      { keepOriginal: true },
    );
    const { apiKey, ...newPage } = page;
    void apiKey;
    const { slug } =
      (await createPage({
        ...newPage,
        slug: newSlug,
      })) || {};
    if (slug) {
      push(`/workspaces/${workspaceId}/pages/${slug}`);
    }
    setDuplicating(false);
    notification.success({
      message: t('pages.duplicate.success'),
      placement: 'bottomRight',
    });
  }, [createPage, page, pages, push, t, trackEvent, workspaceId, notification]);

  const { localize } = useLocalizedText();
  const { setTitle } = useWorkspaceLayout();
  useEffect(() => {
    setTitle(
      t('pages.title', {
        name: localize(page.name),
        slug: page.slug,
      }),
    );
  }, [localize, page.name, page.slug, setTitle, t]);

  const onDelete = useCallback(() => {
    trackEvent({
      name: 'Delete Page',
      action: 'click',
    });
    replace(`/workspaces/${page.workspaceId}`);
    deletePage();
    notification.success({
      message: t('pages.delete.toast'),
      placement: 'bottomRight',
    });
  }, [deletePage, page.workspaceId, replace, trackEvent, notification, t]);

  const save = useCallback(
    async (toSave: Prismeai.Page = value) => {
      const prevSlug = page.slug;
      trackEvent({
        name: 'Save Page',
        action: 'click',
      });
      try {
        const saved = await savePage(getBackTemplateDots(toSave));
        if (!saved) return;
        prevValue.current = saved;
        setDirty(false);
        notification.success({
          message: t('pages.save.toast'),
          placement: 'bottomRight',
        });
        const { slug } = saved;
        if (slug !== prevSlug) {
          replaceSilently(`/workspaces/${workspaceId}/pages/${slug}`);
        }
        reload();
      } catch (e) {
        const { details, error } = e as ApiError;
        const description = (
          <ul>
            {details ? (
              details.map(({ path, message }: any, key: number) => (
                <li key={key}>
                  {t('openapi', {
                    context: message,
                    path: path.replace(/^\.body\./, ''),
                  })}
                </li>
              ))
            ) : (
              <li>
                {t.has(`pages.save.reason_${error}`)
                  ? t(`pages.save.reason_${error}`)
                  : t('pages.save.reason')}
              </li>
            )}
          </ul>
        );
        notification.error({
          message: t('pages.save.error'),
          description,
          placement: 'bottomRight',
        });
      }
    },
    [page.slug, reload, savePage, t, trackEvent, value, workspaceId, notification],
  );

  useKeyboardShortcut([
    {
      key: 's',
      meta: true,
      command: (e) => {
        e.preventDefault();
        trackEvent({
          name: 'Save Page with shortcut',
          action: 'keydown',
        });
        save();
      },
    },
  ]);

  const [displaySource, setDisplaySource] = useState(false);

  const [validationError, setValidationError] = useState<ValidationError>();
  const validateSource = useCallback((json: any) => {
    const isValid = validatePage(json);
    const [error] = validatePage.errors || [];
    setValidationError(error as ValidationError);
    return isValid;
  }, []);
  useEffect(() => {
    if (validateSource(value)) {
      setValidationError(undefined);
    }
  }, [value, validateSource]);
  const [syntaxError, setSyntaxError] = useState<YAMLException | null>(null);

  const source = useMemo(() => {
    const { id, workspaceSlug, workspaceId, apiKey, ...page } = value as Prismeai.Page & {
      apiKey: string;
    };
    void id, workspaceSlug, workspaceId, apiKey;
    return getBackTemplateDots(page);
  }, [value]);
  const setSource = useCallback((source: any) => {
    setValue(deepClone(source));
  }, []);

  useEffect(() => {
    if (displaySource) {
      setTimeout(() => setBuilderMounted(false), 200);
    } else {
      setBuilderMounted(true);
    }
  }, [displaySource]);

  const saveBlocks = useCallback(
    (blocks: Prismeai.Page['blocks']) => {
      setValue({
        ...value,
        blocks,
      });
    },
    [value],
  );

  const snippets = useBlocksSnippets();

  return (
    <>
      <Head
        title={`[${localize(workspaceName)}] ${t('page_title', {
          elementName: localize(value.name),
        })}`}
        description=""
      />
      <LowCodeEditor
        header={{
          title: {
            value: value.name || '',
            onChange: (name) =>
              setValue(
                getBackTemplateDots({
                  ...value,
                  name,
                }),
              ),
          },
          left: (
            <HeaderLeft
              value={value}
              duplicate={duplicate}
              duplicating={duplicating}
              displaySource={displaySource}
              onDisplaySource={setDisplaySource}
              onDelete={onDelete}
              onChange={(p) => {
                setValue(p);
                save(p);
              }}
            />
          ),
          right: (
            <HeaderRight
              onSave={() => save()}
              dirty={dirty}
              reload={reload}
              saving={saving}
              viewMode={viewMode}
              setViewMode={setViewMode}
              viewModeEditOnly={(value?.blocks || []).length === 0}
              error={!!validationError || !!syntaxError}
            />
          ),
        }}
      >
        <PagePreview page={getBackTemplateDots(value)} visible={viewMode === 0} />
        <SourceEdit
          value={source}
          onChange={setSource}
          onSave={save}
          visible={displaySource}
          snippets={snippets}
          links={useBlocksLinks()}
          error={validationError}
          onError={setSyntaxError}
        />
        <div
          className={`${styles['page-builder-ctn']} ${viewMode === 1 ? styles['page-builder-ctn--visible'] : ''}`}
        >
          {builderMounted && <PageBuilder value={value.blocks} onChange={saveBlocks} />}
        </div>
      </LowCodeEditor>
    </>
  );
};

export default Page;
