import { useWorkspace } from '@/providers/Workspace';
import {
  createContext,
  ReactNode,
  useCallback,
  useContext,
  useEffect,
  useRef,
  useState,
} from 'react';
import { useTranslations } from 'next-intl';
import { Tooltip } from 'antd';
import IFrame from '@/components/IFrame';
import styles from './page.module.scss';
import { useEnv } from '@/providers/Env';
import MobileIcon from '@/svgs/mobile.svg';
import TabletIcon from '@/svgs/tablet.svg';
import DesktopIcon from '@/svgs/desktop.svg';
import useRouter from '@/utils/useRouter';

interface PagePreviewContext {
  reload: () => void;
  mounted: boolean;
}

const pagePreviewContext = createContext<PagePreviewContext | undefined>(undefined);
export const usePagePreview = () => {
  const context = useContext(pagePreviewContext);
  if (!context) {
    throw new Error();
  }
  return context;
};

interface PagePreviewProviderProps {
  children: ReactNode;
}
export const PagePreviewProvider = ({ children }: PagePreviewProviderProps) => {
  const [mounted, setMounted] = useState(true);
  const reload = useCallback(() => {
    setMounted(false);
    setTimeout(() => setMounted(true), 1);
  }, []);
  return (
    <pagePreviewContext.Provider value={{ mounted, reload }}>
      {children}
    </pagePreviewContext.Provider>
  );
};

interface PagePreviewProps {
  page: Prismeai.Page;
  visible?: boolean;
}

export const PagePreview = ({ page, visible }: PagePreviewProps) => {
  const { PAGES_HOST = `${global?.location?.origin}/pages` } = useEnv<{ PAGES_HOST: string }>();
  const {
    workspace,
    workspace: { slug },
  } = useWorkspace();
  const iframeRef = useRef<HTMLIFrameElement>(null);
  const t = useTranslations('workspaces');
  const [width, setWidth] = useState<'full' | 'tablet' | 'mobile'>('full');
  const { mounted } = usePagePreview();
  const [loaded, setLoaded] = useState(false);
  const { push } = useRouter();
  void loaded;

  const update = useCallback(() => {
    if (!iframeRef.current || !visible) return;

    const appInstances = Object.entries(workspace.imports || {}).reduce<
      { slug: string; blocks: {} }[]
    >(
      (prev, [slug, { blocks = [] }]) => [
        ...prev,
        {
          slug,
          blocks: blocks.reduce((prev, { slug, ...block }) => ({ ...prev, [slug]: block }), {}),
        },
      ],
      [],
    );

    iframeRef.current?.contentWindow?.postMessage(
      {
        type: 'previewpage.update',
        page: {
          ...page,
          appInstances: [{ slug: '', blocks: workspace.blocks }, ...appInstances],
        },
      },
      '*',
    );
  }, [page, visible, workspace.blocks, workspace.imports]);

  useEffect(() => {
    update();
  }, [update]);

  useEffect(() => {
    const listener = (e: MessageEvent) => {
      const { type } = e.data || {};
      if (type === 'previewpage:init') {
        update();
      }
      if (type === 'previewpage:navigate') {
        push(`/workspaces/${workspace.id}/pages/${e.data.url}`);
      }
    };
    window.addEventListener('message', listener);
    return () => {
      window.removeEventListener('message', listener);
    };
  }, [update, push, workspace.id]);

  return (
    <div className={styles['page-preview-ctn']}>
      <div className={styles['page-preview-iframe-ctn']}>
        {mounted && (
          <IFrame
            ref={iframeRef}
            className={`${styles['page-preview-iframe']} ${width === 'tablet' ? styles['page-preview-iframe--tablet'] : ''} ${width === 'mobile' ? styles['page-preview-iframe--mobile'] : ''}`}
            src={`${window.location.protocol}//${slug}${PAGES_HOST}/__preview/page`}
            onLoad={() => setLoaded(true)}
          />
        )}
      </div>
      <div className={styles['page-preview-toolbar']}>
        <Tooltip title={t('blocks.preview.toggleWidth.desktop')}>
          <button
            onClick={() => {
              setWidth('full');
            }}
            className={styles['page-preview-toolbar-btn']}
          >
            <DesktopIcon
              className={width === 'full' ? styles['page-preview-toolbar-btn-active'] : ''}
            />
          </button>
        </Tooltip>
        <Tooltip title={t('blocks.preview.toggleWidth.tablet')}>
          <button
            onClick={() => {
              setWidth('tablet');
            }}
            className={styles['page-preview-toolbar-btn']}
          >
            <TabletIcon
              className={width === 'tablet' ? styles['page-preview-toolbar-btn-active'] : ''}
            />
          </button>
        </Tooltip>
        <Tooltip title={t('blocks.preview.toggleWidth.mobile')}>
          <button
            onClick={() => {
              setWidth('mobile');
            }}
            className={styles['page-preview-toolbar-btn']}
          >
            <MobileIcon
              className={width === 'mobile' ? styles['page-preview-toolbar-btn-active'] : ''}
            />
          </button>
        </Tooltip>
      </div>
    </div>
  );
};

export default PagePreview;
