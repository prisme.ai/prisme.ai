import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { Form } from 'react-final-form';
import FieldContainer from '@/components/Field';
import { usePermissions } from '@/providers/Permissions';
import { useUser } from '@/providers/User';
import { usePageEndpoint } from '@/utils/urls';
import { UserPermissions } from '@prisme.ai/sdk';
import { useTracking } from '@/providers/Tracking';
import ShareButton from './ShareButton';
import { useTranslations } from 'next-intl';
import { Button, Input, notification, Switch, Table, Tooltip } from 'antd';
import TrashIcon from '@/svgs/trash.svg';
import styles from './share-page.module.scss';
import { FormApi } from 'final-form';

interface SharePageProps {
  pageId: string;
  pageSlug: string;
  workspaceId: string;
}

interface userPermissionForm {
  email: string;
}

const SharePage = ({ pageId, pageSlug, workspaceId }: SharePageProps) => {
  const t = useTranslations('workspaces');
  const pageHost = usePageEndpoint();
  const { user } = useUser();
  const { trackEvent } = useTracking();
  const {
    usersPermissions,
    getUsersPermissions,
    addUserPermissions,
    removeUserPermissions,
    getRoles,
    roles,
  } = usePermissions();
  const subjectType = 'pages';
  const subjectId = `${pageId}`;
  const formRef = useRef<FormApi<any>>();

  const roleNames = useMemo(() => roles.map((role) => role.name), [roles]);

  const generateRowButtons = useCallback(
    (onDelete: Function) => (
      <div className={styles['row-button-ctn']}>
        <Tooltip title={t('share.delete')}>
          <Button onClick={() => onDelete()}>
            <TrashIcon />
          </Button>
        </Tooltip>
      </div>
    ),
    [t],
  );

  const initialFetch = useCallback(async () => {
    getUsersPermissions(subjectType, subjectId);
    getRoles('workspaces', workspaceId);
  }, [getUsersPermissions, subjectId, getRoles, workspaceId]);

  useEffect(() => {
    initialFetch();
  }, [initialFetch]);

  const dataSource = useMemo(() => {
    const data = usersPermissions.get(`${subjectType}:${subjectId}`);

    if (!data) {
      return [];
    }

    const rows = data
      .filter(({ target }) => !!target?.id && !target.public)
      .map(({ target }) => ({
        key: target?.id,
        displayName: target?.displayName,
        actions: generateRowButtons(() => {
          removeUserPermissions(subjectType, subjectId, target);
        }),
      }));
    return rows;
  }, [generateRowButtons, removeUserPermissions, subjectId, subjectType, usersPermissions]);

  const [isPublic, setIsPublic] = useState(false);
  useEffect(() => {
    const data = usersPermissions.get(`${subjectType}:${subjectId}`);
    if (!data) {
      return setIsPublic(false);
    }
    setIsPublic(!!data.find(({ target: { public: p } }) => p));
  }, [subjectId, usersPermissions]);

  const togglePublic = useCallback(
    async (isPublic: boolean) => {
      trackEvent({
        name: `Set page access ${isPublic ? 'Public' : 'Restricted'}`,
        action: 'click',
      });
      setIsPublic(isPublic);
      if (isPublic) {
        await addUserPermissions('pages', subjectId, {
          target: {
            public: true,
          },
          permissions: {
            policies: { read: true },
          },
        });
      } else {
        await removeUserPermissions('pages', subjectId, { public: true });
      }
    },
    [addUserPermissions, removeUserPermissions, subjectId, trackEvent],
  );

  const onSubmit = ({ email }: userPermissionForm) => {
    formRef.current?.reset();
    let target: UserPermissions['target'] = { email };
    if (roleNames.includes(email)) {
      target = { role: email };
    } else if (email === user.email) {
      notification.warning({
        message: t('pages.share.notme'),
        placement: 'bottomRight',
      });
      return;
    }
    trackEvent({
      name: 'Add a permission',
      action: 'click',
    });
    addUserPermissions(subjectType, subjectId, {
      target,
      permissions: {
        policies: { read: true },
      },
    });
  };

  const link = `${pageHost}/${pageSlug}`;

  return (
    <div className={styles['container']}>
      <ShareButton link={link} />
      <label className={styles['access-field']}>
        <div className={styles['access-field-label']}>{t('pages.share.access')}</div>
        <Switch
          checked={isPublic}
          onChange={togglePublic}
          checkedChildren={t('pages.publicAccess.enabled')}
          unCheckedChildren={t('pages.publicAccess.disabled')}
        />
      </label>

      <Form onSubmit={onSubmit} initialValues={{ email: '' }}>
        {({ handleSubmit, form }) => {
          formRef.current = form;
          return (
            <form onSubmit={handleSubmit} className={styles['add-form']}>
              <div className={styles['add-form-title']}>{t('pages.share.access_private')}</div>
              <div className={styles['add-form-field']}>
                <FieldContainer
                  name="email"
                  containerClassName={styles['add-form-field-ctn']}
                  label={t('share.emailOrRole')}
                >
                  {({ input }) => <Input {...input} id="email" />}
                </FieldContainer>
                <Button htmlType="submit" type="primary">
                  {t('add', { ns: 'common' })}
                </Button>
              </div>
            </form>
          );
        }}
      </Form>
      <Table
        dataSource={dataSource}
        columns={[
          {
            title: t('share.displayName'),
            dataIndex: 'displayName',
            key: 'displayName',
          },
          {
            title: t('share.actions'),
            dataIndex: 'actions',
            key: 'actions',
            width: '15%',
          },
        ]}
        bordered
        pagination={{
          defaultPageSize: 10,
          position: ['bottomRight'],
          responsive: true,
          pageSizeOptions: [10, 20, 50],
        }}
        scroll={{ y: 500 }}
      />
    </div>
  );
};

export default SharePage;
