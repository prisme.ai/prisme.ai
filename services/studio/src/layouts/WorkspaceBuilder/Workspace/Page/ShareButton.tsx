import { useCallback } from 'react';
import copy from '@/utils/copy';
import { useTracking } from '@/providers/Tracking';
import { useTranslations } from 'next-intl';
import { notification, Tooltip } from 'antd';
import styles from './share-page.module.scss';
import LinkIcon from '@/svgs/link.svg';
import CopyIcon from '@/svgs/copy.svg';

interface ShareButtonProps {
  link: string;
}
export const ShareButton = ({ link }: ShareButtonProps) => {
  const { trackEvent } = useTracking();
  const t = useTranslations('workspaces');
  const copyLink = useCallback(() => {
    trackEvent({
      name: 'Copy link in Share Panel',
      action: 'click',
    });
    copy(link);
    notification.success({
      message: t('pages.share.copied'),
      placement: 'bottomRight',
    });
  }, [link, t, trackEvent]);
  return (
    <div className={styles['share-button']}>
      <Tooltip title={t('pages.share.link.open')} placement="bottom">
        <button className={styles['share-button-url-btn']}>
          <a href={link} target="_blank" rel="noreferrer" className="flex items-center">
            <LinkIcon className={styles['share-button-url-btn-icon']} />
            <span className={styles['share-button-url-btn-text']}>{link}</span>
          </a>
        </button>
      </Tooltip>
      <Tooltip title={t('pages.share.link.copy')} placement="bottom">
        <button className={styles['share-button-copy-btn']} type="button" onClick={copyLink}>
          <CopyIcon />
        </button>
      </Tooltip>
    </div>
  );
};
export default ShareButton;
