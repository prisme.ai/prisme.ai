export const defaultStyles = `
body {
  --color-accent: var(--pr-accent-color);
  --color-accent-contrast: var(--pr-accent-color-contrast);
  --color-text: var(--pr-main-text);
  background-color: var(--pr-main-bg-color);
}

.page-blocks > .pr-block-blocks-list {
  display: flex;
  flex-direction: column;
  min-width: 100%;
  min-height: 100%;
  flex: 1;
  padding: 0 var(--pr-page-x-margin) 0 var(--pr-page-x-margin);
}

.page-blocks > .pr-block-blocks-list > .block-cards {
  width: 100vw;
  margin: 0 calc(-1 * var(--pr-page-x-margin));
}

.page-blocks > .pr-block-blocks-list > .block-cards .cards-container__cards-container > div:first-child {
  padding-left: var(--pr-page-x-margin);
}

.pr-poweredby {
  position: fixed;
  left: 1rem;
  bottom: 1rem;
}
`;
