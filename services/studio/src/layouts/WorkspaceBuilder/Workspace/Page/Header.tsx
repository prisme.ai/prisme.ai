import { Button, Popover, Segmented, Tooltip } from 'antd';
import { useTranslations } from 'next-intl';
import { useState } from 'react';
import CloseIcon from '@/svgs/close.svg';
import CodeIcon from '@/svgs/code.svg';
import CopyIcon from '@/svgs/copy.svg';
import ShareIcon from '@/svgs/share.svg';
import ReloadIcon from '@/svgs/reload.svg';
import EyeIcon from '@/svgs/eye-open.svg';
import EditIcon from '@/svgs/edit.svg';
import EditDetails from './EditDetails';
import { defaultStyles } from './defaultStyles';
import SharePage from './SharePage';
import styles from './page.module.scss';
import Loading from '@/components/Loading';
import IconButton from '@/components/IconButton';
import SettingsIcon from '@/svgs/gear.svg';

interface HeaderLeftProps {
  value: Prismeai.Page;
  duplicate: () => void;
  duplicating: boolean;
  displaySource: boolean;
  onDisplaySource: (state: boolean) => void;
  onDelete: () => void;
  onChange: (v: Prismeai.Page) => void;
}

export const HeaderLeft = ({
  value,
  duplicate,
  duplicating,
  displaySource,
  onDisplaySource,
  onDelete,
  onChange,
}: HeaderLeftProps) => {
  const t = useTranslations();
  const [shareOpen, setShareOpen] = useState(false);
  return (
    <>
      <Tooltip title={t('workspaces.pages.details.title')} placement="bottom">
        <EditDetails
          value={{
            ...value,
            styles: value.styles === undefined ? defaultStyles : value.styles,
          }}
          onSave={async (v) => {
            onChange({
              ...value,
              ...v,
              styles: v.styles || '',
            });
          }}
          onDelete={onDelete}
          context="pages"
        >
          <IconButton icon={<SettingsIcon />} />
        </EditDetails>
      </Tooltip>
      <Popover
        open={shareOpen}
        onOpenChange={setShareOpen}
        title={() => (
          <div className={styles['share-popover-header']}>
            {t('workspaces.pages.share.label')}
            <button
              onClick={() => {
                setShareOpen(false);
              }}
            >
              <CloseIcon />
            </button>
          </div>
        )}
        content={() => (
          <SharePage
            pageId={`${value.id}`}
            pageSlug={value.slug === 'index' ? '' : value.slug || ''}
            workspaceId={value.workspaceId || ''}
          />
        )}
        trigger={'click'}
      >
        <Tooltip title={t('workspaces.pages.share.label')} placement="bottom">
          <IconButton className={styles['header-button']} icon={<ShareIcon />} />
        </Tooltip>
      </Popover>

      <Tooltip title={t('workspaces.pages.duplicate.help')} placement="bottom">
        <IconButton
          onClick={duplicate}
          className={styles['header-button']}
          disabled={duplicating}
          icon={<CopyIcon width="1.2rem" height="1.2rem" />}
        >
          {t('common.duplicate')}
        </IconButton>
      </Tooltip>
      <Tooltip title={t('workspaces.pages.source.help')} placement="bottom">
        <IconButton
          className={styles['header-button']}
          icon={<CodeIcon width="1.2rem" height="1.2rem" />}
          onClick={() => onDisplaySource(!displaySource)}
        >
          {displaySource ? t('workspaces.pages.source.close') : t('workspaces.pages.source.label')}
        </IconButton>
      </Tooltip>
    </>
  );
};

interface HeaderRightProps {
  onSave: () => void;
  reload: () => void;
  dirty: boolean;
  saving: boolean;
  viewMode: number;
  setViewMode: (mode: number) => void;
  viewModeEditOnly: boolean;
  error?: boolean;
}
export const HeaderRight = ({
  onSave,
  reload,
  dirty,
  saving,
  viewMode,
  setViewMode,
  viewModeEditOnly,
  error,
}: HeaderRightProps) => {
  const t = useTranslations('workspaces');
  return (
    <>
      <Button
        onClick={onSave}
        disabled={!dirty || saving || error}
        type="primary"
        className={styles['header-button']}
      >
        {saving && <Loading />}
        {t('pages.save.label')}
      </Button>

      <Tooltip title={t('pages.reload')} placement="bottom">
        <Button onClick={reload} type="primary" className={styles['header-button']}>
          <ReloadIcon />
        </Button>
      </Tooltip>

      <Segmented
        className={styles['header-segmented']}
        options={[
          {
            value: 0,
            icon: (
              <Tooltip title={t('pages.preview')} placement="bottom">
                <EyeIcon />
              </Tooltip>
            ),
            className: 'flex',
            disabled: viewModeEditOnly,
          },
          {
            value: 1,
            icon: (
              <Tooltip title={t('pages.edit')} placement="bottom">
                <EditIcon />
              </Tooltip>
            ),
            className: 'flex',
          },
        ]}
        value={viewMode}
        onChange={(v) => setViewMode(+v)}
      />
    </>
  );
};
