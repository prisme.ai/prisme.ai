import { ReactNode, useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { useTracking } from '@/providers/Tracking';
import useLocalizedText from '@/utils/useLocalizedText';
import CSSEditor from '@/components/CSSEditor/CSSEditor';
import ConfirmButton from '@/components/ConfirmButton';
import SchemaForm, { FieldProps, InfoBubble, Schema } from '@/components/SchemaForm';
import { Button, Popover, PopoverProps, Segmented, Tabs, Tooltip } from 'antd';
import { useTranslations } from 'next-intl';
import CloseIcon from '@/svgs/close.svg';
import TrashIcon from '@/svgs/trash.svg';
import { useField } from 'react-final-form';
import FieldContainerWithRaw from '@/components/SchemaFormInWorkspace/FieldContainerWithRaw';
import Label from '@/components/SchemaForm/Label';
import styles from './page.module.scss';
import useExtractSelectOptions from '@/components/SchemaFormInWorkspace/useExtractSelectOptions';
import useExtractAutocompleteOptions from '@/components/SchemaFormInWorkspace/useExtractAutocompleteOptions';

interface EditDetailsprops extends Omit<PopoverProps, 'content'> {
  value: any;
  onSave: (values: any) => Promise<void | Record<string, string>>;
  onDelete: () => void;
  context?: string;
  disabled?: boolean;
  children: ReactNode;
}

const ColorSchemeSwitch = ({ name, schema }: FieldProps) => {
  const t = useTranslations('workspaces');
  const field = useField(name);
  return (
    <FieldContainerWithRaw name={name} schema={schema}>
      <Label field={field} schema={schema} className={styles['color-scheme-switch-label']}>
        {t('pages.details.colorScheme.label')}
      </Label>
      <InfoBubble
        text={t.rich('pages.details.colorScheme.description', {
          example: () => (
            <pre className={styles['color-scheme-switch-exemple']}>
              <code>{`[data-color-scheme=\"dark\"] … {
  …
}`}</code>
            </pre>
          ),
        })}
      />
      <div>
        <Segmented
          id={field.input.name}
          value={field.input.value || 'auto'}
          onChange={field.input.onChange}
          options={[
            {
              label: (
                <Tooltip title={t('pages.details.colorScheme.auto.description')} placement="bottom">
                  {t('pages.details.colorScheme.auto.label')}
                </Tooltip>
              ),
              value: 'auto',
            },
            {
              label: (
                <Tooltip
                  title={t('pages.details.colorScheme.light.description')}
                  placement="bottom"
                >
                  {t('pages.details.colorScheme.light.label')}
                </Tooltip>
              ),
              value: 'light',
            },
            {
              label: (
                <Tooltip title={t('pages.details.colorScheme.dark.description')} placement="bottom">
                  {t('pages.details.colorScheme.dark.label')}
                </Tooltip>
              ),
              value: 'dark',
            },
          ]}
        />
      </div>
    </FieldContainerWithRaw>
  );
};

export const EditDetails = ({
  value,
  onSave,
  onDelete,
  disabled,
  onOpenChange,
  children,
  ...props
}: EditDetailsprops) => {
  const t = useTranslations();
  const { localize } = useLocalizedText();
  const { trackEvent } = useTracking();
  const [open, setOpen] = useState(false);
  const [values, setValues] = useState(value);
  const extractSelectOptions = useExtractSelectOptions();
  const extractAutocompleteOptions = useExtractAutocompleteOptions();

  const configSchema: Schema = useMemo(
    () => ({
      type: 'object',
      properties: {
        name: {
          type: 'localized:string',
          title: t('workspaces.pages.details.name.label'),
        },
        slug: {
          type: 'string',
          title: t('workspaces.pages.details.slug.label'),
        },
        description: {
          type: 'localized:string',
          title: t('workspaces.pages.details.description.label'),
          'ui:widget': 'textarea',
          'ui:options': { textarea: { rows: 10 } },
        },
      },
    }),
    [t],
  );
  const lifecycleSchema: Schema = useMemo(
    () => ({
      type: 'object',
      properties: {
        onInit: {
          type: 'string',
          title: t('workspaces.pages.blocks.settings.onInit.label'),
          description: t('workspaces.pages.blocks.settings.onInit.description'),
          'ui:widget': 'autocomplete',
          'ui:options': {
            autocomplete: 'events:listen',
          },
        },
        automation: {
          type: 'string',
          title: t('workspaces.pages.blocks.settings.automation.label'),
          description: t('workspaces.pages.blocks.settings.automation.description'),
          'ui:widget': 'select',
          'ui:options': {
            from: 'automations',
            filter: 'endpoint',
          },
        },
        updateOn: {
          type: 'string',
          title: t('workspaces.pages.blocks.settings.updateOn.label'),
          description: t('workspaces.pages.blocks.settings.updateOn.description'),
          'ui:widget': 'autocomplete',
          'ui:options': {
            autocomplete: 'events:emit',
          },
        },
      },
    }),
    [t],
  );
  const stylesSchema: Schema = useMemo(
    () => ({
      type: 'object',
      properties: {
        colorScheme: {
          type: 'string',
          'ui:widget': ColorSchemeSwitch,
        },
        styles: {
          type: 'string',
          'ui:widget': (props: FieldProps) => <CSSEditor {...props} />,
        },
      },
    }),
    [],
  );

  const initialOpenState = useRef(false);
  useEffect(() => {
    if (!initialOpenState.current) {
      initialOpenState.current = true;
      return;
    }
    trackEvent({
      name: `${open ? 'Open' : 'Close'} Details Panel`,
      action: 'click',
    });
  }, [open, trackEvent]);

  const onChange = useCallback(
    (schema: Schema) => (changedValues: any) => {
      const newValues = { ...values };
      Object.keys(schema?.properties || {}).forEach((k) => {
        newValues[k] = changedValues[k];
      });
      setValues(newValues);
    },
    [values],
  );

  const submit = useCallback(() => {
    onSave(values);
  }, [onSave, values]);

  const buttons = useMemo(
    () => [
      <div key="1" className="mx-4 !mt-2 flex flex-1 justify-end">
        <Button type="primary" htmlType="submit" disabled={disabled}>
          {t('common.save')}
        </Button>
      </div>,
    ],
    [disabled, t],
  );

  return (
    <Popover
      title={() => (
        <div className="flex flex-1 justify-between">
          {t('workspaces.pages.details.title')}
          <button
            onClick={() => {
              trackEvent({
                name: 'Close Details Panel by clicking button',
                action: 'click',
              });
              setOpen(false);
            }}
          >
            <CloseIcon />
          </button>
        </div>
      )}
      destroyTooltipOnHide
      content={() => (
        <Tabs
          className="flex flex-1"
          items={[
            {
              key: 'config',
              label: t('workspaces.blocks.builder.setup.label'),
              children: (
                <SchemaForm
                  schema={configSchema}
                  initialValues={values}
                  onChange={onChange(configSchema)}
                  onSubmit={submit}
                  buttons={buttons}
                />
              ),
              active: true,
            },
            {
              key: 'lifecycle',
              label: t('workspaces.blocks.builder.lifecycle.label'),
              children: (
                <SchemaForm
                  schema={lifecycleSchema}
                  initialValues={values}
                  onChange={onChange(lifecycleSchema)}
                  onSubmit={submit}
                  buttons={buttons}
                  utils={{
                    extractAutocompleteOptions,
                    extractSelectOptions,
                  }}
                />
              ),
            },
            {
              key: 'styles',
              label: t('workspaces.blocks.builder.style.label'),
              children: (
                <SchemaForm
                  schema={stylesSchema}
                  initialValues={values}
                  onChange={onChange(stylesSchema)}
                  onSubmit={submit}
                  buttons={buttons}
                />
              ),
            },
          ]}
          tabBarExtraContent={
            <ConfirmButton
              onConfirm={onDelete}
              confirmLabel={t('workspaces.pages.delete.confirm', {
                name: localize(value.name),
              })}
            >
              <TrashIcon />
              <span>{t('workspaces.pages.delete.label')}</span>
            </ConfirmButton>
          }
        />
      )}
      open={open}
      onOpenChange={(v) => {
        setOpen(v);
        onOpenChange?.(v);
      }}
      trigger={'click'}
      {...props}
    >
      {children}
    </Popover>
  );
};

export default EditDetails;
