import { Button, Segmented } from 'antd';
import { useTranslations } from 'next-intl';
import CodeIcon from '@/svgs/code.svg';
import EyeIcon from '@/svgs/eye-open.svg';
import EditIcon from '@/svgs/edit.svg';
import EditDetails from './EditDetails';
import styles from './app-instance.module.scss';
import { useTracking } from '@/providers/Tracking';
import IconButton from '@/components/IconButton';
import SettingsIcon from '@/svgs/gear.svg';

interface HeaderLeftProps {
  value: Prismeai.Page;
  displaySource: boolean;
  onDisplaySource: (state: boolean) => void;
  onDelete: () => void;
  onChange: (v: Prismeai.AppInstance) => void;
}

export const HeaderLeft = ({
  value,
  displaySource,
  onDisplaySource,
  onDelete,
  onChange,
}: HeaderLeftProps) => {
  const t = useTranslations();
  return (
    <>
      <EditDetails
        value={value}
        onSave={async (v) => {
          onChange({
            ...value,
            ...v,
          });
        }}
        onDelete={onDelete}
        context="apps"
      >
        <IconButton icon={<SettingsIcon />} />
      </EditDetails>
      <IconButton
        icon={<CodeIcon width="1.2rem" height="1.2rem" />}
        className={styles['header-button']}
        onClick={() => onDisplaySource(!displaySource)}
      >
        {displaySource ? t('workspaces.apps.source.close') : t('workspaces.apps.source.label')}
      </IconButton>
    </>
  );
};

interface HeaderRightProps {
  onSave: () => void;
  saving: boolean;
  displaySource: boolean;
  hasDocumentation: boolean;
  setViewMode: (v: number) => void;
}
export const HeaderRight = ({
  onSave,
  saving,
  hasDocumentation,
  displaySource,
  setViewMode,
}: HeaderRightProps) => {
  const t = useTranslations('workspaces');
  const { trackEvent } = useTracking();
  return (
    <>
      <Button
        key="save"
        onClick={() => onSave()}
        className={`${styles['header-button']} ${styles['save-btn']} ${displaySource ? styles['save-btn--visible'] : ''}`}
        type="primary"
        disabled={saving}
      >
        {t('apps.save.label')}
      </Button>
      {hasDocumentation && (
        <div key="doc">
          <div className="ml-3">
            <Segmented
              key="nav"
              options={[
                {
                  label: t('apps.doc'),
                  value: 0,
                  icon: <EyeIcon />,
                },
                {
                  label: t('apps.config'),
                  value: 1,
                  icon: <EditIcon />,
                },
              ]}
              onChange={(v) => {
                trackEvent({
                  name: `Show ${v === 0 ? 'Documentation' : 'Configuration'} tab`,
                  action: 'click',
                });
                setViewMode(+v);
              }}
              className="pr-segmented-accent"
            />
          </div>
        </div>
      )}
    </>
  );

  return null;
};
