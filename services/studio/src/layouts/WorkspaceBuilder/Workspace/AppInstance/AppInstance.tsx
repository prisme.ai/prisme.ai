import { validateAppInstance } from '@prisme.ai/validation';
import { useEffect, useMemo, useState, useCallback, useRef } from 'react';
import IFrame from '@/components/IFrame';
import SourceEdit from '@/components/SourceEdit';
import { useWorkspaceLayout } from '../Layout/context';
import { useAppInstance } from '@/providers/AppInstance';
import { useWorkspace } from '@/providers/Workspace';
import { ApiError } from '@prisme.ai/sdk';
import { replaceSilently, useUrls } from '@/utils/urls';
import useLocalizedText from '@/utils/useLocalizedText';
import styles from './app-instance.module.scss';
import LowCodeEditor from '@/components/LowCodeEditor';
import { HeaderLeft, HeaderRight } from './Header';
import AppEditor from '@/components/AppEditor';
import useTranslations from '@/i18n/useTranslations';
import Head from '@/components/Head';
import useRouter from '@/utils/useRouter';
import useWarningBeforeNavigate from '@/utils/useRouter/useWarningBeforeNavigate';
import equal from 'fast-deep-equal';
import { ValidationError } from '@/utils/useYaml/yaml';
import { useNotifications } from '@/providers/Notifications';

export const AppInstance = () => {
  const { appInstance, documentation, saveAppInstance, saving, uninstallApp } = useAppInstance();
  const { workspace } = useWorkspace();
  const { localize } = useLocalizedText();
  const t = useTranslations('workspaces');
  const { replace } = useRouter();
  const [, { photo }] = Object.entries(workspace.imports || {}).find(
    ([slug]) => slug === appInstance.slug,
  ) || [{}, {}];
  const [value, setValue] = useState(appInstance);
  const [displaySource, setDisplaySource] = useState(false);
  const [viewMode, setViewMode] = useState(documentation ? 0 : 1);
  const { generatePageUrl } = useUrls();
  const notification = useNotifications();

  const [dirty, setDirty] = useState(false);
  useWarningBeforeNavigate(dirty);
  const prevValue = useRef(value);
  useEffect(() => {
    if (equal(value, prevValue.current)) return;
    setDirty(true);
  }, [value]);

  const { setTitle } = useWorkspaceLayout();
  useEffect(() => {
    setTitle(
      t('apps.title', {
        name: localize(appInstance.slug),
        slug: appInstance.appSlug,
      }),
    );
  }, [localize, appInstance.slug, appInstance.appSlug, setTitle, t]);

  useEffect(() => {
    setValue(appInstance);
  }, [appInstance]);

  const save = useCallback(
    async (newValue = value) => {
      const prevSlug = appInstance.slug;
      try {
        const nextValue = await saveAppInstance(newValue);
        if (!nextValue) return null;
        notification.success({
          message: t('apps.saveSuccess'),
          placement: 'bottomRight',
        });
        if (prevSlug !== nextValue.slug) {
          replaceSilently(`/workspaces/${workspace.id}/apps/${nextValue.slug}`);
        }
        return nextValue;
      } catch (e) {
        const { details, error } = e as ApiError;
        const description = (
          <ul>
            {details ? (
              details.map(({ path, message }: any, key: number) => (
                <li key={key}>
                  {t.has(`openapi_${message}`)
                    ? t(`openapi_${message}`, {
                        path: path.replace(/^\.body\./, ''),
                        ns: 'errors',
                      })
                    : t('openapi')}
                </li>
              ))
            ) : (
              <li>
                {t.has(`apps.save.reason_${error}`)
                  ? t(`apps.save.reason_${error}`)
                  : t('apps.save.reason')}
              </li>
            )}
          </ul>
        );
        notification.error({
          message: t('apps.save.error'),
          description,
          placement: 'bottomRight',
        });
      }
    },
    [appInstance.slug, saveAppInstance, t, value, workspace.id, notification],
  );

  const onDelete = useCallback(() => {
    replace(`/workspaces/${workspace.id}`);
    uninstallApp();
    notification.success({
      message: t('apps.delete.toast'),
      placement: 'bottomRight',
    });
  }, [replace, uninstallApp, workspace.id, notification, t]);

  const mergeSource = useCallback(
    (source: any) => ({
      ...value,
      ...source,
      config: {
        ...value.config,
        value: source.config,
      },
    }),
    [value],
  );
  const [validationError, setValidationError] = useState<ValidationError>();
  const validateSource = useCallback(
    (json: any) => {
      const isValid = validateAppInstance(mergeSource(json));
      const [error] = validateAppInstance.errors || [];
      setValidationError(error as ValidationError);
      return isValid;
    },
    [mergeSource],
  );

  const source = useMemo(() => {
    const { appSlug, config = {}, ...source } = value;
    void appSlug;
    return { ...source, config: config && config.value };
  }, [value]);
  const setSource = useCallback(
    (source: any) => {
      setValue(mergeSource(source));
    },
    [mergeSource],
  );

  return (
    <>
      <Head
        title={`[${localize(workspace.name)}] 
        ${t('page_title', {
          elementName: localize(appInstance.slug),
        })}`}
        description=""
      />
      <LowCodeEditor
        header={{
          title: {
            value: appInstance.slug || '',
            icon: photo,
          },
          left: (
            <HeaderLeft
              value={value}
              displaySource={displaySource}
              onDisplaySource={setDisplaySource}
              onDelete={onDelete}
              onChange={({ slug, disabled }) => {
                const { config, ...prevValue } = value;
                const newValue = {
                  ...prevValue,
                  slug,
                  disabled,
                };
                setValue({ ...newValue, config });
                save(newValue);
              }}
            />
          ),
          right: (
            <HeaderRight
              onSave={save}
              saving={saving}
              displaySource={displaySource}
              setViewMode={setViewMode}
              hasDocumentation={!!documentation}
            />
          ),
        }}
      >
        {documentation && documentation.workspaceSlug && documentation.slug && (
          <IFrame
            src={generatePageUrl(documentation.workspaceSlug, documentation.slug)}
            className="flex flex-1"
          />
        )}
        {viewMode === 1 && (
          <div className={styles['app-instance-view-edit']}>
            <AppEditor
              schema={appInstance.config.schema}
              block={appInstance.config.block}
              appId={appInstance.slug || ''}
              key={appInstance.slug}
              config={value.config.value}
              onSave={(config) => {
                save({
                  ...value,
                  config: {
                    ...value.config,
                    value: config,
                  },
                });
              }}
            />
          </div>
        )}

        <SourceEdit
          value={source}
          onChange={setSource}
          onSave={() => save()}
          visible={displaySource}
          validate={validateSource}
          error={validationError}
        />
      </LowCodeEditor>
    </>
  );
};

export default AppInstance;
