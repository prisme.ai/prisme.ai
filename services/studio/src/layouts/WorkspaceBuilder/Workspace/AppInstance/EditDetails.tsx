import { ReactNode, useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { useTracking } from '@/providers/Tracking';
import useLocalizedText from '@/utils/useLocalizedText';
import ConfirmButton from '@/components/ConfirmButton';
import { SLUG_VALIDATION_REGEXP } from '@/utils/regex';
import { Button, Popover, PopoverProps, Tabs } from 'antd';
import { useTranslations } from 'next-intl';
import SchemaForm, { Schema } from '@/components/SchemaForm';
import CloseIcon from '@/svgs/close.svg';
import TrashIcon from '@/svgs/trash.svg';

interface EditDetailsprops extends Omit<PopoverProps, 'content'> {
  value: any;
  onSave: (values: any) => Promise<void | Record<string, string>>;
  onDelete: () => void;
  context?: string;
  disabled?: boolean;
  children: ReactNode;
}

export const EditDetails = ({
  value,
  onSave,
  onDelete,
  disabled,
  onOpenChange,
  children,
  ...props
}: EditDetailsprops) => {
  const t = useTranslations('workspaces');
  const { localize } = useLocalizedText();
  const { trackEvent } = useTracking();
  const [open, setOpen] = useState(false);
  const [values, setValues] = useState(value);

  useEffect(() => {
    setValues(value);
  }, [value]);

  const configSchema: Schema = useMemo(
    () => ({
      type: 'object',
      properties: {
        slug: {
          type: 'string',
          title: t('apps.details.slug.label'),
          pattern: SLUG_VALIDATION_REGEXP.source,
          errors: {
            pattern: t('automations.save.error_InvalidSlugError'),
          },
        },
        disabled: {
          type: 'boolean',
          title: t('apps.details.disabled.label'),
          description: t('apps.details.disabled.description'),
        },
      },
    }),
    [t],
  );

  const initialOpenState = useRef(false);
  useEffect(() => {
    if (!initialOpenState.current) {
      initialOpenState.current = true;
      return;
    }
    trackEvent({
      name: `${open ? 'Open' : 'Close'} Details Panel`,
      action: 'click',
    });
  }, [open, trackEvent]);

  const onChange = useCallback(
    (schema: Schema) => (changedValues: any) => {
      const newValues = { ...values };
      Object.keys(schema?.properties || {}).forEach((k) => {
        newValues[k] = changedValues[k];
      });
      setValues(newValues);
    },
    [values],
  );

  const submit = useCallback(() => {
    onSave(values);
  }, [onSave, values]);

  const buttons = useMemo(
    () => [
      <div key="1" className="mx-4 !mt-2 flex flex-1 justify-end">
        <Button type="primary" htmlType="submit" disabled={disabled}>
          {t('save', { ns: 'common' })}
        </Button>
      </div>,
    ],
    [disabled, t],
  );

  return (
    <Popover
      title={() => (
        <div className="flex flex-1 justify-between">
          {t('apps.details.title')}
          <button
            onClick={() => {
              trackEvent({
                name: 'Close Details Panel by clicking button',
                action: 'click',
              });
              setOpen(false);
            }}
          >
            <CloseIcon />
          </button>
        </div>
      )}
      destroyTooltipOnHide
      content={() => (
        <Tabs
          className="flex flex-1"
          items={[
            {
              key: 'config',
              label: t('blocks.builder.setup.label'),
              children: (
                <SchemaForm
                  schema={configSchema}
                  initialValues={values}
                  onChange={onChange(configSchema)}
                  onSubmit={submit}
                  buttons={buttons}
                />
              ),
              active: true,
            },
          ]}
          tabBarExtraContent={
            <ConfirmButton
              onConfirm={onDelete}
              confirmLabel={t('apps.delete.confirm', {
                name: localize(value.name),
              })}
            >
              <TrashIcon className="translate-y-[-2px]" />
              <span className="flex">{t('apps.delete.label')}</span>
            </ConfirmButton>
          }
        />
      )}
      trigger={'click'}
      open={open}
      onOpenChange={(v: boolean) => {
        setOpen(v);
        onOpenChange?.(v);
      }}
      {...props}
    >
      {children}
    </Popover>
  );
};

export default EditDetails;
