import { kebabCase } from 'lodash';
import { useCallback, useState } from 'react';
import { useWorkspace } from '@/providers/Workspace';
import useLocalizedText from '@/utils/useLocalizedText';
import ExportIcon from '@/svgs/export.svg';
import PublishModal from './PublishModal';
import VersionModal from './VersionModal';
import ArrowDown from '@/svgs/arrow-down.svg';
import ArrowUp from '@/svgs/arrow-up.svg';
import { useTranslations } from 'next-intl';
import Loading from '@/components/Loading';
import useApi from '@/utils/api/useApi';
import IconButton from '@/components/IconButton';
import AppstoreIcon from '@/svgs/appstore.svg';
import styles from '../settings.module.scss';

export const Packaging = () => {
  const api = useApi();
  const { workspace } = useWorkspace();
  const t = useTranslations('workspaces');
  const { localize } = useLocalizedText();
  const [exporting, setExporting] = useState(false);
  const [publishVisible, setPublishVisible] = useState(false);
  const [versionVisible, setVersionVisible] = useState<false | 'push' | 'pull'>(false);

  const onExport = useCallback(async () => {
    if (exporting) return;
    setExporting(true);
    const zip = await api.workspaces(workspace.id).versions.export();
    const a = document.createElement('a');
    a.style.display = 'none';
    a.setAttribute(
      'download',
      `workspace-${kebabCase(localize(workspace.name))}-${workspace.id}.zip`,
    );
    a.setAttribute('href', URL.createObjectURL(zip));
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
    setExporting(false);
  }, [exporting, localize, workspace.id, workspace.name, api]);

  const onPublishAsApp = useCallback(() => {
    setPublishVisible(true);
  }, []);

  const onVersion = useCallback((action: 'push' | 'pull') => {
    setVersionVisible(action);
  }, []);

  return (
    <>
      <IconButton
        className={styles['icon-button']}
        icon={<AppstoreIcon />}
        onClick={onPublishAsApp}
      >
        {t(`apps.publish.menuLabel`)}
      </IconButton>
      <IconButton
        className={styles['icon-button']}
        onClick={() => onVersion('push')}
        icon={<ArrowUp className="mr-2" />}
      >
        {t(`workspace.versions.create.label`)}
      </IconButton>
      <IconButton
        className={styles['icon-button']}
        onClick={() => onVersion('pull')}
        icon={<ArrowDown className="mr-2" />}
      >
        {t(`workspace.versions.pull.label`)}
      </IconButton>
      <IconButton
        className={styles['icon-button']}
        onClick={onExport}
        icon={exporting ? <Loading className="mr-2" /> : <ExportIcon className="mr-2" />}
      >
        {t(`workspace.versions.export.label`)}
      </IconButton>
      <PublishModal visible={publishVisible} close={() => setPublishVisible(false)} />
      <VersionModal visible={versionVisible} close={() => setVersionVisible(false)} />
    </>
  );
};

export default Packaging;
