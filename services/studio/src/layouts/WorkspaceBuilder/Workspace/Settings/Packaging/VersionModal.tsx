import { ReactNode, useCallback, useEffect, useMemo, useState } from 'react';
import { useWorkspace } from '@/providers/Workspace';
import { useDateFormat } from '@/utils/dates';
import { useTracking } from '@/providers/Tracking';
import { useTranslations } from 'next-intl';
import { Modal, Select, Tooltip } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import useApi from '@/utils/api/useApi';
import useRouter from '@/utils/useRouter';
import { useNotifications } from '@/providers/Notifications';
import { Link } from '@/i18n/routing';
import styles from '../settings.module.scss';

interface VersionModalProps {
  visible: false | 'push' | 'pull';
  close: () => void;
}

const LOCAL_REPOSITORY = '__local__';

const VersionModal = ({ visible, close }: VersionModalProps) => {
  const api = useApi();
  const [description, setDescription] = useState('');
  const [repository, setRepository] = useState(LOCAL_REPOSITORY);
  const notification = useNotifications();

  const { workspace } = useWorkspace();
  const t = useTranslations('workspaces');
  const dateFormat = useDateFormat();
  const { push } = useRouter();
  const { trackEvent } = useTracking();
  const { readRepositories, writeRepositories } = useMemo(() => {
    const repositories = [
      {
        label: 'Prisme.ai',
        value: LOCAL_REPOSITORY,
        mode: 'read-write' as string,
      },
    ].concat(
      Object.entries(workspace?.repositories || {})
        .filter(([, repo]) => repo.type !== 'archive')
        .map(([key, repo]) => ({
          label: repo?.name,
          value: key,
          mode: repo.mode || 'read-write',
        })),
    );

    return {
      readRepositories: repositories.filter((cur) => cur.mode.includes('read')),
      writeRepositories: repositories.filter((cur) => cur.mode.includes('write')),
    };
  }, [workspace?.repositories]);

  const [version, setVersion] = useState('latest');
  const [versions, setVersions] = useState<{ label: ReactNode; value: string }[]>();
  useEffect(() => {
    if (visible !== 'pull') {
      return;
    }
    async function fetchVersions() {
      const events = await api.getEvents(workspace.id, {
        type: 'workspaces.versions.published',
        limit: 10,
      });
      const versions = events
        .filter((event) => {
          if (repository === LOCAL_REPOSITORY && event?.payload?.version?.repository?.id) {
            return false;
          }

          if (
            repository !== LOCAL_REPOSITORY &&
            (!event?.payload?.version?.repository?.id ||
              repository !== event?.payload?.version?.repository?.id)
          ) {
            return false;
          }
          return true;
        })
        .flatMap(({ payload: { version } }) =>
          version
            ? [
                {
                  label: (
                    <Tooltip
                      title={
                        <div>
                          <div>{version.name}</div>
                          <div>
                            {dateFormat(new Date(version.createdAt), {
                              relative: true,
                            })}
                          </div>
                        </div>
                      }
                      placement="right"
                    >
                      {version.description || version.name}
                    </Tooltip>
                  ),
                  value: version.name,
                },
              ]
            : [],
        );
      if (versions.length) {
        setVersion('latest');
        if (versions.length === 10) {
          versions.unshift({
            label: <div>{t('workspace.versions.pull.version.latest')}</div>,
            value: 'latest',
          });
          versions.push({
            label: (
              <button
                onClick={(e) => {
                  e.preventDefault();
                  push('/workspaces/KgIMCs2?type=workspaces.versions.published');
                  close();
                }}
              >
                {t('workspace.versions.pull.version.older')}
              </button>
            ),
            value: '',
          });
        }
      }
      setVersions(versions);
    }
    fetchVersions();
  }, [close, dateFormat, push, t, visible, workspace.id, repository, api]);

  const onConfirm = useCallback(
    async (mode: 'pull' | 'push') => {
      trackEvent({
        name: mode === 'push' ? 'Create a new Version' : 'Pull a version',
        action: 'click',
      });
      try {
        const remoteRepository =
          repository === LOCAL_REPOSITORY
            ? undefined
            : {
                id: repository,
              };
        if (mode === 'push') {
          await api
            .workspaces(workspace.id)
            .versions.create({ description, repository: remoteRepository });
        } else if (mode === 'pull') {
          await api.workspaces(workspace.id).versions.rollback(version || 'latest', {
            repository: remoteRepository,
          });
        }
        notification.success({
          message: (
            <Link
              href={
                mode === 'push'
                  ? `/workspaces/${workspace.id}?type=workspaces.versions.published`
                  : `/workspaces/${workspace.id}?type=workspaces.imported`
              }
              className={styles['notification-link']}
            >
              {mode === 'push'
                ? t('workspace.versions.create.success')
                : t('workspace.versions.pull.success')}
            </Link>
          ),
          placement: 'bottomRight',
        });
      } catch (err) {
        notification.error({
          message: `${err}`,
          placement: 'bottomRight',
        });
      }
    },
    [trackEvent, repository, t, workspace.id, description, version, api, notification],
  );

  return (
    <Modal
      open={!!visible}
      title={
        visible === 'push'
          ? t('workspace.versions.create.label')
          : t('workspace.versions.pull.label')
      }
      onOk={() => {
        if (visible) {
          onConfirm(visible);
        }
        close();
      }}
      okText={
        visible === 'push' ? t('apps.publish.confirm.ok') : t('workspace.versions.pull.confirm_ok')
      }
      cancelText={t('cancel', { ns: 'common' })}
      onCancel={close}
    >
      <div className="p-10">
        <div className="mb-10">
          {visible === 'push'
            ? t('workspace.versions.create.description')
            : t('workspace.versions.pull.description')}
        </div>
        {visible == 'push' ? (
          <label>
            {t('workspace.versions.create.name')}
            <TextArea
              value={description}
              onChange={(event) => setDescription(event.target.value)}
            />
          </label>
        ) : null}
        <label className="flex flex-col">
          {t('workspace.versions.repository')}
          <Select
            value={repository}
            options={visible === 'push' ? writeRepositories : readRepositories}
            onChange={(value) => setRepository(value)}
          />
        </label>

        {versions && visible === 'pull' && (
          <label className="mb-2 flex flex-col">
            {t('workspace.versions.pull.version.label')}
            <Select value={version} options={versions} onChange={setVersion} />
          </label>
        )}
      </div>
    </Modal>
  );
};

export default VersionModal;
