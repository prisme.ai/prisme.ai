import { useCallback, useMemo, useState } from 'react';
import SchemaForm from '@/components/SchemaForm/SchemaForm';

import { useWorkspace, Workspace } from '@/providers/Workspace';
import { WORKSPACE_SLUG_VALIDATION_REGEXP } from '@/utils/regex';
import { Schema } from '@/components/SchemaForm';
import { Button, notification } from 'antd';
import useTranslations from '@/i18n/useTranslations';

export const Display = () => {
  const { workspace, saveWorkspace, saving } = useWorkspace();
  const t = useTranslations('workspaces');
  const [values, setValues] = useState(workspace);

  const displaySchema: Schema = useMemo(
    () => ({
      type: 'object',
      properties: {
        name: {
          type: 'string',
          title: t('workspace.details.name.label'),
        },
        slug: {
          type: 'string',
          title: t('workspace.details.slug.label'),
          pattern: WORKSPACE_SLUG_VALIDATION_REGEXP.source,
          errors: {
            pattern: t('workspace.details.slug.error'),
          },
        },
        description: {
          type: 'localized:string',
          title: t('workspace.details.description.label'),
          'ui:widget': 'textarea',
          'ui:options': { textarea: { rows: 6 } },
        },
        photo: {
          type: 'string',
          title: t('workspace.details.photo.label'),
          'ui:widget': 'upload',
          'ui:options': {
            upload: {
              public: true,
            },
          },
        },
      },
    }),
    [t],
  );

  const onChange = useCallback(
    (schema: Schema) => (changedValues: any) => {
      const {
        automations,
        pages,
        blocks,
        imports,
        id,
        createdAt,
        updatedAt,
        registerWorkspace,
        ...newValues
      } = values;
      void automations, pages, blocks, imports, id, createdAt, updatedAt, registerWorkspace;
      Object.keys(schema?.properties || {}).forEach((k) => {
        newValues[k as keyof typeof newValues] = changedValues[k];
      });
      setValues(newValues as Workspace);
    },
    [values],
  );

  const submit = useCallback(async () => {
    try {
      await saveWorkspace(values);
    } catch (err) {
      notification.error({
        message:
          (err as any)?.error === 'InvalidFile' && (err as any)?.details?.maxSize
            ? t('InvalidFileError', {
                type: 'image',
                maxSize: (err as any)?.details?.maxSize,
                ns: 'errors',
              })
            : t('unknown', { errorName: (err as any)?.error, ns: 'errors' }),
        placement: 'bottomRight',
      });
      return;
    }
    if (values.slug !== workspace.slug) {
      try {
        await saveWorkspace({ ...values, slug: values.slug });
      } catch {
        return {
          slug: t('workspace.details.slug.unique'),
        };
      }
    }
    notification.success({
      message: t('expert.save.confirm'),
      placement: 'bottomRight',
    });
  }, [saveWorkspace, t, values, workspace.slug]);

  const buttons = useMemo(
    () => [
      <div key="1" className="mx-4 !mt-2 flex flex-1 justify-end">
        <Button type="primary" htmlType="submit" disabled={saving}>
          {t('save', { ns: 'common' })}
        </Button>
      </div>,
    ],
    [saving, t],
  );

  return (
    <SchemaForm
      schema={displaySchema}
      initialValues={values}
      onChange={onChange(displaySchema)}
      onSubmit={submit}
      buttons={buttons}
    />
  );
};

export default Display;
