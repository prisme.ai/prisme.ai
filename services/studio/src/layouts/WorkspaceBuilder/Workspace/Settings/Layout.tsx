import Link from 'next/link';
import { ReactNode, useEffect } from 'react';
import { useWorkspaceLayout } from '../Layout/context';
import { useWorkspace } from '@/providers/Workspace';
import styles from './settings.module.scss';
import { useTranslations } from 'next-intl';
import { Tabs } from 'antd';
import { useSelectedLayoutSegments } from 'next/navigation';

const tabs = ['display', 'config', 'users', 'packaging', 'advanced'] as const;

interface LayoutProps {
  children: ReactNode;
}
export const Layout = ({ children }: LayoutProps) => {
  const {
    workspace: { id },
  } = useWorkspace();
  const t = useTranslations('workspaces');
  const { setTitle } = useWorkspaceLayout();
  useEffect(() => {
    setTitle(t('workspace.sections.settings'));
  }, [setTitle, t]);
  const [selected] = useSelectedLayoutSegments() || [];
  return (
    <div className={styles['settings-ctn']}>
      <nav className={styles['settings-nav']}>
        <Tabs
          activeKey={selected}
          items={tabs.map((key) => ({
            key,
            label: (
              <Link href={`/workspaces/${id}/settings/${key}`}>
                {t(`workspace.settings.${key}.title`)}
              </Link>
            ),
          }))}
        />
      </nav>
      <div className={styles['settings-view']}>{children}</div>
    </div>
  );
};

export default Layout;
