import { useCallback, useMemo, useRef, useState } from 'react';
import SchemaForm from '@/components/SchemaForm/SchemaForm';
import ArgumentsEditor from '@/components/SchemaFormBuilder/ArgumentsEditor';
import { useWorkspace } from '@/providers/Workspace';
import useLocalizedText from '@/utils/useLocalizedText';
import { useTranslations } from 'next-intl';
import { Schema } from '@/components/SchemaForm';
import { Button, Collapse, notification } from 'antd';

export const Config = () => {
  const { workspace, saveWorkspace, saving } = useWorkspace();
  const t = useTranslations('workspaces');
  const [configValues, setConfigValues] = useState(workspace.config?.value || {});
  const [configSchemaValue, setConfigSchemaValue] = useState<Schema>(
    workspace.config?.schema || {},
  );
  const { localizeSchemaForm } = useLocalizedText();
  const ignoreValueChange = useRef(false);

  const configSchema: Schema = useMemo(
    () => ({
      type: 'object',
      title: t('workspace.details.config.value.label'),
      properties: configSchemaValue && localizeSchemaForm(configSchemaValue),
      additionalProperties: !configSchemaValue,
    }),
    [localizeSchemaForm, t, configSchemaValue],
  );
  const schemaSchema: Schema = useMemo(
    () => ({
      title: t('workspace.details.config.schema.label'),
      description: t('workspace.details.config.schema.description'),
      'ui:widget': ArgumentsEditor,
    }),
    [t],
  );

  const onConfigChanged = useCallback(
    (changed: Record<string, any>) => {
      if (ignoreValueChange.current) {
        return;
      }
      if (!configSchemaValue || configSchemaValue.additionalProperties) {
        setConfigValues(changed);
        return;
      }

      const newConfigValues = configValues;
      Object.keys(configSchemaValue || {}).forEach((k) => {
        newConfigValues[k as keyof typeof newConfigValues] = changed[k];
      });
      setConfigValues(newConfigValues);
    },
    [configSchemaValue, configValues],
  );

  const onSchemaChanged = useCallback((changed: Record<string, any>) => {
    ignoreValueChange.current = true;

    setConfigSchemaValue(changed);

    ignoreValueChange.current = false;
  }, []);

  const submit = useCallback(async () => {
    try {
      await saveWorkspace({
        config: { value: configValues, schema: configSchemaValue },
      });
    } catch (err) {
      notification.error({
        message:
          (err as any)?.error === 'InvalidFile' && (err as any)?.details?.maxSize
            ? t('InvalidFileError', {
                ns: 'errors',
                type: 'image',
                maxSize: (err as any)?.details?.maxSize,
              })
            : t('unknown', { ns: 'errors', errorName: (err as any)?.error }),
        placement: 'bottomRight',
      });
      return;
    }
    notification.success({
      message: t('expert.save.confirm'),
      placement: 'bottomRight',
    });
  }, [saveWorkspace, t, configValues, configSchemaValue]);

  const buttons = useMemo(
    () => [
      <div key="1" className="mx-4 !mt-2 flex flex-1 justify-end">
        <Button type="primary" htmlType="submit" disabled={saving}>
          {t('save', { ns: 'common' })}
        </Button>
      </div>,
    ],
    [saving, t],
  );

  return (
    <>
      <SchemaForm
        schema={configSchema}
        initialValues={configValues}
        onChange={onConfigChanged}
        onSubmit={submit}
        buttons={buttons}
      />
      <Collapse
        items={[
          {
            label: t('workspace.details.config.schema.label'),
            children: (
              <SchemaForm
                schema={schemaSchema}
                initialValues={configSchemaValue}
                onChange={onSchemaChanged}
                onSubmit={submit}
                buttons={buttons}
              />
            ),
          },
        ]}
      />
    </>
  );
};

export default Config;
