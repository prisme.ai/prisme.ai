import { useCallback, useEffect, useRef, useState } from 'react';
import {
  DisplayedSourceType,
  useWorkspaceLayout,
} from '@/layouts/WorkspaceBuilder/Workspace/Layout/context';
import { useWorkspace } from '@/providers/Workspace';
import styles from './settings.module.scss';
import CodeIcon from '@/svgs/code.svg';
import UsersIcon from '@/svgs/users.svg';
import KeyIcon from '@/svgs/key.svg';
import WarningIcon from '@/svgs/warning.svg';
import { useTranslations } from 'next-intl';
import Loading from '@/components/Loading';
import SchemaForm, { Schema } from '@/components/SchemaForm';
import useApi from '@/utils/api/useApi';
import IconButton from '@/components/IconButton';
import ConfirmButton from '@/components/ConfirmButton';
import { useNotifications } from '@/providers/Notifications';
import { useRouter } from 'next/navigation';

interface SecretValue {
  [key: string]: any;
}

interface AdditionalValue {
  key: string;
  value: any;
}

export const Advanced = () => {
  const api = useApi();
  const t = useTranslations('workspaces');
  const {
    workspace: { id: workspaceId, secrets: { schema = {} } = {} },
  } = useWorkspace();
  const { displaySource, sourceDisplayed } = useWorkspaceLayout();
  const { deleteWorkspace } = useWorkspace();
  const [hadSecrets, setHadSecrets] = useState<boolean>(false);
  const [values, setValues] = useState<SecretValue>({});
  const [additionalValues, setAdditionalValues] = useState<AdditionalValue[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [deletedKeys, setDeletedKeys] = useState<string[]>([]);
  const prevAdditionalValuesRef = useRef<AdditionalValue[]>([]);
  const schemaRef = useRef(schema);
  const notification = useNotifications();
  const { push } = useRouter();

  useEffect(() => {
    schemaRef.current = schema;
  }, [schema]);

  const fetchSecrets = useCallback(async () => {
    const secrets = await api.getWorkspaceSecrets(workspaceId);
    if (secrets && Object.keys(secrets).length > 0) setHadSecrets(true);
    const adaptedSecrets: SecretValue = {};
    const extraValues: AdditionalValue[] = [];

    // Split secrets between schema-based and additional
    Object.entries(secrets).forEach(([key, valueObj]) => {
      if (schemaRef.current[key]) {
        adaptedSecrets[key] = valueObj.value;
      } else {
        extraValues.push({ key, value: valueObj.value });
      }
    });

    setValues(adaptedSecrets);
    prevAdditionalValuesRef.current = extraValues;
    setAdditionalValues(extraValues);
    setLoading(false);
  }, [workspaceId, api]);

  useEffect(() => {
    fetchSecrets();
  }, [fetchSecrets]);

  const handleValuesChange = (newValues: SecretValue) => {
    setValues(newValues);
  };

  const handleAdditionalValuesChange = (newValues: AdditionalValue[]) => {
    const prevValues = prevAdditionalValuesRef.current;

    const deleted = Array.isArray(prevValues)
      ? prevValues
          .filter(
            (prevItem) =>
              Array.isArray(newValues) &&
              !newValues.some((newItem) => newItem.key === prevItem.key),
          )
          .map((deletedItem) => deletedItem.key)
      : [];

    setDeletedKeys((prevDeletedKeys) => {
      const updatedDeletedKeys = [...prevDeletedKeys, ...deleted];
      return updatedDeletedKeys;
    });

    setAdditionalValues(newValues);
    prevAdditionalValuesRef.current = newValues;
  };

  const onConfirm = useCallback(async () => {
    if (loading) return;
    try {
      // Combine schema values with additional values
      const mergedValues: SecretValue = {
        ...values,
        ...additionalValues.reduce((acc, { key, value }) => ({ ...acc, [key]: value }), {}),
      };

      const adaptedValues = Object.entries(mergedValues).reduce((acc, [key, value]) => {
        acc[key] = { value };
        return acc;
      }, {} as SecretValue);

      await api.updateWorkspaceSecrets(workspaceId, adaptedValues);

      await Promise.all(
        deletedKeys.map(async (key) => {
          try {
            await api.deleteWorkspaceSecrets(workspaceId, key);
          } catch (err) {
            console.error(`Error deleting secret key ${key}:`, err);
          }
        }),
      );

      setDeletedKeys([]);

      notification.success({
        message: t('workspace.secrets.edit.success'),
        placement: 'bottomRight',
      });
    } catch (err) {
      const error = err as Error;
      notification.error({
        message: t('unknown', { errorName: error.message, ns: 'errors' }),
        placement: 'bottomRight',
      });
      console.error(error);
      return null;
    }
  }, [t, workspaceId, values, additionalValues, deletedKeys, loading, api, notification]);

  const onDisplaySource = useCallback(() => {
    displaySource(
      sourceDisplayed === DisplayedSourceType.Config
        ? DisplayedSourceType.None
        : DisplayedSourceType.Config,
    );
  }, [displaySource, sourceDisplayed]);

  const onDisplayRoles = useCallback(() => {
    displaySource(
      sourceDisplayed === DisplayedSourceType.Roles
        ? DisplayedSourceType.None
        : DisplayedSourceType.Roles,
    );
  }, [displaySource, sourceDisplayed]);

  return (
    <>
      <IconButton
        className={styles['icon-button']}
        icon={<CodeIcon className={styles['settings-btn-icon']} />}
        onClick={onDisplaySource}
      >
        {t(`expert.show`)}
      </IconButton>
      <IconButton
        className={styles['icon-button']}
        icon={<UsersIcon className={styles['settings-btn-icon']} />}
        onClick={onDisplayRoles}
      >
        {t(`expert.security`)}
      </IconButton>
      {!hadSecrets && (
        <IconButton
          className={styles['icon-button']}
          icon={<KeyIcon className={styles['settings-btn-icon']} />}
        >
          {t('workspace.secrets.edit.description')}
        </IconButton>
      )}
      {loading ? (
        <Loading />
      ) : (
        <>
          <SchemaForm
            schema={{
              type: 'object',
              properties: schema as Schema,
            }}
            initialValues={values}
            onChange={handleValuesChange}
            buttons={[]}
          />
          <SchemaForm
            schema={{
              type: 'array',
              items: {
                type: 'object',
                properties: {
                  key: {
                    type: 'string',
                  },
                  value: {
                    type: 'string',
                  },
                },
              },
            }}
            initialValues={additionalValues}
            onChange={handleAdditionalValuesChange}
            onSubmit={onConfirm}
          />
        </>
      )}
      <ConfirmButton
        className={`${styles['icon-button']} ${styles['icon-button--delete']}`}
        onConfirm={() => {
          push('/workspaces');
          deleteWorkspace();
          notification.success({
            message: t('workspace.delete.toast'),
            placement: 'bottomRight',
          });
        }}
        confirmLabel={t('workspace.delete.confirm')}
      >
        <WarningIcon />
        {t(`workspace.delete.label`)}
      </ConfirmButton>
    </>
  );
};

export default Advanced;
