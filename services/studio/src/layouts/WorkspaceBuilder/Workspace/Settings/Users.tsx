import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { usePermissions } from '@/providers/Permissions';
import { useUser } from '@/providers/User';
import { useWorkspace } from '@/providers/Workspace';
import { useTracking } from '@/providers/Tracking';
import { useTranslations } from 'next-intl';
import { Button, Input, notification, Select, Table, Tooltip } from 'antd';
import TrashIcon from '@/svgs/trash.svg';
import useRouter from '@/utils/useRouter';

type SelectOption = {
  value: Prismeai.Role;
  label: string;
};
const subjectType = 'workspaces';
const Users = () => {
  const t = useTranslations();
  const {
    usersPermissions,
    getUsersPermissions,
    addUserPermissions,
    removeUserPermissions,
    roles,
    getRoles,
  } = usePermissions();
  const { user } = useUser();
  const {
    workspace: { name, id: subjectId },
  } = useWorkspace();

  const { push } = useRouter();
  const { trackEvent } = useTracking();

  const rolesOptions = useMemo<SelectOption[]>(
    () => roles.map((role) => ({ value: role.name, label: role.name })),
    [roles],
  );

  const [emailInput, setEmailInput] = useState('');
  const [roleInput, setRoleInput] = useState<Prismeai.Role>(rolesOptions[0].value);

  const generateRowButtons = useCallback(
    (onDelete: Function) => (
      <div className="flex flex-row justify-center">
        <Tooltip title={t('workspaces.share.delete')}>
          <Button onClick={() => onDelete()}>
            <TrashIcon />
          </Button>
        </Tooltip>
      </div>
    ),
    [t],
  );

  const initialFetch = useRef(async () => {
    getUsersPermissions(subjectType, subjectId);
    getRoles(subjectType, subjectId);
  });

  useEffect(() => {
    initialFetch.current();
  }, [initialFetch]);

  const userId = user?.id;

  const dataSource = useMemo(() => {
    const data = usersPermissions.get(`${subjectType}:${subjectId}`);

    if (!data) {
      return [];
    }

    const rows = data
      .filter(({ target }) => !target?.public)
      .map(({ target, permissions }) => ({
        key: target?.id,
        displayName: target?.displayName,
        role: permissions.role,
        actions: generateRowButtons(() => {
          trackEvent({
            name: 'Remove user permission',
            action: 'click',
          });
          removeUserPermissions(subjectType, subjectId, target);
          if (target?.id === userId) {
            // User is removing himself his access to the workspace
            push('/workspaces');
            notification.success({
              message: t('workspaces.share.leave', { name }),
              placement: 'bottomRight',
            });
          }
        }),
      }));

    return rows;
  }, [
    generateRowButtons,
    name,
    push,
    removeUserPermissions,
    subjectId,
    t,
    trackEvent,
    userId,
    usersPermissions,
  ]);

  const onSubmit = useCallback(() => {
    trackEvent({
      name: 'Add user pesmission',
      action: 'click',
    });
    if (emailInput === user.email) {
      notification.warning({
        message: t('workspaces.share.notme'),
        placement: 'bottomRight',
      });
      return;
    }
    addUserPermissions(subjectType, subjectId, {
      target: {
        email: emailInput,
      },
      permissions: {
        role: roleInput,
      },
    });
  }, [addUserPermissions, emailInput, roleInput, subjectId, t, trackEvent, user.email]);

  return (
    <div className="w-[44rem] space-y-5">
      <div className="flex flex-grow flex-row items-center justify-center">
        <Input
          placeholder={t('workspaces.share.email')}
          value={emailInput}
          onChange={({ target: { value } }) => setEmailInput(value)}
          className="!rounded-l-[0.94rem] !rounded-r-[0] !border-r-[0]"
        />
        <Select
          value={roleInput}
          onChange={(value) => {
            setRoleInput(value);
          }}
          options={rolesOptions}
          className="pr-noLeftSelect w-40"
        />
        <Button
          htmlType="submit"
          type="primary"
          onClick={onSubmit}
          className="flex !h-[2.5rem] !w-[9.375rem] items-center justify-center !rounded-[0.94rem]"
        >
          {t('common.add')}
        </Button>
      </div>
      <Table
        dataSource={dataSource}
        columns={[
          {
            title: t('workspaces.share.displayName'),
            dataIndex: 'displayName',
            key: 'displayName',
          },
          {
            title: t('workspaces.share.role'),
            dataIndex: 'role',
            key: 'role',
          },
          {
            title: t('workspaces.share.actions'),
            dataIndex: 'actions',
            key: 'actions',
            width: '15%',
          },
        ]}
        bordered
        pagination={{
          defaultPageSize: 10,
          position: ['bottomRight'],
          responsive: true,
          pageSizeOptions: [10, 20, 50],
        }}
        scroll={{ y: 500 }}
      />
    </div>
  );
};

export default Users;
