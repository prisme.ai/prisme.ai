import StatIcon from '@/svgs/stat.svg';
import { ApiError } from '@prisme.ai/sdk';
import { AppUsageMetricsWithPhoto } from './useWorkspaceUsage';
import { useTranslations } from 'next-intl';
import WarningIcon from '@/svgs/warning.svg';
import styles from './usage.module.scss';

interface UsagesProps {
  appsUsages: AppUsageMetricsWithPhoto[];
  error?: ApiError;
}

export default function AppsUsage({ appsUsages, error }: UsagesProps) {
  const t = useTranslations('user');

  return (
    <div className={styles['usages']}>
      <div className={styles['apps-usages']}>
        {error && (
          <div className={styles['apps-usages-error']}>
            <WarningIcon className={styles['apps-usages-error-icon']} />
            <div>{t('usage.old')}</div>
          </div>
        )}
        {appsUsages && appsUsages.length > 0 && (
          <>
            <div className={styles['title']}>
              <StatIcon width={17} height={17} />
              <div className={styles['title-text']}>{t('usage.apps.title')}</div>
            </div>
            {appsUsages.map((workspaceUsage) => (
              <div key={workspaceUsage.slug} className={styles['apps-usages-app']}>
                {workspaceUsage.photo ? (
                  <img
                    src={workspaceUsage.photo}
                    className={styles['apps-usages-app-photo']}
                    alt=""
                  />
                ) : (
                  <div
                    className={`${styles['apps-usages-app-photo']} ${styles['apps-usages-app-photo--text']}`}
                  >
                    {workspaceUsage.slug.substring(0, 2)}
                  </div>
                )}
                <div className={styles['apps-usages-app-content']}>
                  <div className={styles['apps-usages-app-name']}>{workspaceUsage.slug}</div>
                  <div className={styles['apps-usages-app-value']}>
                    {workspaceUsage.total.custom.billing}
                  </div>
                </div>
              </div>
            ))}
          </>
        )}
      </div>
    </div>
  );
}
