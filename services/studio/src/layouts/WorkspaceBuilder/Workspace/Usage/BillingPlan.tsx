import React from 'react';
import LightningIcon from '@/svgs/lightning.svg';
import { useLocale, useTranslations } from 'next-intl';
import { useEnv } from '@/providers/Env';
import IFrame from '@/components/IFrame';
import styles from './usage.module.scss';

interface BillingPlanProps {
  wpName: string;
  wpId: string;
  userEmail: string;
}

const BillingPlan = ({ wpId, userEmail }: BillingPlanProps) => {
  const { BILLING_HOME = '' } = useEnv<{ BILLING_HOME: string }>();
  const t = useTranslations('user');
  const language = useLocale();
  if (!BILLING_HOME) return null;
  return (
    <>
      <div className="flex flex-col">
        <div className={styles['title']}>
          <LightningIcon width={17} height={17} alt="" />
          <div className={styles['title-text']}>{t('billing.title')}</div>
        </div>
        <IFrame
          src={`${BILLING_HOME.replace(
            /\{\{lang\}\}/,
            language,
          )}?workspaceId=${wpId}&email=${userEmail}`}
        />
      </div>
    </>
  );
};

export default BillingPlan;
