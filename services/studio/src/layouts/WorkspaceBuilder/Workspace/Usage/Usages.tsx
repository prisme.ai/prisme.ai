import StatIcon from '@/svgs/stat.svg';
import { useEffect, useRef } from 'react';
import { useLocale, useTranslations } from 'next-intl';
import IFrame from '@/components/IFrame';
import { useEnv } from '@/providers/Env';
import styles from './usage.module.scss';

interface UsagesProps {
  wpId: string;
}

const Usages = ({ wpId }: UsagesProps) => {
  const { BILLING_USAGE = '' } = useEnv<{ BILLING_USAGE: string }>();
  const t = useTranslations('user');
  const language = useLocale();

  const iframeRef = useRef<HTMLIFrameElement>(null);
  useEffect(() => {
    if (!iframeRef.current) return;

    iframeRef.current.style.height = '450px';
    window.addEventListener('message', (e) => {
      if (!iframeRef.current) return;
      if (!BILLING_USAGE) return;
      const { hostname: origin } = new URL(e.origin);
      const { hostname: allowed } = new URL(BILLING_USAGE);
      if (origin != allowed) return;
      if (e.data.type !== 'update height') return;
      if (!e.data.height) return;
      iframeRef.current.style.height = `${e.data.height}px`;
    });
  }, [BILLING_USAGE]);

  if (!BILLING_USAGE) return null;

  return (
    <div className={styles['usages']}>
      <div className={styles['title']}>
        <StatIcon width={17} height={17} />
        <div className={styles['title-text']}>{t('usage.title')}</div>
      </div>
      <IFrame
        ref={iframeRef}
        src={`${BILLING_USAGE.replace(/\{\{lang\}\}/, language)}?workspaceId=${wpId}`}
      />
    </div>
  );
};

export default Usages;
