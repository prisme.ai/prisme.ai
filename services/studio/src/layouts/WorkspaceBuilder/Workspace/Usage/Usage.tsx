import { useCallback, useEffect } from 'react';
import { usePermissions } from '@/providers/Permissions';

import { useUser } from '@/providers/User';
import { useWorkspaceLayout } from '../Layout/context';
import { useWorkspace } from '@/providers/Workspace';
import BillingPlan from './BillingPlan';
import Usages from './Usages';
import { useWorkspaceUsage } from './useWorkspaceUsage';
import { useTranslations } from 'next-intl';
import Loading from '@/components/Loading';
import AppsUsage from './AppsUsages';
import Head from '@/components/Head';
import useLocalizedText from '@/utils/useLocalizedText';

export const Usage = () => {
  const t = useTranslations('workspaces');
  const { localize } = useLocalizedText();
  const {
    user: { email },
  } = useUser();

  const { setTitle } = useWorkspaceLayout();
  useEffect(() => {
    setTitle(t('usage.title'));
  }, [setTitle, t]);

  const { workspace } = useWorkspace();

  const { usage, loading, error } = useWorkspaceUsage();

  const { getUsersPermissions } = usePermissions();

  const initialFetch = useCallback(async () => {
    getUsersPermissions('workspaces', `${workspace.id}`);
  }, [getUsersPermissions, workspace]);

  useEffect(() => {
    initialFetch();
  }, [initialFetch]);

  return (
    <>
      <Head title={`[${localize(workspace.name)}] ${t('usage.title')}`} description="" />
      {loading ? (
        <Loading />
      ) : (
        <>
          <BillingPlan wpName={workspace.name} wpId={workspace.id} userEmail={email as string} />
          <Usages wpId={workspace.id} />
          {usage?.apps && <AppsUsage appsUsages={usage.apps} error={error} />}
        </>
      )}
    </>
  );
};

export default Usage;
