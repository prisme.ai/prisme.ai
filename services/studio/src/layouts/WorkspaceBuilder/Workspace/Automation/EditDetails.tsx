import { ReactNode, useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { useTracking } from '@/providers/Tracking';
import useLocalizedText from '@/utils/useLocalizedText';
import ConfirmButton from '@/components/ConfirmButton';
import ArgumentsEditor from '@/components/SchemaFormBuilder/ArgumentsEditor';
import { SLUG_VALIDATION_REGEXP } from '@/utils/regex';
import { useTranslations } from 'next-intl';
import SchemaForm, { Schema } from '@/components/SchemaForm';
import { Button, Popover, PopoverProps, Tabs } from 'antd';
import CloseIcon from '@/svgs/close.svg';
import TrashIcon from '@/svgs/trash.svg';
import styles from './automation.module.scss';

interface EditDetailsprops extends Omit<PopoverProps, 'content'> {
  value: any;
  onSave: (values: any) => Promise<void | Record<string, string>>;
  onDelete: () => void;
  context?: string;
  disabled?: boolean;
  children: ReactNode;
}

export const EditDetails = ({
  value,
  onSave,
  onDelete,
  disabled,
  onOpenChange,
  children,
  ...props
}: EditDetailsprops) => {
  const t = useTranslations('workspaces');
  const tCommon = useTranslations('common');
  const { localize } = useLocalizedText();
  const { trackEvent } = useTracking();
  const [open, setOpen] = useState(false);
  const [values, setValues] = useState(value);

  useEffect(() => {
    setValues(value);
  }, [value]);

  const configSchema: Schema = useMemo(
    () => ({
      type: 'object',
      properties: {
        name: {
          type: 'localized:string',
          title: t('automations.details.name.label'),
        },
        slug: {
          type: 'string',
          title: t('automations.details.slug.label'),
          pattern: SLUG_VALIDATION_REGEXP.source,
          errors: {
            pattern: t('automations.save.error_InvalidSlugError'),
          },
        },
        description: {
          type: 'localized:string',
          title: t('automations.details.description.label'),
          'ui:widget': 'textarea',
          'ui:options': { textarea: { rows: 6 } },
        },
      },
      'ui:options': {
        grid: [[['name', 'slug'], ['description']], [['arguments']], [['private']], [['disabled']]],
      },
    }),
    [t],
  );

  const schemaSchema: Schema = useMemo(
    () => ({
      type: 'object',
      properties: {
        arguments: {
          'ui:widget': ArgumentsEditor,
        },
        validateArguments: {
          type: 'boolean',
          title: t('automations.details.validateArguments.title'),
          description: t('automations.details.validateArguments.description'),
        },
      },
    }),
    [t],
  );
  const advancedSchema: Schema = useMemo(
    () => ({
      type: 'object',
      properties: {
        private: {
          type: 'boolean',
          title: t('automations.details.private.label'),
          description: t('automations.details.private.description'),
        },
        disabled: {
          type: 'boolean',
          title: t('automations.details.disabled.label'),
          description: t('automations.details.disabled.description'),
        },
      },
      'ui:options': {
        grid: [[['name', 'slug'], ['description']], [['arguments']], [['private']], [['disabled']]],
      },
    }),
    [t],
  );

  const initialOpenState = useRef(false);
  useEffect(() => {
    if (!initialOpenState.current) {
      initialOpenState.current = true;
      return;
    }
    trackEvent({
      name: `${open ? 'Open' : 'Close'} Details Panel`,
      action: 'click',
    });
  }, [open, trackEvent]);

  const onChange = useCallback(
    (schema: Schema) => (changedValues: any) => {
      const newValues = { ...values };
      Object.keys(schema?.properties || {}).forEach((k) => {
        newValues[k] = changedValues[k];
      });
      setValues(newValues);
    },
    [values],
  );

  const submit = useCallback(() => {
    onSave(values);
  }, [onSave, values]);

  const buttons = useMemo(
    () => [
      <div key="1" className={styles['details-buttons']}>
        <Button type="primary" htmlType="submit" disabled={disabled}>
          {tCommon('save')}
        </Button>
      </div>,
    ],
    [disabled, tCommon],
  );

  return (
    <Popover
      title={() => (
        <div className={styles['details-title']}>
          {t('automations.details.title')}
          <button
            onClick={() => {
              trackEvent({
                name: 'Close Details Panel by clicking button',
                action: 'click',
              });
              setOpen(false);
            }}
          >
            <CloseIcon />
          </button>
        </div>
      )}
      destroyTooltipOnHide
      content={() => (
        <Tabs
          className={styles['details-content']}
          items={[
            {
              key: 'config',
              label: t('automations.details.setup'),
              children: (
                <SchemaForm
                  schema={configSchema}
                  initialValues={values}
                  onChange={onChange(configSchema)}
                  onSubmit={submit}
                  buttons={buttons}
                />
              ),
              active: true,
            },
            {
              key: 'schema',
              label: t('automations.arguments.title'),
              children: (
                <SchemaForm
                  schema={schemaSchema}
                  initialValues={values}
                  onChange={onChange(schemaSchema)}
                  onSubmit={submit}
                  buttons={buttons}
                />
              ),
            },
            {
              key: 'advanced',
              label: t('automations.details.advanced'),
              children: (
                <SchemaForm
                  schema={advancedSchema}
                  initialValues={values}
                  onChange={onChange(advancedSchema)}
                  onSubmit={submit}
                  buttons={buttons}
                />
              ),
            },
          ]}
          tabBarExtraContent={
            <ConfirmButton
              onConfirm={onDelete}
              confirmLabel={t('automations.delete.confirm', {
                name: localize(value.name),
              })}
            >
              <TrashIcon className={styles['details-delete-btn']} />
              {t('automations.delete.label')}
            </ConfirmButton>
          }
        />
      )}
      rootClassName={styles['details-overlay']}
      trigger={'click'}
      open={open}
      onOpenChange={(v) => {
        setOpen(v);
        onOpenChange?.(v);
      }}
      {...props}
    >
      {children}
    </Popover>
  );
};

export default EditDetails;
