import { Button, Space, Tooltip } from 'antd';
import { useTranslations } from 'next-intl';
import CodeIcon from '@/svgs/code.svg';
import CopyIcon from '@/svgs/copy.svg';
import EditDetails from './EditDetails';
import Loading from '@/components/Loading';
import PlayPanel from './PlayPanel';
import styles from './automation.module.scss';
import IconButton from '@/components/IconButton';
import SettingsIcon from '@/svgs/gear.svg';

interface HeaderLeftProps {
  value: Prismeai.Page;
  duplicate: () => void;
  duplicating: boolean;
  displaySource: boolean;
  onDisplaySource: (state: boolean) => void;
  onDelete: () => void;
  onChange: (v: Prismeai.Automation) => void;
}

export const HeaderLeft = ({
  value,
  duplicate,
  duplicating,
  displaySource,
  onDisplaySource,
  onDelete,
  onChange,
}: HeaderLeftProps) => {
  const t = useTranslations();

  return (
    <>
      <Tooltip title={t('workspaces.pages.details.title')} placement="bottom">
        <EditDetails
          value={value}
          onSave={async (v) => {
            onChange({
              ...value,
              ...v,
            });
          }}
          onDelete={onDelete}
          context="automations"
        >
          <IconButton icon={<SettingsIcon />} />
        </EditDetails>
      </Tooltip>

      <Tooltip title={t('workspaces.pages.duplicate.help')} placement="bottom">
        <IconButton
          icon={<CopyIcon width="1.2rem" height="1.2rem" />}
          className={styles['header-button']}
          onClick={duplicate}
          disabled={duplicating}
        >
          {t('common.duplicate')}
        </IconButton>
      </Tooltip>
      <Tooltip title={t('workspaces.pages.source.help')} placement="bottom">
        <IconButton
          icon={<CodeIcon width="1.2rem" height="1.2rem" />}
          className={styles['header-button']}
          onClick={() => onDisplaySource(!displaySource)}
        >
          <span className="flex">
            {displaySource
              ? t('workspaces.pages.source.close')
              : t('workspaces.pages.source.label')}
          </span>
        </IconButton>
      </Tooltip>
    </>
  );
};

interface HeaderRightProps {
  onSave: () => void;
  dirty: boolean;
  saving: boolean;
  error?: boolean;
}
export const HeaderRight = ({ onSave, dirty, saving, error }: HeaderRightProps) => {
  const t = useTranslations('workspaces');
  return (
    <>
      <Button
        onClick={() => {
          onSave();
        }}
        disabled={!dirty || saving || error}
        type="primary"
        className={`${styles['save-button']} ${styles['header-button']}`}
      >
        <Space>
          {t('automations.save.label')}
          {saving && <Loading />}
        </Space>
      </Button>
      <Tooltip title={t('automations.play.help')} placement="left">
        <PlayPanel />
      </Tooltip>
    </>
  );
};
