import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import useKeyboardShortcut from '@/utils/useKeyboardShortcut';
import useLocalizedText from '@/utils/useLocalizedText';
import { useWorkspace } from '@/providers/Workspace';
import { useAutomation } from '@/providers/Automation';
import { ApiError } from '@prisme.ai/sdk';
import SourceEdit from '@/components/SourceEdit';
import { validateAutomation } from '@prisme.ai/validation';
import { incrementName } from '@/utils/incrementName';
import { replaceSilently } from '@/utils/urls';
import { useTracking } from '@/providers/Tracking';
import { useWorkspaceLayout } from '../Layout/context';
import LowCodeEditor from '@/components/LowCodeEditor';
import { HeaderLeft, HeaderRight } from './Header';
import AutomationBuilder from '@/components/AutomationBuilder';
import { useNotifications } from '@/providers/Notifications';
import equal from 'fast-deep-equal';
import Head from '@/components/Head';
import useTranslations from '@/i18n/useTranslations';
import useRouter from '@/utils/useRouter';
import useWarningBeforeNavigate from '@/utils/useRouter/useWarningBeforeNavigate';
import deepClone from '@/utils/deepClone';
import { useInstrutionsSnippets } from '@/utils/useSnippets/useSnippets';
import { useInstructionsLinks } from '@/utils/useLinks/useLinks';
import { ValidationError } from '@/utils/useYaml/yaml';
import { YAMLException } from 'js-yaml';

const cleanInstruction = (instruction: Prismeai.Instruction) => {
  const [type] = Object.keys(instruction);
  if (type === 'conditions') {
    cleanConditions(instruction as { conditions: Prismeai.Conditions });
  }
  if (type === 'repeat') {
    cleanDo((instruction as Prismeai.Repeat).repeat);
  }
  if (type === 'all') {
    cleanDo(instruction as Prismeai.All);
  }
};
const cleanConditions = (instruction: { conditions: Prismeai.Conditions }) => {
  Object.keys(instruction.conditions).forEach((key) => {
    instruction.conditions[key] = instruction.conditions[key].filter(
      (i) => Object.keys(i).length === 1,
    );
    instruction.conditions[key].forEach(cleanInstruction);
  });
};
const isDoList = (
  parent: { do: Prismeai.InstructionList } | { all: Prismeai.InstructionList },
): parent is { do: Prismeai.InstructionList } => {
  return !!(parent as { do: Prismeai.InstructionList }).do;
};
const isAllList = (
  parent: { do: Prismeai.InstructionList } | { all: Prismeai.InstructionList },
): parent is { all: Prismeai.InstructionList } => {
  return !!(parent as { all: Prismeai.InstructionList }).all;
};
const cleanDo = (parent: Prismeai.Automation | Prismeai.Repeat['repeat'] | Prismeai.All) => {
  const filter = (instruction: Prismeai.Instruction) => Object.keys(instruction).length === 1;
  let doList: Prismeai.InstructionList = [];
  if (isDoList(parent)) {
    doList = parent.do = parent.do.filter(filter);
  }
  if (isAllList(parent)) {
    doList = parent.all = parent.all.filter(filter);
  }
  doList.forEach(cleanInstruction);
};
const cleanAutomation = (automation: Prismeai.Automation) => {
  if (!automation.do) return automation;
  cleanDo(automation);
  return automation;
};

export const Automation = () => {
  const { automation, saveAutomation, saving, deleteAutomation } = useAutomation();
  const t = useTranslations('workspaces');
  const { localize } = useLocalizedText();
  const { workspace, createAutomation } = useWorkspace();
  const [value, setValue] = useState(deepClone(automation));
  const [displaySource, setDisplaySource] = useState(false);
  const [duplicating, setDuplicating] = useState(false);
  const { trackEvent } = useTracking();
  const { setAdvancedMode, setTitle } = useWorkspaceLayout();
  const notification = useNotifications();
  useEffect(() => {
    setTitle(
      t('automations.title', {
        name: localize(automation.name),
        slug: automation.slug,
      }),
    );
  }, [automation.name, automation.slug, localize, setTitle, t]);
  const dirty = useMemo(() => !equal(automation, value), [value, automation]);
  const { push, replace } = useRouter();

  useWarningBeforeNavigate(dirty);

  const save = useCallback(
    async (newValue = value) => {
      if (newValue !== value) setValue(deepClone(newValue));
      try {
        const saved = await saveAutomation(cleanAutomation(newValue));
        if (saved) {
          if (automation.slug !== saved.slug) {
            replaceSilently(`/workspaces/${workspace.id}/automations/${saved.slug}`);
          }
          setValue(deepClone(saved));
        }
        notification.success({
          message: t('automations.save.toast'),
          placement: 'bottomRight',
        });
      } catch (e) {
        const { details, error } = e as ApiError;
        const description = (
          <ul>
            {details ? (
              details.map(({ path, message }: any, key: number) => (
                <li key={key}>
                  {t('openapi', {
                    context: message,
                    path: path.replace(/^\.body\./, ''),
                  })}
                </li>
              ))
            ) : (
              <li>
                {t.has(`automations.save.reason_${error}`)
                  ? t(`automations.save.reason_${error}`)
                  : t('automations.save.reason')}
              </li>
            )}
          </ul>
        );
        notification.error({
          message: t('automations.save.error'),
          description,
          placement: 'bottomRight',
        });
      }
    },
    [automation.slug, saveAutomation, t, value, workspace.id, notification],
  );

  // Need to get the latest version with the latest value associated
  const saveAfterChange = useRef(save);
  useEffect(() => {
    saveAfterChange.current = save;
  }, [save]);

  useKeyboardShortcut([
    {
      key: 's',
      meta: true,
      command: (e) => {
        e.preventDefault();
        trackEvent({
          name: 'Save with shortcut',
          action: 'keydown',
        });
        save();
      },
    },
  ]);

  const confirmDeleteAutomation = useCallback(() => {
    replace(`/workspaces/${workspace.id}`);
    deleteAutomation();
    notification.success({
      message: t('automations.delete.toast'),
      placement: 'bottomRight',
    });
  }, [deleteAutomation, replace, t, workspace, notification]);

  const duplicate = useCallback(async () => {
    trackEvent({
      name: 'Duplicate',
      action: 'click',
    });
    if (!automation.slug) return;
    setDuplicating(true);
    const newName =
      typeof automation.name === 'string'
        ? incrementName(
            automation.name,
            Object.values(workspace.automations || {}).map(({ name }) => localize(name)),
          )
        : Object.entries(automation.name).reduce(
            (prev, [key, name]) => ({
              ...prev,
              [key]: incrementName(
                name,
                Object.values(workspace.automations || {}).map(({ name: n }) =>
                  typeof n === 'string' ? n : n[key] || localize(n),
                ),
              ),
            }),
            {},
          );

    const newSlug = incrementName(
      `${automation.slug}`,
      Object.keys(workspace.automations || {}).map((slug) => slug),
      '{{name}} {{n}}',
    );

    await createAutomation({
      ...automation,
      name: newName,
      slug: newSlug,
    });

    push(`/workspaces/${workspace.id}/automations/${newSlug}`);
    setDuplicating(false);
    notification.success({
      message: t('automations.duplicate.success'),
      placement: 'bottomRight',
    });
  }, [
    automation,
    createAutomation,
    localize,
    push,
    t,
    trackEvent,
    workspace.automations,
    workspace.id,
    notification,
  ]);

  useEffect(() => {
    return () => {
      setAdvancedMode(false);
    };
  }, [setAdvancedMode]);

  const [validationError, setValidationError] = useState<ValidationError>();
  const validateSource = useCallback((json: any) => {
    const isValid = validateAutomation(json);
    const [error] = validateAutomation.errors || [];
    setValidationError(error as ValidationError);
    return isValid;
  }, []);
  useEffect(() => {
    if (validateSource(value)) {
      setValidationError(undefined);
    }
  }, [value, validateSource]);
  const [syntaxError, setSyntaxError] = useState<YAMLException | null>(null);

  const source = useMemo(() => {
    return { slug: automation.slug, ...value };
  }, [value, automation.slug]);

  const setSource = useCallback((source: any) => {
    setValue(deepClone(source));
  }, []);

  const snippets = useInstrutionsSnippets();
  const links = useInstructionsLinks();

  return (
    <>
      <Head
        title={`[${localize(workspace.name)}] 
          ${t('page_title', {
            elementName: localize((automation || { name: '' }).name),
          })}`}
        description=""
      />
      <LowCodeEditor
        header={{
          title: {
            value: value.name || '',
            onChange: (name) => {
              setValue(
                deepClone({
                  ...value,
                  name,
                }),
              );
            },
          },
          left: (
            <HeaderLeft
              value={value}
              duplicate={duplicate}
              duplicating={duplicating}
              displaySource={displaySource}
              onDisplaySource={setDisplaySource}
              onChange={(p) => {
                setValue(deepClone(p));
                save(p);
              }}
              onDelete={confirmDeleteAutomation}
            />
          ),
          right: (
            <HeaderRight
              onSave={save}
              dirty={dirty}
              saving={saving}
              error={!!validationError || !!syntaxError}
            />
          ),
        }}
      >
        <SourceEdit
          value={source}
          onChange={setSource}
          onSave={save}
          visible={displaySource}
          error={validationError}
          snippets={snippets}
          links={links}
          onError={setSyntaxError}
        />
        <AutomationBuilder value={value} onChange={setValue} />
      </LowCodeEditor>
    </>
  );
};

export default Automation;
