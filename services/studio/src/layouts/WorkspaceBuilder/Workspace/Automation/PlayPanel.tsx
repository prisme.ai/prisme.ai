import Link from 'next/link';
import { useCallback, useMemo, useState } from 'react';
import { useAutomation } from '@/providers/Automation';
import { useWorkspace } from '@/providers/Workspace';
import Storage from '@/utils/Storage';
import WorskpaceIcon from '@/svgs/workspace-simple.svg';
import components from '@/components/SchemaFormInWorkspace/schemaFormComponents';
import PlayIcon from '@/svgs/play.svg';
import { useTracking } from '@/providers/Tracking';
import useLocalizedText from '@/utils/useLocalizedText';
import copy from '@/utils/copy';
import SchemaForm, { Schema } from '@/components/SchemaForm';
import { useTranslations } from 'next-intl';
import { Collapse, Popover, Tooltip } from 'antd';
import Loading from '@/components/Loading';
import CloseIcon from '@/svgs/close.svg';
import CopyIcon from '@/svgs/copy.svg';
import styles from './automation.module.scss';
import useApi from '@/utils/api/useApi';
import CodeEditor from '@/components/CodeEditor';

interface PreviewResultProps {
  result: string;
  link: string;
}
const PreviewResult = ({ result, link }: PreviewResultProps) => {
  const t = useTranslations('workspaces');
  const prettyPreview = useMemo(() => {
    if (typeof result === 'string') {
      if (result.match(/^http/)) {
        if (result.match(/\.(png|jpeg|jpg|png|svg)$/)) {
          // eslint-disable-next-line @next/next/no-img-element
          return <img src={result} alt="" />;
        }
        return <a href={result}>{result}</a>;
      }
    }
    return null;
  }, [result]);
  const details = (
    <>
      <CodeEditor
        readOnly
        value={result}
        mode="json"
        className={styles['play-result-code-editor']}
        inline
      />
      <Link href={link} className="mt-4 text-right">
        {t('automations.play.activity')}
      </Link>
    </>
  );
  if (prettyPreview) {
    return (
      <>
        {prettyPreview}
        <Collapse
          className={styles['play-result-details']}
          items={[
            {
              label: t('automations.play.details'),
              children: details,
            },
          ]}
        />
      </>
    );
  }
  return details;
};

const PlayView = () => {
  const api = useApi();
  const {
    workspace: { id: wId },
  } = useWorkspace();
  const { automation } = useAutomation();
  const [running, setRunning] = useState(false);
  const lsKey = `__run_automation_with_${automation.slug}`;
  const [values, setValues] = useState(Storage.get(lsKey));
  const [result, setResult] = useState('');
  const [error, setError] = useState('');
  const [correlationId, setCorrelationId] = useState('');
  const t = useTranslations('workspaces');
  const { trackEvent } = useTracking();
  const { localizeSchemaForm } = useLocalizedText();

  const saveValues = useCallback(
    (values: any) => {
      setValues(values);
      Storage.set(lsKey, values);
    },
    [lsKey],
  );

  const run = useCallback(async () => {
    trackEvent({
      name: 'Run automation in Play panel',
      action: 'click',
    });
    if (!automation.slug) return;
    setRunning(true);
    setResult('');
    setError('');
    setCorrelationId('');
    try {
      const res = await api.testAutomation({
        workspaceId: wId,
        automation: automation.slug,
        payload: values,
      });
      if (typeof res === 'string') {
        const { ['x-correlation-id']: correlationId = '' } = api.lastReceivedHeaders || {};
        setCorrelationId(correlationId);
        setResult(res);
      } else {
        const { headers: { ['x-correlation-id']: correlationId = '' } = {}, ...result } = res;
        setCorrelationId(correlationId);
        setResult(result);
      }
    } catch (e) {
      setError((e as Error).message);
    }

    setRunning(false);
  }, [automation.slug, trackEvent, values, wId, api]);

  const schema = useMemo(() => {
    const schema = localizeSchemaForm({
      type: 'object',
      properties: {
        ...automation.arguments,
        ...(automation.when?.endpoint
          ? {
              query: {
                title: t('automations.play.query.label'),
                description: t('automations.play.query.description'),
              },
              body: {
                title: t('automations.play.body.label'),
                description: t('automations.play.body.description'),
              },
            }
          : {}),
        ...(automation.when?.events?.length || 0 > 0
          ? {
              payload: {
                title: t('automations.play.payload.label'),
                description: t('automations.play.payload.description'),
              },
            }
          : {}),
      },
    } as Schema);
    if (Object.keys(schema.properties || {}).length === 0) {
      setValues({});
    }
    return schema;
  }, [
    automation.arguments,
    automation.when?.endpoint,
    automation.when?.events?.length,
    localizeSchemaForm,
    t,
  ]);

  const hasParam = Object.keys(schema.properties || {}).length > 0;

  const copyCurl = useCallback(() => {
    trackEvent({
      name: 'Copy automation play as cURL',
      action: 'click',
    });
    const payload = JSON.stringify(values);
    const cmd = `curl '${api.host}/workspaces/${wId}/test/${automation.slug}' \\
  -H 'content-type: application/json' \\
  -H 'authorization: Bearer ${api.token}' \\
  --data-raw '{"payload": ${payload}}' \\
  --compressed`;
    copy(cmd);
  }, [automation.slug, trackEvent, values, wId, api]);

  return (
    <div className={`${styles['play']} ${hasParam ? styles['play--has-parameters'] : ''}`}>
      {hasParam && (
        <div className={styles['play-parameters']}>
          <div className={styles['play-parameters-title']}>{t('automations.play.parameters')}</div>
          <SchemaForm
            schema={schema}
            onSubmit={run}
            onChange={saveValues}
            initialValues={values}
            buttons={[<button type="submit" key="submit" className="hidden"></button>]}
            components={components}
            initialFieldObjectVisibility={false}
          />
        </div>
      )}
      <div className={styles['play-run']}>
        <div className={styles['play-run-header']}>
          <div className={styles['play-run-header-title']}>{t('automations.play.results')}</div>
          <div className={styles['play-run-header-buttons']}>
            <Tooltip title={t('automations.play.curl.copy')}>
              <button
                className={styles['play-run-header-button-copy']}
                type="button"
                onClick={copyCurl}
              >
                <CopyIcon />
              </button>
            </Tooltip>
            <button
              className={`${styles['play-run-header-button-run']} ${running ? styles['play-result-header-button-run--running'] : ''}`}
              onClick={run}
              disabled={running}
            >
              <PlayIcon />
              {t('automations.play.label')}
            </button>
          </div>
        </div>

        <div
          className={`${styles['play-result']} ${!running && !result && !error ? styles['play-result--empty'] : ''}`}
        >
          {!running && !result && !error && (
            <>
              <WorskpaceIcon width="100px" alt="" className={styles['play-result-icon']} />
              {t.rich('automations.play.empty', {
                button: (children) => (
                  <button onClick={run} className={styles['play-result-button-start']}>
                    {children}
                  </button>
                ),
              })}
            </>
          )}
          {running && <Loading />}
          {!running && error && (
            <div className={styles['play-error']}>
              <div className={styles['play-error-icon']}>
                <CloseIcon />
              </div>
              <div className="text-base">{error}</div>
              <button onClick={run} className={styles['play-result-button-start']}>
                Try again
              </button>
            </div>
          )}
          {!running && result && (
            <>
              <PreviewResult
                result={typeof result === 'string' ? result : JSON.stringify(result, null, '  ')}
                link={`/workspaces/${wId}?source.correlationId=${correlationId}`}
              />
            </>
          )}
        </div>
      </div>
    </div>
  );
};

export const PlayPanel = (props: any) => {
  const t = useTranslations('workspaces');
  const { trackEvent } = useTracking();
  const [open, setOpen] = useState(false);

  return (
    <Popover
      title={() => (
        <div className={styles['play-popover-title']}>
          {t('automations.play.title')}
          <button onClick={() => setOpen(false)}>
            <CloseIcon />
          </button>
        </div>
      )}
      destroyTooltipOnHide
      content={PlayView}
      placement="bottom"
      open={open}
      onOpenChange={(open) => {
        trackEvent({
          name: `${open ? 'Open' : 'Close'} Play Panel`,
          action: 'click',
        });
        setOpen(open);
      }}
      trigger={'click'}
      {...props}
    >
      <button className={styles['play-popover-button']}>
        <PlayIcon />
      </button>
    </Popover>
  );
};

export default PlayPanel;
