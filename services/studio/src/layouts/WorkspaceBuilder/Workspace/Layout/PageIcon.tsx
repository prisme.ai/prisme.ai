import Color from 'color';
import FileIcon from '@/svgs/file.svg';

interface PageIconProps {
  color: string;
  width?: number | string;
  height?: number | string;
}

export const PageIcon = ({ color, width, height }: PageIconProps) => {
  const backgroundColor = new Color(color).fade(0.8).toString();
  return (
    <div
      className="flex justify-center rounded-[3px] p-[3px]"
      style={{ width, height, color, backgroundColor }}
    >
      <FileIcon />
    </div>
  );
};

export default PageIcon;
