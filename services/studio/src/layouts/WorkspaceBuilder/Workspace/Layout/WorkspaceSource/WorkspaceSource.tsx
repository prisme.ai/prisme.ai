import { FC, useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { Workspace } from '@prisme.ai/sdk';
import useYaml from '@/utils/useYaml';
import { ValidationError } from '@/utils/useYaml/yaml';
import { validateWorkspace, validateWorkspaceSecurity } from '@prisme.ai/validation';
import CodeEditor from '@/components/CodeEditor/lazy';
import { DisplayedSourceType, useWorkspaceLayout } from '../context';
import { useWorkspace } from '@/providers/Workspace';
import { useTranslations } from 'next-intl';
import { Button, Space } from 'antd';
import Loading from '@/components/Loading';
import styles from './workspace-source.module.scss';
import useApi from '@/utils/api/useApi';

interface WorkspaceSourceProps {
  onLoad?: () => void;
  sourceDisplayed?: DisplayedSourceType;
}
export const WorkspaceSource: FC<WorkspaceSourceProps> = ({ onLoad, sourceDisplayed }) => {
  const api = useApi();
  const t = useTranslations('workspaces');
  const {
    workspace: {
      id,
      pages,
      automations,
      imports,
      createdAt,
      createdBy,
      updatedAt,
      blocks,
      ...workspace
    },
  } = useWorkspace();
  void pages, automations, imports, createdAt, createdBy, updatedAt, blocks;
  const { onSaveSource } = useWorkspaceLayout();
  const { setInvalid, setNewSource, invalid, saving, displaySource } = useWorkspaceLayout();
  const [value, setValue] = useState<string | undefined>();
  const { toJSON, toYaml } = useYaml();
  const ref = useRef<HTMLDivElement>(null);

  const initYaml = useCallback(async () => {
    try {
      if (sourceDisplayed === DisplayedSourceType.Config) {
        const newValue = await toYaml(workspace);
        setValue(newValue);
      } else if (sourceDisplayed === DisplayedSourceType.Roles) {
        // Without this, api.getWorkspaceSecurity is fetched with every keystroke ...
        if (value?.length) {
          return;
        }
        const security = await api.getWorkspaceSecurity(id);
        const newValue = await toYaml(security);
        setValue(newValue);
      }
    } catch (e) {}
  }, [sourceDisplayed, toYaml, workspace, value?.length, id, api]);

  useEffect(() => {
    initYaml();
  }, [initYaml]);

  const checkSyntaxAndReturnYAML = useCallback(
    async (value: string) => {
      if (!workspace || value === undefined) return;
      try {
        return { ...(await toJSON<Workspace>(value)), id };
      } catch (e) {
        // TODO reproduire les annotations avec Monaco
        //const { mark, message } = e as YAMLException;
      }
    },
    [id, toJSON, workspace],
  );

  const update = useCallback(
    async (newValue: string) => {
      try {
        const json = await checkSyntaxAndReturnYAML(newValue);
        if (!json) return;
        const validate =
          sourceDisplayed === DisplayedSourceType.Config
            ? validateWorkspace
            : validateWorkspaceSecurity;
        validate(json);
        setInvalid((validate.errors as ValidationError[]) || false);
        setNewSource(json);
      } catch (e) {}
    },
    [checkSyntaxAndReturnYAML, setInvalid, setNewSource, sourceDisplayed],
  );

  useEffect(() => {
    if (!invalid || !value) {
      return;
    }
  }, [invalid, value]);

  const save = useCallback(() => {
    onSaveSource();
  }, [onSaveSource]);

  const shortcuts = useMemo(
    () => [
      {
        name: t('expert.save.help'),
        exec: save,
        bindKey: {
          mac: 'cmd-s',
          win: 'ctrl-s',
        },
      },
    ],
    [save, t],
  );

  useEffect(() => {
    onLoad?.();
  }, [onLoad]);

  if (value === undefined) return null;

  return (
    <div className={styles['workspace-source-ctn']} ref={ref}>
      <div className={styles['workspace-source-header']}>
        <div className={styles['workspace-source-header-title']}>
          {t(`workspace.source.${sourceDisplayed}`)}
        </div>
        <Button
          className={styles['workspace-source-header-btn']}
          onClick={() => displaySource(DisplayedSourceType.None)}
        >
          Fermer
        </Button>
        <Button
          className={styles['workspace-source-header-btn']}
          onClick={save}
          disabled={saving}
          type="primary"
        >
          <Space>
            {t('automations.save.label')}
            {saving && <Loading />}
          </Space>
        </Button>
      </div>
      <CodeEditor
        className={styles['workspace-source-editor']}
        mode="yaml"
        value={value}
        onChange={update}
        shortcuts={shortcuts}
      />
    </div>
  );
};

export default WorkspaceSource;
