import { useCallback, useEffect } from 'react';
import IconApps from '@/svgs/apps.svg';
import useLocalizedText from '@/utils/useLocalizedText';
import { useWorkspace } from '@/providers/Workspace';
import AppsProvider, { useApps } from '@/providers/Apps';
import SearchInput from '@/components/SearchInput';
import useScrollListener from '@/utils/useScrollListener';
import { incrementName } from '@/utils/incrementName';
import { TrackingCategory, useTracking } from '@/providers/Tracking';
import { useTranslations } from 'next-intl';
import { Button, Modal, notification, Tooltip } from 'antd';
import Loading from '@/components/Loading';
import styles from './appstore.module.scss';
import useRouter from '@/utils/useRouter';

interface AppStoreProps {
  visible: boolean;
  onCancel: () => void;
}

export const AppsStore = ({ visible, onCancel }: AppStoreProps) => {
  const t = useTranslations('workspaces');
  const { localize } = useLocalizedText();
  const { ref, bottom } = useScrollListener<HTMLDivElement>({ margin: -1 });
  const { apps, loading, filters, setFilters, hasMore, fetchNextApps } = useApps();
  const {
    installApp,
    workspace,
    workspace: { id: workspaceId },
  } = useWorkspace();
  const { push } = useRouter();
  const { trackEvent } = useTracking();

  const onAppClick = useCallback(
    async (id: string) => {
      trackEvent({
        name: 'Add App',
        action: 'click',
      });
      try {
        const slug = incrementName(
          id,
          Object.values(workspace.imports || {}).map(({ slug }) => slug),
          '{{name}}-{{n}}',
        );

        await installApp({
          appSlug: id,
          slug,
        });
        push(`/workspaces/${workspaceId}/apps/${slug}`);
      } catch (e) {
        notification.error({
          message: t('errors.unknown', { errorName: (e as Error).message }),
          placement: 'bottomRight',
        });
      }
      onCancel();
    },
    [t, installApp, onCancel, push, trackEvent, workspace.imports, workspaceId],
  );

  useEffect(() => {
    if (bottom) {
      fetchNextApps();
    }
  }, [bottom, fetchNextApps]);

  return (
    <Modal
      onCancel={onCancel}
      open={visible}
      footer={null}
      title={t('apps.store.title')}
      width="80vw"
    >
      <div className={styles['container']}>
        <div className={styles['search']}>
          <SearchInput
            value={filters.query || ''}
            onChange={({ target: { value } }) => {
              trackEvent({
                name: 'Search App',
                action: 'keydown',
              });
              setFilters({
                query: value,
              });
            }}
            placeholder={t('apps.search')}
            autoFocus
          />
        </div>
        <div ref={ref} className={styles['list']}>
          {loading && <Loading />}
          {!loading &&
            Array.from(apps).map(({ slug, description, photo }) => (
              <button key={slug} className={styles['list-item']} onClick={() => onAppClick(slug)}>
                <div className={styles['list-item-icon-ctn']}>
                  {photo ? (
                    <img
                      src={photo}
                      className={styles['list-item-icon']}
                      alt={t('apps.photoAlt')}
                    />
                  ) : (
                    <IconApps width={80} height={80} className={styles['list-item-icon']} />
                  )}
                </div>
                <div className={styles['list-item-texts']}>
                  <h4 className={styles['list-item-texts-title']}>{slug}</h4>
                  <Tooltip title={localize(description)}>
                    <div className={styles['list-item-texts-description']}>
                      {localize(description)}
                    </div>
                  </Tooltip>
                </div>
              </button>
            ))}
          {hasMore && <Button onClick={fetchNextApps}>{t('events.more')}</Button>}
        </div>
      </div>
    </Modal>
  );
};

const AppStoreWithProvider = (props: AppStoreProps) => {
  return (
    <TrackingCategory category="Apps">
      <AppsProvider>
        <AppsStore {...props} />
      </AppsProvider>
    </TrackingCategory>
  );
};

export default AppStoreWithProvider;
