'use client';

import React, { ReactNode, useCallback, useEffect, useState } from 'react';
import workspaceLayoutContext, { DisplayedSourceType, WorkspaceLayoutContext } from './context';
import useLocalizedText from '@/utils/useLocalizedText';
import Storage from '@/utils/Storage';
import AppsStore from './AppsStore';
import Navigation from './Navigation';
import { useWorkspace } from '@/providers/Workspace';
import { incrementName } from '@/utils/incrementName';
import { TrackingCategory, useTracking } from '@/providers/Tracking';
import { usePermissions } from '@/providers/Permissions';
import HomeIcon from '@/svgs/builder/home.svg';
import ChartIcon from '@/svgs/builder/chart.svg';
import { notification } from 'antd';
import { BlocksProvider } from '@/providers/BlocksProvider';
import { useSelectedLayoutSegments } from 'next/navigation';
import { useLocale } from 'next-intl';
import WorkspaceSource from './WorkspaceSource';
import { BlockProvider } from '@/blocks/Provider';
import Loading from '@/components/Loading';
import ProductLayout from '@/blocks/blocks/ProductLayout';
import styles from './workspace-layout.module.scss';
import useApi from '@/utils/api/useApi';
import SchemaFormConfigProvider from '@/components/SchemaForm/SchemaFormConfigContext';
import { ApiError } from '@prisme.ai/sdk';
import BlockWidget from '@/components/SchemaFormWidgets/BlockWidget';
import useTranslations from '@/i18n/useTranslations';
import Head from '@/components/Head';
import useRouter from '@/utils/useRouter';
import getPreview from '@/components/SchemaFormInWorkspace/getPreview';
import LinkIcon from '@/svgs/link.svg';
import { usePageEndpoint } from '@/utils/urls';

interface WorkspaceLayoutProps {
  children: ReactNode;
}

export const WorkspaceLayout = ({ children }: WorkspaceLayoutProps) => {
  const api = useApi();
  const {
    workspace,
    saveWorkspace,
    createAutomation,
    creatingAutomation,
    createPage,
    creatingPage,
    createBlock,
  } = useWorkspace();

  const router = useRouter();

  const { localize } = useLocalizedText();
  const t = useTranslations('workspaces');
  const language = useLocale();

  const [sourceDisplayed, setSourceDisplayed] = useState(DisplayedSourceType.None);
  const [mountSourceComponent, setMountComponent] = useState(false);
  const [displaySourceView, setDisplaySourceView] = useState(false);
  const [invalid, setInvalid] = useState<WorkspaceLayoutContext['invalid']>(false);
  const [newSource, setNewSource] = useState<WorkspaceLayoutContext['newSource']>();
  const [saving, setSaving] = useState(false);
  const [fullSidebar, setFullSidebar] = useState(
    Storage.get('__workpaceSidebarMinimized') === null
      ? window.innerWidth > 500
      : !Storage.get('__workpaceSidebarMinimized'),
  );
  const [advancedMode, setAdvancedMode] = useState(false);

  const [appStoreVisible, setAppStoreVisible] = useState(false);
  const { trackEvent } = useTracking();
  const { addUserPermissions } = usePermissions();
  const [title, setTitle] = useState('');

  useEffect(() => {
    if (fullSidebar) {
      Storage.set('__workpaceSidebarMinimized', 0);
    } else {
      Storage.set('__workpaceSidebarMinimized', 1);
    }
  }, [fullSidebar]);

  // Manage source panel display
  useEffect(() => {
    if (sourceDisplayed !== DisplayedSourceType.None) {
      setMountComponent(true);
    } else {
      setDisplaySourceView(false);
      setTimeout(() => setMountComponent(false), 200);
    }
  }, [sourceDisplayed]);

  const onSaveSource = useCallback(async () => {
    trackEvent({
      category: 'Workspace',
      name: 'Save source code',
      action: 'click',
    });
    if (!newSource) return;

    setSaving(true);
    try {
      if (sourceDisplayed === DisplayedSourceType.Config) {
        await saveWorkspace(newSource);
      } else if (sourceDisplayed === DisplayedSourceType.Roles) {
        delete (newSource as any).id;
        await api.updateWorkspaceSecurity(workspace.id, newSource as Prismeai.WorkspaceSecurity);
      }
      notification.success({
        message: t('expert.save.confirm'),
        placement: 'bottomRight',
      });
    } catch (e) {
      const { details, message } = e as ApiError;
      const description = Array.isArray(details) ? (
        <ul>
          {details.map(({ path, message }: any, key: number) => (
            <li key={key}>
              {t('openapi', { context: message, ns: 'error', path: path.replace(/^\.body\./, '') })}
            </li>
          ))}
        </ul>
      ) : (
        message
      );
      notification.error({
        message: t('expert.save.fail'),
        description,
        placement: 'bottomRight',
      });
    }
    setSaving(false);
  }, [newSource, saveWorkspace, sourceDisplayed, t, trackEvent, workspace.id, api]);

  const onSave = useCallback(
    async (workspace: Prismeai.Workspace) => {
      trackEvent({
        category: 'Workspace',
        name: 'Save Workspace',
        action: 'click',
      });
      setSaving(true);
      await saveWorkspace(workspace);
      notification.success({
        message: t('save.confirm'),
        placement: 'bottomRight',
      });
      setSaving(false);
    },
    [saveWorkspace, t, trackEvent],
  );

  const displaySource = useCallback(
    (v: DisplayedSourceType) => {
      trackEvent({
        category: 'Workspace',
        name: `${v ? 'Hide' : 'Display'} source code`,
        action: 'click',
      });
      setSourceDisplayed(v);
    },
    [trackEvent],
  );

  const createAutomationHandler = useCallback(
    async ({ slug, name }: Pick<Prismeai.Automation, 'slug' | 'name'>) => {
      trackEvent({
        category: 'Workspace',
        name: 'Create Automation from navigation',
        action: 'click',
      });
      try {
        const createdAutomation = await createAutomation({
          slug,
          name:
            name ||
            incrementName(
              t(`automations.create.defaultName`),
              Object.values(workspace.automations || {}).map(({ name }) => localize(name)),
            ),
          do: [],
        });
        if (createdAutomation) {
          await router.push(`/workspaces/${workspace.id}/automations/${createdAutomation.slug}`);
        }
        return true;
      } catch (e: any) {
        if (e.error === 'AlreadyUsedError') {
          notification.error({
            message: t('alreadyUsed', { ns: 'errors' }),
            placement: 'bottomRight',
          });
        }
      }
      return false;
    },
    [createAutomation, localize, router, t, trackEvent, workspace.automations, workspace.id],
  );

  const createPageHandler: WorkspaceLayoutContext['createPage'] = useCallback(
    async ({ slug, public: isPublic, ...page } = {}) => {
      trackEvent({
        category: 'Workspace',
        name: slug ? 'Create Page template from navigation' : 'Create Page from navigation',
        action: 'click',
      });
      const name =
        t(`pages.create.template.${slug}`, { defaultValue: null }) ||
        incrementName(
          t(`pages.create.defaultName`),
          Object.values(workspace.pages || {}).map(({ name }) => localize(name)),
        );
      try {
        const createdPage = await createPage({
          name: {
            [language]: name,
          },
          slug,
          blocks: [],
          ...page,
        });
        if (isPublic && createdPage && createdPage.id) {
          await addUserPermissions('pages', createdPage.id, {
            target: { public: true },
            permissions: {
              policies: { read: true },
            },
          });
        }
        if (createdPage) {
          await router.push(`/workspaces/${workspace.id}/pages/${createdPage.slug}`);
        }
        return true;
      } catch (e: any) {
        if (e.error === 'AlreadyUsedError') {
          notification.error({
            message: t('alreadyUsed', { ns: 'errors' }),
            placement: 'bottomRight',
          });
        }
      }
      return false;
    },
    [
      addUserPermissions,
      createPage,
      language,
      localize,
      router,
      t,
      trackEvent,
      workspace.id,
      workspace.pages,
    ],
  );

  const installAppHandler = useCallback(() => {
    setAppStoreVisible(true);
    trackEvent({
      category: 'Workspace',
      name: 'Display Apps catalog from navigation',
      action: 'click',
    });
  }, [trackEvent]);

  const createBlockHandler: WorkspaceLayoutContext['createBlock'] = useCallback(
    async ({ slug, name }) => {
      trackEvent({
        category: 'Workspace',
        name: 'Create Block from navigation',
        action: 'click',
      });

      try {
        if (workspace?.blocks?.[slug]) {
          const e: any = new Error();
          e.error = 'AlreadyUsedError';
          throw e;
        }
        const createdBlock = await createBlock({
          slug,
          name:
            name ||
            incrementName(
              t(`blocks.create.defaultName`),
              Object.values(workspace.blocks || {}).map(({ name }) => localize(name)),
            ),
          blocks: [],
        });
        if (createdBlock) {
          await router.push(`/workspaces/${workspace.id}/blocks/${name}`);
        }
        return true;
      } catch (e: any) {
        if (e.error === 'AlreadyUsedError') {
          notification.error({
            message: t('alreadyUsed', { ns: 'errors' }),
            placement: 'bottomRight',
          });
        }
      }
      return false;
    },
    [createBlock, localize, router, t, trackEvent, workspace.blocks, workspace.id],
  );

  const segments = useSelectedLayoutSegments() || [];

  const uploadFile = useCallback(
    async (
      file: string,
      opts?: {
        expiresAfter?: number;
        public?: boolean;
        shareToken?: boolean;
      },
    ) => {
      if (!workspace?.id) return file;
      // Delete these lines as soon as we migrated existing blocks using legacy syntax
      if (typeof opts === 'number') {
        opts = {
          expiresAfter: opts,
        };
      }
      const [{ url, mimetype, name }] = await api.workspaces(workspace?.id).uploadFiles(file, opts);

      return {
        value: url,
        preview: getPreview(mimetype, url),
        label: name,
      };
    },
    [workspace?.id, api],
  );

  return (
    <TrackingCategory category="Workspace">
      <workspaceLayoutContext.Provider
        value={{
          displaySource,
          sourceDisplayed,
          saving,
          setSaving,
          onSave,
          onSaveSource,
          invalid,
          setInvalid,
          newSource,
          setNewSource,
          fullSidebar,
          setFullSidebar,
          createAutomation: createAutomationHandler,
          createPage: createPageHandler,
          createBlock: createBlockHandler,
          installApp: installAppHandler,
          advancedMode,
          setAdvancedMode,
          setTitle,
        }}
      >
        <SchemaFormConfigProvider
          utils={{
            uploadFile,
          }}
          components={{
            UiWidgets: {
              block: BlockWidget,
            },
          }}
        >
          <BlocksProvider>
            <Head
              title={t('workspace.title', { name: localize(workspace.name) })}
              description={t('workspace.description', {
                name: localize(workspace.name),
              })}
            />
            {mountSourceComponent && (
              <div
                className={`${styles['workspace-layout-source-ctn']} ${
                  displaySourceView ? styles['workspace-layout-source-ctn--visible'] : ''
                }`}
              >
                <WorkspaceSource
                  key={sourceDisplayed}
                  sourceDisplayed={sourceDisplayed}
                  onLoad={() => setDisplaySourceView(true)}
                />
              </div>
            )}
            <AppsStore visible={appStoreVisible} onCancel={() => setAppStoreVisible(false)} />
            <BlockProvider
              config={{
                sidebar: {
                  header: {
                    back: {
                      label: t('workspace.back'),
                      href: '/workspaces',
                    },
                  },
                  items: [
                    {
                      key: 'activity',
                      type: 'internal',
                      text: t('workspace.sections.activity'),
                      value: `/workspaces/${workspace.id}`,
                      icon: <HomeIcon />,
                      selected: segments.length === 0,
                    },
                    <Navigation key="navigation" />,
                    {
                      key: 'usage',
                      type: 'internal',
                      text: t('workspace.sections.usage'),
                      value: `/workspaces/${workspace.id}/usage`,
                      icon: <ChartIcon />,
                      selected: segments[0] === 'usage',
                    },
                    {
                      key: `settings`,
                      type: 'internal',
                      text: t('workspace.sections.settings'),
                      value: `/workspaces/${workspace.id}/settings/display`,
                      icon: 'gear',
                      selected: segments[0] === 'settings',
                    },
                  ],
                  opened: true,
                  autoClose: globalThis.innerWidth < 768,
                },
                content: {
                  logo: workspace.photo,
                  title: (
                    <div className={`${styles['content-title']} product-layout-content-title`}>
                      {localize(workspace.name)}
                      <a href={usePageEndpoint()} target="_blank" rel="noreferrer">
                        <LinkIcon />
                      </a>
                    </div>
                  ),
                  description: title,
                  content: creatingAutomation || creatingPage ? <Loading /> : children,
                },
                css: `
                  @import default;
                  @media (max-width: 768px) {
                    .product-layout-sidebar-buttons {
                      margin-top: 4.5rem;
                    }
                  }
                `,
              }}
            >
              <ProductLayout />
            </BlockProvider>
          </BlocksProvider>
        </SchemaFormConfigProvider>
      </workspaceLayoutContext.Provider>
    </TrackingCategory>
  );
};

export default WorkspaceLayout;
