import { Tooltip } from 'antd';
import { ReactNode, useEffect, useState } from 'react';
import Section, { SectionTypes } from './Section';
import PlusIcon from '@/svgs/plus.svg';
import styles from './styles.module.scss';
import Add from './Add';
import { useTranslations } from 'next-intl';
import { useProductLayoutContext } from '@/blocks/blocks/ProductLayout/Provider';
import { Icon } from '@/blocks/blocks/Icon';
import StretchContent from '@/components/StretchContent';
import { useSelectedLayoutSegments } from 'next/navigation';
import { search } from '@/utils/filters';

export interface SidebarButtonProps {
  icon: ReactNode | string;
  children: ReactNode | string;
  search?: ReturnType<typeof search>;
  type: SectionTypes;
  onAdd?: () => void;
}
export const SidebarButton = ({ children, icon, search, type, onAdd }: SidebarButtonProps) => {
  const t = useTranslations('workspaces');
  const { sidebarOpen, toggleSidebar } = useProductLayoutContext();
  const [visible, setVisible] = useState(false);
  const [segmentType] = useSelectedLayoutSegments() || [];

  useEffect(() => {
    if (segmentType === type) {
      setVisible(true);
    }
  }, [segmentType, type]);

  useEffect(() => {
    if (search) {
      setVisible(true);
    }
  }, [search]);

  const [open, setOpen] = useState(false);

  return (
    <>
      <div className="product-layout-sidebar-button-ctn">
        <div
          className={`product-layout-sidebar-button product-layout-sidebar-button ${open ? 'product-layout-sidebar-button--selected' : ''} ${styles['sidebar-nav']}`}
          onClick={() => {
            toggleSidebar('open');
            setVisible(!visible);
          }}
        >
          <Tooltip title={children} placement="right">
            <button type="button" className="product-layout-sidebar-button-icon">
              <Icon icon={icon} />
            </button>
          </Tooltip>
          <button type="button" className="product-layout-sidebar-button-label">
            {children}
          </button>
          <Tooltip title={t(`workspace.add.${type}`)} placement="right">
            <button
              className={styles['sidebar-nav-add-btn']}
              onClick={(e) => {
                e.stopPropagation();
                onAdd?.();
              }}
            >
              <Add type={type} open={open} setOpen={setOpen}>
                <PlusIcon />
              </Add>
            </button>
          </Tooltip>
        </div>
      </div>
      <StretchContent visible={sidebarOpen && visible} forceMount className={styles['links-ctn']}>
        <Section
          type={type}
          icon={icon}
          search={search}
          onAdd={() => {
            setOpen(true);
            onAdd?.();
          }}
          addIsOpen={open}
          setAddIsOpen={setOpen}
        />
      </StretchContent>
    </>
  );
};

export default SidebarButton;
