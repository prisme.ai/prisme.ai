import { Tooltip } from 'antd';

import { useRef } from 'react';
import styles from './styles.module.scss';
import { useProductLayoutContext } from '@/blocks/blocks/ProductLayout/Provider';
import { useTranslations } from 'next-intl';
import { Icon } from '@/blocks/blocks/Icon';

export interface SearchProps {
  search: string;
  setSearch: (v: string) => void;
}
export const Search = ({ search, setSearch }: SearchProps) => {
  const t = useTranslations('workspaces');
  const { toggleSidebar } = useProductLayoutContext();
  const ref = useRef<HTMLInputElement>(null);

  return (
    <div className="product-layout-sidebar-button-ctn">
      <div className="product-layout-sidebar-button product-layout-sidebar-button">
        <Tooltip title={t('workspace.search')} placement="right">
          <button
            className="product-layout-sidebar-button-icon"
            onClick={() => {
              toggleSidebar('open');
              ref.current?.focus();
            }}
          >
            <Icon icon="magnifier" />
          </button>
        </Tooltip>
        <input
          ref={ref}
          className={`product-layout-sidebar-button-label ${styles['search-input']}`}
          placeholder={t('workspace.search')}
          onChange={({ target: { value } }) => setSearch(value)}
          value={search}
        />
        <button
          className={`${styles['search-input-reset-btn']} ${
            search ? '' : styles['search-input-reset-btn--hidden']
          }`}
          onClick={() => {
            setSearch('');
            ref.current?.focus();
          }}
        >
          <Icon icon="cross" />
        </button>
      </div>
    </div>
  );
};

export default Search;
