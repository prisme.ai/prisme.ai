import { ReactNode, useMemo } from 'react';
import { useWorkspace } from '@/providers/Workspace';
import useLocalizedText from '@/utils/useLocalizedText';
import Links from './Links';
import Tooltip from './Tooltip';
import styles from './styles.module.scss';
import { useSelectedLayoutSegments } from 'next/navigation';
import { search } from '@/utils/filters';
import useTranslations from '@/i18n/useTranslations';
import PlusIcon from '@/svgs/plus.svg';

export const types = ['automations', 'pages', 'blocks', 'apps'] as const;

export type SectionTypes = (typeof types)[number];

export interface SectionProps {
  type: SectionTypes;
  icon?: ReactNode;
  search?: ReturnType<typeof search>;
  onAdd: () => void;
  addIsOpen: boolean;
  setAddIsOpen: (open: boolean) => void;
}
export const Section = ({ type, icon, search, onAdd, addIsOpen, setAddIsOpen }: SectionProps) => {
  const t = useTranslations('workspaces');
  const { localize } = useLocalizedText();
  const {
    workspace: { id, automations, pages, blocks, imports: apps },
  } = useWorkspace();
  const currentSlug = decodeURIComponent((useSelectedLayoutSegments() || [])[1]);

  const { links, isFiltered } = useMemo(() => {
    const linkByType = { automations, pages, blocks, apps };
    const links = linkByType[type];
    if (!links) return { links: [], isFiltered: false };

    const filtered = Object.entries(links)
      .filter(([slug, { name: label = '', description = '' }]) =>
        search ? search(`${localize(label)} ${localize(description)} ${slug}`) : true,
      )
      .map(
        ([
          href,
          { appSlug, slug, name: label = slug, description = appSlug, photo: icon, ...entity },
        ]) => ({
          label,
          description,
          icon,
          href: `/workspaces/${id}/${type}/${href}`,
          tooltip: (
            <Tooltip
              slug={href}
              name={label}
              description={
                <div className={styles['link-tooltip']}>
                  <div className={styles['link-tooltip-slug']}>{href}</div>
                  {description && (
                    <div className={styles['link-description']}>{localize(description)}</div>
                  )}
                </div>
              }
              type={type}
              entity={entity}
            />
          ),
          selected: currentSlug === href,
        }),
      );

    return { links: filtered, isFiltered: filtered.length !== Object.keys(links || {}).length };
  }, [apps, currentSlug, automations, blocks, id, localize, pages, search, type]);

  return (
    <div className={styles['links']}>
      {links && links.length > 0 && (
        <Links
          links={links}
          defaultIcon={icon}
          search={search}
          type={type}
          addIsOpen={addIsOpen}
          setAddIsOpen={setAddIsOpen}
        />
      )}
      {!isFiltered && !links?.length && (
        <button onClick={onAdd} className={styles['link']}>
          <PlusIcon className={styles['link-icon']} />
          <div className={styles['link-label']}>{t(`workspace.add.${type}_first`)}</div>
        </button>
      )}
    </div>
  );
};

export default Section;
