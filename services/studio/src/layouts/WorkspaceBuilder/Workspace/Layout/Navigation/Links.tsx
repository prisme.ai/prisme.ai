import { Tooltip } from 'antd';

import Link from 'next/link';
import { Fragment, ReactNode, useEffect, useMemo, useState } from 'react';
import useLocalizedText from '@/utils/useLocalizedText';
import { SectionTypes } from './Section';
import styles from './styles.module.scss';
import FolderIcon from '@/svgs/builder/folder.svg';
import Add from './Add';
import { useTranslations } from 'next-intl';
import { Icon } from '@/blocks/blocks/Icon';
import StretchContent from '@/components/StretchContent';
import { search } from '@/utils/filters';

export interface Link {
  label: string;
  description: string;
  href: string;
  icon?: string;
  links?: Link[];
  tooltip?: ReactNode;
  selected?: boolean;
  parentPath?: string;
}

interface FolderProps {
  links: Link[];
  label: string;
  href: string;
  defaultIcon?: ReactNode;
  search?: ReturnType<typeof search>;
  selected?: boolean;
  type: SectionTypes;
  parentPath?: string;
  addIsOpen: boolean;
  setAddIsOpen: (open: boolean) => void;
}
const Folder = ({
  links,
  label,
  defaultIcon,
  search,
  selected,
  type,
  parentPath = '',
}: FolderProps) => {
  const t = useTranslations('workspaces');
  const [visible, setVisible] = useState(false);
  const [open, setOpen] = useState(false);
  useEffect(() => {
    if (search && links.length) {
      setVisible(true);
    }
  }, [search, links]);

  useEffect(() => {
    if (selected && links.length) {
      setVisible(true);
    }
  }, [links.length, selected]);

  return (
    <>
      <div
        className={`${styles['link']} ${styles['link--folder']} ${selected ? styles['link--selected'] : ''}`}
      >
        <button className={`${styles['link-btn']}`} onClick={() => setVisible(!visible)}>
          <picture className={styles['link-icon']}>
            <FolderIcon />
          </picture>
          <div className={styles['link-label']}>{label}</div>
          <Icon className={styles['chevron-btn']} icon="chevron" rotate={visible ? -180 : 0} />
        </button>
        <Tooltip title={t(`workspace.add.${type}`)} placement="right">
          <Add type={type} path={`${parentPath}/${label}`} open={open} setOpen={setOpen}>
            <button className={`${styles['create-btn']}`}>
              <Icon icon="plus" />
            </button>
          </Add>
        </Tooltip>
      </div>
      <StretchContent visible={visible} className={styles.sublinks} forceMount>
        <Links
          links={links}
          defaultIcon={defaultIcon}
          search={search}
          type={type}
          parentPath={`${parentPath}/${label}`}
          addIsOpen={open}
          setAddIsOpen={setOpen}
        />
      </StretchContent>
    </>
  );
};

interface LinksProps {
  links: Link[];
  defaultIcon?: ReactNode;
  search?: ReturnType<typeof search>;
  type: SectionTypes;
  parentPath?: string;
  addIsOpen: boolean;
  setAddIsOpen: (open: boolean) => void;
}
export const Links = ({
  links,
  defaultIcon,
  search,
  type,
  parentPath,
  addIsOpen,
  setAddIsOpen,
}: LinksProps) => {
  const { localize } = useLocalizedText();

  const linksWithFolders = useMemo(() => {
    const linksWithFolders = links.reduce<Link[]>((prev, link) => {
      const label = localize(link.label);
      const isFolder = label.includes('/');
      if (!isFolder) return [...prev, { ...link, label: localize(label) }];

      const [folder, ...path] = localize(label).replace(/^\//, '').split(/\//);

      const next = [...prev];
      let folderLinks = next.find(({ href }) => href === folder);
      if (!folderLinks) {
        folderLinks = {
          href: folder,
          links: [],
          description: '',
          label: folder,
        };
        next.push(folderLinks);
      }
      folderLinks.links || (folderLinks.links = []);
      folderLinks.links.push({
        ...link,
        label: path.join('/'),
      });

      folderLinks.selected = folderLinks.links.reduce(
        (prev, next) => prev || !!next.selected,
        false,
      );

      return next;
    }, []);
    linksWithFolders.sort(({ label: labelA }, { label: labelB }) => {
      if (labelA > labelB) return 1;
      if (labelA < labelB) return -1;
      return 0;
    });
    linksWithFolders.sort((a, b) => {
      if (a.links && !b.links) return -1;
      if (b.links && !a.links) return 1;
      return 0;
    });
    return linksWithFolders;
  }, [links, localize]);

  return (
    <>
      {linksWithFolders?.map(({ href, label, tooltip, icon, links, selected }, key) => (
        <Fragment key={key}>
          {links && links.length && (
            <Folder
              links={links}
              href={href}
              label={label}
              defaultIcon={defaultIcon}
              search={search}
              selected={selected}
              type={type}
              parentPath={parentPath}
              addIsOpen={addIsOpen}
              setAddIsOpen={setAddIsOpen}
            />
          )}
          {!links && (
            <Link href={href} key={href}>
              <Tooltip
                title={tooltip}
                placement="rightTop"
                className={`${styles['link']} ${selected ? styles['link--selected'] : ''}`}
              >
                {icon && (
                  <picture className={styles['link-icon']}>
                    <img src={icon} alt={label} />
                  </picture>
                )}
                {!icon && defaultIcon && (
                  <picture className={styles['link-icon']}>{defaultIcon}</picture>
                )}
                <div className={styles['link-label']}>{label}</div>
              </Tooltip>
            </Link>
          )}
        </Fragment>
      ))}
    </>
  );
};

export default Links;
