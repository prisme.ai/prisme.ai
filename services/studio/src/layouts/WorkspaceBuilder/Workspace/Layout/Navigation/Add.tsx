import { ReactNode, useCallback, useMemo, useRef, useState } from 'react';
import SchemaForm from '@/components/SchemaForm/SchemaForm';
import { useTracking } from '@/providers/Tracking';
import { useWorkspace, Workspace } from '@/providers/Workspace';
import { PAGE_SLUG_VALIDATION_REGEXP, SLUG_VALIDATION_REGEXP } from '@/utils/regex';
import { useWorkspaceLayout } from '../context';
import { PAGE_TEMPLATES } from '../PageTemplates';
import { useTranslations } from 'next-intl';
import { Schema } from '@/components/SchemaForm';
import { Button, Popover } from 'antd';
import CloseIcon from '@/svgs/cross-large.svg';
import { FormApi } from 'final-form';
import { slugifyUnique } from '@/utils/strings';
import useLocalizedText from '@/utils/useLocalizedText';

interface AppProps {
  children: ReactNode;
  type: string;
  path?: string;
  open: boolean;
  setOpen: (open: boolean) => void;
}

function filterTemplates(templates: typeof PAGE_TEMPLATES, alreadySet: Workspace['pages'] = {}) {
  return templates.filter(({ slug }) => !Object.keys(alreadySet).includes(slug));
}

interface Value {
  name: string;
  slug: string;
}

export const Add = ({ children, type, path = '', open, setOpen }: AppProps) => {
  const t = useTranslations('workspaces');
  const { localize } = useLocalizedText();
  const { workspace } = useWorkspace();
  const { createAutomation, createPage, createBlock } = useWorkspaceLayout();
  const { trackEvent } = useTracking();
  const [submitting, setSubmitting] = useState(false);
  const containerRef = useRef<HTMLDivElement>(null);
  const submit = useCallback(
    async (values: any) => {
      let ok = false;
      setSubmitting(true);
      values.name = `${path && `${path}/`}${values.name}`;
      switch (type) {
        case 'automations':
          ok = await createAutomation(values);
          break;
        case 'pages': {
          values.colorScheme = 'auto';
          const template = PAGE_TEMPLATES.find(({ slug }) => slug === values.template);
          if (template) {
            values = { ...template, public: true };
          }
          ok = await createPage(values);
          break;
        }
        case 'blocks':
          ok = await createBlock(values);
          break;
      }
      setSubmitting(false);
      if (ok) {
        setOpen(false);
      }
    },
    [createAutomation, createBlock, createPage, path, type, setOpen],
  );

  const availablePageTemplates = useMemo(
    () => filterTemplates(PAGE_TEMPLATES, workspace.pages),
    [workspace.pages],
  );

  const schema: Schema = useMemo(
    () =>
      type === 'pages' && availablePageTemplates.length > 0
        ? ({
            type: 'object',
            oneOf: [
              {
                title: t('pages.create.label'),
                properties: {
                  name: {
                    type: 'localized:string',
                    title: t(`${type}.details.name.label`),
                    validators: {
                      required: true,
                    },
                  },
                  slug: {
                    type: 'string',
                    title: t(`${type}.details.slug.label`),
                    placeholder: `${type.replace(/s$/, '')}-slug`,
                    validators: {
                      required: true,
                      pattern: {
                        value:
                          type === 'pages'
                            ? PAGE_SLUG_VALIDATION_REGEXP.source
                            : SLUG_VALIDATION_REGEXP.source,
                        message: t('InvalidSlugError', { ns: 'errors' }),
                      },
                    },
                  },
                } as Schema,
              },
              {
                title: t('pages.create.template.title'),
                properties: {
                  template: {
                    type: 'string',
                    title: t('pages.create.template.title'),
                    enum: availablePageTemplates.map(({ slug }) => slug),
                    enumNames: availablePageTemplates.map(({ slug }) =>
                      t(`pages.create.template.${slug}`),
                    ),
                    validators: {
                      required: true,
                    },
                  },
                },
              },
            ],
          } as Schema)
        : {
            type: 'object',
            properties: {
              name: {
                type: 'localized:string',
                title: t(`${type}.details.name.label`),
                validators: {
                  required: true,
                },
              },
              slug: {
                type: 'string',
                title: t(`${type}.details.slug.label`),
                placeholder: `${type}-slug`,
                validators: {
                  required: true,
                  pattern: {
                    value: SLUG_VALIDATION_REGEXP.source,
                    message: t('InvalidSlugError', { ns: 'errors' }),
                  },
                },
              },
            } as Schema,
          },
    [availablePageTemplates, t, type],
  );

  const formRef = useRef<FormApi<any, any>>();
  const generateSlug = useCallback(
    (values: Value, prev: Value) => {
      if (values.name === prev.name || formRef.current?.getFieldState('values.slug')?.touched) {
        return;
      }
      formRef.current?.change(
        'values.slug',
        slugifyUnique(
          localize(values.name),
          Object.keys(workspace?.[type as 'automations' | 'pages' | 'blocks'] || {}),
        ),
      );
    },
    [localize, type, workspace],
  );

  return (
    <Popover
      trigger={'click'}
      title={() => (
        <div className="flex flex-1 justify-between">
          {t(`workspace.add.${type}`)}
          <button
            onClick={() => {
              trackEvent({
                name: 'Close Details Panel by clicking button',
                action: 'click',
              });
              setOpen(false);
            }}
          >
            <CloseIcon />
          </button>
        </div>
      )}
      destroyTooltipOnHide
      content={() => (
        <div ref={containerRef} className="flex flex-1">
          <style>{`
            .add-ressource .pr-form-object {
              --pr-form-object-border: transparent
            }
            .add-ressource .pr-form-object__label {
              display: none;
            }
          `}</style>
          <SchemaForm
            formRef={formRef}
            schema={schema}
            onSubmit={submit}
            onChange={generateSlug}
            buttons={[
              <Button
                key="1"
                type="primary"
                htmlType="submit"
                className="mr-[10px] self-end"
                disabled={submitting}
              >
                {t(`${type}.create.submit`)}
              </Button>,
            ]}
          />
        </div>
      )}
      placement="bottomLeft"
      open={open}
      onOpenChange={() => {
        setOpen(!open);
        setTimeout(() => {
          if (!containerRef.current) return;
          const input = containerRef.current.querySelector('input');
          if (!input) return;
          input.focus();
        });
      }}
    >
      {children}
    </Popover>
  );
};

const AddController = (props: AppProps) => {
  if (props.type === 'apps') {
    return <>{props.children}</>;
  }
  return <Add {...props} />;
};

export default AddController;
