import { ReactNode } from 'react';
import Highlight from '@/components/Highlight';
import useLocalizedText, { isLocalizedObject } from '@/utils/useLocalizedText';
import { useWorkspaceLayout } from '../context';
import AutomationSnippet from './AutomationSnippet';
import BlockSnippet from './BlockSnippet';
import { SectionTypes } from './Section';

interface TooltipProps {
  search?: string;
  slug: string;
  name: Prismeai.LocalizedText;
  description?: Prismeai.LocalizedText | ReactNode;
  type: SectionTypes;
  entity?: any;
}

export const Tooltip = ({ search, slug, name, description, type, entity }: TooltipProps) => {
  const { localize } = useLocalizedText();
  const { advancedMode } = useWorkspaceLayout();

  return (
    <>
      <div className="font-bold">
        <Highlight highlight={search} component={<span className="font-bold text-[#ecfd18]" />}>
          {localize(name)}
        </Highlight>
      </div>
      {description && (
        <div className="italic">
          <Highlight highlight={search} component={<span className="font-bold text-[#ecfd18]" />}>
            {isLocalizedObject(description) ? localize(description) : description}
          </Highlight>
        </div>
      )}
      {advancedMode && type === 'automations' && <AutomationSnippet slug={slug} {...entity} />}
      {advancedMode && type === 'blocks' && <BlockSnippet slug={slug} {...entity} />}
    </>
  );
};
export default Tooltip;
