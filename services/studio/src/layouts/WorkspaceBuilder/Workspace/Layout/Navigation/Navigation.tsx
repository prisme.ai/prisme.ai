import { useMemo, useState } from 'react';
import Search from './Search';
import SidebarButton from './SidebarButton';
import PageIcon from '@/svgs/builder/page.svg';
import AutomationIcon from '@/svgs/builder/automation.svg';
import BlockIcon from '@/svgs/builder/block.svg';
import AppIcon from '@/svgs/builder/app.svg';
import { useWorkspaceLayout } from '../context';
import { useTranslations } from 'next-intl';
import { search } from '@/utils/filters';

export const Navigation = () => {
  const t = useTranslations('workspaces');
  const { installApp } = useWorkspaceLayout();
  const [filter, setFilter] = useState('');

  const handleSearch = useMemo(() => (filter ? search(filter) : undefined), [filter]);

  return (
    <>
      <Search search={filter} setSearch={(v: string) => setFilter(v)} />
      <SidebarButton icon={<PageIcon />} search={handleSearch} type="pages">
        {t('workspace.sections.pages')}
      </SidebarButton>
      <SidebarButton icon={<AutomationIcon />} search={handleSearch} type="automations">
        {t('workspace.sections.automations')}
      </SidebarButton>
      <SidebarButton icon={<BlockIcon />} search={handleSearch} type="blocks">
        {t('workspace.sections.blocks')}
      </SidebarButton>
      <SidebarButton icon={<AppIcon />} search={handleSearch} type="apps" onAdd={installApp}>
        {t('workspace.sections.apps')}
      </SidebarButton>
    </>
  );
};

export default Navigation;
