'use client';

import { useEffect, useState } from 'react';
import Storage from '@/utils/Storage';
import { useUser } from '@/providers/User';
import { useTranslations } from 'next-intl';
import { notification } from 'antd';
import Loading from '@/components/Loading';
import useApi from '@/utils/api/useApi';
import useRouter from '@/utils/useRouter';

interface InstallWorkspaceProps {
  children?: React.ReactNode;
}
export const InstallWorkspace = ({ children }: InstallWorkspaceProps) => {
  const api = useApi();
  const { user } = useUser();
  const [install, setInstall] = useState(Storage.get('__install'));
  const { push } = useRouter();
  const t = useTranslations('workspaces');

  useEffect(() => {
    if (!install || !user) return;
    async function installWorkspace() {
      try {
        const w = await api.duplicateWorkspace({ id: install });
        if (!w) {
          throw new Error('Cannot install workspace');
        }
        push(`/workspaces/${w.id}`);
        notification.success({
          message: t('workspaces.install.ok'),
        });
      } catch (e) {
        notification.error({
          message: t('workspaces.install.error'),
        });
      }
    }
    if (!user.meta?.onboarded) {
      Storage.set('__onboardingstep', 3);
    }
    installWorkspace();
    Storage.remove('__install');
    setInstall(null);
  }, [install, push, t, user, api]);

  if (!install || !user) return <>{children}</>;

  return <Loading />;
};

export default InstallWorkspace;
