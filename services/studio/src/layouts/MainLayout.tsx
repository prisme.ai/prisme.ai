import getMessages from '@/i18n/getMessages';
import I18nProvider from '@/i18n/Provider';
import { ColorSchemeManagerProvider, Schemes } from '@/providers/ColorSchemeManager';
import { CustomizationProvider, getCustomization } from '@/providers/Customization';
import QueryStringProvider from '@/providers/QueryString';
import { UserProvider } from '@/providers/User';
import { cookies } from 'next/headers';

interface MainLayoutProps {
  children: React.ReactNode;
  locale: string;
  className?: string;
  anonymous?: boolean;
}

export default async function MainLayout({
  children,
  locale,
  className = '',
  anonymous,
}: MainLayoutProps) {
  const [messages, customization] = await Promise.all([getMessages(locale), getCustomization()]);
  const cookieStore = await cookies();
  const colorScheme = (cookieStore.get('color-scheme')?.value || 'light') as Schemes;

  return (
    <html lang={locale} data-color-scheme={colorScheme}>
      <ColorSchemeManagerProvider {...customization}>
        <body className={className}>
          <I18nProvider messages={messages} locale={locale}>
            <QueryStringProvider>
              <CustomizationProvider {...customization}>
                <UserProvider anonymous={anonymous}>{children}</UserProvider>
              </CustomizationProvider>
            </QueryStringProvider>
          </I18nProvider>
        </body>
      </ColorSchemeManagerProvider>
    </html>
  );
}
