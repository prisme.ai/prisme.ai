'use client';

import { useEffect, useState } from 'react';
import { useTranslations } from 'next-intl';
import { useUser } from '@/providers/User';
import useApi from '@/utils/api/useApi';
import { HTTPError } from '@prisme.ai/sdk';
import { SignType } from '../Sign/types';
import { SignContent } from '../Sign/SignContent';
import { useSearchParams } from 'next/navigation';

export const ConfirmDelete = () => {
  const api = useApi();
  const t = useTranslations('sign');
  const qs = useSearchParams();
  const validationToken = qs.get('validationToken');
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState('');

  const { user } = useUser();

  useEffect(() => {
    if (!user) return;
    async function deleteUser() {
      try {
        await api.users(user.id).delete(`${validationToken}`);
      } catch (e) {
        setError((e as HTTPError).message);
      }
      setLoading(false);
    }
    deleteUser();
  }, [user, validationToken, api]);

  if (loading) return null;

  return (
    <SignContent type={error ? SignType.DeleteAccountError : SignType.DeleteAccount}>
      {error && t.has(`account.delete.error.message_${error}`)
        ? t(`account.delete.error.message_${error}`)
        : t('account.delete.error.message')}
    </SignContent>
  );
};

ConfirmDelete.isPublic = true;

export default ConfirmDelete;
