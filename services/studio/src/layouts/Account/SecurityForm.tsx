import { useCallback } from 'react';
import ConfirmButton from '../../components/ConfirmButton';
import { useTranslations } from 'next-intl';
import useApi from '@/utils/api/useApi';

export const SecurityForm = () => {
  const api = useApi();
  const t = useTranslations('user');

  const deleteAccount = useCallback(() => {
    api.users().sendDeleteValidation();
  }, [api]);

  return (
    <div className="">
      <h2 className="text-lg font-bold">{t('account.security.delete.title')}</h2>
      <p className="my-2">{t('account.security.delete.description')}</p>
      <ConfirmButton
        type="primary"
        confirmLabel={t('account.security.delete.confirm')}
        yesLabel={t('account.security.delete.yes')}
        noLabel={t('account.security.delete.no')}
        onConfirm={deleteAccount}
        className="ant-btn-warning"
        placement="right"
      >
        {t('account.security.delete.label')}
      </ConfirmButton>
    </div>
  );
};

export default SecurityForm;
