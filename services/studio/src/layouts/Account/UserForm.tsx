import SchemaForm, { Schema } from '@/components/SchemaForm';
import { useNotifications } from '@/providers/Notifications';
import { useUser } from '@/providers/User';
import { Button, ButtonProps } from 'antd';
import { useTranslations } from 'next-intl';
import { useCallback, useMemo, useState } from 'react';
import { useForm } from 'react-final-form';

const SubmitButton = ({ updating, ...props }: ButtonProps & { updating: boolean }) => {
  const { getState } = useForm();
  const { hasValidationErrors } = getState();

  return (
    <Button
      type="primary"
      htmlType="submit"
      disabled={hasValidationErrors || updating}
      {...props}
    />
  );
};

export const UserForm = () => {
  const t = useTranslations();
  const { user, update } = useUser();
  const notification = useNotifications();
  const [updating, setUpdating] = useState(false);
  const schema = useMemo(
    () =>
      ({
        type: 'object',
        properties: {
          firstName: {
            type: 'string',
            title: t('user.account.settings.user.firstName.label'),
          },
          lastName: {
            type: 'string',
            title: t('user.account.settings.user.lastName.label'),
          },
          photo: {
            type: 'string',
            title: t('user.account.settings.user.photo.label'),
            'ui:widget': 'upload',
          },
        },
      }) as Schema,
    [t],
  );

  const submit = useCallback(
    async ({ email, ...values }: any) => {
      void email;
      if (user.photo && !values.photo) {
        values.photo = '';
      }
      if (user.photo === values.photo) {
        delete values.photo;
      }
      setUpdating(true);
      try {
        await update(values);
      } catch (err) {
        notification.error({
          message:
            (err as any)?.error === 'InvalidFile' && (err as any)?.details?.maxSize
              ? t('errors.InvalidFileError', {
                  type: 'image',
                  maxSize: (err as any)?.details?.maxSize,
                })
              : t('errors.unknown', { errorName: (err as any)?.error }),
          placement: 'bottomRight',
        });
      }
      setUpdating(false);
    },
    [update, user.photo, notification, t],
  );
  return (
    <SchemaForm
      schema={schema}
      initialValues={user}
      buttons={[
        <SubmitButton key="submit" updating={updating}>
          {t('common.save')}
        </SubmitButton>,
      ]}
      onSubmit={submit}
      autoFocus
    />
  );
};

export default UserForm;
