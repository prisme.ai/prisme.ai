import React from 'react';
import UserForm from './UserForm';
import SecurityForm from './SecurityForm';
import { useUser } from '@/providers/User';
import { BlockProvider } from '@/blocks/Provider';
import Avatar from '../UserSpace/Avatar';
import ProductLayoutInContext from '@/blocks/blocks/ProductLayout';
import styles from './account.module.scss';
import useTranslations from '@/i18n/useTranslations';
import Head from '@/components/Head';

const Account = () => {
  const t = useTranslations('user');
  const { user } = useUser();

  if (!user) {
    return null;
  }

  return (
    <>
      <Head title={t('title.myAccount')} description="" />
      <BlockProvider
        config={{
          sidebar: null,
          content: {
            title: (
              <div className={`product-layout-content-title ${styles['title']}`}>
                {t('account.label')}
              </div>
            ),
            description: (
              <div className={`product-layout-content-description ${styles['description']}`}>
                <div className={styles['description-avatar']}>
                  <Avatar size="70px" />
                </div>
                <div className={`text-main-text ${styles['description-texts']}`}>
                  <div className={styles['description-texts-primary']}>
                    {user.firstName} {user.lastName}
                  </div>
                  <div className={styles['description-texts-secondary']}>{user.email}</div>
                </div>
              </div>
            ),
            tabs: [
              {
                title: t('account.settings.user.label'),
                content: (
                  <div className="product-layout-content-panel product-layout-content-panel--1col">
                    <UserForm />
                  </div>
                ),
              },
              {
                title: t('account.settings.security.label'),
                content: (
                  <div className="product-layout-content-panel product-layout-content-panel--1col">
                    <SecurityForm />
                  </div>
                ),
              },
              // {
              //   title: t('account.settings.interface.label'),
              //   content: (
              //     <div className="product-layout-content-panel product-layout-content-panel--1col">
              //       <InterfaceForm />
              //     </div>
              //   ),
              // },
            ],
          },
        }}
      >
        <ProductLayoutInContext />
      </BlockProvider>
    </>
  );
};

export default Account;
