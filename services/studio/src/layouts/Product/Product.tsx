'use client';

import { useEffect, useMemo, useRef, useState } from 'react';
import { useLocale } from 'next-intl';
import { usePathname, useSearchParams } from 'next/navigation';
import styles from './product.module.scss';
import { useEnv } from '@/providers/Env';
import IFrame from '@/components/IFrame';
import useRouter from '@/utils/useRouter';
import isIOS from '@/utils/isIOS';

export default function Product() {
  const { PAGES_HOST = '' } = useEnv<{ PAGES_HOST: string; DEBUG_PROD?: boolean }>();
  const language = useLocale();
  const pathname = usePathname() || '';
  const searchParams = useSearchParams();
  const iframe = useRef<HTMLIFrameElement>(null);
  const router = useRouter();
  const dontSetIFrameSrc = useRef(false);

  const { productSlug, path } = useMemo(() => {
    const [, , , productSlug, ...rest] = pathname.split(/\//);
    return {
      productSlug,
      path: `${rest.join('/')}${searchParams && searchParams.size > 0 ? `?${searchParams.toString()}` : ''}`,
    };
  }, [pathname, searchParams]);

  const [productUrl, setProductUrl] = useState('');
  const pathRef = useRef(path);
  useEffect(() => {
    pathRef.current = path;
  }, [path]);
  useEffect(() => {
    if (dontSetIFrameSrc.current) return;
    setProductUrl(
      `${window.location.protocol}//${productSlug}${PAGES_HOST}/${language}/${pathRef.current}`,
    );
  }, [productSlug, PAGES_HOST, language]);

  useEffect(() => {
    if (!iframe.current) return;

    const listener = ({ origin, data, source, data: { type = '' } = {} }: MessageEvent) => {
      if (source !== iframe.current?.contentWindow) return;
      if (type !== 'page.navigate') return;
      const { host: hostOrigin } = new URL(origin);
      const { host: hostProduct } = new URL(productUrl);
      const productSlug = (hostOrigin === hostProduct ? hostProduct : hostOrigin).split(
        PAGES_HOST,
      )[0];

      dontSetIFrameSrc.current = true;
      router.push(`/${language}/product/${productSlug}${data.path}`);
      setTimeout(() => {
        dontSetIFrameSrc.current = false;
      }, 1);
    };
    window.addEventListener('message', listener);
    return () => {
      window.removeEventListener('message', listener);
    };
  }, [productSlug, productUrl, PAGES_HOST, language, router]);

  useEffect(() => {
    if (!isIOS()) return;
    let prevHeight = window.innerHeight;
    const interval = setInterval(() => {
      const currentHeight = window.innerHeight;
      if (currentHeight > prevHeight) {
        scrollTo(0, 0);
      }
      prevHeight = currentHeight;
    }, 10);
    return () => {
      clearInterval(interval);
    };
  }, []);

  if (!productUrl) return null;

  return (
    <IFrame
      ref={iframe}
      src={productUrl}
      className={styles['product-iframe']}
      allow="clipboard-write; camera; geolocation; microphone; speaker"
      title={productSlug}
    />
  );
}
