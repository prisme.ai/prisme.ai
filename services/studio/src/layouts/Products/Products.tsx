'use client';

import { useEffect, useState } from 'react';
import { Icon } from '../../blocks/blocks/Icon/Icon';
import { useTranslations } from 'next-intl';
import { useUser } from '../../providers/User';
import Loading from '../../components/Loading';
import useLocalizedText from '../../utils/useLocalizedText';
import styles from './products.module.scss';
import { useEnv } from '@/providers/Env';

interface News {
  createdAt: string;
  text: string;
  icon: string;
}

const NEWS: { current: News[] | 'loading' } = { current: 'loading' };

export const Products = () => {
  const { NEWS_ENDPOINT } = useEnv<{ NEWS_ENDPOINT: string }>();
  const t = useTranslations('products');
  const { localize } = useLocalizedText();
  const { user } = useUser();
  const [news, setNews] = useState<News[] | 'loading'>(NEWS.current);

  useEffect(() => {
    async function fetchNews() {
      try {
        if (!NEWS_ENDPOINT) {
          throw new Error('NEWS_ENDPOINT not set');
        }
        const res = await fetch(NEWS_ENDPOINT);
        if (!res.ok) {
          throw new Error(res.statusText);
        }
        const { news } = await res.json();
        setNews(news);
        NEWS.current = news;
      } catch {
        setNews([]);
      }
    }
    fetchNews();
  }, [NEWS_ENDPOINT]);

  return (
    <div className={styles['products-ctn']}>
      <div>
        <h1 className={styles['products-title']}>
          {t('welcome.title', { user: user?.firstName })}
        </h1>
      </div>
      <div className={styles['products-news']}>
        {news === 'loading' && <Loading />}
        {news !== 'loading' && news.length > 0 && (
          <>
            <h2 className={styles['products-news-title']}>{t('news.title')}</h2>
            {news.map(({ createdAt, icon, text }) => (
              <div key={`${createdAt} ${text}`} className={styles['products-news-item']}>
                <Icon icon={icon} className={styles['products-news-item-icon']} />
                {localize(text)}
              </div>
            ))}
          </>
        )}
      </div>
    </div>
  );
};

export default Products;
