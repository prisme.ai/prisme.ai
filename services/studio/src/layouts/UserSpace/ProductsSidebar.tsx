import Link from 'next/link';
import { useCallback, useEffect, useMemo, useState } from 'react';
import Storage from '../../utils/Storage';
import Button from './Button';
import { Product, useProducts } from '../../providers/Products';
import useLocalizedText from '../../utils/useLocalizedText';
import { UserSpaceConfig, useUserSpace } from './context';
import MainLogo from '@/svgs/main-logo.svg';
import IconReduce from '@/svgs/sidebar/sidebar-reduce.svg';
import IconExpand from '@/svgs/sidebar/sidebar-expand.svg';
import IconHome from '@/svgs/sidebar/sidebar-home.svg';
import IconDark from '@/svgs/sidebar/sidebar-dark-mode.svg';
import IconLight from '@/svgs/sidebar/sidebar-light-mode.svg';
import IconBurger from '@/svgs/burger.svg';
import IconCross from '@/svgs/cross-large.svg';
import { useColorSchemeManager } from '../../providers/ColorSchemeManager';
import { UserDropDown } from './UserDropDown';
import { useTranslations } from 'next-intl';
import { usePathname } from 'next/navigation';
import styles from './userSpace.module.scss';

function getProductSlug(path: string) {
  const [, , product, productSlug] = path.split(/\//);
  if (product === 'workspaces') {
    return 'workspaces';
  }
  return productSlug;
}

const getIcon = (icon: string) => {
  if (typeof icon === 'string' && icon.match(/^<svg/)) {
    return (
      <span className={styles['productSidebar-icon']} dangerouslySetInnerHTML={{ __html: icon }} />
    );
  }
  return icon;
};

interface ProductSidebarProps {
  userSpaceConfig: UserSpaceConfig;
}

export const ProductsSidebar = ({ userSpaceConfig }: ProductSidebarProps) => {
  const t = useTranslations('user');
  const { localize } = useLocalizedText();

  const [expanded, setExpanded] = useState(
    window.innerWidth > 768 ? (Storage.get('sidebarExpanded') === false ? false : true) : false,
  );
  const { products: rawProducts, shortcuts: rawShortcuts } = useProducts();
  const [products, setProducts] = useState(Array.from(rawProducts.values()));
  const [shortcuts, setShortcuts] = useState(rawShortcuts);
  const { scheme, toggleScheme } = useColorSchemeManager();
  const { isUserMenuVisible, setUserMenuVisible } = useUserSpace();
  const pathname = usePathname() || '';
  const toggleSidebar = useCallback(() => {
    setExpanded(!expanded);
    Storage.set('sidebarExpanded', !expanded);
  }, [expanded]);

  const selected = useMemo(() => {
    const productSlug = getProductSlug(pathname);
    const index = products.findIndex(({ slug }) => slug === productSlug);
    if (index === -1 && productSlug) return -2;
    return index;
  }, [products, pathname]);

  useEffect(() => {
    async function fetchSvgs() {
      const [products, shortcuts] = await Promise.all(
        [Array.from(rawProducts.values()), rawShortcuts].map(
          async (list) =>
            await Promise.all(
              (list as Product[]).map(async (product) => {
                if (!`${product.icon}`.match(/\.svg$/)) return product;
                try {
                  const svg = await fetch(product.icon);
                  return {
                    ...product,
                    icon: await svg.text(),
                  };
                } catch {
                  return product;
                }
              }),
            ),
        ),
      );
      setProducts(products);
      setShortcuts(shortcuts);
    }
    fetchSvgs();
  }, [rawProducts, rawShortcuts]);

  const [visible, setVisible] = useState(false);
  const toggle = useCallback(() => {
    if (isUserMenuVisible) {
      setUserMenuVisible(false);
      return;
    }
    setVisible(!visible);
    document.body.classList.toggle('overflow-hidden', !visible);
  }, [isUserMenuVisible, setUserMenuVisible, visible]);
  useEffect(() => {
    setVisible(false);
  }, [pathname]);
  useEffect(() => {
    setTimeout(() => {
      document.body.classList.toggle('product-sidebar-is-visible', visible);
    }, 200);
  }, [visible]);

  return (
    <div
      className={`${styles.productSidebar} ${
        expanded ? styles['productSidebar--expanded'] : ''
      } ${visible ? styles['productSidebar--visible'] : ''}`}
    >
      <div className={styles.productSidebarCtn}>
        <div className={styles.homeLinkCtn}>
          <Link href={userSpaceConfig?.mainUrl || '/'} tabIndex={-1}>
            {userSpaceConfig.mainLogo?.url ? (
              <img
                src={userSpaceConfig.mainLogo.url}
                {...(userSpaceConfig.mainLogo.attrs || {})}
                alt="Prisme.ai"
              />
            ) : (
              <MainLogo width={40} height={40} alt="Prisme.ai" />
            )}
          </Link>
        </div>
        <button className={styles.mobileToggleBtn} onClick={toggle}>
          <IconBurger className={styles.mobileToggleBtnOpen} />
          <IconCross className={styles.mobileToggleBtnClose} />
        </button>
        <div className={styles.productsLinks}>
          <div className={styles.productsLinksGroupTop}>
            <div className={`${styles.productsLinksCtn} ${styles['productsLinksCtn--expand']}`}>
              <Button
                tabIndex={0}
                expanded={expanded}
                icon={
                  expanded ? (
                    <IconReduce width={24} height={24} />
                  ) : (
                    <IconExpand width={24} height={24} />
                  )
                }
                onClick={toggleSidebar}
                name={t(`sidebar.expand${expanded ? '_in' : ''}`)}
              />
            </div>
            <div className={styles.productsLinksCtn}>
              <Link href="/" tabIndex={-1}>
                <Button
                  expanded={expanded}
                  icon={<IconHome width={24} height={24} />}
                  name={t('sidebar.home.title')}
                  selected={selected === -1}
                />
              </Link>
            </div>
            {shortcuts && shortcuts.length > 0 && (
              <div className={styles.productsLinksCtn}>
                {shortcuts.map(
                  ({ name, href, icon }, key) =>
                    href && (
                      <Link href={href} key={key} tabIndex={-1}>
                        <Button expanded={expanded} icon={getIcon(icon)} name={localize(name)} />
                      </Link>
                    ),
                )}
              </div>
            )}
            {products.map(({ href, icon, name }, index) => (
              <div key={href} className={styles.productsLinksCtn}>
                <Link href={href} tabIndex={-1}>
                  <Button
                    expanded={expanded}
                    selected={index === selected}
                    icon={getIcon(icon)}
                    name={name}
                  />
                </Link>
              </div>
            ))}
          </div>
          <div className={styles.productsLinksGroupBottom}>
            <div className={styles.productsLinksCtn}>
              <Button
                expanded={expanded}
                icon={
                  scheme === 'light' ? (
                    <IconDark width={24} height={24} />
                  ) : (
                    <IconLight width={24} height={24} />
                  )
                }
                name={t(`sidebar.colorscheme.${scheme === 'light' ? 'dark' : 'light'}`)}
                onClick={toggleScheme}
              />
              <UserDropDown expanded={expanded} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductsSidebar;
