import { useCallback, useEffect, useRef, useState } from 'react';
import { createPortal } from 'react-dom';
import { useUser } from '../../providers/User';
import Avatar from './Avatar';
import Button, { ButtonProps } from './Button';
import MenuUser from './MenuUser';
import IconCross from '@/svgs/cross-large.svg';
import { useColorSchemeManager } from '../../providers/ColorSchemeManager';
import { useUserSpace } from './context';
import { usePathname } from 'next/navigation';
import styles from './userSpace.module.scss';

type UserDropDownProps = Pick<ButtonProps, 'expanded'>;
export const UserDropDown = ({ expanded }: UserDropDownProps) => {
  const ref = useRef<HTMLButtonElement>(null);
  const iframe = useRef<HTMLIFrameElement>(null);
  const panelRef = useRef<HTMLDivElement>(null);
  const { setUserMenuVisible, isUserMenuVisible } = useUserSpace();
  const [sidePage, setSidePage] = useState('');

  const { user } = useUser();
  const pathname = usePathname();
  const { scheme } = useColorSchemeManager();

  useEffect(() => {
    const { current: el } = ref;
    if (!el) return;
    const listener = (e: MouseEvent) => {
      const target = e.target as HTMLElement;
      if (panelRef.current?.contains(target)) return;
      if (target === el || el.contains(target)) {
        return setUserMenuVisible(!isUserMenuVisible);
      }
      setUserMenuVisible(false);
    };
    globalThis.addEventListener('click', listener);

    return () => {
      globalThis.removeEventListener('click', listener);
    };
  }, [isUserMenuVisible, setUserMenuVisible]);

  useEffect(() => {
    setUserMenuVisible(false);
  }, [pathname, setUserMenuVisible]);

  useEffect(() => {
    setSidePage('');
  }, [isUserMenuVisible]);

  const onLoad = useCallback(() => {
    if (!iframe.current) return;
    setTimeout(
      () =>
        iframe.current?.contentWindow?.postMessage({ type: 'prColorSchemeUpdate', scheme }, '*'),
      100,
    );
  }, [scheme]);
  useEffect(() => {
    onLoad();
  }, [onLoad]);

  const [isOverlayVisible, setIsOverlayVisible] = useState(false);
  useEffect(() => {
    setTimeout(() => setIsOverlayVisible(isUserMenuVisible), 200);
  }, [isUserMenuVisible]);

  return (
    <div className={styles.userDropDown}>
      <Button expanded={expanded} icon={<Avatar />} name={user.firstName} ref={ref} />
      {createPortal(
        <div
          ref={panelRef}
          className={`${styles.userDropDownOverlay} ${isOverlayVisible ? styles['userDropDownOverlayPage--visible'] : ''} ${isUserMenuVisible ? styles['userDropDownOverlay--visible'] : ''} ${expanded ? styles['userDropDownOverlay--expanded'] : ''} ${sidePage ? styles['userDropDown--navigated'] : ''}`}
        >
          <div className={styles.userDropDownOverlayCtn}>
            <div className={styles.userDropDownOverlaySurface}>
              <MenuUser onNavigate={setSidePage} />
              <div className={styles.userDropDownOverlayPage}>
                <button onClick={() => setSidePage('')} className={styles.userDropDownClosePageBtn}>
                  <IconCross />
                </button>
                <iframe ref={iframe} src={sidePage} onLoad={onLoad}></iframe>
              </div>
            </div>
          </div>
        </div>,
        document.body,
      )}
    </div>
  );
};
