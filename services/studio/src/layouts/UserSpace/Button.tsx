import { Tooltip } from 'antd';
import { forwardRef, HTMLAttributes, ReactNode } from 'react';
import styles from './userSpace.module.scss';

export interface ButtonProps extends HTMLAttributes<HTMLButtonElement> {
  expanded: boolean;
  selected?: boolean;
  icon: string | ReactNode;
  name: string;
  tooltip?: string;
}
export const Button = forwardRef<HTMLButtonElement, ButtonProps>(function Button(
  { expanded, selected, icon, name, tooltip = name, ...props }: ButtonProps,
  ref,
) {
  return (
    <button
      className={`${styles.sidebarButton} ${
        selected ? styles['sidebarButton--selected'] : ''
      } ${expanded ? styles['sidebarButton--expanded'] : ''}`}
      {...props}
      ref={ref}
    >
      <span className={styles.sidebarButtonIconCtn}>
        <Tooltip title={expanded ? undefined : tooltip} placement="right">
          {typeof icon === 'string' ? (
            /* eslint-disable-next-line @next/next/no-img-element */
            <img src={icon} alt={name} />
          ) : (
            icon
          )}
        </Tooltip>
      </span>
      {name && <span className={styles.sidebarButtonLabel}>{name}</span>}
    </button>
  );
});

export default Button;
