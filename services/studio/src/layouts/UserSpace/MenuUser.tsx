import { Menu, MenuRef } from 'antd';
import Link from 'next/link';
import { useUser } from '../../providers/User';
import Avatar from './Avatar';
import pkg from '@/../package.json';
import IconAccount from '@/svgs/sidebar/account.svg';
import IconChangelog from '@/svgs/sidebar/changelog.svg';
import IconFeedback from '@/svgs/sidebar/feedback.svg';
import IconHelp from '@/svgs/sidebar/help.svg';
import IconPrivacy from '@/svgs/sidebar/privacy.svg';
import IconSignout from '@/svgs/sidebar/signout.svg';
import { Dispatch, SetStateAction, useEffect, useRef } from 'react';
import { useUserSpace } from './context';
import { useTranslations } from 'next-intl';
import { ItemType } from 'antd/lib/menu/interface';
import styles from './userSpace.module.scss';

interface LinkWithIconProps {
  href: string;
  onClick?: () => void;
  icon: typeof IconAccount;
  label: string;
  popup?: true;
}
const LinkWithIcon = ({ href, icon: Icon, label, onClick, popup }: LinkWithIconProps) => {
  return (
    <Link
      href={href}
      onClick={(e) => {
        if (onClick) {
          e.preventDefault();
          return onClick();
        }
        if (popup) {
          const href = (e.target as HTMLAnchorElement).getAttribute('href');

          if (href) {
            e.preventDefault();
            window.open(href);
          }
        }
      }}
      className={styles['user-menu-item-user-ctn-with-icon']}
    >
      <Icon width={24} height={24} />
      <span className={styles['user-menu-item-user-icon']}>{label}</span>
    </Link>
  );
};

interface MenuUserProps {
  onNavigate: Dispatch<SetStateAction<string>>;
}

export const MenuUser = ({ onNavigate }: MenuUserProps) => {
  const t = useTranslations('user');
  const { user, signout } = useUser();
  const {
    whatsnewView,
    feedbackView,
    privacyView,
    helpView,
    isUserMenuVisible,
    setUserMenuVisible,
  } = useUserSpace();
  const menuRef = useRef<MenuRef>(null);

  useEffect(() => {
    if (!isUserMenuVisible) return;
    const button = menuRef.current?.menu?.list.querySelector('a,button') as HTMLElement;
    button?.focus();
  }, [isUserMenuVisible]);

  useEffect(() => {
    const { current } = menuRef;
    if (!current || !current.menu?.list) return;
    const list = current.menu.list;
    const listener = () => {
      setUserMenuVisible(true);
    };

    list.addEventListener('focus', listener);

    return () => {
      list.removeEventListener('focus', listener);
    };
  }, [setUserMenuVisible]);

  useEffect(() => {
    if (!isUserMenuVisible) return;
    const buttons = Array.from(menuRef.current?.menu?.list.querySelectorAll('a,button') || []);
    const lastButton = buttons[buttons.length - 1];
    const listener = (e: Event) => {
      const { key, shiftKey } = e as KeyboardEvent;
      if (key === 'Tab' && !shiftKey) {
        setUserMenuVisible(false);
      }
    };
    lastButton.addEventListener('keydown', listener);
    return () => {
      lastButton.removeEventListener('keydown', listener);
    };
  }, [isUserMenuVisible, setUserMenuVisible]);

  return (
    <Menu
      className={styles['user-menu-ctn']}
      ref={menuRef}
      items={
        [
          {
            key: 'user',
            label: (
              <div className={styles['user-menu-item-user']}>
                <div className={styles['user-menu-item-user-icon']}>
                  <Avatar size="40px" />
                </div>
                <div className={styles['user-menu-item-user-display-name-ctn']}>
                  <div className={styles['user-menu-item-user-name']}>{user.firstName}</div>
                  <div className={styles['user-menu-item-user-email']}>{user.email || ''}</div>
                </div>
              </div>
            ),
            style: {
              pointerEvents: 'none',
            },
            className: styles['user-menu-item-user-ctn'],
          },
          {
            key: 'account',
            label: (
              <LinkWithIcon
                href="/account"
                icon={IconAccount}
                label={t('header.user.settings.title')}
              />
            ),
          },
          privacyView && {
            key: 'privacy',
            label: (
              <LinkWithIcon
                href={privacyView}
                icon={IconPrivacy}
                label={t('header.user.privacy.title')}
                popup
              />
            ),
          },
          {
            key: 'divider',
            type: 'divider',
          },
          helpView && {
            key: 'help',
            label: (
              <LinkWithIcon
                href={helpView}
                onClick={() => onNavigate(helpView)}
                icon={IconHelp}
                label={t('header.user.help.title')}
              />
            ),
          },
          feedbackView && {
            key: 'feedback',
            label: (
              <LinkWithIcon
                href={feedbackView}
                onClick={() => onNavigate(feedbackView)}
                icon={IconFeedback}
                label={t('header.user.feedback.title')}
              />
            ),
          },
          whatsnewView && {
            key: 'changelog',
            label: (
              <LinkWithIcon
                href={whatsnewView}
                onClick={() => onNavigate(whatsnewView)}
                icon={IconChangelog}
                label={t('header.user.changelog.title')}
              />
            ),
          },
          {
            key: 'version',
            label: pkg.version,
          },
          {
            key: 'divider2',
            type: 'divider',
          },
          {
            key: 'signout',
            label: (
              <LinkWithIcon
                href="/signout"
                onClick={() => signout()}
                icon={IconSignout}
                label={t('header.user.signout.title')}
              />
            ),
          },
        ].filter(Boolean) as ItemType[]
      }
    />
  );
};

export default MenuUser;
