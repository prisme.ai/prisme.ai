'use client';

import { ReactNode, useEffect, useState } from 'react';
import { ProductsSidebar } from './ProductsSidebar';
import { ProductsProvider } from '../../providers/Products';
import { useUser } from '../../providers/User';
import userSpaceContext, { UserSpaceConfig } from './context';
import styles from './userSpace.module.scss';
import { useCustomization } from '../../providers/Customization';
import Loading from '../../components/Loading';
import { usePathname } from 'next/navigation';
import Notifications from '@/providers/Notifications';
import { useEnv } from '@/providers/Env';
import useRouter from '@/utils/useRouter';
import ModalProvider from '@/providers/Modal';

interface UserSpaceProps {
  children: ReactNode;
}

export const UserSpace = ({ children }: UserSpaceProps) => {
  const { USER_SPACE_ENDPOINT = '' } = useEnv<{ USER_SPACE_ENDPOINT: string }>();
  const { user } = useUser();
  const pathname = usePathname() || '';
  const { replace } = useRouter();
  const [userSpaceConfig, setUserSpaceConfig] = useState<UserSpaceConfig>();
  const isIOS = globalThis.navigator.userAgent.includes('iPhone');
  const [isUserMenuVisible, setUserMenuVisible] = useState(false);
  const { mainLogo, links } = useCustomization();

  useEffect(() => {
    /**
     * Some elements can be hide or changed by calling a user contextualised
     * endpoint. This endpoint must return an object following the
     * UserSpaceConfig interface.
     */
    if (!USER_SPACE_ENDPOINT) {
      setUserSpaceConfig({});
      return;
    }
    const fetchUserSpaceConfig = async () => {
      try {
        const res = await fetch(USER_SPACE_ENDPOINT, {
          method: 'GET',
          credentials: 'include',
          headers: {
            'Content-Type': 'application/json',
          },
        });
        const userSpaceConfig = await res.json();
        if (mainLogo) {
          userSpaceConfig.mainLogo = {
            url: mainLogo,
          };
        }
        if (links) {
          userSpaceConfig.helpView = links.help || userSpaceConfig.helpView;
          userSpaceConfig.whatsnewView = links.changelog || userSpaceConfig.whatsnewView;
          userSpaceConfig.feedbackView = links.feedback || userSpaceConfig.feedbackView;
          userSpaceConfig.privacyView = links.privacy || userSpaceConfig.privacyView;
        }
        setUserSpaceConfig(userSpaceConfig);
      } catch {
        setUserSpaceConfig({});
      }
    };
    fetchUserSpaceConfig();
  }, [links, mainLogo, user, USER_SPACE_ENDPOINT]);

  useEffect(() => {
    if (!userSpaceConfig?.kiosk || !userSpaceConfig?.mainUrl) return;
    if (!pathname.includes('/account') && !pathname.includes(userSpaceConfig.kiosk)) {
      replace(userSpaceConfig.mainUrl);
    }
  }, [pathname, replace, userSpaceConfig?.kiosk, userSpaceConfig?.mainUrl]);
  if (user === null) return null;
  if (!user || user?.authData?.anonymous) return <>{children}</>;
  if (userSpaceConfig === undefined) return <Loading />;

  return (
    <Notifications>
      <ModalProvider>
        <userSpaceContext.Provider
          value={{ ...userSpaceConfig, isUserMenuVisible, setUserMenuVisible }}
        >
          <ProductsProvider disableBuilder={userSpaceConfig.disableBuilder}>
            <div
              className={`${styles.userSpace} ${
                isIOS ? styles['userSpace--ios'] : ''
              } ${isUserMenuVisible ? styles['userSpace--user-menu-visible'] : ''}`}
              style={userSpaceConfig?.style?.root || ({} as React.CSSProperties)}
            >
              <ProductsSidebar userSpaceConfig={userSpaceConfig} />
              <div className={styles['userSpace-content']}>{children}</div>
            </div>
          </ProductsProvider>
        </userSpaceContext.Provider>
      </ModalProvider>
    </Notifications>
  );
};

export default UserSpace;
