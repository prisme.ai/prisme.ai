import {
  ReactElement,
  ReactNode,
  useState,
  useEffect,
  useRef,
  Component,
  useCallback,
  FC,
} from 'react';
import { BlockProvider, BlockProviderProps } from './Provider';
import { useBlocks } from './Provider/blocksContext';
import { builtinBlocks } from './blocks';
import useExternalModule from '@/utils/useExternalModule';
import { Events } from '@prisme.ai/sdk';
import { Schema } from '@/components/SchemaForm';
import Loading from '@/components/Loading';

class BlockErrorBoundary extends Component<{
  children: ReactElement;
  events?: Events;
  config?: any;
  appConfig?: Prismeai.AppInstance['config'];
}> {
  state = {
    hasError: false,
  };

  static getDerivedStateFromError() {
    return { hasError: true };
  }

  componentDidCatch(error: any, errorInfo: any) {
    this.props.events &&
      this.props.events.emit('error', {
        error: 'DisplayBlockError',
        msg: 'There was an error displaying a block',
        config: this.props.config,
        appConfig: this.props.appConfig,
      });
    console.error(error, errorInfo);
  }

  render() {
    if (this.state.hasError) {
      return <h1>Something went wrong.</h1>;
    }
    return this.props.children;
  }
}

export interface BlockComponentProps<T = any> {
  name?: string;
  token?: string;
  workspaceId?: string;
  appInstance?: string;
  language?: string;
  layout?: {
    container?: HTMLElement;
  };
  config?: T;
}
export type BlockComponent<T = any> = {
  (props: BlockComponentProps<T>): ReactElement | null;
  schema?: Schema;
  Preview?: (props: BlockComponentProps<T>) => ReactElement;
  styles?: string;
};

export interface BlockLoaderProps extends BlockComponentProps {
  children?: ReactNode;
  url?: string;
  onLoad?: (block: any) => void;
}

export const ReactBlock = ({ url = '', onLoad, ...componentProps }: BlockLoaderProps) => {
  const { externals } = useBlocks();

  const { module: Component, loading } = useExternalModule<FC<BlockLoaderProps>>({
    url,
    externals,
  });

  useEffect(() => {
    if (!onLoad || !Component || loading) return;
    onLoad(Component);
  }, [onLoad, Component, loading]);

  return (
    <>
      {loading && <Loading />}
      {Component && <Component {...componentProps} />}
    </>
  );
};

export const IFrameBlock = ({ url, token }: BlockLoaderProps) => {
  const [loading, setLoading] = useState(true);
  const [height] = useState(100);
  const handleLoad = useCallback(
    (e: any) => {
      setLoading(false);
      e.target.contentWindow.postMessage(
        {
          source: 'prisme.ai',
          token,
        },
        '*',
      );
    },
    [token],
  );

  return (
    <>
      {loading && <Loading />}
      <iframe
        src={url}
        onLoad={handleLoad}
        className="flex"
        style={{
          width: '100%',
          height: `${height}px`,
        }}
      />
    </>
  );
};

const getComponentByName = (name: string): BlockComponent | null =>
  (builtinBlocks[name as keyof typeof builtinBlocks] as BlockComponent) || null;

const BuiltinBlock = ({
  onLoad,
  Component,
  ...props
}: BlockLoaderProps & { name: string; Component: BlockComponent }) => {
  const loaded = useRef(false);
  useEffect(() => {
    if (loaded.current || !onLoad) return;
    loaded.current = true;
    onLoad(Component);
  }, [Component, onLoad]);
  return Component ? <Component {...props} /> : null;
};

const BlockRenderMethod = ({ name, url, ...props }: BlockLoaderProps) => {
  const isJs = url && url.replace(/\?.*$/, '').match(/\.js$/);

  if (name && !isJs) {
    const Component = getComponentByName(name);
    if (!Component) console.log('missing block', name);

    if (Component) {
      return <BuiltinBlock Component={Component} name={name} {...props} />;
    }
  }

  if (typeof window === 'undefined') {
    // SSR, return nothing
    return null;
  }

  if (isJs) {
    return <ReactBlock url={url} name={name} {...props} />;
  }

  if (url?.match(/^http/)) {
    return <IFrameBlock url={url} {...props} />;
  }

  return null;
};

export const BlockLoader = ({
  config,
  onConfigUpdate,
  onAppConfigUpdate,
  api,
  ...props
}: BlockProviderProps & BlockLoaderProps) => {
  return (
    <BlockErrorBoundary events={props.events} config={config} appConfig={props.appConfig}>
      <BlockProvider
        config={config}
        onConfigUpdate={onConfigUpdate}
        appConfig={props.appConfig}
        onAppConfigUpdate={onAppConfigUpdate}
        events={props.events}
        api={api}
        onLoad={props.onLoad}
      >
        <BlockRenderMethod {...props} />
      </BlockProvider>
    </BlockErrorBoundary>
  );
};

export default BlockLoader;
