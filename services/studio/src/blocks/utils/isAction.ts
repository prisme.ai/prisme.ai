import { ActionConfig } from '../blocks/Action/Action';

export function isAction(item: ActionConfig | any): item is ActionConfig {
  const { type, value, text } = item as ActionConfig;
  return !!(type && value && text);
}
