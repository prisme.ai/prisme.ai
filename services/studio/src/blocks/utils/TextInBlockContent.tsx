import { isLocalizedObject, useLocalizedText } from '../../utils/useLocalizedText';
import GenericBlock from './GenericBlock';
import { isBlock, isRenderProp } from './getContentType';
import { BlockContent } from './types';

export const TextInBlockContent = ({ text }: { text: BlockContent }) => {
  const { localize } = useLocalizedText();
  return (
    <>
      {typeof text === 'string' && text}
      {isRenderProp(text) && text}
      {isLocalizedObject(text) && localize(text)}
      {isBlock(text) && (
        <GenericBlock
          content={text}
          className="pr-block-drop-down-overlay-menu-item"
          ifString={({ content, className }) => <div className={className}>{content}</div>}
        />
      )}
    </>
  );
};

export default TextInBlockContent;
