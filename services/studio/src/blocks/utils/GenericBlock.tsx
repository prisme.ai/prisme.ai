import { isLocalizedObject, useLocalizedText } from '../../utils/useLocalizedText';
import { memo, ReactElement, ReactNode } from 'react';
import { isBlock, isString } from './getContentType';
import { BlockContent } from './types';
import BlockLoader from '@/components/PageContent/BlockLoader';

export type GenericBlockContent = BlockContent & { hidden?: true };

export interface GenericBlockProps {
  content: GenericBlockContent;
  className?: string;
  ifString?: (props: { content: string; className?: string }) => ReactElement;
}
export const GenericBlock = memo(function GenericBlock({
  content,
  className,
  ifString: Component = () => <>{content as ReactNode}</>,
}: GenericBlockProps) {
  const { localize } = useLocalizedText();
  if (isLocalizedObject(content)) {
    content = localize(content);
  }
  if (isString(content)) return <Component content={content} className={className} />;
  if (isBlock(content))
    return <BlockLoader name="BlocksList" config={{ blocks: content, className }} />;
  return <>{content}</>;
});

export default GenericBlock;
