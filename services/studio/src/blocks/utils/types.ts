import { ReactElement } from 'react';
import { BlocksListConfig } from '../blocks/BlocksList/BlocksList';

export type BlockContent = string | ReactElement | BlocksListConfig['blocks'];

export type EventSpec =
  | string
  | {
      event: string;
      payload: any;
    };
