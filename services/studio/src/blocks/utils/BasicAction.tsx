import { ReactNode, useMemo } from 'react';
import { useBlock } from '../Provider';
import { ActionConfig } from '../blocks/Action/Action';
import Link from 'next/link';

interface BasicActionProps
  extends Pick<ActionConfig, 'type' | 'value' | 'payload'>,
    Partial<Omit<HTMLButtonElement | HTMLAnchorElement, 'children' | 'type'>> {
  children?: ReactNode;
}

export const BasicAction = ({ type, value, children, payload, ...props }: BasicActionProps) => {
  const { events } = useBlock();
  const Container = useMemo(() => {
    switch (type) {
      case 'external':
      case 'internal':
        return function InternalAction(props: any) {
          return <Link href={value || ''} {...props} />;
        };
      case 'event':
        return function EventAction(props: any) {
          return (
            <button
              type="button"
              onClick={() => {
                events?.emit(value, payload);
              }}
              {...props}
            />
          );
        };
      default:
        return () => null;
    }
  }, [type, events, payload, value]);

  return <Container {...props}>{children}</Container>;
};
