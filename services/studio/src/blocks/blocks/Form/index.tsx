import { useBlocks } from '@/blocks/Provider/blocksContext';
import { BaseBlock } from '../BaseBlock';
import { defaultStyles } from './defaultStyles';
import Form, { FormConfig } from './Form';
import { useBlock } from '@/blocks/Provider';

export const FormInContext = () => {
  const {
    components: { SchemaForm },
  } = useBlocks();
  const { config, events } = useBlock<FormConfig>();

  return (
    <BaseBlock defaultStyles={defaultStyles}>
      <Form {...config} SchemaForm={SchemaForm} events={events} />
    </BaseBlock>
  );
};
FormInContext.styles = defaultStyles;

export default FormInContext;
