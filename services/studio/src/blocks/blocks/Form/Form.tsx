import { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import useLocalizedText from '@/utils/useLocalizedText';
import get from 'lodash/get';
import getFieldFromValuePath from '@/utils/getFieldFromValuePath';
import { BaseBlockConfig } from '../types';
import { Schema, SchemaFormProps, UiOptionsCommon } from '@/components/SchemaForm';
import Action, { ActionConfig } from '../Action/Action';
import { BlockContext } from '@/blocks/Provider';
import { BlocksDependenciesContext } from '@/blocks/Provider/blocksContext';
import { useTranslations } from 'next-intl';
import { Button } from 'antd';

const defaultSchema = {};

export interface FormConfig extends BaseBlockConfig {
  title?: Prismeai.LocalizedText;
  schema: Schema;
  onChange?:
    | string
    | {
        event: string;
        payload?: Record<string, any>;
      };
  onSubmit?:
    | string
    | {
        event: string;
        payload?: Record<string, any>;
      };
  submitLabel?: string;
  hideSubmit?: boolean;
  disabledSubmit?: boolean;
  disableSubmitDelay?: number;
  values?: Record<string, any>;
  buttons?: ActionConfig[];
  collapsed?: boolean;
  autoFocus?: boolean;
}

interface FormProps extends FormConfig {
  events: BlockContext['events'];
  SchemaForm: BlocksDependenciesContext['components']['SchemaForm'];
}

function isCommonOptions(options: Schema['ui:options']): options is UiOptionsCommon {
  return !!(options as UiOptionsCommon).field;
}

export const Form = ({
  events,
  SchemaForm,
  className,
  buttons,
  sectionId = '',
  autoFocus,
  ...config
}: FormProps) => {
  const t = useTranslations('common');
  const { localize, localizeSchemaForm } = useLocalizedText();
  const [initialValues] = useState(config.values || {});
  const canChange = useRef(true);
  const formRef: SchemaFormProps['formRef'] = useRef<any>();
  const containerEl = useRef<HTMLDivElement>(null);
  const [hasError, setHasError] = useState(false);
  const [values, setValues] = useState<Record<string, any> | undefined>(initialValues);

  useEffect(
    () => setHasError(Object.keys(formRef?.current?.getState().errors || {}).length > 0),
    [],
  );

  useEffect(() => {
    setValues(config.values);
  }, [config.values]);

  const mergeValuesWithPayload = useCallback((values: any = {}, payload?: any) => {
    if (values && typeof values !== 'object') {
      return values;
    }
    if (!formRef.current) return;
    // This is to delete values in payload if the value in form with same key is unset
    formRef.current.getRegisteredFields().forEach((field: string) => {
      const realField = field.replace('values.', '');
      payload[realField] = values[realField];
    });
    return { ...payload, ...values };
  }, []);
  const onChange = useCallback(
    (values: any) => {
      setValues(values);
      if (!canChange.current || !config.onChange || !events) return;
      const { event, payload = {} } =
        typeof config.onChange === 'string' ? { event: config.onChange } : config.onChange;
      events.emit(event, mergeValuesWithPayload(values, payload));
    },
    [config.onChange, events, mergeValuesWithPayload],
  );

  useEffect(() => {
    const update = async () => {
      const form = formRef?.current;
      if (!form) return;
      canChange.current = false;
      const fields = form.getRegisteredFields();
      await fields
        .filter((field: string) => field !== 'values')
        .filter((field: string) => {
          const schema = getFieldFromValuePath({ properties: { values: config.schema } }, field);
          if (!schema) return true;
          const { 'ui:options': options } = schema;
          if (!options || !isCommonOptions(options)) return true;
          if (options.field.updateValue === false) return false;
          if (options.field.updateValue === 'blur') {
            const { active } = form.getFieldState(field) || {};
            return !active;
          }
          return true;
        })
        .forEach((name: string) => {
          const field = form.getFieldState(name);
          if (!field) return;
          const newValue = get({ values: config.values }, name);
          if (newValue === undefined) return;
          if (field.value !== newValue) {
            form.change(name, newValue);
          }
        });
      setTimeout(() => (canChange.current = true), 1);
    };
    update();
  }, [config.values, config.schema]);

  const disabledSubmit = useRef(false);
  const onSubmit = useCallback(
    (values: any) => {
      if (disabledSubmit.current || !config.onSubmit || !events) return;
      const { event, payload = {} } =
        typeof config.onSubmit === 'string' ? { event: config.onSubmit } : config.onSubmit;
      disabledSubmit.current = true;
      setTimeout(() => (disabledSubmit.current = false), +(config.disableSubmitDelay || 1000));
      events.emit(event, mergeValuesWithPayload(values, payload));
    },
    [config.onSubmit, events, config.disableSubmitDelay, mergeValuesWithPayload],
  );

  const localizedSchema = useMemo(() => {
    return localizeSchemaForm(config.schema || defaultSchema);
  }, [config.schema, localizeSchemaForm]);

  const customButtons = useMemo(
    () =>
      (buttons || []).map((button, key) => {
        const payload = { values, ...button.payload };
        return (
          <Action
            key={key}
            {...button}
            className={`pr-block-form__button ${button.className || ''}`}
            payload={payload}
            events={events}
          />
        );
      }),
    [buttons, events, values],
  );

  useEffect(() => {
    if (!autoFocus || !formRef.current || !containerEl.current) return;
    const form = containerEl.current.querySelector('form');
    if (!form) return;
    const firstField = formRef.current?.getRegisteredFields()[1];
    const input = form[firstField] as HTMLInputElement;
    input?.focus();
  }, [autoFocus]);

  if (!config.schema) return null;

  return (
    <div className={`pr-block-form ${className} block-form`} id={sectionId} ref={containerEl}>
      {config.title && <div className="pr-block-form__title">{localize(config.title)}</div>}
      <SchemaForm
        schema={localizedSchema}
        initialValues={initialValues}
        onChange={(v: any) => {
          setHasError(Object.keys(formRef?.current?.getState().errors || {}).length > 0);

          if (!canChange.current) return;
          onChange(v);
        }}
        onSubmit={onSubmit}
        buttons={
          config.hideSubmit
            ? []
            : [
                ({ disabled }: any) => (
                  <div
                    key={0}
                    className="pr-block-form__buttons-container block-form__buttons-container buttons-container"
                  >
                    {customButtons}
                    <Button
                      htmlType="submit"
                      type="primary"
                      className="pr-block-form__button buttons-container__button button"
                      disabled={disabled || !!config.disabledSubmit || hasError}
                    >
                      {localize(config.submitLabel) || t('form.submit')}
                    </Button>
                  </div>
                ),
              ]
        }
        formRef={formRef}
        initialFieldObjectVisibility={!config.collapsed}
      />
    </div>
  );
};

export default Form;
