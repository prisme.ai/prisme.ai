const css = String.raw;
export const defaultStyles = css`
  :block {
    display: flex;
    flex: 1 1 0%;
    flex-direction: column;
    padding: 2rem;
  }

  .pr-block-form__buttons-container {
    display: flex;
    justify-content: flex-end;
    margin-top: 0.5rem;
    padding-top: 1rem;
  }

  .pr-block-form__buttons-container .pr-block-form__button {
    margin-left: 1rem;
    color: var(--pr-accent-color-contrast);
  }

  .pr-block-form__button {
    padding-bottom: 1rem !important;
    padding-top: 1rem !important;
    padding-left: 2rem !important;
    padding-right: 2rem !important;
    height: 100%;
  }

  .pr-form-autocomplete .ant-select-selector {
    padding: 0 !important;
  }
  .pr-form-field--error > input {
    border-color: var(--pr-error-color, red);
  }
`;
