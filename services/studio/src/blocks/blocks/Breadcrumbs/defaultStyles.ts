const css = String.raw;
export const defaultStyles = css`
  :block {
    padding: 1rem;
  }
  :block .pr-block-breadcrumbs__list {
    display: flex;
    flex-direction: row;
  }
  .pr-block-breadcrumbs__item {
    flex-direction: row;
    display: flex;
  }
  :block .pr-block-breadcrumbs__link {
    display: flex;
  }

  :block .pr-block-breadcrumbs__link--current .pr-block-breadcrumbs__link {
    font-weight: bold;
  }

  :block .pr-block-breadcrumbs__item:not(:last-child):after {
    content: '>';
    display: flex;
    margin: 0 1rem;
  }
`;
