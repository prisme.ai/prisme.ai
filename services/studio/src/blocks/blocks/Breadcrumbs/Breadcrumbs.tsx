import Action, { ActionConfig } from '../Action/Action';
import { BaseBlockConfig } from '../types';
import { BlockContext } from '@/blocks/Provider';

export interface BreadcrumbsConfig extends BaseBlockConfig {
  links: ({
    className?: boolean;
  } & ActionConfig)[];
}
export interface BreadcrumbsProps extends BreadcrumbsConfig {
  events: BlockContext['events'];
}

export const Breadcrumbs = ({
  className = '',
  links,
  events,
  sectionId = '',
}: BreadcrumbsProps) => {
  const last = (links || []).length - 1;

  if (!Array.isArray(links)) return null;

  return (
    <nav className={`pr-block-breadcrumbs ${className}`} aria-label="Breadcrumb" id={sectionId}>
      <ol className="pr-block-breadcrumbs__list" role="list">
        {(links || []).map(({ className = '', ...action }, key) => (
          <li
            key={key}
            className={`pr-block-breadcrumbs__item ${className} ${
              key === last ? 'pr-block-breadcrumbs__link--current' : ''
            }`}
          >
            <Action {...action} events={events} className="pr-block-breadcrumbs__action" />
          </li>
        ))}
      </ol>
    </nav>
  );
};

export default Breadcrumbs;
