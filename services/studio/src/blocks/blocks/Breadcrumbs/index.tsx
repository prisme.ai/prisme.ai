import { useBlock } from '@/blocks/Provider';
import Breadcrumbs, { BreadcrumbsConfig } from './Breadcrumbs';
import { BaseBlock } from '../BaseBlock';
import { defaultStyles } from './defaultStyles';

export const BreadcrumbsInContext = () => {
  const { config, events } = useBlock<BreadcrumbsConfig>();
  return (
    <BaseBlock defaultStyles={defaultStyles}>
      <Breadcrumbs {...config} events={events} />
    </BaseBlock>
  );
};
BreadcrumbsInContext.styles = defaultStyles;

export default BreadcrumbsInContext;
