import { useBlock } from '@/blocks/Provider';
import { Modal, ModalProps } from './Modal';
import { BaseBlock } from '../BaseBlock';
import { defaultStyles } from './defaultStyles';

export const ModalInContext = () => {
  const { config } = useBlock<ModalProps>();
  return (
    <BaseBlock defaultStyles={defaultStyles}>
      <Modal {...config} />
    </BaseBlock>
  );
};
ModalInContext.styles = defaultStyles;

export default ModalInContext;
