import { useCallback, useEffect, useRef, useState } from 'react';
import { createPortal } from 'react-dom';
import { useBlock } from '../../Provider';
import { Icon } from '../Icon';
import { BaseBlockConfig } from '../types';
import { BlockContent, EventSpec } from '@/blocks/utils/types';
import GenericBlock from '@/blocks/utils/GenericBlock';

export interface ModalProps extends BaseBlockConfig {
  trigger?: BlockContent;
  visible?: boolean;
  onClose?: EventSpec;
  title?: BlockContent;
  content: BlockContent;
  closeOn?: string;
}

const modalsVisibleStates = new Map<object, boolean>();

export const Modal = ({
  className,
  trigger,
  title,
  onClose,
  content,
  visible: forcedVisible = false,
  closeOn,
}: ModalProps) => {
  const { events } = useBlock();
  const [visible, setVisible] = useState(forcedVisible);

  useEffect(() => {
    setVisible(forcedVisible);
  }, [forcedVisible]);

  const close = useCallback(() => {
    setVisible(false);
    if (!onClose) return;
    const { event, payload = {} } = typeof onClose === 'string' ? { event: onClose } : onClose;
    events?.emit(event, payload);
  }, [events, onClose]);
  const id = useRef();

  useEffect(() => {
    if (visible === undefined) return;
    modalsVisibleStates.set(id, visible);
    document.body.classList.toggle(
      'modal-is-open',
      Array.from(modalsVisibleStates.values()).filter(Boolean).length > 0,
    );
  }, [visible]);

  useEffect(() => {
    return () => {
      modalsVisibleStates.delete(id);
    };
  }, []);

  const [isReady, setIsReady] = useState(false);
  useEffect(() => {
    setTimeout(() => {
      setIsReady(true);
    }, 200);
  }, []);

  useEffect(() => {
    if (!closeOn || !events) return;
    const off = events?.on(closeOn, () => {
      setVisible(false);
    });
    return () => {
      return off();
    };
  }, [closeOn, events]);

  useEffect(() => {
    const handleKeyDown = (event: KeyboardEvent) => {
      if (event.key === 'Escape') {
        event.preventDefault();
        close();
      }
    };
    document.addEventListener('keydown', handleKeyDown);
    return () => {
      document.removeEventListener('keydown', handleKeyDown);
    };
  }, [close]);

  return (
    <>
      {trigger && (
        <button
          onClick={() => setVisible(true)}
          type="button"
          className={`pr-block-modal-trigger ${className}`}
        >
          <GenericBlock
            content={trigger}
            ifString={({ content, className }) => <div className={className}>{content}</div>}
          />
        </button>
      )}
      {createPortal(
        <div
          className={`pr-block-modal ${
            visible ? 'pr-block-modal--visible' : ''
          } ${isReady ? 'pr-block-modal--is-ready' : ''} ${className}`}
          onClick={close}
        >
          <div className="pr-block-modal-ctn" onClick={(e) => e.stopPropagation()}>
            {title && (
              <GenericBlock
                content={title}
                className="pr-block-modal-title"
                ifString={({ content, className }) => <div className={className}>{content}</div>}
              />
            )}
            <GenericBlock
              content={content}
              className="pr-block-modal-content"
              ifString={({ content, className }) => <div className={className}>{content}</div>}
            />
            <button className="pr-block-modal-close-btn" onClick={close}>
              <Icon icon="cross" />
            </button>
          </div>
        </div>,
        document.body,
      )}
    </>
  );
};
