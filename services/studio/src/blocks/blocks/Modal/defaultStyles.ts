const css = String.raw;
export const defaultStyles = css`
  :block.pr-block-modal-trigger {
    display: flex;
    align-items: center;
    color: var(--pr-main-text);
  }
  :block.pr-block-modal {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    pointer-events: none;
    opacity: 0;
    z-index: 1049;
    background: rgba(10, 14, 31, 0.5);
  }
  :block.pr-block-modal--is-ready {
    transition: opacity 0.2s ease-in;
  }
  :block.pr-block-modal--visible {
    pointer-events: all;
    opacity: 1;
  }
  .pr-block-modal-ctn {
    display: flex;
    flex-direction: column;
    position: relative;
    background: var(--pr-modal-bg-color);
    max-wdith: 45rem;
    max-width: 800px;
    max-height: 80vh;
    overflow: hidden;
    margin: auto;
    padding: 1.2rem;
    border-radius: 0.9rem;
    transform: translate3d(0, -100%, 0);
    --pr-input-bg-color: var(--pr-main-bg-color);
  }
  :block.pr-block-modal--is-ready .pr-block-modal-ctn {
    transition: transform 0.2s ease-in;
  }
  :block.pr-block-modal--visible .pr-block-modal-ctn {
    transform: translate3d(0, 0, 0);
  }
  .pr-block-modal-content {
    overflow: auto;
  }
  :root .modal-is-open .page {
    filter: blur(1rem);
  }
  .pr-block-modal-close-btn {
    position: absolute;
    top: 1.2rem;
    right: 1.2rem;
  }
  .pr-block-modal-title {
    font-size: 1.5rem;
    font-weight: 500;
  }
`;
