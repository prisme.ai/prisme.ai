import * as React from 'react';
const ExpandIcon = ({
  size = 16,
  color = 'currentColor',
  ...props
}: { size?: number; fill?: string } & React.SVGProps<any>) => (
  <svg
    width={size}
    height={size}
    viewBox="0 0 40 40"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M30 13C30 12.4477 29.5523 12 29 12C28.4477 12 28 12.4477 28 13V27C28 27.5523 28.4477 28 29 28C29.5523 28 30 27.5523 30 27V13Z"
      fill="currentColor"
    />
    <path
      d="M21.7071 15.0429C21.3166 14.6524 20.6834 14.6524 20.2929 15.0429C19.9024 15.4334 19.9024 16.0666 20.2929 16.4571L22.8358 19H11C10.4477 19 10 19.4477 10 20C10 20.5523 10.4477 21 11 21H22.8358L20.2929 23.5429C19.9024 23.9334 19.9024 24.5666 20.2929 24.9571C20.6834 25.3476 21.3166 25.3476 21.7071 24.9571L25.4268 21.2374C26.1102 20.554 26.1102 19.446 25.4268 18.7625L21.7071 15.0429Z"
      fill={color}
    />
  </svg>
);
export default ExpandIcon;
