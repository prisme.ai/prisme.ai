export * from './Icon';
import { useBlock } from '../../Provider';
import { Icon } from './Icon';
import { IconProps } from './types';
import { BaseBlock } from '../BaseBlock';

export default function IconInContext() {
  const { config } = useBlock<IconProps>();
  return (
    <BaseBlock>
      <Icon {...config} />
    </BaseBlock>
  );
}

IconInContext.Component = Icon;
