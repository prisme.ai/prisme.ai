import * as React from 'react';
const IconCopy = ({
  size,
  color = 'currentColor',
  ...props
}: { size?: number; fill?: string } & React.SVGProps<any>) => (
  <svg
    width={size || 20}
    height={size || 24}
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M20 5.2C20 4.53726 19.4627 4 18.8 4H10C9.33726 4 8.8 4.53726 8.8 5.2V8.8H5.2C4.53726 8.8 4 9.33726 4 10V18.8C4 19.4627 4.53726 20 5.2 20H14C14.6627 20 15.2 19.4627 15.2 18.8V15.2H18.8C19.4627 15.2 20 14.6627 20 14V5.2ZM15.2 13.6H18.4V5.6H10.4V8.8H14C14.6627 8.8 15.2 9.33726 15.2 10V13.6Z"
      fill={color}
    />
  </svg>
);
export default IconCopy;
