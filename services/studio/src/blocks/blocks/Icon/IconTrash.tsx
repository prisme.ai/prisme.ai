import * as React from 'react';
const TrashIcon = ({
  size,
  color = 'currentColor',
  ...props
}: { size?: number; fill?: string } & React.SVGProps<any>) => (
  <svg
    width={size || 20}
    height={size || 24}
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M7.41633 5C8.18779 3.23448 9.94873 2 12 2C14.0512 2 15.8121 3.23448 16.5836 5H21C21.5523 5 22 5.44772 22 6C22 6.55228 21.5523 7 21 7H19.9356L19.06 20.133C18.99 21.1836 18.1174 22 17.0644 22H6.93555C5.88262 22 5.01002 21.1836 4.93998 20.133L4.06445 7H3C2.44772 7 2 6.55228 2 6C2 5.44772 2.44772 5 3 5H7.41633ZM9.76363 5C10.3133 4.38612 11.112 4 12 4C12.8879 4 13.6866 4.38612 14.2363 5H9.76363ZM11 11C11 10.4477 10.5523 10 10 10C9.44772 10 9 10.4477 9 11V16C9 16.5523 9.44772 17 10 17C10.5523 17 11 16.5523 11 16V11ZM14 10C14.5523 10 15 10.4477 15 11V16C15 16.5523 14.5523 17 14 17C13.4477 17 13 16.5523 13 16V11C13 10.4477 13.4477 10 14 10Z"
      fill={color}
    />
  </svg>
);
export default TrashIcon;
