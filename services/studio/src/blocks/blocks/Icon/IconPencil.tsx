import * as React from 'react';
const IconPencil = ({
  size,
  color = 'currentColor',
  ...props
}: { size?: number; fill?: string } & React.SVGProps<any>) => (
  <svg
    width={size || 20}
    height={size || 24}
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M15.0858 2.99997C15.8668 2.21892 17.1332 2.21892 17.9142 2.99997L21 6.08576C21.7811 6.86681 21.781 8.13314 21 8.91418L19.4142 10.5L13.5 4.58576L15.0858 2.99997Z"
      fill={color}
    />
    <path
      d="M12.0858 5.99997L2.58579 15.5C2.21071 15.875 2 16.3838 2 16.9142V20C2 21.1045 2.89543 22 4 22H7.08579C7.61622 22 8.12493 21.7893 8.5 21.4142L18 11.9142L12.0858 5.99997Z"
      fill={color}
    />
  </svg>
);
export default IconPencil;
