import * as React from 'react';
const IconMagnifyingFilled = ({
  size,
  color = 'currentColor',
  ...props
}: { size?: number; fill?: string } & React.SVGProps<any>) => (
  <svg
    width={size || 24}
    height={size || 24}
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M5 11.2222C5 7.78579 7.78579 5 11.2222 5C14.6587 5 17.4445 7.78579 17.4445 11.2222C17.4445 12.6601 16.9567 13.984 16.1377 15.0377L18.7723 17.6723C19.076 17.976 19.076 18.4685 18.7723 18.7722C18.4685 19.0759 17.9761 19.0759 17.6723 18.7722L15.0378 16.1376C13.9841 16.9567 12.6601 17.4445 11.2222 17.4445C7.78579 17.4445 5 14.6587 5 11.2222Z"
      fill={color}
    />
  </svg>
);
export default IconMagnifyingFilled;
