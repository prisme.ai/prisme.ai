import * as React from 'react';
const IconNews = ({
  size = 24,
  color = 'currentColor',
  ...props
}: { size?: number; fill?: string } & React.SVGProps<any>) => (
  <svg
    width={size}
    height={size}
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path d="M8.80005 11.4V9.80005H11.2V11.4H8.80005Z" fill="#55555E" />
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M4 6.6C4 5.71634 4.71634 5 5.6 5H14.4C15.2837 5 16 5.71634 16 6.6V11.4H18.4C19.2837 11.4 20 12.1163 20 13V16.6C20 18.1464 18.7464 19.4 17.2 19.4H6.8C5.2536 19.4 4 18.1464 4 16.6V6.6ZM17.2 17.8C17.8627 17.8 18.4 17.2627 18.4 16.6V13H16V16.6C16 17.2627 16.5373 17.8 17.2 17.8ZM7.2 15.4C7.2 14.9582 7.55818 14.6 8 14.6H12C12.4418 14.6 12.8 14.9582 12.8 15.4C12.8 15.8418 12.4418 16.2 12 16.2H8C7.55818 16.2 7.2 15.8418 7.2 15.4ZM8 8.2C7.55818 8.2 7.2 8.55818 7.2 9V12.2C7.2 12.6418 7.55818 13 8 13H12C12.4418 13 12.8 12.6418 12.8 12.2V9C12.8 8.55818 12.4418 8.2 12 8.2H8Z"
      fill={color}
    />
  </svg>
);
export default IconNews;
