import { cloneElement, FC, isValidElement, memo, ReactElement, useMemo } from 'react';
import { BuiltinIcons, IconProps } from './types';

export const Icon = memo(function Icon({ icon, className = '', rotate }: IconProps) {
  const style = useMemo(() => {
    if (!rotate) return {};
    return {
      transform: `rotate(${rotate}deg)`,
    };
  }, [rotate]);
  if (!icon) return null;
  if (typeof icon === 'function') {
    const IconC = icon as FC<{ className?: string }>;
    return <IconC className={className} />;
  }
  if (typeof icon === 'object' && isValidElement(icon))
    return cloneElement(icon as ReactElement, {
      className,
    });
  if (typeof icon !== 'string') return <>{icon}</>;
  if (Object.keys(BuiltinIcons).includes(icon)) {
    const Component = BuiltinIcons[icon as keyof typeof BuiltinIcons];
    return <Component className={className} style={style} />;
  }
  if (icon.match(/^<svg/)) {
    return (
      <span
        className={`icon-svg ${className}`}
        dangerouslySetInnerHTML={{ __html: icon }}
        style={style}
      />
    );
  }

  return (
    <img
      src={icon}
      className={className}
      style={style}
      alt={typeof icon === 'string' ? icon : ''}
    />
  );
});
