import IconPrismeAI from '@/svgs/logo.svg';
import IconGear from './IconGear';
import IconShare from '@/svgs/share.svg';
import IconHome from '@/svgs/home.svg';
import IconCharts from './IconCharts';
import IconPlus from '@/svgs/plus.svg';
import IconSearch from '@/svgs/magnifier.svg';
import IconCross from '@/svgs/cross-large.svg';
import IconThreeDots from '@/svgs/three-dots.svg';
import IconTrash from '@/svgs/trash.svg';
import IconSend from './IconSend';
import IconAttachment from './IconAttachment';
import IconPencil from './IconPencil';
import IconChevron from '@/svgs/chevron.svg';
import IconCopy from '@/svgs/copy.svg';
import IconMagnifier from '@/svgs/magnifier.svg';
import IconMagnifyingFilled from '@/svgs/magnifier.svg';
import IconReload from '@/svgs/reload.svg';
import IconThumb from './IconThumb';
import IconTestTube from './IconTestTube';
import IconHelp from '@/svgs/info.svg';
import IconBrain from './IconBrain';
import IconRobot from './IconRobot';
import IconFirework from './IconFirework';
import IconNews from './IconNews';
import IconStore from './IconStore';
import IconArrow from './IconArrow';
import { ReactNode } from 'react';

export const BuiltinIcons = {
  'prisme.ai': IconPrismeAI,
  arrow: IconArrow,
  attachment: IconAttachment,
  brain: IconBrain,
  charts: IconCharts,
  chevron: IconChevron,
  copy: IconCopy,
  cross: IconCross,
  firework: IconFirework,
  gear: IconGear,
  help: IconHelp,
  home: IconHome,
  magnifier: IconMagnifier,
  'magnifying-filled': IconMagnifyingFilled,
  news: IconNews,
  pencil: IconPencil,
  plus: IconPlus,
  reload: IconReload,
  robot: IconRobot,
  search: IconSearch,
  send: IconSend,
  share: IconShare,
  store: IconStore,
  'test-tube': IconTestTube,
  'three-dots': IconThreeDots,
  thumb: IconThumb,
  trash: IconTrash,
} as const;

export type Icons = keyof typeof BuiltinIcons | string;

export interface IconProps {
  icon: string | ReactNode;
  className?: string;
  rotate?: number;
}
