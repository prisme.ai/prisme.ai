import { useBlock } from '../../Provider';
import Action, { ActionConfig } from './Action';
import { BaseBlock } from '../BaseBlock';
import defaultStyles from './defaultStyles';

export const ActionInContext = () => {
  const { config, events } = useBlock<ActionConfig>();

  return (
    <BaseBlock defaultStyles={defaultStyles}>
      <Action {...config} events={events} />
    </BaseBlock>
  );
};
ActionInContext.styles = defaultStyles;
export default ActionInContext;
