const css = String.raw;
export const defaultStyles = css`
  :block {
    padding: 1rem;
  }
  .pr-block-action-wrapper__disabled {
    pointer-events: none;
  }
  :block.pr-block-action--event .pr-block-action__button,
  :block.pr-block-action--upload .pr-block-action__button {
    background: var(--accent-color, var(--pr-accent-color));
    color: var(--accent-contrast-color, var(--pr-accent-color-contrast));
    border-radius: 0.5rem;
    padding: 0.5rem 1rem;
  }
  :root .pr-block-action__confirm-button {
    margin-left: 1rem;
  }

  :block.pr-block-action--upload .pr-block-action__button {
    padding: 0;
  }
  :block.pr-block-action--upload .pr-block-action__upload-label {
    display: flex;
    cursor: pointer;
    padding: 0.5rem 1rem;
  }
`;

export default defaultStyles;
