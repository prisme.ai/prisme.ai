import { Dropdown } from 'antd';
import { Icon } from '../Icon/Icon';
import { BaseBlockConfig } from '../types';
import { DropDownProps, isTriggerWithIcon } from './types';
import { MouseEvent, useMemo } from 'react';
import { isAction } from '@/blocks/utils/isAction';
import { BasicAction } from '@/blocks/utils/BasicAction';
import TextInBlockContent from '@/blocks/utils/TextInBlockContent';
import GenericBlock from '@/blocks/utils/GenericBlock';
import { BlockContent } from '@/blocks/utils/types';

const ItemRenderer = ({ item }: { item: DropDownProps['items'][number] }) => {
  if (isAction(item)) {
    const { text, icon, type, value, payload } = item;
    return (
      <BasicAction
        type={type}
        value={value}
        payload={payload}
        className="pr-block-drop-down-overlay-menu-item"
      >
        <>
          {icon && <Icon icon={icon} className="pr-block-drop-down-overlay-menu-item-icon" />}
          {text && <TextInBlockContent text={text as BlockContent} />}
        </>
      </BasicAction>
    );
  }
  return <GenericBlock content={item} />;
};

export const DropDown = ({
  trigger,
  className,
  items,
  placement,
}: DropDownProps & BaseBlockConfig) => {
  const { menuItems, selectedKey } = useMemo(() => {
    const selectedKey = `${
      (items &&
        Array.isArray(items) &&
        items.findIndex((item) => isAction(item) && item.selected)) ||
      ''
    }`;
    const menuItems = ((Array.isArray(items) && items) || []).map((item, key) => ({
      key: `${key}`,
      label: <ItemRenderer item={item} />,
      selected: isAction(item) && item.selected,
      onClick(e: { domEvent: MouseEvent<HTMLElement> | React.KeyboardEvent<HTMLElement> }) {
        const button = (e.domEvent.target as HTMLLIElement).querySelector('button');
        button?.click();
      },
    }));
    return { menuItems, selectedKey };
  }, [items]);

  return (
    <Dropdown
      className={`pr-block-drop-down ${className}`}
      placement={placement}
      menu={{
        items: menuItems,
        selectedKeys: [selectedKey],
        className: `pr-block-drop-down-overlay ${className}`,
      }}
      trigger={['click']}
    >
      <button type="button" className="pr-block-drop-down-trigger">
        {isTriggerWithIcon(trigger) && (
          <>
            <Icon icon={trigger.icon} />
            {trigger.text && (
              <GenericBlock
                content={trigger.text}
                className="product-layout-content-header-additional-content"
                ifString={({ content, className }) => <div className={className}>{content}</div>}
              />
            )}
          </>
        )}
        {!isTriggerWithIcon(trigger) && (
          <GenericBlock
            content={trigger}
            className="product-layout-content-header-additional-content"
            ifString={({ content, className }) => <div className={className}>{content}</div>}
          />
        )}
      </button>
    </Dropdown>
  );
};

export default DropDown;
