import { DropdownProps } from 'antd';
import { Icons } from '../Icon/types';
import { BlockContent } from '@/blocks/utils/types';
import { ActionConfig } from '../Action/Action';

export interface TriggerWithIcon {
  icon: Icons;
  text: BlockContent;
}

export interface DropDownProps {
  trigger: BlockContent | TriggerWithIcon;
  items: (
    | (ActionConfig & {
        icon?: Icons;
        selected: string;
      })
    | BlockContent
  )[];
  placement: DropdownProps['placement'];
}

export function isTriggerWithIcon(trigger: DropDownProps['trigger']): trigger is TriggerWithIcon {
  if (!trigger) return false;
  if (typeof trigger === 'string') return false;
  const { icon } = trigger as TriggerWithIcon;
  return !!icon;
}
