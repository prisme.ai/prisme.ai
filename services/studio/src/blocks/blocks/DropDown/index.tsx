import { useBlock } from '@/blocks/Provider';
import { DropDownProps } from './types';
import { BaseBlock } from '../BaseBlock';
import { defaultStyles } from './defaultStyles';
import DropDown from './DropDown';

export * from './DropDown';

export const DropDownInContext = () => {
  const { config } = useBlock<DropDownProps>();
  return (
    <BaseBlock defaultStyles={defaultStyles}>
      <DropDown {...config} />
    </BaseBlock>
  );
};

export default DropDownInContext;
