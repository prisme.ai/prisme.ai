const css = String.raw;
export const defaultStyles = css`
  :block .pr-block-rich-text svg,
  :block .pr-block-rich-text p {
    color: var(--pr-main-text);
  }
  :root .ant-dropdown .ant-dropdown-menu {
    background-color: var(--pr-menu-bg-color);
    color: var(--pr-main-text);
  }
  :root button p {
    color: var(--pr-main-text);
  }
  :root .pr-block-drop-down-overlay-menu-item .pr-block-drop-down-overlay {
    max-height: 100vh;
    overflow: auto;
  }
  :root .pr-block-drop-down-overlay-menu-item {
    display: flex;
    flex-direction: row;
    align-items: center;
  }
  :root .pr-block-drop-down-overlay-menu-item-icon {
    margin-right: 1rem;
  }
  :root .pr-block-drop-down-overlay-menu-item svg {
    width: 1rem;
    margin-right: 0.6rem;
  }
`;
