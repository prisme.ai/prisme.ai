import { useBlock } from '@/blocks/Provider';
import Footer, { FooterConfig } from './Footer';
import { BaseBlock } from '../BaseBlock';
import { defaultStyles } from './defaultStyles';

export const FooterInContext = () => {
  const { config } = useBlock<FooterConfig>();
  return (
    <BaseBlock defaultStyles={defaultStyles}>
      <Footer {...config} />
    </BaseBlock>
  );
};
FooterInContext.styles = defaultStyles;

export default FooterInContext;
