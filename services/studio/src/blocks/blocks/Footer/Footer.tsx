import GenericBlock from '@/blocks/utils/GenericBlock';
import { BlocksListConfig } from '../BlocksList/BlocksList';
import { BaseBlockConfig } from '../types';

export interface FooterConfig extends BaseBlockConfig {
  content: BlocksListConfig;
}

export const Footer = ({ content, className = '', sectionId = '' }: FooterConfig) => {
  return (
    <footer className={`pr-block-footer ${className}`} id={sectionId}>
      {content && <GenericBlock content={content.blocks} />}
    </footer>
  );
};

export default Footer;
