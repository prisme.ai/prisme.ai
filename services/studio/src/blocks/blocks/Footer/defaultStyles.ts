const css = String.raw;
export const defaultStyles = css`
  :block {
    display: flex;
    flex: 1;
    flex-direction: column;
    justify-content: flex-end;
  }
  :block > .pr-block-blocks-list {
    padding: 2rem 2rem 6rem 2rem;
    background: var(--color-accent-dark);
    color: var(--accent-contrast-color);
  }
`;
