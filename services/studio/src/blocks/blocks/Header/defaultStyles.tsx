const css = String.raw;
export const defaultStyles = css`
  :block {
    min-height: 4rem;
    --pr-header-color: var(--color-accent, rgba(0, 0, 0, 0.8));

    --pr-header-logo-height: 3rem;
  }
  :block.pr-block-header--fixed {
    --pr-header-contrast: var(--color-accent-contrast, white);
  }
  .pr-block-header__container {
    display: flex;
    flex: 1 1 0%;
    justify-content: space-between;
    position: relative;
    background-color: var(--pr-header-contrast);
  }
  :block.pr-block-header--fixed {
    position: relative;
    z-index: 1;
  }
  :block.pr-block-header--fixed .pr-block-header__container {
    position: fixed;
    left: 0;
    right: 0;
    transition: background-color 0.2s ease-in;
  }
  :block.pr-block-header--fixed.pr-block-header--has-scrolled .pr-block-header__container {
    background-color: var(--pr-header-color);
  }
  :block.pr-block-header--fixed.pr-block-header--has-scrolled
    .pr-block-header__container
    *:not(svg, path) {
    color: var(--pr-header-contrast);
  }

  @media (min-width: 768px) {
    :block {
      align-items: center;
    }
  }

  .pr-block-header__left {
    display: flex;
    margin: 1rem;
    z-index: 1;
  }

  .pr-block-header__logo,
  .pr-block-header__logo a {
    display: flex;
    justify-content: center;
  }
  .pr-block-header__logo {
    margin-right: 1rem;
    height: var(--pr-header-logo-height, 3rem);
  }

  .pr-block-header__logo-image {
    height: 100%;
  }

  .pr-block-header__title {
    display: flex;
    align-items: center;
    margin: 0;
    font-weight: 700;
    color: var(--color-text);
  }

  .pr-block-header__right {
    display: flex;
    justify-content: flex-end;
    color: var(--color-text);
    background-color: transparent;
  }
  .pr-block__menu {
    display: flex;
    flex-direction: row;
  }
  .pr-block__menu-toggle {
    display: none;
  }
  .pr-block__menu-item {
    padding: 0.5rem 1rem;
    display: flex;
    align-items: center;
  }
  @media (max-width: 414px) {
    :block {
      height: 4rem;
    }
    .pr-block-header__container {
      margin-bottom: 2rem;
    }
    .pr-block__menu {
      position: absolute;
      top: 0;
      bottom: 0;
      left: 0;
      right: 0;
      background-color: var(--pr-header-color);
      transition: transform 0.2s ease-in;
      transform: translate3d(0, -100%, 0);
      padding-top: 6rem;
      height: 100vh;
      flex-direction: column;
      color: var(----pr-header-contrast);
      padding: 10rem 2rem 0 2rem;
    }
    .pr-block-header__container *:not(svg, path) {
      color: var(--color-text);
    }
    :block.pr-block-header--fixed.pr-block-header--has-scrolled
      .pr-block-header__container
      *:not(svg, path) {
      color: var(--pr-header-contrast);
    }
    .pr-block__menu--visible {
      transform: translate3d(0, 0, 0);
    }
    .pr-block__menu-toggle {
      display: flex;
      position: absolute;
      top: 1rem;
      right: 1rem;
      font-size: 1.8rem;
    }
    .pr-block__menu-toggle svg path {
      color: var(--pr-header-contrast);
    }
    :block.pr-block-header--fixed:not(.pr-block-header--has-scrolled)
      .pr-block__menu-toggle
      svg
      path {
      color: var(--accent-color);
    }
    :block.pr-block-header--fixed:not(.pr-block-header--has-scrolled)
      .pr-block__menu--visible
      .pr-block__menu-toggle
      svg
      path {
      color: var(--accent-contrast-color);
    }
    .pr-block__menu-toggle--close svg path {
      color: var(--accent-contrast-color);
    }
    :block.pr-block-header--fixed .pr-block__menu-toggle--close svg path {
      color: var(--pr-header-contrast);
    }
  }
  .pr-block__menu .pr-block-action {
    transition: color 0.1s ease-in;
  }
  .pr-block__menu .pr-block-action:hover {
    color: var(--color-accent);
  }
  :block.pr-block-header--fixed .pr-block__menu .pr-block-action:hover {
    color: var(--pr-header-color);
  }
`;
