import { useBlocks } from '../../Provider/blocksContext';
import { BaseBlockConfig } from '../types';
import { useCallback, useEffect, useMemo, useState } from 'react';
import useLocalizedText from '@/utils/useLocalizedText';
import Action, { ActionConfig, ActionProps } from '../Action/Action';
import { BlocksListConfig } from '../BlocksList/BlocksList';
import CloseIcon from '@/svgs/close.svg';
import MenuIcon from '@/svgs/burger.svg';

export interface HeaderConfig extends BaseBlockConfig {
  title?: Prismeai.LocalizedText;
  level: 1 | 2 | 3 | 4 | 5 | 6;
  logo?: {
    src: string;
    alt: string;
    action?: Omit<ActionConfig, 'text'>;
  };
  nav: (ActionConfig | BlocksListConfig)[];
  fixed?: boolean;
}

export interface HeaderProps extends HeaderConfig, Pick<ActionProps, 'events'> {}

const isAction = (item: ActionConfig | BlocksListConfig): item is ActionConfig => {
  return !(item as BlocksListConfig).blocks;
};

interface MenuItemProps extends Pick<ActionProps, 'events'> {
  item: HeaderConfig['nav'][number];
  onClick?: () => void;
}
const MenuItem = ({ item, events, onClick }: MenuItemProps) => {
  const {
    utils: { BlockLoader },
  } = useBlocks();
  if (isAction(item)) {
    return <Action {...item} events={events} onClick={onClick} />;
  }
  return <BlockLoader name="BlocksList" config={item} />;
};

export const Header = ({
  events,
  className,
  level = 1,
  fixed,
  sectionId = '',
  ...config
}: HeaderProps) => {
  const { localize } = useLocalizedText();
  const [visible, setVisible] = useState(false);
  const [hasScrolled, setHasScrolled] = useState(false);
  const nav = config.nav && Array.isArray(config.nav) ? config.nav : [];

  useEffect(() => {
    const listener = () => {
      setHasScrolled(window.scrollY > 0);
    };
    window.addEventListener('scroll', listener);
    return () => {
      window.removeEventListener('scroll', listener);
    };
  });

  const logo = useMemo(
    () =>
      config.logo ? (
        <img src={config.logo.src} alt={config.logo.alt} className="pr-block-header__logo-image" />
      ) : null,
    [config.logo],
  );

  const H = `h${level}` as keyof JSX.IntrinsicElements;
  const title = localize(config.title);

  const toggle = useCallback(() => {
    setVisible((visible) => !visible);
  }, []);

  return (
    <div
      className={`pr-block-header ${className} ${
        fixed ? 'pr-block-header--fixed' : ''
      } ${hasScrolled ? 'pr-block-header--has-scrolled' : ''}`}
      id={sectionId}
    >
      <div className="pr-block-header__container">
        <div className="pr-block-header__left">
          {logo && (
            <div className="pr-block-header__logo">
              {config.logo?.action && (
                <Action {...config.logo.action} text={logo} events={events} />
              )}
              {!config.logo?.action && logo}
            </div>
          )}
          {title && <H className="pr-block-header__title">{title}</H>}
        </div>
        {nav && nav.length > 0 && (
          <div className="pr-block-header__right">
            <button className="pr-block__menu-toggle" onClick={toggle}>
              <MenuIcon />
            </button>
            <div
              className={`pr-block__menu ${
                visible ? 'pr-block__menu--visible' : 'pr-block__menu--hidden'
              }`}
            >
              {nav.map((item, k) => (
                <div key={k} className="pr-block__menu-item">
                  <MenuItem item={item} events={events} onClick={toggle} />
                </div>
              ))}
              <button
                className="pr-block__menu-toggle pr-block__menu-toggle--close"
                onClick={toggle}
              >
                <CloseIcon />
              </button>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default Header;
