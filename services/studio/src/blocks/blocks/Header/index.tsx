import { useBlock } from '@/blocks/Provider';
import { Header, HeaderConfig } from './Header';
import { BaseBlock } from '../BaseBlock';
import { defaultStyles } from './defaultStyles';

export const HeaderInContext = () => {
  const { config, events } = useBlock<HeaderConfig>();
  return (
    <BaseBlock defaultStyles={defaultStyles}>
      <Header {...config} events={events} />
    </BaseBlock>
  );
};

HeaderInContext.styles = defaultStyles;

export default HeaderInContext;
