const css = String.raw;
export const defaultStyles = css`
  :block {
    --accent-color: var(--pr-accent-color);
    --title-color: var(--pr-accent-color);
    --selected-menu-item: var(--pr-accent-color-contrast);
    --selected-menu-item-bg: var(--pr-accent-color);
    --highlighted-bg-color: var(--pr-accent-color);
    --highlighted-color: var(--pr-accent-color-contrast);
    --pr-block-product-layout-text-color: var(--pr-accent-color);
    --product-icon: var(--pr-accent-color);

    display: flex;
    flex-direction: column;
    min-height: 100vh;
    max-height: 100vh;
    min-width: 100%;
    max-width: 100vw;
    flex: 1;
    color: var(--pr-main-text);
  }
  @media (min-width: 768px) {
    :block {
      flex-direction: row;
    }
  }

  :root[data-color-scheme='dark'] :block {
    --pr-block-product-layout-text-color: rgb(204, 204, 204);
    --title-color: var(--main-text);
    --selected-menu-item: var(--main-text);
    --selected-menu-item-bg: var(--pr-menu-bg-color);
    --highlighted-bg-color: #535354;
    --highlighted-color: white;
    --product-icon: var(--pr-main-text);
  }

  .pr-form-label {
    color: var(--main-text);
  }

  .product-layout-sidebar {
    display: flex;
    flex-direction: column;
    flex: none;
    background-color: var(--pr-block-product-layout-sidebar-bg-color);
    width: 5rem;
    overflow: hidden;
    color: var(--pr-main-text);
    transition: width 0.2s ease-in-out;
    position: relative;
    margin: 0.9rem;
    border-radius: 0.9rem;
  }
  @media (max-width: 769px) {
    .product-layout-sidebar {
      background: none;
      position: absolute;
    }
  }

  .product-layout-sidebar a {
    color: inherit;
  }
  @media (min-width: 768px) {
    .product-layout-sidebar--open {
      width: 300px;
    }
  }

  .product-layout-sidebar-buttons {
    display: flex;
    flex: 1;
    flex-direction: column;
    overflow: hidden;
  }
  @media (max-width: 769px) {
    .product-layout-sidebar-buttons {
      position: fixed;
      top: 0;
      bottom: 0;
      width: 100%;
      max-width: calc(300px + 1.8rem);
      background-color: var(--pr-accent-color-contrast);
      left: 0;
      transform: translate3d(-100%, 0, 0);
      padding: 5rem 0.9rem 0.9rem 0.9rem;
      transition:
        transform 0.2s ease-in,
        box-shadow 0.2s ease-in;
      z-index: 2;
    }
  }
  @media (min-width: 768px) {
    .product-layout-sidebar-buttons {
      display: flex;
    }
  }

  @media (max-width: 769px) {
    .product-layout-sidebar--open .product-layout-sidebar-buttons {
      box-shadow: 1rem 0 2rem rgba(0, 0, 0, 0.2);
    }
  }
  @media (max-width: 769px) {
    .product-layout-sidebar--open .product-layout-sidebar-buttons {
      transform: translate3d(0, 0, 0);
    }
  }
  .product-layout-sidebar-bottom {
    display: flex;
    flex-direction: column;
    flex: 1;
    align-items: end;
    justify-content: end;
  }
  .product-layout-sidebar-toggle {
    display: flex;
    flex-direction: column;
    z-index: 2;
  }
  .product-layout-sidebar-toggle .product-layout-sidebar-button-icon {
    transition: transform 0.2s ease-in;
  }
  @media (max-width: 769px) {
    .product-layout-sidebar-toggle
      .product-layout-sidebar-button:hover
      .product-layout-sidebar-button-icon,
    .product-layout-sidebar-toggle .product-layout-sidebar-button-icon,
    .product-layout-sidebar-toggle .product-layout-sidebar-button,
    .product-layout-sidebar--open
      .product-layout-sidebar-toggle
      .product-layout-sidebar-button:hover,
    .product-layout-sidebar--open
      .product-layout-sidebar-toggle
      .product-layout-sidebar-button:active {
      background-color: transparent;
      color: var(--pr-block-product-layout-text-color);
    }
  }
  @media (max-width: 769px) {
    .product-layout-sidebar-toggle .product-layout-sidebar-button-label {
      display: none;
    }
  }
  .product-layout-sidebar--open .product-layout-sidebar-toggle .product-layout-sidebar-button-icon {
    transform: rotate3d(0, 0, 1, 180deg);
  }
  .product-layout-sidebar-button-ctn {
    display: flex;
    width: 300px;
    padding: 0.9rem;
  }
  .product-layout-sidebar-button-ctn.warning {
    --pr-accent-color: var(--pr-error-color);
    --pr-accent-color-contrast: var(--pr-error-color-contrast);
  }
  @media (max-width: 768px) {
    .product-layout-sidebar-button-ctn {
      padding: 0;
    }
  }
  .product-layout-sidebar-button {
    display: flex;
    flex: 1;
    flex-direction: column;
    position: relative;
    justify-content: center;
    color: var(--pr-block-product-layout-text-color);
    border-radius: 0.6rem;
  }

  .product-layout-sidebar-button-icon {
    display: flex;
    padding: 0;
    justify-content: center;
    align-items: center;
    width: 5rem;
    color: var(--pr-block-product-layout-text-color);
    border-radius: 0.6rem;
    width: 3.2rem;
    height: 3.2rem;
  }

  .product-layout-sidebar-button.product-layout-sidebar-button--highlighted,
  .product-layout-sidebar-button.product-layout-sidebar-button--selected {
    transition: background-color 0.2s ease-in;
  }
  .product-layout-sidebar--open .product-layout-sidebar-button:hover,
  .product-layout-sidebar--open .product-layout-sidebar-button:active,
  .product-layout-sidebar--open
    .product-layout-sidebar-button.product-layout-sidebar-button--highlighted,
  .product-layout-sidebar--open
    .product-layout-sidebar-button.product-layout-sidebar-button--selected,
  .product-layout-sidebar-button:hover .product-layout-sidebar-button-icon,
  .product-layout-sidebar-button:active .product-layout-sidebar-button-icon,
  .product-layout-sidebar-button.product-layout-sidebar-button--highlighted
    .product-layout-sidebar-button-icon,
  .product-layout-sidebar-button.product-layout-sidebar-button--selected
    .product-layout-sidebar-button-icon {
    background-color: var(--selected-menu-item-bg);
    color: var(--selected-menu-item);
  }
  .product-layout-sidebar-button-icon svg {
    width: 24px;
    height: 24px;
  }
  :root .product-layout-sidebar-button-confirm-overlay.warning {
    --tooltip-bg-color: var(--pr-error-color);
    --tooltip-text-color: var(--pr-error-color-contrast);
  }
  :root .product-layout-sidebar-button-confirm-overlay-buttons {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
  }
  :root .product-layout-sidebar-button-confirm-overlay-title {
    font-size: 1.2rem;
    margin-bottom: 1rem;
  }
  :root .product-layout-sidebar-button-confirm-overlay-button-no {
    font-weight: 500;
  }

  .product-layout-sidebar-items {
    overflow: hidden auto;
  }
  .product-layout-sidebar-items-section {
    margin-bottom: 1.5rem;
  }
  .product-layout-sidebar-items-section-title {
    font-size: 0.9rem;
    font-weight: 400;
    color: var(--pr-block-product-layout-text-color);
    margin: 0.6rem 0.9rem;
  }
  .product-layout-sidebar-items-section-item {
    font-size: 1.1rem;
    font-weight: 400;
    margin: 0.6rem 0.9rem;
  }

  .product-layout-content {
    display: flex;
    flex-direction: column;
    flex: 1;
    padding: 1.7rem 0.9rem;
    max-height: 100vh;
    overflow-x: hidden;
  }
  @media (max-width: 768px) {
    .product-layout-content {
      padding: 0;
    }
  }
  .product-layout-sidebar-button-label {
    position: absolute;
    left: 5rem;
    font-weight: 400;
    font-size: 1.2rem;
    display: flex;
    white-space: nowrap;
    opacity: 0;
    transition:
      opacity 0.2s ease-in,
      left 0.2s ease-in;
    color: var(--pr-block-product-layout-text-color);
  }
  .product-layout-sidebar-button-label p {
    color: var(--pr-block-product-layout-text-color);
  }
  .product-layout-sidebar-button:hover .product-layout-sidebar-button-label p,
  .product-layout-sidebar-button:active .product-layout-sidebar-button-label p {
    color: var(--pr-accent-color-contrast);
  }
  .product-layout-sidebar-button:hover .product-layout-sidebar-button-label,
  .product-layout-sidebar-button.product-layout-sidebar-button--selected
    .product-layout-sidebar-button-label {
    color: var(--selected-menu-item);
  }
  .product-layout-sidebar--open .product-layout-sidebar-button-label {
    opacity: 1;
    left: 4rem;
  }
  .product-layout-content-header {
    display: flex;
    z-index: 1;
    background: linear-gradient(var(--pr-main-bg-color) 70%, transparent 100%);
  }
  @media (max-width: 768px) {
    .product-layout-content-header {
      display: flex;
      margin: 1.7rem;
    }
  }
  .product-layout-button-ctn {
    display: flex;
    margin-right: 0.6rem;
  }
  .product-layout-button {
    display: flex;
    flex-direction: row;
    align-items: center;
    color: var(--pr-block-product-layout-text-color);
    padding: 0.7rem 0.9rem;
  }
  .product-layout-button.product-layout-button--highlighted {
    background-color: var(--highlighted-bg-color);
    color: var(--highlighted-color);
    border-radius: 0.9rem;
  }
  .product-layout-button-icon {
    margin-right: 0.6rem;
  }
  .product-layout-button-icon svg {
    width: 20px;
    height: 20px;
  }
  .product-layout-button-label {
    font-size: 1.2rem;
    font-weight: 500;
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;
    max-width: 100vw;
    transition: max-width 0.2s ease-in;
  }

  @media (max-width: 1200px) {
    .product-layout-sidebar--open
      + .product-layout-content
      .product-layout-content-additional-buttons
      .product-layout-button-label {
      max-width: 0rem;
    }

    .product-layout-sidebar--open
      + .product-layout-content
      .product-layout-content-additional-buttons
      .product-layout-button-icon {
      margin-right: 0;
    }
  }

  .product-layout-content-header--with-logo {
    flex-direction: row;
    align-items: center;
  }
  .product-layout-content-logo {
    max-width: 40px;
    max-height: 40px;
    margin-right: 0.9rem;
    border-radius: 0.3rem;
    margin-top: -0.5rem;
    color: var(--product-icon);
  }
  .product-layout-header-texts {
    display: flex;
    flex-direction: column;
    line-height: 1.5;
    margin-top: -5px;
    margin-right: 1.2rem;
    justify-content: center;
  }
  @media (max-width: 769px) {
    .product-layout-content-header--with-header {
      flex-direction: column;
    }
    .product-layout-content-header--with-header .product-layout-content-header {
      flex-direction: column;
    }
    .product-layout-content-header--with-header .product-layout-content-additional-buttons-ctn {
      position: absolute;
      top: 0.9rem;
      right: 0.9rem;
    }
  }

  .product-layout-content-title {
    color: var(--title-color);
    font-size: 1.5rem;
    font-weight: 700;
  }
  .product-layout-content-description {
    color: var(--pr-main-text);
    font-weight: 400;
    line-height: 1;
  }
  .product-layout-content-header-additional-content {
    display: flex;
    flex-direction: column;
    justify-content: center;
  }
  .product-layout-content-additional-buttons-ctn {
    display: flex;
    flex-direction: column;
    align-items: end;
    margin-right: 1.5rem;
    flex: 1;
  }
  .product-layout-content-header {
    padding-bottom: 2rem;
  }
  @media (max-width: 769px) {
    .product-layout-content-header {
      padding: 0 0.9rem 0.9rem 0.9rem;
      background: linear-gradient(var(--pr-main-bg-color) 85%, transparent 100%);
    }
    .product-layout-content-header-additional-content {
      margin-top: 0.3rem;
    }
    :block.product-layout--has-sidebar .product-layout-content-header {
      padding-left: 5rem;
    }
    .product-layout-content-additional-buttons-ctn {
      margin: 0;
    }
    .product-layout-content-additional-buttons-ctn .product-layout-button-ctn {
      margin-right: 0;
      margin-left: 0.6rem;
    }
    .product-layout-button-icon {
      margin: 0;
    }
    .product-layout-button-label {
      display: none;
    }
  }
  .product-layout-content-additional-buttons {
    display: flex;
    flex-direction: row;
  }
  .product-layout-content-ctn {
    overflow: auto;
  }
  .product-layout-content-ctn,
  .product-layout-content-panel {
    display: flex;
    flex: 1;
    flex-direction: column;
  }
  .product-layout-content-additional-buttons .pr-block-blocks-list {
    display: flex;
    align-items: center;
  }
  .product-layout-content-additional-buttons .pr-block-action {
    padding: 0 0.5rem;
  }
  .product-layout-assistant-ctn {
    --assistant-width: 400px;
    display: flex;
    position: relative;
    width: calc(var(--assistant-width) - 2rem);
    margin-right: calc(var(--assistant-width) * -1);
    margin-left: 2rem;
    transition: margin-right 0.2s ease-in;
    overflow: auto;
  }
  .product-layout-assistant-ctn.visible {
    margin-right: 0;
  }
  .product-layout-assistant__handle {
    display: none;
    position: absolute;
    top: 0;
    bottom: 0;
    justify-content: center;
    align-items: center;
    left: -2rem;
    width: 2rem;
    cursor: col-resize;
  }
  .product-layout-assistant-ctn.visible .product-layout-assistant__handle {
    display: flex;
  }
  .product-layout-assistant__handle svg {
    height: 2rem;
    width: 2rem;
    color: var(--accent-contrast-color);
  }
  .product-layout-assistant {
    width: 100%;
    height: 100%;
    border-radius: 10px;
  }

  .anticon-info-circle {
    color: var(--pr-block-product-layout-text-color);
  }

  .product-layout-content-ctn .product-layout-content-panel--2col,
  .product-layout-content-ctn .product-layout-content-panel--3col {
    flex-direction: row;
    border: 1px solid var(--pr-menu-border-color);
    border-radius: 1rem;
  }
  .product-layout-content-panel--2col > .pr-block-blocks-list__block,
  .product-layout-content-panel--3col > .pr-block-blocks-list__block {
    padding: 1rem;
    display: flex;
    flex-direction: column;
  }
  .product-layout-content-panel--2col > .pr-block-blocks-list__block:first-child,
  .product-layout-content-panel--3col > .pr-block-blocks-list__block:first-child {
    border-right: 1px solid var(--pr-menu-border-color);
  }

  .product-layout-content-panel--3col > .pr-block-blocks-list__block:nth-child(3) {
    border-left: 1px solid var(--pr-menu-border-color);
  }
  .product-layout-content-panel--2col > .pr-block-blocks-list__block:not(:first-child) {
    display: flex;
    flex: 1;
  }
  .product-layout-content-panel--3col > .pr-block-blocks-list__block:nth-child(1),
  .product-layout-content-panel--3col > .pr-block-blocks-list__block:nth-child(3) {
    max-width: 33%;
  }
  .product-layout-content-panel--3col > .pr-block-blocks-list__block:nth-child(2) {
    flex: 1;
  }
  .product-layout-content-tab {
    font-size: 16px;
    color: var(--pr-block-product-layout-text-color);
    font-weight: 500;
    margin-right: 2rem;
  }
  .product-layout-content-tab--active {
    font-weight: 700;
  }
  .product-layout-content-tabs {
    margin-bottom: 2rem;
  }
  .product-layout-content button.pr-block-action__button,
  .product-layout-content {
    --primary: var(--pr-block-product-layout-text-color);
  }
`;
