import { ReactElement } from 'react';
import { Icons } from '../Icon/types';
import { ActionConfig } from '../Action/Action';
import { BlocksListConfig } from '../BlocksList/BlocksList';
import { BlockContent } from '../../utils/types';

export interface SidebarHeaderProps {
  back?:
    | boolean
    | {
        label: string | Prismeai.LocalizedText;
        href: string;
      };
  buttons?:
    | ReactElement
    | BlocksListConfig['blocks']
    | ({
        icon?: Icons;
        highlighted?: boolean;
        hidden?: boolean;
      } & ActionConfig)[];
}

export interface ContentProps {
  title: BlockContent;
  logo?: string | BlockContent;
  description: BlockContent;
  header?: BlockContent;
  tabs: ({
    title: BlockContent;
    content: BlockContent;
    selected?: boolean;
    columns?: number;
  } & ActionConfig)[];
  additionalButtons?: (ActionConfig & {
    icon?: string;
    highlighted?: boolean;
    hidden?: boolean;
  })[];
  content?: BlockContent;
}

export type SidebarItem = {
  icon?: Icons;
  selected?: boolean;
} & ActionConfig;

export interface ProductLayoutProps {
  sidebar?: {
    header?: ReactElement | SidebarHeaderProps;
    items?: BlockContent | SidebarItem[];
    bottomItems?: ({
      icon?: Icons;
      selected?: boolean;
    } & ActionConfig)[];
    opened?: boolean;
    disabled?: boolean;
    autoClose?: boolean;
  };
  content?: ReactElement | ContentProps;
  toastOn?: string;
  assistant?: {
    url: string;
    visible?: boolean;
    content?: BlockContent;
  };
}
