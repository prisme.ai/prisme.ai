import { useEffect, useMemo } from 'react';
import { isBlock, isRenderProp, isString } from '../../utils/getContentType';
import { useProductLayoutContext } from './Provider';
import { ProductLayoutProps, SidebarHeaderProps, SidebarItem } from './types';
import IconBack from '../Icon/IconBack';
import useLocalizedText from '@/utils/useLocalizedText';
import GenericBlock from '../../utils/GenericBlock';
import ExpandIcon from '../Icon/IconExpand';
import SidebarButton from './SidebarButton';
import { Icon } from '../Icon/Icon';
import { translations } from './translations';
import { isAction } from '../../utils/isAction';
import { usePathname } from 'next/navigation';

export const SidebarHeader = ({ back, buttons }: SidebarHeaderProps) => {
  const { localize } = useLocalizedText();

  const renderButtons = useMemo(() => {
    if (isRenderProp(buttons)) {
      return buttons;
    }
    if (isBlock(buttons)) {
      return <GenericBlock content={buttons} />;
    }
    return buttons?.map((item, key) => {
      if (isAction(item)) {
        const { icon, hidden, ...button } = item;
        if (!icon || hidden) return null;
        return <SidebarButton key={key} icon={<Icon icon={icon} />} {...button} />;
      }

      return (
        <GenericBlock key={key} content={item} className="product-layout-sidebar-button-ctn" />
      );
    });
  }, [buttons]);

  return (
    <div className="product-layout-sidebar__header">
      {back && (
        <SidebarButton
          type="internal"
          value={typeof back === 'boolean' ? '/' : back.href}
          icon={<IconBack />}
          text={typeof back === 'boolean' ? translations.back : localize(back.label)}
        />
      )}
      {renderButtons}
    </div>
  );
};

function StructuredSidebarItems({ items }: { items: SidebarItem[] }) {
  const buttons = useMemo(
    () =>
      items.map((item, key) => {
        if (isAction(item)) {
          const { text, icon, type, value, ...button } = item;
          if (!icon) return null;
          return (
            <SidebarButton
              text={text}
              icon={<Icon icon={icon} />}
              type={type}
              value={value}
              {...button}
              key={`${text}${icon}${type}${value}`}
            />
          );
        }

        return <GenericBlock key={key} content={item} className="product-layout-sidebar-items" />;
      }),
    [items],
  );
  return <div className="product-layout-sidebar-items">{buttons}</div>;
}

const SidebarItems = ({
  items,
}: {
  items: NonNullable<ProductLayoutProps['sidebar']>['items'];
}) => {
  if (!items) return null;
  if (isRenderProp(items)) {
    return items;
  }
  if (isBlock(items)) {
    return <GenericBlock content={items} className="product-layout-sidebar-items" />;
  }
  if (isString(items)) return <>{items}</>;

  return <StructuredSidebarItems items={items} />;
};

const locales = {
  expand: {
    fr: 'Développer',
    en: 'Expand',
  },
  collapse: {
    fr: 'Réduire',
    en: 'Collapse',
  },
};

export const Sidebar = ({
  header,
  items,
  bottomItems,
  disabled = false,
  autoClose,
}: ProductLayoutProps['sidebar'] = {}) => {
  const { sidebarOpen, toggleSidebar } = useProductLayoutContext();
  const pathname = usePathname();

  useEffect(() => {
    if (autoClose) {
      toggleSidebar('close');
    }
  }, [autoClose, pathname, toggleSidebar]);

  return !disabled ? (
    <div className={`product-layout-sidebar ${sidebarOpen ? 'product-layout-sidebar--open' : ''}`}>
      <div className="product-layout-sidebar-buttons">
        {isRenderProp(header) ? header : <SidebarHeader {...header} />}

        <SidebarItems items={items} />

        {bottomItems && (
          <div className="product-layout-sidebar-bottom">
            <SidebarItems items={bottomItems} />
          </div>
        )}
      </div>

      <div className="product-layout-sidebar-toggle">
        <SidebarButton
          type="event"
          onClick={() => toggleSidebar()}
          icon={<ExpandIcon />}
          text={sidebarOpen ? locales.collapse : locales.expand}
        />
      </div>
    </div>
  ) : (
    <></>
  );
};

export default Sidebar;
