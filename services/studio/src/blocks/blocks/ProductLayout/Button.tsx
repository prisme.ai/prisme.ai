import { isLocalizedObject } from '@/utils/useLocalizedText';
import { Tooltip } from 'antd';
import { ReactNode, useCallback, useMemo, useRef, useState } from 'react';
import { useBlock } from '../../Provider';
import useLocalizedText from '@/utils/useLocalizedText';
import { ActionConfig } from '../Action/Action';
import GenericBlock from '../../utils/GenericBlock';
import { isBlock, isRenderProp } from '../../utils/getContentType';
import { translations } from './translations';
import Link from 'next/link';

export interface ButtonProps extends Omit<ActionConfig, 'type' | 'value'> {
  type?: ActionConfig['type'];
  value?: ActionConfig['value'];
  onClick?: () => void;
  icon?: string | ReactNode;
  selected?: boolean;
  highlighted?: boolean;
  classNamePrefix?: string;
}

export const Button = ({
  value,
  icon,
  onClick,
  selected,
  className = '',
  classNamePrefix = 'product-layout-button',
  payload,
  type,
  text,
  highlighted,
  confirm,
}: ButtonProps) => {
  const confirmed = useRef(!confirm);
  const { localize } = useLocalizedText();
  const { events } = useBlock();
  const Container = useMemo(() => {
    if (type && ['external', 'internal'].includes(type))
      return function ContainerAsLink(props: any) {
        return <Link href={value || ''} {...props} />;
      };
    return function ContainerAsButton(props: any) {
      return <button type="button" {...props} />;
    };
  }, [type, value]);

  const renderedText = useMemo(() => {
    if (isLocalizedObject(text)) return localize(text);
    if (isRenderProp(text)) return text;
    if (isBlock(text)) return <GenericBlock content={text} />;
  }, [text, localize]);

  const onClickHandler = useCallback(() => {
    if (!confirmed.current) return;
    if (onClick) return onClick();
    if (!value) return;
    if (type === 'script') {
      try {
        new Function(value)();
      } catch (e) {
        console.error(e);
      }
    }
    if (type === 'event') {
      events?.emit(value, payload);
    }
  }, [confirmed, onClick, value, type, events, payload]);

  const button = (
    <div className={`${classNamePrefix}-ctn ${className}`}>
      <Container
        onClick={onClickHandler}
        value={value}
        type={type}
        className={`${classNamePrefix} ${
          selected ? `${classNamePrefix}--selected` : ''
        } ${highlighted ? `${classNamePrefix}--highlighted` : ''} ${classNamePrefix || ''}`}
      >
        <Tooltip title={renderedText} placement="right">
          <span className={`${classNamePrefix}-icon`}>{icon}</span>
        </Tooltip>
        <span className={`${classNamePrefix}-label`}>{renderedText}</span>
      </Container>
    </div>
  );

  const [open, setOpen] = useState(false);

  if (confirm) {
    const placement = window.innerWidth > 768 ? confirm.placement : 'top';
    return (
      <Tooltip
        open={open}
        onOpenChange={setOpen}
        rootClassName={`${classNamePrefix}-confirm-overlay ${className}`}
        trigger={['click']}
        placement={placement}
        overlay={
          <>
            <div className={`${classNamePrefix}-confirm-overlay-title`}>
              {localize(confirm.label)}
            </div>
            <div className={`${classNamePrefix}-confirm-overlay-buttons`}>
              <button
                type="button"
                className={`${classNamePrefix}-confirm-overlay-button-yes`}
                onClick={() => {
                  confirmed.current = true;
                  onClickHandler();
                  confirmed.current = false;
                  setOpen(false);
                }}
              >
                {localize(confirm.yesLabel || translations.defaultYesLabel)}
              </button>
              <button
                className={`${classNamePrefix}-confirm-overlay-button-no`}
                type="button"
                onClick={() => setOpen(false)}
              >
                {localize(confirm.noLabel || translations.defaultNoLabel)}
              </button>
            </div>
          </>
        }
      >
        {button}
      </Tooltip>
    );
  }
  return button;
};
export default Button;
