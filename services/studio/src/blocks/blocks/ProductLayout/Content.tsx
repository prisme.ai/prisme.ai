import ContentPanel from './ContentPanel';
import { isBlock, isRenderProp } from '../../utils/getContentType';
import { ContentProps, ProductLayoutProps } from './types';
import GenericBlock from '../../utils/GenericBlock';
import { Icon } from '../Icon';
import Button from './Button';
import { isAction } from '../../utils/isAction';
import { memo } from 'react';

function isContentProps(content: any): content is ContentProps {
  if (!content) return false;
  const { logo, title, description, additionalButtons, tabs, content: c } = content as ContentProps;
  return !!(logo || title || description || additionalButtons || tabs || c);
}

const AdditionalButton = ({
  item,
}: {
  item: NonNullable<ContentProps['additionalButtons']>[number];
}) => {
  if (item.hidden) {
    return null;
  }
  if (isAction(item)) {
    const { icon, ...button } = item;
    return <Button {...button} icon={<Icon icon={icon} />} />;
  }
  return <GenericBlock content={item} />;
};

export const Content = memo(function Content({
  content,
  assistant,
}: {
  content: ProductLayoutProps['content'];
  assistant: ProductLayoutProps['assistant'];
}) {
  if (!content) return null;
  if (isRenderProp(content)) return content;
  if (isBlock(content))
    return <GenericBlock content={content} className="product-layout-content" />;
  if (!isContentProps(content)) return null;
  const hasHeader =
    content.logo ||
    content.title ||
    content.description ||
    content.header ||
    content.additionalButtons;

  return (
    <div className="product-layout-content">
      {hasHeader && (
        <div
          className={`product-layout-content-header ${
            content.logo ? 'product-layout-content-header--with-logo' : ''
          } ${content.header ? 'product-layout-content-header--with-header' : ''}`}
        >
          {content.logo && (
            <GenericBlock
              content={content.logo}
              className="product-layout-content-logo"
              ifString={({ content, className }) => <Icon className={className} icon={content} />}
            />
          )}
          <div className="product-layout-header-texts">
            {content.title && (
              <GenericBlock
                content={content.title}
                className="product-layout-content-title"
                ifString={({ content, className }) => <div className={className}>{content}</div>}
              />
            )}
            {content.description && (
              <GenericBlock
                content={content.description}
                className="product-layout-content-description"
                ifString={({ content, className }) => <div className={className}>{content}</div>}
              />
            )}
          </div>
          {content.header && (
            <GenericBlock
              content={content.header}
              className="product-layout-content-header-additional-content"
              ifString={({ content, className }) => <div className={className}>{content}</div>}
            />
          )}
          {content.additionalButtons && (
            <div className="product-layout-content-additional-buttons-ctn">
              <div className="product-layout-content-additional-buttons">
                {content.additionalButtons.map((item, key) => (
                  <AdditionalButton key={key} item={item} />
                ))}
              </div>
            </div>
          )}
        </div>
      )}
      <ContentPanel {...content} assistant={assistant} />
    </div>
  );
});

export default Content;
