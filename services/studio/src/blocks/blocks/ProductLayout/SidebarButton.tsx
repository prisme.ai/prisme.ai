import Button, { ButtonProps } from './Button';

export const SidebarButton = (props: ButtonProps) => (
  <Button {...props} classNamePrefix={`product-layout-sidebar-button`} />
);

export default SidebarButton;
