import { useBlock } from '@/blocks/Provider';
import { ProductLayoutProps } from './types';
import { BaseBlock } from '../BaseBlock';
import ProductLayout from './ProductLayout';
import { useProductLayoutContext } from './Provider';
import IconBack from '../Icon/IconBack';
import IconGear from '../Icon/IconGear';
import IconHome from '../Icon/IconHome';
import IconShare from '../Icon/IconShare';
import IconCharts from '../Icon/IconCharts';
import { defaultStyles } from './defaultStyles';

export const ProductLayoutInContext = () => {
  const { config } = useBlock<ProductLayoutProps>();
  return (
    <BaseBlock defaultStyles={defaultStyles}>
      <ProductLayout {...config} />
    </BaseBlock>
  );
};
ProductLayoutInContext.styles = defaultStyles;
ProductLayoutInContext.Component = ProductLayout;
ProductLayoutInContext.useProductLayoutContext = useProductLayoutContext;
ProductLayoutInContext.IconGear = IconGear;
ProductLayoutInContext.IconShare = IconShare;
ProductLayoutInContext.IconBack = IconBack;
ProductLayoutInContext.IconHome = IconHome;
ProductLayoutInContext.IconCharts = IconCharts;

export default ProductLayoutInContext;
