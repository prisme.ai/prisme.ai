import { BaseBlockConfig } from '../types';
import ProductLayoutProvider from './Provider';
import { ProductLayoutProps } from './types';
import Sidebar from './Sidebar';
import Content from './Content';
import GenericBlock from '../../utils/GenericBlock';
import { memo, useMemo } from 'react';

export const ProductLayout = memo(function ProductLayout({
  sidebar,
  content,
  className,
  toastOn,
  assistant,
}: ProductLayoutProps & BaseBlockConfig) {
  const toastBlocks = useMemo(() => toastOn && [{ slug: 'Toast', toastOn }], [toastOn]);
  return (
    <ProductLayoutProvider opened={sidebar?.opened}>
      <div
        className={`product-layout ${sidebar ? 'product-layout--has-sidebar' : ''} ${className}`}
      >
        {sidebar && <Sidebar {...sidebar} />}
        <Content content={content} assistant={assistant} />
        {toastBlocks && <GenericBlock content={toastBlocks} />}
      </div>
    </ProductLayoutProvider>
  );
});

export default ProductLayout;
