import { createContext, ReactNode, useCallback, useContext, useState } from 'react';

export interface ProductLayoutContext {
  sidebarOpen: boolean;
  toggleSidebar: (state?: 'open' | 'close') => void;
}
export const productLayoutContext = createContext<ProductLayoutContext | undefined>(undefined);
export function useProductLayoutContext() {
  const context = useContext(productLayoutContext);
  if (context === undefined) {
    throw new Error('useProductLayoutContext must be used within a ProductLayoutProvider');
  }
  return context;
}

const keyProductLayoutSidebarIsOpen = 'productLayoutSidebarIsOpen';
function getOpenedState(initialState = false) {
  if (window.innerWidth < 768) return false;
  try {
    const isOpen = sessionStorage.getItem(keyProductLayoutSidebarIsOpen);
    if (isOpen === null) {
      throw new Error('');
    }
    return isOpen === 'true';
  } catch {
    return initialState;
  }
}
function saveOpenedState(state: boolean) {
  try {
    sessionStorage.setItem(keyProductLayoutSidebarIsOpen, `${state}`);
  } catch {}
}

interface ProductLayoutProviderProps {
  children: ReactNode;
  opened?: boolean;
}
export const ProductLayoutProvider = (props: ProductLayoutProviderProps) => {
  const [sidebarOpen, setSidebarOpen] = useState(getOpenedState(props.opened));
  const toggleSidebar: ProductLayoutContext['toggleSidebar'] = useCallback((state) => {
    setSidebarOpen((prev) => {
      const newState = state === undefined ? !prev : state === 'open';
      saveOpenedState(newState);
      return newState;
    });
  }, []);
  return (
    <productLayoutContext.Provider value={{ sidebarOpen, toggleSidebar }}>
      {props.children}
    </productLayoutContext.Provider>
  );
};

export default ProductLayoutProvider;
