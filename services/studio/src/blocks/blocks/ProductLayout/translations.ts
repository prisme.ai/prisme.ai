export const translations = {
  back: {
    fr: "Retour à l'accueil",
    en: 'Back to home',
  },
  defaultYesLabel: {
    fr: 'Oui',
    en: 'Yes',
  },
  defaultNoLabel: {
    fr: 'Non',
    en: 'No',
  },
};
