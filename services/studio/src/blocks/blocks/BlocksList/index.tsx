import { useBlock } from '../../Provider';
import { BaseBlock } from '../BaseBlock';
import { BlocksList, BlocksListConfig } from './BlocksList';

export const BlocksListInContext = () => {
  const { config } = useBlock<BlocksListConfig>();
  return (
    <BaseBlock>
      <BlocksList {...config} />
    </BaseBlock>
  );
};

export default BlocksListInContext;
