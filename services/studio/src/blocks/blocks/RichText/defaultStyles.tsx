const css = String.raw;
export const defaultStyles = css`
  p,
  h1,
  h2,
  h3,
  h4,
  h5,
  h6 {
    line-height: 1.8;
  }
  ol,
  ul {
    margin: 0.9rem 0 0.9rem 1.4rem;
  }
  li {
    list-style: disc;
  }
`;
