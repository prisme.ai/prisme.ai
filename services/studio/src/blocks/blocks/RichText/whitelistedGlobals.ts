import copy from '@/utils/copy';

const whitelist = [
  'console',
  'document',
  'setTimeout',
  'setInterval',
  'clearTimeout',
  'clearInterval',
  'addEventListener',
  'removeEventListener',
  'scrollY',
  'scrollX',
  'scrollTo',
  'innerWidth',
  'innerHeight',
  'Math',
  'Array',
  'Date',
  'Error',
  'Function',
  'Object',
  'RegExp',
  'String',
  'TypeError',
  'location',
  'getSelection',
  'Prisme',
];

function wrapFn(fn: Function) {
  if (fn.name.match(/^[A-Z]/)) return fn;
  return (...args: any) => fn(...args);
}

export function getGlobals(window: Window, base: Record<string, any>) {
  const globals: Record<string, any> = base;
  Object.defineProperties(
    globals,
    whitelist.reduce((prev, k) => {
      const v = window[k as keyof typeof window];
      if (typeof v === 'function') {
        return {
          ...prev,
          [k]: {
            value: wrapFn(v),
            enumerable: true,
          },
        };
      }
      return {
        ...prev,
        [k]: {
          get() {
            return window[k as keyof typeof window];
          },
          enumerable: true,
        },
      };
    }, {}),
  );

  globals.window = globals;
  globals.navigator = {
    clipboard: {
      writeText: copy,
    },
  };

  return globals;
}

export default getGlobals;
