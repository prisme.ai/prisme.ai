import ReactSyntaxHighlighter, { SyntaxHighlighterProps } from 'react-syntax-highlighter';
import { github, monokai } from 'react-syntax-highlighter/dist/esm/styles/hljs';
import { useColorSchemeManager } from '@/providers/ColorSchemeManager';

export const SyntaxHighlighter = (props: SyntaxHighlighterProps) => {
  const { scheme } = useColorSchemeManager();

  return <ReactSyntaxHighlighter {...props} style={scheme === 'light' ? github : monokai} />;
};

export default SyntaxHighlighter;
