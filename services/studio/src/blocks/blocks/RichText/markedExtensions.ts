// Define a custom extension for <pr-block> tags that leaves them unchanged
export const prBlockExtension = {
  name: 'prBlock',
  level: 'block', // process as a block-level element
  start(src: string) {
    return src.indexOf('<pr-block');
  },
  tokenizer(src: string) {
    // This regex matches everything from <pr-block> to </pr-block>
    const rule = /^<pr-block([^>]*)>([\s\S]*?)<\/pr-block>/;
    const match = rule.exec(src);
    if (match) {
      return {
        type: 'prBlock', // custom token type
        raw: match[0], // the full text that matched
      };
    }
  },
  renderer(token: any) {
    // Instead of stripping out the content, return it exactly as found.
    return token.raw;
  },
};

export default [prBlockExtension];
