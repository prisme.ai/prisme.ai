import { describe, it, expect } from 'vitest';
import { addAllowUnsecureRecursively } from './utils';

describe('addAllowUnsecureRecursively', () => {
  it('should return primitives as is', () => {
    const inputNumber = 42;
    const inputString = 'hello';
    const inputNull = null;
    expect(addAllowUnsecureRecursively(inputNumber)).toBe(inputNumber);
    expect(addAllowUnsecureRecursively(inputString)).toBe(inputString);
    expect(addAllowUnsecureRecursively(inputNull)).toBe(inputNull);
  });

  it('should clone an object with slug "RichText" and add allowUnsecure = false', () => {
    const input = { slug: 'RichText' } as any;
    const output = addAllowUnsecureRecursively(input);
    expect(output.allowUnsecure).toBe(false);
    // Original object remains unchanged
    expect(input.allowUnsecure).toBeUndefined();
  });

  it('should clone an object without modifying if slug is not "RichText"', () => {
    const input = { slug: 'OtherSlug' };
    const output = addAllowUnsecureRecursively(input);
    expect(output.allowUnsecure).toBeUndefined();
    // Ensure a new object was returned
    expect(output).not.toBe(input);
  });

  it('should process nested objects correctly', () => {
    const input = {
      level1: {
        slug: 'RichText',
        level2: {
          slug: 'RichText',
          notRich: {
            slug: 'Other',
          },
        },
      },
    } as any;
    const output = addAllowUnsecureRecursively(input);
    expect(output.level1.allowUnsecure).toBe(false);
    expect(output.level1.level2.allowUnsecure).toBe(false);
    expect(output.level1.level2.notRich.allowUnsecure).toBeUndefined();

    // Original should remain unchanged
    expect(input.level1.allowUnsecure).toBeUndefined();
    expect(input.level1.level2.allowUnsecure).toBeUndefined();
  });

  it('should process arrays and clone each element', () => {
    const input = [
      { slug: 'RichText' },
      { slug: 'Other' },
      { nested: { slug: 'RichText' } },
    ] as any;
    const output = addAllowUnsecureRecursively(input);
    expect(output[0].allowUnsecure).toBe(false);
    expect(output[1].allowUnsecure).toBeUndefined();
    expect(output[2].nested.allowUnsecure).toBe(false);

    // Ensure the original array is not mutated
    expect(input[0].allowUnsecure).toBeUndefined();
    expect(input[2].nested.allowUnsecure).toBeUndefined();
  });

  it('should handle circular references without error', () => {
    const input: any = { slug: 'RichText' };
    // Create a circular reference
    input.self = input;
    const output = addAllowUnsecureRecursively(input);
    expect(output.allowUnsecure).toBe(false);
    expect(output.self).toBe(output);
    // Original remains unchanged
    expect(input.allowUnsecure).toBeUndefined();
  });

  it('should handle deep circular references', () => {
    const input: any = {};
    input.a = { slug: 'RichText' };
    input.b = { child: input.a };
    // Introduce circular reference
    input.a.circular = input.b;
    const output = addAllowUnsecureRecursively(input);
    expect(output.a.allowUnsecure).toBe(false);
    expect(output.a.circular).toBe(output.b);
    // Original remains unchanged
    expect(input.a.allowUnsecure).toBeUndefined();
  });
});
