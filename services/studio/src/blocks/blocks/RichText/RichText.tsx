import { HTMLAttributes, ReactNode } from 'react';
import parser, { DOMNode, domToReact, Text } from 'html-react-parser';
import { marked } from 'marked';
import mustache from 'mustache';
import { Tooltip } from 'antd';
import sanitizeHtml from 'sanitize-html';
import domSerializer from 'dom-serializer';
import Link, { LinkProps } from 'next/link';
import useLocalizedText from '@/utils/useLocalizedText';
import { keysKebabToCamel } from '@/utils/kebabToCamel';
import BlockLoader from '@/components/PageContent/BlockLoader';
import { BaseBlockConfig } from '../types';
import SyntaxHighlighter from './SyntaxHighlighter';
import Script from './Script';
import {
  addAllowUnsecureRecursively,
  cleanHTMLToMarkdown,
  isElement,
  parseConfig,
  tryJSONParse,
} from './utils';
import extensions from './markedExtensions';
export interface RichTextConfig extends BaseBlockConfig {
  content: string | Prismeai.LocalizedText;
  values?: Record<string, ReactNode>;
  markdown?: boolean;
  allowUnsecure?: boolean;
  // @deprecated
  allowScripts?: boolean;
  container?: string;
  tag?: string;
}

const NoScript = () => {
  console.warn(
    'Your RichText Block contains HTML <script> tags but you did not allowed their execution by setting `allowUnsecure`.',
  );
  return null;
};

marked.use({ extensions: extensions });

export const RichText = ({
  children,
  className = '',
  values = {},
  // @deprecated
  allowScripts = false,
  allowUnsecure = allowScripts || false,
  markdown = allowUnsecure ? false : true,
  // @deprecated
  container = 'div',
  // @deprecated
  tag = container,
  // @deprecated
  sectionId = '',
}: Omit<RichTextConfig, 'content'> & {
  children: RichTextConfig['content'];
} & HTMLAttributes<HTMLDivElement>) => {
  const { localize } = useLocalizedText();

  if (!children) return null;

  const options = {
    replace(domNode: DOMNode) {
      if (!isElement(domNode)) return domNode;
      switch (domNode.name) {
        case 'script':
          if (!allowUnsecure) return <NoScript />;
          return (
            <Script {...(domNode.attribs as unknown as HTMLScriptElement)}>
              {(domNode.children as any)[0]?.data}
            </Script>
          );
        case 'a': {
          const { style, ...attribs } = domNode.attribs as unknown as LinkProps & {
            style?: string;
          };
          void style;
          if (attribs.href) {
            return <Link {...attribs}>{domToReact(domNode.children as DOMNode[], options)}</Link>;
          } else {
            return domNode;
          }
        }
        case 'code': {
          const [, language] = (domNode.attribs?.class || '').match(/^language-(.+)$/) || [];
          const children = domNode.children[0] as Text;
          if (language && children.type === 'text' && children.data) {
            return <SyntaxHighlighter language={language}>{children.data}</SyntaxHighlighter>;
          }
          return domNode;
        }
        case 'pr-block': {
          const { slug, key, ...config } = domNode.attribs;
          const additionalConfig = tryJSONParse(
            domSerializer(domNode.childNodes, { decodeEntities: false }),
          ); // Support config as a stringified JSON between the tags

          let blockConfig = {
            ...parseConfig(keysKebabToCamel(config)),
            ...additionalConfig,
            parentClassName: className,
          };
          if (!allowUnsecure) {
            blockConfig = addAllowUnsecureRecursively(blockConfig); // Prevents child RichText to allowUnsecure
          }
          return <BlockLoader key={key} name={slug} config={blockConfig} />;
        }
        case 'pr-tooltip':
          return (
            <Tooltip {...domNode.attribs}>
              {domToReact(domNode.children as DOMNode[], options)}
            </Tooltip>
          );
        default: {
          // This fixes crashes when html is invalid and tags contains invalid
          // characters

          if (!domNode.name.match(/^[a-z\-]+$/)) {
            domNode.name = domNode.name.replace(/[^a-zA-Z0-9\-]/g, '');
          }

          // This fix crash when the lib try to set child for an unclosed node which can't accept children
          if (domNode.name == 'img' && domNode.children.length > 0) {
            domNode.name = 'div';
          }

          return domNode;
        }
      }
    },
  };
  const text = localize(children) || '';

  let toRender =
    markdown && typeof text === 'string'
      ? marked(cleanHTMLToMarkdown(text), { async: false })
      : typeof text === 'object'
        ? text
        : `${text}`;

  if (!allowUnsecure) {
    toRender = sanitizeHtml(toRender, {
      allowedTags: sanitizeHtml.defaults.allowedTags.concat(['img', 'pr-block']),
      allowedAttributes: {
        ...sanitizeHtml.defaults.allowedAttributes,
        'pr-block': ['slug', 'key'],
      },
    }).replaceAll('{', '&#123;');
  }

  const Container = typeof tag === 'string' ? (tag as keyof JSX.IntrinsicElements) : null;
  let child: string | JSX.Element | JSX.Element[] = '';

  try {
    child = parser(mustache.render(toRender, values).replaceAll('\\{', '{'), options);
  } catch {}

  if (!Container) return <>{child}</>;

  return (
    <Container className={`pr-block-rich-text ${className}`} id={sectionId}>
      {child}
    </Container>
  );
};

export default RichText;
