import { DOMNode, Element } from 'html-react-parser';

export function isElement(domNode: DOMNode): domNode is Element {
  return !!(domNode as Element).name;
}

export function addAllowUnsecureRecursively(data: any, visited = new WeakMap()): any {
  // If the data is a primitive or null, return it as is.
  if (data === null || typeof data !== 'object') {
    return data;
  }

  // If we've already cloned this object (to handle circular references), return it.
  if (visited.has(data)) {
    return visited.get(data);
  }

  let cloned: any;
  if (Array.isArray(data)) {
    cloned = [];
    visited.set(data, cloned);
    for (const item of data) {
      cloned.push(addAllowUnsecureRecursively(item, visited));
    }
  } else {
    cloned = {};
    visited.set(data, cloned);
    for (const key in data) {
      if (Object.prototype.hasOwnProperty.call(data, key)) {
        cloned[key] = addAllowUnsecureRecursively(data[key], visited);
      }
    }
    // Apply the condition: if slug is "RichText", add allowUnsecure property.
    if (data.slug === 'RichText') {
      cloned.allowUnsecure = false;
    }
  }

  return cloned;
}

export function parseConfig(config: Record<string, any>): typeof config {
  return Object.entries(config).reduce((prev, [k, v]) => {
    let value = v;
    try {
      value = JSON.parse(v);
    } catch {}
    return {
      ...prev,
      [k]: value,
    };
  }, {});
}

export function tryJSONParse(value: string) {
  try {
    return JSON.parse(value);
  } catch {
    console.warn(`JSON parsing failed`, { value });
    return {};
  }
}

/** Sometimes markdown is malformed, but it can be fixed here */
export function cleanHTMLToMarkdown(html: string) {
  // The case here is when the content is a code block directly after the <p> tag
  if (html.match(/^<p>```/)) return html.replace(/^<p>```/, '<p>\n\n```');
  return html;
}
