import { useBlock } from '@/blocks/Provider';
import useLocalizedText from '@/utils/useLocalizedText';
import { BaseBlock } from '../BaseBlock';
import RichText, { RichTextConfig } from './RichText';
import { defaultStyles } from './defaultStyles';

export default function RichTextInContext() {
  const { config: { content = '', ...config } = {} } = useBlock<RichTextConfig>();
  const { localize } = useLocalizedText();
  return (
    <BaseBlock defaultStyles={defaultStyles}>
      <RichText {...config}>{localize(content)}</RichText>
    </BaseBlock>
  );
}
