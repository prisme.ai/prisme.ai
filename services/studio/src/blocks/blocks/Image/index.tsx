import { useBlock } from '@/blocks/Provider';
import ImageComp, { ImageConfig } from './Image';
import { BaseBlock } from '../BaseBlock';

export const ImageInContext = () => {
  const { config } = useBlock<ImageConfig>();
  return (
    <BaseBlock>
      <ImageComp {...config} />
    </BaseBlock>
  );
};

export default ImageInContext;
