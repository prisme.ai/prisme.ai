import useLocalizedText from '@/utils/useLocalizedText';
import { BaseBlockConfig } from '../types';
import RichText from '../RichText/RichText';

export interface ImageConfig extends BaseBlockConfig {
  caption?: Prismeai.LocalizedText;
  src?: string;
  alt?: string;
}

export const Image = ({ className = '', caption, src = '', alt, sectionId = '' }: ImageConfig) => {
  const { localize } = useLocalizedText();
  return (
    <figure className={`pr-block-image ${className}`} id={sectionId}>
      <img className="pr-block-image__image" src={src} loading="lazy" alt={localize(alt)} />
      {caption && (
        <figcaption className="pr-block-image__caption">
          <RichText>{localize(caption)}</RichText>
        </figcaption>
      )}
    </figure>
  );
};

export default Image;
