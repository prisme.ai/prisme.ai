import { useBlock } from '@/blocks/Provider';
import { BaseBlock } from '../BaseBlock';
import Tooltip, { TooltipProps } from './Tooltip';

export const TooltipInContext = () => {
  const { config } = useBlock<TooltipProps>();
  return (
    <BaseBlock>
      <Tooltip {...config} />
    </BaseBlock>
  );
};

export default TooltipInContext;
