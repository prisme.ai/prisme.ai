import { BlockContent } from '@/blocks/utils/types';
import { BaseBlockConfig } from '../types';
import { TooltipPlacement } from 'antd/es/tooltip';
import { Tooltip } from 'antd';
import GenericBlock from '@/blocks/utils/GenericBlock';

export interface TooltipProps extends BaseBlockConfig {
  content: BlockContent;
  label: BlockContent;
  placement?: TooltipPlacement;
}

export const TooltipBlock = ({ className, content, label, placement = 'left' }: TooltipProps) => {
  return (
    <Tooltip
      rootClassName={`pr-block-tooltip-overlay ${className}`}
      title={<GenericBlock content={label} />}
      placement={placement}
      trigger={['hover']}
    >
      <span>
        <GenericBlock content={content} />
      </span>
    </Tooltip>
  );
};

export default TooltipBlock;
