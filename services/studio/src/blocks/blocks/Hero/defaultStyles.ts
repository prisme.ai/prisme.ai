const css = String.raw;
export const defaultStyles = css`
  .pr-block-hero__container {
    display: flex;
    flex-direction: row;
    padding: 1rem;
    justify-content: space-between;

    max-width: 70%;
    margin: 0 auto;
    padding: 5rem 0;
    font-size: 1.6rem;
  }
  @media (max-width: 500px) {
    .pr-block-hero__container {
      flex-direction: column;
    }
  }
  .pr-block-hero__text {
    flex: 1;
  }
  .pr-block-hero__title {
    font-size: 3rem;
    font-weight: 700;
  }
  .pr-block-hero__lead {
    margin-top: 1rem;
  }
  .pr-block-hero__img {
    max-width: 24rem;
    width: 100%;
  }
`;
