import { useBlock } from '@/blocks/Provider';
import { Hero, HeroConfig } from './Hero';
import { BaseBlock } from '../BaseBlock';
import { defaultStyles } from './defaultStyles';

export const HeroInContext = () => {
  const { config } = useBlock<HeroConfig>();

  return (
    <BaseBlock defaultStyles={defaultStyles}>
      <Hero {...config} />
    </BaseBlock>
  );
};
HeroInContext.styles = defaultStyles;

export default HeroInContext;
