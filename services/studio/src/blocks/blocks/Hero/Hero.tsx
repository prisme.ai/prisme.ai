import useLocalizedText from '@/utils/useLocalizedText';
import { BaseBlockConfig } from '../types';
import GenericBlock, { GenericBlockContent } from '@/blocks/utils/GenericBlock';

export interface HeroConfig extends BaseBlockConfig {
  title: Prismeai.LocalizedText;
  lead: Prismeai.LocalizedText;
  content?: GenericBlockContent;
  img: string;
  backgroundColor: string;
  level: 1 | 2 | 3 | 4 | 5 | 6;
}

type HeroProps = HeroConfig;

export const Hero = ({
  className,
  title,
  lead,
  content,
  img,
  backgroundColor,
  level = 2,
}: HeroProps) => {
  const { localize } = useLocalizedText();
  const H = `h${level}` as keyof JSX.IntrinsicElements;
  return (
    <div className={`pr-block-hero ${className}`} style={{ backgroundColor }}>
      <div className="pr-block-hero__container">
        <div className="pr-block-hero__text">
          <H className="pr-block-hero__title">{localize(title)}</H>
          {lead && <p className="pr-block-hero__lead">{localize(lead)}</p>}
          {content && <GenericBlock content={content} />}
        </div>
        {img && (
          <div className="pr-block-hero__img">
            <img src={img} role="img" alt="" />
          </div>
        )}
      </div>
    </div>
  );
};

export default Hero;
