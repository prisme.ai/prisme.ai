import { useBlock } from '@/blocks/Provider';
import Toast, { ToastProps } from './Toast';
import { BaseBlock } from '../BaseBlock';
import { defaultStyles } from './defaultStyles';

export const ToastInContext = () => {
  const { config } = useBlock<ToastProps>();
  return (
    <BaseBlock defaultStyles={defaultStyles}>
      <Toast {...config} />
    </BaseBlock>
  );
};
ToastInContext.styles = defaultStyles;

export default ToastInContext;
