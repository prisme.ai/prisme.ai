import { useBlock } from '@/blocks/Provider';
import { BaseBlockConfig } from '../types';
import { useCallback, useEffect, useState } from 'react';
import useLocalizedText from '@/utils/useLocalizedText';
import { createPortal } from 'react-dom';

const DEFAULT_DURATION = 5;

export interface ToastProps extends BaseBlockConfig {
  toastOn: string;
}

interface ToastMessage {
  type?: 'success' | 'error' | 'warning' | 'loading';
  content: string | Prismeai.LocalizedText;
  duration?: number;
  closed?: boolean;
  key: string;
}

let count = 0;

export const Toast = ({ className, toastOn }: ToastProps) => {
  const { events } = useBlock();
  const [messages, setMessages] = useState<ToastMessage[]>([]);
  const { localize } = useLocalizedText();

  const close = useCallback(
    (message: ToastMessage) => () => {
      const closedMessage = { ...message, closed: true };
      setMessages((prev) => prev.map((item) => (item === message ? closedMessage : item)));
      setTimeout(() => setMessages((prev) => prev.filter((item) => item !== closedMessage)), 200);
    },
    [],
  );

  useEffect(() => {
    if (!events) return;
    const off = events.on(toastOn, ({ payload }) => {
      if (!payload.content) return;
      const { type, content, duration = DEFAULT_DURATION } = payload as ToastMessage;
      const message = { type, content, key: `${++count}` };
      setMessages((prev) => [...prev, message]);
      setTimeout(close(message), duration * 1000);
    });
    return () => off();
  }, [toastOn, events, close]);

  return createPortal(
    <div className={`pr-block-toast ${className || ''}`}>
      {messages.map((message) => (
        <div
          key={message.key}
          className={`pr-toast-message pr-toast-message--${message.type} ${
            message.closed ? 'pr-toast-message--closed' : ''
          }`}
        >
          {localize(message.content)}
          <button className="pr-toast-message-close" type="button" onClick={close(message)}>
            x
          </button>
        </div>
      ))}
    </div>,
    document.body,
  );
};

export default Toast;
