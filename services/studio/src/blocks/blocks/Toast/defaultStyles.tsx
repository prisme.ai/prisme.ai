const css = String.raw;
export const defaultStyles = css`
  :block {
    position: fixed;
    bottom: 1rem;
    right: 1rem;
    display: flex;
    flex-direction: column;
    align-items: flex-end;
    z-index: 99999999;
  }
  .pr-toast-message {
    position: relative;
    background: white;
    border-radius: 1rem;
    padding: 1rem 2rem;
    margin: 0.5rem 0;
    border: 1px solid;
    opacity: 1;
    transition:
      opacity 0.2s ease-in,
      transform 0.2s ease-in;
  }
  .pr-toast-message--closed {
    opacity: 0;
    transform: translate3d(100%, 0, 0);
  }
  .pr-toast-message--success {
    background: #52a943;
    color: white;
  }
  .pr-toast-message--error {
    background: #f6685f;
    color: white;
  }
  .pr-toast-message--warning {
    background: #ba8f34;
    color: white;
  }
  .pr-toast-message-close {
    position: absolute;
    top: 0;
    right: 0.5rem;
    font-weight: bold;
  }
`;
