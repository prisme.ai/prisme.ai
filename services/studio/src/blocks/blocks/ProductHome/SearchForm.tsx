import { Input, Tooltip } from 'antd';
import { FormEvent, useCallback, useEffect, useRef, useState } from 'react';
import { useBlock } from '../../Provider';
import useLocalizedText from '../../../utils/useLocalizedText';
import { Icon } from '../Icon/Icon';
import { EventSpec } from '../../utils/types';
import { translations } from './translations';
import { ProductHomeProps } from './types';

export const SearchForm = ({
  placeholder = translations.placeholder,
  onSubmit,
  onChange,
  submitLabel = translations.submitLabel,
  initialValue = '',
}: NonNullable<ProductHomeProps['search']>) => {
  const { localize } = useLocalizedText();
  const { events } = useBlock();
  const [search, setSearch] = useState(initialValue);
  const prevInitialValue = useRef(initialValue);
  const pristine = useRef(true);

  useEffect(() => {
    if (prevInitialValue.current === initialValue) return;
    prevInitialValue.current = initialValue;
    pristine.current = true;
    setSearch(initialValue);
  }, [initialValue]);

  const emitEvent = useCallback(
    (eventSpec: EventSpec) => (search: string) => {
      const { event, payload = {} } =
        typeof eventSpec === 'string' ? { event: eventSpec } : eventSpec;
      events?.emit(event, {
        ...payload,
        search,
      });
    },
    [events],
  );

  const debounced = useRef<NodeJS.Timeout>();
  useEffect(() => {
    if (!onChange) return;
    if (debounced.current) clearTimeout(debounced.current);
    if (pristine.current && initialValue !== search) {
      pristine.current = false;
    }
    if (pristine.current) return;
    debounced.current = setTimeout(() => {
      emitEvent(onChange)(search);
    }, 500);
  }, [emitEvent, search, initialValue, onChange]);

  const onSubmitHandler = useCallback(
    (e: FormEvent) => {
      e.preventDefault();
      if (!onSubmit) return;
      emitEvent(onSubmit)(search);
    },
    [emitEvent, search, onSubmit],
  );

  const onResetHandler = useCallback(
    (e: FormEvent) => {
      e.preventDefault();
      if (!onSubmit) return;
      setSearch('');
      emitEvent(onSubmit)('');
    },
    [emitEvent, onSubmit],
  );

  return (
    <form className="product-home-search-form" onSubmit={onSubmitHandler}>
      <Input
        type="search"
        placeholder={localize(placeholder)}
        className="product-home-search-form-input"
        onChange={({ target: { value } }) => setSearch(value)}
        value={search}
        prefix={
          <div
            className={`product-home-search-form-reset-ctn ${search ? 'product-home-search-form-reset-ctn--visible' : ''}`}
          >
            <Tooltip title={localize(translations.reset)} placement="right">
              <button
                type="button"
                onClick={onResetHandler}
                className="product-home-search-form-reset-btn"
              >
                <Icon icon="cross" />
              </button>
            </Tooltip>
          </div>
        }
        suffix={
          onSubmit && (
            <div className="product-home-search-form-submit-ctn">
              <Tooltip title={localize(submitLabel)} placement="left">
                <button type="submit" className="product-home-search-form-submit-btn">
                  <Icon icon="search" />
                </button>
              </Tooltip>
            </div>
          )
        }
      />
      {}
    </form>
  );
};

export default SearchForm;
