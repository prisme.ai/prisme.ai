import useLocalizedText from '../../../utils/useLocalizedText';
import { BasicAction } from '../../utils/BasicAction';
import { ProductHomeProps } from './types';

export const Tags = ({ tags = [] }: Pick<ProductHomeProps, 'tags'>) => {
  const { localize } = useLocalizedText();

  return (
    <div className="product-home-tags">
      {tags.map(({ selected, type, value, payload, text }, key) => (
        <BasicAction
          key={`${localize(text)}-${key}`}
          type={type}
          value={value}
          payload={payload}
          className={`product-home-tags-item ${selected ? 'product-home-tags-item--selected' : ''}`}
        >
          {localize(text)}
        </BasicAction>
      ))}
    </div>
  );
};

export default Tags;
