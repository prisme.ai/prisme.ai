import { useBlock } from '../../Provider';
import { ProductHomeProps } from './types';
import { BaseBlock } from '../BaseBlock';
import ProductHome from './ProductHome';
import { defaultStyles } from './defaultStyles';

export default function ProductHomeInContext() {
  const { config } = useBlock<ProductHomeProps>();
  return (
    <BaseBlock defaultStyles={defaultStyles}>
      <ProductHome {...config} />
    </BaseBlock>
  );
}
ProductHomeInContext.styles = defaultStyles;
ProductHomeInContext.Component = ProductHome;
