const css = String.raw;
export const defaultStyles = css`
  :block {
    /* Tags */
    --block-product-home-tag-border-color: #c6c6c7;
    --block-product-home-tag-border: 1px solid var(--block-product-home-tag-border-color);
    --block-product-home-tag-active-border-color: #0c2256;
    --block-product-home-tag-active-border: 1px solid
      var(--block-product-home-tag-active-border-color);
    --block-product-home-tag-hover-border-color: var(--pr-accent-color);
    --block-product-home-tag-hover-border: 1px solid
      var(--block-product-home-tag-hover-border-color);
    --block-product-home-tag-border-radius: 0.9rem;
    /* Items */
    --block-product-home-item-border-color: #c6c6c7;
    --block-product-home-item-border: var(--block-product-home-item-border-color) 1px solid;
    --block-product-home-item-hover-border-color: var(--pr-accent-color);
    --block-product-home-item-hover-border: var(--block-product-home-item-hover-border-color) 1px
      solid;
    --block-product-home-item-active-border-color: var(--pr-accent-color);
    --block-product-home-item-active-border: var(--block-product-home-item-active-border-color) 1px
      solid;
    --block-product-home-item-bg-color: var(--pr-main-bg-color);
    --block-product-home-item-create-color: var(--pr-main-bg-color);
  }
  :root[data-color-scheme='dark'] :block {
    /* Tags */
    --block-product-home-tag-border-color: #969696;
    --block-product-home-tag-active-border-color: #f6f6f7;
    /* Items */
    --block-product-home-item-border-color: #969696;
    --block-product-home-item-active-border-color: #f6f6f7;
  }
  :block {
    display: flex;
    flex-direction: column;
    min-height: 100%;
    flex: 1;
    color: var(--pr-main-text);
    z-index: 1;
    display: flex;
    padding-top: 1.7rem;
  }
  @media (min-width: 76rem) {
    :block {
      padding-top: calc(50vh - 200px);
      overflow: visible;
    }
  }
  .product-home-ctn {
    margin: 0 auto;
    max-width: calc(76rem - 0.9rem * 2);
    padding: 0 0.9rem;
    width: 100%;
  }
  .product-home-heading {
    display: flex;
    flex-direction: column;
    margin-bottom: 1.2rem;
  }
  .product-home-heading-title-ctn {
    display: flex;
    flex-direction: row;
    align-items: center;
    color: var(--pr-accent-color);
  }
  .product-home-heading-icon {
    display: flex;
    flex: none;
    height: 3rem;
    max-width: 3rem;
    color: var(--pr-accent-color);
    margin-right: 0.9rem;
  }
  .product-home-heading-title {
    display: flex;
    font-size: 2.4rem;
    color: var(--pr-accent-color);
    font-weight: 700;
  }
  .product-home-heading-description {
    display: flex;
    font-size: 1.5rem;
    font-weight: 500;
  }
  .product-home-search-form {
    margin-bottom: 2.2rem;
  }
  .product-home-search-form-submit-ctn,
  .product-home-search-form-reset-ctn {
    display: flex;
  }
  .product-home-search-form-submit-btn {
    color: var(--pr-accent-color);
  }
  .product-home-search-form-reset-btn {
    color: var(--pr-accent-color);
  }
  .product-home-search-form-reset-ctn {
    display: none;
  }
  .product-home-search-form-reset-ctn--visible {
    display: flex;
  }
  .product-home-title {
    font-size: 1.5rem;
    font-weight: 700;
    margin-bottom: 1.2rem;
  }
  .product-home-tags {
    display: flex;
    flex-wrap: nowrap;
    overflow: auto;
    margin: 0 -0.9rem;
    padding: 0.6rem 0.9rem 2.2rem 0.9rem;
    flex: none;
  }
  .product-home-tags-item {
    margin-right: 1rem;
    border-radius: var(--block-product-home-tag-border-radius);
    padding: 0.6rem 0.9rem;
    white-space: nowrap;
  }
  .product-home-tags-item:not(.product-home-tags-item--selected) {
    border: var(--block-product-home-tag-border);
  }
  .product-home-tags-item--selected {
    font-weight: 700;
    border: var(--block-product-home-tag-active-border);
  }
  .product-home-tags-item:hover {
    border: var(--block-product-home-tag-hover-border);
  }
  .product-home-tags-item:focus {
    box-shadow:
      0 0 0px 3px var(--pr-main-bg-color),
      0 0 0px 5px var(--block-product-home-tag-hover-border-color);
  }
  .product-home-list {
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    padding: 0.45rem;
    margin: -0.9rem;
  }
  .product-home-list-item {
    display: flex;
    flex-direction: row;
    align-items: center;
    border: var(--block-product-home-item-border);
    background: var(--block-product-home-item-bg-color);
    border-radius: 0.9rem;
    padding: 0.9rem;
    margin: 0.45rem;
  }
  .product-home-list-item:hover {
    border: var(--block-product-home-item-hover-border);
  }
  .product-home-list-item--selected,
  .product-home-list-item:active {
    border: var(--block-product-home-item-active-border);
  }
  .product-home-list-item:focus {
    box-shadow:
      0 0 0px 3px var(--pr-main-bg-color),
      0 0 0px 5px var(--block-product-home-tag-hover-border-color);
  }
  .product-home-list-item {
    width: calc(100% / 4 - 0.9rem);
  }
  @media (min-width: 701px) and (max-width: 1100px) {
    .product-home-list-item {
      width: calc(100% / 3 - 0.9rem);
    }
  }
  @media (min-width: 501px) and (max-width: 700px) {
    .product-home-list-item {
      width: calc(100% / 2 - 0.9rem);
    }
  }
  @media (max-width: 500px) {
    .product-home-list-item {
      width: calc(100% - 0.9rem);
    }
  }
  .product-home-list-item-icon {
    display: flex;
    width: 3.2rem;
    height: 3.2rem;
    margin-right: 1.8rem;
    border-radius: 0.9rem;
    color: var(--pr-accent-color);
  }
  .product-home-list-item-icon svg {
    width: 100%;
    height: 100%;
  }
  .product-home-list-texts {
    display: flex;
    flex-direction: column;
    justify-content: center;
    color: var(--pr-main-text);
    height: 100%;
    min-height: 3.2rem;
    font-size: 1.2rem;
    white-space: nowrap;
    width: 1px;
    flex: 1;
  }
  @media (max-width: 768px) {
    .product-home-list-texts {
      align-items: flex-start;
    }
  }
  .product-home-list-name {
    flex-direction: column;
    font-weight: 500;
    text-overflow: ellipsis;
    overflow: hidden;
    display: block;
  }
  .product-home-list-description {
    font-weight: 400;
    text-overflow: ellipsis;
    overflow: hidden;
    display: block;
  }
  .product-home-list-create-btn .product-home-list-item-icon {
    background-color: var(--pr-accent-color);
    color: var(--block-product-home-item-create-color);
    padding: 0.4rem;
  }
`;
