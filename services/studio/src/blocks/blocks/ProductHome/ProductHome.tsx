import { BaseBlockConfig } from '../types';
import List from './List';
import SearchForm from './SearchForm';
import Tags from './Tags';
import { ProductHomeProps } from './types';
import GenericBlock from '../../utils/GenericBlock';
import { BasicAction } from '../../utils/BasicAction';
import Heading from './Heading';
import useLocalizedText from '../../../utils/useLocalizedText';

export const ProductHome = ({
  className,
  search,
  tags,
  heading,
  title,
  list,
  additionalButtons,
  before,
  after,
}: ProductHomeProps & BaseBlockConfig) => {
  const { localize } = useLocalizedText();
  return (
    <div className={`product-home ${className}`}>
      <div className="product-home-ctn">
        {heading && <Heading heading={heading} />}
        {search?.active !== false && <SearchForm {...search} />}
        {(additionalButtons || []).map(({ type, value, payload, text }, key) => (
          <BasicAction
            key={key}
            type={type}
            value={value}
            payload={payload}
            className={`product-home-additional-btn`}
          >
            <>
              {text && (
                <GenericBlock
                  content={text}
                  className="product-home-additional-btn-text"
                  ifString={({ content, className }) => (
                    <span className={className}>{content}</span>
                  )}
                />
              )}
            </>
          </BasicAction>
        ))}
        {before && <GenericBlock content={before} />}
        {title && <div className="product-home-title">{localize(title)}</div>}
        {Array.isArray(tags) && tags?.length && <Tags tags={tags} />}
        {list && <List {...list} />}
        {after && <GenericBlock content={after} />}
      </div>
    </div>
  );
};

export default ProductHome;
