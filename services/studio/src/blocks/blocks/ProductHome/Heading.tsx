import useLocalizedText from '../../../utils/useLocalizedText';
import { Icon } from '../Icon';
import GenericBlock from '../../utils/GenericBlock';
import { isBlock, isRenderProp } from '../../utils/getContentType';
import { ProductHomeProps } from './types';

export const Heading = ({ heading }: { heading: NonNullable<ProductHomeProps['heading']> }) => {
  const { localize } = useLocalizedText();

  if (isRenderProp(heading) || typeof heading === 'string') return <>{heading}</>;

  if (isBlock(heading)) return <GenericBlock content={heading} />;

  const { icon, title, description } = heading;

  return (
    <div className="product-home-heading">
      <div className="product-home-heading-title-ctn">
        {icon && <Icon icon={icon} className="product-home-heading-icon" />}
        {title && <h1 className="product-home-heading-title">{localize(title)}</h1>}
      </div>
      {description && (
        <div className="product-home-heading-description">{localize(description)}</div>
      )}
    </div>
  );
};
export default Heading;
