import { ActionConfig } from '../Action/Action';
import { IconProps } from '../Icon/types';
import { BlockContent, EventSpec } from '../../utils/types';

export interface Filters {
  search?: string;
  tag?: string | null;
}

export interface ProductHomeProps {
  title?: Prismeai.LocalizedText;
  heading?:
    | BlockContent
    | {
        icon?: IconProps['icon'];
        title?: Prismeai.LocalizedText;
        description?: Prismeai.LocalizedText;
      };
  search?: {
    active?: false;
    placeholder?: Prismeai.LocalizedText;
    onSubmit?: EventSpec;
    onChange?: EventSpec;
    submitLabel?: Prismeai.LocalizedText;
    initialValue?: string;
  };
  tags?: (Omit<ActionConfig, 'text'> & {
    text: Prismeai.LocalizedText;
    selected?: boolean;
  })[];
  additionalButtons?: (Pick<ActionConfig, 'type' | 'value' | 'payload'> & {
    text: string;
  })[];
  list?: {
    items: (Omit<ActionConfig, 'text'> & {
      id?: string;
      text: Prismeai.LocalizedText;
      description?: string;
      icon?: string;
      before?: BlockContent;
      after?: BlockContent;
    })[];
    hasMore?: EventSpec & {
      enabled?: boolean;
    };
    create?: ActionConfig & {
      disabled?: boolean;
      icon?: string;
      before?: BlockContent;
      after?: BlockContent;
    };
  };
  before?: BlockContent;
  after?: BlockContent;
}
