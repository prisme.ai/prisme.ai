import useLocalizedText from '../../../utils/useLocalizedText';
import { Icon } from '../Icon/Icon';
import { BasicAction } from '../../utils/BasicAction';
import { translations } from './translations';
import { ProductHomeProps } from './types';
import GenericBlock from '@/blocks/utils/GenericBlock';

const ItemButton = ({
  className = '',
  text,
  type,
  value,
  description,
  payload,
  icon,
  before,
  after,
}: NonNullable<ProductHomeProps['list']>['items'][number] & {
  className?: string;
}) => {
  const { localize } = useLocalizedText();
  return (
    <BasicAction
      type={type}
      value={value}
      payload={payload}
      className={`product-home-list-item ${className}`}
    >
      <>
        {before && <GenericBlock content={before} />}
        {icon && (
          <div className="product-home-list-item-icon">
            <Icon icon={icon} />
          </div>
        )}
        <div className="product-home-list-texts">
          <div className="product-home-list-name">{localize(text)}</div>
          {description && (
            <div className="product-home-list-description">{localize(description)}</div>
          )}
        </div>
        {after && <GenericBlock content={after} />}
      </>
    </BasicAction>
  );
};

export const List = ({ items = [], create, hasMore }: NonNullable<ProductHomeProps['list']>) => {
  const { localize } = useLocalizedText();
  return (
    <div className="product-home-list">
      {create && !create.disabled && (
        <ItemButton
          {...create}
          text={localize(create?.text as Prismeai.LocalizedText) || translations.create}
          icon="plus"
          className="product-home-list-create-btn"
        />
      )}
      {Array.isArray(items) &&
        items.map((item, key) => <ItemButton key={item.id || key} {...item} />)}
      {hasMore?.enabled && (
        <ItemButton
          type="event"
          value={typeof hasMore === 'string' ? hasMore : hasMore.event}
          payload={typeof hasMore === 'string' ? undefined : hasMore.payload}
          text={translations.loadMore}
          className="product-home-list-load-more"
        />
      )}
    </div>
  );
};
export default List;
