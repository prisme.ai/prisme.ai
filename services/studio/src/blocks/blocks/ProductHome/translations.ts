export const translations = {
  placeholder: {
    fr: 'Chercher un agent',
    en: 'Search en agent',
  },
  submitLabel: {
    fr: 'Chercher',
    en: 'Search',
  },
  reset: {
    fr: 'Vider le champs',
    en: 'Reset field',
  },
  create: {
    fr: 'Créer un agent',
    en: 'Create agent',
  },
  loadMore: {
    fr: "Charger plus d'agents…",
    en: 'Load more agents…',
  },
};
