const css = String.raw;
export const defaultStyles = css`
  /* Layout */
  :block {
    display: flex;
    flex-direction: column;
    width: 100%;
  }
  .layout-grid {
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: center;
  }
  .layout-column {
    display: flex;
    flex-wrap: wrap;
    flex-direction: column;
    align-items: center;
  }
  .layout-carousel {
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
    overflow: auto;
    scrollbar-width: none;
    scroll-snap-type: x mandatory;
    scroll-snap-align: start;
    padding-bottom: 1.5rem;
  }

  .cards-container {
    position: relative;
    width: 100%;
    padding-top: 0;
  }

  .block-cards__title-container {
    padding-top: 1rem;
  }
  .block-title {
    color: var(--color-text, var(--pr-accent-color));
    font-size: 1.333rem;
    font-weight: 600;
    line-height: 1.4;
    margin: 0 0 0.5em 0;
    word-wrap: break-word;
  }
  @media (min-width: 768px) {
    .block-title {
      font-size: 2.5rem;
    }
  }

  .button-link__image-container,
  .button-event__image-container {
    display: flex;
    margin-right: 0.5rem;
  }
  .truncated {
    position: relative;
  }
  .truncated-ellipsis {
    position: absolute;
    bottom: 0;
    right: 0;
  }

  .card-content-outer__button {
    display: flex;
    flex: 1;
    flex-direction: row;
    background-color: #e6efff;
    font-size: 10px;
    color: var(--pr-accent-color);
    padding: 1rem;
    border-radius: 0%.3rem;
    text-align: left;
  }

  /* Carousel */
  .block-cards__scroll {
    color: white;
  }
  .block-cards__scroll__left,
  .block-cards__scroll__right {
    display: flex;
    justify-content: center;
    border-radius: 100%;
    background-color: var(--pr-accent-color-contrast);
    box-shadow:
      0 0 transparent,
      0 0 transparent,
      0 10px 15px -3px rgba(0, 0, 0, 0.1),
      0 4px 6px -2px rgba(0, 0, 0, 0.05);
    height: 2rem;
    width: 2rem;
    position: absolute;
    top: 40%;
    transition: all 0.1s ease-in;
  }
  .block-cards__scroll__left {
    left: 1.5rem;
    opacity: 0;
  }
  .block-cards__scroll__right {
    right: 1.5rem;
  }
  .block-cards__scroll__left--visible {
    opacity: 1;
  }
  .block-cards__scroll__left__button,
  .block-cards__scroll__right__button {
    align-items: center;
    justify-content: center;
    outline: none;
  }
  .block-cards__scroll__left__button__icon,
  .block-cards__scroll__right__button__icon {
    border-radius: 50%;
    transform: rotate(90deg);
    color: var(--pr-accent-color);
  }
  .block-cards__scroll__right__button__icon {
    transform: rotate(-90deg);
  }

  :block:not(.variant-classic) .card-container {
    box-shadow: 0 0 0.3rem rgba(0, 0, 0, 0.2);
    transition:
      transform 0.1s ease-in,
      box-shadow 0.1s ease-in;
  }
  :block:not(.variant-classic) .card-container:hover {
    transform: translate3d(0, 0.25rem, 0) scale3d(1.05, 1.05, 1.05);
    box-shadow:
      0 0 transparent,
      0 0 transparent,
      0 10px 15px -3px rgba(0, 0, 0, 0.1),
      0 4px 6px -2px rgba(0, 0, 0, 0.05);
  }

  .cards-container__card-container {
    flex: none;
  }

  /* Actions cards */
  :block.variant-actions .card-container {
    display: flex;
    flex-direction: column;
    margin: 0.625rem;
    padding: 0.625rem 0;
    min-width: 19.563rem;
    max-width: 19.563rem;
    overflow: hidden;
    border-radius: 0.938rem;
  }
  :block.variant-actions .cards-container__card-container--with-header {
    height: 23.188rem;
    padding: 0;
  }
  :block.variant-actions .cards-container__card-container-header {
    display: flex;
    flex-direction: row;
    margin: 1.25rem;
  }
  :block.variant-actions .card-container__card-cover {
    display: flex;
    flex-direction: row;
    height: 5.5rem;
    width: 5.25rem;
    min-width: 5.25rem;
    margin-bottom: 0;
    border-radius: 0.625rem;
    background-position: center center !important;
    background-size: cover !important;
  }
  :block.variant-actions .card-container__card-content {
    display: flex;
    flex-direction: column;
    justify-content: center;
    height: 5.5rem;
    margin-left: 1.25rem;
  }
  :block.variant-actions .card-container__card-content-title {
    font-size: 1.25rem;
    font-weight: bold;
  }
  :block.variant-actions .card-container__card-content-text {
    font-size: 0.875;
  }
  :block.variant-actions .card-container__card-action {
    display: flex;
    flex: 1;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  }
  :block.variant-actions .card__card-action-outer {
    display: flex;
    width: 100%;
    padding: 0.625rem 1.25rem;
    font-weight: 500;
  }
  :block.variant-actions .card__card-action-inner {
    display: flex;
    height: 3.125rem;
    flex: 1;
    align-items: center;
    justify-content: center;
    border-radius: 0.625rem;
  }
  :block.variant-actions .card__card-action-content {
    display: flex;
    flex: 1;
    align-items: center;
    justify-content: center;
    height: 3.125rem;
    border-radius: 0.625rem;
    text-align: center;
    font-size: 0.875rem;
    font-weight: 500;
  }

  /* Article cards */
  :block.variant-article .card-container {
    display: flex;
    flex-direction: column;
    height: 23.188rem;
    min-width: 19.563rem;
    max-width: 19.563rem;
    margin: 0.625rem;
    border-radius: 0.938rem;
    border: 1px solid rgba(0, 0, 0, 0.2);
    background-color: white;
  }
  :block.variant-article .card-container__card-cover {
    display: flex;
    flex-direction: row;
    height: 10.25rem;
    min-height: 10.25rem;
    margin: 0.438rem;
    margin-bottom: 0;
    border-radius: 0.625rem;
    background-position: center center !important;
    background-size: cover !important;
  }
  :block.variant-article .card-cover__card-tag {
    margin: 0.625rem;
    max-height: 2rem;
    overflow: hidden;
    border-radius: 0.938rem;
    padding: 0.45rem 0.93rem;
    font-size: 0.9rem;
    line-height: 1.7;
    color: white;
    background-color: rgba(0, 0, 0, 0.75);
  }
  :block.variant-article .cards-container__card-content {
    margin: 1.25rem;
    margin-bottom: 0.7rem;
    overflow: hidden;
  }
  :block.variant-article .cards-container__card-content-subtitle {
    max-height: 1rem;
    overflow: hidden;
    font-size: 0.75rem;
    line-height: 1.2;
  }
  :block.variant-article .cards-container__card-content-title {
    max-height: 3rem;
    overflow: hidden;
    font-size: 1.25rem;
    font-weight: bold;
    line-height: 1.2;
  }
  :block.variant-article .cards-container__card-content-text {
    overflow: hidden;
    padding-bottom: 0.5rem;
    font-size: 0.875rem;
    line-height: 1.2;
  }

  /* Classic cards */
  :block.variant-classic .cards-container__card-container {
    display: flex;
    flex-direction: column;
    min-height: 23rem;
    width: 15rem;
    margin-top: 1.5rem;
    margin-bottom: 1.5rem;
    padding-left: 10px;
  }
  :block.variant-classic .card-container__card {
    display: flex;
    flex-direction: column;
    flex: 1;
    position: relative;
    margin-left: 1rem;
    margin-right: 1rem;
    border-radius: 20px;
  }
  :block.variant-classic .card__card-image {
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    height: 10rem;
    border-radius: 15px;
    background-size: contain;
    background-position: top;
    background-repeat: no-repeat;
    padding: -1px;
  }
  :block.variant-classic .card__card-content {
    display: flex;
    flex-direction: column;
    position: relative;
    margin-top: 103px;
    min-height: 203px;
    border-radius: 15px;
    border: 1px solid #e5e7eb;
    background-color: white;
    padding: 1.375rem;
    font-size: 0.93rem;
    box-shadow: 0 0 0.3rem rgba(0, 0, 0, 0.2);
    transition:
      transform 0.1s ease-in,
      box-shadow 0.1s ease-in;
  }
  :block.variant-classic .card__card-content:hover {
    transform: translate3d(0, 0.25rem, 0) scale3d(1.05, 1.05, 1.05);
    box-shadow:
      0 0 transparent,
      0 0 transparent,
      0 10px 15px -3px rgba(0, 0, 0, 0.1),
      0 4px 6px -2px rgba(0, 0, 0, 0.05);
  }
  :block.variant-classic .card__card-title {
    color: var(--pr-accent-color);
    text-align: center;
    font-size: 1rem;
    font-weight: bold;
  }
  :block.variant-classic .card__card-description {
    margin: 0.5rem 0;
    text-align: center;
    font-size: 10px;
    color: #737373;
  }
  :block.variant-classic .card__card-content-outer {
    display: flex;
    margin-bottom: 1rem;
    font-size: 0.625rem;
  }
  :block.variant-classic .card-content-outer__accordion {
    display: flex;
    flex-direction: row;
    align-items: center;
  }
  .accordion__container {
    display: flex;
    flex: 1;
    flex-direction: column;
  }
  .accordion__container .container__button {
    display: flex;
    flex: 1;
    align-items: center;
    justify-content: space-between;
    padding: 0.25rem;
  }
  .accordion__container .button__icon {
    transition: transform 0.1s ease-in;
  }
  .accordion__container .button__icon--visible {
    transform: rotate(-180deg);
  }
  .accordion__container .container__accordion-content-container {
    padding: 0.5rem;
  }

  /* Short cards */
  :block.variant-short .card-container {
    display: flex;
    flex-direction: column;
    height: 10rem;
    width: 19.563rem;
    margin: 0.625rem;
    overflow: hidden;
    border-radius: 0.938rem;
    border: 1px solid rgba(0, 0, 0, 0.2);
  }
  :block.variant-short .card-container--transparent-black {
    background-color: transparent;
    color: black;
    border: 1px solid rgba(0, 0, 0, 0.2);
  }
  :block.variant-short .card-container--transparent-white {
    background-color: transparent;
    color: white;
    border: 1px solid rgba(255, 255, 255, 0.2);
  }
  :block.variant-short .card-container--white {
    background-color: white;
    color: black;
  }
  :block.variant-short .card-container--black {
    background-color: black;
    color: white;
    border: 1px solid black;
  }
  :block.variant-short .cards-container__card-content {
    margin: 1.25rem;
  }
  :block.variant-short .cards-container__card-content-subtitle {
    max-height: 1rem;
    overflow: hidden;
    font-size: 0.75rem;
    line-height: 1.2;
  }
  :block.variant-short .cards-container__card-content-title {
    max-height: 2rem;
    overflow: hidden;
    font-size: 0.875rem;
    font-weight: bold;
    line-height: 1.2;
  }
  :block.variant-short .cards-container__card-content-text {
    max-height: 4.2rem;
    overflow: hidden;
    font-size: 0.875rem;
    line-height: 1.2;
  }

  /* Square cards */
  :block.variant-square .card-container {
    display: flex;
    flex-direction: column;
    height: 12.625rem;
    width: 12.625rem;
    max-width: 12.625rem;
    margin: 0.625rem;
  }
  :block.variant-square .cards-container__card-content {
    display: flex;
    flex-direction: column;
    flex: 1;
    justify-content: flex-end;
    text-align: left;
    overflow: hidden;
    border-radius: 0.625rem;
    background-position: center center !important;
    background-size: cover !important;
    padding: 0.938rem;
    color: white;
  }
  :block.variant-square .cards-container__card-content-title {
    margin-bottom: 0.5rem;
    max-height: 4.6rem;
    overflow: hidden;
    font-size: 1.25rem;
    font-weight: bold;
    line-height: 1.2;
  }
  :block.variant-square .cards-container__card-content-text {
    max-height: 4.2rem;
    overflow: hidden;
    font-size: 0.875rem;
    line-height: 1.2;
  }
`;
