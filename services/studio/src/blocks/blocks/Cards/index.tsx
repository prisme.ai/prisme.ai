import { useBlock } from '@/blocks/Provider';
import { CardsConfig } from './types';
import { BaseBlock } from '../BaseBlock';
import { defaultStyles } from './defaultStyles';
import { Cards } from './Cards';

export default function CardsInContext() {
  const { config } = useBlock<CardsConfig>();
  return (
    <BaseBlock defaultStyles={defaultStyles}>
      <Cards {...config} />
    </BaseBlock>
  );
}
