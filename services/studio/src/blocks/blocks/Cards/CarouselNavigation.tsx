import useTranslations from '@/i18n/useTranslations';
import { Tooltip } from 'antd';
import React, { RefObject, useEffect, useState } from 'react';
import ArrowIcon from '@/svgs/arrow-down.svg';

interface CarouselNavigationProps {
  scrollable: boolean | null;
  scroll: (step: number) => () => void;
  children: React.ReactElement;
  scrollableRef: RefObject<HTMLDivElement>;
}

const CarouselNavigation = ({
  scrollable,
  scroll,
  children,
  scrollableRef,
}: CarouselNavigationProps) => {
  const t = useTranslations('pages');
  const [displayedArrows, setDisplayedArrows] = useState({
    left: false,
    right: true,
  });

  useEffect(() => {
    scrollableRef?.current?.addEventListener('scroll', () => {
      if (!scrollableRef?.current?.scrollLeft) return;
      if (scrollableRef.current.scrollLeft > 100) {
        setDisplayedArrows({ ...displayedArrows, left: true });
      } else {
        setDisplayedArrows({ ...displayedArrows, left: false });
      }
    });
  }, [displayedArrows, scrollableRef]);

  return (
    <>
      {children}
      {scrollable && (
        <div className="block-cards__scroll">
          <div
            className={`block-cards__scroll__left ${displayedArrows.left ? 'block-cards__scroll__left--visible' : ''}`}
          >
            <Tooltip title={t('blocks.cards.prev')} placement="right">
              <button onClick={scroll(-1)} className="block-cards__scroll__left__button">
                <ArrowIcon className="block-cards__scroll__left__button__icon" />
              </button>
            </Tooltip>
          </div>
          <div className="block-cards__scroll__right">
            <Tooltip title={t('blocks.cards.next')} placement="left">
              <button
                onClick={scroll(1)}
                className={`block-cards__scroll__right__button flex items-center justify-center outline-none`}
              >
                <ArrowIcon className="block-cards__scroll__right__button__icon" />
              </button>
            </Tooltip>
          </div>
        </div>
      )}
    </>
  );
};

export default CarouselNavigation;
