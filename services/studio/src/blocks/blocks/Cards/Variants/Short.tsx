import { CardProps, CardShort } from '../types';
import CarouselNavigation from '../CarouselNavigation';
import Truncated from '../Truncated';
import useLocalizedText from '@/utils/useLocalizedText';
import BlockTitle from '../Legacy/BlockTitle';
import ActionOrLink from '../Legacy/ActionOrLink';
import RichText from '../../RichText/RichText';

type ShortProps = CardProps<CardShort[]>;

const Short = ({
  title,
  styles,
  cards,
  container,
  canScroll,
  scroll,
  className = ',',
}: ShortProps) => {
  const { localize } = useLocalizedText();

  return (
    <div className={`block-cards variant-short ${className}`}>
      {title && <BlockTitle value={localize(title)} />}
      <div className={`block-cards__cards-container cards-container`}>
        <CarouselNavigation scroll={scroll} scrollable={canScroll} scrollableRef={container}>
          <div
            ref={container}
            className={`cards-container__cards-container cards-container ${styles.container}`}
          >
            {(cards as CardShort[]).map(
              ({ title, subtitle, description, backgroundColor, action }, index) => (
                <ActionOrLink action={action} key={index}>
                  <div
                    className="cards-container__card-container card-container bg-black"
                    style={{ backgroundColor }}
                  >
                    <div className="cards-container__card-content">
                      {subtitle && (
                        <Truncated
                          className="cards-container__card-content-subtitle"
                          text={localize(subtitle)}
                        >
                          {(truncatedText) => truncatedText}
                        </Truncated>
                      )}
                      <Truncated
                        className="cards-container__card-content-title"
                        text={localize(title)}
                      >
                        {(truncatedText) => truncatedText}
                      </Truncated>
                      <Truncated
                        className="cards-container__card-content-text"
                        text={localize(description)}
                        ellipsis="…"
                      >
                        {(truncatedText) => <RichText>{truncatedText || ''}</RichText>}
                      </Truncated>
                    </div>
                  </div>
                </ActionOrLink>
              ),
            )}
          </div>
        </CarouselNavigation>
      </div>
    </div>
  );
};

export default Short;
