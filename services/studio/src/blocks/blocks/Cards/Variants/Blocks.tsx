import { CardBlocks, CardProps } from '../types';
import CarouselNavigation from '../CarouselNavigation';
import useLocalizedText from '@/utils/useLocalizedText';
import BlockTitle from '../Legacy/BlockTitle';
import BlocksList from '../../BlocksList/BlocksList';

type BlocksProps = CardProps<CardBlocks[]>;

const Blocks = ({
  title,
  styles,
  cards,
  container,
  canScroll,
  scroll,
  className = '',
}: BlocksProps) => {
  const { localize } = useLocalizedText();

  return (
    <div className={`pr-block-cards block-cards variant-blocks ${className}`}>
      {title && <BlockTitle value={localize(title)} />}
      <div className={`block-cards__cards-container cards-container relative w-full !pt-0`}>
        <CarouselNavigation scroll={scroll} scrollable={canScroll} scrollableRef={container}>
          <div
            ref={container}
            className={`cards-container__cards-container cards-container ${styles.container}`}
          >
            {(cards as CardBlocks[]).map(({ content }, index) => (
              <div
                key={index}
                className="pr-block-cards__card cards-container__card-container card-container"
              >
                <BlocksList {...content} />
              </div>
            ))}
          </div>
        </CarouselNavigation>
      </div>
    </div>
  );
};

export default Blocks;
