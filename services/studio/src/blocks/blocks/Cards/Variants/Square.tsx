import { useCallback } from 'react';
import { CardProps, CardSquare } from '../types';
import CarouselNavigation from '../CarouselNavigation';
import Truncated from '../Truncated';
import useLocalizedText from '@/utils/useLocalizedText';
import BlockTitle from '../Legacy/BlockTitle';
import ActionOrLink from '../Legacy/ActionOrLink';
import RichText from '../../RichText/RichText';

type SquareProps = CardProps<CardSquare[]>;

const Square = ({
  title,
  styles,
  cards,
  container,
  canScroll,
  scroll,
  className = '',
}: SquareProps) => {
  const { localize } = useLocalizedText();

  const getCoverStyle = useCallback(
    (index: number) => {
      const { cover } = cards[index] || {};
      return {
        background: `linear-gradient(rgba(81, 81, 81, 0), rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), ${
          cover ? `url("${cover}")` : 'rgb(140, 140, 140)'
        }`,
      };
    },
    [cards],
  );

  return (
    <div className={`block-cards variant-square ${className}`}>
      {title && <BlockTitle value={localize(title)} />}
      <div className="block-cards__cards-container cards-container">
        <CarouselNavigation scroll={scroll} scrollable={canScroll} scrollableRef={container}>
          <div
            ref={container}
            className={`cards-container__cards-container cards-container ${styles.container}`}
          >
            {(cards as CardSquare[]).map(({ title, description, action }, index) => (
              <ActionOrLink action={action} key={index}>
                <div className="cards-container__card-container card-container">
                  <div className="cards-container__card-content" style={getCoverStyle(index)}>
                    <Truncated
                      className="cards-container__card-content-title"
                      text={localize(title)}
                    >
                      {(truncatedText) => truncatedText}
                    </Truncated>
                    <Truncated
                      className="cards-container__card-content-text"
                      text={localize(description)}
                      ellipsis="…"
                    >
                      {(truncatedText) => <RichText>{truncatedText || ''}</RichText>}
                    </Truncated>
                  </div>
                </div>
              </ActionOrLink>
            ))}
          </div>
        </CarouselNavigation>
      </div>
    </div>
  );
};

export default Square;
