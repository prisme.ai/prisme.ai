import { CardAction, CardProps } from '../types';
import CarouselNavigation from '../CarouselNavigation';
import CardButton from '../CardButton';
import useLocalizedText from '@/utils/useLocalizedText';
import BlockTitle from '../Legacy/BlockTitle';
import { truncate } from '@/utils/strings';

type ActionsProps = CardProps<CardAction[]>;

const getBgStyles = (bgColor: CardAction['backgroundColor']) => {
  switch (bgColor) {
    case 'white':
    case 'transparent-white':
      return {
        color: 'white',
        backgroundColor: 'rgba(255,255,255,0.1)',
      };
    case 'black':
    case 'transparent-black':
    default:
      return {
        color: 'black',
        backgroundColor: 'rgba(0,0,0,0.05)',
      };
  }
};

const getBtnStyles = (bgColor: CardAction['backgroundColor']) => {
  switch (bgColor) {
    case 'white':
    case 'transparent-white':
      return {
        color: 'white',
        border: '1px solid white',
        backgroundColor: 'transparent',
      };
    case 'black':
    case 'transparent-black':
    default:
      return {
        color: 'black',
      };
  }
};

const Actions = ({
  title,
  styles,
  cards,
  container,
  getCoverStyle,
  canScroll,
  scroll,
  className = '',
}: ActionsProps) => {
  const { localize } = useLocalizedText();

  return (
    <div className={`block-cards variant-actions ${className}`}>
      {title && <BlockTitle value={localize(title)} />}
      <div className="block-cards__cards-container cards-container">
        <CarouselNavigation scroll={scroll} scrollable={canScroll} scrollableRef={container}>
          <div
            ref={container}
            className={`cards-container__cards-container cards-container ${styles.container}`}
          >
            {(cards as CardAction[]).map(
              ({ title, description, content = [], backgroundColor }, index) => {
                const withHeader = localize(title) || localize(description);
                return (
                  <div
                    key={index}
                    className={`cards-container__card-container card-container ${withHeader ? 'cards-container__card-container--with-header' : ''}`}
                    style={getBgStyles(backgroundColor)}
                  >
                    {withHeader && (
                      <div className="cards-container__card-container-header">
                        {getCoverStyle(index) && (
                          <div
                            className="card-container__card-cover card-cover"
                            style={getCoverStyle(index)}
                          />
                        )}
                        <div className="card-container__card-content">
                          <div className="card-container__card-content-title">
                            {truncate(localize(title), getCoverStyle(index) ? 14 : 25)}
                          </div>
                          <div className="card-container__card-content-text">
                            {truncate(localize(description), getCoverStyle(index) ? 43 : 80)}
                          </div>
                        </div>
                      </div>
                    )}
                    <div className="card-container__card-action">
                      {content &&
                        Array.isArray(content) &&
                        content.slice(0, 3).map((item, index) => (
                          <div key={index} className="card__card-action-outer card-action-outer">
                            <div className="card__card-action-inner">
                              <CardButton
                                type="button"
                                url={item.type === 'url' ? item.value : undefined}
                                event={item.type === 'event' ? item.value : undefined}
                                payload={item.payload}
                                value={truncate(localize(item.text), 60)}
                                popup={item.popup}
                                className="card__card-action-content"
                                style={getBtnStyles(backgroundColor)}
                              />
                            </div>
                          </div>
                        ))}
                    </div>
                  </div>
                );
              },
            )}
          </div>
        </CarouselNavigation>
      </div>
    </div>
  );
};

export default Actions;
