import { CardClassic, CardProps } from '../types';
import CardButton from '../CardButton';
import Accordion from '../Accordion';
import CarouselNavigation from '../CarouselNavigation';
import useLocalizedText from '@/utils/useLocalizedText';
import BlockTitle from '../Legacy/BlockTitle';
import RichText from '../../RichText/RichText';

type ClassicProps = CardProps<CardClassic[]>;

const Classic = ({
  title,
  styles,
  cards,
  container,
  getCoverStyle,
  canScroll,
  scroll,
  className = '',
}: ClassicProps) => {
  const { localize } = useLocalizedText();

  return (
    <div className={`block-cards variant-classic ${className}`}>
      {title && <BlockTitle value={localize(title)} />}
      <div className="block-cards__cards-container cards-container">
        <CarouselNavigation scroll={scroll} scrollable={canScroll} scrollableRef={container}>
          <div
            ref={container}
            className={`cards-container__cards-container cards-container ${styles.container}`}
          >
            {(cards as CardClassic[]).map(({ title, description, content = [] }, index) => (
              <div key={index} className="cards-container__card-container card-container">
                <div className="card-container__card card">
                  <div className="card__card-image card-image" style={getCoverStyle(index)} />
                  <div className="card__card-content card-content">
                    <div className="card__card-title card-content">{localize(title)}</div>
                    <div className="card__card-description card-description">
                      <RichText>{localize(description)}</RichText>
                    </div>
                    {content &&
                      Array.isArray(content) &&
                      content.map((item, index) => (
                        <div key={index} className="card__card-content-outer card-content-outer">
                          {item.type === 'text' && (
                            <div className="card-content-outer__content-text content-text">
                              <div
                                className="content-text__content content"
                                dangerouslySetInnerHTML={{
                                  __html: localize(item.value),
                                }}
                              />
                            </div>
                          )}
                          {item.type === 'button' && <CardButton {...item} />}
                          {item.type === 'accordion' && (
                            <div
                              className={`${`card-content-outer__accordion accordion flex max-w-full flex-1 rounded border-[1px] border-neutral-200 p-2`}`}
                            >
                              <Accordion
                                title={
                                  <div className="accordion__accordion-title accordion-title">
                                    {item.icon && (
                                      <img
                                        src={item.icon}
                                        alt={localize(item.title)}
                                        width={16}
                                        height={16}
                                        className={`accordion-title__image image`}
                                      />
                                    )}{' '}
                                    {localize(item.title)}
                                  </div>
                                }
                                collapsed={item.collapsed}
                              >
                                <div className={'accordion-content-container__content content'}>
                                  <RichText>{localize(item.content)}</RichText>
                                </div>
                              </Accordion>
                            </div>
                          )}
                        </div>
                      ))}
                  </div>
                </div>
              </div>
            ))}
          </div>
        </CarouselNavigation>
      </div>
    </div>
  );
};

export default Classic;
