import { CardArticle, CardProps } from '../types';
import CarouselNavigation from '../CarouselNavigation';
import Truncated from '../Truncated';
import useLocalizedText from '@/utils/useLocalizedText';
import BlockTitle from '../Legacy/BlockTitle';
import ActionOrLink from '../Legacy/ActionOrLink';
import RichText from '../../RichText/RichText';

type ArticleProps = CardProps<CardArticle[]>;

const Article = ({
  title,
  styles,
  cards,
  container,
  getCoverStyle,
  canScroll,
  scroll,
  className = '',
}: ArticleProps) => {
  const { localize } = useLocalizedText();

  return (
    <div className={`block-cards variant-article ${className}`}>
      {title && <BlockTitle value={localize(title)} />}
      <div className="block-cards__cards-container cards-container">
        <CarouselNavigation scroll={scroll} scrollable={canScroll} scrollableRef={container}>
          <div
            ref={container}
            className={`cards-container__cards-container cards-container ${styles.container}`}
          >
            {(cards as CardArticle[]).map(
              ({ title, subtitle, tag, description, action }, index) => (
                <ActionOrLink action={action} key={index}>
                  <div key={index} className="cards-container__card-container card-container">
                    {getCoverStyle(index) && (
                      <div
                        className="card-container__card-cover card-cover"
                        style={getCoverStyle(index)}
                      >
                        {tag && (
                          <Truncated className="card-cover__card-tag card-tag" text={localize(tag)}>
                            {(truncatedText) => truncatedText}
                          </Truncated>
                        )}
                      </div>
                    )}
                    <div className="cards-container__card-content">
                      {subtitle && (
                        <Truncated
                          className="cards-container__card-content-subtitle"
                          text={localize(subtitle)}
                        >
                          {(truncatedText) => truncatedText}
                        </Truncated>
                      )}
                      <Truncated
                        className="cards-container__card-content-title"
                        text={localize(title)}
                      >
                        {(truncatedText) => truncatedText}
                      </Truncated>
                      <Truncated
                        className="cards-container__card-content-text"
                        text={localize(description)}
                        ellipsis="…"
                      >
                        {(truncatedText) => <RichText>{truncatedText || ''}</RichText>}
                      </Truncated>
                    </div>
                  </div>
                </ActionOrLink>
              ),
            )}
          </div>
        </CarouselNavigation>
      </div>
    </div>
  );
};

export default Article;
