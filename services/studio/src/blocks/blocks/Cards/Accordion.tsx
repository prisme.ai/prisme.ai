import { ReactNode, useState } from 'react';
import StretchContent from '@/components/StretchContent';
import ChevronIcon from '@/svgs/chevron.svg';

const Accordion = ({
  title,
  children,
  collapsed = true,
}: {
  title: ReactNode;
  collapsed: boolean | undefined;
  children: ReactNode;
}) => {
  const [visible, setVisible] = useState(!collapsed);

  return (
    <div className="accordion__container container">
      <button className="container__button button" onClick={() => setVisible(!visible)}>
        {title}
        <ChevronIcon className={`button__icon icon ${visible ? 'button__icon--visible' : ''}`} />
      </button>
      <StretchContent visible={visible}>
        <div className="container__accordion-content-container accordion-content-container">
          {children}
        </div>
      </StretchContent>
    </div>
  );
};

export default Accordion;
