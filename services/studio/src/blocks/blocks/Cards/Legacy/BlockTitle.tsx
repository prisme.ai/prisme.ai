import { HTMLAttributes } from 'react';

interface BlockTitleProps {
  value: string;
}
const Title = ({ level, ...props }: HTMLAttributes<HTMLHeadingElement> & { level: number }) => {
  switch (level) {
    case 1:
      return <h1 {...props} />;
    case 2:
      return <h2 {...props} />;
    case 3:
      return <h3 {...props} />;
    case 4:
      return <h4 {...props} />;
    case 5:
      return <h5 {...props} />;
    case 6:
      return <h6 {...props} />;
  }
  return null;
};

const BlockTitle = ({ value }: BlockTitleProps) => {
  return (
    <div className="block-cards__title-container title-container">
      <Title level={4} className="block-title">
        {value}
      </Title>
    </div>
  );
};

export default BlockTitle;
