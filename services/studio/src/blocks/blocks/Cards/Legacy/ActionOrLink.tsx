import { useBlock } from '@/blocks/Provider';
import { useBlocks } from '@/blocks/Provider/blocksContext';
import { ReactElement } from 'react';

interface ActionOrLinkProps {
  children: ReactElement;
  action?: Action;
}

export interface Action {
  type: 'event' | 'url';
  value: string;
  popup?: boolean;
  payload?: any;
}

const ActionOrLink = ({ action, children }: ActionOrLinkProps) => {
  const {
    components: { Link },
  } = useBlocks();
  const { events } = useBlock();

  if (!action || !action.value) return children;

  switch (action.type) {
    case 'event':
      return (
        <button
          onClick={() => {
            const urlSearchParams = new URLSearchParams(window.location.search);
            const query = Object.fromEntries(urlSearchParams.entries());

            events?.emit(action.value, {
              ...(Object.keys(query).length > 0 ? { query } : {}),
              ...action.payload,
            });
          }}
        >
          {children}
        </button>
      );
    case 'url':
      return (
        <Link href={action.value} target={action.popup ? '_blank' : undefined}>
          {children}
        </Link>
      );
    default:
      return children;
  }
};

export default ActionOrLink;
