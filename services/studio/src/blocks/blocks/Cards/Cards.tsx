import { useCallback, useEffect, useLayoutEffect, useMemo, useRef, useState } from 'react';
import {
  CardAction,
  CardArticle,
  CardBlocks,
  CardClassic,
  Cards as TCards,
  CardsConfig,
  CardShort,
  CardSquare,
} from './types';
import Classic from './Variants/Classic';
import Square from './Variants/Square';
import Article from './Variants/Article';
import Short from './Variants/Short';
import Actions from './Variants/Actions';
import Blocks from './Variants/Blocks';

const cardsIsShort = (cards: TCards, variant: CardsConfig['variant']): cards is CardShort[] =>
  variant === 'short';
const cardsIsBlocks = (cards: TCards, variant: CardsConfig['variant']): cards is CardBlocks[] =>
  variant === 'blocks';

const getContainerStyle = (type: CardsConfig['layout']['type']) => {
  switch (type) {
    case 'grid':
      return {
        container: 'layout-grid',
      };
    case 'column':
      return {
        container: 'layout-column',
      };
    case 'carousel':
    default:
      return {
        container: 'layout-carousel',
      };
  }
};

const EMPTY_ARRAY: CardsConfig['cards'] = [];

export const Cards = ({
  cards = EMPTY_ARRAY,
  layout,
  variant,
  className,
  ...config
}: CardsConfig) => {
  const [canScroll, setCanScroll] = useState<boolean | null>(false);

  const container = useRef<HTMLDivElement>(null);

  const scroll = useCallback(
    (step: number) => () => {
      if (!container.current) return;
      const { current } = container;
      const currentLeft = current.scrollLeft;
      const cardWidth = current.firstChild
        ? (current.firstChild as Element).getBoundingClientRect().width
        : 1;

      setTimeout(() => {
        if (currentLeft === current.scrollLeft) {
          const from = current.scrollLeft;
          current.scrollBy({
            left: step * cardWidth,
            top: 0,
            behavior: 'smooth',
          });
          setTimeout(() => {
            if (from !== current.scrollLeft) return;
            // Go back to the start or the the end
            current.scrollTo({
              left: from === 0 ? current.scrollWidth : 0,
              top: 0,
              behavior: 'smooth',
            });
          }, 50);
        }
      }, 50);
    },
    [],
  );

  useEffect(() => {
    const listener = (e: KeyboardEvent) => {
      switch (e.key) {
        case 'ArrowLeft':
          return scroll(-1)();
        case 'ArrowRight':
          return scroll(1)();
      }
    };
    document.body.addEventListener('keydown', listener);

    return () => {
      document.body.removeEventListener('keydown', listener);
    };
  }, [scroll]);

  useEffect(() => {
    if (
      !layout ||
      !layout.type ||
      layout.type !== 'carousel' ||
      !layout.autoScroll ||
      !container.current
    )
      return;
    const { current } = container;
    const interval = setInterval(() => {
      scroll(current.getBoundingClientRect().width)();
    }, 5000);

    return () => {
      clearInterval(interval);
    };
  });

  useLayoutEffect(() => {
    const { current: containerEl } = container;
    if (!containerEl) return;

    const resizeObserver = new ResizeObserver(() => {
      setCanScroll(
        (!layout || !layout.type || layout.type === 'carousel') &&
          containerEl &&
          containerEl.scrollWidth > containerEl.getBoundingClientRect().width,
      );
    });
    resizeObserver.observe(containerEl);

    return () => {
      resizeObserver.unobserve(containerEl);
    };
  }, [layout, cards]);

  const styles = useMemo(() => {
    const { type = 'carousel' } = layout || {};
    return getContainerStyle(type);
  }, [layout]);

  const getCoverStyle = useCallback(
    (index: number) => {
      if (cardsIsShort(cards, variant) || cardsIsBlocks(cards, variant)) return null;

      const cover = (cards[index] || {}).cover;

      if (!cover) return null;

      return {
        background: `url("${cover}")`,
      };
    },
    [cards, variant],
  );

  const cardsProps = {
    styles,
    container,
    canScroll,
    scroll,
    getCoverStyle,
    layout,
    variant,
    className,
    ...config,
  };

  const filteredCards = useMemo(() => (cards as []).filter(Boolean), [cards]);

  switch (variant) {
    case 'square':
      return <Square {...cardsProps} cards={(filteredCards as CardSquare[]) || []} />;
    case 'article':
      return <Article {...cardsProps} cards={(filteredCards as CardArticle[]) || []} />;
    case 'short':
      return <Short {...cardsProps} cards={(filteredCards as CardShort[]) || []} />;
    case 'actions':
      return <Actions {...cardsProps} cards={(filteredCards as CardAction[]) || []} />;
    case 'blocks':
      return <Blocks {...cardsProps} cards={(filteredCards as CardBlocks[]) || []} />;
    case 'classic':
    default:
      return <Classic {...cardsProps} cards={(filteredCards as CardClassic[]) || []} />;
  }
};
