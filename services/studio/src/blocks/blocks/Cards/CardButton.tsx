import { useBlock } from '../../Provider';
import { useBlocks } from '../../Provider/blocksContext';
import { CardButtonType } from './types';
import useLocalizedText from '@/utils/useLocalizedText';
import RichText from '../RichText/RichText';

const CardButton = ({
  url,
  popup,
  event,
  icon,
  value,
  payload,
  className = '',
  style,
}: CardButtonType) => {
  const { localize } = useLocalizedText();
  const { events } = useBlock();

  const {
    components: { Link },
  } = useBlocks();
  if (url) {
    return (
      <Link
        href={url}
        className={`card-content-outer__button card-content-outer__button-link button-link ${className}`}
        style={style}
        target={popup ? '_blank' : undefined}
      >
        <div className="button-link__image-container image-container">
          {icon && (
            <img
              className="image-container__image image image-container__image--custom image--custom"
              src={icon}
              alt={localize(value)}
              height={16}
              width={16}
            />
          )}
        </div>
        <RichText>{localize(value)}</RichText>
      </Link>
    );
  }
  if (event) {
    return (
      <button
        type="button"
        className={`card-content-outer__button block-cards__button-event button-event ${className}`}
        style={style}
        onClick={() => events?.emit(event, payload)}
      >
        <div className="button-event__image-container image-container">
          {icon && (
            <img
              className="image-container__image--custom image--custom"
              src={icon}
              alt={localize(value)}
              height={16}
              width={16}
            />
          )}
        </div>
        <RichText>{localize(value)}</RichText>
      </button>
    );
  }
  return <RichText>{localize(value)}</RichText>;
};

export default CardButton;
