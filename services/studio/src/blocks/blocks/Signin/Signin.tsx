import { useCallback } from 'react';
import { useBlocks } from '../../Provider/blocksContext';
import { BaseBlockConfig } from '../types';
import useLocalizedText from '@/utils/useLocalizedText';

export interface SigninConfig extends BaseBlockConfig {
  label: string | Prismeai.LocalizedText;
  up?: true;
  redirect?: string;
}

export const Signin = ({
  label = 'Signin',
  up,
  redirect,
  className,
  sectionId = '',
}: SigninConfig) => {
  const {
    utils: { auth: { getSigninUrl, getSignupUrl } = {} },
  } = useBlocks();

  const { localize } = useLocalizedText();
  const signin = useCallback(async () => {
    if (!getSigninUrl) return;
    const url = await getSigninUrl({ redirect });
    window.location.assign(url);
  }, [getSigninUrl, redirect]);

  const signup = useCallback(async () => {
    if (!getSignupUrl) return;
    const url = await getSignupUrl({ redirect });
    window.location.assign(url);
  }, [getSignupUrl, redirect]);

  return (
    <button onClick={up ? signup : signin} className={className} id={sectionId}>
      {localize(label)}
    </button>
  );
};

export default Signin;
