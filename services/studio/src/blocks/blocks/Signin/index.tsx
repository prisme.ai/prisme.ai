import { useBlock } from '@/blocks/Provider';
import Signin, { SigninConfig } from './Signin';
import { BaseBlock } from '../BaseBlock';
import { defaultStyles } from './defaultStyles';

export const SigninInContext = () => {
  const { config } = useBlock<SigninConfig>();
  return (
    <BaseBlock defaultStyles={defaultStyles}>
      <Signin {...config} />
    </BaseBlock>
  );
};

export default SigninInContext;
