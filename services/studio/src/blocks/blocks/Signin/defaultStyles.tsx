const css = String.raw;
export const defaultStyles = css`
  :block {
    display: flex;
    border-radius: 1rem;
    align-self: center;
    padding: 0.5rem 1rem;
    background: var(--color-accent);
    color: var(--color-accent-contrast);
  }
`;
