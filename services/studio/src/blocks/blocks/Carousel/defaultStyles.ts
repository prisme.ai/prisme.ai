const css = String.raw;
export const defaultStyles = css`
  :block {
    position: relative;
  }

  .pr-block-carousel__content {
    display: flex;
    flex-direction: row;
    overflow: auto;
    scroll-snap-type: x mandatory;
  }
  .pr-block-carousel__content > .pr-block-blocks-list__block {
    scroll-snap-align: start;
  }

  .pr-block-carousel__arrows {
    color: white;
    opacity: 0;
    transition: opacity 0.2s ease-in;
  }
  :block:hover .pr-block-carousel__arrows {
    opacity: 1;
  }

  .pr-block-carousel__arrow {
    position: absolute;
    display: flex;
    justify-content: center;
    align-items: center;
    top: 40%;
    height: 2rem;
    width: 2rem;
    background: black;
    border-radius: 100%;
    box-shadow: 0 0 1rem rgba(0, 0, 0, 0.8);
  }

  .pr-block-carousel__arrow--left {
    left: 1rem;
  }

  .pr-block-carousel__arrow--right {
    right: 1rem;
  }

  .pr-block-carousel__arrow__icon--prev {
    transform: rotate(-90deg);
  }
  .pr-block-carousel__arrow__icon--next {
    transform: rotate(90deg);
  }
  .pr-block-blocks-list > .pr-block-blocks-list__block {
    flex-shrink: 0;
  }
  .pr-block-carousel__indicators {
    display: flex;
    margin-left: 3rem;
  }
  .pr-block-carousel__indicator {
    display: flex;
    width: 1rem;
    height: 1rem;
    background: var(--color-accent);
    border: 4px solid var(--color-accent);
    margin: 0.2rem;
    border-radius: 50%;
    transition: background-color 0.2s ease-in;
  }
  .pr-block-carousel__indicator--current {
    background: var(--color-accent-contrast, white);
  }
`;
