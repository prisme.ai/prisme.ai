import { Tooltip } from 'antd';
import { useCallback, useEffect, useRef, useState } from 'react';
import { useBlocks } from '../../Provider/blocksContext';
import { BaseBlockConfig } from '../types';
import { BlocksListConfig } from '../BlocksList/BlocksList';
import useTranslations from '@/i18n/useTranslations';
import ArrowIcon from '@/svgs/arrow-up.svg';

export interface CarouselConfig extends BaseBlockConfig, BlocksListConfig {
  autoscroll?: {
    active: boolean;
    speed?: number;
  };
  displayIndicators?: boolean;
}

type CarouselProps = CarouselConfig;

function getStepsWidths(container: HTMLElement) {
  return Array.from(container.children).map((el) => (el as HTMLElement).offsetWidth);
}

export default function Carousel({
  className,
  autoscroll: { active: autoscrollIsActive = true, speed: autoscrollSpeed = 5 } = {
    active: true,
  },
  displayIndicators,
  ...props
}: CarouselProps) {
  const {
    utils: { BlockLoader },
  } = useBlocks();
  const container = useRef<HTMLDivElement>(null);
  const t = useTranslations('pages');

  const scroll = useCallback(
    (step: 1 | -1 | 'screen') => () => {
      const { current } = container;
      if (!current) return;
      const child = current.firstChild as HTMLDivElement;
      if (!child) return;
      const currentLeft = child.scrollLeft;

      setTimeout(() => {
        if (currentLeft !== child.scrollLeft) return;

        const from = step === 'screen' ? child.offsetWidth + child.scrollLeft : child.scrollLeft;

        if (from === 0 && step === -1) {
          child.scrollTo({
            left: child.scrollWidth,
            top: 0,
            behavior: 'smooth',
          });
          return;
        }
        const stepsWidths = getStepsWidths(child);
        const currentStep = stepsWidths.reduce<{ width: number; key?: number }>(
          (prev, next, index) => {
            if (prev.key !== undefined) return prev;
            if (prev.width >= from) {
              const width =
                step === 'screen'
                  ? prev.width
                  : step === 1
                    ? prev.width + next
                    : prev.width - stepsWidths[index - 1] || 0;
              return {
                width: width > child.scrollWidth - child.offsetWidth ? 0 : width,
                key: index,
              };
            }
            return {
              width: prev.width + next,
            };
          },
          { width: 0 },
        );

        child.scrollTo({
          left: currentStep.width,
          top: 0,
          behavior: 'smooth',
        });
      }, 50);
    },
    [],
  );

  const indicators = props.blocks?.length || 0;
  const [currentIndicator, setCurrentIndicator] = useState(-1);

  useEffect(() => {
    const scrollingEl = container.current?.querySelector('.pr-block-carousel__content');
    const listener = () => {
      if (!scrollingEl) return;
      const children = Array.from(scrollingEl.children) as HTMLElement[];

      const currentPage = children.reduce((prev, child, k) => {
        if (prev > -1) return prev;
        const { width } = child.getBoundingClientRect();
        const left = child.offsetLeft;
        const right = left + width;

        if (left <= scrollingEl.scrollLeft && right > scrollingEl.scrollLeft) {
          return k;
        }
        return prev;
      }, -1);
      setCurrentIndicator(currentPage);
    };
    scrollingEl?.addEventListener('scroll', listener);
    setTimeout(listener);
    return () => {
      scrollingEl?.removeEventListener('scroll', listener);
    };
  }, []);

  const scrollTo = useCallback((step: number) => {
    const scrollingEl = container.current?.querySelector('.pr-block-carousel__content');
    if (!scrollingEl) return;
    const children = Array.from(scrollingEl.children) as HTMLElement[];
    const left = children.splice(0, step).reduce((prev, child) => prev + child.offsetWidth, 0);
    scrollingEl.scrollTo({
      left,
      top: 0,
      behavior: 'smooth',
    });
  }, []);

  useEffect(() => {
    const listener = (e: KeyboardEvent) => {
      switch (e.key) {
        case 'ArrowLeft':
          return scroll(-1)();
        case 'ArrowRight':
          return scroll(1)();
      }
    };
    document.body.addEventListener('keydown', listener);

    return () => {
      document.body.removeEventListener('keydown', listener);
    };
  }, [scroll]);

  useEffect(() => {
    if (!autoscrollIsActive) return;
    const { current } = container;
    if (!current) return;
    const child = current.firstChild as HTMLDivElement;
    if (!child) return;
    const interval = setInterval(
      () => {
        scroll('screen')();
      },
      (autoscrollSpeed || 1) * 1000,
    );

    return () => {
      clearInterval(interval);
    };
  }, [autoscrollIsActive, autoscrollSpeed, scroll]);

  const [canScroll, setCanScroll] = useState<boolean | null>(false);
  useEffect(() => {
    const t = setInterval(() => {
      const { current } = container;
      if (!current) return;
      const child = current.firstChild as HTMLDivElement;
      if (!child) return;
      setCanScroll(child.scrollWidth > child.getBoundingClientRect().width);
    }, 200);
    return () => {
      clearInterval(t);
    };
  }, []);

  return (
    <div ref={container} className={`pr-block-carousel ${className}`}>
      <BlockLoader
        name="BlocksList"
        config={{ ...props, className: 'pr-block-carousel__content' }}
      />
      {canScroll && (
        <>
          {displayIndicators && (
            <div className="pr-block-carousel__indicators">
              {Array.from(new Array(indicators), (v, k) => k).map((k) => (
                <button
                  key={k}
                  className={`pr-block-carousel__indicator ${
                    currentIndicator === k ? 'pr-block-carousel__indicator--current' : ''
                  }`}
                  aria-label={`display page ${k}`}
                  onClick={() => scrollTo(k)}
                />
              ))}
            </div>
          )}
          <div className="pr-block-carousel__arrows">
            <div className="pr-block-carousel__arrow pr-block-carousel__arrow--left">
              <Tooltip title={t('blocks.cards.prev')} placement="right">
                <button
                  onClick={scroll(-1)}
                  className="pr-block-carousel__arrow__button"
                  aria-label="display previous page"
                >
                  <ArrowIcon className="pr-block-carousel__arrow__icon pr-block-carousel__arrow__icon--prev" />
                </button>
              </Tooltip>
            </div>
            <div className="pr-block-carousel__arrow pr-block-carousel__arrow--right">
              <Tooltip title={t('blocks.cards.next')} placement="left">
                <button
                  onClick={scroll(1)}
                  className="pr-block-carousel__arrow__button"
                  aria-label="display next page"
                >
                  <ArrowIcon className="pr-block-carousel__arrow__icon pr-block-carousel__arrow__icon--next" />
                </button>
              </Tooltip>
            </div>
          </div>
        </>
      )}
    </div>
  );
}
