import { useBlock } from '@/blocks/Provider';
import { BaseBlock } from '../BaseBlock';
import Carousel, { CarouselConfig } from './Carousel';
import { defaultStyles } from './defaultStyles';

export const CarouselInContext = () => {
  const { config } = useBlock<CarouselConfig>();
  return (
    <BaseBlock defaultStyles={defaultStyles}>
      <Carousel {...config} />
    </BaseBlock>
  );
};

CarouselInContext.styles = defaultStyles;

export default CarouselInContext;
