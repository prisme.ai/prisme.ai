import { ReactNode, useEffect, useState } from 'react';
import { BlockContext } from '../../Provider';
import { useBlocks } from '../../Provider/blocksContext';
import useLocalizedText, { isLocalizedObject } from '@/utils/useLocalizedText';
import { BaseBlockConfig } from '../types';
import { BlocksListConfig } from '../BlocksList/BlocksList';
import Action, { ActionConfig } from '../Action/Action';
import GenericBlock from '@/blocks/utils/GenericBlock';

export interface TabsViewConfig extends BaseBlockConfig {
  tabs: ({
    text: ReactNode | BlocksListConfig;
    selectedText?: ReactNode;
    content: BlocksListConfig;
  } & ActionConfig)[];
  direction: 'vertical' | 'horizontal';
  selected?: number;
}

interface TabsViewProps extends TabsViewConfig {
  events: BlockContext['events'];
}

function isAction(
  action: Partial<TabsViewConfig['tabs'][number]>,
): action is Omit<ActionConfig, 'text'> {
  return !!(action.type && action.value);
}

function isBlocksList(text: BlocksListConfig): text is BlocksListConfig {
  return !!(text as BlocksListConfig).blocks;
}

export const TabsView = ({
  tabs = [],
  direction,
  className,
  events,
  selected = 0,
}: TabsViewProps) => {
  const { localize } = useLocalizedText();
  const [currentTab, setCurrentTab] = useState(selected);
  useEffect(() => {
    setCurrentTab(selected);
  }, [selected]);
  const isHorizontal = direction !== 'vertical';
  const {
    utils: { BlockLoader },
  } = useBlocks();
  return (
    <div className={`pr-block-tabs-view ${isHorizontal ? 'flex-col' : 'flex-row'} ${className}`}>
      <div className={`pr-block-tabs-view__tabs ${isHorizontal ? 'flex-row' : 'flex-col'}`}>
        {tabs.map(({ text, selectedText, ...action }, k) => {
          const navigate = () => setCurrentTab(k);
          const currentText = currentTab === k && selectedText ? selectedText : text;

          if (isBlocksList(text as BlocksListConfig)) {
            return (
              <button
                key={k}
                type="button"
                onClick={navigate}
                className={`pr-block-tabs-view__tab ${
                  currentTab === k ? 'pr-block-tabs-view__tab--active' : ''
                }`}
              >
                <BlockLoader name="BlocksList" config={text} />
              </button>
            );
          }
          if (isAction(action)) {
            return (
              <Action
                key={k}
                text={currentText}
                {...action}
                events={events}
                onClick={navigate}
                className={`pr-block-tabs-view__tab ${
                  currentTab === k ? 'pr-block-tabs-view__tab--active' : ''
                }`}
              />
            );
          }
          return (
            <button
              key={k}
              type="button"
              onClick={navigate}
              className={`pr-block-tabs-view__tab ${
                currentTab === k ? 'pr-block-tabs-view__tab--active' : ''
              }`}
            >
              {isLocalizedObject(currentText) ? localize(currentText) : (currentText as ReactNode)}
            </button>
          );
        })}
      </div>
      <div className="pr-block-tabs-view__content">
        {tabs.map((tab, index) => (
          <GenericBlock
            key={index}
            content={tab.content?.blocks}
            className={`${tab.content?.className || ''}${index === currentTab ? '' : 'hidden'}`}
          />
        ))}
      </div>
    </div>
  );
};

export default TabsView;
