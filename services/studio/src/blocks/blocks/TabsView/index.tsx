import { useBlock } from '@/blocks/Provider';
import TabsView, { TabsViewConfig } from './TabsView';
import { BaseBlock } from '../BaseBlock';
import { defaultStyles } from './defaultStyles';

export const TabsViewInContext = () => {
  const { config, events } = useBlock<TabsViewConfig>();

  return (
    <BaseBlock defaultStyles={defaultStyles}>
      <TabsView {...config} events={events} />
    </BaseBlock>
  );
};

TabsViewInContext.styles = defaultStyles;

export default TabsViewInContext;
