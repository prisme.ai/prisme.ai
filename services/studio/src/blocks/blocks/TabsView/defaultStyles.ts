const css = String.raw;
export const defaultStyles = css`
  :block {
    display: flex;
    flex: 1;
    flex-direction: column;
  }

  .pr-block-tabs-view__tabs {
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
  }

  .pr-block-tabs-view__tab {
    display: flex;
    padding: 0.5rem 1rem;
    margin: 0.5rem;
    border-radius: 0.5rem;
    border: 1px solid var(--pr-accent-color);
  }
  .pr-block-tabs-view__tab--active {
    background: var(--pr-accent-color);
    color: var(--accent-contrast-color);
  }
  .pr-block-tabs-view__content--hidden {
    display: none;
  }
`;
