import { useMemo, useRef } from 'react';
import { StackedNavigationContext, useStackedNavigation } from './context';
import { useBlocks } from '../../Provider/blocksContext';
import styles from './stacked-navigation.module.scss';
import ChevronIcon from '@/svgs/chevron.svg';

interface HeadRendererProps {
  blocks: StackedNavigationContext['head'];
  onBack: () => void;
  hasHistory: boolean;
}
export const HeadRenderer = ({ blocks, onBack, hasHistory }: HeadRendererProps) => {
  const {
    utils: { BlockLoader },
  } = useBlocks();
  const buttonEl = useRef<HTMLButtonElement>(null);

  const margin = buttonEl.current ? buttonEl.current.getBoundingClientRect().width * 2 : 0;

  const legacyBlocks = blocks.map(({ block, slug, ...rest }) => ({
    slug: slug || block || '',
    ...rest,
  }));
  return (
    <div
      className={styles['head-animated-ctn']}
      style={{
        transform: hasHistory ? undefined : `translate(-${margin}px, 0)`,
      }}
    >
      <div
        className={styles['head-animated']}
        //style={{ marginRight: `-${margin}px`, transform: `translate3d(${margin}px, 0, 0)` }}
      >
        <button
          onClick={onBack}
          className={`head__button-back button-back ${styles['head-back-btn']}`}
          ref={buttonEl}
        >
          <ChevronIcon className={styles['head-back-btn-icon']} />
        </button>
        {blocks && (
          <BlockLoader name="BlocksList" config={{ blocks: legacyBlocks, className: 'flex-1' }} />
        )}
      </div>
    </div>
  );
};

export const Head = () => {
  const { back, history, head } = useStackedNavigation();

  return useMemo(
    () => <HeadRenderer onBack={back} hasHistory={history.length > 1} blocks={head} />,
    [back, history, head],
  );
};

export default Head;
