import { useLayoutEffect, useRef, useState } from 'react';
import { Content as IContent } from './context';
import { useBlocks } from '../../Provider/blocksContext';
import styles from './stacked-navigation.module.scss';

interface ContentProps {
  content: IContent;
  onUnmount: () => void;
  removed: boolean;
  className?: string;
}

export const Content = ({ content: { blocks }, onUnmount, removed }: ContentProps) => {
  const {
    utils: { BlockLoader },
  } = useBlocks();
  const [visible, setVisible] = useState(false);
  const containerEl = useRef<HTMLDivElement>(null);

  useLayoutEffect(() => {
    setTimeout(() => setVisible(true));
  }, []);

  useLayoutEffect(() => {
    if (!removed) return;
    setTimeout(() => setVisible(false));
    setTimeout(onUnmount, 200);
  }, [removed, onUnmount]);

  const legacyBlocks = blocks.map(({ block, slug, ...rest }) => ({
    slug: slug || block || '',
    ...rest,
  }));

  return (
    <div
      ref={containerEl}
      className={`${styles['content-page']} content-stack__content content ${visible ? styles['content-page--visible'] : ''}`}
    >
      {blocks && (
        <BlockLoader
          name="BlocksList"
          config={{
            blocks: legacyBlocks,
            className: styles['content-page-blocks-list'],
            blocksClassName: `content__block-container block-container ${styles['content-page-blocks-list-block']}`,
          }}
        />
      )}
    </div>
  );
};
export default Content;
