import { useCallback, useEffect, useMemo, useState } from 'react';
import Content from './Content';
import { Content as IContent, StackedNavigationContext, useStackedNavigation } from './context';
import styles from './stacked-navigation.module.scss';

interface ContentContainerProps {
  history: StackedNavigationContext['history'];
  marginTop: number;
  marginBottom: number;
}

interface Page {
  content: IContent;
  removed: boolean;
}

export const ContentContainerRenderer = ({ history }: ContentContainerProps) => {
  const [pages, setPages] = useState<Page[]>([]);

  useEffect(() => {
    setPages((pages) => {
      // Update content case
      if (
        history.length &&
        pages.length &&
        pages[pages.length - 1].content.title === history[history.length - 1].title
      ) {
        return pages.map((page, index) =>
          index === pages.length - 1
            ? ({
                content: history[history.length - 1],
                removed: false,
              } as Page)
            : page,
        );
      }

      // A new page appears
      if (history.length > pages.length) {
        return [
          ...pages,
          {
            content: [...history].pop(),
            removed: false,
          } as Page,
        ];
      }

      //
      return pages.map((page, k) =>
        k === pages.length - 1
          ? {
              ...page,
              removed: true,
            }
          : page,
      );
    });
  }, [history]);

  const onUnmount = useCallback(() => {
    const newPages = [...pages];
    newPages.pop();
    setPages(newPages);
  }, [pages]);

  return (
    <div className={`block-layout__content-stack content-stack ${styles['content-stack']}`}>
      <div className={styles['content-pages-ctn']}>
        {pages.map(({ content, removed }, index) => (
          <Content key={index} content={content} onUnmount={onUnmount} removed={removed} />
        ))}
      </div>
    </div>
  );
};

export const ContentContainer = () => {
  const { history, headBox } = useStackedNavigation();
  const marginTop = headBox ? +headBox.height.toFixed() : 0;
  return useMemo(
    () => <ContentContainerRenderer history={history} marginTop={marginTop} marginBottom={0} />,
    [history, marginTop],
  );
};
export default ContentContainer;
