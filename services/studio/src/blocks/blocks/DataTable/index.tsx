import { useBlock } from '@/blocks/Provider';
import { DataTable, DataTableConfig } from './DataTable';
import { BaseBlock } from '../BaseBlock';
import { defaultStyles } from './defaultStyles';

export * from './DataTable';

export const DataTableInContext = () => {
  const { config, events } = useBlock<DataTableConfig>();

  return (
    <BaseBlock defaultStyles={defaultStyles}>
      <DataTable {...config} events={events} />
    </BaseBlock>
  );
};
DataTableInContext.styles = defaultStyles;

export default DataTableInContext;
