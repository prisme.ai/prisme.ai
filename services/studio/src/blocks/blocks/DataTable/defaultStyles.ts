const css = String.raw;
export const defaultStyles = css`
  .pr-block-data-table__title {
    padding-top: 1rem;
  }
  .pr-block-data-table__table-container {
    overflow: auto;
  }
  .ant-table-selection {
    display: flex;
    flex-direction: row;
    width: 3rem;
  }
  .ant-table-selection-column {
    text-align: left;
  }
  .ant-table-selection-extra {
    left: 1.5rem;
    opacity: 0;
    transition: opacity 0.2s ease-in;
  }
  :block.pr-block-data-table--has-bulk-selection .ant-table-selection-extra {
    opacity: 1;
  }
  .ant-table-selection-extra svg {
    color: black;
  }
  .pr-block-data-table__editable-cell {
    cursor: pointer;
    padding: 0.5rem 0.75rem;
    min-width: 145px;
  }
`;
