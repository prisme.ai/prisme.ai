import Icon from './Icon';
import Action from './Action';
import BlocksList from './BlocksList';
import RichText from './RichText';
import ProductHome from './ProductHome';
import ProductLayout from './ProductLayout';
import Form from './Form';
import DataTable from './DataTable';
import DropDown from './DropDown';
import Hero from './Hero';
import Modal from './Modal';
import StackedNavigation from './StackedNavigation';
import TabsView from './TabsView';
import Breadcrumbs from './Breadcrumbs';
import Footer from './Footer';
import Head from './Head';
import Header from './Header';
import Image from './Image';
import Signin from './Signin';
import Toast from './Toast';
import Tooltip from './Tooltip';
import Cards from './Cards';
import Carousel from './Carousel';

export const builtinBlocks = {
  Action,
  BlocksList,
  Icon,
  ProductHome,
  ProductLayout,
  RichText,
  Form,
  DataTable,
  DropDown,
  Hero,
  Modal,
  StackedNavigation,
  TabsView,
  Breadcrumbs,
  Footer,
  Head,
  Header,
  Image,
  Signin,
  Toast,
  Tooltip,
  Cards,
  Carousel,
};
