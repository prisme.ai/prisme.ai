import { useBlock } from '../../Provider';
import { useEffect } from 'react';

interface HeadConfig {
  content?: string;
}

export const HeadBlock = () => {
  const {
    config: { content = '' },
  } = useBlock<HeadConfig>();

  useEffect(() => {
    const markup = document.createElement('head');
    markup.innerHTML = content;
    const prevTags = Array.from(markup.querySelectorAll('*'));
    try {
      prevTags.forEach((tag) => document.head.appendChild(tag));
    } catch {}
    return () => {
      try {
        prevTags.forEach((tag) => document.head.removeChild(tag));
      } catch {}
    };
  }, [content]);
  return null;
};

export default HeadBlock;
