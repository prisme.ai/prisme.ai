import SchemaForm from '@/components/SchemaForm';
import { SchemaFormContext } from '@/components/SchemaForm/context';
import { createContext, FC, ReactNode, useContext } from 'react';
import { Block, TBlockLoader } from './types';

export interface BlocksDependenciesContext {
  externals: any;
  components: {
    Link: FC<{ href: string; children?: ReactNode } & any>;
    Loading: FC;
    DownIcon: FC<{ className?: string }>;
    SchemaForm: typeof SchemaForm;
    Head?: FC<{ children: string }>;
  };
  utils: Partial<SchemaFormContext['utils']> & {
    BlockLoader: TBlockLoader;
    getWorkspaceHost: () => string;
    auth?: {
      getSigninUrl: (options?: { redirect?: string }) => Promise<string>;
      getSignupUrl: (options?: { redirect?: string }) => Promise<string>;
    };
    changeBlockConfig: (block: any, newConfig: Record<string, any>) => Block;
    fetchWorkspaceOnly: typeof globalThis.fetch;
    colorScheme: 'dark' | 'light';
  };
}

export const blocksContext = createContext<BlocksDependenciesContext | undefined>(undefined);
export const useBlocks = () => {
  const context = useContext(blocksContext);
  if (!context) {
    throw new Error();
  }
  return context;
};

export default blocksContext;
