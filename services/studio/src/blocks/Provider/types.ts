import { Schema } from '@/components/SchemaForm';
import { ReactElement, ReactNode } from 'react';

export type Block = ((props: {
  workspaceId: string;
  appInstance: string;
  language: string;
}) => ReactNode) & {
  schema?: Schema;
};

interface BlockLoaderProps {
  // Block name
  name: string;
  // Block config
  config?: any;
  // Callback called on block load. Usefull for external Blocks
  onLoad?: () => void;
  // Container element
  container?: HTMLElement;
}
export type TBlockLoader = (props: BlockLoaderProps) => ReactElement | null;
