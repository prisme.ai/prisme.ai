import blocksContext, { BlocksDependenciesContext } from './blocksContext';
import { ReactNode } from 'react';

export interface BlocksProviderProps extends Omit<BlocksDependenciesContext, 'utils'> {
  children: ReactNode;
  utils?: Partial<BlocksDependenciesContext['utils']>;
  language?: string;
}

const dftUtils = {
  BlockLoader: () => null,
  getWorkspaceHost() {
    return `${window.location.protocol}//${window.location.host}`;
  },
  changeBlockConfig: (block: any) => block,
  fetchWorkspaceOnly: async () => new Response(),
};

const uploadFile = async (file: string) => file;

export const BlocksProvider = ({
  children,
  externals,
  utils = dftUtils,
  components,
}: BlocksProviderProps) => {
  return (
    <blocksContext.Provider
      value={{
        externals,
        components,
        utils: {
          uploadFile,
          colorScheme: 'light',
          ...dftUtils,
          ...utils,
        },
      }}
    >
      {children}
    </blocksContext.Provider>
  );
};

export default BlocksProvider;
