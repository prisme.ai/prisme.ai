import { Api, Events } from '@prisme.ai/sdk';
import { AppConfig } from 'antd/es/app/context';
import { createContext, useContext } from 'react';

export interface BlockContext<T = any> {
  setAppConfig?: (config: AppConfig) => void;
  appConfig?: AppConfig;
  setConfig?: (config: Record<string, unknown>) => void;
  config: T;
  events?: Events;
  api?: Api;
  onLoad?: (block: any) => void;
}

export const blockContext = createContext<BlockContext | undefined>(undefined);
export const useBlock = <T>(): BlockContext<T> => {
  const context = useContext(blockContext);
  if (!context) {
    throw new Error();
  }

  return context;
};

export default blockContext;
