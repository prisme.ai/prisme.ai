import NotFound from '@/layouts/NotFound';

export default function NotFoundServer() {
  return (
    <div>
      Not found
      <NotFound />
    </div>
  );
}
