import { getTranslations } from 'next-intl/server';
import EnvProvider from '@/providers/Env';
import { Viewport } from 'next';
import dftFaviconUrl from '@/svgs/logo.svg?url';
import '@/styles/globals.scss';
import { getCustomization } from '@/providers/Customization';

export async function generateMetadata({ params: { locale } }: { params: { locale: string } }) {
  const [t, customization] = await Promise.all([
    getTranslations({ locale, namespace: 'common' }),
    getCustomization(),
  ]);
  return {
    title: t('main.title'),
    description: t('main.description'),
    icons: {
      icon: customization?.favicon || dftFaviconUrl.src,
    },
  };
}

export const viewport: Viewport = {
  width: 'device-width',
  initialScale: 1,
  maximumScale: 1,
  userScalable: false,
};

export default async function Layout({ children }: { children: React.ReactNode }) {
  return <EnvProvider>{children}</EnvProvider>;
}
