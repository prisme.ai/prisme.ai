import MainLayout from '@/layouts/MainLayout';
import UserSpace from '../../../layouts/UserSpace/UserSpace';
import WorkspaceBuilder from '../../../layouts/WorkspaceBuilder/WorkspaceBuilder';
import { ReactNode } from 'react';
import localFont from 'next/font/local';
import { getCustomization } from '@/providers/Customization';

const satoshMediumItalic = localFont({
  src: '../fonts/satoshi/fonts/Satoshi-MediumItalic.woff2',
  variable: '--font-satoshi-medium-italic',
  weight: '500',
});
const satoshiRegular = localFont({
  src: '../fonts/satoshi/fonts/Satoshi-Regular.woff2',
  variable: '--font-satoshi-regular',
  weight: '400',
});
const satoshiVariable = localFont({
  src: '../fonts/satoshi/fonts/Satoshi-Variable.woff2',
  variable: '--font-satoshi-variable',
  weight: '300 900',
});
const satoshiVariableItalic = localFont({
  src: '../fonts/satoshi/fonts/Satoshi-VariableItalic.woff2',
  variable: '--font-satoshi-variable-italic',
  weight: '400',
});

export async function generateMetadata() {
  const customization = await getCustomization();
  return {
    title: customization?.mainTitle || 'Prisme.ai',
  };
}

export default function LocalizedStudioLayout({
  children,
  params: { locale },
}: {
  children: ReactNode;
  params: {
    locale: string;
  };
}) {
  return (
    <MainLayout
      locale={locale}
      className={`${satoshiVariable.variable} ${satoshMediumItalic.variable} ${satoshiVariableItalic.variable} ${satoshiRegular.variable}`}
    >
      <UserSpace>
        <WorkspaceBuilder>{children}</WorkspaceBuilder>
      </UserSpace>
    </MainLayout>
  );
}
