'use client';

import { useUserSpace } from '../../../../layouts/UserSpace/context';
import { notFound } from 'next/navigation';
import { ReactNode } from 'react';

export default function ({ children }: { children: ReactNode }) {
  const { disableBuilder } = useUserSpace();
  if (disableBuilder) {
    notFound();
  }
  return children;
}
