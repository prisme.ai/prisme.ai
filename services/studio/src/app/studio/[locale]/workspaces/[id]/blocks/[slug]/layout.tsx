'use client';

import { BlockPreviewProvider } from '@/layouts/WorkspaceBuilder/Workspace/Block/BlockPreview';
import BlockProvider from '@/providers/Block';
import { TrackingCategory } from '@/providers/Tracking';
import { useParams } from 'next/navigation';
import { ReactNode } from 'react';

export default function BlockLayout({ children }: { children: ReactNode }) {
  const { slug = [], id: workspaceId } = useParams() || {};

  return (
    <TrackingCategory category="Block Builder">
      <BlockProvider
        workspaceId={`${workspaceId}`}
        slug={decodeURIComponent(`${(Array.isArray(slug) ? slug : [slug]).join('/')}`)}
      >
        <BlockPreviewProvider>{children}</BlockPreviewProvider>
      </BlockProvider>
    </TrackingCategory>
  );
}
