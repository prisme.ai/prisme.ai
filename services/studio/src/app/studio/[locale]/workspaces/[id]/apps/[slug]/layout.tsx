'use client';

import { AppInstanceProvider } from '@/providers/AppInstance';
import { TrackingCategory } from '@/providers/Tracking';
import { useWorkspace } from '@/providers/Workspace';
import { useParams } from 'next/navigation';
import { ReactNode } from 'react';

export default function AppInstanceWithProvider({ children }: { children: ReactNode }) {
  const { slug } = useParams() || {};
  const { workspace, events } = useWorkspace();

  return (
    <TrackingCategory category="Apps">
      <AppInstanceProvider
        id={decodeURIComponent(`${slug}`)}
        workspaceId={workspace.id}
        events={events}
      >
        {children}
      </AppInstanceProvider>
    </TrackingCategory>
  );
}
