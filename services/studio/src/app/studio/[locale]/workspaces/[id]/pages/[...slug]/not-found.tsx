import BackLink from '@/components/WorkspaceBackLink';

export default function PageNotFound() {
  return (
    <div>
      <p>Page introuvable</p>
      <BackLink>Retour</BackLink>
    </div>
  );
}
