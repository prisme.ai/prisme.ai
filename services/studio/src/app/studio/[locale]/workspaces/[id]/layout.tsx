'use client';

import WorkspaceLayout from '@/layouts/WorkspaceBuilder/Workspace/Layout';
import EventsProvider from '@/providers/Events';
import WorkspaceProvider from '@/providers/Workspace';
import { useWorkspaces } from '@/providers/Workspaces';
import { useParams } from 'next/navigation';
import { ReactNode } from 'react';

export default function WorkspaceLayoutStatic({ children }: { children: ReactNode }) {
  const { id } = useParams() || {};
  const { refreshWorkspace } = useWorkspaces();

  return (
    <WorkspaceProvider id={`${id}`} onUpdate={refreshWorkspace}>
      <EventsProvider workspaceId={`${id}`}>
        <WorkspaceLayout>{children}</WorkspaceLayout>
      </EventsProvider>
    </WorkspaceProvider>
  );
}
