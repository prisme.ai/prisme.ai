import BackLink from '@/components/WorkspaceBackLink';

export default function AppNotFound() {
  return (
    <div>
      <p>App introuvable</p>
      <BackLink>Retour</BackLink>
    </div>
  );
}
