'use client';

import AutomationProvider from '@/providers/Automation';
import { TrackingCategory } from '@/providers/Tracking';
import { useWorkspace } from '@/providers/Workspace';
import { useParams } from 'next/navigation';
import { ReactNode } from 'react';

export default function AutomationWithProvider({ children }: { children: ReactNode }) {
  const { workspace } = useWorkspace();
  const { slug } = useParams() || {};

  return (
    <TrackingCategory category="Automation Builder">
      <AutomationProvider
        workspaceId={workspace.id}
        automationSlug={decodeURIComponent(Array.isArray(slug) ? slug.join('/') : `${slug}`)}
      >
        {children}
      </AutomationProvider>
    </TrackingCategory>
  );
}
