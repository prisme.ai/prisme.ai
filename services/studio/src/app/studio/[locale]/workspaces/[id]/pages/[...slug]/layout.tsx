'use client';

import Page from '@/layouts/WorkspaceBuilder/Workspace/Page';
import { PagePreviewProvider } from '@/layouts/WorkspaceBuilder/Workspace/Page/PagePreview';
import PageProvider from '@/providers/Page';
import { TrackingCategory } from '@/providers/Tracking';
import useRouter from '@/utils/useRouter';
import { useParams } from 'next/navigation';
import { useEffect } from 'react';

export default function PageLayout() {
  const { push } = useRouter();
  const { slug, id: workspaceId } = useParams() || {};
  const pageSlug = (Array.isArray(slug) ? slug : [slug]).join('/');

  useEffect(() => {
    // For preview in console
    const listener = async (e: MessageEvent) => {
      const { type, href } = e.data || {};

      if (type === 'pagePreviewNavigation') {
        const [, slug] = href.match(/^\/(.+$)/) || [];
        push(`/workspaces/${workspaceId}/pages/${slug}`);
      }
    };
    window.addEventListener('message', listener);
    return () => {
      window.removeEventListener('message', listener);
    };
  }, [push, workspaceId]);

  return (
    <TrackingCategory category="Page Builder">
      <PageProvider workspaceId={`${workspaceId}`} slug={decodeURIComponent(pageSlug)}>
        <PagePreviewProvider>
          <Page key={pageSlug} />
        </PagePreviewProvider>
      </PageProvider>
    </TrackingCategory>
  );
}
