'use client';

import { useSelectedLayoutSegments } from 'next/navigation';
import UserSpace from '@/layouts/UserSpace/UserSpace';
import WorkspaceBuilder from '@/layouts/WorkspaceBuilder/WorkspaceBuilder';
import { ReactNode } from 'react';

export default function LayoutClient({ children }: { children: ReactNode }) {
  const segments = useSelectedLayoutSegments() || [];
  if (segments[0] === 'account' && segments[1] === 'delete') {
    return children;
  }

  return (
    <UserSpace>
      <WorkspaceBuilder>{children}</WorkspaceBuilder>
    </UserSpace>
  );
}
