'use client';

import { SignContent } from '@/layouts/Sign/SignContent';
import SigninForm from '@/layouts/Sign/SigninForm';
import { SignType } from '@/layouts/Sign/types';
import ModalProvider from '@/providers/Modal';
import Notifications from '@/providers/Notifications';

export default function Signin() {
  return (
    <SignContent type={SignType.In}>
      <Notifications>
        <ModalProvider>
          <SigninForm />
        </ModalProvider>
      </Notifications>
    </SignContent>
  );
}
