'use client';

import ForgotForm from '@/layouts/Sign/ForgotForm';
import ResetForm from '@/layouts/Sign/ResetForm';
import { SignContent } from '@/layouts/Sign/SignContent';
import { SignType } from '@/layouts/Sign/types';
import ModalProvider from '@/providers/Modal';
import Notifications from '@/providers/Notifications';
import { useSearchParams } from 'next/navigation';

export default function Forgot() {
  const qs = useSearchParams();
  const token = qs?.get('token');
  return (
    <SignContent type={SignType.In}>
      <Notifications>
        <ModalProvider>
          {!token && <ForgotForm />}
          {token && <ResetForm token={token} />}
        </ModalProvider>
      </Notifications>
    </SignContent>
  );
}
