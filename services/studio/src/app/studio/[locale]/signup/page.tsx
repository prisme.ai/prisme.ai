import { SignContent } from '@/layouts/Sign/SignContent';
import SignupForm from '@/layouts/Sign/SignupForm';
import { SignType } from '@/layouts/Sign/types';

export default function Signup() {
  return (
    <SignContent type={SignType.Up}>
      <SignupForm />
    </SignContent>
  );
}
