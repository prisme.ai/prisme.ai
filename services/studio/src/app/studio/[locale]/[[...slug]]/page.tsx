import Playground from '@/components/AutomationBuilder/Graph/Playground';
import { redirect } from 'next/navigation';

export default function CatchAll({ params: { slug } }: { params: { slug: string[] } }) {
  if (
    slug &&
    slug[0] === 'playground' &&
    process.env.CONSOLE_HOST === 'http://studio.local.prisme.ai:3000'
  ) {
    return <Playground />;
  }
  redirect('/products');
}
