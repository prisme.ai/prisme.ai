'use client';

import { useUser } from '@/providers/User';
import { useEffect } from 'react';

export const Signout = () => {
  const { signout } = useUser();
  useEffect(() => {
    const t = setTimeout(signout, 200);
    return () => {
      clearInterval(t);
    };
  }, [signout]);

  return null;
};

export default Signout;
