'use client';

import { useEffect } from 'react';

import { useUser } from '@/providers/User';
import { useTranslations } from 'next-intl';
import SigninForm from '@/layouts/Sign/SigninForm';
import { useParams } from 'next/navigation';

export default function Signin() {
  const t = useTranslations('sign');
  const params = useParams();
  const { validationToken } = params || {};
  const { validateMail, initAuthentication } = useUser();

  useEffect(() => {
    async function validate() {
      if (!validationToken || typeof validationToken !== 'string') return;
      await validateMail(validationToken);
      setTimeout(async () => {
        const url = await initAuthentication();
        window.location.assign(url);
      }, 500);
    }
    validate();
  }, [initAuthentication, validationToken, validateMail]);

  return (
    <div className="m-auto flex">
      <SigninForm show403={t('pages.restricted')} />
    </div>
  );
}
