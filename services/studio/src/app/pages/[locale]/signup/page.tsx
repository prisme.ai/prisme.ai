'use client';

import { useState } from 'react';

import { parse } from 'querystring';
import SignupForm from '@/layouts/Sign/SignupForm';
import ValidateForm from '@/layouts/Sign/ValidateForm';

export const Signup = () => {
  const [newUser, setNewUser] = useState<Prismeai.User>();
  const { location } =
    typeof window !== 'undefined'
      ? window
      : {
          location: {
            search: '',
          } as Location,
        };
  const { redirect } = parse(location.search.replace(/^\?/, ''));
  return (
    <div className="m-auto flex">
      {!newUser && <SignupForm onSignup={setNewUser} redirect={`${redirect}`} />}
      {newUser && (
        <div className="flex-col">
          <ValidateForm email={`${newUser.email}`} sent={true} />
        </div>
      )}
    </div>
  );
};

export default Signup;
