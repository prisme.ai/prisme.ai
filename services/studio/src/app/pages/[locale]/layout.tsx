import MainLayout from '@/layouts/MainLayout';
import { ReactNode } from 'react';
import localFont from 'next/font/local';
import { PublicBlocksProvider } from '@/providers/BlocksProvider';
import { PageContentProvider } from '@/providers/PageContent';

const satoshMediumItalic = localFont({
  src: '../fonts/satoshi/fonts/Satoshi-MediumItalic.woff2',
  variable: '--font-satoshi-medium-italic',
  weight: '500',
});
const satoshiRegular = localFont({
  src: '../fonts/satoshi/fonts/Satoshi-Regular.woff2',
  variable: '--font-satoshi-regular',
  weight: '400',
});
const satoshiVariable = localFont({
  src: '../fonts/satoshi/fonts/Satoshi-Variable.woff2',
  variable: '--font-satoshi-variable',
  weight: '300 900',
});
const satoshiVariableItalic = localFont({
  src: '../fonts/satoshi/fonts/Satoshi-VariableItalic.woff2',
  variable: '--font-satoshi-variable-italic',
  weight: '400',
});

export default function LocalizedStudioLayout({
  children,
  params: { locale },
}: {
  children: ReactNode;
  params: {
    locale: string;
  };
}) {
  return (
    <MainLayout
      locale={locale}
      className={`${satoshiVariable.variable} ${satoshMediumItalic.variable} ${satoshiVariableItalic.variable} ${satoshiRegular.variable}`}
      anonymous
    >
      <PublicBlocksProvider>
        <PageContentProvider>{children}</PageContentProvider>
      </PublicBlocksProvider>
    </MainLayout>
  );
}
