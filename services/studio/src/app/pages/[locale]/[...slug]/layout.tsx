import { headers } from 'next/headers';
import { ReactNode } from 'react';
import env from '@/providers/Env/env';
import { Api } from '@prisme.ai/sdk';
import { stdLocalize } from '@/utils/useLocalizedText';

const api = new Api({
  host: env.INTERNAL_API_URL || env.API_URL,
});

const cache = new Map();

async function getPage(wSlug: string, pSlug: string) {
  const key = `${wSlug}|${pSlug}`;
  if (!cache.has(key)) {
    cache.set(
      key,
      new Promise(async (resolve) => {
        try {
          resolve(await api.getPageBySlug(wSlug, pSlug));
        } catch {
          resolve(null);
        }
      }),
    );
    setTimeout(() => cache.delete(key), 1000 * 60 * 5);
  }
  return cache.get(key);
}

function removePlaceholders(str: string) {
  return str.replaceAll(/\{\{[^}]+\}\}/g, '');
}

export async function generateMetadata({
  params: { locale = 'en', slug = [] },
}: {
  params: { locale: string; slug: string[] };
}): Promise<
  | {
      title: string;
      description: string;
      icons: {
        icon: string;
      };
    }
  | undefined
> {
  const host = headers().get('host') || '';
  const [workspaceSlug] = host.split(env.PAGES_HOST);
  const pageSlug = slug.join('/');
  try {
    const page = await getPage(workspaceSlug, decodeURIComponent(pageSlug));
    if (!page) return;
    return {
      title: removePlaceholders(stdLocalize(page.name, locale)),
      description: removePlaceholders(stdLocalize(page.description, locale)),
      icons: {
        icon: page.favicon,
      },
    };
  } catch {}
}

export default function ({ children }: { children: ReactNode }) {
  return children;
}
