'use client';

import BlockPreview from '@/components/BlockPreview';
import PagePreview from '@/components/PagePreview';
import Page from '@/layouts/Pages/Page';
import { PublicBlocksProvider } from '@/providers/BlocksProvider';
import { PageContentProvider } from '@/providers/PageContent/PageContentProvider';
import isIOS from '@/utils/isIOS';
import { removeLocaleFromPath } from '@/utils/urls';
import { useParams, usePathname, useSearchParams } from 'next/navigation';
import { useEffect } from 'react';

export default function CatchAll() {
  const params = useParams();
  const { slug = '' } = params || {};
  const pathname = usePathname();
  const search = useSearchParams();

  useEffect(() => {
    if (isIOS()) {
      document.body.classList.add('is-ios');
    }
  }, []);

  useEffect(() => {
    const path = removeLocaleFromPath(pathname || '');
    window.parent?.postMessage(
      {
        type: 'page.navigate',
        path: `/${path}${search && search.size ? `?${search.toString()}` : ''}`.replaceAll(
          /\/+/g,
          '/',
        ),
      },
      '*',
    );
  }, [pathname, search]);
  if (slug[0] === '__preview') {
    if (slug[1] === 'block') {
      return <BlockPreview />;
    }
    return <PagePreview />;
  }

  return (
    <PublicBlocksProvider>
      <PageContentProvider
        slug={(Array.isArray(slug) ? slug : [slug])
          .map((s) => decodeURIComponent(`${s}`))
          .join('/')}
      >
        <Page />
      </PageContentProvider>
    </PublicBlocksProvider>
  );
}
