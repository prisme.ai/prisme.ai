'use client';

import BlockPreview from '@/components/BlockPreview';
import PagePreview from '@/components/PagePreview';
import Page from '@/layouts/Pages/Page';
import { usePageContent } from '@/providers/PageContent';
import isIOS from '@/utils/isIOS';
import { removeLocaleFromPath } from '@/utils/urls';
import { useParams, usePathname, useSearchParams } from 'next/navigation';
import { useEffect } from 'react';

export default function CatchAll() {
  const params = useParams();
  const { slug = '' } = params || {};
  const pathname = usePathname();
  const search = useSearchParams();
  const { fetchPage } = usePageContent();

  useEffect(() => {
    if (isIOS()) {
      document.body.classList.add('is-ios');
    }
  }, []);

  useEffect(() => {
    const path = removeLocaleFromPath(pathname || '');
    window.parent?.postMessage(
      {
        type: 'page.navigate',
        path: `/${path}${search && search.size ? `?${search.toString()}` : ''}`.replaceAll(
          /\/+/g,
          '/',
        ),
      },
      '*',
    );
  }, [pathname, search]);

  const isPagePreview = slug[0] === '__preview' && slug[1] === 'page';
  const isBlockPreview = slug[0] === '__preview' && slug[1] === 'block';

  useEffect(() => {
    if (isPagePreview || isBlockPreview) return;
    const pageSlug = (Array.isArray(slug) ? slug : [slug])
      .map((s) => decodeURIComponent(`${s}`))
      .join('/');
    fetchPage(pageSlug);
  }, [slug, fetchPage, isPagePreview, isBlockPreview]);

  if (isBlockPreview) {
    return <BlockPreview />;
  }
  if (isPagePreview) {
    return <PagePreview />;
  }

  return <Page />;
}
