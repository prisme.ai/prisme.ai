import { getTranslations } from 'next-intl/server';
import type { Metadata, Viewport } from 'next';
import '@/styles/globals.scss';
import EnvProvider from '@/providers/Env';

export const _metadata: Metadata = {
  title: 'Prisme.ai',
  description: 'Prisme.ai, AI Gen & cie',
};

export async function generateMetadata({ params: { locale } }: { params: { locale: string } }) {
  const t = await getTranslations({ locale, namespace: 'common' });
  return {
    title: t('main.title'),
    description: t('main.description'),
  };
}

export const viewport: Viewport = {
  width: 'device-width',
  initialScale: 1,
  maximumScale: 1,
  userScalable: false,
};

export default async function LocaleLayout({ children }: { children: React.ReactNode }) {
  return <EnvProvider>{children}</EnvProvider>;
}
