// Because legacy translations contains keys with dots like 'a.b.c' and must be
// cleaned manually
export default function replaceDotsKeysByNestedObjects(
  translations: Record<string, any>,
): Record<string, any> {
  if (!translations) return {};
  return Object.fromEntries(
    Object.entries(translations).reduce<[string, any][]>((prev, [k, v]) => {
      const [root, ...path] = k.split(/\./);
      if (path.length > 0) {
        return [
          ...prev,
          [
            root,
            replaceDotsKeysByNestedObjects({
              [path.join('.')]: v,
            }),
          ],
        ];
      }
      if (typeof v === 'object') {
        return [...prev, [k, replaceDotsKeysByNestedObjects(v)]];
      }
      return [...prev, [k, v]];
    }, []),
  );
}
