import { useTranslations as orig } from 'next-intl';

/**
 * Bridge with i18next functionnalities
 */
export default function useTranslations(namespace?: string) {
  const t = orig();

  function getKey(
    k: string,
    params: {
      ns?: string;
      context?: string;
    } = {},
  ) {
    let key = k as string;
    if (params.ns || namespace) {
      key = `${params.ns || namespace}.${key}`;
    }
    if (params.context) {
      const keyWithContext = `${key}_${params.context}`;
      if (t.has(keyWithContext)) {
        key = keyWithContext;
      }
    }
    return key;
  }

  const enhancedT: typeof t = (k, params = {}) => {
    const key = getKey(k, params);
    let translation = t(key, params) || '';

    if (translation.match(/\$t\([^)]+\)/g)) {
      translation = translation.replace(/\$t\(([^)]+)\)/g, (match, nskey) => {
        let [ns, key] = nskey.split(/:/);
        if (!key) {
          key = ns;
          ns = null;
        }
        return enhancedT(key, {
          ns,
        });
      });
    }
    return translation;
  };
  enhancedT.has = (key, params: Record<string, unknown> = {}) => t.has(getKey(key, params));
  enhancedT.rich = (key, params) => t.rich(getKey(key, params), params);
  enhancedT.raw = (key) => t.raw(key);
  enhancedT.markup = (key, params) => t.markup(getKey(key, params), params);
  return enhancedT;
}
