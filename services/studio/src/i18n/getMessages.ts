import env from '@/providers/Env/env';
import { promises as fs } from 'fs';
import replaceDotsKeysByNestedObjects from './replaceDotsKeysByNestedObjects';
import deepmerge from 'deepmerge';
import path from 'path';
import { fileURLToPath } from 'url';

const { TRANSLATIONS_OVERRIDE } = env;

let customizationPromise: Promise<any | null> | null;

async function getCustomTranslations(locale: string) {
  if (!TRANSLATIONS_OVERRIDE) return;
  if (!customizationPromise) {
    setTimeout(() => {
      customizationPromise = null;
    }, 1000 * 60);
    customizationPromise = new Promise(async (resolve) => {
      try {
        const res = await fetch(TRANSLATIONS_OVERRIDE);
        if (!res.ok) {
          throw new Error();
        }
        const messages = await res.json();
        resolve(replaceDotsKeysByNestedObjects(messages?.[locale]));
      } catch (e) {
        resolve(null);
      }
    });
  }

  return customizationPromise;
}

async function getNamespaces(defaultLocale: string) {
  const __dirname = path.dirname(fileURLToPath(import.meta.url));
  const files = await fs.readdir(`${__dirname}/translations/${defaultLocale}`);
  return files.map((name) => name.replace(/\.json$/, ''));
}

export async function getMessages(locale: string, defaultLocale = 'en') {
  const namespaces = await getNamespaces(defaultLocale);

  const messages = (
    await Promise.all(
      namespaces.map(async (ns) => {
        const messagesChunks = [];
        try {
          messagesChunks.push({
            [ns]: (await import(`./translations/${defaultLocale}/${ns}.json`)).default,
          });
        } catch {}
        try {
          if (locale !== defaultLocale) {
            messagesChunks.push({
              [ns]: (await import(`./translations/${locale}/${ns}.json`)).default,
            });
          }
        } catch {}
        return deepmerge(messagesChunks[0] || {}, messagesChunks[1] || {});
      }),
    )
  ).reduce((prev, next) => deepmerge(prev, next));

  return {
    locale,
    messages: messages && replaceDotsKeysByNestedObjects(messages),
  };
}

export default async function getAllMessages(locale: string) {
  const [messages, customMessages] = await Promise.all([
    await getMessages(locale),
    await getCustomTranslations(locale),
  ]);

  return deepmerge(messages.messages, customMessages || {});
}
