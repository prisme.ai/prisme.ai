'use client';

import { AbstractIntlMessages, IntlError, NextIntlClientProvider, useLocale } from 'next-intl';
import { ReactNode, useCallback } from 'react';

export default function I18nProvider({
  children,
  messages,
}: {
  children: ReactNode;
  messages: AbstractIntlMessages | undefined;
}) {
  const locale = useLocale();
  const onError = useCallback((error: IntlError) => {
    if (process.env.NEXT_PUBLIC_DEBUG_INTL) {
      console.error(error);
    }
  }, []);
  const getMessageFallback = useCallback(
    ({ key }: { error: IntlError; key: string; namespace?: string }) => {
      return key;
    },
    [],
  );
  return (
    <NextIntlClientProvider
      messages={messages}
      onError={onError}
      getMessageFallback={getMessageFallback}
      locale={locale}
    >
      {children}
    </NextIntlClientProvider>
  );
}
