'use client';

import { AbstractIntlMessages, IntlError, NextIntlClientProvider } from 'next-intl';
import { ReactNode, useCallback } from 'react';

export default function I18nProvider({
  children,
  messages,
  locale,
}: {
  children: ReactNode;
  messages: AbstractIntlMessages | undefined;
  locale: string;
}) {
  const onError = useCallback((error: IntlError) => {
    if (process.env.NEXT_PUBLIC_DEBUG_INTL) {
      console.error(error);
    }
  }, []);
  const getMessageFallback = useCallback(
    ({ key }: { error: IntlError; key: string; namespace?: string }) => {
      return key;
    },
    [],
  );
  return (
    <NextIntlClientProvider
      messages={messages}
      onError={onError}
      getMessageFallback={getMessageFallback}
      locale={locale}
    >
      {children}
    </NextIntlClientProvider>
  );
}
