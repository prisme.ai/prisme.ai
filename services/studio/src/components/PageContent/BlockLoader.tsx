'use client';

import {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import { useUser } from '@/providers/User';
import { computeBlock, interpolateValue, original } from './computeBlocks';
import { useDebug } from './useDebug';
import fastDeepEqual from 'fast-deep-equal';
import isServerSide from '@/utils/isServerSide';
import { applyCommands } from './commands';
import PoweredBy from './PoweredBy';
import useLocalizedText from '@/utils/useLocalizedText';
import { TBlockLoader } from '@/blocks/Provider';
import { useLocale } from 'use-intl';
import BLoader from '@/blocks/BlockLoader';
import { defaultStyles } from '@/layouts/WorkspaceBuilder/Workspace/Page/defaultStyles';
import { useRedirect } from '@/providers/PageContent/useRedirect';
import { usePageContent } from '@/providers/PageContent';
import { useParams, usePathname } from 'next/navigation';
import { Api } from '@prisme.ai/sdk';
import useApi from '@/utils/api/useApi';
import { useEnv } from '@/providers/Env';
import Head from '../Head';

/**
 * This function aims to replace deprecated Block names by the new one
 */
function getBlockName(name: string) {
  switch (name) {
    case 'Layout':
      return 'StackedNavigation';
    default:
      return name;
  }
}

export const recursiveConfigContext = createContext<Record<string, any>>({});
export const useRecursiveConfigContext = () => useContext(recursiveConfigContext);

async function callAutomation(
  api: Api,
  workspaceId: string,
  automation: Prismeai.Block['automation'],
  query: any,
) {
  const { slug, payload = {} } =
    !automation || typeof automation === 'string' ? { slug: automation } : automation;

  if (!slug) return;

  const computed = computeBlock(payload, query);
  return await api.callAutomation(workspaceId, slug, {
    ...query,
    SYSTEM: computed.SYSTEM,
    updateOn: computed.updateOn,
  });
}

export const BlockLoader: (
  props: Parameters<TBlockLoader>[0] & {
    isRoot?: true;
  },
) => ReturnType<TBlockLoader> = ({
  name = '',
  config: initialConfig,
  onLoad,
  container,
  isRoot,
}) => {
  const api = useApi();
  const { IS_WHITE_MARK } = useEnv<{ IS_WHITE_MARK: string }>();
  const { user } = useUser();
  const [appConfig, setAppConfig] = useState<any>();
  const { page, events } = usePageContent();
  const [loaded, setLoaded] = useState(false);
  const language = useLocale();
  const { localize } = useLocalizedText();
  const lock = useRef(false);
  const [listening, setListening] = useState(false);
  const recursiveConfig = useRecursiveConfigContext();
  const [config, setConfig] = useState(initialConfig);
  const pathname = usePathname();

  const debug = useDebug();

  const prevInitialConfig = useRef(initialConfig);
  useEffect(() => {
    if (!fastDeepEqual(prevInitialConfig.current, initialConfig)) {
      prevInitialConfig.current = initialConfig;
      setConfig(initialConfig);
    }
  }, [initialConfig]);

  // These values must be computed on the first render to make page rendered
  // on server side
  const prevConfig = useRef(config);
  const { blockName, computedConfig, url } = useMemo(() => {
    if (!fastDeepEqual(prevConfig.current, config)) {
      prevConfig.current = config;
    }
    const output = {
      blockName: name,
      url: '',
      computedConfig: prevConfig.current && computeBlock(prevConfig.current, recursiveConfig),
    };

    if (!name) return output;
    if (name.match(/^http/)) {
      return {
        ...output,
        url: name,
      };
    }

    const parts = name.split(/\./);

    if (recursiveConfig?.__app?.blocks?.[`${recursiveConfig.__app.slug}.${name}`]) {
      const block = recursiveConfig.__app.blocks[`${recursiveConfig.__app.slug}.${name}`];
      return {
        ...output,
        ...block,
        computedConfig: {
          ...computeBlock(
            {
              ...prevConfig.current,
              ...block,
            },
            recursiveConfig,
            true, // merged from a templated Block and a config, so there should
            // not exist any original here
          ),
          slug: name,
        },
        blockName: 'BlocksList',
      };
    }

    const { blocks: workspaceBlocks } =
      (page?.appInstances || []).find(({ slug }) => slug === '') || {};
    if (workspaceBlocks && workspaceBlocks[name]) {
      const block = workspaceBlocks[name];

      if (typeof block === 'string') {
        return {
          ...output,
          url: block,
        };
      }

      // This should be rewrote as a WorkspaceBlock Component
      return {
        ...output,
        ...block,
        computedConfig: computeBlock(
          {
            ...prevConfig.current,
            ...block,
          },
          recursiveConfig,
          true, // merged from a templated Block and a config, so there should
          // not exist any original here
        ),
        blockName: 'BlocksList',
      };
    }
    if (parts.length === 1) {
      return output;
    }

    const [appSlug] = parts;
    const app = (page?.appInstances || []).find(({ slug }) => appSlug === slug);
    if (!app || !app.blocks?.[name]) {
      console.error(`"${name}" Block is not installed`);
      return output;
    }
    const debugUrl = debug.get(name);
    const b = app.blocks[name];
    const appBlock = typeof b === 'string' ? { url: b } : b;
    recursiveConfig.__app = app;
    if (debugUrl) {
      appBlock.url = debugUrl;
    }

    if (appBlock.url) {
      const { url = getBlockName('BlocksList'), blocks = undefined, ...props } = appBlock;

      if (blocks && blocks.length > 0) {
        return {
          ...output,
          blockName: 'BlocksList',
          computedConfig: computeBlock(
            {
              ...output.computedConfig,
              blocks,
              ...props,
            },
            recursiveConfig,
          ),
        };
      }
      return {
        ...output,
        url,
      };
    }

    // This should be rewrote as a WorkspaceBlock Component
    return {
      ...output,
      ...appBlock,
      computedConfig: computeBlock(
        {
          ...prevConfig.current,
          ...appBlock,
        },
        recursiveConfig,
        true, // merged from a templated Block and a config, so there should
        // not exist any original here
      ),
      blockName: 'BlocksList',
    };
  }, [config, name, recursiveConfig, page?.appInstances, debug]);

  const { onInit, updateOn, automation } = computedConfig || {};
  const onBlockLoad = useCallback(() => {
    setLoaded(true);
  }, []);

  const redirect = useRedirect();
  const automationLoadingState = useRef(-1);
  const query = useParams();
  const initWithAutomation = useRef(() => {
    return;
  });

  useEffect(() => {
    initWithAutomation.current = async () => {
      if (!user || !page || !page.workspaceId || automationLoadingState.current > -1 || !events)
        return;

      try {
        automationLoadingState.current = 0;
        const urlSearchParams = new URLSearchParams(window.location.search);
        const query = {
          pageSlug: page.slug,
          ...Object.fromEntries(urlSearchParams.entries()),
        };

        const newConfig = await callAutomation(api, page.workspaceId, automation, query);
        if (isRoot) {
          redirect(newConfig);
        }
        setConfig(({ [original]: ignore = null, ...prev } = {}) => {
          void ignore;
          return {
            ...prev,
            ...newConfig,
          };
        });
        if (updateOn && newConfig.userTopics) {
          events.listenTopics({
            event: updateOn,
            topics: newConfig.userTopics,
          });
        }
      } catch {}
      automationLoadingState.current = 1;
    };
  }, [automation, computedConfig, events, isRoot, page, redirect, updateOn, user, api, pathname]);

  // This is needed to re-init block when page navigate without reloading
  // by changing query string
  const unmount = useRef(false);
  useEffect(() => {
    unmount.current = false;
    return () => {
      unmount.current = true;
    };
  }, []);
  /*
  If pathname change, a new page is fetched. But in the case where querystring
  change in the same time (ex from /agent?id=123 to /chat?id=456)
  the init automation sometimes is called too soon (because of query string
  change) before the new page and its own automation name is fetched.
  To avoid this specific case, when pathname change, we anticip the component
  is unmount.
  */
  const prevPathname = useRef(pathname);
  useEffect(() => {
    if (pathname !== prevPathname.current) {
      unmount.current = true;
    }
  }, [pathname]);

  const refetch = useRef(() => {
    setTimeout(() => {
      if (automation && !unmount.current) {
        automationLoadingState.current = -1;
        initWithAutomation.current();
      }
    });
  });
  const prevHref = useRef(window.location.href);
  useEffect(() => {
    if (fastDeepEqual(prevHref.current, window.location.href)) return;
    prevHref.current = window.location.href;
    refetch.current();
  }, [query]);

  useEffect(() => {
    if (!user || !loaded || !events) return;
    onLoad && onLoad();
    // Set listeners
    const off: Function[] = [];
    if (updateOn) {
      off.push(
        events.on(updateOn, ({ payload: config }) => {
          setConfig((prev = {}) => {
            const newConfig = applyCommands(prev, config);

            if (fastDeepEqual(newConfig, prev)) return prev;
            const newBlock = computeBlock(newConfig, { ...recursiveConfig }, true);
            return newBlock;
          });
          if (config.userTopics) {
            events.listenTopics({ event: updateOn, topics: config.userTopics });
          }
        }),
      );
    }
    setListening(true);

    if (automation) {
      initWithAutomation.current();
    }

    return () => {
      off.forEach((off) => off());
    };
  }, [
    events,
    onLoad,
    updateOn,
    onInit,
    page,
    initialConfig,
    loaded,
    automation,
    initWithAutomation,
    user,
    recursiveConfig,
  ]);

  const alreadySentInit = useRef(false);

  useEffect(() => {
    if (!user || !listening || !events || alreadySentInit.current || !onInit) return;
    alreadySentInit.current = true;
    const payload: any = {
      page: page && page.id,
      config: initialConfig,
      language,
    };
    if (window.location.hash && !payload.hash) {
      payload.hash = window.location.hash;
    }
    if (window.location.search) {
      payload.query = Array.from(new URLSearchParams(window.location.search).entries()).reduce(
        (prev, [key, value]) => ({
          ...prev,
          [key]: value,
        }),
        {},
      );
    }
    const { event, payload: staticPayload = {} } =
      typeof onInit === 'string' ? { event: onInit } : onInit;
    events.emit(event, { ...staticPayload, ...payload });
  }, [events, initialConfig, language, listening, onInit, page, user]);

  const onAppConfigUpdate = useCallback(
    async (newConfig: any) => {
      lock.current = true;
      setAppConfig(() => newConfig);
      if (name.match(/^http/) || !name.match(/\./)) return;
      const [appInstance] = name.split(/\./);
      if (!page?.workspaceId || !appInstance) return;
      return api.updateAppConfig(page.workspaceId, appInstance, newConfig);
    },
    [name, page, api],
  );

  // This lines force browser to re render page and regenerate classnames
  const [render, setRender] = useState(isServerSide());
  useEffect(() => {
    setRender(true);
  }, []);

  const cumulatedConfig = useMemo(
    () => ({
      ...recursiveConfig,
      ...computedConfig,
    }),
    [computedConfig, recursiveConfig],
  );

  if (!page || !render) return null;
  if (computedConfig?.hidden) {
    return null;
  }
  const finalBlock = (
    <recursiveConfigContext.Provider value={cumulatedConfig}>
      <BLoader
        name={getBlockName(blockName)}
        url={url}
        appConfig={appConfig}
        onAppConfigUpdate={onAppConfigUpdate}
        api={api}
        language={language}
        workspaceId={`${page.workspaceId}`}
        events={events}
        config={computedConfig}
        layout={{
          container,
        }}
        onLoad={onBlockLoad}
      />
    </recursiveConfigContext.Provider>
  );

  if (isRoot) {
    const { styles = defaultStyles } = page;
    return (
      <>
        <Head
          title={localize(interpolateValue(page.name, computedConfig))}
          description={localize(interpolateValue(page.description, computedConfig))}
        >
          {page.favicon && <link rel="icon" href={page.favicon || '/favicon.png'} />}
        </Head>
        <div className="page m-0 flex min-h-full max-w-[100vw] flex-1 flex-col p-0">
          {styles && (
            <style
              dangerouslySetInnerHTML={{
                __html: interpolateValue(styles, computedConfig),
              }}
            />
          )}

          <div className="page-blocks flex w-full flex-1 flex-col">{finalBlock}</div>
          {!IS_WHITE_MARK && <PoweredBy />}
        </div>
      </>
    );
  }

  return finalBlock;
};

export default BlockLoader;
