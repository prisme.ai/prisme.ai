import MainLogo from '@/svgs/logo.svg';
import { useTranslations } from 'next-intl';

export const PoweredBy = () => {
  const t = useTranslations('common');
  return (
    <a
      href="https://prisme.ai"
      target="_blank"
      rel="noreferrer"
      className="pr-poweredby text-pr-grey m-4 flex items-center justify-center self-start rounded-[4px] border-[1px] border-gray-200 bg-white pr-2 !font-[Montserrat] text-[0.75rem]"
      style={{
        boxShadow: '0 2px 2px #888',
      }}
    >
      <div className="m-2 flex">
        <MainLogo width="16px" height="16px" alt="Prisme.ai" />
      </div>
      <div>{t('powered')}</div>
    </a>
  );
};

export default PoweredBy;
