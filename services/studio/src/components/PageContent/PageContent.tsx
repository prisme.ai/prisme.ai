import { useCallback, useEffect, useMemo } from 'react';
import BlockLoader from './BlockLoader';
import { useUser } from '@/providers/User';
import dynamic from 'next/dynamic';
import { usePageContent } from '@/providers/PageContent';
import useApi from '@/utils/api/useApi';
import SchemaFormConfigProvider from '@/components/SchemaForm/SchemaFormConfigContext';
import BlockWidget from '@/components/SchemaFormWidgets/BlockWidget';
import getPreview from '@/components/SchemaFormInWorkspace/getPreview';
import BlockSchemaFormBuilder from '../SchemaFormWidgets/BlockSchemaFormBuilder';

const Debug = dynamic(() => import('./Debug'), { ssr: false });

export interface PageProps {
  page: Prismeai.DetailedPage;
  error?: number | null;
}

export const PageContent = ({ page }: PageProps) => {
  const api = useApi();
  const { events } = usePageContent();
  const { user } = useUser();
  const isSignedIn = user?.authData && !user?.authData?.anonymous;

  useEffect(() => {
    window.Prisme = window.Prisme || {};
    window.Prisme.ai = window.Prisme.ai || {};
    window.Prisme.ai.api = api;
    window.Prisme.ai.events = events;
  }, [events, api]);

  const blocksListConfig = useMemo(() => {
    const blocks = (page.blocks || []).map(({ config: oldSchoolConfig = {}, ...config }) => {
      const { className = '', ...consolidatedConfig } = {
        ...oldSchoolConfig,
        ...config,
      };
      consolidatedConfig.className = `${className} block-${consolidatedConfig.slug}`;
      return consolidatedConfig;
    });
    const {
      appInstances,
      id,
      labels,
      name,
      public: _public,
      slug,
      styles,
      workspaceId,
      workspaceSlug,
      ...pageConfig
    } = page;
    void appInstances, id, labels, name, _public, styles, slug, workspaceId, workspaceSlug;

    return {
      ...pageConfig,
      blocks,
      SYSTEM: {
        userIsSignedIn: isSignedIn,
      },
    };
  }, [isSignedIn, page]);

  const uploadFile = useCallback(
    async (
      file: string,
      opts?: {
        expiresAfter?: number;
        public?: boolean;
        shareToken?: boolean;
      },
    ) => {
      if (!page?.workspaceId) return file;
      // Delete these lines as soon as we migrated existing blocks using legacy syntax
      if (typeof opts === 'number') {
        opts = {
          expiresAfter: opts,
        };
      }
      const [{ url, mimetype, name }] = await api
        .workspaces(page.workspaceId)
        .uploadFiles(file, opts);

      return {
        value: url,
        preview: getPreview(mimetype, url),
        label: name,
      };
    },
    [page?.workspaceId, api],
  );

  return (
    <SchemaFormConfigProvider
      utils={{
        uploadFile,
      }}
      components={{
        UiWidgets: {
          block: BlockWidget,
          'schema-form-builder': BlockSchemaFormBuilder,
        },
      }}
    >
      <BlockLoader key={page.id} name="BlocksList" config={blocksListConfig} isRoot />
      <Debug />
    </SchemaFormConfigProvider>
  );
};

export default PageContent;
