import { expect, it } from 'vitest';
import getErrorLineInYaml from './getErrorLineInYaml';

it('should find the correct line error', () => {
  expect(
    getErrorLineInYaml(
      `name: test
description: test
do:
  emit
  - set:
      name: foo
      value: bar
`,
      '/do',
    ),
  ).toEqual([3, 3]);

  expect(
    getErrorLineInYaml(
      `name: test
description: test
do:
  - emit:
    foo: bar
  - set:
      name: foo
      value: bar
`,
      '/do/0',
    ),
  ).toEqual([4, 9]);

  expect(
    getErrorLineInYaml(
      `name: test
description: test
do:
  - emit:
  - set:
    foo: bar
      name: foo
      value: bar
`,
      '/do/1',
    ),
  ).toEqual([5, 8]);
});
