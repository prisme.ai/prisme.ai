export default function getErrorLineInYaml(data: string, instancePath: string) {
  const [, ...path] = instancePath.split(/\//);
  const alllines = data.split(/\n/);
  let lines = [...alllines];
  let lineNumber = -2;
  path.forEach((part, index) => {
    if (isNaN(+part)) {
      const spaces = Array.from(new Array(index), () => ' ').join('');
      const search = `${spaces}${part}:`;
      lineNumber = lines.findIndex((text) => text === search);
      lines = lines.slice(lineNumber);
    } else {
      const arrayItemsLines: number[] = [];
      lines.forEach((line, index) => {
        if (line.match(/^\s+-/)) {
          arrayItemsLines.push(index);
        }
      });
      if (arrayItemsLines[+part]) {
        lineNumber += arrayItemsLines[+part];
        lines = lines.slice(lineNumber);
      }
    }
  });

  return [lineNumber + 1, alllines?.[lineNumber]?.length || -1];
}
