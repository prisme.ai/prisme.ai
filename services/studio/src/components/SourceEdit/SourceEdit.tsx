import { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import useYaml from '@/utils/useYaml';
import CodeEditor from '@/components/CodeEditor/lazy';
import styles from './source-edit.module.scss';
import getErrorLineInYaml from './getErrorLineInYaml';
import { CodeEditorProps, Decoration } from '../CodeEditor/CodeEditor';
import { YAMLException } from 'js-yaml';
import { debounce } from 'lodash';
import { useNotifications } from '@/providers/Notifications';
import useTranslations from '@/i18n/useTranslations';
import { ValidationError } from '@/utils/useYaml/yaml';

interface SourceEditProps {
  value: any;
  onChange: (value: SourceEditProps['value']) => void;
  onSave: () => void;
  visible?: boolean;
  mounted?: boolean;
  validate?: (value: any) => boolean;
  error?: ValidationError;
  snippets?: CodeEditorProps['snippets'];
  links?: CodeEditorProps['links'];
  onError?: (error: YAMLException | null) => void;
}

export const SourceEdit = <T,>({
  value: original,
  onChange,
  onSave,
  visible = true,
  validate,
  error,
  snippets,
  links,
  onError,
}: SourceEditProps) => {
  const { toJSON, toYaml } = useYaml();
  const t = useTranslations('workspaces');
  const notification = useNotifications();
  const [syntaxError, setSyntaxError] = useState<Decoration[]>();

  const [value, setValue] = useState<string>('');

  const initYaml = useCallback(async () => {
    try {
      const value = await toYaml(original);
      setValue(value);
    } catch (e) {}
  }, [original, toYaml]);

  const checkSyntaxAndReturnYAML = useCallback(
    async (value: string) => {
      if (!original || value === undefined) return;
      try {
        const json = await toJSON<T>(value);
        setSyntaxError([]);
        onError?.(null);
        return json;
      } catch (e) {
        const { mark, message } = e as YAMLException;
        setSyntaxError([
          {
            range: [mark.line, mark.column, mark.line, mark.column],
            wholeLine: true,
            icon: 'warning',
            message,
          },
        ]);
        onError?.(e as YAMLException);
      }
    },
    [original, toJSON, onError],
  );

  const update = useRef<(value: string) => void>(() => null);
  const onChangeRef = useRef(onChange);
  useEffect(() => {
    onChangeRef.current = onChange;
  }, [onChange]);
  const checkSyntaxAndReturnYAMLRef = useRef(checkSyntaxAndReturnYAML);
  useEffect(() => {
    checkSyntaxAndReturnYAMLRef.current = checkSyntaxAndReturnYAML;
  }, [checkSyntaxAndReturnYAML]);
  useEffect(() => {
    update.current = debounce(async (newValue: string) => {
      try {
        const json = await checkSyntaxAndReturnYAMLRef.current(newValue);

        if (!json) return;

        if (validate && !validate(json)) return;
        onChangeRef.current(json);
      } catch (e) {}
    }, 500);
  }, [validate]);

  const onSaveRef = useRef(onSave);
  useEffect(() => {
    onSaveRef.current = () => {
      if (error || syntaxError?.[0]) {
        notification.error({
          message: t('automations.save.error', {
            context: 'codeContainsError',
          }),
          placement: 'bottomRight',
        });
        return;
      }
      onSave();
    };
  }, [onSave, error, t, notification, syntaxError]);
  const shortcuts = useMemo(
    () => [
      {
        name: t('expert.save.help'),
        exec: () => {
          // This tiemout is needed while we debounce the value
          // Then we need to put the onSave in a ref because the fn is regen
          // after the value is set, after the debounce of 500ms is executed
          setTimeout(() => {
            onSaveRef.current();
          }, 600);
        },
        bindKey: {
          mac: 'cmd-s',
          win: 'ctrl-s',
        },
      },
    ],
    [t],
  );

  useEffect(() => {
    initYaml();
  }, [initYaml]);

  const decorations: Decoration[] = useMemo(() => {
    const decorations = [...(syntaxError || [])];
    if (!error) return decorations;
    const [lineNumber, lineLength] = getErrorLineInYaml(value, error.instancePath);
    return [
      ...decorations,
      {
        range: [lineNumber, 0, lineNumber, lineLength],
        wholeLine: true,
        icon: 'warning',
        message: `${error.keyword} ${error.message}`,
      },
    ];
  }, [value, error, syntaxError]);

  if (value === undefined) return null;

  return (
    <div className={`${styles['container']} ${visible ? styles['container--visible'] : ''}`}>
      {visible && (
        <CodeEditor
          mode="yaml"
          defaultValue={value}
          onChange={update.current}
          shortcuts={shortcuts}
          decorations={decorations}
          snippets={snippets}
          links={links}
        />
      )}
    </div>
  );
};

export default SourceEdit;
