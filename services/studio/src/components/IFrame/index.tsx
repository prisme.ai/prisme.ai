import { useColorSchemeManager } from '@/providers/ColorSchemeManager';
import {
  forwardRef,
  IframeHTMLAttributes,
  Ref,
  useCallback,
  useEffect,
  useImperativeHandle,
  useRef,
} from 'react';

export const IFrame = forwardRef(function IFrame(
  props: IframeHTMLAttributes<HTMLIFrameElement>,
  ref: Ref<HTMLIFrameElement | null>,
) {
  const iframe = useRef<HTMLIFrameElement>(null);
  const { scheme } = useColorSchemeManager();

  useImperativeHandle(ref, () => iframe.current);

  const onLoad = useCallback(() => {
    if (!iframe.current) return;
    setTimeout(
      () =>
        iframe.current?.contentWindow?.postMessage({ type: 'prColorSchemeUpdate', scheme }, '*'),
      100,
    );
  }, [scheme]);

  useEffect(() => {
    onLoad();
  }, [onLoad]);

  return (
    <iframe
      ref={iframe}
      {...props}
      onLoad={(e) => {
        onLoad();
        props.onLoad?.(e);
      }}
    />
  );
});

export default IFrame;
