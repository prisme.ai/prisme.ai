import { createContext, ReactNode, useCallback, useContext, useEffect, useState } from 'react';
import { Workspace } from '@prisme.ai/sdk';
import useApi from '@/utils/api/useApi';

interface Source {
  name: Workspace['name'];
  description?: Workspace['description'];
  photo?: string;
}

const sourceDetailsContext = createContext<Source>({
  name: '',
});
export const useSourceDetails = () => useContext(sourceDetailsContext);

interface SourceDetailsProps {
  children: ReactNode;
  workspaceId?: string;
  appSlug?: string;
}

const sourceCache = new Map<string, Promise<Source> | undefined>();

export const SourceDetails = ({ workspaceId, appSlug, children }: SourceDetailsProps) => {
  const api = useApi();
  const [details, setDetails] = useState<Source>();

  const fetchWorkspaceDetails = useCallback(
    async (workspaceId: string): Promise<Source> => {
      const { name, description, photo } = (await api.getWorkspace(workspaceId)) || { name: '' };
      return { name, description, photo };
    },
    [api],
  );
  const fetchAppDetails = useCallback(
    async (appSlug?: string): Promise<Source> => {
      const {
        name = '',
        description = '',
        photo = '',
      } = (appSlug && (await api.getApp({ slug: appSlug }))) || {};
      return { name, description, photo };
    },
    [api],
  );

  useEffect(() => {
    if (!workspaceId) return;
    const key = appSlug || workspaceId;
    if (!sourceCache.has(key)) {
      if (appSlug) {
        sourceCache.set(key, fetchAppDetails(appSlug));
      } else {
        sourceCache.set(key, fetchWorkspaceDetails(workspaceId));
      }
    }

    const fetchDetails = async () => {
      const details = await sourceCache.get(key);
      details && setDetails(details);
    };
    fetchDetails();
  }, [appSlug, workspaceId, fetchAppDetails, fetchWorkspaceDetails]);

  return (
    <sourceDetailsContext.Provider value={details || ({} as Source)}>
      {children}
    </sourceDetailsContext.Provider>
  );
};

export default SourceDetails;
