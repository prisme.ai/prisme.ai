import { useCallback } from 'react';
import useLocalizedText from '@/utils/useLocalizedText';
import { useWorkspace } from '@/providers/Workspace';
import { Button, notification } from 'antd';
import Loading from '@/components/Loading';
import SchemaForm, { Schema } from '@/components/SchemaForm';
import { BlockLoader } from '@/blocks/BlockLoader';
import useApi from '@/utils/api/useApi';
import styles from './app-editor.module.scss';
import useTranslations from '@/i18n/useTranslations';

interface AppEditorProps {
  schema?: Schema;
  block?: string;
  appId: string;
  config?: Record<string, unknown>;
  onSave: (config: AppEditorProps['config']) => void;
}

const AppEditor = ({ schema, block, appId, config = {}, onSave }: AppEditorProps) => {
  const api = useApi();
  const {
    workspace: { id: workspaceId },
  } = useWorkspace();
  const t = useTranslations('workspaces');
  const { localizeSchemaForm } = useLocalizedText();

  const onSubmit = async (value: any) => {
    try {
      await onSave(value);
    } catch (e) {
      notification.error({
        message: t('unknown', { errorName: (e as Error).name, ns: 'errors' }),
        placement: 'bottomRight',
      });
    }
  };

  const onChange = useCallback(
    (value: any) => {
      const keys = Object.keys(value || {});
      const prevValue = keys.reduce(
        (prev, key) => ({
          ...prev,
          [key]: config[key],
        }),
        {},
      );
      if (JSON.stringify(prevValue) === JSON.stringify(value || {})) return;
    },
    [config],
  );

  if (!config) return <Loading />;

  if (schema) {
    const s: Schema = {
      type: 'object',
      properties: localizeSchemaForm(schema) as Schema['properties'],
    };

    return (
      <div className={styles['container']}>
        <SchemaForm
          schema={s}
          onChange={onChange}
          onSubmit={onSubmit}
          initialValues={config}
          buttons={[
            <Button htmlType="submit" type="primary" key="submit">
              {t('save', { ns: 'common' })}
            </Button>,
          ]}
        />
      </div>
    );
  }
  if (block) {
    return (
      <BlockLoader
        api={api}
        url={block}
        token={`${api.token}`}
        workspaceId={workspaceId}
        appInstance={appId}
        config={{}}
        appConfig={config}
        onAppConfigUpdate={onSave}
      />
    );
  }

  return null;
};

export default AppEditor;
