import { ReactNode, useEffect, useRef, useState } from 'react';
import PlusIcon from '@/svgs/plus.svg';
import { Dropdown, Popover } from 'antd';
import { paste } from '@/utils/copy';
import { Block } from '@/providers/Block';
import { useTranslations } from 'next-intl';
import styles from './blocks-list-editor.module.scss';
import BlockPicker from './BlockPicker';

interface AddBlockProps {
  onClick: (block?: Block) => void;
  children: ReactNode;
}
const AddBlock = ({ onClick, children }: AddBlockProps) => {
  const t = useTranslations('workspaces');
  const labelRef = useRef<HTMLButtonElement>(null);
  const [maxWidth, setMaxWidth] = useState(0);
  const [width, setWidth] = useState('auto');
  const [open, setOpen] = useState(false);

  useEffect(() => {
    if (!labelRef.current) return;
    const { width } = labelRef.current.getBoundingClientRect();
    setMaxWidth(+width.toFixed());
    setWidth('26px');
  }, []);

  return (
    <div className={styles['add-button-container']}>
      <div
        className={styles['add-button-decoration']}
        style={{
          width: `${maxWidth}px`,
          marginLeft: `-${maxWidth / 2}px`,
        }}
      />
      <Popover
        content={
          <BlockPicker
            onAdd={(block) => {
              onClick(block);
              setOpen(false);
            }}
          />
        }
        trigger={'click'}
        open={open}
        onOpenChange={(o) => {
          setOpen(o);
        }}
      >
        <button
          ref={labelRef}
          className={styles['add-button']}
          style={{
            width,
            boxShadow: '0 0 0px 10px inherit',
          }}
          onMouseEnter={() => setWidth(`${maxWidth}px`)}
          onMouseLeave={() => setWidth(`${26}px`)}
          type="button"
        >
          <Dropdown
            dropdownRender={(children) => (
              <div onClick={(e) => e.stopPropagation()}>{children}</div>
            )}
            menu={{
              items: [
                {
                  key: '1',
                  label: t('blocks.builder.add.label'),
                  onClick: () => setOpen(true),
                },
                {
                  key: '2',
                  label: t('blocks.builder.paste.label'),
                  onClick: async () => {
                    try {
                      const block = JSON.parse(await paste());
                      if (!block.slug) return;
                      onClick(block);
                    } catch {}
                  },
                },
              ],
            }}
          >
            <PlusIcon className={styles['add-button-icon']} />
          </Dropdown>
          <span className={styles['add-button-content']}>{children}</span>
        </button>
      </Popover>
    </div>
  );
};

export default AddBlock;
