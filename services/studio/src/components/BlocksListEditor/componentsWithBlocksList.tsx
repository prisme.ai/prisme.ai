import components from '@/components/SchemaFormInWorkspace/schemaFormComponents';
import FieldBlocksList from './FieldBlocksList';

export const componentsWithBlocksList = {
  ...components,
  UiWidgets: { ...components.UiWidgets, BlocksList: FieldBlocksList },
};

export default componentsWithBlocksList;
