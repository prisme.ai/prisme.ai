import SchemaForm from '@/components/SchemaForm';
import BlocksListEditorProvider from './BlocksListEditorProvider';
import componentsWithBlocksList from './componentsWithBlocksList';
import { Schema } from '../SchemaForm';
import { Block } from '@/providers/Block';
import styles from './blocks-list-editor.module.scss';

const schema: Schema = {
  type: 'object',
  properties: {
    blocks: {
      'ui:widget': 'BlocksList',
    },
  },
};

interface BlocksListEditorProps {
  value: Block;
  onChange?: (b: Prismeai.Block) => void;
}

export const BlocksListEditor = ({ value, onChange }: BlocksListEditorProps) => {
  return (
    <BlocksListEditorProvider>
      <div className={styles['blocks-list-editor']}>
        <SchemaForm
          schema={schema}
          initialValues={value}
          components={componentsWithBlocksList}
          onChange={onChange}
          buttons={[]}
          className={styles['block-list-editor']}
        />
      </div>
    </BlocksListEditorProvider>
  );
};

export default BlocksListEditor;
