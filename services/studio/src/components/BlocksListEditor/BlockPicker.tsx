import { InputRef, Tooltip } from 'antd';
import { ReactNode, useEffect, useMemo, useRef, useState } from 'react';
import useLocalizedText from '@/utils/useLocalizedText';
import { BlockInCatalog } from '../PageBuilder/useBlocks';
import { useTranslations } from 'next-intl';
import { useBlocksListEditor } from './BlocksListEditorProvider';
import { search } from '@/utils/filters';
import SearchInput from '@/components/SearchInput';
import styles from './blocks-list-editor.module.scss';
import LogoIcon from '@/svgs/logo.svg';
import { useWorkspace } from '@/providers/Workspace';

interface BlockPickerProps {
  onAdd: (block: BlockInCatalog) => void;
}

interface BlocksGroup {
  name: string;
  icon?: ReactNode;
  blocks: BlockInCatalog[];
}

export const BlockPicker = ({ onAdd }: BlockPickerProps) => {
  const { blocks } = useBlocksListEditor();
  const {
    workspace: { photo },
  } = useWorkspace();
  const t = useTranslations('workspaces');
  const { localize } = useLocalizedText();

  const [filter, setFilter] = useState('');
  const ref = useRef<InputRef>(null);

  const filtered = useMemo(() => {
    const blocksByGroup = blocks.reduce<BlocksGroup[]>((prev, { from, builtIn, ...block }) => {
      const groupName = builtIn
        ? t('blocks.builder.picker.builtIn')
        : from
          ? t('blocks.builder.picker.workspace', { from: localize(from) })
          : block.slug.split(/\./)[0] || '';
      const group =
        prev.find(({ name }) => name === groupName) ||
        (() => {
          const group = {
            name: localize(groupName),
            icon: builtIn ? <LogoIcon /> : <img src={block.icon || photo} alt={groupName} />,
            blocks: [] as BlockInCatalog[],
          };
          prev.push(group);
          return group;
        })();
      group.blocks.push(block);
      return prev;
    }, []);
    if (!filter) return blocksByGroup;
    const searchFilter = search(filter);
    return blocksByGroup.flatMap((group) => {
      const { name, blocks } = group;
      if (
        !searchFilter(name) &&
        !blocks.some(({ slug, name, description }) =>
          searchFilter(`${slug} ${name} ${description}`),
        )
      )
        return [];
      const filtered = blocks.filter(({ slug, name, description }) =>
        searchFilter(`${slug} ${name} ${description}`),
      );
      return {
        ...group,
        blocks: filtered.length === 0 ? blocks : filtered,
      };
    });
  }, [filter, blocks, localize, photo, t]);

  useEffect(() => {
    ref.current?.focus();
  });

  function getDescription(block: BlockInCatalog) {
    if (block.description) return localize(block.description);
    if (t.has(`pages.blocks.description_${block.slug}`)) {
      return t.raw(`pages.blocks.description_${block.slug}`);
    }
    return block.slug;
  }

  return (
    <div className={styles['blocks-list-ctn']}>
      <div>{t('blocks.builder.picker.title')}</div>
      <SearchInput
        className={styles['blocks-search']}
        value={filter}
        onChange={({ target: { value } }) => setFilter(value)}
        ref={ref}
      />
      <div className={styles['blocks-list']}>
        {filtered.map(({ name, icon, blocks }) => (
          <div key={name} className={styles['blocks-list-group']}>
            <div className={styles['blocks-list-group-header']}>
              <div className={styles['blocks-list-group-header-icon']}>{icon}</div>
              <div className={styles['blocks-list-group-header-title']}>{name}</div>
            </div>
            <div className={styles['blocks-list-blocks']}>
              {blocks.map((block) => (
                <Tooltip
                  title={
                    <div>
                      <div>{getDescription(block)}</div>
                      {block.photo && (
                        <div className={styles['blocks-list-block-photo']}>
                          <img src={block.photo} alt={block.slug} />
                        </div>
                      )}
                    </div>
                  }
                  key={block.slug}
                  placement="right"
                >
                  <button
                    className={styles['blocks-list-block']}
                    onClick={() => {
                      onAdd(block);
                    }}
                  >
                    <div className={styles['blocks-list-block-name']}>{localize(block.name)}</div>
                    <div className={styles['blocks-list-block-description']}>
                      {getDescription(block)}
                    </div>
                  </button>
                </Tooltip>
              ))}
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default BlockPicker;
