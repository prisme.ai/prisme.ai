import { useTranslations } from 'next-intl';
import { FieldProps } from '../SchemaForm';
import { useFieldArray } from 'react-final-form-arrays';
import { useCallback, useState } from 'react';
import copy from '@/utils/copy';
import AddBlock from './AddBlock';
import { Field } from 'react-final-form';
import BlockForm from './BlockForm';
import { Dropdown } from 'antd';
import ThreeDotsIcon from '@/svgs/three-dots.svg';
import { Block } from '@/providers/Block';
import styles from './blocks-list-editor.module.scss';

export default function BlocksList({ name }: FieldProps) {
  const t = useTranslations('workspaces');
  const { fields } = useFieldArray(name);
  const [mounted, setMounted] = useState(true);
  const reallyAdd = useCallback(
    async (index: number, block: Block) => {
      setMounted(false);
      await fields.insert(index, { slug: block.slug });
      setMounted(true);
    },
    [fields],
  );

  const removeBlock = useCallback(
    (block: Block) => async () => {
      setMounted(false);
      const pos = fields.value.indexOf(block);
      await fields.remove(pos);
      setMounted(true);
    },
    [fields],
  );

  const getMenuItems = useCallback(
    (value: any) => [
      {
        key: 'copy',
        label: t('blocks.builder.copy.label'),
        onClick: () => {
          copy(JSON.stringify(value));
        },
      },
      {
        key: 'cut',
        label: t('blocks.builder.cut.label'),
        onClick: () => {
          copy(JSON.stringify(value));
          removeBlock(value)();
        },
      },
    ],
    [removeBlock, t],
  );

  return (
    <div className={styles['field']}>
      <AddBlock onClick={(block) => block && reallyAdd(0, block)}>
        {t('blocks.builder.add.label')}
      </AddBlock>

      {mounted &&
        fields.map((name, k) => (
          <Field name={name} key={name}>
            {({ input: { value, name: itemName } }) => (
              <div key={itemName}>
                <div className={styles['field-block-form-container']}>
                  <BlockForm
                    key={itemName}
                    name={itemName}
                    onRemove={removeBlock(value)}
                    className="flex-1"
                  />
                  <Dropdown
                    trigger={['click']}
                    dropdownRender={(children) => (
                      <div onClick={(e) => e.stopPropagation()}>{children}</div>
                    )}
                    menu={{
                      items: getMenuItems(value),
                    }}
                  >
                    <button className={styles['field-options-button']}>
                      <ThreeDotsIcon />
                    </button>
                  </Dropdown>
                </div>
                <AddBlock
                  onClick={(block) => {
                    block && reallyAdd(k + 1, block);
                  }}
                >
                  {t('blocks.builder.add.label')}
                </AddBlock>
              </div>
            )}
          </Field>
        ))}
    </div>
  );
}
