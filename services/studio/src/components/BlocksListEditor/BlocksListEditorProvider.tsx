import { createContext, ReactNode, useCallback, useContext, useState } from 'react';
import getEditSchema from '../PageBuilder/Panel/EditSchema/getEditSchema';
import useBlocks, { BlockInCatalog } from '../PageBuilder/useBlocks';
import { extendsSchema } from './extendsSchema';
import { Schema } from '@/components/SchemaForm';
import { BlockComponent } from '@/blocks/BlockLoader';
import { loadModule } from '@/utils/useExternalModule';

interface BlocksListEditorContext {
  blocks: BlockInCatalog[];
  schemas: Map<string, Schema>;
  getSchema: (slug: string) => Promise<Schema | undefined>;
  getModule: (slug: string) => Promise<BlockComponent<any> | undefined>;
}

function isSchema(schema: Schema | Prismeai.TypedArgument): schema is Schema {
  return !!schema.type;
}

export const blockSelectorContext = createContext<BlocksListEditorContext | undefined>(undefined);

export const useBlocksListEditor = () => {
  const context = useContext(blockSelectorContext);
  if (!context) {
    throw new Error();
  }
  return context;
};

interface BlocksListEditorProviderProps {
  children: ReactNode;
}

const SCHEMAS = new Map<string, Schema>();
const MODULES = new Map<string, Promise<BlockComponent<any> | undefined>>();
const CACHE = new Map<string, Schema | null>();

async function fetchModule(url: string) {
  if (!MODULES.get(url)) {
    MODULES.set(url, loadModule<BlockComponent>(url));
  }
  return await MODULES.get(url);
}

export const BlocksListEditorProvider = ({ children }: BlocksListEditorProviderProps) => {
  const { variants: blocks } = useBlocks();
  const [schemas, setSchemas] = useState(SCHEMAS);

  const fetchSchema = useCallback(
    async (slug: string) => {
      if (!slug) return null;
      const inCatalog = blocks.find(({ slug: bslug }) => slug === bslug);

      if (!inCatalog) return;

      if (inCatalog.builtIn) {
        const schema = getEditSchema(slug);
        if (!schema) return;
        SCHEMAS.set(slug, schema);
        setSchemas(SCHEMAS);
        return schema;
      }
      if (inCatalog.url) {
        if (!CACHE.has(inCatalog.url)) {
          const module = await fetchModule(inCatalog.url);
          if (module && module.schema) {
            SCHEMAS.set(slug, module.schema || null);
            setSchemas(SCHEMAS);
            CACHE.set(inCatalog.url, module.schema);
          }
        }
        return CACHE.get(inCatalog.url);
      }
      if (inCatalog.schema) {
        SCHEMAS.set(slug, inCatalog.schema as Schema);
        setSchemas(SCHEMAS);
        return inCatalog.schema;
      }
    },
    [blocks],
  );

  const getModule = useCallback(
    async (slug: string) => {
      const inCatalog = blocks.find(({ slug: bslug }) => slug === bslug);
      if (!inCatalog?.url) return;
      const module = await fetchModule(inCatalog.url);
      return module;
    },
    [blocks],
  );

  const getSchema = useCallback(
    async (slug: string): Promise<Schema | undefined> => {
      async function getSchema(slug: string) {
        const schema = (await fetchSchema(slug)) || undefined;
        if (!schema) return schema;
        return isSchema(schema) ? schema : ({ type: 'object', properties: schema } as Schema);
      }
      const schema = await getSchema(slug);
      if (!schema) return schema;
      return extendsSchema(
        isSchema(schema) ? schema : ({ type: 'object', properties: schema } as Schema),
        getSchema,
      );
    },
    [fetchSchema],
  );

  return (
    <blockSelectorContext.Provider
      value={{
        blocks,
        schemas,
        getSchema,
        getModule,
      }}
    >
      {children}
    </blockSelectorContext.Provider>
  );
};

export default BlocksListEditorProvider;
