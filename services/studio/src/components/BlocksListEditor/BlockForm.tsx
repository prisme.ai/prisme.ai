import { BlockComponent } from '@/blocks/BlockLoader';
import { Collapse, Tabs, TabsProps, Tooltip } from 'antd';
import { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { useField } from 'react-final-form';
import useLocalizedText from '@/utils/useLocalizedText';
import ConfirmButton from '@/components/ConfirmButton';
import SchemaForm, { FieldProps, Schema, schemaFormUtils } from '@/components/SchemaForm';
import { useBlocksListEditor } from './BlocksListEditorProvider';
import { CollapseProps } from 'antd/lib';
import { CodeEditor } from '../CodeEditor/lazy';
import { useTranslations } from 'next-intl';
import Loading from '../Loading';
import TrashIcon from '@/svgs/trash.svg';
import InfoIcon from '@/svgs/info.svg';
import componentsWithBlocksList from './componentsWithBlocksList';
import styles from './blocks-list-editor.module.scss';
import useExtractSelectOptions from '@/components/SchemaFormInWorkspace/useExtractSelectOptions';
import useExtractAutocompleteOptions from '@/components/SchemaFormInWorkspace/useExtractAutocompleteOptions';

interface SchemaFormProps extends Partial<CollapseProps> {
  name: string;
  onRemove: () => void;
}

const getCSSEditorField = (currentStyles: string) =>
  function CSSEditorField(props: FieldProps) {
    const t = useTranslations('workspaces');
    const field = useField(props.name, {
      value: currentStyles,
    });

    return (
      <div>
        <div className={styles['css-editor']}>
          <div>
            <label className={styles['css-editor-label']}>
              {t('pages.blocks.settings.css.label')}
            </label>
            <Tooltip title={t('pages.blocks.settings.css.description')} placement="right">
              <button type="button" className={styles['css-editor-info']}>
                <InfoIcon height="1.4rem" width="1.4rem" />
              </button>
            </Tooltip>
          </div>
          <Tooltip title={t('pages.blocks.settings.css.reset')} placement="left">
            <button
              type="button"
              className={styles['css-editor-delete-btn']}
              onClick={(event) => {
                event.stopPropagation();
                field.input.onChange(currentStyles);
              }}
            >
              <TrashIcon />
            </button>
          </Tooltip>
        </div>
        <div className={styles['css-editor-content']}>
          <CodeEditor mode="css" {...field.input} {...props} inline />
        </div>
      </div>
    );
  };

const defaultStyles = `:block {
  
}`;

export const BlockForm = ({ name, onRemove, ...props }: SchemaFormProps) => {
  const t = useTranslations('workspaces');
  const { localizeSchemaForm } = useLocalizedText();
  const { getSchema, getModule } = useBlocksListEditor();
  const [schema, setSchema] = useState<Schema | 'loading' | null>(null);
  const [Preview, setPreview] = useState<BlockComponent['Preview'] | null>(null);
  const field = useField(name);

  useEffect(() => {
    const loadSchema = async () => {
      setSchema('loading');
      const schema = await getSchema(field.input.value.slug);
      setSchema(schema || null);
    };
    loadSchema();
  }, [field.input.value.slug, getSchema]);

  useEffect(() => {
    async function loadPreview() {
      const module = await getModule(field.input.value.slug);
      setPreview(() => module?.Preview || null);
    }
    loadPreview();
  }, [field.input.value.slug, getModule]);

  const lifecycleSchema: Schema = useMemo(
    () =>
      localizeSchemaForm({
        type: 'object',
        properties: {
          onInit: {
            type: 'string',
            title: 'pages.blocks.settings.onInit.label',
            description: 'pages.blocks.settings.onInit.description',
            'ui:widget': 'autocomplete',
            'ui:options': {
              autocomplete: 'events:listen',
            },
          },
          automation: {
            type: 'string',
            title: 'pages.blocks.settings.automation.label',
            description: 'pages.blocks.settings.automation.description',
            'ui:widget': 'select',
            'ui:options': {
              from: 'automations',
              filter: 'endpoint',
            },
          },
          updateOn: {
            type: 'string',
            title: 'pages.blocks.settings.updateOn.label',
            description: 'pages.blocks.settings.updateOn.description',
            'ui:widget': 'autocomplete',
            'ui:options': {
              autocomplete: 'events:emit',
            },
          },
        },
      }),
    [localizeSchemaForm],
  );

  const styleSchema: Schema = useMemo(
    () =>
      localizeSchemaForm({
        type: 'object',
        properties: {
          sectionId: {
            type: 'string',
            title: 'pages.blocks.settings.sectionId.label',
            description: 'pages.blocks.settings.sectionId.description',
          },
          className: {
            type: 'string',
            title: 'pages.blocks.settings.className.label',
            description: 'pages.blocks.settings.className.description',
          },
          css: {
            type: 'string',
            'ui:widget': getCSSEditorField(defaultStyles),
            defaut: defaultStyles,
          },
        },
      }),
    [localizeSchemaForm],
  );

  const logicalSchema: Schema = useMemo(
    () =>
      localizeSchemaForm({
        type: 'object',
        properties: {
          template_if: {
            type: 'string',
            title: 'pages.blocks.settings.logical.if.label',
            description: 'pages.blocks.settings.logical.if.description',
          },
          template_repeat: {
            type: 'object',
            title: 'pages.blocks.settings.logical.repeat.label',
            description: 'pages.blocks.settings.logical.repeat.description',
            properties: {
              on: {
                type: 'string',
                title: 'pages.blocks.settings.logical.repeat.on.label',
                description: 'pages.blocks.settings.logical.repeat.on.description',
              },
              as: {
                type: 'string',
                title: 'pages.blocks.settings.logical.repeat.as.label',
                description: 'pages.blocks.settings.logical.repeat.as.description',
              },
            },
          },
        },
      }),
    [localizeSchemaForm],
  );
  const throttled = useRef<NodeJS.Timeout>();
  const onChange = useCallback(
    (schema: Schema) => (value: any) => {
      if (throttled.current) {
        clearTimeout(throttled.current);
      }
      throttled.current = setTimeout(() => {
        let values: any = {};
        if (schema.properties) {
          const properties = schemaFormUtils.getProperties(schema, value);
          values = properties.reduce((prev, k) => {
            return {
              ...prev,
              [k]: value[k],
            };
          }, {});

          if (schema.additionalProperties) {
            const additionnalProperties = Object.keys(value).filter((k) => !properties.includes(k));
            additionnalProperties.forEach((k) => {
              values[k] = value[k];
            });
          }
        } else {
          values = value;
        }
        field.input.onChange({
          ...field.input.value,
          ...values,
          slug: field.input.value.slug,
        });
      }, 200);
    },
    [field.input],
  );

  const { slug, ...previewConfig } = field.input.value;
  void slug;

  const utils = {
    extractAutocompleteOptions: useExtractAutocompleteOptions(),
    extractSelectOptions: useExtractSelectOptions(),
  };

  return (
    <Collapse
      {...props}
      items={[
        {
          label: (
            <div className={styles['block-form-title']}>
              <div>{field.input.value.slug}</div>
              <div className={styles['block-form-id']}>
                {field.input.value.sectionId ? `#${field.input.value.sectionId}` : ''}
              </div>
            </div>
          ),
          children: (
            <Tabs
              items={
                [
                  Preview && {
                    key: 'preview',
                    label: t('blocks.builder.preview.label'),
                    children: <Preview config={previewConfig} />,
                  },
                  schema && {
                    key: 'config',
                    label: t('blocks.builder.setup.label'),
                    children: (
                      <>
                        {schema === 'loading' && <Loading />}
                        {schema !== 'loading' && (
                          <SchemaForm
                            schema={localizeSchemaForm(schema)}
                            buttons={[]}
                            initialValues={field.input.value}
                            onChange={onChange(schema)}
                            components={componentsWithBlocksList}
                            utils={utils}
                          />
                        )}
                      </>
                    ),
                  },
                  {
                    key: 'lifecycle',
                    label: t('blocks.builder.lifecycle.label'),
                    children: (
                      <SchemaForm
                        schema={lifecycleSchema}
                        buttons={[]}
                        initialValues={field.input.value}
                        onChange={onChange(lifecycleSchema)}
                        utils={utils}
                      />
                    ),
                  },
                  {
                    key: 'logical',
                    label: t('blocks.builder.logical.label'),
                    children: (
                      <SchemaForm
                        schema={logicalSchema}
                        buttons={[]}
                        initialValues={field.input.value}
                        onChange={onChange(logicalSchema)}
                      />
                    ),
                  },
                  {
                    key: 'style',
                    label: t('blocks.builder.style.label'),
                    children: (
                      <SchemaForm
                        schema={styleSchema}
                        buttons={[]}
                        initialValues={field.input.value}
                        onChange={onChange(styleSchema)}
                      />
                    ),
                  },
                ].filter(Boolean) as TabsProps['items']
              }
              tabBarExtraContent={
                <ConfirmButton
                  onConfirm={onRemove}
                  confirmLabel={t('blocks.builder.delete.confirm')}
                >
                  {t('blocks.builder.delete.label')}
                </ConfirmButton>
              }
            />
          ),
        },
      ]}
    />
  );
};

export default BlockForm;
