import getEditSchema from '../PageBuilder/Panel/EditSchema/getEditSchema';
import { BlockInCatalog } from '@/components/PageBuilder/useBlocks';
import { Schema } from '@/components/SchemaForm';
import { loadModule } from '@/utils/useExternalModule';

const SCHEMAS = new Map<string, Promise<Schema | {}>>();

export default async function getSchema({
  slug,
  blocks,
  workspaceId,
}: {
  slug: string;
  blocks: BlockInCatalog[];
  workspaceId: string;
}): Promise<Schema> {
  if (!slug) return Promise.resolve({});

  const key = `${workspaceId}--${slug}`;

  if (SCHEMAS.has(key)) return SCHEMAS.get(key);

  const inCatalog = blocks.find(({ slug: bslug }) => slug === bslug);

  if (!inCatalog) {
    SCHEMAS.set(key, Promise.resolve({}));
    return Promise.resolve({});
  }

  if (inCatalog.builtIn) {
    const schema = getEditSchema(slug);
    SCHEMAS.set(key, Promise.resolve(schema || {}));
    return Promise.resolve(schema || {});
  }
  if (inCatalog.schema) {
    SCHEMAS.set(key, Promise.resolve(inCatalog.schema as Schema));
    return Promise.resolve(
      inCatalog.schema
        ? ({
            type: 'object',
            properties: inCatalog.schema,
          } as Schema)
        : {},
    );
  }
  if (inCatalog.url) {
    const { url } = inCatalog;
    const promise = new Promise<Schema | {}>(async (resolve) => {
      try {
        const module = await loadModule(url, { timeout: 500 });
        if (module && module.schema) {
          return resolve(module.schema as Schema);
        }
      } catch {}
      resolve({});
    });
    SCHEMAS.set(key, promise);
    return promise;
  }

  SCHEMAS.set(key, Promise.resolve({}));
  return Promise.resolve({});
}
