import { ReactNode } from 'react';
import styles from './low-code.module.scss';
import EditableTitle from './EditableTitle';
import useLocalizedText from '@/utils/useLocalizedText';
import { Icon } from '@/blocks/blocks/Icon';

interface Header {
  title?: {
    value: Prismeai.LocalizedText;
    icon?: string | ReactNode;
    onChange?: (v: Prismeai.LocalizedText) => void;
  };
  left?: ReactNode;
  right?: ReactNode;
}

interface LowCodeEditorProps {
  header: ReactNode | Header;
  children: ReactNode;
}

function isHeader(header: LowCodeEditorProps['header']): header is Header {
  const { title, left, right } = header as Header;
  return !!(title || left || right);
}

export default function LowCodeEditor({ header, children }: LowCodeEditorProps) {
  const { localize } = useLocalizedText();
  return (
    <div className={styles['low-code-ctn']}>
      {isHeader(header) ? (
        <div className={styles['low-code-header']}>
          {header.title &&
            (header.title.onChange ? (
              <EditableTitle
                className={styles['low-code-header-title']}
                value={header.title.value}
                onChange={header.title.onChange}
              />
            ) : (
              <div className={styles['low-code-header-title']}>
                {header.title.icon && (
                  <Icon icon={header.title.icon} className={styles['low-code-header-title-icon']} />
                )}
                {localize(header.title.value)}
              </div>
            ))}
          {header.left && <div className={styles['low-code-header-left']}>{header.left}</div>}
          {header.right && <div className={styles['low-code-header-right']}>{header.right}</div>}
        </div>
      ) : (
        header
      )}

      <div className={styles['low-code-content']}>{children}</div>
    </div>
  );
}
