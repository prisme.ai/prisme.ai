import { forwardRef, HTMLAttributes } from 'react';
import LocalizedInput from '@/components/LocalizedInput';
import useContentEditable from '@/utils/useContentEditable';
import { useLocale } from 'next-intl';
import styles from './low-code.module.scss';

interface EditableTitleProps extends Omit<HTMLAttributes<HTMLInputElement>, 'onChange'> {
  value: Prismeai.LocalizedText;
  onChange: (v: Prismeai.LocalizedText) => void;
  onEnter?: (v: Prismeai.LocalizedText) => void;
}

const Input = forwardRef<any, any>(function Input(
  { value, onChange, onBlur, onEnter, ...props },
  ref,
) {
  const contentEditable = useContentEditable({
    value,
    onChange,
    onEnter,
  });

  return (
    <span
      {...contentEditable}
      onBlur={(e) => {
        e.preventDefault();
        contentEditable.onBlur(e);
        onBlur?.();
      }}
      onKeyDown={(e) => {
        contentEditable.onKeyDown(e);
      }}
      ref={ref}
      {...props}
      className={`${styles['low-code-editable-input']} ${props.className || ''}`}
    >
      {contentEditable.value}
    </span>
  );
});

export const EditableTitle = ({
  value,
  onChange,
  onEnter,
  className = '',
  ...props
}: EditableTitleProps) => {
  const language = useLocale();
  return (
    <LocalizedInput
      value={value}
      onChange={onChange}
      Input={Input}
      InputProps={{ onEnter, ...props }}
      initialLang={language}
      className={`${styles['low-code-editable-input-ctn']} ${className}`}
    />
  );
};

export default EditableTitle;
