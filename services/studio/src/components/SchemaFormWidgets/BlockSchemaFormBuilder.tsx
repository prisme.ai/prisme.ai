import { Tooltip } from 'antd';
import { useCallback, useEffect, useState } from 'react';
import { useField, useForm } from 'react-final-form';
import { get } from 'lodash';
import { FieldComponent } from '@/components/SchemaForm/context';
import { InfoBubble, Schema, schemaFormUtils } from '@/components/SchemaForm';
import ArgumentsEditor from '@/components/SchemaFormBuilder/ArgumentsEditor';
import useTranslations from '@/i18n/useTranslations';
import SchemaFormBuilder from '@/components/SchemaFormBuilder';

export const BlockSchemaFormBuilder: FieldComponent = ({ schema, name, className, label }) => {
  const t = useTranslations('pages');
  const field = useField(name);
  const form = useForm();
  const [formValues, setFormValues] = useState({});
  // const { 'schema-form-builder': { arguments: asArgumentsBuilder = false } = {} } = (schema[
  //   'ui:options'
  // ] || {}) as {
  //   'schema-form-builder': {
  //     arguments: Schema;
  //   };
  // };

  useEffect(() => {
    form.subscribe(
      ({ values }) => {
        setFormValues(get(values, name));
      },
      {
        values: true,
      },
    );
  }, [form, name]);

  const hasError = schemaFormUtils.getError(field.meta);
  const _label = label || schema.title || schemaFormUtils.getLabel(field.input.name, schema.title);
  const children =
    label || schema.title || schemaFormUtils.getLabel(field.input.name, schema.title);

  const onChange = useCallback(
    (v: Schema) => {
      field.input.onChange(v);
    },
    [field.input],
  );

  if (ArgumentsEditor) {
    return (
      <ArgumentsEditor
        name={name}
        title={typeof children === 'string' ? children : ''}
        description={typeof schema.description === 'string' ? schema.description : ''}
        add={t('schemaForm.builder.property.add', { ns: 'common' })}
        schema={schema}
      />
    );
  }

  return (
    <div className="pr-form-block">
      {_label && _label !== '' && (
        <label
          className={`${className} pr-form-block__label pr-form-label`}
          htmlFor={field.input.name}
          dangerouslySetInnerHTML={typeof children === 'string' ? { __html: children } : undefined}
        >
          {typeof children === 'object' ? children : undefined}
        </label>
      )}
      <Tooltip title={hasError} rootClassName="pr-form-error">
        <SchemaFormBuilder onChange={onChange} value={formValues} />
      </Tooltip>
      <InfoBubble className="pr-form-block__description" text={schema.description} />
    </div>
  );
};

export default BlockSchemaFormBuilder;
