import { Tooltip } from 'antd';
import { useEffect, useState } from 'react';
import { useField, useForm } from 'react-final-form';
import { FieldComponent } from '../SchemaForm/context';
import { useBlock } from '@/blocks/Provider';
import BlockLoader, {
  useRecursiveConfigContext,
  recursiveConfigContext,
} from '../PageContent/BlockLoader';
import { getError, getLabel } from '../SchemaForm/utils';
import InfoBubble from '../SchemaForm/InfoBubble';

interface BlockWidgetOptions {
  slug: string;
  onChange: string;
}

export const BlockWidget: FieldComponent = ({ schema, name, className, label }) => {
  const { ['ui:options']: { block: { slug, ...config } = {} as BlockWidgetOptions } = {} } =
    schema as {
      ['ui:options']: {
        block: BlockWidgetOptions;
      };
    };
  const { events } = useBlock();
  const field = useField(name);
  const form = useForm();
  const [formValues, setFormValues] = useState({});
  const recursive = useRecursiveConfigContext();

  useEffect(() => {
    // Values are extravcted to be injected in Block inherited config as {{formValues}}
    form.subscribe(
      ({ values }) => {
        setFormValues(values.values);
      },
      {
        values: true,
      },
    );
  }, [form]);

  useEffect(() => {
    if (!config.onChange || !events) return;
    const off = events.on(config.onChange, ({ payload }) => {
      field.input.onChange(payload);
    });
    return () => {
      off();
    };
  }, [config.onChange, events, field.input]);

  const hasError = getError(field.meta);
  const _label = label || schema.title || getLabel(field.input.name, schema.title);
  const children = label || schema.title || getLabel(field.input.name, schema.title);

  return (
    <div className="pr-form-block">
      {_label && _label !== '' && (
        <label
          className={`${className} pr-form-block__label pr-form-label`}
          htmlFor={field.input.name}
          dangerouslySetInnerHTML={typeof children === 'string' ? { __html: children } : undefined}
        >
          {typeof children === 'object' ? children : undefined}
        </label>
      )}
      <Tooltip title={hasError} rootClassName="pr-form-error">
        <recursiveConfigContext.Provider value={{ ...recursive, formValues }}>
          <BlockLoader name={slug} config={config} />
        </recursiveConfigContext.Provider>
      </Tooltip>
      <InfoBubble className="pr-form-block__description" text={schema.description} />
    </div>
  );
};

export default BlockWidget;
