'use client';

import { ReactNode, useEffect } from 'react';

export default function Head({
  title = '',
  description = '',
  children,
}: {
  title: string;
  description: string;
  children?: ReactNode;
}) {
  useEffect(() => {
    document.title = title;
  }, [title]);
  return (
    <>
      <meta name="description" content={description} key="description" />
      {children}
    </>
  );
}
