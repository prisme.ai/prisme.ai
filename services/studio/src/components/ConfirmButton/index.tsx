import { Button, Tooltip, TooltipProps } from 'antd';
import { ButtonProps } from 'antd/lib';
import { useTranslations } from 'next-intl';
import { CSSProperties, ReactNode, useEffect, useRef, useState } from 'react';
import styles from './confirm-button.module.scss';

interface ConfirmButtonProps extends Omit<ButtonProps, 'color'> {
  children: ReactNode;
  onConfirm: () => void;
  confirmLabel?: string;
  yesLabel?: string;
  noLabel?: string;
  placement?: TooltipProps['placement'];
  ButtonComponent?: string | React.ComponentType<ButtonProps>;
  color?: string;
}
export const ConfirmButton = ({
  children,
  onConfirm,
  confirmLabel,
  yesLabel,
  noLabel,
  placement,
  ButtonComponent = Button,
  color = 'var(--pr-warning-color)',
  ...props
}: ConfirmButtonProps) => {
  const [open, setOpen] = useState(false);
  const t = useTranslations('common');
  const buttonsCtnRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const { current: buttonsEl } = buttonsCtnRef;
    if (!buttonsEl) return;
    const observer = new ResizeObserver(() => {
      const overlay = document.querySelector(`.${styles['overlay']}`) as HTMLDivElement;
      if (!overlay) return;
      const width = buttonsEl.getBoundingClientRect().width;
      if (!width) return;
      const maxWidth = Math.max(250, width);
      overlay.style.maxWidth = `${maxWidth}px`;
    });
    observer.observe(buttonsEl);

    return () => {
      observer.disconnect();
    };
  }, [open]);

  return (
    <Tooltip
      rootClassName={styles['overlay']}
      open={open}
      onOpenChange={setOpen}
      title={
        <div>
          <div>{confirmLabel}</div>
          <div className={styles['buttons-ctn']}>
            <div
              ref={buttonsCtnRef}
              className={styles['buttons']}
              style={{ display: 'flex', flexDirection: 'row' }}
            >
              <Button
                htmlType="button"
                type="text"
                onClick={() => {
                  onConfirm();
                  setOpen(false);
                }}
              >
                {yesLabel || t('yes')}
              </Button>
              <Button
                htmlType="button"
                onClick={() => {
                  setOpen(false);
                }}
              >
                {noLabel || t('no')}
              </Button>
            </div>
          </div>
        </div>
      }
      trigger={['click']}
      color={color}
      placement={placement}
    >
      <ButtonComponent
        style={
          {
            '--color': color,
          } as CSSProperties
        }
        {...props}
        className={`${styles['trigger-button']} ${props.className || ''}`}
        type="text"
      >
        {children}
      </ButtonComponent>
    </Tooltip>
  );
};
export default ConfirmButton;
