import { FieldProps, UiOptionsUpload, defaultUploadAccept } from './types';
import { useField } from 'react-final-form';
import { ChangeEvent, ReactElement, useCallback, useEffect, useMemo, useState } from 'react';
import { Button, Tooltip } from 'antd';
import { SchemaFormContext, useSchemaForm } from './context';
import { Label } from './Label';
import InfoBubble from './InfoBubble';
import FieldContainer from './FieldContainer';
import Loading from '@/components/Loading';
import TrashIcon from '@/svgs/trash.svg';
import { useTranslations } from 'next-intl';
import PictureIcon from '@/svgs/picture.svg';

const Preview = ({ src }: { src: string[] }) => {
  return (
    <>
      {src.map((src) => (
        <img key={src} src={src} className="max-h-24 min-w-[3rem]" alt="upload preview" />
      ))}
    </>
  );
};

export const FieldArrayUpload = ({
  uploadFile,
  ...props
}: FieldProps & {
  uploadFile: SchemaFormContext['utils']['uploadFile'];
}) => {
  const t = useTranslations('common');
  const field = useField(props.name);
  const [uploading, setUploading] = useState(false);
  const { setState } = useSchemaForm();
  const { 'ui:options': uiOptions = { upload: {} } } = props.schema as {
    'ui:options': UiOptionsUpload;
  };

  const [preview, setPreview] = useState<ReactElement | null>(
    field.input.value ? <Preview src={field.input.value} /> : null,
  );
  const [previewLabel, setPreviewLabel] = useState('');
  const [error, setError] = useState(false);

  useEffect(() => {
    if (field.input.value) {
      setPreview(<Preview src={field.input.value} />);
    }
  }, [field.input.value]);

  const readFile = useCallback(
    (e: ChangeEvent<HTMLInputElement>) => {
      if (!e.target.files || e.target.files.length === 0) return;
      setError(false);
      const uploadedFiles: string[] = [];
      const uploadedLabels: string[] = [];
      field.input.onChange([]);

      for (const file of Array.from(e.target.files)) {
        const reader = new FileReader();
        reader.onload = async ({ target }) => {
          if (!target || typeof target.result !== 'string') return;

          setState((prev) => ({
            ...prev,
            loading: true,
          }));
          setUploading(true);
          try {
            const value = await uploadFile(
              target.result.replace(
                /base64/,
                `filename:${file.name.replace(/[;\s]/g, '-')}; base64`,
              ),
            );
            setPreviewLabel('');
            if (typeof value === 'string') {
              uploadedFiles.push(value);
              field.input.onChange([...uploadedFiles]);
              setPreview(<Preview src={[...uploadedFiles]} />);
              setPreviewLabel(`${uploadedFiles.length} files`);
            } else if (!value) {
              throw 'Upload failed';
            } else {
              const { value: v, preview, label } = value;
              if (v) {
                uploadedFiles.push(v);
                field.input.onChange([...uploadedFiles]);
                if (preview) {
                  setPreview(
                    typeof preview === 'string' ? <Preview src={[...uploadedFiles]} /> : preview,
                  );
                }
                if (label) {
                  uploadedLabels.push(label);
                  const labelsSlice = uploadedLabels.join(', ').slice(0, 20);
                  const displayLabels =
                    labelsSlice.length == 20 ? `${labelsSlice}...` : labelsSlice;
                  setPreviewLabel(`${displayLabels} (${uploadedFiles.length})`);
                }
              }
            }
          } catch (error) {
            setError(true);
          }
          setState((prev) => ({
            ...prev,
            loading: false,
          }));
          setUploading(false);
        };
        reader.readAsDataURL(file);
      }
    },
    [setPreviewLabel, setPreview, uploadFile, field.input, setState],
  );

  const defaultPreview = useMemo(() => {
    const defaultPreview = uiOptions?.upload?.defaultPreview || (
      <PictureIcon className="flex items-center text-4xl !text-gray-200" />
    );
    if (typeof defaultPreview === 'string') {
      return <Preview src={[defaultPreview]} />;
    }

    return defaultPreview;
  }, [uiOptions?.upload?.defaultPreview]);

  return (
    <FieldContainer {...props} className="pr-form-upload">
      <Label field={field} schema={props.schema} className="pr-form-upload__label pr-form-label">
        {props.label}
      </Label>
      <div className={`pr-form-upload__input pr-form-input ${error ? 'pr-form-error' : ''}`}>
        <div className="pr-form-upload__placeholder">
          <div className="pr-form-upload__preview pr-form-upload__multi-preview">
            {!uploading && (field.input.value ? preview : defaultPreview)}
            {uploading && <Loading />}
          </div>
          {!uploading && (previewLabel || t('form.uploadLabel') || 'Choose files')}
          {uploading && (t('form.uploadingLabel') || 'Loading')}
        </div>

        <input
          type="file"
          onChange={readFile}
          accept={
            (uiOptions && uiOptions?.upload && uiOptions?.upload?.accept) || defaultUploadAccept
          }
          multiple={true}
        />
        {field.input.value && (
          <div className="pr-form-upload__delete">
            <Tooltip title={t('form.uploadRemove') || 'Remove files'} placement="left">
              <Button
                onClick={() => {
                  field.input.onChange('');
                  setPreview(null);
                  setPreviewLabel('');
                }}
                variant="link"
              >
                <TrashIcon />
              </Button>
            </Tooltip>
          </div>
        )}
      </div>
      <InfoBubble className="pr-form-upload__description" text={props.schema.description} />
    </FieldContainer>
  );
};

const LinkedFieldArrayUpload = (props: FieldProps) => {
  const {
    utils: { uploadFile },
  } = useSchemaForm();
  return <FieldArrayUpload {...props} uploadFile={uploadFile} />;
};
export default LinkedFieldArrayUpload;
