import {
  Children,
  cloneElement,
  FunctionComponent,
  HTMLAttributes,
  MutableRefObject,
  ReactElement,
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import { Form } from 'react-final-form';
import arrayMutators from 'final-form-arrays';
import Field from './Field';
import { Schema } from './types';
import { root } from './utils';
import { context, SchemaFormContext } from './context';
import FieldAny from './FieldAny';
import FieldBoolean from './FieldBoolean';
import FieldDate from './FieldDate';
import FieldLocalizedBoolean from './FieldLocalizedBoolean';
import FieldLocalizedText from './FieldLocalizedText';
import FieldObject from './FieldObject';
import FieldArray from './FieldArray';
import FieldTags from './FieldTags';
import FieldSelect from './FieldSelect';
import FieldRadio from './FieldRadio';
import FieldText from './FieldText';
import FieldArrayUpload from './FieldArrayUpload';
import OnChange from './OnChange';
import { FormApi } from 'final-form';
import { Button } from 'antd';
import './schema-form.scss';
import { useTranslations } from 'use-intl';
import { useSchemaFormConfig } from './SchemaFormConfigContext';

export interface SchemaFormProps extends Omit<HTMLAttributes<HTMLFormElement>, 'onChange'> {
  schema: Schema;
  onSubmit?: (values: any) => void | Record<string, any> | Promise<Record<string, any>>;
  buttons?: (ReactElement | FunctionComponent<{ disabled: boolean }>)[];
  onChange?: (values: any, previous: any) => void;
  initialValues?: any;
  components?: Partial<SchemaFormContext['components']>;
  utils?: Partial<SchemaFormContext['utils']>;
  formRef?: MutableRefObject<FormApi<any, any> | undefined>;
  initialFieldObjectVisibility?: boolean;
  autoFocus?: boolean;
  locales?: {
    submit?: string;
  };
}

export const SchemaFormWithSchema = ({
  schema,
  onSubmit,
  buttons,
  onChange,
  initialValues,
  components,
  utils,
  formRef,
  initialFieldObjectVisibility = true,
  className,
  autoFocus,
  locales,
  ...props
}: SchemaFormProps) => {
  const t = useTranslations('common');
  const values = useRef({ values: initialValues });
  const formElRef = useRef<HTMLFormElement>(null);
  const [state, setState] = useState<SchemaFormContext['state']>({});
  const config = useSchemaFormConfig();

  const onSubmitHandle = useCallback(
    async (values: any) => {
      if (!onSubmit) return;
      const errors = await onSubmit(values.values);
      if (!errors) return;
      return { values: errors };
    },
    [onSubmit],
  );

  const componentsWithDefault = useMemo<SchemaFormContext['components']>(
    () => ({
      FieldArray,
      FieldAny,
      FieldBoolean,
      FieldDate,
      FieldLocalizedBoolean,
      FieldLocalizedText,
      FieldObject: FieldObject(initialFieldObjectVisibility),
      FieldTags,
      FieldArrayUpload,
      FieldSelect,
      FieldRadio,
      FieldText,
      ...components,
      ...config.components,
      UiWidgets: {
        ...components?.UiWidgets,
        ...config?.components?.UiWidgets,
      },
    }),
    [components, initialFieldObjectVisibility, config.components],
  );

  const utilsWithDefault = useMemo(
    () => ({
      extractSelectOptions: () => [],
      extractAutocompleteOptions: () => [],
      uploadFile: async (file: string) => file,
      ...utils,
      ...config.utils,
    }),
    [utils, config.utils],
  );

  useEffect(() => {
    if (!autoFocus) return;
    formElRef.current?.querySelector('input')?.focus();
  }, [autoFocus]);

  const handleOnChange = useCallback(
    (value: string, previous: string) => {
      if (previous === value) return;
      onChange?.(value, previous);
    },
    [onChange],
  );

  return (
    <context.Provider
      value={{
        components: componentsWithDefault,
        utils: utilsWithDefault,
        state,
        setState,
      }}
    >
      <Form
        onSubmit={onSubmitHandle}
        initialValues={values.current}
        mutators={{ ...arrayMutators }}
      >
        {({ handleSubmit, hasValidationErrors, form }) => {
          formRef && (formRef.current = form);
          return (
            <form
              ref={formElRef}
              onSubmit={handleSubmit}
              className={`pr-form ${
                hasValidationErrors ? 'pr-form--has-validation-errors' : ''
              } ${className || ''}`}
              {...props}
            >
              {onChange && <OnChange name="values">{handleOnChange}</OnChange>}
              <Field schema={schema} name={root} />
              <div className="pr-form-buttons">
                {buttons ? (
                  Children.map(
                    buttons.map((Button, key) =>
                      typeof Button === 'function' ? (
                        <Button
                          disabled={!!(hasValidationErrors || state.loading)}
                          key={`${key}`}
                        />
                      ) : (
                        Button
                      ),
                    ),
                    (button, key) =>
                      cloneElement(button, {
                        key: button.key || key,
                        disabled: hasValidationErrors || state.loading,
                      }),
                  )
                ) : (
                  <Button
                    htmlType="submit"
                    className="pr-form-submit"
                    disabled={hasValidationErrors || state.loading}
                    data-testid="schema-form-submit-button"
                  >
                    {state.loading}
                    {t(locales?.submit || 'form.submit')}
                  </Button>
                )}
              </div>
            </form>
          );
        }}
      </Form>
    </context.Provider>
  );
};

export default function SchemaForm(props: SchemaFormProps) {
  if (!props.schema) return null;
  return <SchemaFormWithSchema {...props} />;
}
