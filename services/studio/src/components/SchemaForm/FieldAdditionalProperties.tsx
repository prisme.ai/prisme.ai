import { Button, Input, Tooltip } from 'antd';
import { useCallback, useEffect, useState } from 'react';
import { FieldRenderProps, useField } from 'react-final-form';
import { SelfField } from './Field';
import FieldContainer from './FieldContainer';
import { FieldProps, Schema } from './types';
import CodeEditorInline from '../CodeEditor/Inline';
import TrashIcon from '@/svgs/trash.svg';
import equal from 'fast-deep-equal';
import { useTranslations } from 'next-intl';

interface AdditionalPropertiesProps extends FieldProps {
  field: FieldRenderProps<any, HTMLElement, any>;
}

const cleanValue = (schema: Schema, value: any) => {
  const keys = Object.keys(schema.properties || {});
  return Object.keys(value || {}).reduce(
    (prev, key) =>
      keys.includes(key)
        ? prev
        : {
            ...prev,
            [key]: value[key],
          },
    {},
  );
};
const getInitialValue = (schema: Schema, value: any) => {
  if (typeof value === 'string') return value;
  return JSON.stringify(cleanValue(schema, value), null, '  ');
};

export const FreeAdditionalProperties = ({ ...props }: AdditionalPropertiesProps) => {
  const t = useTranslations('common');
  const [value, setValue] = useState(getInitialValue(props.schema, props.field.input.value));
  useEffect(() => {
    try {
      const json = JSON.parse(value);
      const keys = Object.keys(props.schema.properties || {});
      const cleanedValue = Object.keys(props.field.input.value || {}).reduce(
        (prev, key) =>
          keys.includes(key)
            ? {
                ...prev,
                [key]: props.field.input.value[key],
              }
            : prev,
        {},
      );
      const newValue = {
        ...cleanedValue,
        ...json,
      };
      if (equal(newValue, props.field.input.value)) return;
      props.field.input.onChange(newValue);
    } catch {
      //
    }
  }, [value, props.field.input, props.schema.properties]);
  return (
    <FieldContainer
      {...props}
      className="pr-form-additional-properties pr-form-additional-properties--free"
    >
      <label>{t('form.additionalProperties')}</label>
      <CodeEditorInline mode="json" onChange={setValue} value={value} />
    </FieldContainer>
  );
};

export const ManagedAdditionalProperties = (props: AdditionalPropertiesProps) => {
  const [value, setValue] = useState(Object.entries(props.field.input.value));

  const updateKey = useCallback(
    (i: number) => (v: string) => {
      setValue((prev) => {
        const newValue = [...prev];
        newValue[i][0] = v;
        return newValue;
      });
    },
    [],
  );
  const updateValue = useCallback(
    (i: number) => (v: string) => {
      setValue((prev) => {
        const newValue = [...prev];
        newValue[i][1] = v;
        return newValue;
      });
    },
    [],
  );
  const add = useCallback(() => {
    setValue((prev) => {
      const newValue = [...prev, ['', ''] as [string, string]];
      return newValue;
    });
  }, []);
  const remove = useCallback(
    (i: number) => () => {
      setValue((prev) => {
        const newValue = prev.filter((item, index) => i !== index);
        return newValue;
      });
    },
    [],
  );

  useEffect(() => {
    const newValue = Object.fromEntries(value);
    if (equal(newValue, props.field.input.value)) return;
    props.field.input.onChange(Object.fromEntries(value));
  }, [value, props.field.input]);

  return (
    <FieldContainer {...props} className="pr-form-additional-properties">
      {value.map(([key, value], index) => (
        <div key={index} className="pr-form-additional-properties__property pr-form-input">
          <div className="pr-form-additional-properties__property-key">
            <label className="pr-form-label" htmlFor={`${props.field.name}-${index}`}>
              {props.schema.propertyKey || 'Key'}
            </label>
            <Input
              value={key}
              onChange={({ target: { value } }) => updateKey(index)(value)}
              id={`${props.field.name}-${index}`}
            />
          </div>
          <span className="pr-form-additional-properties__property-separator"> : </span>
          <div className="pr-form-additional-properties__property-value">
            <SelfField
              schema={{
                ...(props.schema.additionalProperties as Schema),
              }}
              label={props.schema.propertyValue || 'Value'}
              value={value}
              onChange={updateValue(index)}
            />
          </div>

          <Tooltip
            title={(props.schema.additionalProperties as Schema)?.remove || 'Remove'}
            placement="left"
          >
            <Button
              onClick={remove(index)}
              className="pr-form-additional-properties__property-delete"
            >
              <TrashIcon />
            </Button>
          </Tooltip>
        </div>
      ))}
      <Button onClick={add}>
        {(props.schema.additionalProperties as Schema)?.add || 'Add a property'}
      </Button>
    </FieldContainer>
  );
};

export const FieldAdditionalProperties = ({ schema, name }: FieldProps) => {
  const field = useField(name);

  if (!schema || !schema.additionalProperties) return null;

  if (schema.additionalProperties === true) {
    return <FreeAdditionalProperties field={field} schema={schema} name={name} />;
  }
  return <ManagedAdditionalProperties schema={schema} field={field} name={name} />;
};

export default FieldAdditionalProperties;
