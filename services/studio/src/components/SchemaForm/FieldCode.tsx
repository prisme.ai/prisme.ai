import { useField } from 'react-final-form';
import FieldContainer from './FieldContainer';
import { FieldProps, UiOptionsCode } from './types';
import Label from './Label';
import { Tooltip } from 'antd';
import { getError } from './utils';
import InfoBubble from './InfoBubble';
import CodeEditorInline from '@/components/CodeEditor/Inline';
import { useCallback, useState } from 'react';

export default function FieldCode(props: FieldProps & { options?: UiOptionsCode }) {
  const field = useField(props.name);
  const hasError = getError(field.meta);
  const [value] = useState(
    typeof field.input.value === 'string'
      ? field.input.value
      : JSON.stringify(field.input.value, null, '  '),
  );
  const onChange = useCallback(
    (v: string) => {
      try {
        const valueAsObj = JSON.parse(v);
        field.input.onChange(valueAsObj);
      } catch {
        field.input.onChange(v);
      }
    },
    [field.input],
  );

  return (
    <FieldContainer {...props} className="pr-form-code">
      <Label field={field} schema={props.schema} className="pr-form-code__label pr-form-label">
        {props.label}
      </Label>
      <Tooltip title={hasError} rootClassName="pr-form-error">
        <div className="pr-form-code__input">
          <CodeEditorInline
            defaultValue={value}
            onChange={onChange}
            mode={props.options?.code?.mode || 'json'}
          />
        </div>
      </Tooltip>
      <InfoBubble className="pr-form-text__description" text={props.schema.description} />
    </FieldContainer>
  );
}
