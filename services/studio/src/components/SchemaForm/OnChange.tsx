import { useEffect, useRef } from 'react';
import { useField } from 'react-final-form';
import equal from 'fast-deep-equal';

const usePrevious = (val: string) => {
  const ref = useRef(val);

  useEffect(() => {
    ref.current = val;
  }, [val]);

  return ref.current;
};

const useFieldValue = (name: string) => {
  const {
    input: { value },
  } = useField(name, { subscription: { value: true }, allowNull: true });
  const prevValue = usePrevious(value);

  return [value, prevValue];
};

export default ({
  name,
  children,
}: {
  name: string;
  children: (newValue: string, prevValue: string) => void;
}) => {
  const [value, prevValue] = useFieldValue(name);

  useEffect(() => {
    if (!equal(value, prevValue)) {
      children(value, prevValue);
    }
  }, [children, value, prevValue]);

  return null;
};
