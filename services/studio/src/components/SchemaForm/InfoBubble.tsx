import { Tooltip } from 'antd';
import { ReactNode } from 'react';
import InfoIcon from '@/svgs/info.svg';

interface InfoBubbleProps {
  text?: string | ReactNode;
  className?: string;
  children?: ReactNode;
}

export const InfoBubble = ({ children, className = '', text }: InfoBubbleProps) => {
  if (!text) return <>{children}</>;

  return (
    <div className={`${className} pr-form-description`}>
      <Tooltip
        title={typeof text === 'string' ? <div dangerouslySetInnerHTML={{ __html: text }} /> : text}
        placement="right"
      >
        <button type="button" className="pr-form-description__button">
          <InfoIcon width="1.4rem" height="1.4rem" />
        </button>
      </Tooltip>
    </div>
  );
};

export default InfoBubble;
