import { createContext, ReactNode, useContext } from 'react';
import { SchemaFormContext } from './context';

interface SchemaFormConfigContext {
  utils?: Partial<SchemaFormContext['utils']>;
  components?: Partial<SchemaFormContext['components']>;
}
const schemaFormConfigContext = createContext<Partial<SchemaFormConfigContext> | undefined>(
  undefined,
);
export function useSchemaFormConfig() {
  const context = useContext(schemaFormConfigContext);

  return {
    ...context,
  };
}

interface SchemaFormConfigProviderProps extends SchemaFormConfigContext {
  children: ReactNode;
}

export default function SchemaFormConfigProvider({
  children,
  ...config
}: SchemaFormConfigProviderProps) {
  return (
    <schemaFormConfigContext.Provider value={config}>{children}</schemaFormConfigContext.Provider>
  );
}
