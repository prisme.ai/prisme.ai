import { ReactNode } from 'react';
import { useField } from 'react-final-form';
import { FieldProps } from './types';
import { Tooltip } from 'antd';

interface FieldContainerProps extends FieldProps {
  children?: ReactNode;
}

export const FieldContainer = (props: FieldContainerProps) => {
  const field = useField(props.name);

  return (
    <Tooltip
      title={typeof field.meta.error === 'string' && field.meta.error}
      rootClassName="pr-form-field-error"
    >
      <div
        className={`pr-form-field ${props.className} ${
          props.name !== 'values' && field.meta.dirty && field.meta.error
            ? 'pr-form-field--error'
            : ''
        }`}
      >
        {props.children}
      </div>
    </Tooltip>
  );
};

export default FieldContainer;
