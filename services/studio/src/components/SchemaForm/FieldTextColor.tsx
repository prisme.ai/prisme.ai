import { useCallback, useRef } from 'react';
import { ColorPicker } from 'antd';
import { useField } from 'react-final-form';
import { FieldProps } from './types';
import { getLabel } from './utils';
import Color from 'color';
import InfoBubble from './InfoBubble';
import FieldContainer from './FieldContainer';
import { AggregationColor } from 'antd/es/color-picker/color';

export const FieldTextColor = (props: FieldProps) => {
  const field = useField(props.name);

  const t = useRef<NodeJS.Timeout>();
  const onChange = useCallback(
    (color: AggregationColor) => {
      t.current && clearTimeout(t.current);
      t.current = setTimeout(() => {
        field.input.onChange(`#${color.toHex()}`);
      }, 100);
    },
    [field],
  );

  let colorIsLight = true;
  try {
    colorIsLight = Color(field.input.value || 'black').isLight();
  } catch {}

  return (
    <FieldContainer {...props} className="pr-form-text-color">
      <label
        htmlFor={field.input.name}
        className="pr-form-text-color__label pr-form-label"
        style={{
          color: colorIsLight ? '' : field.input.value,
          backgroundColor: colorIsLight ? field.input.value : '',
        }}
      >
        {props.label || props.schema.title || getLabel(props.name)}
      </label>
      <div className="pr-form-text-color__input pr-form-input">
        <ColorPicker
          defaultValue={field.input.value}
          onChange={onChange}
          data-testid={`schema-form-field-${field.input.name}`}
          showText
        />
      </div>
      <InfoBubble className="pr-form-text-color__description" text={props.schema.description} />
    </FieldContainer>
  );
};

export default FieldTextColor;
