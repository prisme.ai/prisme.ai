import { Tooltip } from 'antd';
import { useField } from 'react-final-form';
import FieldContainer from './FieldContainer';
import InfoBubble from './InfoBubble';
import Label from './Label';
import { FieldProps, UiOptionsHTML } from './types';
import { getError } from './utils';
import RichTextEditor from '../RichTextEditor';

export const FieldHTML = (props: FieldProps) => {
  const field = useField(props.name);
  const { 'ui:options': uiOptions = {} } = props.schema;

  const hasError = getError(field.meta);

  return (
    <FieldContainer {...props} className="pr-form-text">
      <Label field={field} schema={props.schema} className="pr-form-text__label pr-form-label">
        {props.label}
      </Label>
      <Tooltip title={hasError} rootClassName="pr-form-error">
        <div className="pr-form-input">
          <RichTextEditor
            {...field.input}
            {...(uiOptions as UiOptionsHTML)}
            data-testid={`schema-form-field-${field.input.name}`}
          />
        </div>
      </Tooltip>
      <InfoBubble className="pr-form-text__description" text={props.schema.description} />
    </FieldContainer>
  );
};

export default FieldHTML;
