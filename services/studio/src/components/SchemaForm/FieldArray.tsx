import { Button, Tooltip } from 'antd';
import { FieldArray as FFFieldArray } from 'react-final-form-arrays';
import Field from './Field';
import { FieldProps, Schema, UiOptionsArray } from './types';
import { getDefaultValue, getLabel } from './utils';
import FieldContainer from './FieldContainer';
import InfoBubble from './InfoBubble';
import TrashIcon from '@/svgs/trash.svg';
import PlusIcon from '@/svgs/plus.svg';
import { useTranslations } from 'next-intl';

function isUiOptionsArray(uiOptions: Schema['ui:options']): uiOptions is UiOptionsArray {
  return !!uiOptions && !!(uiOptions as UiOptionsArray).array;
}

export default function FieldArray(props: FieldProps) {
  const t = useTranslations('common');
  const { items = {} } = props.schema;
  const { 'ui:options': uiOptions } = props.schema;

  if (!items) return null;

  const asRow = isUiOptionsArray(uiOptions) && uiOptions.array === 'row';

  return (
    <FieldContainer {...props} className="pr-form-array">
      <label className="pr-form-array__label pr-form-label" htmlFor={props.name}>
        {props.label || props.schema.title || getLabel(props.name)}

        <InfoBubble className="pr-form-array__description" text={props.schema.description} />
      </label>
      <FFFieldArray name={props.name}>
        {({ fields }) => (
          <>
            <div className={`pr-form-array__item ${asRow ? 'pr-form-array__item--as-row' : ''}`}>
              {fields.map((field, index) => (
                <div
                  key={field}
                  className={`pr-form-array__item-field ${
                    asRow ? 'pr-form-array__item-field--as-row' : ''
                  }`}
                >
                  <Field schema={items} name={field} />
                  <Button
                    onClick={() => fields.remove(index)}
                    className="pr-form-array__item-field-remove"
                    disabled={props.schema.disabled}
                  >
                    <Tooltip
                      title={items.remove || t('form.removeItem') || 'Remove'}
                      placement="left"
                    >
                      <TrashIcon />
                    </Tooltip>
                  </Button>
                </div>
              ))}
            </div>
            <div className="pr-form-array__item-add">
              <Tooltip title={items.add || t('form.addItem') || 'Add item'} placement="right">
                <Button
                  onClick={() => fields.push(getDefaultValue(items.type))}
                  className="pr-form-array__item-add-button"
                  disabled={props.schema.disabled}
                  id={props.name}
                >
                  <PlusIcon />
                </Button>
              </Tooltip>
            </div>
          </>
        )}
      </FFFieldArray>
    </FieldContainer>
  );
}
