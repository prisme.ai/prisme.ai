import {
  createContext,
  Dispatch,
  ReactElement,
  ReactNode,
  SetStateAction,
  useContext,
} from 'react';
import { FieldRenderProps } from 'react-final-form';
import { Field } from './Field';
import {
  FieldProps,
  UiOptionsAutocomplete,
  UiOptionsCode,
  UiOptionsHTML,
  UiOptionsUpload,
  UiOptionsSelect,
  Schema,
} from './types';
import { SelectProps } from '../Select';

export type FieldComponent<T = {}> = (
  props: FieldProps & { field?: FieldRenderProps<any, HTMLElement, any> } & T,
) => ReactElement | null;
export type InputComponent<T = {}> = (
  props: T & {
    value: string;
    onChange: (e: React.ChangeEvent<HTMLInputElement> | string) => void;
  },
) => ReactElement;

export interface SchemaFormContext {
  components: {
    Field?: typeof Field;
    FieldLocalizedText?: FieldComponent;
    FieldText?: FieldComponent;
    FieldBoolean?: FieldComponent;
    FieldLocalizedBoolean?: FieldComponent;
    FieldObject?: FieldComponent;
    FieldArray?: FieldComponent;
    FieldArrayUpload?: FieldComponent<{ options?: UiOptionsUpload }>;
    FieldAny?: FieldComponent;
    FieldFreeAdditionalProperties?: FieldComponent;
    FieldTags?: FieldComponent<{
      options?: {
        value: string;
        label: string | ReactNode;
        color?: string;
      };
    }>;
    FieldSelect?: FieldComponent<{ options?: SelectProps['selectOptions'] }>;
    FieldRadio?: FieldComponent<{ options?: SelectProps['selectOptions'] }>;
    FieldDate?: FieldComponent;
    FieldCode?: FieldComponent<{ options?: UiOptionsCode }>;
    JSONEditor?: InputComponent;
    HTMLEditor?: InputComponent<{ options?: UiOptionsHTML }>;
    FreeAdditionalProperties?: FieldComponent;
    ManagedAdditionalProperties?: FieldComponent;
    UiWidgets: Record<string, FieldComponent>;
  };
  utils: {
    extractSelectOptions: (schema: Schema) => UiOptionsSelect['select']['options'] | null;
    extractAutocompleteOptions: (
      schema: Schema,
    ) => UiOptionsAutocomplete['autocomplete']['options'] | null;
    uploadFile: (
      base64File: string,
      opts?: {
        expiresAfter?: number;
        public?: boolean;
        shareToken?: boolean;
      },
    ) => Promise<
      string | { value: string; preview: string | ReactElement; label?: string } | undefined
    >;
  } & Record<string, any>;
  state: {
    loading?: boolean;
  };
  setState: Dispatch<SetStateAction<{}>>;
}

export const context = createContext<SchemaFormContext>({
  components: {
    UiWidgets: {},
  },
  utils: {
    extractSelectOptions: () => [],
    extractAutocompleteOptions: () => [],
    uploadFile: async () => '',
  },
  state: {},
  setState: () => null,
});

export const useSchemaForm = () => useContext(context);
