import { ReactNode, useMemo, useRef, useState } from 'react';
import { Dropdown, Input, InputRef, Tooltip } from 'antd';
import PlusIcon from '@/svgs/plus.svg';
import TrashIcon from '@/svgs/trash.svg';
import MagnifierIcon from '@/svgs/magnifier.svg';
import styles from './localized-input.module.scss';
import { search } from '@/utils/filters';
import { useTranslations } from 'use-intl';

type Lang = {
  label: string;
  value: string;
};

interface LangMenuProps {
  addLang: (lang: string) => void;
  selectLang: (lang: string) => void;
  removeLang: (lang: string) => void;
  setLangs: Lang[];
  unsetLangs: Lang[];
  selectedLang?: string;
  children: ReactNode;
}
export default function LangMenu({
  setLangs,
  unsetLangs,
  children,
  addLang,
  removeLang,
  selectLang,
  selectedLang,
}: LangMenuProps) {
  const t = useTranslations('common');
  const inputRef = useRef<InputRef>(null);
  const [filter, setFilter] = useState('');

  const items = useMemo(() => {
    return [
      {
        key: 'search',
        className: styles['lang-menu-item--search'],
        label: (
          <Input
            ref={inputRef}
            value={filter}
            onChange={({ target: { value } }) => setFilter(value)}
            onClick={(e) => e.stopPropagation()}
            suffix={<MagnifierIcon />}
          />
        ),
      },
      ...setLangs
        .filter(({ label, value }) => search(filter)(`${label} ${value}`))
        .map(({ label, value }) => ({
          key: value,
          label: (
            <div
              className={`${styles['lang-menu-item--set']} ${selectedLang === value ? styles['lang-menu-item--selected'] : ''}`}
            >
              {label}
              <Tooltip title={t('form.localized.delete')} placement="left">
                <button
                  type="button"
                  onClick={(e) => {
                    e.stopPropagation();
                    removeLang(value);
                  }}
                >
                  <TrashIcon width="1rem" />
                </button>
              </Tooltip>
            </div>
          ),
          onClick: () => selectLang(value),
        })),
      {
        key: 'available',
        label: t('form.localized.available'),
        className: styles['lang-menu-item--available'],
      },
      ...unsetLangs
        .filter(({ label, value }) => search(filter)(`${label} ${value}`))
        .map(({ label, value }) => ({
          key: value,
          label,
          onClick: () => addLang(value),
          icon: (
            <Tooltip title={t('form.localized.add')} placement="right">
              <PlusIcon />
            </Tooltip>
          ),
        })),
    ];
  }, [setLangs, unsetLangs, removeLang, selectLang, addLang, filter, t, selectedLang]);

  return (
    <Dropdown
      menu={{
        items,
      }}
      trigger={['click']}
      rootClassName={styles['lang-menu-overlay']}
      placement="bottomRight"
      onOpenChange={(open) => {
        if (!open) return;
        setTimeout(() => {
          inputRef.current?.select();
          inputRef.current?.focus();
        });
      }}
    >
      {children}
    </Dropdown>
  );
}
