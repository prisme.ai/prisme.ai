import { ChangeEvent, forwardRef, useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { locales } from 'iso-lang-codes';
import { Input, Tooltip } from 'antd';
import WorldIcon from '@/svgs/world.svg';
import { InputProps } from 'antd/lib';
import LangMenu from './LangMenu';
import styles from './localized-input.module.scss';
import { useTranslations } from 'next-intl';

const allLangs = locales();
const availableLangs = Object.keys(allLangs).reduce((prev, next) => {
  const shortLang = next.substring(0, 2);
  const shortLocale = allLangs[next].replace(/(;| \().*\)?$/, '');
  prev.set(shortLang, shortLocale);
  return prev;
}, new Map());

type LocalizedTextObject = Record<string, string>;
const isLocalizedTextObject = (value: string | LocalizedTextObject): value is LocalizedTextObject =>
  typeof value !== 'string';

export interface LocalizedInputProps {
  value: LocalizedTextObject | string;
  onChange: (v: LocalizedTextObject | string) => void;
  Input?: string | any; //((props: Pick<InputProps, 'value' | 'onChange'>) => ReactElement);
  InputProps?: any;
  setLangsTitle?: string;
  availableLangsTitle?: string;
  setLangTooltip?: string;
  // String with {{lang}} placeholder to display current lang
  addLangTooltip?: string;
  deleteTooltip?: string;
  iconMarginTop?: number | string;
  className?: string;
  initialLang?: string;
  unmountOnLangChange?: boolean;
}

const DftInput = forwardRef(function DftInput(props: InputProps, ref: any) {
  return <Input ref={ref} {...props} />;
});

function getInitialLang(text: LocalizedTextObject, initialLang: string) {
  if (Object.hasOwn(text, initialLang)) {
    return initialLang;
  }
  return Object.keys(text)[0];
}

export const LocalizedInput = ({
  value,
  onChange,
  Input: Component = DftInput,
  InputProps = {},
  className,
  initialLang = 'en',
  unmountOnLangChange = false,
  iconMarginTop,
  ...props
}: LocalizedInputProps) => {
  void iconMarginTop;
  const t = useTranslations('common');
  const [selectedLang, setSelectedLang] = useState(
    isLocalizedTextObject(value) ? getInitialLang(value, initialLang) : '',
  );
  const [mounted, setMounted] = useState(true);
  const input = useRef<any>(null);
  const [currentValue, setCurrentValue] = useState(
    typeof value === 'string' ? value : value[selectedLang],
  );

  useEffect(() => {
    if (typeof value === 'string') {
      setCurrentValue(value);
      setSelectedLang('');
    } else {
      if (value[selectedLang] !== undefined) {
        setCurrentValue(value[selectedLang]);
      } else {
        const lang = getInitialLang(value, initialLang);
        setCurrentValue(value[lang]);
        setSelectedLang(lang);
      }
    }
  }, [value, selectedLang, initialLang]);

  const onSetCurrentValue = useCallback(
    (currentValue: string) => {
      setCurrentValue(currentValue);
      if (!selectedLang) return onChange(currentValue);
      onChange({
        ...(typeof value === 'object' ? value : {}),
        [selectedLang]: currentValue || '',
      });
    },
    [selectedLang, value, onChange],
  );

  const onSetLang = useCallback(
    async (selectedLang: string) => {
      await setMounted(false);
      setSelectedLang(selectedLang);
      setCurrentValue(typeof value === 'object' ? value[selectedLang] : '');
      setMounted(true);
    },
    [value],
  );

  const onAddLang = useCallback(
    (selectedLang: string) => {
      setSelectedLang(selectedLang);
      const newValue =
        typeof value === 'string'
          ? {
              [selectedLang]: value,
            }
          : {
              ...value,
              [selectedLang]: '',
            };
      onChange(newValue);
      setCurrentValue(newValue[selectedLang] || '');
      input?.current?.focus();
    },
    [value, onChange],
  );

  const onDeleteLang = useCallback(
    (lang: string) => {
      if (typeof value === 'string') return;
      const newValue = { ...value };
      delete newValue[lang];
      onChange(newValue);
      onSetLang(Object.keys(newValue)[0] || '');
    },
    [value, onSetLang, onChange],
  );

  const { setLangs, unsetLangs } = useMemo(() => {
    const setLangs = isLocalizedTextObject(value)
      ? Object.keys(value).map((key) => ({
          label: availableLangs.get(key) || key,
          value: key,
        }))
      : [];
    const ignoredLangs = setLangs.map(({ value }) => value);

    const unsetLangs = Array.from(availableLangs.keys())
      .filter((key) => !ignoredLangs.includes(key))
      .map((key) => ({
        label: availableLangs.get(key) || key,
        value: key,
      }));

    if (selectedLang && setLangs[0]) {
      if (!setLangs.find(({ value }) => selectedLang === value)) {
        setLangs.push({
          label: availableLangs.get(selectedLang),
          value: selectedLang,
        });
      }
    }

    return { setLangs, unsetLangs };
  }, [value, selectedLang]);

  return (
    <div className={`${styles['localized-input']} ${className || ''}`}>
      {(!unmountOnLangChange || mounted) && (
        <Component
          ref={input}
          value={currentValue}
          onChange={(v: ChangeEvent<HTMLInputElement> | string) => {
            const value = typeof v === 'string' ? v : v.target.value;
            onSetCurrentValue(value);
          }}
          {...InputProps}
          {...props}
        />
      )}

      <div
        className={styles['lang-menu']}
        style={{
          marginTop: iconMarginTop
            ? typeof iconMarginTop === 'number'
              ? `${iconMarginTop}px`
              : iconMarginTop
            : undefined,
        }}
      >
        <LangMenu
          setLangs={setLangs}
          unsetLangs={unsetLangs}
          selectLang={onSetLang}
          addLang={onAddLang}
          removeLang={onDeleteLang}
          selectedLang={selectedLang}
        >
          <Tooltip
            title={t(`form.localized.tooltip${selectedLang ? '' : '_unset'}`, {
              lang: availableLangs.get(selectedLang) || selectedLang,
            })}
            placement="left"
          >
            <button type="button">
              {selectedLang ? (
                <span className={styles['lang-menu-icon']}>{selectedLang}</span>
              ) : (
                <WorldIcon className={styles['lang-menu-icon']} />
              )}
            </button>
          </Tooltip>
        </LangMenu>
      </div>
    </div>
  );
};

export default LocalizedInput;
