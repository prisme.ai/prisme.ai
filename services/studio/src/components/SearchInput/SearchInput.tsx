import { Input, InputProps, InputRef } from 'antd';
import CloseIcon from '@/svgs/close.svg';
import MagnifierIcon from '@/svgs/magnifier.svg';
import { ChangeEvent, forwardRef } from 'react';
import styles from './search-input.module.scss';

export interface SearchInputProps extends InputProps {
  onClear?: () => void;
}

const SearchInput = forwardRef<InputRef, SearchInputProps>(function SearchInput(
  { onClear, ...props },
  ref,
) {
  return (
    <Input
      ref={ref}
      prefix={<MagnifierIcon />}
      suffix={
        <button
          className={`${styles['search-suffix']} ${props.value ? styles['search-suffix--visible'] : ''}`}
          onClick={() => {
            onClear?.();

            props.onChange?.({
              target: { value: '' },
            } as ChangeEvent<HTMLInputElement>);
          }}
        >
          <CloseIcon />
        </button>
      }
      {...props}
    />
  );
});

export default SearchInput;
