import { MouseEventHandler, ReactElement } from 'react';
import ChevronIcon from '@/svgs/chevron.svg';

export interface ListItemProps {
  title: string | ReactElement;
  content?: string | ReactElement;
  rightContent?: string | ReactElement;
  onClick?: MouseEventHandler<HTMLDivElement>;
  className?: string;
}

const ListItem = ({ title, content, rightContent, onClick, className }: ListItemProps) => (
  <div
    className={`flex grow cursor-pointer items-center justify-between rounded border border-gray-200 px-6 py-4 text-black ${className}`}
    onClick={onClick}
  >
    <div className="flex h-full flex-col items-baseline space-y-2 overflow-hidden">
      <div>{title}</div>
      {content && <div className="text-gray">{content}</div>}
    </div>
    <div>{rightContent || <ChevronIcon className="rotate-[-90deg]" />}</div>
  </div>
);

export default ListItem;
