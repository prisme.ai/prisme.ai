import React from 'react';

export interface SidePanelProps {
  children: React.ReactNode;
  variant?: 'rounded' | 'squared';
  className?: string;
  Header?: React.ReactNode;
}

const SidePanel = ({ Header, className, variant = 'rounded', children }: SidePanelProps) => {
  if (variant === 'rounded') {
    return (
      <div className={`h-full p-2 ${className}`}>
        <div>{Header}</div>
        <div className="flex h-full grow rounded border border-solid border-gray-200 p-4">
          {children}
        </div>
      </div>
    );
  }

  return (
    <div className={`h-full ${className}`}>
      <div>{Header}</div>
      <div className="flex h-full grow border !border-t-0 border-solid border-gray-200 p-4">
        {children}
      </div>
    </div>
  );
};

export default SidePanel;
