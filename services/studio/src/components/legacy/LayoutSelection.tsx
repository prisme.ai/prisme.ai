import { ReactNode, useMemo, useState } from 'react';
import ListItem, { ListItemProps } from './ListItem';
import { Button, Col, Row } from 'antd';
import SidePanel from './SidePanel';
import PlusIcon from '@/svgs/plus.svg';
import SearchInput from '@/components/SearchInput';
import ChevronIcon from '@/svgs/chevron.svg';

const DEFAULT_ITEM_PER_PAGE = 10;

const paginate = (array: any[], page_size: number, page_number: number) => {
  // human-readable page numbers usually start with 1, so we reduce 1 in the first argument
  return array.slice((page_number - 1) * page_size, page_number * page_size);
};

interface ListItemWithId extends ListItemProps {
  id: string;
  title: string;
}

export interface LayoutSelectionProps {
  children: ReactNode;
  items: ListItemWithId[];
  selected: string;
  onSelect: (s: string) => void;
  Header?: ReactNode;
  onAdd?: () => void;
  addLabel?: string;
  searchLabel?: string;
  itemPerPage?: number;
  leftPanelWidth?: number;
}

interface ListItemWithSelection extends ListItemWithId {
  selected: boolean;
  onSelect: (s: string) => void;
  id: string;
}

const ListItemWithSelection = ({
  selected,
  onSelect,
  className,
  ...listItemProps
}: ListItemWithSelection) => (
  <ListItem
    {...listItemProps}
    onClick={(e) => {
      listItemProps.onClick && listItemProps.onClick(e);
      onSelect(listItemProps.id);
    }}
    className={`!flex-initial ${className || ''} ${
      selected ? '!border-blue-500 !text-blue-500' : ''
    }`}
  />
);

const LayoutSelection = ({
  Header,
  children,
  items,
  selected,
  onSelect,
  onAdd,
  addLabel = 'add',
  searchLabel = '',
  leftPanelWidth = 8,
  itemPerPage = DEFAULT_ITEM_PER_PAGE,
}: LayoutSelectionProps) => {
  const [searchValue, SetSearchValue] = useState('');
  const [currentPage, setCurrentPage] = useState(1);
  const rightPanelWidth = 24 - leftPanelWidth;

  const filteredItems = useMemo(
    () =>
      items.filter(
        (item) =>
          item.title.toLowerCase().includes(searchValue.toLowerCase()) ||
          (typeof item.content === 'string' &&
            item.content.toLowerCase().includes(searchValue.toLowerCase())),
      ),
    [searchValue, items],
  );
  const paginatedItems = useMemo(
    () => paginate(filteredItems, itemPerPage, currentPage),
    [itemPerPage, currentPage, filteredItems],
  );

  const totalPages = Math.ceil(filteredItems.length / itemPerPage);

  return (
    <Row className="flex h-full flex-1">
      <Col span={leftPanelWidth} className="h-full">
        <SidePanel>
          <div className="flex w-full flex-col space-y-2 overflow-y-auto">
            {Header || null}
            <div className="mb-3 flex flex-col items-center">
              {onAdd && (
                <Button onClick={onAdd} className="!flex items-center">
                  {addLabel}
                  <PlusIcon />
                </Button>
              )}
              <SearchInput
                value={searchValue}
                onChange={(e) => SetSearchValue(e.target.value)}
                className="flex-1"
                placeholder={searchLabel}
              />
            </div>
            <div className={'flex flex-1 flex-col space-y-2 overflow-x-auto'}>
              {paginatedItems.map((item) => (
                <ListItemWithSelection
                  {...item}
                  key={item.id}
                  selected={item.id === selected}
                  onSelect={onSelect}
                />
              ))}
            </div>
            {totalPages > 1 && (
              <div className="flex flex-row items-center justify-between">
                <Button
                  onClick={() => setCurrentPage(currentPage - 1)}
                  disabled={currentPage === 1}
                >
                  <ChevronIcon className="rotate-[90deg]" />
                </Button>
                <div>
                  {currentPage} / {totalPages}
                </div>
                <Button
                  onClick={() => setCurrentPage(currentPage + 1)}
                  disabled={currentPage === totalPages}
                >
                  <ChevronIcon className="rotate-[-90deg]" />
                </Button>
              </div>
            )}
          </div>
        </SidePanel>
      </Col>
      <Col span={rightPanelWidth} className="h-full">
        {children}
      </Col>
    </Row>
  );
};

export default LayoutSelection;
