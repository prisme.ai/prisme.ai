'use client';

import LoadingIcon from '@/svgs/loading.svg';
import styles from './loading.module.scss';

export interface LoadingProps {
  className?: string;
  spinClassName?: string;
}
export const Loading = ({ className, spinClassName }: LoadingProps) => (
  <div className={`${styles['loading-ctn']} ${className || ''}`}>
    <LoadingIcon className={`${styles['loading']} ${spinClassName || ''}`} />
  </div>
);

export default Loading;
