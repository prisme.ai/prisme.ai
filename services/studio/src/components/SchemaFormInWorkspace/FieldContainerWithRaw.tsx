import { Children, ReactElement, useCallback, useMemo, useState } from 'react';
import { useField } from 'react-final-form';
import { CodeEditorInline } from '@/components/CodeEditor/lazy';
import { useTranslations } from 'next-intl';
import { FieldProps, Schema } from '@/components/SchemaForm';
import { Tooltip } from 'antd';
import CodeIcon from '@/svgs/code.svg';
import styles from './schema-form-in-workspace.module.scss';

const typeIsOk = (value: any, type: Schema['type']) => {
  if (!type || !value) return true;

  const valueType = typeof value;

  switch (type) {
    case 'object':
      if (valueType === 'object') return true;
      try {
        JSON.parse(value);
        return true;
      } catch {
        return false;
      }
  }

  switch (valueType) {
    case 'string':
      return ['string', 'localized:string'].includes(type);
  }
  return true;
};

export const FieldContainerWithRaw = ({ schema, name, label, children, className }: FieldProps) => {
  const t = useTranslations('workspaces');
  const field = useField(name);
  const [value, setValue] = useState(field.input.value);
  const [displayRaw, setDisplayRaw] = useState(!typeIsOk(field.input.value, schema.type));

  const toggle = useCallback(() => {
    setDisplayRaw(!displayRaw);
    if (!displayRaw) {
      setValue(
        typeof field.input.value === 'string'
          ? field.input.value
          : JSON.stringify(field.input.value, null, '  '),
      );
    }
  }, [displayRaw, field.input.value]);

  const onChange = useCallback(
    (value: string) => {
      setValue(value);
      try {
        field.input.onChange(JSON.parse(value));
      } catch {
        field.input.onChange(value);
      }
    },
    [field.input],
  );

  const labelClassName = useMemo(() => {
    let className = '';
    if (!displayRaw) return className;
    Children.map(children, (child) => {
      const c = child as ReactElement;
      if (!c || c.type !== 'label') return;
      className = c.props.className;
    });
    return className;
  }, [children, displayRaw]);
  return (
    <div
      className={`pr-form-field ${className} ${
        (className || '').includes('pr-form-object') ? 'pt-[1rem]' : '' // We had some padding in order to be sure that the displayRaw button doesn't overlap another one
      }`}
    >
      <Tooltip title={t(`form.raw${displayRaw ? '_hide' : ''}`)} placement="left">
        <button
          type="button"
          className={`pr-form-raw absolute right-0 top-0 z-[1] mt-[0.35rem] flex flex-1 flex-row items-center text-[12px]`}
          onClick={toggle}
          tabIndex={-1}
        >
          <CodeIcon
            className={`${styles['code-icon']} ${displayRaw ? styles['code-icon--active'] : ''}`}
          />
        </button>
      </Tooltip>
      {!displayRaw && children}
      {displayRaw && (
        <>
          <label className={`pr-form-label pr-form-label--raw ${labelClassName}`}>
            {label || schema.title || name.replace(/^values./, '')}
          </label>
          <div className="pr-form-input">
            <CodeEditorInline
              mode="json"
              value={value}
              onChange={onChange}
              className={styles['code-editor']}
            />
          </div>
        </>
      )}
    </div>
  );
};

export default FieldContainerWithRaw;
