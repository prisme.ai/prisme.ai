import { Schema } from '@/components/SchemaForm';
import useTranslations from '@/i18n/useTranslations';
import { useWorkspaceRaw } from '@/providers/Workspace';
import { useCallback } from 'react';

export default function useExtractAutocompleteOptions() {
  const t = useTranslations('workspaces');
  const { workspace: { automations, pages, name: workspaceName, imports = {} } = {} } =
    useWorkspaceRaw() || {};
  const extractAutocompleteOptions = useCallback(
    function useSelectOptions(schema: Schema) {
      const { ['ui:options']: uiOptions = {} } = schema;

      function extract(type: 'listen' | 'emit') {
        return [
          ...Object.entries({ automations, pages }).flatMap(([key, list]) => {
            if (!list) return [];
            const events = new Set(
              Object.entries(list).flatMap(([, { events = {} }]) => {
                return events?.[type] || [];
              }),
            );
            if (events.size === 0) return [];
            return [
              {
                label: t.has(`events.autocomplete.label_${key}`)
                  ? t(`events.autocomplete.label_${key}`, {
                      workspace: workspaceName,
                    })
                  : t('events.autocomplete.label', {
                      workspace: workspaceName,
                    }),
                options: Array.from(events).map((event) => ({
                  label: event,
                  value: event,
                })),
              },
            ];
          }),
          ...Object.entries(imports).flatMap(([slug, { events = {} }]) => {
            if (!events || !events[type]) return [];
            const typedEvents = events[type];
            if (!typedEvents || typedEvents.length === 0) return [];

            return [
              {
                label: t('events.autocomplete.label_imports', {
                  app: slug,
                }),
                options: typedEvents.map((event) => ({
                  label: event,
                  value: event,
                })),
              },
            ];
          }),
        ];
      }
      switch ((uiOptions as { autocomplete: string }).autocomplete) {
        case 'events:listen': {
          return extract('listen');
        }
        case 'events:emit':
          return extract('emit');
      }

      return [];
    },
    [automations, pages, imports, t, workspaceName],
  );
  return extractAutocompleteOptions;
}
