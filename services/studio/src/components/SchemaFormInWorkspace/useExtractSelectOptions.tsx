import { Schema } from '@/components/SchemaForm';
import { useWorkspaceRaw } from '@/providers/Workspace';
import useLocalizedText from '@/utils/useLocalizedText';
import { Tooltip } from 'antd';
import { useCallback, useMemo } from 'react';
import { JSONPath } from 'jsonpath-plus';
import { usePageRaw } from '@/providers/Page';

export const readAppConfig = (appConfig: any, path: string) => {
  return JSONPath({ path, json: appConfig });
};

interface UseExtractSelectOptionsOptions {
  config?: Record<string, unknown>;
}

export default function useExtractSelectOptions({ config }: UseExtractSelectOptionsOptions = {}) {
  const { workspace, workspace: { pages, automations } = {} } = useWorkspaceRaw() || {};
  const { page } = usePageRaw() || {};
  const { localize } = useLocalizedText();
  const pageSections = useMemo(() => {
    if (!page) return [];
    const pageSections: string[] = [];
    function recursivelySearchSectionIds(o: typeof page) {
      if (o && typeof o === 'object') {
        Object.entries(o).forEach(([k, v]) => {
          if (k === 'sectionId') {
            pageSections.push(v);
          }
          if (typeof v === 'object') {
            recursivelySearchSectionIds(v);
          }
        });
      }
    }
    recursivelySearchSectionIds(page);
    return pageSections;
  }, [page]);

  const extractSelectOptions = useCallback(
    function useSelectOptions(schema: Schema) {
      const uiOptions: Record<string, unknown> = schema['ui:options'] || {};
      if (uiOptions.from === 'pages') {
        if (!pages) return [];
        return [
          {
            label: '',
            value: '',
          },
          ...Object.entries(pages).flatMap(([slug, { id, name = slug, description }]) => {
            return {
              label: (
                <div className={`flex flex-col ${!slug ? 'text-neutral-200' : ''}`}>
                  <div>{localize(name)}</div>
                  <div className="text-xs text-neutral-500">{localize(description)}</div>
                </div>
              ),
              value: slug ? `/${slug}` : id,
            };
          }),
        ];
      }

      if (uiOptions.from === 'automations') {
        if (!automations) return [];
        return [
          {
            label: '',
            value: '',
          },
          ...Object.entries(automations)
            .filter(([, { when: { endpoint = null } = {} }]) => {
              switch (uiOptions.filter) {
                case 'endpoint':
                  return endpoint;
                default:
                  return true;
              }
            })
            .map(([slug, { name, description }]) => ({
              label: (
                <Tooltip title={localize(description)}>
                  <div className="flex flex-1">{localize(name) || slug}</div>
                </Tooltip>
              ),
              value: slug,
            })),
        ];
      }

      if (uiOptions.from === 'config') {
        const path = (uiOptions.path as string) || '';
        if (!path) return [];
        const values: string[] = readAppConfig(config || workspace?.config, path) || [];
        return [
          {
            label: '',
            value: '',
          },
          ...values.map((value) => ({
            label: value,
            value,
          })),
        ];
      }

      if (uiOptions.from === 'pageSections') {
        return [
          {
            label: '',
            value: '',
          },
          ...(pageSections || []).map((sectionId: string) => ({
            label: sectionId,
            value: sectionId,
          })),
        ];
      }
      return [];
    },
    [pages, automations, config, pageSections, localize, workspace],
  );
  return extractSelectOptions;
}
