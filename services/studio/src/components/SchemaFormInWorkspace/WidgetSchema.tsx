import { useEffect, useState } from 'react';
import { useForm } from 'react-final-form';
import { get as _get } from 'lodash';
import { Schema, useSchemaForm } from '@/components/SchemaForm';
import equal from 'fast-deep-equal';
import FieldObject from '@/components/SchemaForm/FieldObject';

function computePath(path: string, values: {}) {
  return path.replace(/\{\{(.+)\}\}/g, (match, $1) => {
    return _get(values, $1);
  });
}

function cleanProperty(property: NonNullable<Schema['properties']>[string]): Schema {
  if (property.type === 'object' && property.properties) return cleanChildSchema(property);
  if (property.type !== 'object' || property.properties) return property;
  const { type, ...cleaned } = property;
  void type;
  return cleaned;
}
/**
 * Some schema like the ones from Custom Code comes with a object type but no
 * properties which leads to display an uneditable field.
 * Removing type for this kind transform field into a any wich let set any value
 * in a code editor.
 */
function cleanChildSchema(schema: Schema): Schema {
  if (schema.type === 'object') {
    return {
      ...schema,
      properties: Object.entries(schema.properties || {}).reduce(
        (prev, [key, next]) => ({
          ...prev,
          [key]: cleanProperty(next),
        }),
        {},
      ),
    };
  }
  return schema;
}

const FieldObjectC = FieldObject();

export const WidgetSchema = ({ name, schema }: any) => {
  const form = useForm();
  const { from, path } = schema['ui:options'] || {};
  const {
    utils: { extractFromConfig },
  } = useSchemaForm();
  const [childSchema, setChildSchema] = useState<Schema | null>(null);

  useEffect(() => {
    if (!extractFromConfig) return;
    setChildSchema((prev) => {
      const computedPath = computePath(path, form.getState().values);
      if (from !== 'config') return prev;
      const childSchema = extractFromConfig(computedPath);
      if (equal(childSchema, prev)) return prev;
      if (!childSchema || typeof childSchema !== 'object' || Array.isArray(childSchema)) {
        return prev;
      }
      if (childSchema.type) {
        return cleanChildSchema(childSchema);
      } else {
        const { title, description } = schema;
        return cleanChildSchema({
          title,
          description,
          type: 'object',
          properties: {
            ...childSchema,
          },
        });
      }
    });
  }, [extractFromConfig, form, from, path, schema]);

  if (!childSchema) return null;

  return <FieldObjectC schema={childSchema} name={name} />;
};
export default WidgetSchema;
