import FilePDF from '@/svgs/file-pdf.svg';
import FileWord from '@/svgs/file-word.svg';
import FileExcel from '@/svgs/file-excel.svg';
import FileAudio from '@/svgs/file-audio.svg';
import FileText from '@/svgs/file-text.svg';
import styles from './preview.module.scss';
import FilePPTIcon from '@/svgs/file-ppt.svg';

export default function getPreview(mimetype: string, url: string) {
  const [type] = mimetype.split(/\//);
  if (type === 'image') {
    return url;
  }

  if (mimetype === 'application/pdf') {
    return <FilePDF className={styles['icon']} />;
  }
  if (
    mimetype.includes('officedocument.wordprocessingml') ||
    mimetype.includes('msword') ||
    mimetype.includes('ms-word')
  ) {
    return <FileWord className="!text-accent flex items-center text-4xl" />;
  }

  if (mimetype.includes('officedocument.spreadsheetml') || mimetype.includes('ms-excel')) {
    return <FileExcel className={styles['icon']} />;
  }

  if (mimetype.includes('officedocument.presentationml') || mimetype.includes('ms-powerpoint')) {
    return <FilePPTIcon className={styles['icon']} />;
  }
  if (type === 'audio') {
    return <FileAudio className={styles['icon']} />;
  }
  return <FileText className={styles['icon']} />;
}
