import { ReactNode } from 'react';
import { FieldInputProps, FieldMetaState, FieldProps as FProps, useField } from 'react-final-form';
import { isFormFieldValid } from '../../utils/forms';
import styles from './field.module.scss';

interface FieldProps extends Partial<FProps<any, any>> {
  label?: string | ReactNode;
  className?: string;
  containerClassName?: string;
  children:
    | ReactNode
    | ((props: {
        className: string;
        input: FieldInputProps<any, HTMLElement>;
        meta: FieldMetaState<any>;
      }) => ReactNode);
}
export const FieldContainer = ({
  label,
  className,
  children,
  name = '',
  containerClassName,
  ...fieldProps
}: FieldProps) => {
  const { input, meta } = useField(name, fieldProps);
  return (
    <div className={`${styles['field-ctn']} ${containerClassName || ''}`}>
      <span className={`${styles['field']} ${className || ''}`}>
        {label && (
          <label className={styles['field-label']} htmlFor={name}>
            {label}
          </label>
        )}
        {typeof children === 'function'
          ? children({
              input,
              meta,
              className: `${styles['field-input']} ${isFormFieldValid(meta) ? 'ant-input-status-error' : ''}`,
            })
          : children}
      </span>
    </div>
  );
};

export default FieldContainer;
