import Page from '@/layouts/Pages/Page';
import { usePageContent } from '@/providers/PageContent';
import { useEffect } from 'react';

export default function PagePreview() {
  const { setPage, page } = usePageContent();
  useEffect(() => {
    const listener = (e: MessageEvent) => {
      if (e.data.type === 'previewpage.update') {
        const { page } = e.data;
        setPage(page);
      }
    };
    window.addEventListener('message', listener);
    window.parent.postMessage({ type: 'previewpage:init' }, '*');
    return () => {
      window.removeEventListener('message', listener);
    };
  }, [setPage]);

  if (!page) return null;

  return <Page />;
}
