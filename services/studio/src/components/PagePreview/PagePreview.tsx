import Page from '@/layouts/Pages/Page';
import { PublicBlocksProvider } from '@/providers/BlocksProvider';
import { PageContentProvider } from '@/providers/PageContent';
import { useEffect, useState } from 'react';
import LinkInPreview from './LinkInPreview';

export const PagePreview = () => {
  const [page, setPage] = useState<Prismeai.DetailedPage>();
  useEffect(() => {
    const listener = (e: MessageEvent) => {
      if (e.data.type === 'previewpage.update') {
        const { page } = e.data;
        setPage(page);
      }
    };
    window.addEventListener('message', listener);
    window.parent.postMessage({ type: 'previewpage:init' }, '*');
    return () => {
      window.removeEventListener('message', listener);
    };
  }, []);

  if (!page) return null;

  return (
    <PublicBlocksProvider components={{ Link: LinkInPreview }}>
      <PageContentProvider slug={page.slug} page={page}>
        <Page />
      </PageContentProvider>
    </PublicBlocksProvider>
  );
};
export default PagePreview;
