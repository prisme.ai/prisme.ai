import { Tooltip } from 'antd';
import { useTranslations } from 'next-intl';
import Link, { LinkProps } from 'next/link';
import { useCallback } from 'react';

export default function LinkInPreview(props: LinkProps) {
  const t = useTranslations('pages');
  const url = props.href.toString();
  const edit = useCallback(() => {
    window.parent.postMessage({ type: 'previewpage:navigate', url }, '*');
  }, [url]);
  const isInternal = !url.match(/^http/);
  if (isInternal) {
    return (
      <Tooltip
        trigger={['click']}
        title={<button onClick={edit}>{t('link.edit', { url })}</button>}
      >
        <Link
          {...props}
          onClick={(e) => {
            e.preventDefault();
          }}
        />
      </Tooltip>
    );
  }
  return (
    <Link
      {...props}
      onClick={(e) => {
        e.preventDefault();
        window.open(url);
      }}
    />
  );
}
