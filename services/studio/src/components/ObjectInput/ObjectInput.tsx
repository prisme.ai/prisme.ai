import { Button, Input, Tooltip } from 'antd';
import { ReactNode, useCallback } from 'react';
import TrashIcon from '@/svgs/trash.svg';
import styles from './object-input.module.scss';

interface ObjectInputProps {
  value: Record<string, string>;
  onChange: (v: ObjectInputProps['value']) => void;
  label?: string | ReactNode;
  keyLabel?: string;
  valueLabel?: string;
  removeLabel?: string;
  deleteIconClassName?: string;
}

const EMPTY_VALUE = {};

export const ObjectInput = ({
  value = EMPTY_VALUE,
  onChange,
  label,
  keyLabel,
  valueLabel,
  removeLabel,
  deleteIconClassName = '',
}: ObjectInputProps) => {
  const updateKey = useCallback(
    (prevKey: string) => (newKey: string) => {
      onChange(
        Object.entries(value).reduce(
          (prev, [k, v]) => ({
            ...prev,
            [k === prevKey ? newKey : k]: v,
          }),
          {},
        ),
      );
    },
    [onChange, value],
  );
  const updateValue = useCallback(
    (key: string) => (newValue: string) => {
      onChange(
        Object.entries(value).reduce(
          (prev, [k, v]) => ({
            ...prev,
            [k]: k === key ? newValue : v,
          }),
          {},
        ),
      );
    },
    [onChange, value],
  );
  const removeKey = useCallback(
    (key: string) => () => {
      onChange(
        Object.entries(value).reduce(
          (prev, [k, v]) =>
            k === key
              ? prev
              : {
                  ...prev,
                  [k]: v,
                },
          {},
        ),
      );
    },
    [onChange, value],
  );
  return (
    <div className={styles['container']}>
      {label}
      {Object.entries(value).map(([key, v], index) => (
        <div key={index} className={styles['field']}>
          <label>
            {keyLabel}
            <Input value={key} onChange={({ target: { value } }) => updateKey(key)(value)} />
          </label>
          <span className={styles['separator']}> : </span>
          <div className={styles['value-ctn']}>
            <label className={styles['value-label']}>
              {valueLabel}
              <Input value={v} onChange={({ target: { value } }) => updateValue(key)(value)} />
              <Tooltip title={removeLabel} placement="left">
                <Button onClick={removeKey(key)} className={styles['value-btn']} type="text">
                  <TrashIcon className={deleteIconClassName} />
                </Button>
              </Tooltip>
            </label>
          </div>
        </div>
      ))}
    </div>
  );
};

export default ObjectInput;
