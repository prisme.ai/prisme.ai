import { useTranslations } from 'next-intl';
import SchemaForm, { defaultUploadAccept, UiOptionsUpload } from '@/components/SchemaForm';

interface UploadOptionsProps {
  value: Partial<UiOptionsUpload>;
  onChange: (v: UiOptionsUpload) => void;
}

export const UploadOptions = ({ value, onChange }: UploadOptionsProps) => {
  const t = useTranslations('common');

  return (
    <SchemaForm
      schema={{
        type: 'object',
        properties: {
          upload: {
            type: 'object',
            title: t('schemaForm.builder.uiOptions.upload.title'),
            properties: {
              public: {
                type: 'boolean',
                title: t('schemaForm.builder.uiOptions.upload.public.title'),
                description: t('schemaForm.builder.uiOptions.upload.public.description'),
                default: false,
              },
              accept: {
                type: 'string',
                title: t('schemaForm.builder.uiOptions.upload.accept.title'),
                description: t('schemaForm.builder.uiOptions.upload.accept.description'),
                default: defaultUploadAccept,
              },
            },
          },
        },
      }}
      buttons={[]}
      initialValues={value}
      onChange={onChange}
    />
  );
};

export default UploadOptions;
