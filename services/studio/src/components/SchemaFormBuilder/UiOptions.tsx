import { useMemo } from 'react';
import SliderOptions from './SliderOptions';
import TagsOptions, { TagsOptionsProps } from './TagsOptions';
import UploadOptions from './UploadOptions';
import { Schema } from '@/components/SchemaForm';
import { useTranslations } from 'next-intl';
import { Collapse } from 'antd';
import styles from './schema-form-builder.module.scss';

interface UiOptionsProps {
  value: Schema;
  onChange: (schema: Schema) => void;
}

const WidgetsWithOptions = ['slider', 'tags', 'upload'];

const getOptionsForm = (widget: (typeof WidgetsWithOptions)[number]) => {
  switch (widget) {
    case 'slider':
      return SliderOptions;
    case 'tags':
      return TagsOptions;
    case 'upload':
      return UploadOptions;
  }
};

export const UiOptions = ({ value, onChange }: UiOptionsProps) => {
  const t = useTranslations('common');
  const widget = value?.['ui:widget'];
  const hasOptions = widget && typeof widget === 'string' && WidgetsWithOptions.includes(widget);

  const items = useMemo(() => {
    if (!widget || !hasOptions) return null;
    const Component = getOptionsForm(widget);
    if (!Component) return null;
    return [
      {
        label: (
          <div className={styles['ui-options-label']}>
            {t('schemaForm.builder.uiOptions.label')}
          </div>
        ),
        children: (
          <Component
            value={(value['ui:options'] as TagsOptionsProps['value']) || {}}
            onChange={(newValue) => {
              onChange({
                ...value,
                ['ui:options']: newValue,
              });
            }}
          />
        ),
      },
    ];
  }, [hasOptions, onChange, t, value, widget]);

  if (!hasOptions || !items) return null;

  return <Collapse items={items} className={styles['ui-options-collapse']} />;
};

export default UiOptions;
