import { Button, Tooltip } from 'antd';
import { useField } from 'react-final-form';
import Properties from './Properties';
import { useTranslations } from 'next-intl';
import { FieldProps } from '../SchemaForm/types';
import InfoIcon from '@/svgs/info.svg';
import PlusIcon from '@/svgs/plus.svg';

interface ArgumentsEditorProps extends FieldProps {
  title?: string;
  description?: string;
  add?: string;
}

export const ArgumentsEditor = ({
  name,
  title = 'automations.arguments.title',
  description = 'automations.arguments.description',
  add = 'automations.arguments.add',
}: ArgumentsEditorProps) => {
  const field = useField(name);
  const t = useTranslations('workspaces');

  return (
    <div className="m-[1rem] flex flex-1 flex-col">
      <div className="flex flex-1 items-baseline justify-between">
        <div>
          <label className="font-bold">{t(title)}</label>
          <Tooltip title={t(description)} placement="left">
            <button type="button" className="ml-2">
              <InfoIcon />
            </button>
          </Tooltip>
        </div>
        <Tooltip title={t(add)} placement="left">
          <Button
            className="-mr-2"
            onClick={() =>
              field.input.onChange({
                ...field.input.value,
                properties: {
                  ...field.input.value.properties,
                },
              })
            }
          >
            <PlusIcon />
          </Button>
        </Tooltip>
      </div>
      <div className="!rounded-[0.3rem]">
        <Properties value={field.input.value} onChange={field.input.onChange} />
      </div>
    </div>
  );
};

export default ArgumentsEditor;
