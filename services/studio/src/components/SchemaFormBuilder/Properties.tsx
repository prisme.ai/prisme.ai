import { Button, Collapse, Divider, Input, Tooltip } from 'antd';
import { useCallback } from 'react';
import SchemaFormBuilder from './SchemaFormBuilder';
import { Schema } from '@/components/SchemaForm';
import { useTranslations } from 'next-intl';
import TrashIcon from '@/svgs/trash.svg';

interface PropertiesProps {
  value: Record<string, Schema>;
  onChange: (v: Record<string, Schema>) => void;
  addLabel?: string;
}

export const Properties = ({ value, onChange }: PropertiesProps) => {
  const t = useTranslations('common');
  const update = useCallback(
    (updatedKey: keyof typeof value) => (schema: Schema) => {
      onChange(
        Object.keys(value).reduce(
          (prev, key) => ({
            ...prev,
            [key]: key === updatedKey ? schema : value[key],
          }),
          {},
        ),
      );
    },
    [onChange, value],
  );

  const updateKey = useCallback(
    (prevKey: keyof typeof value) => (newKey: string) => {
      onChange(
        Object.keys(value).reduce((prev, key) => {
          return {
            ...prev,
            [prevKey === key ? newKey : key]: value[key],
          };
        }, {}),
      );
    },
    [onChange, value],
  );

  const remove = useCallback(
    (oldKey: string) => () => {
      onChange(
        Object.keys(value).reduce(
          (prev, key) => (key === oldKey ? prev : { ...prev, [key]: value[key] }),
          {},
        ),
      );
    },
    [onChange, value],
  );

  return (
    <Collapse className="pr-collapse-light flex flex-1 flex-col" bordered={false}>
      {Object.entries(value || {}).map(([key, v], i) => (
        <Collapse.Panel
          key={i}
          className="-mx-[1rem] flex flex-1 flex-col !p-4"
          header={
            <div
              className="relative flex flex-1"
              onClick={(e) => {
                const target = e.target as HTMLElement;
                if (target.nodeName.toLowerCase() === 'label') return;
                e.stopPropagation();
              }}
            >
              <label>
                {t('schemaForm.builder.property.name')}
                <Input
                  value={key}
                  onChange={({ target: { value } }) => updateKey(key)(value)}
                  pattern={/^[a-zA-Z0-9_]+$/.source}
                />
              </label>
              <div className="absolute -right-2 -top-3">
                <Tooltip title={t('schemaForm.builder.property.delete')} placement="left">
                  <Button onClick={remove(key)}>
                    <TrashIcon />
                  </Button>
                </Tooltip>
              </div>
            </div>
          }
        >
          <div className="ml-7 flex flex-1 flex-col">
            <SchemaFormBuilder value={v} onChange={update(key)} />
            <Divider />
          </div>
        </Collapse.Panel>
      ))}
    </Collapse>
  );
};

export default Properties;
