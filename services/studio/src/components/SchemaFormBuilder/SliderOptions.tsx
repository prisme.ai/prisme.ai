import { Button, Collapse, Input, Tooltip } from 'antd';
import { useCallback } from 'react';
import RichTextEditor from '../RichTextEditor';
import { UiOptionsSlider } from '../SchemaForm/types';
import { useTranslations } from 'next-intl';
import PlusIcon from '@/svgs/plus.svg';
import TrashIcon from '@/svgs/trash.svg';
import styles from './schema-form-builder.module.scss';

interface SliderOptionsProps {
  value: Partial<UiOptionsSlider>;
  onChange: (v: UiOptionsSlider) => void;
}

export const SliderOptions = ({ value, onChange }: SliderOptionsProps) => {
  const t = useTranslations('common');

  const addStep = useCallback(() => {
    onChange({
      slider: {
        ...value?.slider,
        steps: [
          ...(value?.slider?.steps || []),
          {
            label: '',
            description: '',
            value: 0,
          },
        ],
      },
    });
  }, [onChange, value]);

  const removeStep = useCallback(
    (index: number) => () => {
      onChange({
        slider: {
          ...value?.slider,
          steps: (value?.slider?.steps || []).filter((v, k) => k !== index),
        },
      });
    },
    [onChange, value?.slider],
  );

  const udpateStepValue = useCallback(
    (index: number) => (key: string) => (newValue: string | number) => {
      onChange({
        slider: {
          ...value?.slider,
          steps: (value?.slider?.steps || []).map((item, k) =>
            k === index
              ? {
                  ...item,
                  [key]: newValue,
                }
              : item,
          ),
        },
      });
    },
    [onChange, value?.slider],
  );

  return (
    <div>
      <label className={styles['slider-options']}>
        {t('schemaForm.builder.uiOptions.slider.steps.label')}
        <div className={styles['slider-options-add-btn']}>
          <Tooltip title={t('schemaForm.builder.uiOptions.slider.steps.add')} placement="left">
            <Button onClick={addStep} htmlType="button">
              <PlusIcon />
            </Button>
          </Tooltip>
        </div>
      </label>
      <div>
        {(value?.slider?.steps || []).map(({ description, label, value }, key) => (
          <div key={key} className={styles['slider-options-step']}>
            <div className={styles['slider-options-step-section']}>
              <div className={styles['slider-options-step-field-value']}>
                <label htmlFor={`value-${key}`}>
                  {t('schemaForm.builder.uiOptions.slider.steps.value.label')}
                </label>
                <Input
                  id={`value-${key}`}
                  type="number"
                  value={value}
                  onChange={({ target: { value } }) => udpateStepValue(key)('value')(+value)}
                />
              </div>
              <div className={styles['slider-options-step-field-label']}>
                <label htmlFor={`label-${key}`}>
                  {t('schemaForm.builder.uiOptions.slider.steps._label.label')}
                </label>
                <Input
                  id={`label-${key}`}
                  value={label}
                  onChange={({ target: { value } }) => udpateStepValue(key)('label')(value)}
                />
              </div>
            </div>
            <Collapse
              className={`${styles['ui-options-collapse']} ${styles['slider-options-step-field-description']}`}
              items={[
                {
                  label: (
                    <label className={styles['ui-options-label']}>
                      {t('schemaForm.builder.uiOptions.slider.steps.description.label')}
                    </label>
                  ),
                  children: (
                    <RichTextEditor
                      value={description}
                      onChange={udpateStepValue(key)('description')}
                    />
                  ),
                },
              ]}
            />
            <Tooltip title={t('schemaForm.builder.uiOptions.slider.steps.remove')} placement="left">
              <button
                onClick={removeStep(key)}
                className={styles['slider-options-delete-btn']}
                type="button"
              >
                <TrashIcon />
              </button>
            </Tooltip>
          </div>
        ))}
      </div>
    </div>
  );
};

export default SliderOptions;
