import { Button, Input, Tooltip } from 'antd';
import { ChangeEvent, useCallback } from 'react';
import { UiOptionsSelect } from '@/components/SchemaForm/types';
import { useTranslations } from 'next-intl';
import SchemaForm from '@/components/SchemaForm';
import TrashIcon from '@/svgs/trash.svg';
import InfoIcon from '@/svgs/info.svg';
import PlusIcon from '@/svgs/plus.svg';

interface Value {
  enum?: string[];
  enumNames?: string[];
  'ui:options'?: { select: Omit<UiOptionsSelect['select'], 'options'> };
}
interface EnumProps {
  value: Value;
  onChange: (v: Value) => void;
}
export const Enum = ({ value, onChange }: EnumProps) => {
  const t = useTranslations('common');
  const addValue = useCallback(() => {
    const newValue = { ...value, enum: value.enum || [] };
    const index = newValue.enum.push('');
    if (value.enumNames && !value.enumNames[index]) {
      value.enumNames[index] = '';
    }
    onChange(newValue);
  }, [onChange, value]);

  const deleteValue = useCallback(
    (index: number) => () => {
      const newEnum = (value.enum || []).filter((v, k) => k !== index);
      const newEnumNames = (value.enumNames || []).filter((v, k) => k !== index);
      const { enum: _enum, enumNames, ...prevValue } = value;
      void _enum, enumNames;
      const newValue: any = prevValue;
      if (newEnum.length) {
        newValue.enum = newEnum;
      }
      if (newEnumNames.length) {
        newValue.enumNames = newEnumNames;
      }

      onChange(newValue);
    },
    [onChange, value],
  );

  const updateValue = useCallback(
    (index: number) =>
      ({ target: { value: v } }: ChangeEvent<HTMLInputElement>) => {
        const newValue = { ...value, enum: value.enum || [] };
        newValue.enum[index] = v;
        onChange(newValue);
      },
    [onChange, value],
  );
  const updateLabel = useCallback(
    (index: number) =>
      ({ target: { value: v } }: ChangeEvent<HTMLInputElement>) => {
        const newEnumNames = [...(value.enumNames || [])];
        newEnumNames[index] = v;
        onChange({
          ...value,
          enumNames: newEnumNames.map((v) => v || ''),
        });
      },
    [onChange, value],
  );

  return (
    <div className="flex-1 flex-col">
      <div className="flex flex-1 flex-row justify-between">
        <div>
          <label className="font-bold">{t('schemaForm.builder.property.enum.label')}</label>
          <Tooltip title={t('schemaForm.builder.property.enum.description')} placement="right">
            <button type="button" className="ml-2">
              <InfoIcon />
            </button>
          </Tooltip>
        </div>
        <Tooltip title={t('schemaForm.builder.property.enum.add')} placement="left">
          <Button htmlType="button" onClick={addValue} className="-mt-2">
            <PlusIcon />
          </Button>
        </Tooltip>
      </div>
      <div>
        {(value.enum || []).map((v, k) => (
          <div key={k} className="relative mb-2 flex flex-1 flex-row items-center">
            <Input
              placeholder={t('schemaForm.builder.property.enum.key')}
              value={(value.enumNames || [])[k]}
              onChange={updateLabel(k)}
            />
            <span className="m-1 flex">:</span>
            <Input
              placeholder={t('schemaForm.builder.property.enum.value')}
              value={v}
              onChange={updateValue(k)}
            />
            <Tooltip title={t('schemaForm.builder.property.enum.remove')}>
              <button
                type="button"
                className="absolute right-2 top-[50%] text-xs text-neutral-500"
                onClick={deleteValue(k)}
              >
                <TrashIcon />
              </button>
            </Tooltip>
          </div>
        ))}
      </div>
      <SchemaForm
        schema={{
          type: 'boolean',
          title: t('schemaForm.builder.uiOptions.select.hideSearch.title'),
        }}
        buttons={[]}
        initialValues={value}
        onChange={(formVal) => {
          onChange({
            ...value,
            'ui:options': {
              ...(value?.['ui:options'] || {}),
              select: {
                ...(value?.['ui:options']?.select || {}),
                hideSearch: formVal,
              },
            },
          });
        }}
      />
    </div>
  );
};

export default Enum;
