import { useCallback, useMemo } from 'react';
import LocalizedInput, { LocalizedInputProps } from '@/components/LocalizedInput';
import Enum from './Enum';
import Properties from './Properties';
import UiOptions from './UiOptions';
import { Schema, schemaTypes, UiOptionsSelect, UIWidgetsByType } from '@/components/SchemaForm';
import { useTranslations } from 'next-intl';
import { Button, Select, Tooltip } from 'antd';
import InfoIcon from '@/svgs/info.svg';
import PlusIcon from '@/svgs/plus.svg';

const WidgetsByType: Record<string, readonly string[]> = {
  ...UIWidgetsByType,
  string: Array.from(new Set([...UIWidgetsByType.string.filter((t) => t !== 'select'), 'enum'])),
};

interface SchemaFormBuilderProps {
  value: Schema;
  onChange: (schema: Schema) => void;
}

export const SchemaFormBuilder = ({ value = {}, onChange }: SchemaFormBuilderProps) => {
  const t = useTranslations('common');

  const update = useCallback(
    (type: string) => (v: any) => {
      const newValue = { ...value };
      if (type === 'type') {
        if (v !== 'array') {
          delete newValue.items;
        }
        if (v !== 'object') {
          delete newValue.properties;
          delete newValue.additionalProperties;
        }

        if (v) {
          newValue.type = v;
        } else {
          delete newValue.type;
        }
        delete newValue['ui:widget'];

        return onChange({
          ...newValue,
        });
      }

      if (type === 'ui:widget') {
        const newValue = { ...value };
        if (['enum', 'radio'].includes(v)) {
          newValue.enum = newValue.enum || [];
          newValue.type = 'string';
          if (v === 'radio') {
            newValue['ui:widget'] = 'radio';
          } else {
            delete newValue['ui:widget'];
          }
        } else {
          delete newValue.enum;
          newValue['ui:widget'] = v;
          if (
            newValue.type &&
            v &&
            !(WidgetsByType[newValue.type as keyof typeof WidgetsByType] || []).includes(v)
          ) {
            delete newValue.type;
          }
        }

        return onChange(newValue);
      }

      if (type === 'enum') {
        const newValueWithEnum = {
          ...newValue,
          enum: v.enum,
          enumNames: v.enumNames,
        };

        const hideSearch = v?.['ui:options']?.select?.hideSearch;
        if (typeof hideSearch === 'boolean') {
          newValueWithEnum['ui:options'] = {
            select: {
              hideSearch,
            },
          };
        }

        if (!newValueWithEnum.enum) {
          delete newValueWithEnum.enum;
        }
        if (!newValueWithEnum.enumNames) {
          delete newValueWithEnum.enumNames;
        }

        return onChange(newValueWithEnum);
      }

      onChange({
        ...newValue,
        [type]: v,
      });
    },
    [onChange, value],
  );

  const options = useMemo(() => {
    const uiWidget = value['ui:widget'] || (value.enum ? 'enum' : null);

    const uiWidgetIsSet = !!(uiWidget && typeof uiWidget === 'string');
    const filteredTypes = uiWidgetIsSet
      ? Object.keys(WidgetsByType).flatMap((type) =>
          WidgetsByType[type as keyof typeof WidgetsByType].includes(uiWidget) ? [type] : [],
        )
      : schemaTypes;

    return [
      {
        label: t('schemaForm.builder.types.any'),
        value: '',
      },
      ...filteredTypes.map((value) => ({
        label: t(`schemaForm.builder.types.${value.replace(':', '_')}`),
        value,
      })),
    ];
  }, [t, value]);

  const uiWidget = useMemo(() => {
    const filteredWidgets = value.type
      ? WidgetsByType[value.type as keyof typeof WidgetsByType] || []
      : Array.from(new Set(Object.values(WidgetsByType).flatMap((widgets) => widgets)));

    return [
      {
        label: (
          <Tooltip title={t('schemaForm.builder.widget.default_description')}>
            {t('schemaForm.builder.widget.default')}
          </Tooltip>
        ),
        value: '',
      },
      ...filteredWidgets.map((widget) => ({
        label: (
          <Tooltip
            title={
              t.has(`schemaForm.builder.widget.description_${widget}`)
                ? t(`schemaForm.builder.widget.description_${widget}`)
                : t('schemaForm.builder.widget.description')
            }
          >
            {t.has(`schemaForm.builder.widget.name_${widget}`)
              ? t(`schemaForm.builder.widget.name_${widget}`)
              : t('schemaForm.builder.widget.name')}
          </Tooltip>
        ),
        value: widget,
      })),
    ];
  }, [t, value]);

  return (
    <div className="flex flex-1 flex-col">
      <div className="my-2 flex flex-row">
        <div className="mr-1 flex flex-1 flex-col">
          <div className="my-2 flex flex-row">
            <label className="font-bold">{t('schemaForm.builder.property.widget.label')}</label>
            <Tooltip title={t('schemaForm.builder.property.widget.description')} placement="right">
              <button type="button" className="ml-2">
                <InfoIcon />
              </button>
            </Tooltip>
          </div>
          <Select
            options={uiWidget || []}
            value={value.enum && !value['ui:widget'] ? 'enum' : value['ui:widget'] || ''}
            onChange={update('ui:widget')}
          />
        </div>

        <div className="ml-1 flex flex-1 flex-col">
          <div className="my-2 flex flex-row">
            <label className="font-bold">{t('schemaForm.builder.property.type.label')}</label>
            <Tooltip title={t('schemaForm.builder.property.type.description')} placement="right">
              <button type="button" className="ml-2">
                <InfoIcon />
              </button>
            </Tooltip>
          </div>
          <Select options={options} value={value.type || ''} onChange={update('type')} />
        </div>
      </div>
      {!!value.enum && (
        <div className="mt-2 flex flex-1 border-l-[1px] border-gray-200 pl-4">
          <Enum
            value={{
              ...value,
              'ui:options': {
                select: (value?.['ui:options'] as UiOptionsSelect)?.select as Omit<
                  UiOptionsSelect['select'],
                  'options'
                >,
              },
            }}
            onChange={update('enum')}
          />
        </div>
      )}
      <div>
        <div className="my-2 flex flex-row">
          <label className="font-bold">{t('schemaForm.builder.property.title')}</label>
        </div>
      </div>
      <LocalizedInput
        value={(value.title as LocalizedInputProps['value']) || ''}
        onChange={update('title')}
      />
      <div>
        <div className="my-2 flex flex-row">
          <label className="font-bold">{t('schemaForm.builder.property.description.label')}</label>
          <Tooltip
            title={t('schemaForm.builder.property.description.description')}
            placement="right"
          >
            <button type="button" className="ml-2">
              <InfoIcon />
            </button>
          </Tooltip>
        </div>
        <LocalizedInput
          value={(value.description as LocalizedInputProps['value']) || ''}
          onChange={update('description')}
        />
      </div>
      {value.type && (
        <div>
          <div className="my-2 flex flex-row">
            <label className="font-bold">
              {t('schemaForm.builder.property.placeholder.label')}
            </label>
            <Tooltip
              title={t('schemaForm.builder.property.placeholder.description')}
              placement="right"
            >
              <button type="button" className="ml-2">
                <InfoIcon />
              </button>
            </Tooltip>
          </div>
          <LocalizedInput value={value.placeholder || ''} onChange={update('placeholder')} />
        </div>
      )}

      {/*
      // Required is not already available
      <label className="flex text-gray my-4">
        <Switch
          checked={!!value.required}
          onChange={(checked) => update('required')(checked)}
          className="!mr-2"
        />
        {t('schemaForm.builder.property.required')}
      </label>*/}
      {value.type === 'array' && !['upload'].includes((value?.['ui:widget'] as string) || '') && (
        <div>
          <div className="flex flex-1 flex-row items-baseline justify-between">
            <div className="mb-4">
              <label className="font-bold">{t('schemaForm.builder.items.label')}</label>
              <Tooltip title={t('schemaForm.builder.items.description')} placement="right">
                <button type="button" className="ml-2">
                  <InfoIcon />
                </button>
              </Tooltip>
            </div>
          </div>
          <SchemaFormBuilder value={value.items || {}} onChange={update('items')} />
        </div>
      )}
      {value.type === 'object' && (
        <div className="flex flex-1 flex-col">
          <div className="flex flex-1 flex-row items-baseline justify-between">
            <div>
              <label className="font-bold">{t('schemaForm.builder.properties.label')}</label>
              <Tooltip title={t('schemaForm.builder.properties.description')} placement="right">
                <button type="button" className="ml-2">
                  <InfoIcon />
                </button>
              </Tooltip>
            </div>
            <Tooltip title={t('schemaForm.builder.properties.add')} placement="left">
              <Button
                className="-mr-2"
                onClick={() =>
                  onChange({
                    ...value,
                    properties: {
                      ...value.properties,
                      '': {
                        type: 'string',
                      },
                    },
                  })
                }
              >
                <PlusIcon />
              </Button>
            </Tooltip>
          </div>
          <Properties value={value.properties || {}} onChange={update('properties')} />
        </div>
      )}
      <UiOptions value={value} onChange={onChange} />
    </div>
  );
};

export default SchemaFormBuilder;
