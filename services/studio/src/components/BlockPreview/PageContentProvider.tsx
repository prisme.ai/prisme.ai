import { pageContentContext } from '@/providers/PageContent';
import usePageEvents from '@/providers/PageContent/usePageEvents';
import { ReactNode, useState } from 'react';

interface PageContentProviderProps {
  children: ReactNode;
}
export default function PageContentProvider({ children }: PageContentProviderProps) {
  const [page, setPage] = useState<Prismeai.DetailedPage | null>({
    slug: '',
    appInstances: [],
  });
  const { events } = usePageEvents(page);

  return (
    <pageContentContext.Provider
      value={{
        page,
        setPage,
        loading: false,
        async fetchPage() {
          return;
        },
        events,
        error: null,
      }}
    >
      {children}
    </pageContentContext.Provider>
  );
}
