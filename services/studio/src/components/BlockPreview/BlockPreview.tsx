import { usePageContent } from '@/providers/PageContent';
import { useEffect, useState } from 'react';
import BlockLoader from '@/components/PageContent/BlockLoader';

export default function BlockPreview() {
  const [config, setConfig] = useState({});
  const { setPage } = usePageContent();
  useEffect(() => {
    const listener = (e: MessageEvent) => {
      if (e.data.type === 'previewblock.update') {
        const { page, config: { blocks = [], css = '', ...config } = {} } = e.data;
        setPage(page);
        setConfig({ blocks, css, ...config });
      }
    };
    window.addEventListener('message', listener);
    window.parent.postMessage({ type: 'previewblock:init' }, '*');
    return () => {
      window.removeEventListener('message', listener);
    };
  }, [setPage]);

  // This make force load css at runtime
  const [mounted, setMounted] = useState(false);
  useEffect(() => {
    setMounted(true);
  }, []);

  if (!mounted) return null;
  return (
    <div className="page building-surface m-0 flex min-h-full max-w-[100vw] flex-1 flex-col p-0">
      <div className="page-blocks flex w-full flex-1 flex-col">
        <BlockLoader name="BlocksList" config={config} />
      </div>
    </div>
  );
}
