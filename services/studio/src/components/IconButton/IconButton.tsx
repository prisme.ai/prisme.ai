import { ButtonHTMLAttributes, ReactNode } from 'react';
import styles from './icon-button.module.scss';

interface IconButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  children?: ReactNode;
  icon: ReactNode;
}

export default function IconButton({ icon, children, ...props }: IconButtonProps) {
  return (
    <button {...props} className={`${styles['icon-button']} ${props.className || ''}`}>
      <span className={styles['icon-button-icon']}>{icon}</span>
      {children && <span className={styles['icon-button-text']}>{children}</span>}
    </button>
  );
}
