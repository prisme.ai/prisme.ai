import { Schema } from '@/components/SchemaForm';

const schema: Schema = {
  type: 'object',
  properties: {
    blocks: {
      'ui:widget': 'BlocksList',
    },
  },
};

export default schema;
