import { FieldProps, Schema, schemaFormUtils } from '@/components/SchemaForm';
import { Tooltip } from 'antd';
import { useField } from 'react-final-form';
import LocalizedInput from '@/components/LocalizedInput';
import RichTextEditor from '@/components/RichTextEditor';
import InfoIcon from '@/svgs/info.svg';

const Editor = ({ name, schema, label }: FieldProps) => {
  const field = useField(name);
  const {
    input: { value: allowScripts },
  } = useField('values.allowScripts');
  const {
    input: { value: allowUnsecure },
  } = useField('values.allowUnsecure');

  return (
    <div className="pr-form-field">
      <label className="pr-form-label">
        {label || schema.title || schemaFormUtils.getLabel(name)}
      </label>
      {schema.description && (
        <Tooltip title={schema.description} placement="right">
          <button type="button" className="pr-form-description">
            <InfoIcon />
          </button>
        </Tooltip>
      )}
      <div className="pr-form-input">
        <LocalizedInput
          Input={RichTextEditor}
          InputProps={{ htmlModeOnly: allowScripts || allowUnsecure }}
          {...field.input}
          unmountOnLangChange
        />
      </div>
    </div>
  );
};

const schema: Schema = {
  type: 'object',
  properties: {
    content: {
      type: 'localized:string',
      title: 'pages.blocks.richtext.settings.content.label',
      description: 'pages.blocks.richtext.settings.content.description',
      'ui:widget': Editor,
    },
    allowUnsecure: {
      type: 'boolean',
      title: 'pages.blocks.richtext.settings.allowUnsecure.label',
      description: 'pages.blocks.richtext.settings.allowUnsecure.description',
    },
    markdown: {
      type: 'boolean',
      title: 'pages.blocks.richtext.settings.markdown.label',
      description: 'pages.blocks.richtext.settings.markdown.description',
      default: true,
    },
    tag: {
      type: 'string',
      title: 'pages.blocks.richtext.settings.tag.label',
      description: 'pages.blocks.richtext.settings.tag.description',
    },
  },
};

export default schema;
