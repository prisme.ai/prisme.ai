import { useField } from 'react-final-form';
import Properties from '@/components/SchemaFormBuilder/Properties';
import { Button, Tooltip } from 'antd';
import { useTranslations } from 'next-intl';
import { FieldProps, Schema } from '@/components/SchemaForm';
import InfoIcon from '@/svgs/info.svg';
import PlusIcon from '@/svgs/plus.svg';

const SchemaEditor = ({ name }: FieldProps) => {
  const field = useField(name);
  const t = useTranslations('workspaces');

  return (
    <div className="pr-form-field">
      <label className="pr-form-label">{t('pages.blocks.form.schema.label')}</label>
      <div className="pr-form-description">
        <Tooltip title={t('pages.blocks.form.schema.description')} placement="left">
          <InfoIcon />
        </Tooltip>
      </div>
      <div className="absolute -top-2 right-2">
        <Tooltip title={t('pages.blocks.form.schema.add')} placement="left">
          <Button
            onClick={() =>
              field.input.onChange({
                ...field.input.value,
                properties: {
                  ...field.input.value.properties,
                  '': { type: 'string' },
                },
              })
            }
          >
            <PlusIcon />
          </Button>
        </Tooltip>
      </div>
      <div className="pr-form-input">
        <Properties
          value={field.input.value.properties}
          onChange={(v: Record<string, Schema>) =>
            field.input.onChange({
              type: 'object',
              properties: v,
            })
          }
        />
      </div>
    </div>
  );
};

const schema: Schema = {
  type: 'object',
  properties: {
    schema: {
      type: 'object',
      'ui:widget': SchemaEditor,
    },
    submitLabel: {
      type: 'localized:string',
      title: 'pages.blocks.form.submitLabel.label',
      description: 'pages.blocks.form.submitLabel.description',
    },
    hideSubmit: {
      type: 'boolean',
      title: 'pages.blocks.form.hideSubmit.label',
      description: 'pages.blocks.form.hideSubmit.description',
    },
    onSubmit: {
      type: 'string',
      title: 'pages.blocks.form.onSubmit.label',
      description: 'pages.blocks.form.onSubmit.description',
    },
    onChange: {
      type: 'string',
      title: 'pages.blocks.form.onChange.label',
      description: 'pages.blocks.form.onChange.description',
    },
    disabledSubmit: {
      type: 'boolean',
      title: 'pages.blocks.form.disabledSubmit.label',
      description: 'pages.blocks.form.disabledSubmit.description',
    },
    values: {
      title: 'pages.blocks.form.values.label',
      description: 'pages.blocks.form.values.description',
    },
    autoFocus: {
      type: 'boolean',
      title: 'pages.blocks.form.autoFocus.label',
      description: 'pages.blocks.form.autoFocus.description',
    },
  },
};

export default schema;
