import React, { useCallback, useMemo } from 'react';
import BlocksListEditor from '@/components/BlocksListEditor';

interface PageBuilderProps {
  value: Prismeai.Page['blocks'];
  onChange: (value: Prismeai.Page['blocks'], events?: string[]) => void;
}

export const PageBuilder = ({ value, onChange }: PageBuilderProps) => {
  const pageAsBlocksList = useMemo(
    () => ({
      slug: 'BlocksList',
      blocks: value,
    }),
    [value],
  );

  const onPageBlocksChange = useCallback(
    ({ blocks }: Prismeai.Block) => {
      onChange(blocks);
    },
    [onChange],
  );
  return <BlocksListEditor value={pageAsBlocksList} onChange={onPageBlocksChange} />;
};

export default PageBuilder;
