import { useEffect, useRef } from 'react';
import { useQuill } from 'react-quilljs';
import Delta from 'quill-delta';
import MagicUrl from 'quill-magic-url';
import 'quill/dist/quill.snow.css';
import Quill, { QuillOptions } from 'quill';

Quill.register('modules/magicUrl', MagicUrl);

interface QuillProps {
  defaultValue?: string;
  onChange: (v: { text: string; html: string; delta: Delta }) => void;
  modules?: {
    toolbar?: {
      container?: string;
    };
  };
  options: QuillOptions;
}
export default function Qull({ defaultValue = '', onChange, options }: QuillProps) {
  const { quillRef, quill } = useQuill({
    ...options,
    modules: { magicUrl: true, ...options?.modules },
  });

  const defaultValueRef = useRef(typeof defaultValue === 'string' ? defaultValue : '');

  useEffect(() => {
    if (!quill) return;
    quill.clipboard.dangerouslyPasteHTML(defaultValueRef.current);
  }, [quill]);

  useEffect(() => {
    if (!quill) return;
    quill.on('text-change', () => {
      onChange?.({
        text: quill.getText(),
        html: quill.root.innerHTML,
        delta: quill.getContents(),
      });
    });
  }, [onChange, quill]);

  return <div ref={quillRef} />;
}
