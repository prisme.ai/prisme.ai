import { cloneElement, DetailedReactHTMLElement, ReactNode, useMemo } from 'react';
import { tokenize } from './tokenize';

interface HighlightProps {
  children: ReactNode;
  highlight?: string;
  component?: ReactNode;
}

export const Highlight = ({
  children,
  highlight = '',
  component = <span className="font-bold"></span>,
}: HighlightProps) => {
  const parts = useMemo(
    () =>
      typeof children === 'string'
        ? tokenize(children, highlight)
        : [{ text: children, highlight: null, start: null, end: null }],
    [children, highlight],
  );

  return (
    <>
      {parts.map(({ highlight, text, start, end }) =>
        highlight && component ? (
          cloneElement(component as DetailedReactHTMLElement<any, HTMLElement>, {
            key: `${start}-${end}`,
            children: text,
          })
        ) : (
          <span key={`${start}-${end}`}>{text}</span>
        ),
      )}
    </>
  );
};

export default Highlight;
