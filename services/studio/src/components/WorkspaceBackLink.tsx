'use client';

import Link from 'next/link';
import { useParams } from 'next/navigation';
import { ReactNode } from 'react';

export default function WorkspaceBackLink({ children }: { children: ReactNode }) {
  const { id } = useParams() || {};
  return <Link href={`/workspaces/${id}`}>{children}</Link>;
}
