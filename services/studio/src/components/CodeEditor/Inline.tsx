import * as React from 'react';

import CodeEditor, { CodeEditorProps } from './CodeEditor';

export const CodeEditorInline = ({ value, ...props }: CodeEditorProps) => {
  return (
    <React.Suspense fallback={<div />}>
      <CodeEditor
        {...props}
        value={typeof value === 'string' ? value : JSON.stringify(value, null, '  ') || ''}
        inline
      />
    </React.Suspense>
  );
};

export default CodeEditorInline;
