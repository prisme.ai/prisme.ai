import { useColorSchemeManager } from '@/providers/ColorSchemeManager';
import Editor, { EditorProps, Monaco } from '@monaco-editor/react';
import { editor } from 'monaco-editor';
import { useCallback, useEffect, useRef, useState } from 'react';
import Loading from '../Loading';
import styles from './code-editor.module.scss';
import { ErrorBoundary } from 'next/dist/client/components/error-boundary';
import TextArea from 'antd/es/input/TextArea';

function isMac() {
  return navigator.platform.indexOf('Mac') === 0 || navigator.platform === 'iPhone';
}

export type Decoration = {
  range: [number, number, number, number];
  wholeLine?: boolean;
  icon: 'warning';
  message?: string;
};
export type Snippet = {
  label: string;
  documentation: string;
  insertText: string;
};
export type Link = {
  keyword: string;
  onClick: () => void;
  hoverMessage?: string;
};

export interface CodeEditorProps extends Omit<EditorProps, 'onChange'> {
  mode: 'javascript' | 'css' | 'html' | 'yaml' | 'json';
  value?: string;
  onChange?: (value: string) => void;
  placeholder?: string;
  height?: string;
  width?: string;
  readOnly?: boolean;
  shortcuts?: {
    name: string;
    bindKey: {
      mac: string;
      win: string;
    };
    exec: () => void;
  }[];
  inline?: boolean;
  decorations?: Decoration[];
  snippets?: {
    trigger?: RegExp;
    suggestions: Snippet[];
  };
  links?: Link[];
}

function isOpenLinkKey(e: { metaKey: boolean; ctrlKey: boolean }) {
  return isMac() ? e.metaKey : e.ctrlKey;
}

export default function CodeEditor({
  value,
  mode,
  onChange,
  shortcuts,
  inline,
  readOnly,
  decorations,
  className = '',
  snippets,
  links,
  ...props
}: CodeEditorProps) {
  const { scheme } = useColorSchemeManager();
  const editorRef = useRef<editor.IStandaloneCodeEditor>();
  const monacoRef = useRef<Monaco>();
  const decorationsRef = useRef<editor.IEditorDecorationsCollection>();
  const linkDecorationsRef = useRef<editor.IEditorDecorationsCollection>();
  const lineHeight = useRef(18);
  const shortcutsRef = useRef(shortcuts);
  // Sometimes, editor crash on  load. Display a Textarea instead.
  const [fallback, displayFallback] = useState(false);
  useEffect(() => {
    shortcutsRef.current = shortcuts;
  }, [shortcuts]);

  const updateEditorHeight = useCallback(() => {
    const { current: editor } = editorRef;
    if (!editor) return;
    const linesCount = editor.getModel()?.getLineCount() || 1;
    if (inline) {
      const nextHeight = `${lineHeight.current * (linesCount + 2)}px`;
      if (editor.getContainerDomNode().style.height === nextHeight) return;
      editor.getContainerDomNode().style.height = `${lineHeight.current * (linesCount + 2)}px`;
      editor.updateOptions({
        lineNumbers: linesCount > 1 ? 'on' : 'off',
        minimap: {
          enabled: linesCount > 5,
        },
      });
      editor.layout();
      return;
    }
    setInterval(() => {
      const parentNode = editor.getContainerDomNode().parentNode as HTMLDivElement;
      parentNode.style.height = `auto`;
      editor.layout();
      parentNode.style.height = `100%`;
      editor.layout();
    }, 20);
  }, [inline]);

  useEffect(() => {
    updateEditorHeight();
  }, [value, updateEditorHeight]);

  const setLinksDecorations = useCallback(
    (editor: editor.IStandaloneCodeEditor, monaco: Monaco) => {
      function updateLinksDecorations() {
        const model = editor.getModel();
        if (!model || !links) return;

        const decorations: editor.IModelDeltaDecoration[] = [];
        const text = model.getValue();
        links.forEach(({ keyword, hoverMessage = '' }, k) => {
          let match;
          const regex = new RegExp(`\\b${keyword}\\b`, 'g'); // Mot clé exact

          while ((match = regex.exec(text)) !== null) {
            decorations.push({
              range: new monaco.Range(
                model.getPositionAt(match.index).lineNumber,
                model.getPositionAt(match.index).column,
                model.getPositionAt(match.index + keyword.length).lineNumber,
                model.getPositionAt(match.index + keyword.length).column,
              ),
              options: {
                inlineClassName: `link-keyword link-keyword--${k}`,
                hoverMessage: { value: hoverMessage },
              },
            });
          }
        });

        linkDecorationsRef.current = editor.createDecorationsCollection(decorations);
      }

      const keydownListener = (e: KeyboardEvent) => {
        if (!isOpenLinkKey(e)) return;
        updateLinksDecorations();
      };
      window.addEventListener('keydown', keydownListener);
      const keyupListener = () => {
        linkDecorationsRef.current?.clear();
      };
      window.addEventListener('keyup', keyupListener);
      const { dispose } = editor.onMouseDown((e) => {
        if (!links || !isOpenLinkKey(e.event) || !e.target?.position?.lineNumber) return;
        if (e.target.element?.classList.contains('link-keyword')) {
          e.event.preventDefault();
          const k = (
            Array.from(e.target.element?.classList).find((className = '') =>
              className.match(/^link-keyword--/),
            ) || ''
          ).split(/--/)[1];
          links[+k]?.onClick?.();
        }
      });

      return () => {
        window.removeEventListener('keydown', keydownListener);
        window.removeEventListener('keyup', keyupListener);
        dispose();
      };
    },
    [links],
  );

  const setDecorations = useCallback(
    (editor: editor.IStandaloneCodeEditor, monaco: Monaco, decorations: Decoration[] = []) => {
      if (decorations.length) {
        editor.updateOptions({ glyphMargin: true });
        const decorationsCollection = decorations.map(
          ({ icon, range, message = '', wholeLine }) => ({
            range: new monaco.Range(...range),
            options: {
              isWholeLine: wholeLine,
              glyphMarginClassName: styles[`glyph-icon--${icon}`],
              glyphMarginHoverMessage: { value: message },
            },
          }),
        );
        if (!decorationsRef.current) {
          decorationsRef.current = editor.createDecorationsCollection(decorationsCollection);
        } else {
          decorationsRef.current.set(decorationsCollection);
        }
      } else {
        editor.updateOptions({ glyphMargin: false });
      }
    },
    [],
  );

  useEffect(() => {
    if (!editorRef.current || !monacoRef.current) return;
    setDecorations(editorRef.current, monacoRef.current, decorations);
  }, [decorations, setDecorations]);

  const setContainerClassNames = useCallback(
    (editor: editor.IStandaloneCodeEditor) => {
      const containerEl = editor.getContainerDomNode() as HTMLElement;
      containerEl.classList.add(styles['editor']);
      className && (containerEl.parentNode as HTMLDivElement).classList.add(className);
      if (inline) {
        containerEl.classList.add(styles['editor--inline']);
      }
    },
    [className, inline],
  );
  useEffect(() => {
    const { current } = editorRef;
    if (!current) return;
    setContainerClassNames(current);
  }, [setContainerClassNames]);

  return (
    <>
      <ErrorBoundary
        errorComponent={() => {
          displayFallback(true);
          return null;
        }}
      >
        {!fallback && (
          <Editor
            onMount={(editor, monaco) => {
              editorRef.current = editor;
              monacoRef.current = monaco;
              lineHeight.current = editor.getOption(monaco.editor.EditorOption.lineHeight);
              updateEditorHeight();
              setDecorations(editorRef.current, monacoRef.current, decorations);

              setContainerClassNames(editor);
              editor.onKeyDown((e) => {
                setTimeout(updateEditorHeight);
                shortcutsRef.current?.forEach(({ bindKey, exec }) => {
                  const [modifier, key] = (bindKey[isMac() ? 'mac' : 'win'] || '').split(/-/);
                  if (`Key${key.toUpperCase()}` !== e.code) return;
                  if (modifier === 'cmd' && !e.metaKey) return;
                  if (modifier === 'ctrl' && !e.ctrlKey) return;
                  e.stopPropagation();
                  e.preventDefault();
                  exec();
                });
              });
              if (isMac()) {
                editor.addCommand(monaco.KeyMod.CtrlCmd | monaco.KeyCode.KeyZ, () => {
                  editor.trigger('keyboard', 'undo', {});
                });

                editor.addCommand(monaco.KeyMod.CtrlCmd | monaco.KeyCode.KeyY, () => {
                  editor.trigger('keyboard', 'redo', {});
                });
              }

              if (snippets && snippets.suggestions.length) {
                monaco.languages.registerCompletionItemProvider('yaml', {
                  provideCompletionItems(model, position) {
                    const textUntilPosition = model.getValueInRange({
                      startLineNumber: position.lineNumber,
                      startColumn: 1,
                      endLineNumber: position.lineNumber,
                      endColumn: position.column,
                    });
                    const word = model.getWordUntilPosition(position);
                    if (snippets.trigger && !textUntilPosition.match(snippets.trigger)) return;
                    return {
                      suggestions: snippets.suggestions.map((snippet) => ({
                        range: {
                          startLineNumber: position.lineNumber,
                          endLineNumber: position.lineNumber,
                          startColumn: word.startColumn,
                          endColumn: word.endColumn,
                        },
                        kind: monaco.languages.CompletionItemKind.Snippet,
                        insertTextRules:
                          monaco.languages.CompletionItemInsertTextRule.InsertAsSnippet,
                        ...snippet,
                      })),
                    };
                  },
                });
              }

              setLinksDecorations(editor, monaco);
            }}
            {...props}
            theme={scheme === 'light' ? 'light' : 'vs-dark'}
            defaultLanguage={mode}
            value={typeof value === 'string' ? value : ''}
            onChange={(v) => {
              onChange?.(v || '');
            }}
            loading={<Loading />}
            options={{
              readOnly,
              tabSize: 2,
              detectIndentation: false,
              insertSpaces: true,
              tabCompletion: 'on',
              stickyTabStops: true,
              suggestOnTriggerCharacters: true,
              acceptSuggestionOnEnter: 'on',
              wordBasedSuggestions: 'currentDocument',
              quickSuggestions: {
                other: true,
                comments: false,
                strings: true,
              },
            }}
          />
        )}
      </ErrorBoundary>
      {fallback && (
        <TextArea value={value} onChange={({ target: { value } }) => onChange?.(value || '')} />
      )}
    </>
  );
}
