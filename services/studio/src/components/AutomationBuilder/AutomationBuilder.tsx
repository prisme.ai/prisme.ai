import { useEffect, useMemo, useRef, useState } from 'react';
import Graph from './Graph/Graph';
import BUILTIN_INSTRUCTIONS from '@prisme.ai/validation/instructions.json';
import { Instruction } from '@/components/AutomationBuilder/Graph/context';
import styles from './automation-builder.module.scss';
import { useWorkspace } from '@/providers/Workspace';
import useLocalizedText from '@/utils/useLocalizedText';
import { automationToGraph } from './automationToGraph';
import { useTranslations } from 'next-intl';
import { useNotifications } from '@/providers/Notifications';
import { Schema } from '../SchemaForm';
import useLocalizeBuiltinSchemas from './useLocalizeBuiltInSchema';
import { useUrls } from '@/utils/urls';
import FetchForm from './forms/FetchForm';
import SetForm from './forms/SetForm';

const HIDDEN_INSTRUCTIONS = ['ratelimit'];

interface AutomationBuilderProps {
  value: Prismeai.Automation;
  onChange: (v: Prismeai.Automation) => void;
}

function getInitialValue(slug: string) {
  switch (slug) {
    case 'conditions':
      return { default: [] };
    case 'all':
      return [];
    case 'repeat':
      return {
        do: [],
      };
  }
}

export default function AutomationBuilder({ value, onChange }: AutomationBuilderProps) {
  const t = useTranslations('workspaces');
  const { localize, localizeSchemaForm } = useLocalizedText();
  const localizeBuiltinSchemas = useLocalizeBuiltinSchemas();
  const {
    workspace: { automations, imports, photo, name, id: workspaceId },
  } = useWorkspace();
  const [added, setAdded] = useState('');
  const [deleted, setDeleted] = useState('');
  const notifications = useNotifications();
  const { generateEndpoint } = useUrls();

  const instructions = useMemo(() => {
    function getSchema(slug: string, schema: Schema): Schema {
      switch (slug) {
        case 'emit':
          return {
            ...schema,
            properties: {
              ...schema.properties,
              event: {
                ...schema.properties?.event,
                'ui:widget': 'autocomplete',
                'ui:options': {
                  autocomplete: 'events:listen',
                },
              },
            },
          };
        case 'fetch':
          return {
            type: 'object',
            'ui:widget': FetchForm,
          };
        case 'set':
          return {
            type: 'object',
            'ui:widget': SetForm,
            'ui:options': schema,
          };
        default:
          return localizeBuiltinSchemas(schema, slug);
      }
    }
    const instructions = [
      {
        name: t('automations.instruction.title_builtin'),
        icon: '',
        instructions: Object.entries(BUILTIN_INSTRUCTIONS)
          .filter(([slug]) => !HIDDEN_INSTRUCTIONS.includes(slug))
          .map(([slug, schema]) => {
            return {
              slug,
              name: t(`automations.instruction.label_${slug}`),
              description: t(`automations.instruction.description_${slug}`),
              schema: getSchema(slug, schema?.properties?.[slug as keyof typeof schema.properties]),
              initialValue() {
                return getInitialValue(slug);
              },
            } as Instruction;
          }),
      },
    ];
    if (automations) {
      instructions.push({
        name: t('automations.instruction.title_workspace', {
          name,
        }),
        icon: photo || '',
        instructions: Object.entries(automations).map(
          ([slug, { name, arguments: schema, description }]) => {
            const finalSchema: Schema =
              schema && Object.keys(schema).length
                ? {
                    type: 'object',
                    properties: localizeSchemaForm(schema as Schema),
                  }
                : {};
            finalSchema.type = finalSchema.type || 'object';
            if (finalSchema.type === 'object') {
              finalSchema.properties =
                finalSchema.properties &&
                typeof finalSchema.properties === 'object' &&
                !Array.isArray(finalSchema.properties)
                  ? finalSchema.properties
                  : {};
              finalSchema.properties.output = {
                type: 'string',
                title: t('automations.instruction.output.label'),
                description: t('automations.instruction.output.description'),
              } as Schema;
            }
            return {
              slug,
              name: localize(name),
              schema: finalSchema,
              description: localize(description),
              href: `/workspaces/${workspaceId}/automations/${slug}`,
            };
          },
        ),
      });
    }
    if (imports) {
      Object.values(imports).forEach(({ appSlug, slug, photo, automations }) => {
        if (!automations || automations.length === 0) return;
        instructions.push({
          name: `${appSlug} (${slug})`,
          icon: photo || '',
          instructions: automations.map(
            ({ slug: instructionSlug, name, arguments: instructionArguments, description }) => {
              const schema: Schema | undefined =
                instructionArguments && Object.keys(instructionArguments).length
                  ? {
                      type: 'object',
                      properties: localizeSchemaForm(instructionArguments as Schema),
                    }
                  : undefined;
              if (schema) {
                schema.properties =
                  schema.properties &&
                  typeof schema.properties === 'object' &&
                  !Array.isArray(schema.properties)
                    ? schema.properties
                    : {};
                schema.properties.output = schema.properties.output || {
                  type: 'string',
                  description: t('automations.instruction.output.description'),
                  title: t('automations.instruction.output.label'),
                };
              }
              return {
                slug: `${slug}.${instructionSlug}`,
                name: `${slug}.${localize(name)}`,
                description: localize(description),
                schema,
              };
            },
          ),
        });
      });
    }
    return instructions;
  }, [
    automations,
    imports,
    photo,
    name,
    localize,
    localizeBuiltinSchemas,
    localizeSchemaForm,
    t,
    workspaceId,
  ]);

  const newNodeAdding = useRef(false);

  // Throttle value changes to avoid UI freezing while editing in code editor
  const [throttledValue, setThrottledValue] = useState(value);
  const throttled = useRef<NodeJS.Timeout>();
  useEffect(() => {
    if (throttled.current) {
      clearTimeout(throttled.current);
    }
    throttled.current = setTimeout(() => setThrottledValue(value), 500);
  }, [value]);

  const { nodes, edges } = useMemo(
    () =>
      automationToGraph(throttledValue, {
        height: 150,
        width: 400,
        onChange: (newValue, { deleted, add } = {}) => {
          const finalize = () => {
            const valueChanged = {
              ...throttledValue,
              ...newValue,
            };
            onChange(valueChanged);
            setThrottledValue(valueChanged);
          };
          if (deleted) {
            setDeleted(deleted);
            setTimeout(finalize, 200);
            return;
          }
          if (add) newNodeAdding.current = true;
          finalize();
          if (add) setTimeout(() => (newNodeAdding.current = false));
        },
        onNewNode: (id) => {
          if (!newNodeAdding.current) return;
          setAdded(id);
        },
        onError(error) {
          notifications.error({
            message: error.message,
            placement: 'bottomRight',
          });
        },
        endpointUrl: generateEndpoint(workspaceId, throttledValue.slug || ''),
      }),
    [throttledValue, generateEndpoint, notifications, onChange, workspaceId],
  );

  return (
    <div className={styles['container']}>
      <Graph
        instructions={instructions}
        nodes={nodes}
        edges={edges}
        added={added}
        deleted={deleted}
      />
    </div>
  );
}
