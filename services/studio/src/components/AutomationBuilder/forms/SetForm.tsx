import SchemaForm, { FieldProps, InfoBubble, Schema } from '@/components/SchemaForm';
import useTranslations from '@/i18n/useTranslations';
import { useField } from 'react-final-form';
import { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import useBlocks from '@/components/PageBuilder/useBlocks';
import { Select, Tooltip } from 'antd';
import useLocalizedText from '@/utils/useLocalizedText';
import styles from '../automation-builder.module.scss';
import getEditSchema from '@/components/PageBuilder/Panel/EditSchema/getEditSchema';
import { loadModule } from '@/utils/useExternalModule';
import { BlockComponent } from '@/blocks/BlockLoader';
import componentsWithBlocksList from '@/components/BlocksListEditor/componentsWithBlocksList';
import BlocksListEditorProvider from '@/components/BlocksListEditor/BlocksListEditorProvider';

export function InterfaceSelector(props: FieldProps) {
  const field = useField(props.name);
  const { variants: blocks } = useBlocks();
  const t = useTranslations('workspaces');
  const { localize } = useLocalizedText();

  const selectOptions = useMemo(
    () =>
      blocks.map(({ slug, name, description, photo }) => ({
        label: (
          <Tooltip
            title={
              localize(description) ? (
                <>
                  {localize(description)}
                  {photo && (
                    <img
                      src={photo}
                      alt={localize(name)}
                      className={styles['form-set-interface-select-option-img']}
                    />
                  )}
                </>
              ) : (
                ''
              )
            }
          >
            <div className={styles['form-set-interface-select-option']}>{localize(name)}</div>
          </Tooltip>
        ),
        value: slug,
      })),
    [blocks, localize],
  );
  const filterOption = useCallback(
    (input: string, options: any) => {
      const block = blocks.find(({ slug }) => slug === options.value);
      if (!block) return false;
      const search = `${block.slug} ${localize(block.name)} ${localize(
        block.description,
      )}`.toLowerCase();
      return search.includes(input);
    },
    [blocks, localize],
  );

  return (
    <div className="pr-form-field">
      <label htmlFor={`${field.input.name}.name`} className="pr-form-label">
        {t('automations.instruction.form.set.interface.label')}
      </label>
      <div className="pr-form-input">
        <Select
          options={selectOptions}
          value={field.input.value}
          onChange={field.input.onChange}
          showSearch
          filterOption={filterOption}
          className={styles['form-set-interface-select']}
          allowClear
        />
      </div>
      <div className="pr-form-description">
        <InfoBubble className="pr-form-object__description" text={props.schema.description} />
      </div>
    </div>
  );
}

const SCHEMAS_CACHE: Map<string, Promise<Schema>> = new Map();

export default function SetForm({ name, schema: initialSchema }: FieldProps) {
  const field = useField(name);
  const interfaceField = useField(`${name}.interface`);
  const initialSchemaValue = useRef(initialSchema?.properties?.value || {});
  const [valueScheme, setValueSchema] = useState<Schema>(initialSchemaValue.current);
  const { variants: blocks } = useBlocks();
  const { localizeSchemaForm } = useLocalizedText();

  useEffect(() => {
    async function fetchSchema(name: string) {
      let promise = SCHEMAS_CACHE.get(name);
      if (!promise) {
        promise = new Promise(async (resolve) => {
          const { builtIn, url, schema } = blocks.find(({ slug }) => slug === name) || {};
          if (builtIn) {
            return resolve(localizeSchemaForm(getEditSchema(name) || {}));
          }
          if (schema) {
            return resolve(localizeSchemaForm(schema || {}));
          }
          if (url) {
            const module = await loadModule<BlockComponent>(url);
            if (module && module.schema) {
              resolve(module.schema);
            }
          }
        });
        SCHEMAS_CACHE.set(name, promise);
      }
      setValueSchema(await promise);
    }
    if (interfaceField.input.value) {
      fetchSchema(interfaceField.input.value);
    } else {
      setValueSchema(initialSchemaValue.current);
    }
  }, [interfaceField.input.value, blocks, localizeSchemaForm]);

  const schema = useMemo(() => {
    const schema = initialSchema['ui:options'] as Schema;
    if (schema?.properties?.interface) {
      schema.properties.interface['ui:widget'] = InterfaceSelector;
    }
    if (schema?.properties?.value) {
      schema.properties.value = valueScheme;
    }
    return schema;
  }, [initialSchema, valueScheme]);

  return (
    <BlocksListEditorProvider>
      <SchemaForm
        initialValues={field.input.value}
        onChange={field.input.onChange}
        schema={schema}
        components={componentsWithBlocksList}
        buttons={[]}
      />
    </BlocksListEditorProvider>
  );
}
