import { useCallback, useEffect, useMemo, useRef } from 'react';
import { useField } from 'react-final-form';
import BUILTIN_INSTRUCTIONS from '@prisme.ai/validation/instructions.json';
import { debounce, get } from 'lodash';
import { useTranslations } from 'next-intl';
import SchemaForm, { FieldProps, Schema } from '@/components/SchemaForm';
import components from '@/components/SchemaFormInWorkspace/schemaFormComponents';

const basicKeys = ['url', 'method', 'body', 'output'];
const hiddenKeys = ['advanced.stream.concatenate'];

function getSortedProperties() {
  const advanced = Object.entries(BUILTIN_INSTRUCTIONS['fetch'].properties.fetch.properties).filter(
    ([k]) => !basicKeys.includes(k),
  );
  const order = [
    'query',
    'headers',
    'multipart',
    'outputMode',
    'prismeaiApiKey',
    'stream',
    'emitErrors',
  ];
  const sorted = advanced.filter(([k]) => order.includes(k));
  sorted.sort(([ka], [kb]) => {
    const ia = order.indexOf(ka);
    const ib = order.indexOf(kb);
    if (ia > ib) return 1;
    if (ia < ib) return -1;
    return 0;
  });
  const unsorted = advanced.filter(([k]) => !order.includes(k));
  return Object.fromEntries([...sorted, ...unsorted]) as Schema['properties'];
}

function hasValue(value: Record<string, any>, key: string) {
  const v = get(value, `advanced.${key}`);

  return typeof v === 'object' ? Object.keys(v).length > 0 : !!v;
}

export const FetchForm = ({ name }: FieldProps) => {
  const t = useTranslations('workspaces');
  const { input } = useField(name);

  const value = useMemo(() => {
    const properties = Object.entries(input.value);
    return {
      ...Object.fromEntries(properties.filter(([k]) => basicKeys.includes(k))),
      advanced: Object.fromEntries(properties.filter(([k]) => !basicKeys.includes(k))),
    };
  }, [input.value]);

  const schemaForm = useMemo(() => {
    const advanced = getSortedProperties();

    function localizeProperties(
      properties: NonNullable<Schema['properties']>,
      prefix?: string,
    ): NonNullable<Schema['properties']> {
      return Object.entries(properties).reduce((prev, [k, v]) => {
        const key = `${prefix ? `${prefix}.` : ''}${k}`;

        return {
          ...prev,
          [k]: {
            ...v,
            title: t(`automations.instruction.form.fetch.${key}.label`),
            description: t(`automations.instruction.form.fetch.${key}.description`),
            properties: v.properties ? localizeProperties(v.properties, key) : undefined,
            'ui:options': {
              object: {
                opened: k !== 'stream' && hasValue(value, key),
              },
            },
            hidden: hiddenKeys.includes(`advanced.${key}`),
            enumNames:
              v.enum && !v.enumNames
                ? v.enum.map((k) => t(`automations.instruction.form.fetch.${key}.enum.${k}`))
                : undefined,
            items: v.items
              ? {
                  ...v.items,
                  title: t(`automations.instruction.form.fetch.${key}.items.label`),
                  description: t(`automations.instruction.form.fetch.${key}.items.description`),
                  add: t(`automations.instruction.form.fetch.${key}.items.add`),
                  remove: t(`automations.instruction.form.fetch.${key}.items.remove`),
                  properties: v.items.properties
                    ? localizeProperties(v.items.properties, `${key}.items`)
                    : undefined,
                }
              : undefined,
          },
        };
      }, {});
    }

    return {
      type: 'object',
      title: t('automations.instruction.label', { context: 'fetch' }),
      description: t('automations.instruction.description', {
        context: 'fetch',
      }),
      properties: {
        url: {
          type: 'string',
          title: t('automations.instruction.form.fetch.url.label'),
          description: t('automations.instruction.form.fetch.url.description'),
        },
        method: {
          type: 'string',
          title: t('automations.instruction.form.fetch.method.label'),
          description: t('automations.instruction.form.fetch.method.description'),
          enum: ['get', 'post', 'put', 'patch', 'delete'],
          enumNames: ['GET', 'POST', 'PUT', 'PATCH', 'DELETE'],
        },
        body: {
          type: 'object',
          additionalProperties: true,
          title: t('automations.instruction.form.fetch.body.label'),
          description: t('automations.instruction.form.fetch.body.description'),
          'ui:options': {
            object: {
              opened: hasValue(value, 'body'),
            },
          },
        },
        advanced: {
          type: 'object',
          title: t('automations.instruction.form.fetch.advanced.label'),
          description: t('automations.instruction.form.fetch.advanced.description'),
          properties: advanced && localizeProperties(advanced),
          'ui:options': {
            object: {
              opened: false,
            },
          },
        },
        output: {
          type: 'string',
          title: t('automations.instruction.form.fetch.output.label'),
          description: t('automations.instruction.form.fetch.output.description'),
        },
      },
    } as Schema;
  }, [t, value]);

  const onChange = useRef(input.onChange);
  useEffect(() => {
    onChange.current = input.onChange;
  }, [input.onChange]);
  const debouncedOnChange = useRef(
    debounce((v: any) => {
      onChange.current(v);
    }, 500),
  );
  const restructureValue = useCallback(
    (v: any) => {
      const { advanced, ...newValue } = v;
      debouncedOnChange.current({
        ...newValue,
        ...advanced,
      });
    },
    [debouncedOnChange],
  );

  return (
    <SchemaForm
      initialValues={value}
      schema={schemaForm}
      onChange={restructureValue}
      components={components}
      buttons={[]}
    />
  );
};

export default FetchForm;
