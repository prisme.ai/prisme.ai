import { Handle, Position } from '@xyflow/react';
import styles from './graph.module.scss';
export default function NodeEmpty() {
  return (
    <>
      <Handle type="target" position={Position.Top} />
      <div className={`${styles['graph-node-empty-container']}`}>&nbsp;</div>
      <Handle type="source" position={Position.Bottom} />
    </>
  );
}
