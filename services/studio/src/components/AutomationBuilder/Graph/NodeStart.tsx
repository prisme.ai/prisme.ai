import { NodeProps, Node, Handle, Position } from '@xyflow/react';
import styles from './graph.module.scss';
import { useLocale, useTranslations } from 'next-intl';
import LogoIcon from '@/svgs/logo.svg';
import EditIcon from '@/svgs/edit.svg';
import CopyIcon from '@/svgs/copy.svg';
import { Popover, Tooltip } from 'antd';
import { useEffect, useMemo, useState } from 'react';
import copy from '@/utils/copy';
import cronstrue from 'cronstrue/i18n';
import { useGraph } from './context';
import EditInstruction from './EditInstruction';
import { Schema } from '@/components/SchemaForm';
import Schedule from './Schedule';
import { useNotifications } from '@/providers/Notifications';

type NodeData = Prismeai.When & {
  onSave: (data: Prismeai.When) => void;
  endpointUrl?: string;
};

function crontabToString(s: string, locale: string) {
  try {
    return cronstrue.toString(s, {
      locale,
    });
  } catch {
    return s;
  }
}

export default function NodeStart({
  id,
  data: { events, schedules, endpoint, onSave, endpointUrl },
}: NodeProps<Node<NodeData>>) {
  const t = useTranslations('workspaces');
  const locale = useLocale();
  const [open, setOpen] = useState(false);
  const { active, setActive } = useGraph();

  const [value, setValue] = useState({ events, schedules, endpoint: !!endpoint });
  useEffect(() => {
    setValue({ events, schedules, endpoint: !!endpoint });
  }, [events, schedules, endpoint]);

  const notification = useNotifications();

  const schema: Schema = useMemo(
    () => ({
      type: 'object',
      properties: {
        events: {
          type: 'array',
          title: t('automations.trigger.events.title'),
          description: t('automations.trigger.events.help'),
          items: {
            type: 'string',
            title: t('automations.trigger.events.item'),
            'ui:widget': 'autocomplete',
            'ui:options': {
              autocomplete: 'events:emit',
            },
          },
        },
        schedules: {
          type: 'array',
          title: t('automations.trigger.schedules.title'),
          description: t('automations.trigger.schedules.help'),
          items: {
            type: 'string',
            'ui:widget': Schedule,
          },
        },
        endpoint: {
          type: 'boolean',
          title: t('automations.trigger.endpoint.custom'),
          description: t('automations.trigger.endpoint.help'),
        },
      },
    }),
    [t],
  );
  return (
    <>
      <div
        className={`${styles['graph-node-container']} ${active === id ? styles['graph-node-container--active'] : ''} ${styles['graph-node-container--start']}`}
      >
        <div className={styles['graph-node-header']}>
          <LogoIcon className={styles['graph-node-header-icon']} />
          <span className={styles['graph-node-header-title']}>
            {t('automations.node.title_trigger')}
          </span>
        </div>
        <Popover
          className={`${styles['graph-node-content']} ${styles['graph-node-content--editable']}`}
          content={<EditInstruction schema={schema} value={value} onChange={setValue} />}
          open={open}
          onOpenChange={(o) => {
            setActive(o ? id : '');
            setOpen(o);
            if (!o && value && onSave) {
              onSave(value);
            }
          }}
          trigger={'click'}
          destroyTooltipOnHide
        >
          {endpoint && (
            <p>
              {t.rich('automations.trigger.endpoint.display', {
                a: (children) => (
                  <Tooltip title={t('automations.trigger.endpoint.tooltip')} placement="right">
                    <a
                      href={endpointUrl || ''}
                      className={styles['graph-node-content-copy-btn']}
                      onClick={(e) => {
                        e.preventDefault();
                        e.stopPropagation();
                        copy(endpointUrl || '');
                        notification.success({
                          message: t('automations.builder.trigger.copy.success'),
                          placement: 'bottomRight',
                        });
                      }}
                    >
                      {children}
                    </a>
                  </Tooltip>
                ),
                icon: () => <CopyIcon />,
              })}
            </p>
          )}
          {events && (
            <p>
              {t(`automations.trigger.events.display_${events.length > 1 ? 'other' : 'one'}`, {
                events: events.join(', '),
              })}
            </p>
          )}

          {schedules && schedules.map((s, key) => <p key={key}>{crontabToString(s, locale)}</p>)}
          <Tooltip title={t('automations.builder.trigger.edit.tooltip')} placement="left">
            <span className={styles['graph-node-content-edit-btn']}>
              <EditIcon />
            </span>
          </Tooltip>
        </Popover>
      </div>
      <Handle type="source" position={Position.Bottom} />
    </>
  );
}
