import SchemaForm, { Schema } from '@/components/SchemaForm';
import { useMemo } from 'react';
import styles from './graph.module.scss';
import { useTranslations } from 'use-intl';

interface EditConditionProps {
  value?: string;
  onSave: (condition: string) => void;
  type: 'create' | 'edit';
}

export default function EditCondition({ value, onSave, type }: EditConditionProps) {
  const t = useTranslations('workspaces');
  const schema: Schema = useMemo(
    () => ({
      type: 'string',
      validators: {
        required: true,
      },
    }),
    [],
  );

  return (
    <SchemaForm
      className={styles['edit-condition']}
      initialValues={value}
      schema={schema}
      onSubmit={onSave}
      autoFocus
      locales={{
        submit: t(
          `automations.builder.instruction.conditions.save.${type === 'create' ? 'create' : 'update'}`,
        ),
      }}
    />
  );
}
