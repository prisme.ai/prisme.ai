import { Edge, EdgeLabelRenderer, EdgeProps, getStraightPath } from '@xyflow/react';
import { Popover, Tooltip } from 'antd';
import { useTranslations } from 'next-intl';
import PlusIcon from '@/svgs/plus.svg';
import styles from './graph.module.scss';
import EditCondition from './EditCondition';

type EdgeData = {
  onAdd: (condition: string) => void;
};

export default function EdgeCondition({
  id,
  sourceX,
  sourceY,
  targetX,
  targetY,
  markerEnd,
  data,
}: EdgeProps<Edge<EdgeData>>) {
  const t = useTranslations('workspaces');
  const [edgePath, , labelY] = getStraightPath({
    sourceX,
    sourceY,
    targetX,
    targetY,
  });

  if (!data?.onAdd) return null;

  return (
    <>
      <path
        id={id}
        d={edgePath}
        markerEnd={markerEnd}
        stroke="var(--pr-accent-color)"
        strokeWidth={1}
        strokeDasharray="5,5"
        fill="none"
      />
      <EdgeLabelRenderer>
        <Popover
          title={t('automations.builder.instruction.conditions.add.title')}
          content={<EditCondition onSave={data.onAdd} type="create" />}
          trigger={'click'}
          destroyTooltipOnHide
        >
          <Tooltip title={t('automations.instruction.label_conditions_add')} placement="top">
            <button
              className={styles['graph-edge-button']}
              style={{
                position: 'absolute',
                transform: `translate(-50%, -50%) translate(${targetX - 60}px, ${labelY}px)`,
                pointerEvents: 'all',
              }}
            >
              <PlusIcon />
            </button>
          </Tooltip>
        </Popover>
      </EdgeLabelRenderer>
    </>
  );
}
