import { NodeProps, Node, Handle, Position, HandleType } from '@xyflow/react';
import LogoIcon from '@/svgs/logo.svg';
import EditIcon from '@/svgs/edit.svg';
import TrashIcon from '@/svgs/trash.svg';
import styles from './graph.module.scss';
import { CSSProperties, useEffect, useMemo, useState } from 'react';
import { useTranslations } from 'next-intl';
import { Popover, Tooltip } from 'antd';
import EditInstruction from './EditInstruction';
import { useGraph } from './context';
import useInstructionLabel from './useInstructionLabel';
import ConfirmButton from '@/components/ConfirmButton';
import LinkIcon from '@/svgs/link.svg';

const DEFAULT_HANDLE_STYLE = {
  width: 5,
  height: 5,
  bottom: -5,
};

type CustomHandleType = {
  type: HandleType;
  position: Position;
  style?: CSSProperties;
  id?: string;
};

type NodeData = {
  instruction: string;
  config?: Record<string, unknown>;
  onSave?: (v: Record<string, unknown>) => void;
  onDelete: () => void;
  horizontal?: boolean;
  handles?: CustomHandleType[];
};

export default function NodeInstruction({ id, data }: NodeProps<Node<NodeData>>) {
  const t = useTranslations('workspaces');
  const { active, setActive, added, deleted } = useGraph();
  const [open, setOpen] = useState(false);
  const [displayAdded, setDisplayAdded] = useState(false);
  const [displayDeleted, setDisplayDeleted] = useState(false);
  const { getInstruction, getInstructionSchema, getInstructionGroup } = useGraph();
  const schema = useMemo(
    () => getInstructionSchema(data.instruction),
    [data.instruction, getInstructionSchema],
  );

  const [value, setValue] = useState(data.config);
  const handles: CustomHandleType[] = useMemo(() => {
    return (
      data.handles || [
        {
          type: 'target',
          position: Position.Top,
        },
        {
          type: 'source',
          position: Position.Bottom,
        },
      ]
    );
  }, [data.handles]);

  useEffect(() => {
    if (added === id) {
      setOpen(true);
      setDisplayAdded(true);
      setTimeout(() => setDisplayAdded(false), 200);
    }
  }, [added, id]);

  useEffect(() => {
    if (deleted === id) {
      setOpen(false);
      setDisplayDeleted(true);
    }
  }, [deleted, id]);

  function getTitle(instruction: string) {
    const { name = instruction, href = '' } = getInstruction(instruction) || {};
    return (
      <>
        {name}
        {href && (
          <Tooltip title={t('automations.instruction.link')}>
            <a
              href={href}
              target="_blank"
              rel="noreferrer"
              className={styles['graph-node-header-title-link']}
            >
              <LinkIcon />
            </a>
          </Tooltip>
        )}
      </>
    );
  }

  const { icon, name: appSlug } = getInstructionGroup(data.instruction) || {};


  const label = useInstructionLabel(data.instruction, data.config);

  return (
    <>
      <div
        className={`${styles['graph-node-container']} ${active === id ? styles['graph-node-container--active'] : ''} ${displayAdded ? styles['graph-node-container--added'] : ''} ${displayDeleted ? styles['graph-node-container--deleted'] : ''}`}
      >
        <div className={styles['graph-node-header']}>
          {icon ? (
            <img src={icon} alt={appSlug} className={styles['graph-node-header-icon']} />
          ) : (
            <LogoIcon className={styles['graph-node-header-icon']} />
          )}
          <span className={styles['graph-node-header-title']}>{getTitle(data.instruction)}</span>
          <ConfirmButton
            className={styles['graph-node-header-delete-btn']}
            onConfirm={data.onDelete}
            confirmLabel={t('automations.instruction.delete')}
          >
            <TrashIcon />
          </ConfirmButton>
        </div>
        {data.onSave && schema && (
          <Popover
            className={`${styles['graph-node-content']} ${styles['graph-node-content--editable']}`}
            content={
              <EditInstruction
                schema={schema}
                value={value}
                onChange={setValue}
                instruction={data.instruction}
              />
            }
            open={open}
            onOpenChange={(o) => {
              setActive(o ? id : '');
              setOpen(o);
              if (!o && value && data.onSave) {
                data.onSave(value);
              }
            }}
            trigger={'click'}
            destroyTooltipOnHide
            placement="right"
          >
            <div className={styles['graph-node-content-label']}>{label}</div>
            <Tooltip title={t('automations.builder.instruction.edit.tooltip')} placement="left">
              <span className={styles['graph-node-content-edit-btn']}>
                <EditIcon />
              </span>
            </Tooltip>
          </Popover>
        )}
      </div>
      {handles?.map(({ type, position, id, style }) => (
        <Handle
          key={`${type}${position}${id}`}
          type={type}
          position={position}
          id={id}
          style={{ ...DEFAULT_HANDLE_STYLE, ...style }}
        />
      ))}
    </>
  );
}
