import { NodeProps, Node, Handle, Position } from '@xyflow/react';
import LogoIcon from '@/svgs/logo.svg';
import EditIcon from '@/svgs/edit.svg';
import styles from './graph.module.scss';
import { Popover, Tooltip } from 'antd';
import { useTranslations } from 'next-intl';
import EditInstruction from './EditInstruction';
import { useEffect, useMemo, useState } from 'react';
import { useGraph } from './context';
import { Schema } from '@/components/SchemaForm';

type NodeData = {
  output?: string;
  onSave: (output: string) => void;
};

export default function NodeOutput({ id, data }: NodeProps<Node<NodeData>>) {
  const t = useTranslations('workspaces');
  const [open, setOpen] = useState(false);
  const { active, setActive } = useGraph();

  const [value, setValue] = useState(data.output);
  useEffect(() => {
    setValue(data.output);
  }, [data.output]);

  const schema: Schema = useMemo(
    () => ({
      additionalProperties: true,
      title: t('automations.output.edit.title'),
      description: t('automations.output.edit.description'),
    }),
    [t],
  );

  return (
    <>
      <Handle type="target" position={Position.Top} />
      <div
        className={`${styles['graph-node-container']} ${styles['graph-node-container--end']} ${active === id ? styles['graph-node-container--active'] : ''}`}
      >
        <div className={styles['graph-node-header']}>
          <LogoIcon className={styles['graph-node-header-icon']} />
          <span className={styles['graph-node-header-title']}>
            {t('automations.node.title_output')}
          </span>
        </div>
        <Popover
          className={`${styles['graph-node-content']} ${styles['graph-node-content--editable']}`}
          content={
            <EditInstruction<string>
              schema={schema}
              value={value}
              onChange={setValue}
              onSave={(output) => {
                data.onSave(output);
                setOpen(false);
              }}
            />
          }
          open={open}
          onOpenChange={(o) => {
            setActive(o ? id : '');
            setOpen(o);
            if (!o && value && data.onSave) {
              data.onSave(value);
            }
          }}
          trigger={'click'}
          destroyTooltipOnHide
          placement="right"
        >
          <p>{typeof data.output === 'string' ? data.output : JSON.stringify(data.output)}</p>
          <Tooltip title={t('automations.builder.instruction.edit.tooltip')} placement="left">
            <span className={styles['graph-node-content-edit-btn']}>
              <EditIcon />
            </span>
          </Tooltip>
        </Popover>
      </div>
    </>
  );
}
