import SchemaForm, { Schema } from '@/components/SchemaForm';
import styles from './graph.module.scss';
import { useTranslations } from 'next-intl';
import useExtractAutocompleteOptions from '@/components/SchemaFormInWorkspace/useExtractAutocompleteOptions';
import useExtractSelectOptions from '@/components/SchemaFormInWorkspace/useExtractSelectOptions';
import components from '@/components/SchemaFormInWorkspace/schemaFormComponents';
import { useCallback, useEffect, useMemo, useState } from 'react';
import { useWorkspace } from '@/providers/Workspace';
import useApi from '@/utils/api/useApi';
import { JSONPath } from 'jsonpath-plus';

interface EditInstructionProps<T = Record<string, unknown>> {
  schema: Schema;
  value?: T;
  onSave?: (v: T) => void;
  onChange?: (v: T) => void;
  instruction?: string;
}

export default function EditInstruction<T>({
  schema,
  value,
  onSave,
  onChange,
  instruction,
}: EditInstructionProps<T>) {
  const api = useApi();
  const t = useTranslations('workspaces');
  const appName = useMemo(() => {
    const [appName, isApp] = (instruction || '').split(/\./);
    return isApp ? appName : undefined;
  }, [instruction]);
  const { workspace } = useWorkspace();

  const [config, setConfig] = useState({});

  useEffect(() => {
    async function getConfig() {
      if (!workspace || !appName) return;
      const { config: { value } = {} } = await api.getAppInstance(workspace.id, appName);
      if (!value) return;
      setConfig(value);
    }
    getConfig();
  }, [appName, api, workspace]);

  const extractFromConfig = useCallback(
    (path: string) => {
      return JSONPath({ path, json: config })?.[0] || null;
    },
    [config],
  );

  return (
    <div className={styles['graph-node-popover']}>
      <div className={styles['graph-node-popover-title']}>{t('automations.instruction.panel')}</div>
      <SchemaForm
        initialValues={value}
        schema={schema}
        onSubmit={onSave}
        onChange={onChange}
        buttons={[]}
        components={components}
        utils={{
          extractAutocompleteOptions: useExtractAutocompleteOptions(),
          extractSelectOptions: useExtractSelectOptions({
            config,
          }),
          extractFromConfig,
        }}
      />
    </div>
  );
}
