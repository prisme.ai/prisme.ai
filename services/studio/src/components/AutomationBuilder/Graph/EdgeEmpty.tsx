import { Edge, EdgeProps } from '@xyflow/react';
import getPath, { PathStyles } from './getPath';

type EdgeData = {
  onAdd: (instruction: Prismeai.Instruction) => void;
  pathStyle?: PathStyles;
};

export default function EdgeEmpty({
  id,
  sourceX,
  sourceY,
  targetX,
  targetY,
  markerEnd,
  data,
}: EdgeProps<Edge<EdgeData>>) {
  const [edgePath] = getPath(
    {
      sourceX,
      sourceY,
      targetX,
      targetY,
    },
    data?.pathStyle,
  );

  return (
    <>
      <path
        id={id}
        d={edgePath}
        markerEnd={markerEnd}
        stroke="var(--pr-accent-color)"
        strokeWidth={1}
        strokeDasharray="5,5"
        fill="none"
      />
    </>
  );
}
