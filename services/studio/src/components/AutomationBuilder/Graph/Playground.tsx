'use client';

import RichText from '@/blocks/blocks/RichText/RichText';

export default function Playground() {
  const content = `<p>Pouet</p><iframe onload="console.log('pwnd')"></iframe>`;
  return (
    <>
      <RichText allowUnsecure>{content}</RichText>
      <RichText>{content}</RichText>
    </>
  );
}
