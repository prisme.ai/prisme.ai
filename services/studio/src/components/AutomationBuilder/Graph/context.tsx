import { Schema } from '@/components/SchemaForm';
import useLocalizedText from '@/utils/useLocalizedText';
import { createContext, ReactNode, useCallback, useContext, useEffect, useState } from 'react';

export interface Instruction {
  slug: string;
  name: string;
  description?: string;
  schema?: Schema;
  initialValue?: () => unknown;
  href?: string;
}

export interface IntructionsGroup {
  name: string;
  icon?: string;
  instructions: Instruction[];
}

interface GraphContext {
  instructions: IntructionsGroup[];
  active?: string;
  setActive: (active: string) => void;
  getInstruction: (instruction: string) => Instruction | null;
  getInstructionGroup: (instruction: string) => IntructionsGroup | null;
  getInstructionSchema: (instruction: string) => Schema | null;
  added?: string;
  deleted?: string;
}

export const graphContext = createContext<GraphContext | undefined>(undefined);

export const useGraph = () => {
  const context = useContext(graphContext);
  if (!context) {
    throw new Error();
  }
  return context;
};

export interface GraphProviderProps {
  instructions: IntructionsGroup[];
  children: ReactNode;
  added?: string;
  deleted?: string;
}

export default function GraphProvider({
  instructions,
  children,
  added: lastAdded,
  deleted: lastDeleted,
}: GraphProviderProps) {
  const { localizeSchemaForm } = useLocalizedText();
  const [active, setActive] = useState('');
  const [added, setAdded] = useState(lastAdded);
  const [deleted, setDeleted] = useState(lastAdded);

  useEffect(() => {
    if (!lastAdded) return;
    setAdded(lastAdded);
    setActive(lastAdded);
  }, [lastAdded]);

  useEffect(() => {
    if (!lastDeleted) return;
    setDeleted(lastDeleted);
  }, [lastDeleted]);

  const getInstruction = useCallback(
    (instruction: string) => {
      return instructions.reduce<Instruction | null>((found, { instructions }) => {
        if (found) return found;
        return instructions.find(({ slug }) => slug === instruction) || null;
      }, null);
    },
    [instructions],
  );

  const getInstructionGroup = useCallback(
    (instruction: string) => {
      return (
        instructions.find(({ instructions }) =>
          instructions.find(({ slug }) => slug === instruction),
        ) || null
      );
    },
    [instructions],
  );

  const getInstructionSchema = useCallback(
    (instruction: string) => {
      if (['all', 'conditions'].includes(instruction)) return null;
      const { schema } = getInstruction(instruction) || {};

      if (!schema) {
        return null;
      }

      return localizeSchemaForm(schema);
    },
    [localizeSchemaForm, getInstruction],
  );

  return (
    <graphContext.Provider
      value={{
        instructions,
        active,
        setActive,
        getInstruction,
        getInstructionGroup,
        getInstructionSchema,
        added,
        deleted,
      }}
    >
      {children}
    </graphContext.Provider>
  );
}
