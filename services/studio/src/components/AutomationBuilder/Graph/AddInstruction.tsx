import { useEffect, useMemo, useRef, useState } from 'react';
import { useGraph } from './context';
import styles from './graph.module.scss';
import SearchInput from '@/components/SearchInput';
import { search } from '@/utils/filters';
import { InputRef, Tooltip } from 'antd';
import LogoIcon from '@/svgs/logo.svg';

interface AddInstructionProps {
  onAdd: (instruction: Prismeai.Instruction) => void;
  onClose: () => void;
}

export default function AddInstruction({ onAdd, onClose }: AddInstructionProps) {
  const { instructions } = useGraph();
  const [filter, setFilter] = useState('');
  const ref = useRef<InputRef>(null);

  const filtered = useMemo(() => {
    if (!filter) return instructions;
    const searchFilter = search(filter);
    return instructions.flatMap((group) => {
      const { name, instructions } = group;
      if (
        !searchFilter(name) &&
        !instructions.some(({ slug, name, description }) =>
          searchFilter(`${slug} ${name} ${description}`),
        )
      )
        return [];
      const filtered = instructions.filter(({ slug, name, description }) =>
        searchFilter(`${slug} ${name} ${description}`),
      );
      return {
        ...group,
        instructions: filtered.length === 0 ? instructions : filtered,
      };
    });
  }, [filter, instructions]);

  useEffect(() => {
    ref.current?.focus();
  });

  return (
    <div className={styles['instructions-list-ctn']}>
      <SearchInput
        className={styles['instructions-search']}
        value={filter}
        onChange={({ target: { value } }) => setFilter(value)}
        ref={ref}
      />
      <div className={styles['instructions-list']}>
        {filtered.map(({ name, icon, instructions }) => (
          <div key={name} className={styles['instructions-list-group']}>
            <div className={styles['instructions-list-group-header']}>
              <div className={styles['instructions-list-group-header-icon']}>
                {icon ? <img src={icon} alt={name} /> : <LogoIcon />}
              </div>
              <div className={styles['instructions-list-group-header-title']}>{name}</div>
            </div>
            <div className={styles['instructions-list-instructions']}>
              {instructions.map(({ name, slug, description, initialValue }) => (
                <Tooltip title={description} key={slug} placement="right">
                  <button
                    className={styles['instructions-list-instruction']}
                    onClick={() => {
                      onAdd({ [slug]: initialValue?.() || {} });
                      onClose();
                    }}
                  >
                    <div className={styles['instructions-list-instruction-icon']}>
                      {icon ? <img src={icon} alt={name} /> : <LogoIcon />}
                    </div>
                    <div className={styles['instructions-list-instruction-texts']}>
                      <div className={styles['instructions-list-instruction-name']}>{name}</div>
                      <div className={styles['instructions-list-instruction-description']}>
                        {description || slug}
                      </div>
                    </div>
                  </button>
                </Tooltip>
              ))}
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}
