import {
  getSmoothStepPath,
  getStraightPath,
  getBezierPath,
  getSimpleBezierPath,
} from '@xyflow/react';

export type PathStyles = 'smooth' | 'straight' | 'bezier';

export default function getPath(
  options: Parameters<typeof getSmoothStepPath>[0],
  type?: PathStyles,
) {
  if (type === 'smooth') {
    return getSmoothStepPath(options);
  }
  if (type === 'straight') {
    return getStraightPath(options);
  }
  if (type === 'bezier') {
    return getBezierPath(options);
  }
  return getSimpleBezierPath(options);
}
