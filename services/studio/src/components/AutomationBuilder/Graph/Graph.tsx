import {
  Background,
  BackgroundVariant,
  Controls,
  Edge,
  MiniMap,
  Node,
  ReactFlow,
} from '@xyflow/react';
import EdgeInstruction from './EdgeInstruction';
import NodeInstruction from './NodeInstruction';
import NodeOutput from './NodeOutput';
import NodeStart from './NodeStart';
import '@xyflow/react/dist/style.css';
import GraphProvider, { IntructionsGroup } from './context';
import styles from './graph.module.scss';
import NodeCondition from './NodeCondition';
import EdgeCondition from './EdgeCondition';
import NodeEmpty from './NodeEmpty';
import EdgeEmpty from './EdgeEmpty';
import NodeAdd from './NodeAdd';

const nodeTypes = {
  start: NodeStart,
  add: NodeAdd,
  instruction: NodeInstruction,
  end: NodeOutput,
  empty: NodeEmpty,
  condition: NodeCondition,
};
const edgeTypes = {
  empty: EdgeEmpty,
  instruction: EdgeInstruction,
  condition: EdgeCondition,
};

interface GraphProps {
  nodes?: Node[];
  edges?: Edge[];
  instructions: IntructionsGroup[];
  added?: string;
  deleted?: string;
}
export default function Graph({ nodes, edges, instructions, added, deleted }: GraphProps) {
  return (
    <GraphProvider instructions={instructions} added={added} deleted={deleted}>
      <ReactFlow
        nodes={nodes}
        edges={edges}
        nodeTypes={nodeTypes}
        edgeTypes={edgeTypes}
        fitView
        panOnScroll
        nodesConnectable={false}
        nodesDraggable={true}
        className={styles['graph']}
      >
        <Controls />
        <Background variant={BackgroundVariant.Dots} gap={26} size={1} />
        <MiniMap
          nodeColor="var(--pr-accent-color)"
          pannable
          bgColor="var(--pr-accent-color-contrast)"
        />
      </ReactFlow>
    </GraphProvider>
  );
}
