import copy from '@/utils/copy';
import { truncate } from '@/utils/strings';
import { Tooltip } from 'antd';
import { useTranslations } from 'next-intl';
import { ReactNode } from 'react';

export default function useInstructionLabel(instruction: string, config: unknown) {
  const t = useTranslations('workspaces');
  switch (instruction) {
    case 'emit': {
      const { event } = (config as Prismeai.Emit['emit']) || {};
      return event || '…';
    }
    case 'wait': {
      const { oneOf = [], timeout } = (config as Prismeai.Wait['wait']) || {};
      if (timeout !== undefined) {
        if (oneOf && Array.isArray(oneOf) && oneOf.length)
          return t('automations.instruction.label_wait_for_until', {
            timeout: timeout,
            events: oneOf.map(({ event }) => event).join(', '),
            count: oneOf.length,
          });
        return t('automations.instruction.label_wait_until', {
          timeout: timeout,
        });
      }
      if (oneOf.length)
        return t('automations.instruction.label_wait_for', {
          events: oneOf.map(({ event }) => event).join(', '),
          count: oneOf.length,
        });
      return '…';
    }
    case 'set': {
      const { name, value } = (config as Prismeai.Set['set']) || {};
      return name ? `${name} = ${truncate(JSON.stringify(value), 10, '…') || ''}` : '…';
    }
    case 'delete': {
      const { name } = (config as Prismeai.Delete['delete']) || {};
      return name || '…';
    }
    case 'repeat': {
      const { until } = (config as Prismeai.Repeat['repeat']) || {};
      const { on } = (config as { on: string }) || {};
      const labels = [];
      if (on) {
        labels.push(
          t('automations.instruction.label_repeat_on', {
            on,
          }),
        );
      }
      if (until) {
        labels.push(
          t('automations.instruction.label_repeat_until', {
            count: until,
          }),
        );
      }
      return labels.length ? labels.join(', ') : '…';
    }
    case 'break': {
      const { scope } = (config as Prismeai.Break['break']) || {};
      return scope ? t(`automations.instruction.form.break.scope.enum.${scope}`) : '…';
    }
    case 'fetch': {
      const { url } = (config as Prismeai.Fetch['fetch']) || {};
      return url || '…';
    }
    case 'comment': {
      const comment = (config as Prismeai.Comment['comment']) || {};
      const actualValue =
        typeof comment === 'string' ? comment : JSON.stringify(comment, null, '  ');
      const displayedValue = truncate(actualValue, 150, '…');
      return (
        <Tooltip className="text-xs italic text-neutral-500" title={actualValue}>
          {displayedValue}
        </Tooltip>
      );
    }
    default: {
      const { output } = (config as { output: string }) || { output: '' };
      if (output) {
        return t.rich('automations.instruction.form.output.label', {
          output: `{{${output}}}`,
          copy: (children: ReactNode) => <button onClick={() => copy(output)}>{children}</button>,
        });
      }
      return '';
    }
  }
}
