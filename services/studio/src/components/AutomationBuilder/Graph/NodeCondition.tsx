import { NodeProps, Node, Handle, Position } from '@xyflow/react';
import EditIcon from '@/svgs/edit.svg';
import TrashIcon from '@/svgs/trash.svg';
import styles from './graph.module.scss';
import { useState } from 'react';
import { useTranslations } from 'next-intl';
import { Popover, Tooltip } from 'antd';
import EditCondition from './EditCondition';
import { useGraph } from './context';
import ConfirmButton from '@/components/ConfirmButton';

type NodeData = {
  condition: string;
  onSave: (condition: string) => void;
  onDelete: () => void;
};

export default function NodeCondition({ id, data }: NodeProps<Node<NodeData>>) {
  const t = useTranslations('workspaces');
  const [open, setOpen] = useState(false);
  const { active, setActive } = useGraph();

  return (
    <>
      <Handle type="target" position={Position.Left} />
      <div
        className={`${styles['graph-node-container']} ${styles['graph-node--condition']} ${active === id ? styles['graph-node-container--active'] : ''}`}
      >
        {data.condition !== 'default' && (
          <Popover
            className={`${styles['graph-node-header']} ${styles['graph-node-header--editable']}`}
            content={
              <EditCondition
                value={data.condition || ''}
                onSave={(condition) => {
                  try {
                    data.onSave(condition);
                    setOpen(false);
                  } catch {}
                }}
                type="edit"
              />
            }
            open={open}
            onOpenChange={(o) => {
              setActive(o ? id : '');
              setOpen(o);
            }}
            trigger={'click'}
            destroyTooltipOnHide
            placement="top"
          >
            <div className={styles['graph-node-condition-content']}>
              <Tooltip title={t('automations.builder.instruction.edit.tooltip')} placement="bottom">
                {data.condition}
                <span className={styles['graph-node-content-edit-btn']}>
                  <EditIcon />
                </span>
              </Tooltip>
            </div>
            <ConfirmButton
              className={styles['graph-node-header-delete-btn']}
              onConfirm={data.onDelete}
              confirmLabel={t('automations.instruction.delete')}
              onClick={(e) => {
                e.stopPropagation();
              }}
            >
              <TrashIcon />
            </ConfirmButton>
          </Popover>
        )}
        {data.condition === 'default' && (
          <div className={styles['graph-node-header']}>
            {t('automations.instruction.label_conditions_default')}
          </div>
        )}
      </div>
      <Handle type="source" position={Position.Bottom} />
    </>
  );
}
