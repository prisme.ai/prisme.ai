import { Edge, EdgeLabelRenderer, EdgeProps } from '@xyflow/react';
import PlusIcon from '@/svgs/plus.svg';
import { Tooltip, Popover } from 'antd';
import { useTranslations } from 'use-intl';
import AddInstruction from './AddInstruction';
import styles from './graph.module.scss';
import { CSSProperties, useState } from 'react';
import getPath, { PathStyles } from './getPath';

type ButtonPosition = 'end';
type EdgeData = {
  onAdd: (instruction: Prismeai.Instruction) => void;
  pathStyle?: PathStyles;
  buttonPosition?: ButtonPosition;
  invisible?: boolean;
  style?: CSSProperties;
};

function getButtonPosition({
  pathStyle,
  buttonPosition,
  targetX,
  labelY,
  labelX,
}: {
  pathStyle?: PathStyles;
  buttonPosition?: ButtonPosition;
  targetX: number;
  targetY: number;
  labelY: number;
  labelX: number;
}) {
  if (buttonPosition === 'end') {
    return `translate(${targetX}px, ${labelY}px)`;
  }
  switch (pathStyle) {
    case 'straight':
      return `translate(${targetX - 60}px, ${labelY}px)`;
  }
  return `translate(${labelX}px, ${labelY}px)`;
}

export default function EdgeInstruction({
  id,
  sourceX,
  sourceY,
  targetX,
  targetY,
  markerEnd,
  data,
}: EdgeProps<Edge<EdgeData>>) {
  const t = useTranslations('workspaces');
  const [edgePath, labelX, labelY] = getPath(
    {
      sourceX,
      sourceY,
      targetX,
      targetY,
    },
    data?.pathStyle,
  );
  const [open, setOpen] = useState(false);
  if (!data?.onAdd) return null;

  return (
    <>
      {!data.invisible && (
        <path
          id={id}
          d={edgePath}
          markerEnd={markerEnd}
          stroke="var(--pr-accent-color)"
          strokeWidth={1}
          strokeDasharray="5,5"
          fill="none"
        />
      )}
      <EdgeLabelRenderer>
        <Popover
          title={t('automations.builder.instruction.add.title')}
          content={<AddInstruction onAdd={data?.onAdd} onClose={() => setOpen(false)} />}
          trigger={'click'}
          destroyTooltipOnHide
          open={open}
          onOpenChange={(o) => {
            setOpen(o);
          }}
          placement="right"
        >
          <Tooltip title={t('automations.builder.instruction.add.label')} placement="right">
            <button
              className={styles['graph-edge-button']}
              style={{
                ...(data.style || {}),
                position: 'absolute',
                transform: `translate(-50%, -50%) ${getButtonPosition({
                  pathStyle: data?.pathStyle,
                  buttonPosition: data?.buttonPosition,
                  targetX,
                  targetY,
                  labelX,
                  labelY,
                })}`,
                pointerEvents: 'all',
              }}
            >
              <PlusIcon />
            </button>
          </Tooltip>
        </Popover>
      </EdgeLabelRenderer>
    </>
  );
}
