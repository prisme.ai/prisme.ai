import { Handle, Node, NodeProps, Position } from '@xyflow/react';
import { Popover, Tooltip } from 'antd';
import AddInstruction from './AddInstruction';
import { useState } from 'react';
import { useTranslations } from 'use-intl';
import PlusIcon from '@/svgs/plus.svg';
import styles from './graph.module.scss';

type NodeData = {
  onAdd?: () => void;
  handlePositions?: {
    source?: Position;
    target?: Position;
  };
};

export default function NodeAdd({ data }: NodeProps<Node<NodeData>>) {
  const t = useTranslations('workspaces');
  const [open, setOpen] = useState(false);
  if (!data.onAdd) return null;
  return (
    <>
      <Handle type="target" position={data.handlePositions?.target || Position.Top} />
      <div className={`${styles['graph-node-add-container']}`}>
        <Popover
          title={t('automations.builder.instruction.add.title')}
          content={<AddInstruction onAdd={data?.onAdd} onClose={() => setOpen(false)} />}
          trigger={'click'}
          destroyTooltipOnHide
          open={open}
          onOpenChange={(o) => {
            setOpen(o);
          }}
        >
          <Tooltip title={t('automations.builder.instruction.add.label')} placement="right">
            <button className={styles['graph-edge-button']}>
              <PlusIcon />
            </button>
          </Tooltip>
        </Popover>
      </div>
      <Handle type="source" position={data.handlePositions?.source || Position.Bottom} />
    </>
  );
}
