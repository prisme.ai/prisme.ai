import { Schema } from '@/components/SchemaForm';
import useLocalizedText from '@/utils/useLocalizedText';
import { useTranslations } from 'next-intl';
import { useCallback } from 'react';

export default function useLocalizeBuiltinSchemas() {
  const ot = useTranslations('workspaces');
  const { localize } = useLocalizedText();

  const t = useCallback((k: string) => (ot.has(k) ? ot(k) : ''), [ot]);

  const localizeSchema = useCallback(
    (schema: Schema, name: string) => {
      if (!name) return schema;
      const localizedSchema = { ...schema };
      const { title, description } = localizedSchema;
      localizedSchema.title =
        t(`automations.instruction.form.${name}.label`) ||
        (typeof title === 'string' ? localize(title) : title);
      localizedSchema.description =
        t(`automations.instruction.form.${name}.description`) ||
        (typeof description === 'string' ? localize(description) : description);
      localizedSchema.add = t(`automations.instruction.form.${name}.add`);
      localizedSchema.remove = t(`automations.instruction.form.${name}.remove`);
      const { properties, items, enum: _enum, oneOf } = localizedSchema;
      if (properties) {
        Object.keys(properties).forEach(
          (k) => (properties[k] = localizeSchema(properties[k], `${name}.${k}`)),
        );
      }
      if (items) {
        localizedSchema.items = localizeSchema(items, `${name}.items`);
      }
      if (_enum && Array.isArray(_enum) && _enum.length > 0) {
        const localized = _enum.map((enumName) =>
          t(`automations.instruction.form.${name}.enum.${enumName}`),
        );
        localizedSchema.enumNames = localized;
      }
      if (oneOf) {
        oneOf.forEach((one, k) => {
          oneOf[k] = localizeSchema(one, name);
        });
      }
      return localizedSchema;
    },
    [localize, t],
  );

  return localizeSchema;
}
