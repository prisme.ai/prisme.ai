import { Edge, Node } from '@xyflow/react';
import { v4 } from 'uuid';

const ids = new WeakMap();

interface AutomationToGraphOptions {
  height?: number;
  width?: number;
  endpointUrl?: string;
  onChange?: (
    automation: Pick<Prismeai.Automation, 'do' | 'when' | 'output'>,
    meta?: {
      deleted?: string;
      add?: boolean;
    },
  ) => void;
  onNewNode?: (id: string) => void;
  onError?: (error: Error) => void;
}

function getHandles(type: string, { withSource }: { withSource: boolean } = { withSource: true }) {
  switch (type) {
    case 'conditions':
      return [
        {
          type: 'target',
          position: 'top',
        },
        {
          type: 'source',
          position: 'right',
        },
      ];
    case 'all':
      return [
        {
          type: 'target',
          position: 'top',
        },
        {
          type: 'source',
          position: 'bottom',
        },
        {
          type: 'source',
          position: 'right',
          id: 'right',
        },
      ];
    case 'repeat':
      return [
        {
          type: 'target',
          position: 'top',
        },
        withSource
          ? {
              type: 'source',
              position: 'bottom',
            }
          : null,
        {
          type: 'source',
          position: 'right',
          id: 'repeat-start',
          style: {
            top: '10px',
          },
        },
        {
          type: 'target',
          position: 'right',
          id: 'repeat-end',
          style: {
            top: 'calc(100% - 10px)',
          },
        },
      ].filter(Boolean);
  }
}

export function automationToGraph(
  automation: Pick<Prismeai.Automation, 'do' | 'when' | 'output'>,
  {
    onChange: onAutomationChange,
    onNewNode,
    onError,
    width = 400,
    height = 200,
    endpointUrl,
  }: AutomationToGraphOptions = {},
): {
  nodes: Node[];
  edges: Edge[];
} {
  const changedAutomation = { ...automation };
  const nodes: Node[] = [];
  const edges: Edge[] = [];

  function getId(o: WeakKey): string {
    if (o === undefined) return '';
    if (!ids.has(o)) {
      ids.set(o, v4());
      onNewNode?.(ids.get(o));
    }
    return ids.get(o) || '';
  }

  function buildConditions({
    conditions,
    parentNode,
  }: {
    conditions: Record<string, Prismeai.Instruction[]>;
    parentNode: Node;
  }) {
    let maxY = parentNode.position.y;
    let maxX = parentNode?.position?.x || 0;
    const lastNodes: Node[] = [];
    if (!conditions)
      return {
        endNode: parentNode,
        maxX,
        maxY,
      };
    const entries = Object.entries(conditions);
    if (!Object.keys(conditions).includes('default')) {
      entries.push(['default', []]);
    }
    entries.sort(([conditionA], [conditionB]) => {
      if (conditionA === 'default') return 1;
      if (conditionB === 'default') return 1;
      return -1;
    });

    entries.forEach(([condition, instructions]) => {
      if (!instructions || !Array.isArray(instructions)) return;
      const node = {
        id: getId(instructions),
        position: {
          x: maxX + width,
          y: parentNode?.position?.y || 0,
        },
        type: 'condition',
        data: {
          condition,
          onSave(next: string) {
            if (conditions[next]) {
              onError?.(new Error('condition already exists'));
              throw new Error('condition already exists');
            }
            conditions[next] = conditions[condition] || [];
            delete conditions[condition];
            onAutomationChange?.(automation);
          },
          onDelete() {
            delete conditions[condition];
            onAutomationChange?.(automation, { deleted: node.id });
          },
        },
      };
      nodes.push(node);
      const source = parentNode.id;
      const target = node.id;
      const edge = {
        id: `${source}--${target}`,
        source,
        target,
        type: 'condition',
        data: {
          onAdd: (next: string) => {
            if (conditions[next]) {
              onError?.(new Error('condition already exists'));
              throw new Error('condition already exists');
            }
            conditions[next] = [];
            // We need to rebuild the object content without losing its reference
            // to keep default at last position
            const prevConditions = { ...conditions };
            const keys = Object.keys(conditions);
            const conditionsKeys = keys.filter((k) => k !== 'default');
            keys.forEach((k) => {
              delete conditions[k];
            });
            conditionsKeys.forEach((k) => {
              conditions[k] = prevConditions[k];
            });
            conditions.default = prevConditions.default;
            onAutomationChange?.(automation, { add: true });
          },
        },
      };
      edges.push(edge);

      // build instructions
      const res = buildInstructions({
        instructions,
        parentNode: node,
      });
      const endNode = {
        id: v4(),
        type: 'empty',
        position: { x: res.maxX, y: res.maxY + height },
        data: {},
      };
      nodes.push(endNode);
      edges.push({
        id: `${res.endNode.id}--${endNode.id}`,
        source: res.endNode.id,
        target: endNode.id,
        type: 'instruction',
        data: {
          onAdd(instruction: Prismeai.Instruction) {
            instructions.push(instruction);
            onAutomationChange?.(automation, { add: true });
          },
        },
      });

      lastNodes.push(endNode);

      maxY = Math.max(maxY, endNode.position.y);
      maxX = Math.max(maxX, endNode.position.x);
    });

    // Set end of conditions
    maxY = maxY + height;
    const endNode = {
      id: v4(),
      type: 'empty',
      position: { x: parentNode?.position?.x || 0, y: maxY },
      data: {},
    };
    nodes.push(endNode);
    lastNodes.forEach((node) => {
      const source = node.id;
      const target = endNode.id;
      edges.push({
        id: `${source}--${target}`,
        source,
        target,
        type: 'empty',
        data: {
          pathStyle: 'smooth',
        },
      });
    });

    return {
      endNode,
      maxX,
      maxY,
    };
  }

  function buildAll({ all, parentNode }: { all: Prismeai.Instruction[]; parentNode: Node }) {
    let maxY = parentNode.position.y;
    let maxX = parentNode?.position?.x || 0;

    if (all && Array.isArray(all)) {
      all.forEach((instruction, index) => {
        if (!instruction || typeof instruction !== 'object') return;
        const [slug] = Object.keys(instruction);
        const config: Record<string, Prismeai.Instruction[]> =
          instruction[slug as keyof typeof instruction];
        if (slug === 'conditions') {
          const emptyNode = {
            id: v4(),
            position: { x: maxX + width, y: parentNode.position.y + 25 },
            type: 'empty',
            data: {},
          };
          nodes.push(emptyNode);

          edges.push({
            id: `${parentNode.id}--${emptyNode.id}`,
            type: 'instruction',
            source: parentNode.id,
            target: emptyNode.id,
            sourceHandle: 'right',
            data: {
              onAdd(instruction: Prismeai.Instruction) {
                all.splice(index, 0, instruction);
                onAutomationChange?.(automation, { add: true });
              },
              pathStyle: 'straight',
            },
          });

          const node = {
            id: getId(instruction),
            position: { x: maxX + width, y: maxY + height },
            type: 'instruction',
            data: {
              instruction: slug,
              config,
              handles: getHandles(slug),
            },
          };
          maxY = Math.max(maxY, maxY + height);
          nodes.push(node);

          edges.push({
            id: `${emptyNode.id}--${node.id}`,
            type: 'empty',
            source: emptyNode.id,
            target: node.id,
            data: {
              pathStyle: 'straight',
            },
          });

          maxX = node.position.x;

          const res = buildConditions({
            conditions: config as Record<string, Prismeai.Instruction[]>,
            parentNode: node,
          });

          maxX = Math.max(maxX, res.maxX);
          maxY = Math.max(maxY, res.maxY);
        } else if (slug === 'all') {
          const emptyNode = {
            id: v4(),
            position: { x: maxX + width, y: parentNode.position.y + 25 },
            type: 'empty',
            data: {},
          };
          nodes.push(emptyNode);

          edges.push({
            id: `${parentNode.id}--${emptyNode.id}`,
            type: 'instruction',
            source: parentNode.id,
            target: emptyNode.id,
            sourceHandle: 'right',
            data: {
              onAdd(instruction: Prismeai.Instruction) {
                all.splice(index, 0, instruction);
                onAutomationChange?.(automation, { add: true });
              },
              pathStyle: 'straight',
            },
          });

          const node = {
            id: getId(instruction),
            position: { x: maxX + width, y: maxY + height },
            type: 'instruction',
            data: {
              instruction: 'all',
              config,
              handles: getHandles(slug),
            },
          };
          maxY = Math.max(maxY, maxY + height);
          nodes.push(node);

          edges.push({
            id: `${emptyNode.id}--${node.id}`,
            type: 'empty',
            source: emptyNode.id,
            target: node.id,
            data: {
              pathStyle: 'straight',
            },
          });

          maxX = node.position.x;
          const res = buildAll({
            all: config as unknown as Prismeai.Instruction[],
            parentNode: node,
          });

          maxX = Math.max(maxX, res.maxX);
          maxY = Math.max(maxY, res.maxY);
        } else if (slug === 'repeat') {
          const emptyNode = {
            id: v4(),
            position: { x: maxX + width, y: parentNode.position.y + 25 },
            type: 'empty',
            data: {},
          };
          nodes.push(emptyNode);

          edges.push({
            id: `${parentNode.id}--${emptyNode.id}`,
            type: 'instruction',
            source: parentNode.id,
            target: emptyNode.id,
            sourceHandle: 'right',
            data: {
              onAdd(instruction: Prismeai.Instruction) {
                all.splice(index, 0, instruction);
                onAutomationChange?.(automation, { add: true });
              },
              pathStyle: 'straight',
            },
          });

          const node = {
            id: getId(instruction),
            position: { x: maxX + width, y: maxY + height },
            type: 'instruction',
            data: {
              instruction: slug,
              config,
              handles: getHandles(slug, { withSource: false }),
            },
          };
          maxY = Math.max(maxY, maxY + height);
          nodes.push(node);

          edges.push({
            id: `${emptyNode.id}--${node.id}`,
            type: 'empty',
            source: emptyNode.id,
            target: node.id,
            data: {
              pathStyle: 'straight',
            },
          });

          maxX = node.position.x;
          const res = buildRepeat({
            instructions: (config as unknown as Prismeai.Repeat['repeat']).do,
            parentNode: node,
          });

          maxX = Math.max(maxX, res.maxX);
          maxY = Math.max(maxY, res.maxY);
        } else {
          const node = {
            id: getId(instruction),
            position: { x: maxX + width, y: parentNode.position.y },
            type: 'instruction',
            data: {
              instruction: slug,
              config,
              onSave(config: unknown) {
                all[index] = {
                  [slug]: config,
                };
                onAutomationChange?.(automation);
              },
              onDelete() {
                delete all[index];
                onAutomationChange?.(automation);
              },
              handles: [
                {
                  type: 'target',
                  position: 'left',
                  style: {
                    background: 'transparent',
                    border: 'transparent',
                  },
                },
                {
                  type: 'source',
                  position: 'right',
                  style: {
                    background: 'transparent',
                    border: 'transparent',
                  },
                },
              ],
            },
          };
          nodes.push(node);

          edges.push({
            id: `${parentNode.id}--${node.id}`,
            type: 'instruction',
            source: parentNode.id,
            target: node.id,
            sourceHandle: 'right',
            data: {
              onAdd: (instruction: Prismeai.Instruction) => {
                all.splice(index, 0, instruction);
                onAutomationChange?.(automation, { add: true });
              },
              pathStyle: 'straight',
              invisible: true,
              style: {
                marginTop: '-9px',
              },
            },
          });

          maxX = node.position.x;
        }
      });
    }

    const addNode = {
      id: v4(),
      type: 'empty',
      position: { x: maxX + width / 2, y: parentNode.position.y + 25 },
      data: {},
    };
    nodes.push(addNode);

    edges.push({
      id: `${parentNode.id}--${addNode.id}`,
      source: parentNode.id,
      target: addNode.id,
      sourceHandle: 'right',
      type: 'instruction',
      data: {
        onAdd(instruction: Prismeai.Instruction) {
          all.push(instruction);
          onAutomationChange?.(automation, { add: true });
        },
        pathStyle: 'straight',
        buttonPosition: 'end',
      },
    });

    return { endNode: parentNode, maxX, maxY };
  }

  function buildRepeat({
    instructions,
    parentNode,
  }: {
    instructions: Prismeai.Instruction[];
    parentNode: Node;
  }) {
    let maxY = parentNode.position.y;
    const maxX = parentNode.position.x;
    const startNode = {
      id: v4(),
      type: 'empty',
      position: { x: maxX + width, y: maxY + 13 },
      data: {},
    };
    nodes.push(startNode);
    edges.push({
      id: `${parentNode.id}--${startNode.id}`,
      source: parentNode.id,
      target: startNode.id,
      sourceHandle: 'repeat-start',
      type: 'empty',
      data: {
        pathStyle: 'straight',
      },
    });
    let prevNode: Node = startNode;
    maxY = startNode.position.y;
    if (instructions && Array.isArray(instructions) && instructions.length) {
      const res = buildInstructions({ instructions, parentNode: startNode });
      maxY = res.maxY;
      prevNode = res.endNode;
    }
    maxY = maxY + height;
    const endNode = {
      id: v4(),
      type: 'empty',
      position: { x: maxX + width, y: maxY },
      data: {},
    };
    nodes.push(endNode);
    edges.push({
      id: `${prevNode.id}--${endNode.id}`,
      source: prevNode.id,
      target: endNode.id,
      type: 'instruction',
      data: {
        onAdd(instruction: Prismeai.Instruction) {
          instructions.push(instruction);
          onAutomationChange?.(automation, { add: true });
        },
      },
    });
    edges.push({
      id: `${endNode.id}--${parentNode.id}`,
      source: endNode.id,
      target: parentNode.id,
      type: 'empty',
      targetHandle: 'repeat-end',
      data: {
        pathStyle: 'smooth',
      },
    });
    return {
      endNode: parentNode,
      maxY,
      maxX,
    };
  }

  function buildInstructions({
    instructions,
    parentNode,
  }: {
    instructions: Prismeai.Instruction[];
    parentNode?: Node;
  }) {
    let prevNode = parentNode;
    let maxX = prevNode?.position?.x || 0;
    let maxY = prevNode?.position?.y || 0;
    instructions.forEach((instruction, index) => {
      if (!instruction || typeof instruction !== 'object') return;
      const [slug] = Object.keys(instruction);
      const config: unknown = instruction[slug as keyof typeof instruction];
      maxY = maxY + height;
      const node = {
        id: getId(instruction),
        data: {
          instruction: slug,
          config,
          handles: getHandles(slug),
          onSave(config: Record<string, unknown>) {
            instructions[index] = {
              [slug]: config,
            };
            onAutomationChange?.(automation);
          },
          onDelete() {
            delete instructions[index];
            onAutomationChange?.(automation, { deleted: node.id });
          },
        },
        type: 'instruction',
        position: {
          x: prevNode?.position?.x || 0,
          y: maxY,
        },
      };
      nodes.push(node);

      if (prevNode) {
        const source = prevNode.id;
        const target = node.id;
        const edge = {
          id: `${prevNode.id}--${node.id}`,
          source,
          target,
          type: 'instruction',
          data: {
            onAdd: (instruction: Prismeai.Instruction) => {
              instructions.splice(index, 0, instruction);
              onAutomationChange?.(automation, { add: true });
            },
          },
        };
        edges.push(edge);
      }
      prevNode = node;
      maxX = Math.max(maxX, node.position.x);
      maxY = Math.max(maxY, node.position.y);

      if (slug === 'conditions') {
        const conditions = config as Record<string, Prismeai.Instruction[]>;
        const res = buildConditions({
          conditions,
          parentNode: prevNode,
        });
        prevNode = res.endNode;
        maxX = Math.max(maxX, res.maxX);
        maxY = Math.max(maxY, res.maxY);
      }
      if (slug === 'all') {
        const all = config as Prismeai.Instruction[];
        const res = buildAll({
          all,
          parentNode: prevNode,
        });
        prevNode = res.endNode;
        maxX = Math.max(maxX, res.maxX);
        maxY = Math.max(maxY, res.maxY);
      }
      if (slug === 'repeat') {
        const repeatConfig = config as Prismeai.Repeat['repeat'];
        if (!repeatConfig || typeof repeatConfig !== 'object') return;
        if (!repeatConfig.do) {
          repeatConfig.do = [];
        }
        const res = buildRepeat({
          instructions: repeatConfig.do,
          parentNode: prevNode,
        });
        prevNode = res.endNode;
        maxX = Math.max(maxX, res.maxX);
        maxY = Math.max(maxY, res.maxY);
      }
    });

    return { endNode: prevNode || ({} as Node), maxX, maxY };
  }

  const { do: instructions, when, output } = changedAutomation;
  const startNode = {
    id: 'start',
    data: {
      ...when,
      onSave(when: Prismeai.When) {
        automation.when = when;
        onAutomationChange?.(automation);
      },
      endpointUrl,
    },
    position: { x: 0, y: 0 },
    type: 'start',
  };
  nodes.push(startNode);
  let maxY = height;
  let prevNode: Node = startNode;
  if (instructions && Array.isArray(instructions)) {
    const res = buildInstructions({
      instructions,
      parentNode: startNode,
    });
    prevNode = res.endNode;
    maxY = maxY + res.maxY;
  }
  const endNode = {
    id: 'output',
    data: {
      output,
      onSave(output: string) {
        automation.output = output;
        onAutomationChange?.(automation);
      },
    },
    position: { x: 0, y: maxY },
    type: 'end',
  };
  nodes.push(endNode);
  edges.push({
    id: `${prevNode.id}--${endNode.id}`,
    source: prevNode.id,
    target: endNode.id,
    type: 'instruction',
    data: {
      onAdd(instruction: Prismeai.Instruction) {
        automation.do.push(instruction);
        onAutomationChange?.(automation, { add: true });
      },
    },
  });

  return {
    nodes,
    edges,
  };
}
