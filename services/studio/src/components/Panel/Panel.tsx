import { ReactNode, useCallback, useEffect, useState } from 'react';
import LeftIcon from '@/svgs/chevron.svg';
import Storage from '@/utils/Storage';
import { useTracking } from '@/providers/Tracking';
import { useTranslations } from 'next-intl';
import { Button, Tooltip } from 'antd';
import CloseIcon from '@/svgs/close.svg';
import CompressIcon from '@/svgs/compress.svg';
import ExpandIcon from '@/svgs/expand.svg';
import GearIcon from '@/svgs/gear.svg';

const noop = () => null;
interface PanelProps {
  visible: boolean;
  title: string;
  onVisibleChange?: (v: boolean) => void;
  className?: string;
  onBack?: () => void;
  context?: 'automations' | 'pages';
  children: ReactNode;
}
export const Panel = ({
  visible,
  title,
  onVisibleChange = noop,
  onBack,
  className,
  children,
}: PanelProps) => {
  const t = useTranslations('workspaces');
  const [hidden, setHidden] = useState(true);
  const [large, setLarge] = useState(!!Storage.get('__panel__large'));
  const { trackEvent } = useTracking();

  useEffect(() => {
    if (hidden) {
      const t = setTimeout(() => onVisibleChange(false), 200);
      return () => {
        clearTimeout(t);
      };
    }
  }, [hidden, onVisibleChange]);

  useEffect(() => {
    setTimeout(() => setHidden(!visible), 1);
  }, [visible]);

  const toggleLarge = useCallback(() => {
    setLarge((prev) => {
      const large = !prev;
      if (large) {
        trackEvent({
          name: 'Reduce panel width',
          action: 'click',
        });
        Storage.set('__panel__large', 1);
      } else {
        trackEvent({
          name: 'Enlarge panel width',
          action: 'click',
        });
        Storage.remove('__panel__large');
      }
      return large;
    });
  }, [trackEvent]);

  return (
    <div
      className={` ${large ? 'panel-is-large' : ''} absolute bottom-0 top-0 flex ${
        large ? '-right-full w-full' : '-right-[30rem] w-[30rem]'
      } z-10 flex-col overflow-hidden bg-[var(--pr-menu-bg-color)] transition-transform duration-200 ease-in ${hidden ? '' : '-translate-x-full'} ${className || ''} `}
      data-testid="panel"
    >
      <div className="bg-dark-accent flex w-full flex-row items-center justify-between p-5 font-semibold">
        <div className="flex flex-row items-center">
          {onBack && (
            <button
              onClick={() => {
                trackEvent({
                  name: 'Back to blocks list',
                  action: 'click',
                });
                onBack();
              }}
              className="mx-1 flex w-[20px] items-center"
              data-testid="panel-back-btn"
            >
              <span className="flex rotate-90">
                <LeftIcon width=".8rem" height=".8rem" />
              </span>
            </button>
          )}
          {!onBack && <GearIcon className="mr-3 text-[20px] font-bold" />}
          {title}
        </div>
        <div className="flex">
          <Tooltip title={t(`panel.enlarge_${large ? 'off' : 'on'}`)} placement="bottom">
            <Button
              type="default"
              className="flex items-center justify-center"
              onClick={toggleLarge}
              data-testid="panel-enlarge-btn"
            >
              {large ? <CompressIcon /> : <ExpandIcon />}
            </Button>
          </Tooltip>
          <Tooltip title={t('panel.close')} placement="bottom">
            <Button
              type="default"
              className="flex items-center justify-center"
              onClick={() => {
                trackEvent({
                  name: 'Close panel',
                  action: 'click',
                });
                setHidden(true);
              }}
              data-testid="panel-close-btn"
            >
              <CloseIcon />
            </Button>
          </Tooltip>
        </div>
      </div>
      <div className="border-light-gray flex h-full flex-1 flex-col overflow-y-scroll border-l">
        {children}
      </div>
    </div>
  );
};

export default Panel;
