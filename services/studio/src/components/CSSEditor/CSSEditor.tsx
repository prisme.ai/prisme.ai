import { useField } from 'react-final-form';
import { CodeEditor } from '@/components/CodeEditor/lazy';
import { useEffect, useState } from 'react';
import { defaultStyles as _defaultStyles } from '../../layouts/WorkspaceBuilder/Workspace/Page/defaultStyles';
import { FieldProps, InfoBubble } from '@/components/SchemaForm';
import { useTranslations } from 'next-intl';
import { Tooltip } from 'antd';
import TrashIcon from '@/svgs/trash.svg';
import styles from './css-editor.module.scss';

interface CSSEditorProps extends FieldProps {
  label?: string;
  description?: string;
  reset?: string;
  defaultStyles?: string;
}

export const CSSEditor = ({
  name,
  label = 'pages.details.styles.label',
  description = 'pages.details.styles.description',
  reset = 'pages.details.styles.reset.description',
  defaultStyles = _defaultStyles,
}: CSSEditorProps) => {
  const t = useTranslations('workspaces');
  const field = useField(name);
  const [reseting, setReseting] = useState(false);
  useEffect(() => {
    if (!reseting) return;
    field.input.onChange(defaultStyles);
    setReseting(false);
  }, [defaultStyles, field.input, reseting]);

  return (
    <div>
      <div className={styles['css-editor-label-ctn']}>
        <div className={styles['css-editor-label']}>
          <label>{t(label)}</label>
          <InfoBubble text={t(description)} />
        </div>
        <Tooltip title={t(reset)}>
          <button
            type="button"
            onClick={(event) => {
              event.stopPropagation();
              setReseting(true);
            }}
          >
            <TrashIcon />
          </button>
        </Tooltip>
      </div>
      <div className={styles['css-editor-ctn']}>
        {!reseting && (
          <CodeEditor mode="css" value={field.input.value} onChange={field.input.onChange} />
        )}
      </div>
    </div>
  );
};
export default CSSEditor;
