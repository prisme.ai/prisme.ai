'use client';

import {
  createContext,
  ReactNode,
  useCallback,
  useContext,
  useEffect,
  useRef,
  useState,
} from 'react';
import Storage from '@/utils/Storage';
import { ConfigProvider, theme } from 'antd';
import isServerSide from '@/utils/isServerSide';
import Cookies from 'js-cookie';

export type Schemes = 'dark' | 'light';
export interface ColorSchemeManagerContext {
  scheme: Schemes;
  setScheme: (mode: Schemes) => void;
  toggleScheme: () => void;
  toggleAuto: (auto: boolean) => void;
}

export const colorSchemeManagerContext = createContext<ColorSchemeManagerContext | undefined>(
  undefined,
);

export const useColorSchemeManager = () => {
  const context = useContext(colorSchemeManagerContext);
  if (!context) {
    throw new Error();
  }
  return context;
};

interface ColorSchemeManagerProviderProps {
  children: ReactNode;
}

const cssVarsKeys = [
  '--pr-main-bg-color',
  '--pr-accent-color',
  '--pr-accent-color-contrast',
] as const;
type CssVars = Record<(typeof cssVarsKeys)[number], string>;

function getAutoScheme() {
  return window.matchMedia('(prefers-color-scheme: dark)').matches ? 'dark' : 'light';
}
function getInitialScheme() {
  if (isServerSide()) return 'light';

  return Storage.get('color-scheme') || getAutoScheme();
}

export function ColorSchemeManagerProvider({ children }: ColorSchemeManagerProviderProps) {
  const [scheme, _setScheme] = useState<Schemes>(getInitialScheme());

  const auto = useRef(true);
  const lockScheme = useRef(false);

  const setScheme = useCallback((scheme: Schemes) => {
    if (lockScheme.current) return;
    _setScheme(scheme);
    document.documentElement.setAttribute('data-color-scheme', scheme);
    if (auto.current) {
      Storage.set('color-scheme', scheme);
    }
    Cookies.set('color-scheme', scheme);
  }, []);

  const toggleScheme = useCallback(() => {
    setScheme(scheme === 'light' ? 'dark' : 'light');
  }, [scheme, setScheme]);

  const toggleAuto = useCallback(
    (newValue: boolean) => {
      auto.current = newValue;
      if (newValue) {
        setScheme(getInitialScheme());
      }
    },
    [setScheme],
  );

  useEffect(() => {
    const listener = (e: MessageEvent) => {
      if (e.data.type !== 'prColorSchemeUpdate' || !auto.current) return;
      if (!['dark', 'light'].includes(e.data.scheme)) return;
      lockScheme.current = false;
      setScheme(e.data.scheme);
      lockScheme.current = true;
    };
    window.addEventListener('message', listener);
    return () => {
      window.removeEventListener('message', listener);
    };
  }, [setScheme]);

  useEffect(() => {
    document.documentElement.setAttribute('data-color-scheme', scheme);
    window.Prisme = window.Prisme || {};
    window.Prisme.ai = window.Prisme.ai || {};
    window.Prisme.ai.colorScheme = {
      get: () => scheme,
      set: setScheme,
      toggle: toggleScheme,
    };
  }, [scheme, setScheme, toggleScheme]);

  const [cssVars, setCssVars] = useState<CssVars>({} as CssVars);
  useEffect(() => {
    const root = getComputedStyle(document.documentElement);
    setCssVars(
      cssVarsKeys.reduce(
        (prev, key) => ({
          ...prev,
          [key]: root.getPropertyValue(key).trim(),
        }),
        {} as CssVars,
      ),
    );
  }, [scheme]);

  return (
    <colorSchemeManagerContext.Provider value={{ scheme, setScheme, toggleScheme, toggleAuto }}>
      <meta name="theme-color" content={cssVars['--pr-main-bg-color']} key="theme-color" />
      <ConfigProvider
        theme={{
          algorithm: scheme === 'dark' ? theme.darkAlgorithm : theme.defaultAlgorithm,
          token: {
            colorPrimary: cssVars['--pr-accent-color'] || '#1b4abb',
            fontFamily: 'var(--font-satoshi-variable), sans-serif',
          },
          components: {
            Button: {
              primaryColor: cssVars['--pr-accent-color-contrast'],
            },
            Segmented: {
              itemSelectedBg: 'var(--pr-accent-color)',
              itemSelectedColor: 'var(--pr-accent-color-contrast)',
              trackBg: 'var(--pr-input-bg-color)',
            },
            Modal: {
              boxShadow:
                scheme === 'dark' ? '0 0.2rem 1rem 0rem var(--pr-accent-color)' : undefined,
            },
          },
        }}
      >
        {children}
      </ConfigProvider>
    </colorSchemeManagerContext.Provider>
  );
}
