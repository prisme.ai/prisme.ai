import {
  createContext,
  ReactNode,
  useState,
  useCallback,
  useEffect,
  useRef,
  useContext,
} from 'react';
import { Events } from '@prisme.ai/sdk';
import Loading from '@/components/Loading';
import { notFound } from 'next/navigation';
import useApi from '@/utils/api/useApi';

export interface AppInstanceContext {
  appInstance: Prismeai.AppInstance;
  documentation: Prismeai.Page | null;
  loading: boolean;
  fetchAppInstance: () => void;
  saveAppInstance: (app: Prismeai.AppInstance) => Promise<Prismeai.AppInstance | null>;
  saving: boolean;
  uninstallApp: () => Promise<Prismeai.AppInstance | null>;
}

interface AppInstanceProviderProps {
  id: string;
  workspaceId: string;
  events: Events;
  children: ReactNode;
}

export const appInstanceContext = createContext<AppInstanceContext | undefined>(undefined);

export const useAppInstance = () => {
  const context = useContext(appInstanceContext);
  if (!context) {
    throw new Error();
  }
  return context;
};

export const AppInstanceProvider = ({
  id,
  workspaceId,
  events,
  children,
}: AppInstanceProviderProps) => {
  const api = useApi();
  const [slug, setSlug] = useState(id);
  void slug;
  const [appInstance, setAppInstance] = useState<AppInstanceContext['appInstance']>();
  const [documentation, setDocumentation] = useState<Prismeai.Page | null>(null);
  const [loading, setLoading] = useState<AppInstanceContext['loading']>(true);
  const [saving, setSaving] = useState<AppInstanceContext['saving']>(false);
  const [isNotFound, setNotFound] = useState(false);

  const fetchAppInstance: AppInstanceContext['fetchAppInstance'] = useCallback(async () => {
    try {
      setNotFound(false);
      const {
        documentation = null,
        appSlug = '',
        ...rest
      } = await api.getAppInstance(workspaceId, id);
      const appInstance = {
        appSlug,
        ...rest,
      };
      setAppInstance(appInstance);
      setDocumentation(documentation);
      return appInstance;
    } catch (e) {
      setNotFound(true);
    }
  }, [id, workspaceId, api]);

  const saveAppInstance: AppInstanceContext['saveAppInstance'] = useCallback(
    async (data) => {
      if (!workspaceId || !appInstance || !appInstance.slug) return null;
      setSaving(true);
      const { config } = data;
      void config;
      try {
        const saved = await api.saveAppInstance(workspaceId, appInstance.slug, {
          ...data,
          config: data?.config?.value,
        });
        const nextAppInstance = {
          ...saved,
          config: { ...appInstance.config, value: saved?.config?.value },
        };
        setAppInstance(nextAppInstance);
        setSaving(false);
        if (nextAppInstance.slug) {
          setSlug(nextAppInstance.slug);
        }
        return nextAppInstance;
      } catch (e) {
        setSaving(false);
        throw e;
      }
    },
    [appInstance, workspaceId, api],
  );

  const uninstallApp: AppInstanceContext['uninstallApp'] = useCallback(async () => {
    if (!workspaceId || !appInstance || !appInstance.slug) return null;
    api.uninstallApp(workspaceId, appInstance?.slug);
    return appInstance;
  }, [appInstance, workspaceId, api]);

  const prevId = useRef<string>('');
  useEffect(() => {
    if (prevId.current === id) return;
    prevId.current = id;
    const initialFetch = async () => {
      setLoading(true);
      await fetchAppInstance();
      setLoading(false);
    };
    initialFetch();
  }, [fetchAppInstance, id]);

  useEffect(() => {
    const off = events.on('workspaces.apps.configured', ({ payload }) => {
      if (!appInstance || !payload.appInstance || appInstance.slug !== payload.appInstance.slug)
        return;
      const updated = {
        ...appInstance,
        config: {
          ...appInstance.config,
          value: payload.appInstance.config,
        },
      };
      setAppInstance(updated);
    });

    return () => {
      off();
    };
  }, [appInstance, events]);

  if (loading) return <Loading />;
  if (isNotFound) return notFound();
  if (!appInstance) return null;

  return (
    <appInstanceContext.Provider
      value={{
        appInstance,
        // TODO : afficher la dod avec un /_doc dans services/pages
        documentation,
        loading,
        fetchAppInstance,
        saveAppInstance,
        saving,
        uninstallApp,
      }}
    >
      {children}
    </appInstanceContext.Provider>
  );
};

export default AppInstanceProvider;
