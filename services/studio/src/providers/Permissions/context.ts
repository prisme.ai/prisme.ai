import { createContext, useContext } from 'react';
import { UserPermissions } from '@prisme.ai/sdk';

type SubjectType = PrismeaiAPI.GetPermissions.Parameters.SubjectType;

type Roles = PrismeaiAPI.GetRoles.Responses.$200;
export interface PermissionsContext {
  usersPermissions: Map<string, UserPermissions[]>;
  roles: Roles;
  addUserPermissions: (
    subjectType: SubjectType,
    subjectId: string,
    permissions: UserPermissions,
  ) => Promise<UserPermissions | null>;
  removeUserPermissions: (
    subjectType: SubjectType,
    subjectId: string,
    target: UserPermissions['target'],
  ) => Promise<PrismeaiAPI.RevokePermissions.Responses.$200 | null>;
  getUsersPermissions: (subjectType: SubjectType, subjectId: string) => Promise<UserPermissions[]>;
  getRoles: (subjectType: SubjectType, subjectId: string) => Promise<Roles>;
}

export const workspacesContext = createContext<PermissionsContext | undefined>(undefined);

export const usePermissions = () => {
  const context = useContext(workspacesContext);
  if (!context) {
    throw new Error();
  }
  return context;
};

export default workspacesContext;
