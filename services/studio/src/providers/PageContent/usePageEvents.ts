import { useEffect, useRef, useState } from 'react';
import { useUser } from '@/providers/User';
import { useRedirect } from './useRedirect';
import useApi from '@/utils/api/useApi';
import { Events } from '@prisme.ai/sdk';
import useRouter from '@/utils/useRouter';
import isServerSide from '@/utils/isServerSide';

export const usePageEvents = (page: Prismeai.Page | null) => {
  const api = useApi();
  const { user } = useUser();
  const [events, setEvents] = useState<Events>();
  const { push } = useRouter();

  // init socket
  const prevSocketWorkspaceId = useRef('');
  useEffect(() => {
    if (!user) return;
    let off: () => void;
    async function initEvents() {
      if (isServerSide()) return;

      if (!page || !page.workspaceId || prevSocketWorkspaceId.current === page.workspaceId) return;

      prevSocketWorkspaceId.current = page.workspaceId;
      const events = await api.streamEvents(page.workspaceId, {
        'source.sessionId': true,
      });

      setEvents((prev) => {
        if (prev) {
          prev.destroy();
        }
        if (typeof window !== 'undefined') {
          window.Prisme = window.Prisme || {};
          window.Prisme.ai = window.Prisme.ai || {};
          window.Prisme.ai.events = events;
          window.Prisme.ai.api = api;
        }
        return events;
      });

      off = events.once('error', (payload: any) => {
        console.error('ERROR', payload);
        if (payload?.error !== 'ForbiddenError') return;
        const eventName = payload?.details?.subject?.type;
        if (!eventName) return;
        events.emit(eventName, payload?.details?.subject?.payload);
      });
    }
    initEvents();

    return () => {
      off?.();
    };
  }, [page, user, api]);

  useEffect(() => {
    return () => {
      events?.destroy();
    };
  }, [events]);

  // Listen to update page events
  const redirect = useRedirect();
  useEffect(() => {
    if (!page || !events) return;
    const offs: Function[] = [];

    if (page.updateOn) {
      offs.push(
        events.on(page.updateOn, ({ payload }) => {
          redirect(payload);
        }),
      );
    }

    if (page.notifyOn) {
      offs.push(
        events.on(page.notifyOn, async ({ payload: { title, body, icon } }) => {
          try {
            await Notification.requestPermission();
            new Notification(title, {
              body,
              icon,
            });
          } catch {}
        }),
      );
    }

    return () => {
      offs.forEach((off) => off());
    };
  }, [events, page, push, redirect]);

  return { events };
};

export default usePageEvents;
