import { useCallback, useRef, useState } from 'react';
import { HTTPError } from '@prisme.ai/sdk';
import BUILTIN_PAGES from './builtinPages';
import useApi from '@/utils/api/useApi';
import { useUrls } from '@/utils/urls';

export const usePageFetcher = () => {
  const api = useApi();
  const [page, setPage] = useState<Prismeai.DetailedPage>();

  const [error, setError] = useState<null | number>(null);
  const [loading, setLoading] = useState(true);
  const { getSubdomain } = useUrls();
  const lastDisplayedPage = useRef<Prismeai.DetailedPage>({
    slug: 'Loading',
    appInstances: [],
    blocks: [
      {
        slug: 'RichText',
        content: `<div class="flex flex-1 align-center justify-center "><div class="ant-spin ant-spin-spinning !flex justify-center items-center " aria-live="polite" aria-busy="true"><span class="ant-spin-dot ant-spin-dot-spin"><i class="ant-spin-dot-item"></i><i class="ant-spin-dot-item"></i><i class="ant-spin-dot-item"></i><i class="ant-spin-dot-item"></i></span></div></div>`,
      },
    ],
    styles: `.page-blocks {
  justify-content: center;
}`,
  });

  const fetchPage = useCallback(
    async (slug: string) => {
      const workspaceSlug = getSubdomain(window.location.host);
      try {
        const page = await api.getPageBySlug(workspaceSlug, slug === '' ? 'index' : slug);
        setPage(page);
        lastDisplayedPage.current = page;
      } catch (e) {
        const statusCode = (e as HTTPError).code;
        setError(statusCode);
        if ([401, 403].includes(statusCode)) {
          const fallbackSlug = [401, 403].includes((e as HTTPError).code) ? '_401' : slug;
          const builtinPage = BUILTIN_PAGES.find(({ slug }) => slug === fallbackSlug);
          let page;
          if (builtinPage) {
            page = builtinPage;
          }
          if (page && page.slug === '_401') {
            try {
              page = await api.getPageBySlug(workspaceSlug, '_401');
            } catch {}
          }
          setPage(page);
        } else {
          setPage(undefined);
        }
      }
      setLoading(false);
    },
    [api, getSubdomain],
  );

  const setPageFromChildren = useCallback(
    (page: Prismeai.DetailedPage | null, error?: number | null) => {
      if (page || (error && ![401, 403].includes(error))) {
        page && setPage(page);
        setLoading(false);
        return;
      }
    },
    [],
  );

  return { page, setPage: setPageFromChildren, loading, fetchPage, error };
};

export default usePageFetcher;
