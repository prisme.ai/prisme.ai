'use client';

import { createContext, ReactNode, useContext, useEffect } from 'react';
import usePageEvents from './usePageEvents';
import usePageFetcher from './usePageFetcher';
import usePageParent from './usePageParent';
import { useColorSchemeManager } from '../ColorSchemeManager';

export interface PageContentContext
  extends ReturnType<typeof usePageFetcher>,
    ReturnType<typeof usePageEvents> {
  error: number | null;
}
export const pageContentContext = createContext<PageContentContext | undefined>(undefined);

export const usePageContent = () => {
  const context = useContext(pageContentContext);
  if (!context) {
    throw new Error();
  }
  return context;
};

export interface PageContentProviderProps {
  styles?: string;
  initialConfig?: Record<string, any>[];
  clientId?: string;
  children?: ReactNode;
}

export const PageContentProvider = ({ children }: PageContentProviderProps) => {
  const { page, setPage, loading, fetchPage, error } = usePageFetcher();
  const { events } = usePageEvents(page || null);
  usePageParent(events, setPage);
  const { setScheme, toggleAuto } = useColorSchemeManager();

  useEffect(() => {
    if (!page) return;
    const colorScheme = page?.colorScheme || 'light';
    const isAuto = colorScheme === 'auto';
    toggleAuto(isAuto);
    if (isAuto) return;
    setScheme(colorScheme);
  }, [page, setScheme, toggleAuto]);

  return (
    <pageContentContext.Provider
      value={{
        page,
        setPage,
        loading,
        fetchPage,
        events,
        error: error || null,
      }}
    >
      {children}
    </pageContentContext.Provider>
  );
};

export default pageContentContext;
