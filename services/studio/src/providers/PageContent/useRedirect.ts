import { useCallback } from 'react';
import { usePathname } from 'next/navigation';
import { getPathname, useRouter } from '@/i18n/routing';
import { useLocale } from 'next-intl';

interface RedirectGetAttributes {
  url?: string;
  redirect?: {
    url?: string;
    method?: string;
    body?: Record<string, any>;
    locale?: string;
    push?: string;
    // Temp. Need to be replace by a openOn page action
    popup?: boolean;
  };
}

export function useRedirect() {
  const { push } = useRouter();
  const pathname = usePathname();
  const currentLocale = useLocale();

  return useCallback(
    ({ url, redirect }: RedirectGetAttributes) => {
      function redirectGet(url: string, locale?: string) {
        if (url.match(/^#/)) {
          window.location.hash = url;
          return;
        }
        if (url.match(/^http/)) {
          window.location.href = url;
          return;
        }
        const cleanedUrl = url.replace(/(^\/)index/, '$1').replace(/\/+/, '/');
        if (!locale || currentLocale === locale) {
          return push(cleanedUrl, { locale });
        }
        return window.location.assign(
          getPathname({
            href: cleanedUrl,
            locale,
          }),
        );
      }
      function redirectPost(url: string, body: Record<string, string>) {
        const form = document.createElement('form');
        form.setAttribute('action', url);
        form.setAttribute('method', 'post');
        Object.entries(body).forEach(([k, v]) => {
          const field = document.createElement('input');
          field.setAttribute('type', 'hidden');
          field.setAttribute('name', k);
          field.setAttribute('value', v);
          form.appendChild(field);
        });
        document.body.appendChild(form);
        form.submit();
      }

      if (redirect) {
        const { url = pathname, method = 'get', body = {}, locale, push, popup } = redirect;
        if (!url) return;
        if (popup) {
          return window.open(url);
        }
        if (push) {
          return window.history.pushState({}, '', url);
        }
        if (`${method}`.toLowerCase() === 'get') {
          return redirectGet(url, locale);
        }
        return redirectPost(url, body);
      }
      if (url) {
        redirectGet(url);
      }
    },
    [pathname, push, currentLocale],
  );
}
