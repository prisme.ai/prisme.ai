import { notification } from 'antd';
import { NotificationInstance } from 'antd/es/notification/interface';
import { createContext, ReactNode, useContext } from 'react';

const notificationsContext = createContext<NotificationInstance | undefined>(undefined);
export const useNotifications = () => {
  const context = useContext(notificationsContext);
  if (!context) {
    throw new Error();
  }
  return context;
};

interface NotificationsProps {
  children: ReactNode;
}

export default function Notifications({ children }: NotificationsProps) {
  const [api, context] = notification.useNotification();
  return (
    <notificationsContext.Provider value={api}>
      {context}
      {children}
    </notificationsContext.Provider>
  );
}
