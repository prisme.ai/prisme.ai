import { expect, it } from 'vitest';
import { compareSearchParams } from './compareSearchParams';

it('should compare search params', () => {
  expect(
    compareSearchParams(
      new URLSearchParams('?foo=bar&bar=1'),
      new URLSearchParams('?foo=bar&bar=1'),
    ),
  ).toBe(true);
  expect(
    compareSearchParams(
      new URLSearchParams('?foo=bar&bar=1'),
      new URLSearchParams('?foo=bar&bar=2'),
    ),
  ).toBe(false);
  expect(
    compareSearchParams(
      new URLSearchParams('?foo=bar&bar=1'),
      new URLSearchParams('?bar=1&foo=bar'),
    ),
  ).toBe(true);
  expect(
    compareSearchParams(new URLSearchParams('?bar=1'), new URLSearchParams('?bar=1&foo=bar')),
  ).toBe(false);
});
