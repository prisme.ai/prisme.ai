'use client';

import { ReadonlyURLSearchParams, useSearchParams } from 'next/navigation';
import {
  createContext,
  useState,
  useEffect,
  Dispatch,
  SetStateAction,
  useContext,
  ReactNode,
  useRef,
} from 'react';
import { compareSearchParams } from './compareSearchParams';

export interface QueryStringProviderContext {
  setQueryString: Dispatch<SetStateAction<URLSearchParams>>;
  queryString: URLSearchParams;
}

export const queryStringContext = createContext<QueryStringProviderContext | undefined>(undefined);

export const useQueryString = () => {
  const context = useContext(queryStringContext);
  if (!context) {
    throw new Error();
  }
  return context;
};

const initialQueryString = new URLSearchParams(
  typeof window !== 'undefined' ? window.location.search : '',
);

export const QueryStringProvider = ({ children }: { children: ReactNode }) => {
  const [queryString, setQueryString] = useState(initialQueryString);
  const searchParams = useSearchParams();

  useEffect(() => {
    setQueryString(new URLSearchParams(searchParams || new ReadonlyURLSearchParams()));
  }, [searchParams]);

  useEffect(() => {
    const newQueryString = new URLSearchParams(window.location.search);
    if (initialQueryString.toString() === newQueryString.toString()) return;
    setQueryString(newQueryString);
  }, []);

  const searchParamsRef = useRef(searchParams || new ReadonlyURLSearchParams());
  useEffect(() => {
    searchParamsRef.current = searchParams || new ReadonlyURLSearchParams();
  }, [searchParams]);
  useEffect(() => {
    // Clean empty fields
    const qs = new URLSearchParams(
      Object.fromEntries(Array.from(queryString.entries()).filter(([k, v]) => k && v)),
    );
    const q = qs.toString();
    if (compareSearchParams(qs, searchParamsRef.current)) return;
    history.pushState(null, '', `${window.location.pathname}${q ? `?${q}` : ''}`);
  }, [queryString]);

  return (
    <queryStringContext.Provider
      value={{
        queryString,
        setQueryString,
      }}
    >
      {children}
    </queryStringContext.Provider>
  );
};

export default QueryStringProvider;
