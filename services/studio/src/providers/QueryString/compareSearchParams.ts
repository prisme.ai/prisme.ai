export function compareSearchParams(a: URLSearchParams, b: URLSearchParams) {
  function compareSearchParams(a: URLSearchParams, b: URLSearchParams) {
    return Array.from(a.entries()).reduce((prev, [k, v]) => prev && b.get(k) === v, true);
  }
  return compareSearchParams(a, b) && compareSearchParams(b, a);
}
