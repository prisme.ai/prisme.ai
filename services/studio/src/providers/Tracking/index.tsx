'use client';

import { createContext, ReactNode, useCallback, useContext, useEffect } from 'react';
import { usePathname } from 'next/navigation';
import useApi from '@/utils/api/useApi';
import { useEnv } from '../Env';

type TrackEventParameters = {
  category: string;
  action: string;
  name: string;
  value?: string | object;
  dimensions?: Record<string, string>;
};

declare global {
  interface Window {
    _paq: any;
  }
}

function trackPageView() {
  if (!window._paq) return;
  window._paq.push([
    'setCustomUrl',
    `/${window.location.pathname}${
      window.location.hash ? `/${window.location.hash.substring(1)}` : ''
    }`.replace(/\/\//g, '/'),
  ]);
  window._paq.push(['setDocumentTitle', document.title]);
  window._paq.push(['trackPageView']);
  window._paq.push(['MediaAnalytics::scanForMedia']);
  window._paq.push(['enableLinkTracking']);
}

interface TrackingContext {
  trackPageView: typeof trackPageView;
  trackEvent: (p: Partial<TrackEventParameters>) => void;
}
const trackingContext = createContext<TrackingContext | undefined>(undefined);
export const useTracking = () => {
  const context = useContext(trackingContext);
  if (!context) {
    throw new Error();
  }
  return context;
};

interface TrackingCategoryProps {
  category: string;
  children: ReactNode;
}

const useTrackEvent = () => {
  const api = useApi();
  const { TRACKING_WEBHOOK } = useEnv<{
    TRACKING_WEBHOOK: string;
  }>();
  return useCallback(
    async function ({ category, action, name, value, dimensions }: TrackEventParameters) {
      const push = window._paq ? (...args: any) => window._paq.push(...args) : console.log;
      push([
        'trackEvent',
        category,
        action,
        name,
        typeof value === 'object' ? null : value,
        dimensions,
      ]);
      if (TRACKING_WEBHOOK) {
        try {
          await api.prepareRequest(TRACKING_WEBHOOK, {
            method: 'POST',
            headers: {
              'content-type': 'application/json',
            },
            body: JSON.stringify({
              event: {
                category,
                action,
                name,
                value,
                dimensions,
              },
            }),
          });
        } catch {}
      }
    },
    [api, TRACKING_WEBHOOK],
  );
};

export const TrackingCategory = ({ category = '', children }: TrackingCategoryProps) => {
  const trackEvent = useTrackEvent();
  const trackEventHandler = useCallback<TrackingContext['trackEvent']>(
    ({ name = '', action = '', category: c = category }) => {
      trackEvent({ category: c, name, action });
    },
    [category, trackEvent],
  );
  return (
    <trackingContext.Provider value={{ trackPageView, trackEvent: trackEventHandler }}>
      {children}
    </trackingContext.Provider>
  );
};

export const Tracking = ({ children }: { children: ReactNode }) => {
  const { TRACKING } = useEnv<{
    TRACKING: { url: string; siteId: string };
  }>();
  const { url = '', siteId = '' } = TRACKING || {};
  const trackEvent = useTrackEvent();
  const pathname = usePathname();
  useEffect(() => {
    if (!url || window._paq) return;
    const paq = (window._paq = window._paq || []);
    paq.push(['trackPageView']);
    paq.push(['enableLinkTracking']);
    const u = url;
    paq.push(['setTrackerUrl', u + 'matomo.php']);
    paq.push(['setSiteId', siteId]);
    const s = document.createElement('script');
    s.async = true;
    s.src = '//cdn.matomo.cloud/prismeai.matomo.cloud/matomo.js';
    document.body.appendChild(s);
  }, [siteId, url]);

  useEffect(() => {
    trackPageView();
  }, [pathname]);

  const trackEventHandler = useCallback(
    (p: Partial<Parameters<typeof trackEvent>[0]>) => {
      trackEvent(p as Parameters<typeof trackEvent>[0]);
    },
    [trackEvent],
  );

  return (
    <trackingContext.Provider value={{ trackPageView, trackEvent: trackEventHandler }}>
      {children}
    </trackingContext.Provider>
  );
};

export default Tracking;
