import { createContext, ReactNode, useEffect, useCallback, useState, useContext } from 'react';
import Loading from '@/components/Loading';
import { notFound } from 'next/navigation';
import useApi from '@/utils/api/useApi';
import { useWorkspace } from '../Workspace';

export interface AutomationContext {
  automation: Prismeai.Automation;
  loading: boolean;
  fetchAutomation: () => Promise<Prismeai.Automation | null>;
  saveAutomation: (automation: Prismeai.Automation) => Promise<Prismeai.Automation | null>;
  saving: boolean;
  deleteAutomation: () => Promise<Prismeai.Automation | null>;
}

export const automationContext = createContext<AutomationContext | undefined>(undefined);

export const useAutomation = () => {
  const context = useContext(automationContext);
  if (!context) {
    throw new Error();
  }
  return context;
};

interface AutomationProviderProps {
  workspaceId: string;
  automationSlug: string;
  children: ReactNode;
}

export const AutomationProvider = ({
  workspaceId,
  automationSlug,
  children,
}: AutomationProviderProps) => {
  const api = useApi();
  const [automation, setAutomation] = useState<AutomationContext['automation']>();
  const [slug, setSlug] = useState(automationSlug);
  const [loading, setLoading] = useState<AutomationContext['loading']>(true);
  const [saving, setSaving] = useState<AutomationContext['saving']>(false);
  const [isNotFound, setNotFound] = useState(false);
  const { fetchWorkspace } = useWorkspace();

  const fetchAutomation = useCallback(async () => {
    if (!workspaceId || !automationSlug) return null;
    try {
      setNotFound(false);
      const automation = await api.getAutomation(workspaceId, automationSlug);
      setAutomation(automation);
      return automation || null;
    } catch (e) {
      setNotFound(true);
      return null;
    }
  }, [workspaceId, automationSlug, api]);

  const saveAutomation: AutomationContext['saveAutomation'] = useCallback(
    async (newAutomation) => {
      if (!workspaceId || !automation) return null;
      setSaving(true);
      try {
        const saved = await api.updateAutomation(workspaceId, slug, newAutomation);
        if (saved.slug !== slug) {
          setSlug(saved.slug);
        }
        setAutomation(saved);
        setSaving(false);
        // update workspace with lst events suggestions
        fetchWorkspace();
        return saved;
      } catch (e) {
        setSaving(false);
        throw e;
      }
    },
    [automation, slug, workspaceId, api, fetchWorkspace],
  );

  const deleteAutomation: AutomationContext['deleteAutomation'] = useCallback(async () => {
    if (!workspaceId || !automation) return null;
    setAutomation(undefined);
    api.deleteAutomation(workspaceId, slug);
    return automation;
  }, [automation, slug, workspaceId, api]);

  useEffect(() => {
    const initAutomation = async () => {
      setSlug(automationSlug);
      setLoading(true);
      await fetchAutomation();
      setLoading(false);
    };
    initAutomation();
  }, [automationSlug, fetchAutomation]);

  if (loading) return <Loading />;
  if (isNotFound) return notFound();
  if (!automation) return null;

  return (
    <automationContext.Provider
      value={{
        automation,
        loading,
        fetchAutomation,
        saveAutomation,
        saving,
        deleteAutomation,
      }}
    >
      {children}
    </automationContext.Provider>
  );
};

export default AutomationProvider;
