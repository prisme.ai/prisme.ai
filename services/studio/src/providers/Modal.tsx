import { Modal } from 'antd';
import { HookAPI } from 'antd/es/modal/useModal';
import { createContext, ReactNode, useContext } from 'react';

const modalContext = createContext<HookAPI | undefined>(undefined);
export const useModal = () => {
  const context = useContext(modalContext);
  if (!context) {
    throw new Error();
  }
  return context;
};

interface ModalProps {
  children: ReactNode;
}

export default function ModalProvider({ children }: ModalProps) {
  const [api, context] = Modal.useModal();
  return (
    <modalContext.Provider value={api}>
      {context}
      {children}
    </modalContext.Provider>
  );
}
