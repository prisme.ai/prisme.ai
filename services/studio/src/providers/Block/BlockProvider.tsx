import { createContext, ReactNode, useEffect, useCallback, useState, useContext } from 'react';
import { useWorkspace } from '../Workspace';
import Loading from '@/components/Loading';
import useApi from '@/utils/api/useApi';
import { notFound } from 'next/navigation';

export type Block = Prismeai.Block & { slug: string };
export interface BlockContext {
  block: Block;
  loading: boolean;
  fetchBlock: () => Promise<Block | null>;
  saveBlock: (block: Block) => Promise<Block | null>;
  saving: boolean;
  deleteBlock: () => Promise<Block | null>;
}

export const blockContext = createContext<BlockContext | undefined>(undefined);

export const useBlock = () => {
  const context = useContext(blockContext);
  if (!context) {
    throw new Error();
  }
  return context;
};

interface BlockProviderProps {
  workspaceId?: string;
  slug?: string;
  children: ReactNode;
}

export const BlockProvider = ({ workspaceId, slug, children }: BlockProviderProps) => {
  const api = useApi();
  const [block, setBlock] = useState<BlockContext['block']>();
  const [loading, setLoading] = useState<BlockContext['loading']>(true);
  const [saving, setSaving] = useState<BlockContext['saving']>(false);
  const [isNotFound, setNotFound] = useState(false);
  const { saveWorkspace } = useWorkspace();

  const fetchBlock = useCallback(async () => {
    setNotFound(false);
    if (!workspaceId || !slug) return null;
    try {
      const { blocks: { [slug]: block } = {} } = await api.getWorkspace(workspaceId);
      if (!block) {
        throw new Error('not found');
      }
      return { slug, ...block };
    } catch (e) {
      setNotFound(true);
      return null;
    }
  }, [slug, workspaceId, api]);

  const saveBlock: BlockContext['saveBlock'] = useCallback(
    async ({ slug: newSlug, ...block }) => {
      if (!workspaceId || !slug) return null;
      setSaving(true);
      try {
        const { blocks: lastBlocks = {} } = await api.getWorkspace(workspaceId);
        lastBlocks[newSlug] = block;
        if (slug !== newSlug) {
          delete lastBlocks[slug];
        }

        const { blocks = {} } =
          (await saveWorkspace({
            id: workspaceId,
            blocks: lastBlocks,
          })) || {};

        const newBlock = { slug: newSlug, ...blocks[newSlug] };
        setBlock(newBlock);
        setSaving(false);
        return newBlock;
      } catch (e) {
        setSaving(false);
        throw e;
      }
    },
    [saveWorkspace, slug, workspaceId, api],
  );

  const deleteBlock: BlockContext['deleteBlock'] = useCallback(async () => {
    if (!workspaceId || !slug) return null;
    const { blocks: { [slug]: currentBlock, ...lastBlocks } = {} } =
      await api.getWorkspace(workspaceId);
    await saveWorkspace({
      id: workspaceId,
      blocks: lastBlocks,
    });
    return { ...currentBlock, slug };
  }, [saveWorkspace, slug, workspaceId, api]);

  useEffect(() => {
    const initPage = async () => {
      setLoading(true);
      const block = await fetchBlock();
      setLoading(false);
      if (!block) return;
      setBlock(block);
    };
    initPage();
  }, [fetchBlock, slug]);

  if (loading) return <Loading />;
  if (isNotFound) return notFound();

  if (!block || !slug) return null;

  return (
    <blockContext.Provider
      value={{
        block,
        loading,
        fetchBlock,
        saveBlock,
        saving,
        deleteBlock,
      }}
    >
      {children}
    </blockContext.Provider>
  );
};

export default BlockProvider;
