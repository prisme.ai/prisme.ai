'use client';

import { ReactNode, useCallback } from 'react';
import Link from 'next/link';
import { useExternals } from './externals';
import DownIcon from '@/svgs/down.svg';
import { usePageEndpoint } from '@/utils/urls';
import { useColorSchemeManager } from '@/providers/ColorSchemeManager';
import Loading from '@/components/Loading';
import SchemaForm from '@/components/SchemaForm';
import BlocksProviderOriginal, {
  BlocksProviderProps as BlocksProviderOriginalProps,
} from '@/blocks/Provider/BlocksProvider';
import BlockLoader from '@/components/PageContent/BlockLoader';
import { useUser } from '../User';

export const BlocksProvider = ({ children }: { children: ReactNode }) => {
  const pageHost = usePageEndpoint();
  const { scheme } = useColorSchemeManager();
  const externals = useExternals();

  return (
    <BlocksProviderOriginal
      externals={externals}
      components={{ Link, Loading, DownIcon, SchemaForm }}
      utils={{
        getWorkspaceHost() {
          return pageHost;
        },
        colorScheme: scheme,
        BlockLoader,
      }}
    >
      {children}
    </BlocksProviderOriginal>
  );
};

interface PublicBlocksProviderProps {
  children: ReactNode;
  components?: Partial<BlocksProviderOriginalProps['components']>;
  utils?: Partial<BlocksProviderOriginalProps['utils']>;
}
export const PublicBlocksProvider = ({
  children,
  components,
  utils,
}: PublicBlocksProviderProps) => {
  const externals = useExternals();
  const { user, initAuthentication } = useUser();
  const getSigninUrl = useCallback(
    async ({ redirect = '' } = {}) => {
      if (!user || user.authData?.anonymous) {
        return initAuthentication({ redirect });
      }

      return redirect;
    },
    [user, initAuthentication],
  );

  const getSignupUrl = useCallback(async ({ redirect = '' } = {}) => {
    return `/signup${redirect ? `?redirect=${redirect}` : ''}`;
  }, []);
  return (
    <BlocksProviderOriginal
      externals={externals}
      components={{ Link, Loading, DownIcon, SchemaForm, ...components }}
      utils={{
        BlockLoader,
        auth: {
          getSigninUrl,
          getSignupUrl,
        },
        ...utils,
      }}
    >
      {children}
    </BlocksProviderOriginal>
  );
};
