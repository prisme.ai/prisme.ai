import React from 'react';
import ReactDom from 'react-dom';
import * as prismeaiSDK from '@prisme.ai/sdk';
import * as antd from 'antd';
import { BaseBlock } from '@/blocks/blocks/BaseBlock';
import { useBlocks } from '@/blocks/Provider/blocksContext';
import BlocksList from '@/blocks/blocks/BlocksList';
import { builtinBlocks } from '@/blocks';
import { useBlock } from '@/blocks/Provider';
import useLocalizedText from '@/utils/useLocalizedText';
import SchemaForm from '@/components/SchemaForm';
import LayoutSelection from '@/components/legacy/LayoutSelection';
import useApi from '@/utils/api/useApi';
import { useSchemaFormConfig } from '@/components/SchemaForm/SchemaFormConfigContext';
import LegacySelect from './LegacySelect';
import StretchContent from '@/components/StretchContent';
import Loading from '@/components/Loading';

const useBlocksWithUploadFile = () => {
  const context = useBlocks();
  const { utils, components } = useSchemaFormConfig();
  return {
    ...context,
    utils: {
      ...context.utils,
      ...utils,
    },
    components: {
      ...context.components,
      ...components,
    },
  };
};

const prismeaiBlocks = {
  useBlock,
  BaseBlock,
  useBlocks: useBlocksWithUploadFile,
  BlocksList,
  builtinBlocks,
};
const prismeaiDS = {
  ...antd,
  useLocalizedText,
  SchemaForm,
  LayoutSelection,
  Select: LegacySelect,
  StretchContent,
  Loading,
};

const externals = {
  React: { ...React, default: React },
  ReactDom: { ...ReactDom, default: ReactDom },
  prismeaiBlocks,
  prismeaiDS,
  antd,
};

declare global {
  interface Window {
    __external: typeof externals;
  }
}

if (typeof window !== 'undefined') {
  window.__external = externals;
}

export const useExternals = () => {
  const api = useApi();

  return {
    ...externals,
    prismeaiSDK: {
      ...prismeaiSDK,
      default: api,
    },
  };
};
