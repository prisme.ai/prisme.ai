'use client';

import { ReactNode } from 'react';
import { envContext } from './context';

interface EnvProviderProps {
  children: ReactNode;
  env: Record<string, unknown>;
}

export default function EnvProvider({ env, children }: EnvProviderProps) {
  return <envContext.Provider value={env}>{children}</envContext.Provider>;
}
