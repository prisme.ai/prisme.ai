export default {
  API_URL: process.env.API_URL || '',
  CONSOLE_URL: process.env.CONSOLE_URL || '',
  INTERNAL_API_URL: process.env.INTERNAL_API_URL || '', // Optional. Useful to ensure prismeai-pages makes server-side calls using this url
  PAGES_HOST: process.env.PAGES_HOST || '',
  SENTRY_DSN: process.env.SENTRY_DSN || '',
  SUGGESTIONS_ENDPOINT: getWorkspaceOpsUrl('suggestions') || process.env.SUGGESTIONS_ENDPOINT || '',
  BILLING_HOME: process.env.BILLING_HOME || '',
  BILLING_USAGE: process.env.BILLING_USAGE || '',
  TRACKING: jsonParse(process.env.TRACKING),
  TRACKING_WEBHOOK: getWorkspaceOpsUrl('tracking') || process.env.TRACKING_WEBHOOK || '',
  PRODUCTS_ENDPOINT: getWorkspaceOpsUrl('products') || process.env.PRODUCTS_ENDPOINT || '',
  USER_SPACE_ENDPOINT: getWorkspaceOpsUrl('user-space'),
  OIDC_PROVIDER_URL:
    process.env.OIDC_PROVIDER_URL ||
    (process.env.API_URL && process.env.API_URL.replace('/v2', '')) ||
    'http://studio.local.prisme.ai:3001',
  OIDC_STUDIO_CLIENT_ID: process.env.OIDC_STUDIO_CLIENT_ID || 'local-client-id',
  OIDC_CLIENT_ID_HEADER: process.env.OIDC_CLIENT_ID_HEADER || 'x-prismeai-client-id',
  ENABLED_AUTH_PROVIDERS: jsonParse(
    process.env.ENABLED_AUTH_PROVIDERS,
    (process.env.ENABLED_AUTH_PROVIDERS || 'local').split(',').map((name) => ({ name })),
  ),
  TRANSLATIONS_OVERRIDE: getWorkspaceOpsUrl('translations') || process.env.TRANSLATIONS_OVERRIDE,

  // https://socket.io/docs/v3/client-initialization/#transports
  WEBSOCKETS_DEFAULT_TRANSPORTS: (
    process.env.WEBSOCKETS_DEFAULT_TRANSPORTS || 'polling,websocket'
  ).split(','),
  PRIVACY_POLICIES_LINK: process.env.PRIVACY_POLICIES_LINK,
  NEWS_ENDPOINT: getWorkspaceOpsUrl('news'),
  CUSTOMIZATION_ENDPOINT: getWorkspaceOpsUrl('customization'),
  DEBUG_PROD: booleanParse(process.env.DEBUG_PROD),
  IS_WHITE_MARK: process.env.IS_WHITE_MARK === 'true',
};

function jsonParse(env?: string, dft = {}) {
  if (!env) return null;
  try {
    return JSON.parse(env) || {};
  } catch {
    return dft;
  }
}

function booleanParse(env?: string) {
  if (!env) return false;
  return ['true', 'y', '1'].includes((env || '').toLowerCase());
}

function getWorkspaceOpsUrl(slug: string) {
  return process.env.WORKSPACE_OPS_MANAGER ? `${process.env.WORKSPACE_OPS_MANAGER}/${slug}` : '';
}
