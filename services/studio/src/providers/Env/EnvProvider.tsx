import { ReactNode } from 'react';
import EnvProvider from './EnvProviderClient';
import env from './env';

interface EnvProviderServerProps {
  children: ReactNode;
}

export default function EnvProviderServer({ children }: EnvProviderServerProps) {
  return <EnvProvider env={env}>{children}</EnvProvider>;
}
