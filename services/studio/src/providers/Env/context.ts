'use client';

import { createContext, useContext } from 'react';

type EnvContext = Record<string, unknown>;

export const envContext = createContext<EnvContext | undefined>(undefined);

export const useEnv = <T = EnvContext>() => {
  const context = useContext(envContext);
  if (!context) {
    throw new Error();
  }
  return context as T;
};
