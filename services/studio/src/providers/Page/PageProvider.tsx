import { createContext, ReactNode, useEffect, useCallback, useState, useContext } from 'react';
import { notFound } from 'next/navigation';
import Loading from '@/components/Loading';
import useApi from '@/utils/api/useApi';
import { useWorkspace } from '../Workspace';

interface Page extends Prismeai.Page {
  apiKey?: string;
}

export interface PageContext {
  page: Page;
  appInstances: Prismeai.PageDetails['appInstances'];
  loading: boolean;
  fetchPage: () => Promise<Page | null>;
  savePage: (page: Page) => Promise<Page | null>;
  saving: boolean;
  deletePage: () => Promise<Page | null>;
}

export const pageContext = createContext<PageContext | undefined>(undefined);

export const usePage = () => {
  const context = useContext(pageContext);
  if (!context) {
    throw new Error();
  }
  return context;
};
export const usePageRaw = () => useContext(pageContext);

interface PageProviderProps {
  workspaceId?: string;
  slug?: string;
  children: ReactNode;
}

export const PageProvider = ({ workspaceId, slug, children }: PageProviderProps) => {
  const api = useApi();
  const [page, setPage] = useState<PageContext['page']>();
  const [appInstances, setAppInstances] = useState<PageContext['appInstances']>([]);
  const [loading, setLoading] = useState<PageContext['loading']>(true);
  const [saving, setSaving] = useState<PageContext['saving']>(false);
  const [isNotFound, setNotFound] = useState(false);
  const { fetchWorkspace } = useWorkspace();

  const fetchPage = useCallback(async () => {
    setNotFound(false);
    if (!workspaceId || !slug) return null;
    try {
      const {
        appInstances,
        public: isPublic,
        favicon,
        ...page
      } = await api.getPage(workspaceId, slug);
      void isPublic;
      void favicon;
      setAppInstances(appInstances);
      return page || null;
    } catch (e) {
      setNotFound(true);
      return null;
    }
  }, [slug, workspaceId, api]);

  const savePage: PageContext['savePage'] = useCallback(
    async ({ apiKey, ...newPage }) => {
      void apiKey;
      if (!workspaceId) return null;
      setSaving(true);
      try {
        const updated = await api.updatePage(workspaceId, newPage, page && page.slug);
        setPage(updated);
        setSaving(false);
        // update workspace with lst events suggestions
        fetchWorkspace();
        return updated;
      } catch (e) {
        setSaving(false);
        throw e;
      }
    },
    [page, workspaceId, api, fetchWorkspace],
  );

  const deletePage: PageContext['deletePage'] = useCallback(async () => {
    if (!workspaceId || !page?.slug) return null;
    setPage(undefined);
    api.deletePage(workspaceId, page.slug);
    return page;
  }, [page, workspaceId, api]);

  useEffect(() => {
    const initPage = async () => {
      setLoading(true);
      const page = await fetchPage();
      setLoading(false);
      if (!page) return;
      setPage(page);
    };
    initPage();
  }, [fetchPage, slug]);

  if (loading) return <Loading />;
  if (isNotFound) return notFound();
  if (!page) return null;

  return (
    <pageContext.Provider
      value={{
        page,
        appInstances,
        loading,
        fetchPage,
        savePage,
        saving,
        deletePage,
      }}
    >
      {children}
    </pageContext.Provider>
  );
};

export default PageProvider;
