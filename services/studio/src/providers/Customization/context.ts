'use client';

import { createContext, useContext } from 'react';
import { CustomizationsValue } from './getCustomization';

export const customizationContext = createContext<CustomizationsValue | undefined>(undefined);

export const useCustomization = () => {
  const context = useContext(customizationContext);
  if (!context) {
    throw new Error();
  }
  return context;
};
