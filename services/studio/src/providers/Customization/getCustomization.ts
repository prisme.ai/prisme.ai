import env from '@/providers/Env/env';

const { CUSTOMIZATION_ENDPOINT = '' } = env;

export type CustomizationsValue = {
  styles?: string;
  favicon?: string;
  mainLogo?: string;
  mainTitle?: string;
  links?: {
    privacy: string;
    help: string;
    feedback: string;
    changelog: string;
  };
};
let customizationPromise: Promise<CustomizationsValue | null> | null;

export async function getCustomization() {
  if (!customizationPromise) {
    setTimeout(() => {
      customizationPromise = null;
    }, 1000 * 60);
    customizationPromise = new Promise(async (resolve) => {
      try {
        const res = await fetch(CUSTOMIZATION_ENDPOINT);
        if (!res.ok) {
          throw new Error();
        }
        const {
          styles = null,
          favicon = null,
          mainLogo = null,
          mainTitle = null,
          links = null,
        } = await res.json();
        resolve({ styles, favicon, mainLogo, mainTitle, links });
      } catch (e) {
        resolve(null);
      }
    });
  }

  return customizationPromise;
}

export default getCustomization;
