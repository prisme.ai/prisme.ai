'use client';

import { ReactNode, useEffect, useRef } from 'react';
import { CustomizationsValue } from './getCustomization';
import { customizationContext } from './context';
import Color from 'color';
import { useColorSchemeManager } from '../ColorSchemeManager';

interface CustomizationProviderProps extends CustomizationsValue {
  children: ReactNode;
}
export const CustomizationProvider = ({ children, ...props }: CustomizationProviderProps) => {
  const { scheme } = useColorSchemeManager();
  const stylesRef = useRef<HTMLStyleElement | null>(
    typeof document !== 'undefined' ? document.createElement('style') : null,
  );
  useEffect(() => {
    if (!props.styles || !stylesRef.current) return;
    stylesRef.current.innerHTML = props.styles;
    document.getElementsByTagName('head')[0].appendChild(stylesRef.current);
  }, [props.styles]);

  useEffect(() => {
    const root = getComputedStyle(document.documentElement);
    ['--pr-accent-color', '--pr-main-text'].forEach((cssVar) => {
      const cssColor = root.getPropertyValue(cssVar).trim();
      document.documentElement.style.setProperty(
        `${cssVar}-light`,
        Color(cssColor).lightness(85).hexa(),
      );
      document.documentElement.style.setProperty(
        `${cssVar}-very-light`,
        Color(cssColor).lightness(95).hexa(),
      );
      document.documentElement.style.setProperty(
        `${cssVar}-dark`,
        Color(cssColor).darken(0.7).hexa(),
      );
      document.documentElement.style.setProperty(
        `${cssVar}-very-dark`,
        Color(cssColor).darken(0.8).hexa(),
      );
    });
  }, [props.styles, scheme]);

  return <customizationContext.Provider value={props}>{children}</customizationContext.Provider>;
};
