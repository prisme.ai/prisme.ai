import { Schemes } from '@/providers/ColorSchemeManager';
import { Events } from '@prisme.ai/sdk';

declare global {
  interface Window {
    Prisme: {
      ai: {
        api: typeof api;
        events?: Events;
        debug: {
          events: {
            state: boolean;
            toggle: () => void;
          };
          blocks: {
            state: boolean;
            setBlockUrl: (name: string, url: string) => void;
            reset: () => void;
          };
        };
        colorScheme?: {
          get: () => string;
          set: (scheme: Schemes) => void;
          toggle: () => void;
        };
      };
    };
  }
}
