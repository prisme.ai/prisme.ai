FROM node:20 as build
WORKDIR /www

COPY ./package.json ./package-lock.json ./rollup.config.js ./dtsgen.json /www/
COPY ./scripts/buildInstructions.ts /www/scripts/
COPY ./packages /www/packages
COPY ./services/studio/package*  /www/services/studio/

RUN npm ci

COPY ./services/studio/ /www/services/studio
RUN rm -rf /www/services/studio/.next && \
    rm -rf /www/services/studio/.eslintrc.json

COPY ./specifications /www/specifications
ENV NEXT_TELEMETRY_DISABLED 1

RUN npm run build:packages -- --packages=sdk,validation,types

# Use the SERVICE environment variable to determine the build process
ARG SERVICE=studio
RUN if [ "$SERVICE" = "console" ] || [ "$SERVICE" = "studio" ]; then \
      npm run build:studio; \
    elif [ "$SERVICE" = "pages" ]; then \
      npm run build:pages; \
    else \
      echo "Invalid SERVICE value: $SERVICE" && exit 1; \
    fi

RUN npm prune --production

FROM node:20-alpine as runtime

WORKDIR /www
COPY --from=build /www/packages /www/packages
COPY --from=build /www/services/studio/  /www/services/studio/
COPY --from=build /www/package.json /www/package-lock.json /www/
COPY --from=build /www/specifications/  /www/specifications/
COPY --from=build /www/node_modules/  /www/node_modules/

EXPOSE 3000
ENV PORT 3000
ENV NODE_ENV production
ENV NPM_CONFIG_OFFLINE true
ENV NPM_CONFIG_LOGS_MAX 0

# Next.js collects completely anonymous telemetry data about general usage.
# Learn more here: https://nextjs.org/telemetry
# Uncomment the following line in case you want to disable telemetry.
ENV NEXT_TELEMETRY_DISABLED 1

ARG SERVICE=studio
ENV SERVICE=${SERVICE}

RUN chown -R node:node "/www/services/studio/.next_${SERVICE}" && \
    chmod g+w "/www/services/studio/.next_${SERVICE}" && \
    find "/www/services/studio/.next_${SERVICE}/" -name "404.html" -exec chmod g+w {} \;

CMD ["sh", "-c", "npm --prefix services/studio run start:${SERVICE}"]
