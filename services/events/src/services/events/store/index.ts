export * from './types';

import fs from 'fs';
import {
  CleanupDriverTasks,
  StoreDriverOptions,
  StoreDriverType,
} from './types';
import { ElasticsearchStore } from './ElasticsearchStore';
import { ConfigurationError } from '../../../errors';
import { Opensearchstore } from './OpenSearchStore';
import { CLEANUP_TASKS_FILEPATH } from '../../../../config';
import { logger } from '../../../logger';

function loadCleanupTasks(): CleanupDriverTasks {
  try {
    const raw = fs.readFileSync(CLEANUP_TASKS_FILEPATH).toString();
    const config = JSON.parse(
      raw.replace(/\$\{([\w]+)(?::([\w\d]+))?\}/gi, (match, p1, p2) => {
        return process.env[p1] || p2 || match;
      })
    );
    return config as CleanupDriverTasks;
  } catch (err) {
    logger.error({
      msg: `Failed loading cleanup tasks from ${CLEANUP_TASKS_FILEPATH}. /cleanup/:task API won't run anything.`,
      err,
    });
  }
  return {};
}

export function buildEventsStore(opts: StoreDriverOptions) {
  const cleanupTasks = loadCleanupTasks();

  switch (opts.driver) {
    case StoreDriverType.Elasticsearch: {
      const driver = new ElasticsearchStore(
        opts,
        cleanupTasks.elasticsearch || {}
      );
      return driver;
    }
    case StoreDriverType.Opensearch: {
      const driver = new Opensearchstore(opts, {
        ...cleanupTasks.elasticsearch,
        ...cleanupTasks.opensearch,
      });
      return driver;
    }
    default:
      throw new ConfigurationError(`Invalid Users storage "${opts.driver}"`, {
        storage: opts.driver,
      });
  }
}
