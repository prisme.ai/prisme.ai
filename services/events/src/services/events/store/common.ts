import { EVENTS_AGGREGATION_PAYLOAD_FIELD } from '../../../../config';
import { ElasticBucket } from './types';

export const EVENTS_MAPPING = {
  properties: {
    createdAt: {
      type: 'date',
    },
    id: {
      type: 'keyword',
    },
    type: {
      type: 'keyword',
    },
    'source.correlationId': {
      type: 'keyword',
    },
    'source.userId': {
      type: 'keyword',
    },
    'source.sessionId': {
      type: 'keyword',
    },
    'source.workspaceId': {
      type: 'keyword',
    },
    'source.appSlug': {
      type: 'keyword',
    },
    'source.appInstanceFullSlug': {
      type: 'keyword',
    },
    'source.appInstanceSlug': {
      type: 'keyword',
    },
    'source.automationDepth': {
      type: 'short',
    },
    'source.automationSlug': {
      type: 'keyword',
    },
    'source.topic': {
      type: 'keyword',
    },
    'target.userTopic': {
      type: 'keyword',
    },
    'source.serviceTopic': {
      type: 'keyword',
    },
    payload: {
      type: 'flattened',
      ignore_above: 32700,
    },
    [EVENTS_AGGREGATION_PAYLOAD_FIELD]: {
      type: 'object',
      // Without this, any workspace with aggPayload: true emits but no mapping defined would get all of their payload fields dynamically mapped, eventually leading to mapping explosion
      dynamic: false,
    },
    'error.error': {
      type: 'keyword',
    },
    'error.message': {
      type: 'text',
    },
  },
};

export function mergeArrays(firstArray: any[] = [], secondArray: any[] = []) {
  const mergedMap = new Map();

  for (const item of firstArray) {
    mergedMap.set(item.date, { ...item });
  }

  for (const item of secondArray) {
    mergedMap.set(item.date, { ...mergedMap.get(item.date), ...item });
  }

  const mergedArray = [...mergedMap.values()];

  mergedArray.sort((a, b) => a.date.localeCompare(b.date));

  return mergedArray;
}

export function mapElasticBuckets(
  buckets: ElasticBucket[]
): Record<string, { count: number; buckets: any }> {
  return buckets.reduce(
    (prev: any, { key, doc_count, ...buckets }: any) => ({
      ...prev,
      [key]: {
        count: doc_count,
        buckets,
      },
    }),
    {}
  );
}

export type ElasticMapping = Record<
  string,
  {
    type: string;
  }
>;
function flattenSchemaForm(
  schema: Prismeai.WorkspaceEventConfiguration['schema'],
  mapping: ElasticMapping = {},
  baseKey: string[] = []
): ElasticMapping {
  return Object.entries(schema || {}).reduce(
    (mapping, [field, { type, format, properties }]) => {
      const completeField = baseKey.concat([field]);
      const flattedFieldname = completeField.join('.');
      if (type == 'number' || (type as any) == 'integer') {
        if (format === 'int32') {
          return { ...mapping, [flattedFieldname]: { type: 'integer' } };
        } else if (format === 'int64') {
          return { ...mapping, [flattedFieldname]: { type: 'long' } };
        } else if (format === 'float') {
          return { ...mapping, [flattedFieldname]: { type: 'float' } };
        } else if (format === 'double') {
          return { ...mapping, [flattedFieldname]: { type: 'double' } };
        } else {
          return { ...mapping, [flattedFieldname]: { type: 'long' } };
        }
      } else if (type === 'string') {
        if (format === 'keyword') {
          return { ...mapping, [flattedFieldname]: { type: 'keyword' } };
        } else {
          return { ...mapping, [flattedFieldname]: { type: 'text' } };
        }
      }
      if (type == 'object' && Object.keys(properties || {}).length) {
        return flattenSchemaForm(properties, mapping, completeField);
      }
      return mapping;
    },
    mapping
  );
}

export function mapEventsSchemaToESMapping(
  events: Prismeai.WorkspaceEventsConfiguration
): ElasticMapping {
  return Object.values(events?.types || {}).reduce((mapping, { schema }) => {
    return {
      ...mapping,
      ...flattenSchemaForm(schema),
    };
  }, {} as ElasticMapping);
}
