import { Broker } from '@prisme.ai/broker';
import { EventType } from '../../eda';
import { logger } from '../../logger';
import { EventsStore } from './store';

export function syncEventMappingWithWorkspaces(
  store: EventsStore,
  broker: Broker
) {
  broker.on<Prismeai.CreatedWorkspace['payload']>(
    [EventType.CreatedWorkspace],
    async function initMapping(event): Promise<boolean> {
      const workspaceId = event?.payload?.workspace?.id;
      const events = event?.payload?.workspace?.events || {};
      if (!workspaceId) {
        logger.error({ msg: 'Missing source workspaceId', event });
        return false;
      }
      if (!Object.keys(events).length) {
        return true;
      }
      await store.updateWorkspaceMapping(workspaceId, events || {});
      return true;
    }
  );

  broker.on<Prismeai.UpdatedWorkspaceEvents['payload']>(
    [EventType.UpdatedWorkspaceEvents],
    async function updateMapping(event): Promise<boolean> {
      if (!event?.source?.workspaceId) {
        logger.error({ msg: 'Missing source workspaceId', event });
        return false;
      }

      await store.updateWorkspaceMapping(
        event?.source?.workspaceId,
        event.payload?.events || {}
      );
      return true;
    }
  );
}
