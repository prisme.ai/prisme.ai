import { Broker, PrismeEvent } from '@prisme.ai/broker';
import { Cache, WorkspaceSubscriber } from '../../../cache';
import { EventType } from '../../../eda';
import { AccessManager, SubjectType, ActionType } from '../../../permissions';
import { getWorkspaceUser } from '../users';
import { Permissions, Rules } from '@prisme.ai/permissions';
import { logger } from '../../../logger';
import { Readable } from 'stream';
import { QueryEngine } from './filters/engine';
import {
  LocalSubscriber,
  SocketId,
  Subscriber,
  TargetTopic,
  WorkspaceId,
  WorkspaceSubscribers,
} from './types';
import { updateSubscriberUserTopics } from './updateSubscriberUserTopics';
import { ClusterEvents, ClusterNode } from '../../cluster';

export * from './types';

type TargetTopicSubscriptions = {
  workspaceIds: Record<WorkspaceId, Set<SocketId>>;
  socketIds: Set<SocketId>;
};

export class Subscriptions extends Readable {
  public broker: Broker;
  public accessManager: AccessManager;
  private subscribers: Record<WorkspaceId, WorkspaceSubscribers>;
  private targetTopics: Record<TargetTopic, TargetTopicSubscriptions>;
  private queries: Record<WorkspaceId, QueryEngine>;
  private cache: Cache;

  public cluster: ClusterNode;

  constructor(
    broker: Broker,
    accessManager: AccessManager,
    cache: Cache,
    cluster: ClusterNode
  ) {
    super({ objectMode: true });
    this.broker = broker;
    this.targetTopics = {};
    this.queries = {};
    this.subscribers = {};
    this.accessManager = accessManager;
    this.cache = cache;
    this.cluster = cluster;

    // cache workspaces in accessManager
    const uncachedFetch = this.accessManager.__unsecureGet.bind(
      this.accessManager
    );
    const cachedWorkspaces: Record<
      string,
      {
        updatedAt: number;
        data: any;
      }
    > = {};
    this.accessManager.__unsecureGet = async (subjectType: any, id: string) => {
      if (subjectType !== SubjectType.Workspace) {
        return uncachedFetch(subjectType, id);
      }

      // Keep workspaces in cache for 5s so we avoid heavy database traffic on peak usage while keeping this cache simple
      if (
        !cachedWorkspaces[id] ||
        Date.now() - cachedWorkspaces[id].updatedAt > 5000
      ) {
        cachedWorkspaces[id] = {
          updatedAt: Date.now(),
          data: await uncachedFetch(subjectType, id),
        };
      }

      return cachedWorkspaces[id].data;
    };
  }

  _read(): void {
    return;
  }

  metrics() {
    const workspaceSubscriptions = Object.entries(this.subscribers);
    const totalSubscribers = workspaceSubscriptions.reduce(
      (total, [, { socketIds }]) => total + Object.values(socketIds).length,
      0
    );
    return {
      workspacesNb: workspaceSubscriptions.length,
      totalSubscribers,
    };
  }

  private setSubscriber(subscriber: Subscriber) {
    const workspaceId = subscriber.workspaceId;
    if (!(workspaceId in this.subscribers)) {
      this.subscribers[workspaceId] = {
        socketIds: {},
        userIds: {},
      };
    }
    if (!(subscriber.userId in this.subscribers[workspaceId].userIds)) {
      this.subscribers[workspaceId].userIds[subscriber.userId] = new Set();
    }

    this.subscribers[workspaceId].socketIds[subscriber.socketId] = subscriber;
    this.subscribers[workspaceId].userIds[subscriber.userId].add(
      subscriber.socketId
    );

    if (!this.queries[workspaceId]) {
      this.queries[workspaceId] = new QueryEngine();
    }
    this.queries[workspaceId].saveQuery(
      subscriber.socketId,
      subscriber.filters
    );

    if (subscriber.targetTopic) {
      if (!this.targetTopics[subscriber.targetTopic]) {
        this.targetTopics[subscriber.targetTopic] = {
          workspaceIds: {},
          socketIds: new Set(),
        };
      }
      this.targetTopics[subscriber.targetTopic].socketIds.add(
        subscriber.socketId
      );
      if (
        !this.targetTopics[subscriber.targetTopic].workspaceIds?.[workspaceId]
      ) {
        this.targetTopics[subscriber.targetTopic].workspaceIds[workspaceId] =
          new Set();
      }
      this.targetTopics[subscriber.targetTopic].workspaceIds[workspaceId].add(
        subscriber.socketId
      );
    }
  }

  private unsetSubscriber(workspaceId: string, socketId: string) {
    if (!(workspaceId in this.subscribers)) {
      this.subscribers[workspaceId] = {
        socketIds: {},
        userIds: {},
      };
    }
    if (!this.queries[workspaceId]) {
      this.queries[workspaceId] = new QueryEngine();
    }

    // Remove associated query
    this.queries[workspaceId].removeQuery(socketId);

    // Remove subscriber refs from this.subscribers
    const subscriber = this.subscribers[workspaceId]?.socketIds?.[socketId];
    if (!subscriber) {
      return;
    }

    if (!(subscriber?.userId in this.subscribers[workspaceId].userIds)) {
      this.subscribers[workspaceId].userIds[subscriber.userId] = new Set();
    }

    if (subscriber) {
      delete this.subscribers[workspaceId].socketIds[socketId];
      this.subscribers[workspaceId].userIds[subscriber.userId].delete(socketId);

      if (this.targetTopics[subscriber.targetTopic]?.socketIds) {
        this.targetTopics[subscriber.targetTopic].socketIds.delete(socketId);
      }

      const targetTopicSockets =
        subscriber.targetTopic &&
        this.targetTopics[subscriber.targetTopic]?.workspaceIds?.[workspaceId];
      if (targetTopicSockets) {
        targetTopicSockets.delete(socketId);
      }
    }

    return subscriber;
  }

  async initClusterSynchronization() {
    await this.initSubscribersSynchronization();

    // When a node normally exits
    this.cluster.on(ClusterEvents.RemoteNodeLeft, (node) => {
      this.releaseAllSubscribersFromTopic(node.targetTopic);
    });

    // When an inactive node is detected, force cache cleanup as it might not have been done in case of a crash
    this.cluster.on(ClusterEvents.RemoteNodeInactive, (node) => {
      this.releaseAllSubscribersFromTopic(node.targetTopic, true);
    });
  }

  private async releaseAllSubscribersFromTopic(
    targetTopic: string,
    clearCache?: boolean
  ) {
    const removeSubscribers: {
      socketId: string;
      sessionId: string;
      workspaceId: string;
    }[] = [];
    const targetWorkspaces: Record<WorkspaceId, Set<SocketId>> = this
      .targetTopics[targetTopic]?.workspaceIds || {};

    for (let [workspaceId, socketIds] of Object.entries(targetWorkspaces)) {
      const currentSize = socketIds.size;
      socketIds.forEach((socketId) => {
        const subscriber = this.unsetSubscriber(workspaceId, socketId);
        if (subscriber) {
          removeSubscribers.push({
            socketId: subscriber.socketId,
            sessionId: subscriber.sessionId,
            workspaceId: subscriber.workspaceId,
          });
        }
      });

      const filteredSize =
        this.targetTopics?.[targetTopic]?.workspaceIds?.[workspaceId]?.size ||
        0;
      if (filteredSize < currentSize) {
        logger.info({
          msg: `Removed ${
            currentSize - filteredSize
          } subscribers from left node topic ${targetTopic}`,
        });
      }
    }

    delete this.targetTopics[targetTopic];

    // Also clear from cache
    if (clearCache && removeSubscribers.length) {
      await Promise.all(
        removeSubscribers.map((cur) =>
          this.cache.unregisterSubscriber(
            cur.workspaceId,
            cur.sessionId,
            cur.socketId
          )
        )
      );
      logger.info({
        msg: `Cleared ${removeSubscribers.length} subscribers from node topic ${targetTopic}`,
      });
    }
  }

  async close() {
    await this.releaseAllSubscribersFromTopic(this.cluster.localTopic, true);
  }

  private async initSubscribersSynchronization() {
    // Start subscribers cache synchronisation
    this.cluster.on(
      ClusterEvents.RemoteNodeSynchronized,
      ({ currentState, previousState, emitted_at }) => {
        const subscribers = currentState?.metadata?.subscribers;
        if (
          previousState?.metadata?.subscribers?.sockets &&
          subscribers?.sockets
        ) {
          const currentNodeSubscribers = new Set(subscribers.sockets);
          const leftSubscribers =
            previousState.metadata.subscribers.sockets.filter(
              (id) => !currentNodeSubscribers.has(id)
            );
          leftSubscribers.forEach((socket) => {
            const [workspaceId, socketId] = socket.split(':');
            const subscriber =
              this.subscribers[workspaceId]?.socketIds?.[socketId];
            logger.debug({
              msg: 'Removing subscriber',
              workspaceId: workspaceId,
              userId: subscriber?.userId,
              socketId: socketId,
            });
            if (subscriber) {
              this.unregisterSubscriber(subscriber);
            }
          });
        }

        if (subscribers?.joined?.length) {
          subscribers.joined.forEach(({ oldSocketId, ...joined }) => {
            const previousSubscriber =
              this.subscribers?.[joined.workspaceId]?.socketIds?.[
                joined?.socketId
              ];
            // This prevents initializing an incomplete subscriber from a partial update when the instance just started & has not pulled existing cache yet
            if (
              (!joined.filters || !joined.permissions?.length) &&
              !previousSubscriber
            ) {
              return;
            }

            this.saveSubscriber(
              {
                ...joined,
                targetTopic:
                  joined.targetTopic || previousSubscriber?.targetTopic,
                // Reuse previous filters in case of partial updates
                filters: joined.filters || previousSubscriber?.filters,
                permissions: joined.permissions?.length
                  ? Permissions.buildFrom(joined.permissions)
                  : previousSubscriber.permissions,
              },
              { oldSocketId }
            );
          });
          logger.debug({
            msg: `Pulled ${
              subscribers?.joined?.length
            } new or updated subscribers ${Date.now() - emitted_at}ms after ${
              currentState.targetTopic
            } sync event`,
          });
        }
      }
    );

    // Now that our event based synchronization is ready, pull all existing subscribers from cache
    let totalSubscribersRestored = 0;
    const cachedSubscribers = await this.cache.getAllWorkspaceSubscribers();
    for (let [workspaceId, wkSubscribers] of Object.entries(
      cachedSubscribers
    )) {
      const subscribers: Subscriber[] = wkSubscribers.map((cur) => {
        const subscriber: Subscriber = {
          ...cur,
          workspaceId,
          permissions: Permissions.buildFrom(cur.permissions),
        };

        return subscriber;
      });

      for (let subscriber of subscribers) {
        // Drop this subscriber if its hosting node is not up anymore
        if (!this.cluster.clusterTopics.has(subscriber.targetTopic)) {
          continue;
        }
        // Do not update any existing subscriber already synced from events
        if (this.saveSubscriber(subscriber, { disableUpdate: true })) {
          totalSubscribersRestored += 1;
        }
      }
    }
    logger.info({
      msg: `Retrieved ${totalSubscribersRestored} subscribers accross ${
        Object.keys(this.subscribers).length
      } workspaces from cache.`,
    });
  }

  async start(
    callback: (event: PrismeEvent, subscribers: Subscriber[]) => void
  ) {
    // Listen to events, find matching subscribers & pass to listener callback for websocket transmission
    this.on('data', async (event) => {
      const workspaceId = event?.source?.workspaceId;
      const queryEngine = this.queries[workspaceId];
      const workspaceSubscribers = this.subscribers[workspaceId] || {};
      if (!workspaceId || !queryEngine || !workspaceSubscribers) {
        return;
      }

      // If we do not have any subscriber corresponding to this event socketId, this means we are late on events.subscribers.* live synchronization & need to pull from cache
      if (
        event?.source?.socketId &&
        !workspaceSubscribers.socketIds?.[event?.source?.socketId]
      ) {
        const subscriberData = await this.cache.getWorkspaceSubscriber(
          event?.source?.workspaceId,
          event?.source?.sessionId,
          event?.source?.socketId
        );
        const stillNoMatch =
          !workspaceSubscribers.socketIds?.[event?.source?.socketId];
        logger.debug({
          msg: `No subscriber matching ${event?.source?.socketId}, pulled from cache.`,
          found: !!subscriberData,
          stillNoMatch,
          event: event?.type,
        });
        if (subscriberData && stillNoMatch) {
          const subscriber: Subscriber = {
            ...subscriberData,
            permissions: Permissions.buildFrom(subscriberData.permissions),
          };
          this.saveSubscriber(subscriber);
        }
      }

      const matchingSocketIds = queryEngine.matches(event);
      const matchingSubscribers: Subscriber[] = [];

      for (let socketId of matchingSocketIds) {
        const subscriber = workspaceSubscribers?.socketIds[socketId];
        if (!subscriber) {
          logger.error({
            msg: `Query engine returned a matching socketId '${socketId}' but no known subscriber matches this socketId !`,
            workspaceId,
          });
          continue;
        }

        const readable = subscriber.permissions.can(
          ActionType.Read,
          SubjectType.Event,
          event
        );
        if (readable) {
          matchingSubscribers.push(subscriber);
        }
      }

      // Finally send our event & matching subscribers
      callback(event, matchingSubscribers);
    });

    // Persist userTopics subscriptions in cache
    this.broker.on<
      Prismeai.CreatedUserTopic['payload'] | Prismeai.JoinedUserTopic['payload']
    >(
      [EventType.CreatedUserTopic, EventType.JoinedUserTopic],
      async (event) => {
        if (!event?.payload) {
          return true;
        }
        const workspaceId = event?.source?.workspaceId!;

        const userIds: string[] = (<any>event.payload).user?.id
          ? [(<any>event.payload).user?.id]
          : (<any>event.payload).userIds || [];
        const topicName = event.payload.topic;
        if (!topicName) {
          return true;
        }
        const workspaceSubscribers = this.subscribers[workspaceId];
        if (!workspaceSubscribers) {
          return true;
        }
        await Promise.all(
          userIds.map(async (userId) => {
            const result = await this.cache.joinUserTopic(
              workspaceId,
              userId,
              topicName
            );

            // Update permissions from corresponding subscribers & emit to the rest of the cluster
            const activeSubscribers =
              this.subscribers[workspaceId]?.userIds?.[userId] || new Set();
            activeSubscribers.forEach((socketId) => {
              const subscriber = workspaceSubscribers?.socketIds?.[socketId];
              if (subscriber) {
                updateSubscriberUserTopics(subscriber, topicName);
                // Only emit permissions update to the cluster in order to avoid concurrent updates conflicts which could loose latest filters or permissions (easily happens when both filters and permissions are updated at the same time by distinct nodes)
                this.registerSubscriber(
                  {
                    socketId: subscriber.socketId,
                    sessionId: subscriber.sessionId,
                    workspaceId: subscriber.workspaceId,
                    permissions: subscriber.permissions,
                    targetTopic: subscriber.targetTopic,
                  },
                  {
                    persistedSubscriber: subscriber, // However we still want the full subscriber persisted in cache
                  }
                );
              }
            });
            return result;
          })
        );

        return true;
      }
    );
  }

  async subscribe(
    workspaceId: string,
    subscriber: {
      workspaceId: string;
      userId: string;
      sessionId: string;
      socketId: string;
      apiKey?: string;
      authData: any;
      targetTopic: string;
    } & Pick<Subscriber, 'filters'>
  ): Promise<LocalSubscriber> {
    const workspaceUser = await getWorkspaceUser(
      workspaceId,
      {
        id: subscriber.userId,
        sessionId: subscriber.sessionId,
      },
      this.cache
    );
    logger.trace({
      msg: `Retrieved subscriber userTopics`,
      userId: subscriber.userId,
      sessionId: subscriber.sessionId,
      socketId: subscriber.socketId,
      workspaceId,
    });
    const userAccessManager = await this.accessManager.as(
      workspaceUser,
      subscriber.apiKey
    );

    await userAccessManager.pullRoleFromSubject(
      SubjectType.Workspace,
      workspaceId
    );
    logger.trace({
      msg: `Retrieved user's workspace permissions`,
      userId: subscriber.userId,
      sessionId: subscriber.sessionId,
      socketId: subscriber.socketId,
      workspaceId,
    });

    // Small performance improvement : reduce casl overhead by removing all others rules
    const userPermissions = (<any>userAccessManager)
      .permissions as Permissions<any>;
    const filteredRules = userPermissions.ability.rules.filter(
      (cur) =>
        cur.subject === SubjectType.Event ||
        (Array.isArray(cur.subject) && cur.subject.includes(SubjectType.Event))
    ) as Rules;
    userPermissions.updateRules(filteredRules);

    const fullSubscriber = {
      ...subscriber,
      ...workspaceUser,
      accessManager: userAccessManager,
      permissions: userPermissions,
      unsubscribe: () => {
        this.unregisterSubscriber(fullSubscriber, true);
      },
      local: true,
    };
    this.saveSubscriber(fullSubscriber);
    await this.registerSubscriber(fullSubscriber);

    return fullSubscriber;
  }

  private saveSubscriber(
    subscriber: Subscriber,
    opts?: { oldSocketId?: string; disableUpdate?: boolean }
  ) {
    const workspaceId = subscriber.workspaceId;
    if (!subscriber.filters) {
      subscriber.filters = {};
    }

    // First check if this subscriber already exists to keep it updated without pushing it twice
    // If we were given an oldSocketId, keep the corresponding subscriber in priority so we don't reinstantiate Subscriber instance currently used by local websockets
    const existingSubscriber =
      (opts?.oldSocketId &&
        this.subscribers[workspaceId]?.socketIds?.[opts.oldSocketId]) ||
      this.subscribers[workspaceId]?.socketIds?.[subscriber.socketId];

    const previousSocketId =
      opts?.oldSocketId && existingSubscriber.socketId === opts?.oldSocketId
        ? opts?.oldSocketId
        : undefined;

    if (opts?.oldSocketId) {
      logger.trace({
        msg: 'Clear old subscriber socketId from local cache',
        oldSocketId: opts.oldSocketId,
        workspaceId,
      });
      this.unsetSubscriber(workspaceId, opts.oldSocketId);
    }

    if (!existingSubscriber) {
      this.setSubscriber(subscriber);
      logger.debug({
        msg: 'Adding new subscriber',
        workspaceId: workspaceId,
        userId: subscriber.userId,
        socketId: subscriber.socketId,
        filters: subscriber.filters,
      });
      return true;
    }

    if (opts?.disableUpdate) {
      return false;
    }

    Object.assign(existingSubscriber, {
      socketId: subscriber.socketId,
      filters: subscriber.filters,
      permissions: subscriber.permissions,
      targetTopic: subscriber.targetTopic,
    });
    // Even on update we still setSubscriber since it could be a socketId update, which thus needs to be set again
    this.setSubscriber(existingSubscriber);
    logger.debug({
      msg: 'Update existing subscriber',
      workspaceId: existingSubscriber.workspaceId,
      userId: existingSubscriber.userId,
      socketId: existingSubscriber.socketId,
      previousSocketId,
      filters: existingSubscriber.filters,
      targetTopic: existingSubscriber.targetTopic,
    });

    return false;
  }

  // Declare the new or updated Subscriber to the rest of the cluster
  private async registerSubscriber(
    subscriber: Subscriber,
    {
      oldSocketId,
      persistedSubscriber,
    }: { oldSocketId?: string; persistedSubscriber?: Subscriber } = {}
  ) {
    const getWorkspaceSubscriber = (subscriber: Subscriber) => ({
      workspaceId: subscriber.workspaceId,
      userId: subscriber.userId,
      sessionId: subscriber.sessionId,
      socketId: subscriber.socketId,
      filters: subscriber.filters,
      permissions: (subscriber.permissions?.ability?.rules || []) as Rules,
      oldSocketId: oldSocketId,
      targetTopic: subscriber.targetTopic,
    });
    const publishedSubscriber: WorkspaceSubscriber =
      getWorkspaceSubscriber(subscriber);

    let promises: Promise<any>[] = [];

    // So we can know later on if this user can reuse this socketId for contexts persistence throughout disconnections
    promises.push(
      this.cache
        .registerSocketId(
          subscriber.workspaceId,
          subscriber.sessionId,
          subscriber.socketId
        )
        .catch((err) => logger.error({ err }))
    );

    // So future prismeai-events instances know when to emit events to this subscriber
    promises.push(
      this.cache
        .registerSubscriber(
          subscriber.workspaceId,
          subscriber.sessionId,
          subscriber.socketId,
          persistedSubscriber
            ? getWorkspaceSubscriber(persistedSubscriber)
            : publishedSubscriber
        )
        .catch((err) => logger.error({ err }))
    );

    // Inform prismeai-events cluster when & where to send events to this subscriber
    this.cluster.saveLocalSubscriber(publishedSubscriber);

    // Finally, clear subscriber from previous socketId if we just changed it
    if (oldSocketId) {
      promises.push(
        this.cache
          .unregisterSubscriber(
            subscriber.workspaceId,
            subscriber.sessionId,
            oldSocketId
          )
          .catch((err) => logger.error({ err }))
      );
    }

    // Wait for this subscriber to be propagated to the rest of the cluster
    promises.push(this.cluster.waitForSync());

    await Promise.all(promises);
  }

  async unregisterSubscriber(
    subscriber: Omit<Subscriber, 'permissions'>,
    emit?: boolean
  ) {
    logger.trace({
      msg: 'Remove subscriber from local cache',
      socketId: subscriber.socketId,
      workspaceId: subscriber.workspaceId,
    });
    this.unsetSubscriber(subscriber.workspaceId, subscriber.socketId);

    if (!emit) {
      return;
    }

    this.cluster.removeLocalSubscriber(
      subscriber.workspaceId,
      subscriber.socketId
    );

    this.cache
      .unregisterSubscriber(
        subscriber.workspaceId,
        subscriber.sessionId,
        subscriber.socketId
      )
      .catch((err) => logger.error({ err }));
  }

  async isAllowedSubscriberSocketId(
    workspaceId: string,
    sessionId: string,
    socketId: string
  ) {
    return await this.cache.isKnownSocketId(workspaceId, sessionId, socketId);
  }

  async updateLocalSubscriber(
    subscriber: LocalSubscriber,
    update: { socketId?: string; filters?: any }
  ) {
    if (update?.socketId) {
      // Allow keeping same socketIds accross reconnection
      // in order to make runtime socket context persistent through reconnections
      const allowed = await this.isAllowedSubscriberSocketId(
        subscriber.workspaceId,
        subscriber.sessionId as string,
        update.socketId
      );
      if (!allowed) {
        return false;
      }
    }

    let oldSocketId;
    if (update?.filters) {
      subscriber.filters = update.filters;
    }
    if (update?.socketId) {
      oldSocketId = subscriber.socketId;
      subscriber.socketId = update.socketId;
    }

    this.saveSubscriber(subscriber, { oldSocketId });
    this.registerSubscriber(
      {
        socketId: update.socketId || subscriber.socketId,
        sessionId: subscriber.sessionId,
        workspaceId: subscriber.workspaceId,
        filters: subscriber.filters,
        targetTopic: subscriber.targetTopic,
      } as any as Subscriber,
      {
        persistedSubscriber: subscriber,
        oldSocketId,
      }
    );

    return true;
  }
}
