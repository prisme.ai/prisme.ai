import { Socket } from 'socket.io';
import { Subscriptions } from '../../../services/events/subscriptions';
import { ExtendedError } from 'socket.io/dist/namespace';

export const getSubscribersMiddleware =
  (subscriptions: Subscriptions) =>
  async (socket: Socket, next: (err?: ExtendedError) => void) => {
    const socketHandler = socket?.data?.handler;
    if (!socketHandler?.userId) {
      return next(new Error(`Received a socket without authenticated handler`));
    }

    // Handle previous socketId reuse
    if (socketHandler.reuseSocketId) {
      const allowed = await subscriptions.isAllowedSubscriberSocketId(
        socketHandler.workspaceId,
        socketHandler.sessionId,
        socketHandler.reuseSocketId
      );
      if (allowed) {
        socketHandler.logger.info({
          msg: 'Websocket reconnected with its previous socketId during authentication.',
          previousSocketId: socketHandler.reuseSocketId,
        });
        socketHandler.update({
          socketId: socketHandler.reuseSocketId,
        });
      } else {
        socketHandler.logger.info({
          msg: 'Websocket tried to reconnect with a forbidden/invalid previous socketId during authentication.',
          askedSocketId: socketHandler.reuseSocketId,
        });
      }
      delete socketHandler.reuseSocketId;
    }

    try {
      const subscribed = await subscriptions.subscribe(
        socketHandler.workspaceId,
        {
          workspaceId: socketHandler.workspaceId,
          userId: socketHandler.userId as string,
          sessionId: socketHandler.sessionId as string,
          apiKey: socketHandler.apiKey as string,
          authData: socketHandler.authData,
          socketId: socketHandler.socketId,
          filters: socketHandler.filters,
          targetTopic: subscriptions.cluster.localTopic,
        }
      );
      if (socketHandler.disconnected) {
        socketHandler.logger.debug({
          msg: `Websocket disconnected before subscriber readiness.`,
        });
        subscribed.unsubscribe();
        return;
      }

      socketHandler.logger.debug({
        msg: `Websocket's subscriber ready.`,
      });
      socketHandler.setSubscriber(subscribed);
      next();
    } catch (err) {
      next(new Error('Internal error : please try again later.'));
      socketHandler.logger.error({
        msg: 'Disconnect websocket : could not instantiate subscriber',
        err,
      });
    }
  };
