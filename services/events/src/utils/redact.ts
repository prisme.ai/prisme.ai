import { extractObjectsByPath } from './extractObjectsByPath';

export function redact(
  event: Prismeai.PrismeEvent,
  fields: string[],
  replaceWith = 'REDACTED'
) {
  for (const field of fields) {
    set(event, field, replaceWith);
  }
}

export function set<T = any>(obj: any, k: string, v: T): T {
  const parentPath = k.split('.');
  if (parentPath.length === 1) {
    // We're setting a root field
    obj[k] = v;
    return obj;
  }
  const lastKey = parentPath.pop();
  const parentObj = extractObjectsByPath(obj, parentPath);
  if (parentObj && lastKey && lastKey in parentObj) {
    parentObj[lastKey] = v;
  }
  return obj;
}
