import { TextDecoder } from 'util';
import { logger } from '../logger';

const decoder = new TextDecoder();

export function base64ToUnicode(base64: string) {
  try {
    const binString = atob(base64);
    const bytes = Uint8Array.from(binString, (m) => m.codePointAt(0)!);
    return decoder.decode(bytes);
  } catch {
    logger.error({
      msg: `Could not convert supposed base64 string to unicode string`,
      base64,
    });
    return base64;
  }
}
