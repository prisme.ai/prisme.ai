module.exports = () => {
  process.env.TZ = 'UTC';

  return {
    testEnvironment: 'node',
    coveragePathIgnorePatterns: [],
    transformIgnorePatterns: ['node_modules/(?!(mime)/)'],
    testTimeout: 500,
  };
};
