const date = new Date(Date.now());
import fs from 'fs';

const major = Array.from(`${date.getFullYear()}`).splice(2).join('');
const minor = `${date.getMonth() + 1}`.padStart(2, '0');
const patch =
  `${date.getDate()}`.padStart(2, '0') +
  `${date.getHours()}`.padStart(2, '0') +
  `${date.getMinutes()}`.padStart(2, '0');

const version = `${major}.${minor}.${patch}`;

const packages = [
  `${__dirname}/../package.json`,
  ...fs.readdirSync(`${__dirname}/../services`).flatMap((path) => {
    const pkgPath = `${__dirname}/../services/${path}/package.json`;
    if (fs.existsSync(pkgPath)) return [pkgPath];
    return [];
  }),
];

packages.forEach((path) => {
  const pkg = JSON.parse(fs.readFileSync(path).toString());
  pkg.version = version;
  fs.writeFileSync(path, JSON.stringify(pkg, null, '  '));
});

process.stdout.write(`v${version}`);
process.exit();
