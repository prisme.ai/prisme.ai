# Instructions

## Logic-related

### Conditions

Conditionally execute instructions

**Parameters :**

No parameter

**Children nodes :**  
Every conditions will be represented as parallel branches on your graph, each beginning with a blue box allowing you to edit the condition itself, followed by the instruction that you will add for that specific condition.

A **default** branch is automatically added to configure what instructions to execute when no other condition matches.

[More details on Conditions syntax](../conditions)

### Repeat

Execute a list of instructions N times, or for each record from an array or object variable

**Parameters :**

- **on** : An array or object variable name to loop on. Each record will be available inside an **item** variable, and current index will be inside an **$index** variable.
- **until** : A number of times to repeat this loop. Each iteration number will be stored inside an **item** variable

If **on** and **until** parameters are used together, only the first **until** items from **on** will be iterated.

**Children nodes :**  
When adding a **Repeat** instruction, a new graph branch will appear, allowing you to create the instructions that will be repeated.

#### Parallel batches

**Repeat** can also run **batches** of iterations in parallel.  
Each batch is only started after all iterations from previous batch are done.  

**Parameters :**

- **batch.size** : Number of iterations executed at the same time. Each batch is only started after all iterations from previous batch are done
- **batch.interval** : Wait interval in ms between each batch

**Example :**  
```yaml
  - set:
      name: workQueue
      value:
        - one
        - to
        - three
        - four
        - five
        - six
        - seven
        - height
        - nine
        - ten
  # Initialize now our output variable so we'll be able to access our batch results
  - set:
      name: output
      value:
        lastItem: ''
        list: []
  - repeat:
      'on': '{{workQueue}}'
      batch:
        size: 3 # Process 3 items at once
        interval: 500 # Pause 500ms between each batch
      do:
        - set:
            name: wait
            value: '{% rand(1, 600)  / 1000 %}'
        # Randomize the order of our items inside each batch
        - wait:
            timeout: '{{wait}}'            
        - set:
            name: output.list[]
            value:
              item: '{{item}}'
              index: '{{$index}}'
              waited: '{{wait}}'
        - set:
            name: output.lastItem
            value: '{{item}}'

# Output example : {"lastItem":"ten","list":[{"item":"one","index":0,"waited":0.063},{"item":"three","index":2,"waited":0.107},{"item":"two","index":1,"waited":0.488},{"item":"six","index":5,"waited":0.036},{"item":"five","index":4,"waited":0.15},{"item":"four","index":3,"waited":0.422},{"item":"nine","index":8,"waited":0.188},{"item":"seven","index":6,"waited":0.371},{"item":"height","index":7,"waited":0.408},{"item":"ten","index":9,"waited":0.301}]}
```

In order to access resulting variables after this batch repeat, these variables must be initialized before (i.e here, **output**).  
Any other variable (which was not already defined) set inside the batch repeat won't be visible outside.  

### Break

Stops currently executing automation

**Parameters :**

- **scope** : possible values : **all** (breaks all parents automations), **automation** (breaks current automation only), **repeat** (breaks parent repeat)
- **payload** : Any payload which can be retrieved from parent try / catch instruction with `{{$error.break}}` variable

When **break** is meant to be handled from a parent automation try / catch, **scope** must be set to **all**. Otherwise, it would only break current automation, letting parent automation continue its **try** clause.  

### All

Execute the given instructions in parallel

**Exemple :**  
```yaml
- all:
    - automation1: {}
    - automation2: {}    
```

### Try / Catch

Handle any unexpected error that would otherwise make the current automation crash.  
An optional **catch** parameter allows executing specific instructions upon error.  

Whenever an error is raised, it is saved inside a `{{$error}}` variable, accessible both inside & outside the **catch**.  

This also handles any **break** instruction including its optional payload.  

**Example :**  
```yaml
- try:
    do:
      - automation1: {}
      - thisCanRaise: {}    
    catch:
      - set:
          name: output
          value: "{{$error}}"
- set:
    name: internalMsg
    value: "{{$error}} is still accessible"  
```

## Variable-related

### Set

Set a new or existing variable

**Parameters :**

- **name** : Variable name
- **value** : Variable value (might be a JSON object, a string, a number, ...)
- **type** : **replace** to replace target variable with given value, **merge** to try merging objects or arrays, or **push** to push to an array. **replace** by default

#### Objects manipulation  

When setting object fields, parent objects are created on-the-fly :

```
- set:
    name: some.house.field
    value: ok
```

Here, `some` and `some.house` are automatically created :

```
  "some": {
    "house": {
      "field": "ok
    }
  }
```

It is also possible to create lists and automatically add items to their end with a variable name suffix `[]` :

```
- set:
    name: session.names
    type: push
    value: Mickael
# or :
- set:
    name: session.names[]
    value: Mickael
```

Each time this set is executed, `Mickael` is appended to the `session.names` list variable.  

Same feature with **type: merge** option :  
```
- set:
    name: session.names
    type: merge
    value: Mickael
```

When both previous value and **value** are arrays, **type: merge** concatenate **value** at the end of the previous value :  
```yaml
- set:
    name: myArray
    value:
      - one
- set:
    name: myArray
    type: merge
    value:
      - two
      - three
# {{myArray}} = ['one', 'two', 'three']
```  

When both previous value and **value** are objects, **type: merge** merge them together :  
```yaml
- set:
    name: 'myObject'
    value:
      firstName: Martin
- set:
    name: 'myObject'
    type: merge
    value:
      age: 25 
# {{myObject}} = { "firstName": "Martin", "age": 25 }
```  

#### Expressions

We can also use [expressions](./conditions.md) inside `value` parameter, with all the same features (arithmetic operators, comparisons, ...) conditions have :  

```yaml
- set:
    name: counter
    value: '{% {{counter}} + 1 %}
```

Note that `{% ... %}` is not specific to the `set` instruction but can be used anywhere in automations.  

### Delete

Delete a variable

**Parameters :**

- **name** : Variable name

## Interaction-related

### Fetch

Sends an HTTP request to call external web services

**Parameters :**

- **URL** : Target URL
- **method** : Request method (get | post | put | patch | delete)
- **headers** : Request headers
- **query** : Query string (as an object)
- **body** : Request body (might be a JSON object, a string, a number, ...)
- **multipart** : List of field definitions for multipart/form-data requests
- **emitErrors** : Boolean enabling or disabling error events upon 4xx or 5xx responses. Enabled by default.  
- **stream** : For [streamed SSE responses](https://developer.mozilla.org/en-US/docs/Web/API/Server-sent_events/Using_server-sent_events), emit data chunks as they are received
- **outputMode** : Choose between 'body' | 'detailed_response' | 'data_url' | 'base64'. Defaults to 'body'  (output will be the response body), pick `detailed_response` in order to access response headers within `output.headers` and body within `output.body` 
- **output** : Name of the variable that will store the response body

When receiving 4xx or 5xx HTTP errors, a native event `runtime.fetch.failed` is automatically emitted, including both request & response contents.

If **Content-Type** header is set to 'application/x-www-form-urlencoded', the **body** will be automatically transformed into an urlencoded body.

**multipart**

```
- fetch:
    url: ..
    multipart:
      - fieldname: file
        value: someBase64EncodedFile | someBufferArray
        filename: filename.png # Required if value is a file
        contentType: image/png # Optional
      - fieldname: metadata
        value: some random metadata
```

**Handling SSE**  
By default, HTTP responses indicating a `content-type: text/event-stream` header will force their **responseMode** to `detailed_response`, and their `{{output.body}}` will be a stream that can be read in real time from the calling automation :  

```yaml
- fetch:
    url: "some SSE url (i.e openai /chat-completion endpoint)"
    output: output
- repeat:
    'on': '{{output.body}}'
    do:
      - set:
          name: test[]
          value: '{{item}}'    
```

Here, the `repeat` instruction will block until all chunks have been processed & the HTTP socket is closed.  
Each `{{item}}` will look like this :  
```json
{
  "chunk": {
    "data": [
      "data1",
      "data2"
    ]
  }
}
```
Here, `data1` and `data2` correspond to 2 individual chunks written by the remote server, which can be grouped together if they were received at the same time.  
If these data strings are valid JSON, they will be automatically parsed into objects.  


Alternatively, we can `emit` received chunks so they can be processed asynchronously & concurrently from other automations :  

```yaml
- fetch:
    url: ..
    stream:
      event: chunk
      payload:
        foo: bar
```

Each chunk will be emitted like this :  
```json
{
  "type": "chunk",
  "payload": {
    "chunk": {
      "data": [
        "data1",
        "data2"
      ]
    },
    "additionalPayload": {
      "foo": "bar"
    }
  }
}
```

An ending chunk can be specified with **endChunk** :  
```yaml
- fetch:
    url: ..
    stream:
      event: chunk
      endChunk:
        foo: bar
```


**stream** option also accepts **target** and **options** fields from emit instruction.  

**AWS Signature V4**  
The `auth.awsv4` parameter allows configuring an access / secret access key pair to automatically sign your request with [**AWS Signature V4**](https://docs.aws.amazon.com/AmazonS3/latest/API/sig-v4-authenticating-requests.html) :  

```yaml
  - fetch:
      url: https://bedrock-runtime.eu-west-3.amazonaws.com/model/{{model}}/invoke
      auth:
        awsv4:
          accessKeyId: ''
          secretAccessKey: ''
          service: bedrock
          region: 'eu-west-3'
      method: POST
      body:
        inputText: Hello
      output: output
      emitErrors: true
```

**service** and **region** are optional and automatically calculated from given url.  


### Emit

Emit a new event.  
Events size must not exceed **100Ko**, larger events are blocked and an error event is emitted.

**Parameters :**

- **event** : Event name
- **payload** : JSON object that will be sent as the event payload
- **target** : An optional object specifying this event target
- **autocomplete** : An optional object helping to find all events that can be computed when you set variables in your event value
- **private** : Optionnal Boolean. Exclude this event name from autocomplete values. Set it to `false` if you don't want your event is known from out of your application.
- **options** : optional object  

**target parameters :**

- **userId** : target user id
- **sessionId** : target session id
- **userTopic** : target userTopic

Event targets are automatically granted read access to the event

**options parameters :**  

- **persist** : boolean enabling or disabling events persistence. Enabled by default.  

**autocomplete parameter :**

When a field use an **autocomplete** widget and ask for **events:emit** or **events:listen** data source, they will be extracted from current workspace and all its installed apps instructions. If the event is a simple string, no problem, but if it contains variables, this is a way to help people to know what are the expected values.

- **from** : should be **config** to target the current workspace configuration or **appConfig** to target the installed app from where the instruction comes configuration.
- **path** : a JSON Path string starting from the **from** target. The syntax is described in [this project](https://github.com/JSONPath-Plus/JSONPath).
- **template** : sometimes the event name is not explicit because it comes from computed variables. You can help building values with a string containing static caracters and a `${value}` placeholder.

Exemples :

With this in your workspace DSUL:

```yaml
config:
  value:
    things:
      - foo
      - bar
```

```yaml
- emit:
    event: 'event.{{var}}'
    autocomplete:
      var:
        from: config
        path: things~
```

or

```yaml
- set:
    name: event
    value: 'event.{{var}}'
- emit:
    event: '{{event}}'
    autocomplete:
      var:
        from: config
        path: things~
        template: event.${value}
```

will generate automplete values : _event.foo_ and _event.bar_.

### Wait

Wait for an event. Pauses the current execution until the requested event is received.

**Parameters :**

- **oneOf** : Pauses until **one** of these events is received
- **timeout** : If **waits** timeouts after **N** seconds, resume execution & outputs an empty result. Defaults to **20**
- **output** : Name of the variable that will store the received event

**oneOf parameter :**  
List of event candidates with the following parameters :

- **event** : Event name, required
- **filters** : Free form JSON object for filtering on any event fields. For example :

```yaml
- wait:
    oneOf:
      event: someEvent
      filters:
        source.correlationId: '{{run.correlationId}}'
```

- **cancelTriggers** : If true, cancels the execution of the usual triggers for this event. Other **waits** still receive this event.

**Waiting for an event triggered automation to finish**

In addition to every regular events which might be directly listened by automations, **wait** instruction can also waits for **runtime.automations.executed** event.  
This native events tells when an automation finishes executing, with its source trigger & output.  
Waiting for this allows you to emit an event, than hang until whichever automation that received it finishes processing :

```yaml
AutomationA:
  name: AutomationA
  when:
    endpoint: true
  output: '{{output}}'
  do:
    - emit:
        event: 'someEvent'
        output: emittedIntentEvent
    - wait:
        oneOf:
          - event: runtime.automations.executed
            # Filters specifically for our emited event
            filters:
              payload.trigger.id: '{{emittedIntentEvent.id}}'
        output: event
    - set:
        name: output
        value: '{{event.payload.output}}'

AutomationB:
  name: AutomationB
  when:
    events:
      - someEvent
  output: 'someOutput'
  do: []
```

### createUserTopic

Create a new userTopic.

User topics allow sending events to multiple users without knowing who they are in advance, automatically granting them read access to these events without requiring any API Key.

**Parameters :**

- **topic** : Topic name
- **userIds** : List of the first userIds to join this topic

### joinUserTopic

Makes a given user join a userTopic.

Joining a userTopic automatically grants read access to any event sent within this topic.

**Parameters :**

- **topic** : Topic name to join
- **userIds** : List of the userIds to join this topic. If not defined, automatically pick current user's id

### rateLimit

Enforce custom rate limits per consumer during automation execution.  

**Required parameters :**

- **name** : Rate limit name, i.e your automation name  
- **consumer** : Consumer stable string identifier (user id, ip, project id, ...)
- **window** : Rate limit window in seconds (60 for 1 minute, 3600 for 1 hour, ...)
- **limit** : Maximum number of points consumer can use over given window

**Optional parameters :**

- **consume** : Number of points we want to consume per call (defaults to **1**)
- **break** : Set to false in order to receive errors instead of breaking current automation (defaults to **true**)

**Example :**  

```yaml
  - rateLimit:
      name: FetchMyApi
      window: 60
      limit: 2
      consumer: '{{user.id}}'
      break: false # We want to handle errors ourself
      output: limits
  - conditions:
      '!{{limits.ok}}':
        ...
```

Output **limits** variable will be :  

```json
{
  "operation": "FetchMyApi",
  "retryAfter": 58.458, // Number of seconds we have to wait before our limit resets
  "limit": 2,
  "window": 60,
  "remaining": 0,
  "ok": false,
  "consumer": "<consumer user id>"
}
```
