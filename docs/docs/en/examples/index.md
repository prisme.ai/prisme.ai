# Prisme.ai guides

Explore Prisme.ai features and discover our best practices with our guides. 

<div class="grid cards" markdown>

-   :material-lightning-bolt:{ .lg .middle } __Templates__

    ---

    Discover our best practices through our templates.   
    Those can be imported directly in the Builder and help you jump start your Prisme.ai journey.

    [:octicons-arrow-right-24: Coming soon](#)

-   :material-bookshelf:{ .lg .middle } __Tutorials__

    ---

    Focus on __learning the basics__ of the platform with step by step tutorials.

    [:octicons-arrow-right-24: Start with the first tutorial](./form-genai.md)

-   :fontawesome-solid-graduation-cap:{ .lg .middle } __Academy__

    ---

    Expand your knowledge of Prisme.ai through short comprehensive videos.

    [:octicons-arrow-right-24: Coming soon](#)

</div>