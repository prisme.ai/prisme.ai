# Welcome

Prisme.ai is a Generative AI platform that allows you to create, connect, expose and govern your AI agents on the infrastructure and with the LLM/SLM of your choice, while controlling your Gen AI ROI.

The Prisme.ai platform offers a wide range of products which works together to leverage the power of Gen AI.  
This documentation will give you insight on each product, how they work and how you can use them.

## Products
Discover the different products included in the Prisme.ai platform.

<div class="grid cards" markdown>

-   :material-message-processing:{ .lg .middle } __AI SecureChat__

    ---

    Give access to your users to a __secure chat__, using your own SLM/LLM.

    [:octicons-arrow-right-24: Coming soon](#)

-   :material-storefront:{ .lg .middle } __AI Store__

    ---

    Learn more about how the AI Store can help __centralize your AI__.

    [:octicons-arrow-right-24: Coming soon](#)

-   :fontawesome-solid-brain:{ .lg .middle } __AI Knowledge__

    ---

    Build on your knowledge with our __RAG as a service__.

    [:octicons-arrow-right-24: Coming soon](#)

-   :material-bookshelf:{ .lg .middle } __AI Collection__

    ---

    Unlock the potential of your data, learn how the AI Collection works.

    [:octicons-arrow-right-24: Coming soon](#)

-   :material-eye:{ .lg .middle } __AI Insights__

    ---

    Keep an eye on your agents using the AI Insights and optimize their performance.

    [:octicons-arrow-right-24: Coming soon](#)

-   :fontawesome-solid-wand-magic-sparkles:{ .lg .middle } __AI Builder__

    ---

    Connect and orchestrate your AI agent with the AI Builder.

    [:octicons-arrow-right-24: See the documentation](./workspaces/index.md)

</div>