# Example of Platform Sizing in Self-Hosted Mode

**Note:** This sizing depends on the use case, whether it's full automation (https://docs.eda.prisme.ai/en/architecture/technical/runtime/specifications/#performance-scalability) or chat mode. We recommend that you do your own testing depending on your infrastructure and use cases

!!! info "Access"

    We recommend conducting your own load testing tailored to your specific infrastructure and use cases.
    <!-- Depends on your use case -->

## Desired KPIs

- **Number of interactions per user:** 4 to 10 interactions
- **Response time for the first token with OpenAI API (P95):** 478 ms
- **Number of simultaneous users:** 100 new users each second


## Kubernetes Cluster

- **Node Configuration:** 5 nodes with 8 GB RAM and 4 vCPU each

## Databases

### EFS (Elastic File System)

- **Disk Space:** 50 GB
- **Note:** Can be shared or isolated between different environments

### Object Storage

- **Bucket Separation:**
    - 1 bucket “models” per environment
    - 1 bucket “uploads” per environment
    - 1 bucket “uploads-public” per environment behind a CDN

### MongoDB

- **Data Types:** RBAC permissions, users, application data
- **Configuration:**
    - Replica set of 3 nodes
    - 2 GB RAM and 2 vCPU per node
    - 1,000 IOPS
- **Disk Space:** 10 GB
- **Sharing:** Cluster can be shared with database separation
- **Database Separation:**
    - 1 “permissions” database per environment
    - 1 “users” database per environment
    - 1 “collections” database per environment
- **MongoDB Version:** 6 → 7

### Redis Cache & Broker

- **Data Types:** Real-time EDA streams, permission cache, OIDC sessions, rate limits, application cache
- **Configuration:**
    - 1 master and 2 replicas
    - 3 GB RAM and 2 vCPU per node
- **Environments:** 1 cluster per environment
- **Redis Version:** 5

### Redis Crawler

- **Data Types:** Crawl queue, metadata of known documents, search engine configurations
- **Configuration:**
    - 1 master and 2 replicas
    - 2 GB RAM and 2 vCPU per node
- **Sharing:** Cluster can be shared with database separation
- **Database Separation:**
    - 1 database per environment
- **Redis Version:** 5
- **Scaling Example:**
    - 100,000 documents → 600 MB RAM

### Elasticsearch/OpenSearch

- **Data Types:** Persisted EDA events for traceability and statistics calculations, text content of crawled documents
- **Configuration:**
    - 3 nodes with 8 GB RAM and 4 vCPU
- **Disk Space:**
    - 400 GB per node, NVMe or SSD
- **Elasticsearch Version:** 8+

### Vector Database (Redis Stack)

- **Data Types:** Text chunks accompanied by their vector
- **Configuration:**
    - 1 master and 2 replicas
    - 5 GB RAM and 2 vCPU per node
- **Environments:** 1 cluster per environment
- **Redis Version:** 5+ with **SEARCH** and **JSON** modules
- **Scaling Example:**
    - 100,000 chunks from 20,000 documents → 2.6 GB RAM
