# Permissions management

With a fully featured Prisme.ai platform, authorization process happen at multiple layers :  

1. **Super admins** configured at **infrastructure** level for technical or management people, grant acces to all workspaces  
2. **Workspaces roles** declared **workspace by workspace** for individual or management people
3. **SSO accesses** declared **workspace by workspace** for end users 
4. **Product roles** for fine-grained permissions, **product by product** for groups of users   

In order to fully understand below documentation, it is important to distinguish the 2 following terms :  


**[Workspaces](https://docs.eda.prisme.ai/en/workspaces/#workspaces)** :  

  * Refer to the building projects created with Prisme.ai low-code capabilities    
  * In more traditionel IT environments, these would be standalone software develoment projects & teams  
  * Workspaces can be accessed from the left menu with **AI Builder**    

**Products** :  

  * Web applications built on workspaces, these are web pages or APIs designed for end users.  
  * These users do not have access to the building **workspaces**   
  * Products are listed on the left menu below **AI Builder**  


## Super Admins  

Super admins emails can be configured with **SUPER_ADMIN_EMAILS** environment variable inside **api-gateway** microservice, or with `api-gateway.config.admins` helm value (`prismeai-core` chart).  

**Super admins** can access, share and update **every existing workspace**, including its events, configuration, automations, ...  
It's basically the most powerful role and the owner of every existing workspace.  

These accounts are generally used to install/update and [configure Prisme.ai products](./configuration.md), but they are not needed on a day to day basis.  We recommend super-admin peoples to [share most important workspaces](#workspaces-roles) (AI Knowledge, AI Store) with a secondary account.  

Although they manage every workspaces, **super admins** might not always have admin access to all products since these products can have [their own permissions layer](#product-roles).  
However and since they access all products' workspaces, they always can manage to grant themselves necessary permissions from inside the workspace.  

## Workspaces roles    

Workspaces can be shared to specific user with owner, editor or [any other custom role](https://docs.eda.prisme.ai/en/workspaces/security).  
These users are then able to update the workspace configuration or secrets, search through activity events for debugging or audit purposes, update automations/pages to deliver new features, ...  

## SSO accesses

Existing Prisme.ai products (like [AI Knowledge, AI Store](./configuration.md#sso-authentication), ...) use [custom security rules](https://docs.eda.prisme.ai/en/workspaces/security/#binding-roles-with-auth-providers) to automatically grant access to their pages to SSO-authenticated end users.  
These custom rules generally give only access to the workspace **user pages**, but they could also be used to give full workspace access exactly like for [**owner users**](#workspaces-roles).  


## Product roles

AI Governance (one of Prisme.ai products) allows managing existing users to enable/disable them, update basic information, manage **groups of users**, or give them **product roles**.  

**Product roles** are fine grained roles to manage users permissions inside each distinct product.  
For example, the default **Product Admin** role allows accessing AI Knowledge analytics, every existing AI Knowledge projects or create new ones. On the other hand, the **Knowledge User** can only access AI Knowledge projects shared with him and not even create now projects.  

But remember, even **Product Admin** users cannot change **AI Knowledge** configuration (LLM providers, models, rate limits, ...) if they do not have access to **AI Knowledge** workspace !  

In order to grant management people access to this AI Governance product, a **Manager** role must be given from the same **Users & Permissions** page.  



