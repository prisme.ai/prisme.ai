# AI Products initialization

Once the prerequisites detailed below have been met, importing the Prisme.ai App Store and associated product workspaces involves three steps, depending on the interdependencies of the Apps during initialization.  
Each archive must be imported into the studio in the correct order, with functionality tests performed after each import before moving to the next step. To import an archive, navigate to the studio, hover over the "**Create a workspace**" button, and select "**Import a workspace from an archive**" from the menu.  

Each archive can be re-imported to reset all Apps/workspaces it contains, overwriting any modifications made in the studio since the first import.  

It is recommended to perform archive imports using a studio account dedicated to this task to avoid cluttering the dashboard of an administrator account with the multiple app workspaces.  


## Prerequisites

**A fully operational prismeai environment, including private microservices.**    

**Open Source Services:**  

* prismeai-api-gateway  
* prismeai-console  
* prismeai-pages  
* prismeai-workspaces  
* prismeai-events  
* prismeai-runtime  

**Private Services:**  

* prismeai-functions: Executes arbitrary JS or Python code  
* prismeai-crawler: Web document indexing  
* prismeai-searchengine: Search engine  
* prismeai-llm: Optional, only required if **AI Knowledge** is used with local models  
* prismeai-nlu: Optional: Required only if NLU-based intents and entity recognition are needed.

**For offline environments, [see additional prerequisites](../../installation/index.md#offline-environments).**  

## Configuration  

Several environment variables must be configured in the **prismeai-runtime** microservice to ensure the proper functioning of the installed Apps. If the default API ports are modified by a container orchestrator or configuration, these values must be adapted accordingly.  

All of these variables should be already initialized with our Helm charts.  

**Custom Code**  
```plaintext
APP_CONFIG_CustomCode_apiUrl='http://FUNCTIONS_HOST:4000'
```

`FUNCTIONS_HOST` refers to the domain from which the `prismeai-runtime` microservice can reach `prismeai-functions`.

**Collections**  
```plaintext
WORKSPACE_CONFIG_collection_mongoURI='mongodb://MONGO_HOST/collections'
```

`MONGO_HOST` should be replaced with the host that allows the `prismeai-functions` microservice to connect to MongoDB (e.g., `mongo` if `prismeai-functions` and `mongo` are launched from the same docker-compose).

**Crawler**  
```plaintext
APP_CONFIG_Crawler_apiUrl='http://SEARCHENGINE_HOST:8000'
```

`SEARCHENGINE_HOST` is the domain from which `prismeai-runtime` can reach `prismeai-searchengine`.

**LLM Open-Source**  
```plaintext
APP_CONFIG_LLMOpen-Source_apiUrl='http://LLM_HOST:8888'
```

`LLM_HOST` is the domain from which `prismeai-runtime` can reach `prismeai-llm`.  

**The LLM Open-Source App is only required for using AI Knowledge with open-source models instead of OpenAI.**

**NLU**  
```plaintext
APP_CONFIG_NLU_apiUrl='http://NLU_HOST:8000'
```

`NLU_HOST` is the domain from which `prismeai-runtime` can reach `prismeai-nlu`.

**The NLU App is not required for the operation of AI Products.**

**SendMail**  
```plaintext
APP_CONFIG_SendMail_apiKey='a mailgun API key'
APP_CONFIG_SendMail_region='https://api.eu.mailgun.net/'
APP_CONFIG_SendMail_domain='the mailgun domain'
```

**The SendMail App is currently compatible only with MailGun. It remains optional and is not required for the proper functioning of the AI products.**


## Package 1: apps-base1.zip

This archive mainly contains the following 2 Apps, which are necessary for building many others:  

* **Custom Code**: Execution of custom JS/Python code (requires the **functions** microservice to be online and configured).  
* **Prismeai API**: API calls to Prisme.ai from Automations (YAML files).  

Proceed with the import before moving to configuration: in the studio, hover over the **Create a workspace** button, click on the three-dot menu, and then select **Import a workspace from an archive**. Refresh the page after the loading icon finishes. The three new workspaces should now appear.  
The detailed result of the import (i.e., errors & successful imports) can be found in the logs of the **workspaces** microservice or individually in each imported workspace with `workspaces.imported` events.  

### Tests

1. Create a test workspace
2. Install the `Custom Code` App in your test workspace.  
3. Create the following function, `hello`:  

```js
import fetch from 'node-fetch';
const res = await fetch('https://httpbin.org/headers')
return await res.json();
```
4. Test the function using the green play button  
5. Make sure that `node-fetch` package has been properly installed and that you receive the expected website response without any import/internal errors.  


## Package 2: apps-base2.zip

This archive contains the following five Apps:  

* **Collection**: MongoDB (requires the **Custom Code** App to work correctly).  
* **Crawler**: Web page indexing & search engine (requires the **crawler** and **searchengine** microservices).  
* **NLU**: NLU engine (requires the **nlu** microservice).  
* **RedisSearch**: Interface with the redis-stack-server database.  

Proceed with importing the `apps-base2.zip` archive before moving to the tests.

### Tests

Open the created **PlatformTests** workspace  

**Collection**

1. Open the **Collection/TestCollection** automation
2. Run the automation from the top-right corner play button
3. Make sure you receive a valid JSON response including a "Toulouse" city object  

**Crawler**

1. Open the **Crawler/Crawl page** automation & run it with the top-right corner play button  
2. Open the **Activity** tab and regularly reload it until you see **Crawler.notifications event has been fired**  
    * If this never occurs, please check **prismeai-crawler** container logs (**apps namespace**) to resolve issues  
3. Open the **Crawler/Search page** & run it : make sure you receive the freshly indexed page result   

## Package 3 : apps-extended.zip

The main components having now been imported, it is time to initialize main Prisme.ai AI products.  

Since the `apps-extended.zip` archive is larger than the first two, the import request may time out, and the loading icon might spin indefinitely. However, the process will continue in the background and finish within a minute or two, with the log `Terminated bulk import` from the **workspaces** microservice likely being one of the last logs to appear:  
```json
{"level":30,"time":1691420134138,"app":"core-prismeai-workspaces","msg":"Terminated bulk import","result":{"imported":[...],"errors":[...],"createdWorkspaceIds":[...],"updatedWorkspaceIds":[],"publishedApps":[...]}}
``` 

Several workspaces should appear during the first refresh after this log.  

### Tests & Configuration

Congratulation, you can now proceed with [AI products configuration](./configuration.md) !  

## Troubleshooting  

### Custom Code
Several critical AI products and Apps (including AI Knowledge) are built on Custom Code. If any issues arise, monitor the logs of the `prismeai-functions` service, as some functions or dependencies may not be properly initialized or installed.  

Look for the following error messages:  

* `Failed npm install`
* `Dependencies are still being installed from`
* `Some dependencies are missing`
* `TimeoutError`
* `Failed running function`

If a dependency is missing or still being installed after several minutes, it may be necessary to re-save the **Custom Code** App for the affected workspace from the studio. This will prompt `prismeai-functions` to restart the installation (`Installing N modules ...`).  
If the issue concerns functions of an App (indicated by the `app.` prefix in the logs), it will also be necessary to re-publish the App.  
