# Getting started

Use Prisme.ai’s API to build custom integrations, streamline workflows, and create tailored solutions for your uses cases.

This documentation provides all the information needed to start using the Prisme.ai API.
You will also find the specification of the DSUL syntax. 

<div class="grid cards" markdown>

-   :material-lightning-bolt:{ .lg .middle } __Prisme.ai API__

    ---

    Explore our API reference to learn more about the Prisme.ai API and the DSUL syntax.

    [:octicons-arrow-right-24: Explore](/api){:target="_blank"}

</div>